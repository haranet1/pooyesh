<?php

use App\Models\Article;
use App\Models\ArticleCategory;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;


// Home
Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push('خانه', route('home'));
});

// Home > mag
Breadcrumbs::for('mag', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('مجله', route('articles.blog'));
});

// // Home > mag > [Category] 
Breadcrumbs::for('category', function (BreadcrumbTrail $trail, Article $article) {
    $trail->parent('mag');
    $trail->push($article->category->title, route('category.blog.search', $article->category->slug));
});

// Home > mag > [Category] > [title]
Breadcrumbs::for('article', function (BreadcrumbTrail $trail, Article $article) {
    $trail->parent('category', $article);
    $trail->push($article->title, route('single.article.blog', $article));
});