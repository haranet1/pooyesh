<?php

use App\Models\User;
use App\Jobs\SendEmail;
use App\Models\Payment;
use App\Models\University;
use App\Mail\UserRegistred;
use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use App\Models\OjobPosition;
use Illuminate\Http\Request;
use App\Models\PersonalityJob;
use App\Mail\VerificationEmail;
use App\Models\EventTicketCode;
use App\Models\MbtiPersonality;
use App\Http\Middleware\isCompany;
use App\Models\CooperationRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Filament\Forms\Components\Group;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Models\PooyeshRequestProcess;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CvController;
use App\Http\Controllers\InfoController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\StudentController;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ExtraUserController;
use App\Http\Controllers\Onet\JobsController;
use App\Http\Controllers\Onet\OnetController;
use App\Http\Controllers\AccountingController;
use App\Http\Controllers\Event\PlanController;
use App\Http\Controllers\Event\SansController;
use App\Http\Controllers\ProtectionController;
use App\Http\Controllers\RoleAssignController;
use App\Http\Controllers\Event\EventController;
use App\Http\Controllers\Sadra\SadraController;
use App\Http\Controllers\AcademicInfoController;
use App\Http\Controllers\Admin\ResumeController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Onet\HireListController;
use App\Http\Controllers\Search\SearchController;
use App\Http\Middleware\CheckInternPositionLimit;
use App\Http\Controllers\Ajax\FrontAjaxController;
use App\Http\Controllers\GeneralPaymentController;
use App\Http\Controllers\UserManagementController;
use App\Http\Controllers\Job\JobPositionController;
use App\Http\Controllers\Job\Position\CoController;
use App\Http\Controllers\Job\Request\StdController;
use App\Http\Controllers\Pooyesh\PooyeshController;
use App\Http\Controllers\Ajax\CompanyAjaxController;
use App\Http\Controllers\Job\Position\EmpController;
use App\Http\Controllers\Ajax\EmployeeAjaxController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Frontend\SinglePageController;
use App\Http\Controllers\PersonalityTests\GhqController;
use App\Http\Controllers\PersonalityTests\NeoController;
use App\Http\Controllers\PersonalityTests\MBTIController;
use App\Http\Controllers\PersonalityTests\HaalandController;
use App\Http\Controllers\PersonalityTests\EnneagramController;
use App\Http\Controllers\Event\TicketController as EventTicketController;
use App\Http\Controllers\Job\Request\CoController as RequestCoController;
use App\Http\Controllers\Event\PaymentController as EventPaymentController;
use App\Http\Controllers\Job\Request\EmpController as RequestEmpController;
use App\Http\Controllers\Pooyesh\RequestsController as PooyeshRequestController;
use App\Http\Controllers\Intern\Position\ListController as InternPositionListController;
use App\Http\Controllers\Intern\Request\OptionController as InternRequestOptionController;
use App\Http\Controllers\Intern\Position\OptionController as InternPositionOptionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , [HomeController::class , 'index'])->name('home');

Route::view('FAQ' , 'frontend.faq')->name('faq.h');

Route::get('contact-us' , [HomeController::class , 'contactUs'])->name('contactus.h');

Route::get('job/{id}' , [SinglePageController::class , 'ojob'])->name('job');
// Route::get('ojob/{id}' , [OnetController::class , 'singleOnetJob'])->name('ojob');

Route::get('about-us' , [HomeController::class , 'aboutUs'])->name('aboutus');

Route::prefix('list')->group( function () {
    Route::get('companies' , [HomeController::class , 'companies'])->name('companies.list');
    Route::get('jobs' , [HomeController::class , 'jobs'])->name('jobs.list');
});

Route::prefix('search')->group( function () {
    Route::post('job' , [HomeController::class , 'searchJob'])->name('job.home.search');
    Route::post('mag' , [ArticleController::class , 'searchArticle'])->name('article.blog.search');

    Route::get('companies' , [HomeController::class , 'searchCompany'])->name('companies.search');
    Route::get('jobs' , [HomeController::class , 'searchJobs'])->name('jobs.search');
    Route::post('get-cities' , [FrontAjaxController::class , 'getCities'])->name('get.cities.ajax.front');
});

Route::get('/2', [\App\Http\Controllers\HomeController::class, 'index'])->name('h');

Route::get('/jobs-list', [\App\Http\Controllers\HomeController::class, 'jobs_list'])->name('jobs.list.h');
Route::post('/jobs-ajax', [\App\Http\Controllers\HomeController::class, 'jobs_ajax'])->name('jobs.ajax');
Route::get('/jobs-list/category/{category}', [\App\Http\Controllers\HomeController::class, 'jobs_list_by_category'])->name('jobs.list.by-category');
Route::get('/jobs-list/type/{type}', [\App\Http\Controllers\HomeController::class, 'jobs_list_by_type'])->name('jobs.list.by-type');
Route::post('/jobs-list/experience/type', [\App\Http\Controllers\HomeController::class, 'job_list_by_experience_type'])->name('jobs.list.by-experience-type');
Route::get('/search-jobs', [\App\Http\Controllers\HomeController::class, 'search_jobs'])->name('jobs.list.search');
Route::get('/search-ideas', [\App\Http\Controllers\HomeController::class, 'search_ideas'])->name('ideas.list.search');
Route::get('/search-companies', [\App\Http\Controllers\HomeController::class, 'search_companies'])->name('companies.list.search');

// Route::get('/single-job/{id}', [\App\Http\Controllers\HomeController::class, 'single_job'])->name('job.single');

// Route::get('/company/{id}', [\App\Http\Controllers\HomeController::class, 'single_company'])->name('company.single');

Route::get('/companies-list', [\App\Http\Controllers\HomeController::class, 'companies_list'])->name('companies.list.h');

Route::get('/about-us-h', function () {
    $students_count = User::where('groupable_type', StudentInfo::class)->get();
    $company_count = User::where('groupable_type', CompanyInfo::class)->get();
    $jobs_count = OjobPosition::all();
    return view('front.about-us', compact(['students_count', 'company_count', 'jobs_count']));
})->name('about');

Route::get('/pricing', function () { return view('front.pricing'); })->name('pricing');

Route::get('/faqs', function () { return view('front.faqs'); })->name('faqs');

Route::get('/contact-uss', function () { return view('front.contact-us'); })->name('contact');

Route::get('/test/salamat', function () { return view('front.test-salamat'); })->name('test.salamat');

// magazine routes
Route::prefix('mag')->group( function(){
    Route::get('/' , [ArticleController::class , 'index'])->name('articles.blog');
    Route::get('category/{category}' , [ArticleController::class , 'filterCategory'])->name('category.blog.search');
    Route::get('{article}' , [ArticleController::class , 'show'])->name('single.article.blog');
    Route::post('addview' , [ArticleController::class , 'additionView'])->name('additionView.article');
    Route::post('like' , [ArticleController::class , 'likeOrUnlike'])->name('like.article');
    Route::post('comment' , [CommentController::class , 'store'])->name('store.comment.article');

});

Route::get('work-knowledge', [JobsController::class, 'standardIndex'])->name('standard.index');
Route::get('category/job/', [JobsController::class, 'categoryJob'])->name('job.category');
Route::get('job-info/{id}', [JobsController::class, 'singleJob'])->name('jobs.single');

// public single pages
Route::prefix('single')->group( function () {
    Route::get('company/{id}' , [SinglePageController::class , 'company'])->name('company.single');
    Route::get('candidate/{id}' , [SinglePageController::class , 'candidate'])->name('candidate.single');
    Route::get('job/{id}' , [SinglePageController::class , 'job'])->name('job.single');
});


Route::get('/candidates-list', [\App\Http\Controllers\HomeController::class, 'candidate_list'])->name('candidates.list');

// Route::get('/candidates-single/{id}', [\App\Http\Controllers\HomeController::class, 'single_candidate'])->name('candidate.single');

Route::get('/ideas-list', [\App\Http\Controllers\HomeController::class, 'ideas'])->name('ideas.list');

Route::get('/idea/{id}', [\App\Http\Controllers\HomeController::class, 'singleIdea'])->name('ideas.single');

Route::get('/projects-list', [\App\Http\Controllers\HomeController::class, 'projects_list'])->name('projects.list');
Route::get('/project/{id}/{title}', [\App\Http\Controllers\HomeController::class, 'single_project'])->name('projects.single');
Route::get('/project/{id}', [\App\Http\Controllers\HomeController::class, 'search_projects_by_cat'])->name('projects.search-by-cat');
Route::get('/projects-search', [\App\Http\Controllers\HomeController::class, 'search_projects'])->name('projects.search');
//Route::get('/ticket-to-company',[\App\Http\Controllers\CompanyController::class,'store_ticket'])->name('companies.ticket.store');


Route::post('/password-reset/sms', [\App\Http\Controllers\Auth\ForgotPasswordController::class, 'sendVerifyCode'])->name('password.reset.sms');
Route::post('/password-reset/sms/verify', [\App\Http\Controllers\Auth\ForgotPasswordController::class, 'VerifyCode'])->name('password.reset.sms.verify');
Route::post('/password-reset/sms/reset', [\App\Http\Controllers\Auth\ForgotPasswordController::class, 'passwordReset'])->name('password.change');

// Front Ajax routes --------------------------------

Route::prefix('ajax')->middleware('auth')->group(function(){
    Route::post('get-latest-jobPositions',[FrontAjaxController::class ,'getLatestJobPositions'])->name('front.ajax.get.jobPositions');
    Route::middleware('CheckInfo')->post('cooperation-request' , [FrontAjaxController::class , 'cooperationRequest'])->name('front.ajax.cooperation-request');
    Route::post('co-cooperation-request', [FrontAjaxController::class , 'companyCooperationRequest'])->name('front.ajax.co-cooperation-request');

    // student send resume for company
    Route::post('resume/send' , [FrontAjaxController::class , 'sendResume'])->name('front.ajax.send.resume');
    Route::post('resume/accept' , [FrontAjaxController::class , 'acceptResume'])->name('front.ajax.accept.resume');
    Route::post('resume/reject' , [FrontAjaxController::class , 'rejectResume'])->name('front.ajax.reject.resume');

    // student or applicant send ticket request
    Route::post('ticket/store-company-ticket' , [FrontAjaxController::class , 'storeCompanyTicket'])->name('front.ajax.ticket.store-company-ticket');

    Route::post('add-ojob-to-favorite' , [OnetController::class , 'addFavorite'])->name('add.ojob.to.favorite.ajax');

});

// end Front Ajax routes ----------------------------

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/stdInfo', function () {
    $provinces = DB::table('provinces')->get(['name', 'id']);
    $universities = University::all();
    return view('front.student-info', compact(['provinces', 'universities']));
})->name('set.std.info')->middleware('auth', 'verifyMobile');


// Route::get('/coInfo', function () {
//     return view('front.company-info');
// })->name('set.co.info')->middleware('auth', 'verifyMobile');

Route::get('/empInfo', function () {
    $provinces = DB::table('provinces')->get(['name', 'id']);
    $universities = University::all();
    return view('front.employee-info', compact(['provinces', 'universities']));
})->name('set.emp.info')->middleware('auth', 'verifyMobile');

Route::get('/applicantInfo', function () {
    $provinces = DB::table('provinces')->get(['name', 'id']);
    return view('information.applicant.create',compact('provinces'));
})->name('set.applicant.info')->middleware('auth', 'verifyMobile');

Route::get('/academic-guidance/form', [\App\Http\Controllers\HomeController::class, 'show_academic_guidance_form'])->name('academic-guidance.form');
Route::post('/academic-guidance/store', [\App\Http\Controllers\HomeController::class, 'store_academic_guidance'])->name('academic-guidance.store');

Route::get('/verify-mobile', [App\Http\Controllers\UserController::class, 'showVerifyMobile'])->name('user.showVerifyMobile');
Route::post('/verify-mobile', [App\Http\Controllers\UserController::class, 'verifyMobile'])->name('user.verifyMobile');
Route::post('/register-ajax', [\App\Http\Controllers\HomeController::class, 'registerAjax'])->name('users.registerAjax')->middleware('auth');
Route::post('/front-ajax', [\App\Http\Controllers\HomeController::class, 'frontAjax'])->name('frontAjax')->middleware('auth');

// exams route
// Route::view('/exams', 'panel.student.personalityTests.index')->name('std.exams');
Route::view('/exams', 'frontend.list.tests')->name('std.exams');
Route::get('/enneagram', [EnneagramController::class , 'index'])->name('enneagram.index');
Route::get('/mbti', [MBTIController::class , 'index'])->name('mbti.index');
Route::get('/neo' , [NeoController::class , 'index'])->name('neo.index');
Route::get('/ghq', [GhqController::class , 'index'])->name('ghq.index');
Route::get('/haaland',[HaalandController::class , 'index'])->name('haaland.index');

Route::get('/mbti/test', [MBTIController::class , 'takeTest'])->name('mbti.takeTest');
Route::prefix('mbti')->middleware('auth')->group( function(){
    Route::get('result/test', [MBTIController::class , 'testResult'])->name('mbti.test.result');
    Route::post('show' , [MBTIController::class , 'show_gateway'])->name('test.mbti.show');
    Route::post('config' , [MBTIController::class , 'config'])->name('test.mbti.config');
    Route::get('verify/{paymentId}' , [MBTIController::class , 'verify'])->name('test.mbti.return-url');
    Route::get('receipt/{paymentId}' , [MBTIController::class , 'receipt'])->name('test.mbti.receipt');
});
// end exams route

Route::post('/custom-register', [RegisterController::class , 'customRegister'])->name('auth.customRegister');
Route::post('/custom-login', [RegisterController::class , 'customRegisterLogin'])->name('auth.customLogin');

Route::get('select-role', [InfoController::class , 'selectRole'])->middleware('auth')->name('select.role');
Route::post('store-role', [InfoController::class , 'storeRole'])->middleware('auth')->name('store.role');


Route::prefix('panel')->middleware('auth', 'verifyMobile')->group(function () {

    Route::get('test-sadra-api' , [PaymentController::class , 'registerCompanyToSadraSite']);

    // Route::get('email-temp', function () {
    //     return view('mail.verification-email' , ['name' => Auth::user()->name. ' '. Auth::user()->family , 'link' => 'https://jobist.ir']);
    // });

    Route::get('test-email' , [VerificationController::class , 'test']);

    Route::post('email-verify', [VerificationController::class , 'send'])->name('auth.email.verify');

    // Route::get('test-userMail' , function () {
    //     $message = 'شرکت'. ' ' . Auth::user()->groupable->name . ' ' . ' به لیست شرکت های تایید نشده اضافه شد.';
    //     $link = route('company.un.user');
    //     Mail::to(Auth::user())->send(new UserRegistred($message , $link));
    // });

    // Route::get('email-test' , function () {
    //     Mail::to('steve.em.m@outlook.com')->send(new VerificationEmail(Auth::user()));
    // });

    Route::get('url-test' , function () {
        dd (URL::temporarySignedRoute('verification.email' , now()->addMinutes(15)));
    });

    Route::get('verification-email' , [VerificationController::class , 'verify'])->name('verification.email');

    Route::prefix('info')->group( function () {
        Route::prefix('co')->group( function() { #replace prefix to middalwar
            Route::get('/' , [InfoController::class , 'coCreate'])->name('create.co.info');
            Route::post('co-store' , [InfoController::class , 'coStore'])->name('store.co.info');
        });

        Route::prefix('std')->group( function () {
            Route::get('/' , [InfoController::class , 'stdCreate'])->name('create.std.info');
            Route::post('std-store' , [InfoController::class , 'stdStore'])->name('store.std.info');
        });
    });

    Route::post('/store-stu-info', [\App\Http\Controllers\StudentController::class, 'storeStudentInfo'])->name('students.storeInfo');
    Route::post('/store-emp-info', [\App\Http\Controllers\EmployeeController::class, 'storeEmployeeInfo'])->name('employees.storeInfo');
    Route::post('/store-comp-info', [\App\Http\Controllers\CompanyController::class, 'storeCompanyInfo'])->name('companies.storeInfo');
    Route::post('/store-applicant-info', [\App\Http\Controllers\InfoController::class, 'storeApplicantInfo'])->name('applicants.storeInfo');

    Route::post('/notify/ajax', [\App\Http\Controllers\NotificationsController::class, 'ajax'])->name('notifications.ajax');

    // personality tests routes

    // enneagram test

    Route::get('/enneagram/test', [EnneagramController::class , 'takeTest'])->name('enneagram.takeTest');
    Route::post('/enneagram/step2', [EnneagramController::class , 'step_2'])->name('enneagram.step2');
    Route::post('/enneagram/result', [EnneagramController::class , 'result'])->name('enneagram.test.result');
    // neo test

    Route::get('/neo/test' , [NeoController::class , 'takeTest'])->name('neo.take.test');
    Route::post('/neo/result' , [NeoController::class , 'testResult'])->name('neo.test.result');
    // ghq test

    Route::get('/ghq/test' , [GhqController::class , 'takeTest'])->name('ghq.take.test');
    Route::post('/ghq/result' , [GhqController::class , 'testResult'])->name('ghq.take.result');
    // haaland test

    Route::get('/haaland/test' , [HaalandController::class , 'takeTest'])->name('haaland.take.test');
    Route::post('haaland/result' , [HaalandController::class , 'testResult'])->name('haaland.test.result');
    Route::get('haaland/show-result' , [HaalandController::class , 'showResult'])->name('haaland.show.result');

    // personality result index route
    Route::get('/exams-result', [StudentController::class , 'show_result_exams'])->name('exams.result');
    Route::get('/mbti/show-result', [MBTIController::class , 'showResult'])->name('mbti.show.result');
    Route::get('/enneagram/show-result', [EnneagramController::class , 'showResult'])->name('enneagram.show.result');
    Route::get('/neo/show-result', [NeoController::class , 'showResult'])->name('neo.show.result');
    Route::get('/ghq/show-result', [GhqController::class , 'showResult'])->name('ghq.show.result');
    // ------------------------------

    

    Route::middleware('Dashboard')->group( function () {
        Route::get('/' , function() {
            return redirect()->route('employee.panel');
        })->name('dashboard');

    });

    Route::middleware('CheckInfo' , 'VerifyEmail')->group(function () {

        /** job Routes */
        Route::prefix('job')->group( function () {
            Route::prefix('position')->group( function () {

                Route::post('getJobsByCatId' , [OnetController::class , 'getJobsByCatId'])->name('get.jobs.by.catid');
                Route::post('getJobsByCatIdAndName' , [OnetController::class , 'getJobsByCatIdAndName'])->name('get.jobs.by.catid.and.name');

                Route::middleware('isCompany','AcceptedUser')->group( function () {

                    Route::prefix('index')->group( function () {
                        Route::get('hire' , [CoController::class , 'hireIndex'])->name('co.hire.pos.job');
                        Route::get('intern' , [CoController::class , 'internIndex'])->name('co.intern.pos.job');
                    });

                    Route::get('create' , [CoController::class , 'create'])->name('create.pos.job');
                    Route::post('show' , [CoController::class , 'show'])->name('show.pos.job');
                    Route::get('store' , [CoController::class , 'store'])->name('store.pos.job');
                    Route::get('edit/{id}' , [CoController::class , 'edit'])->name('edit.pos.job');
                    Route::post('update' , [CoController::class , 'update'])->name('update.pos.job');
                    Route::post('delete' , [CoController::class , 'delete'])->name('delete.pos.job');
                    Route::post('active' , [CoController::class , 'active'])->name('active.pos.job');

                    Route::prefix('package')->group( function (){
                        Route::get('/' , [PackageController::class , 'company_index'])->name('index.package.company');
                        Route::post('config' , [PackageController::class , 'pay_config'])->name('company.package.config');
                        Route::get('verify/{paymentId}' , [PackageController::class , 'verify'])->name('company.package.return-url');
                        Route::get('receipt/{paymentId}' , [PackageController::class , 'receipt'])->name('company.package.receipt');
                    });

                });

                Route::middleware('isEmployee')->prefix('emp')->group( function () {
                    
                    Route::prefix('index')->group( function () {
                        Route::get('hire' , [EmpController::class , 'hireIndex'])->name('emp.hire.pos.job');
                        Route::get('intern' , [EmpController::class , 'internIndex'])->name('emp.intern.pos.job');
                    });

                    Route::prefix('intern')->group( function () {
                        Route::get('un' , [EmpController::class , 'unConfirmedIntern'])->name('emp.un.intern.pos.job');
                        Route::get('un-export' , [ExportController::class , 'unInternPosition'])->name('export.emp.un.intern.pos.job');

                        Route::get('ac' , [EmpController::class , 'acceptedIntern'])->name('emp.ac.intern.pos.job');
                        Route::get('ac-export' , [ExportController::class , 'acceptedInternPosition'])->name('export.emp.ac.intern.pos.job');

                        Route::get('re' , [EmpController::class , 'rejectedIntern'])->name('emp.re.intern.pos.job');
                        Route::get('re-export' , [ExportController::class , 'rejectedInternPosition'])->name('export.emp.re.intern.pos.job');
                    });

                    Route::prefix('hire')->group( function () {
                        Route::get('un' , [EmpController::class , 'unConfirmedHire'])->name('emp.un.hire.pos.job');
                        Route::get('un-export' , [ExportController::class , 'unHirePosition'])->name('export.emp.un.hire.pos.job');

                        Route::get('ac' , [EmpController::class , 'acceptedHire'])->name('emp.ac.hire.pos.job');
                        Route::get('ac-export' , [ExportController::class , 'acceptedHirePosition'])->name('export.emp.ac.hire.pos.job');

                        Route::get('re' , [EmpController::class , 'rejectedHire'])->name('emp.re.hire.pos.job');
                        Route::get('re-export' , [ExportController::class , 'rejectedHirePosition'])->name('export.emp.re.hire.pos.job');
                    });

                    Route::post('accept' , [EmpController::class , 'accept'])->name('accept.pos.job');
                    Route::post('reject' , [EmpController::class , 'reject'])->name('reject.pos.job');
                    Route::get('search'  , [EmpController::class , 'search'])->name('search.pos.job');

                });
            });

            Route::prefix('request')->group( function () {

                Route::middleware('AcceptedUser')->group( function () {

                    Route::middleware('isCompany')->group( function () {
                        Route::prefix('hire')->group( function () {
                            Route::get('new' , [RequestCoController::class , 'newHireByPos'])->name('co.new.hire.req.job');

                            Route::get('rec' , [RequestCoController::class , 'listOfHireReceivedByPos'])->name('co.received.hire.req.job');
                            Route::get('received/{id}' , [RequestCoController::class , 'coReceivedHireById'])->name('co.received-by-id.hire.req.job');

                            Route::get('se' , [RequestCoController::class , 'listOfHireSentByPos'])->name('co.sent.hire.req.job');
                            Route::get('sent/{id}' , [RequestCoController::class , 'coSentHireById'])->name('co.sent-by-id.hire.req.job');

                            Route::get('accepted' , [RequestCoController::class , 'coAcceptedHire'])->name('co.accepted.hire.req.job');
                        });

                        Route::prefix('intern')->group( function () {
                            Route::get('new' , [RequestCoController::class , 'newInternByPos'])->name('co.new.intern.req.job');

                            Route::get('rec' , [RequestCoController::class , 'listOfInternReceivedByPos'])->name('co.received.intern.req.job');
                            Route::get('received/{id}' , [RequestCoController::class , 'coReceivedIntetnById'])->name('co.received-by-id.intern.req.job');

                            Route::get('se' , [RequestCoController::class , 'listOfInternSentByPos'])->name('co.sent.intern.req.job');
                            Route::get('sent/{id}' , [RequestCoController::class , 'coSentInternById'])->name('co.sent-by-id.intern.req.job');

                            Route::get('accepted' , [RequestCoController::class , 'coAcceptedIntern'] )->name('co.accepted.intern.req.job');

                        });

                        Route::post('accept' , [RequestCoController::class , 'accept'])->name('co.accept.req.job');
                        Route::post('reject' , [RequestCoController::class , 'reject'])->name('co.reject.req.job');
                        Route::post('cancel' , [RequestCoController::class , 'cancel'])->name('co.cancel.req.job');
                    });

                    Route::middleware('isStudent')->prefix('std')->group( function () {
                        Route::prefix('hire')->group( function () {
                            Route::get('sent' , [StdController::class , 'listOfHireSent'])->name('std.sent.hire.req.job');
                            Route::get('received' , [StdController::class , 'listOfHireReceived'])->name('std.received.hire.req.job');
                        });
                        Route::prefix('intern')->group( function () {
                            Route::get('sent' , [StdController::class , 'listOfInternSent'])->name('std.sent.intern.req.job');
                            Route::get('received' , [StdController::class , 'listOfInternReceived'])->name('std.received.intern.req.job');
                        });
                        
                        Route::post('accept' , [StdController::class , 'accept'])->name('std.accept.req.job');
                        Route::post('reject' , [StdController::class , 'reject'])->name('std.reject.req.job');
                        Route::post('cancle' , [StdController::class , 'cancle'])->name('std.cancle.req.job');
                        
                    });

                });

                Route::middleware('isEmployee')->prefix('emp')->group( function () {

                    Route::prefix('intern')->group( function (){
                        
                        Route::get('/' , [RequestEmpController::class , 'index'])->name('emp.intern.req.job');

                        Route::prefix('std')->group( function () {
                            Route::get('requests' , [RequestEmpController::class , 'stdRequests'])->name('std-requests.emp.list.request.intern');
                            Route::get('req-export' , [ExportController::class , 'stdReqList'])->name('export.std-requests.emp.list.request.intern');

                            Route::get('accepted' , [RequestEmpController::class , 'stdAccepted'])->name('std-accepted.emp.list.request.intern');
                            Route::get('export-accepted' , [ExportController::class , 'stdAcceptedReqList'])->name('export.std-accepted.emp.list.request.intern');

                            Route::get('canceled' , [RequestEmpController::class , 'stdCanceled'])->name('std-canceled.emp.list.request.intern');
                            Route::get('export-canceled' , [ExportController::class , 'stdCanceledReqList'])->name('export.std-canceled.emp.list.request.intern');

                            Route::get('rejected' , [RequestEmpController::class , 'stdRejected'])->name('std-rejected.emp.list.request.intern');
                            Route::get('export-rejected' , [ExportController::class , 'stdRejectedReqList'])->name('export.std-rejected.emp.list.request.intern');
                        });

                        Route::prefix('co')->group( function () {
                            Route::get('requests' , [RequestEmpController::class , 'coRequests'])->name('co-requests.emp.list.request.intern');
                            Route::get('req-export' , [ExportController::class , 'coReqList'])->name('export.co-requests.emp.list.request.intern');

                            Route::get('accepted' , [RequestEmpController::class , 'coAccepted'])->name('co-accepted.emp.list.request.intern');
                            Route::get('export-accepted' , [ExportController::class , 'coAcceptedReqList'])->name('export.co-accepted.emp.list.request.intern');

                            Route::get('canceled' , [RequestEmpController::class , 'coCanceled'])->name('co-canceled.emp.list.request.intern');
                            Route::get('export-canceled' , [ExportController::class , 'coCanceledReqList'])->name('export.co-canceled.emp.list.request.intern');

                            Route::get('rejected' , [RequestEmpController::class , 'coRejected'])->name('co-rejected.emp.list.request.intern');
                            Route::get('export-rejected' , [ExportController::class , 'coRejectedReqList'])->name('export.co-rejected.emp.list.request.intern');
                        });

                        Route::prefix('uni')->group( function () {
                            Route::get('accepted' , [RequestEmpController::class , 'uniAccepted'])->name('uni-accepted.emp.list.request.intern');
                            Route::get('export-accepted' , [ExportController::class , 'uniAcceptedReqList'])->name('export.uni-accepted.emp.list.request.intern');

                            Route::get('rejected' , [RequestEmpController::class , 'uniRejected'])->name('uni-rejected.emp.list.request.intern');
                            Route::get('export-rejected' , [ExportController::class , 'uniRejectedReqList'])->name('export.uni-rejected.emp.list.request.intern');

                            Route::get('printed' , [RequestEmpController::class , 'uniPrinted'])->name('uni-printed.emp.list.request.intern');
                            Route::get('export-printed' , [ExportController::class , 'uniPrintedReqList'])->name('export.uni-printed.emp.list.request.intern');
                        });

                        Route::get('done' , [RequestEmpController::class , 'done'])->name('done.list.request.intern');
                        Route::get('export-done' , [ExportController::class , 'doneReqList'])->name('export.done.list.request.intern');

                        Route::get('user-search' , [InternRequestOptionController::class , 'userSearch'])->name('search.user.option.request.intern');
                        Route::get('search' , [InternRequestOptionController::class , 'search'])->name('search.option.request.intern');
                        Route::get('uni-search' , [InternRequestOptionController::class , 'uniSearch'])->name('uni.search.option.request.intern');
                        Route::get('uni-hire-search' , [InternRequestOptionController::class , 'uniHireSearch'])->name('uni.search.option.request.hire');

                        Route::post('confirm-by-uni' , [PooyeshRequestController::class , 'confirmByUni'])->name('emp.accept.req.job');
                        Route::post('reject-by-uni' , [TicketController::class , 'sendRejectRequestByUni'])->name('emp.reject.req.job');

                    });

                    Route::prefix('hire')->group( function (){
                        Route::get('/' , [RequestEmpController::class , 'hireIndex'])->name('emp.hire.req.job');

                        Route::post('accept' , [RequestEmpController::class , 'acceptHire'])->name('emp.accept.hire.req.job');
                        Route::post('reject' , [RequestEmpController::class , 'rejectHire'])->name('emp.reject.hire.req.job');
                    });
                });

            });
        });

        /** pooyesh Routes */
        Route::prefix('pooyesh')->group( function () {
            Route::prefix('request')->group( function () {
                Route::middleware('isEmployee')->group( function () {
                    Route::get('hire' , [PooyeshRequestController::class , 'hire'])->name('hire.request.pooyesh');
                });
            });
        });

        Route::prefix('cv')->middleware('isStudent')->group( function () {
            Route::prefix('academic-info')->group( function () {
                Route::get('/' , [CvController::class , 'academicInfo'])->name('academic.info.cv');
                Route::post('store' , [CvController::class ,'storeAcademicInfo'])->name('store.academic.info.cv');
                Route::post('delete', [CvController::class , 'deleteAcademicInfo'])->name('delete.AcademicInfo.cv');
            });

            Route::prefix('skills')->group( function () {
                Route::get('/' , [CvController::class , 'skills'])->name('skills.cv');
                Route::post('store' , [CvController::class ,'storeSkills'])->name('store.skills.cv');
                Route::get('delete/{id}', [CvController::class , 'deleteSkill'])->name('delete.skills.cv');
            });
            
            Route::prefix('knowledgs')->group( function () {
                Route::get('/' , [CvController::class , 'knowledgs'])->name('knowledgs.cv');
                Route::post('store' , [CvController::class ,'storeKnowledg'])->name('store.knowledgs.cv');
                Route::get('delete/{id}', [CvController::class , 'deleteKnowledge'])->name('delete.knowledges.cv');
            });

            Route::prefix('abilities')->group( function () {
                Route::get('/' , [CvController::class , 'abilities'])->name('abilities.cv');
                Route::post('store' , [CvController::class ,'storeAbility'])->name('store.abilities.cv');
                Route::get('delete/{id}', [CvController::class , 'deleteAbilitiy'])->name('delete.abilities.cv');
            });

            Route::prefix('technology-skills')->group( function () {
                Route::get('/' , [CvController::class , 'technologySkills'])->name('technology.skills.cv');
                Route::post('get-examples', [CvController::class , 'getExmapeles'])->name('get.examples.cv');
                Route::post('store' , [CvController::class ,'storeTechnologySkills'])->name('store.technology.skills.cv');
                Route::get('delete/{id}' , [CvController::class , 'deleteTechnologySkill'])->name('delete.technology.skills.cv');
            });

            Route::prefix('work-styles')->group( function () {
                Route::get('/' , [CvController::class , 'workStyles'])->name('work.styles.cv');
                Route::post('store' , [CvController::class ,'storeWorkStyles'])->name('store.work.styles.cv');
                Route::get('delete/{id}' , [CvController::class , 'deleteWorkStyles'])->name('delete.work.styles.cv');
            });

            Route::prefix('lang')->group( function () {
                Route::get('/' , [CvController::class , 'lang'])->name('lang.cv');
                Route::post('store' , [CvController::class ,'storeLang'])->name('store.lang.cv');
                Route::get('delete/{id}' , [CvController::class , 'deleteLang'])->name('delete.lang.cv');
            });

            Route::prefix('work-experience')->group( function () {
                Route::get('/' , [CvController::class , 'workExperiences'])->name('work.experiences.cv');
                Route::post('store' , [CvController::class ,'storeWorkExperiences'])->name('store.work.experiences.cv');
                Route::post('delete', [CvController::class , 'deleteWorkExperiences'])->name('delete.work.experiences.cv');
            });

            Route::prefix('socials')->group( function () {
                Route::get('/' , [CvController::class , 'socials'])->name('socials.cv');
                Route::post('store' , [CvController::class ,'storeSocials'])->name('store.socials.cv');
                Route::post('delete', [CvController::class , 'deleteSocials'])->name('delete.socials.cv');
            });

            Route::prefix('certificates')->group( function () {
                Route::get('/' , [CvController::class , 'certificates'])->name('certificates.cv');
                Route::post('store' , [CvController::class ,'storeCertificates'])->name('store.certificates.cv');
                Route::post('delete', [CvController::class , 'deleteCertificates'])->name('delete.certificates.cv');
            });
            
        });

        /** resume Routes */
        Route::prefix('resume')->group( function () {

            Route::middleware('isEmployee')->group( function () {
                Route::get('index' , [ResumeController::class , 'index'])->name('index.resume');
            });
        });

        Route::prefix('ticket')->group( function () {
            Route::get('inbox' , [TicketController::class , 'inbox'])->name('inbox.ticket');
            Route::get('index/{id}' , [TicketController::class , 'index'])->name('index.ticket');

            Route::post('send-reply' , [TicketController::class , 'sendRelpy'])->name('send.reply.ticket');

            Route::middleware('isEmployee')->group( function () {
                Route::prefix('rejected')->group( function () {

                    Route::get('user' , [TicketController::class , 'reg1Sent'])->name('user.rejected.sent.ticket');

                    Route::get('co-user' , [TicketController::class , 'reg2Sent'])->name('co-user.rejected.sent.ticket');

                    Route::get('i-pos' , [TicketController::class , 'iPosSent'])->name('i-pos.rejected.sent.ticket');
                    Route::get('h-pos' , [TicketController::class , 'hPosSent'])->name('h-pos.rejected.sent.ticket');

                    Route::get('i-req' , [TicketController::class , 'iReqSent'])->name('i-req.rejected.sent.ticket');
                    Route::get('h-req' , [TicketController::class , 'hReqSent'])->name('h-req.rejected.sent.ticket');

                });

            });

            Route::prefix('sup')->group( function () {
                Route::get('compose' , [TicketController::class , 'compose'])->name('compose.sup.ticket');
                Route::post('send' , [TicketController::class , 'sendSup'])->name('send.sup.ticket');

                Route::get('sent' , [TicketController::class , 'sentSup'])->name('sent.sup.ticket');

            });
        });


        Route::prefix('mag')->middleware('isEmployee','can:manageArticles')->group( function () {
            Route::get('/' , [ArticleController::class , 'index_admin'])->name('index.article.blog');
            Route::get('create' , [ArticleController::class , 'create_article'])->name('create.article.blog');
            Route::post('store' , [ArticleController::class , 'store'])->name('store.article.blog');
            Route::get('edit/{id}' , [ArticleController::class , 'edit'])->name('edit.article.blog');
            Route::post('update' , [ArticleController::class , 'update'])->name('update.article.blog');
            Route::get('delete/{id}' , [ArticleController::class , 'delete'])->name('delete.article.blog');
            Route::post('rank' , [ArticleController::class , 'Raising_rank_article'])->name('rank.article.blog');
            Route::post('upload' , [ArticleController::class , 'upload'])->name('upload.article.blog');
            // filepond

            Route::post('restore' , [ArticleController::class , 'storeImage'])->name('storeImage.article.blog');
            Route::delete('revert' , [ArticleController::class , 'revert'])->name('revert.article.blog');
            // category
            Route::get('category/index' , [ArticleController::class , 'category_index'])->name('all.category.mag');
            Route::post('category/store' , [ArticleController::class , 'category_store'])->name('store.category.mag');
            Route::post('category/update' , [ArticleController::class , 'category_update'])->name('update.category.mag');
            Route::post('category/delete' , [ArticleController::class , 'categoryـdelete'])->name('delete.category.mag');
            Route::post('category/rank' , [ArticleController::class , 'Raising_rank_category'])->name('rank.category.mag');

            Route::get('comments',[CommentController::class, 'index'])->name('admin.comments');
            Route::post('comment/reject',[CommentController::class, 'reject'])->name('reject.comment');
            Route::post('comment/confirm',[CommentController::class, 'confirm'])->name('confirm.comment');
        });


        /** user management Routes */
        Route::prefix('user')->middleware('isEmployee')->group( function () {
            Route::get('index' , [UserManagementController::class , 'index'])->name('index.user');
            Route::get('all' , [UserManagementController::class , 'all'])->name('all.user');

            Route::get('setting' , [SettingController::class , 'edit'])->name('edit.setting.user');
            Route::post('setting' , [SettingController::class , 'update'])->name('update.setting.user');

            Route::prefix('package')->group( function(){
                Route::get('/' , [PackageController::class , 'index'])->name('index.package.user');
                Route::get('create' , [PackageController::class , 'create'])->name('create.package.user');
                Route::post('store' , [PackageController::class , 'store'])->name('store.package.user');
                Route::get('edit/{id}' , [PackageController::class , 'edit'])->name('edit.package.user');
                Route::post('update' , [PackageController::class , 'update'])->name('update.package.user');
                Route::post('delete' , [PackageController::class , 'delete'])->name('delete.package.user');
            });

            /** unConfirmed users lists */
            Route::prefix('un')->group( function () {
                Route::get('company' , [UserManagementController::class , 'unCompany'])->name('company.un.user');
                Route::get('co-export' , [ExportController::class , 'unCompany'])->name('export.company.un.user');

                Route::get('student' , [UserManagementController::class , 'unStudent'])->name('student.un.user');
                Route::get('std-export' , [ExportController::class , 'unStudent'])->name('export.student.un.user');

                Route::get('applicant' , [UserManagementController::class , 'unApplicant'])->name('applicant.un.user');
                Route::get('app-export' , [ExportController::class , 'unApplicant'])->name('export.applicant.un.user');

            });
            /** accepted users lists */
            Route::prefix('accepted')->group( function () {
                Route::get('company' , [UserManagementController::class , 'acceptedCompany'])->name('company.accepted.user');
                Route::get('co-export' , [ExportController::class , 'acceptedCompany'])->name('export.company.accepted.user');

                Route::get('student' , [UserManagementController::class , 'acceptedStudent'])->name('student.accepted.user');
                Route::get('std-export' , [ExportController::class , 'acceptedStudent'])->name('export.student.accepted.user');

                Route::get('applicant' , [UserManagementController::class , 'acceptedApplicant'])->name('applicant.accepted.user');
                Route::get('app-export' , [ExportController::class , 'acceptedApplicant'])->name('export.applicant.accepted.user');
            });
            /** rejected users lists */
            Route::prefix('rejected')->group( function () {
                Route::get('company' , [UserManagementController::class , 'rejectedCompany'])->name('company.rejected.user');
                Route::get('co-export' , [ExportController::class , 'rejectedCompany'])->name('export.company.rejected.user');

                Route::get('student' , [UserManagementController::class , 'rejectedStudent'])->name('student.rejected.user');
                Route::get('std-export' , [ExportController::class , 'rejectedStudent'])->name('export.student.rejected.user');

                Route::get('applicant' , [UserManagementController::class , 'rejectedApplicant'])->name('applicant.rejected.user');
                Route::get('app-export' , [ExportController::class , 'rejectedApplicant'])->name('export.applicant.rejected.user');
            });
            /**  */
            Route::post('upload-co-newsletter' , [UserManagementController::class , 'uploadCoNewsletter'])->name('co.newsletter.upload.user');

            /** confirm and reject for normal verification */
            Route::post('confirm' , [UserManagementController::class , 'confirmUser'])->name('confirm.user');
            Route::post('reject/candidate' , [TicketController::class , 'sendRejectCandidate'])->name('candidate.rejecte.user');

            /** confirm and reject for company verification */
            Route::post('confirm-co-info' , [UserManagementController::class , 'confirmCoInfo'])->name('co.info.confirm.user');
            Route::post('reject-co-info' , [TicketController::class , 'sendRejectCompanyInfo'])->name('co.info.reject.user');
            Route::post('reject/co' , [TicketController::class , 'sendRejectCompanyAfterAccept'])->name('co.reject.user');

            Route::get('create-user', [UserManagementController::class,'createUser'])->name('admin.create.user');
            Route::post('create-new-user', [UserManagementController::class,'storeUser'])->name('admin.create.new.user');

            Route::prefix('edit-role')->group( function () {
                Route::post('company' , [UserManagementController::class , 'editCompanyRole'])->name('edit.company.role.user');
                Route::post('applicant' , [UserManagementController::class , 'editApplicantRole'])->name('edit.applicant.role.user');
                Route::post('student' , [UserManagementController::class , 'editStudentRole'])->name('edit.student.role.user');
            });

            Route::prefix('search')->group( function () {

                Route::get('company' , [UserManagementController::class , 'companySearch'])->name('company.search.user');
                Route::get('student' , [UserManagementController::class , 'studentSearch'])->name('student.search.user');
                Route::get('applicant' , [UserManagementController::class , 'applicantSearch'])->name('applicant.search.user');
                Route::get('all' , [UserManagementController::class , 'allSearch'])->name('search.all.user');

            });

        });

        Route::prefix('secretariat')->middleware('can:secretariat')->group(function(){
            Route::get('not-printed' , [PooyeshController::class , 'showNotPrintedContracts'])->name('show.notprinted.secretariat');
            Route::get('printed' , [PooyeshController::class , 'showPrintedContracts'])->name('show.printed.secretariat');


            Route::get('contract/{id}' , [PooyeshRequestController::class , 'showContract'])->name('show.intern.contract');

            Route::post('print' , [PooyeshRequestController::class , 'printContract'])->name('print.secertariat');
        });

        Route::middleware('can:amozeshyarExpert')->prefix('connection-with-amozeshyar')->group( function () {
            Route::get('wait-for-review' , [PooyeshController::class , 'waitForReviewAmozeshyarExpert'])->name('wait.for.review.amozeshyar.expert');
            Route::post('register-course-unit' , [PooyeshController::class , 'registerCourseUnit'])->name('register.course.unit.amozeshyar.expert');
            Route::get('course-unit-registered' , [PooyeshController::class , 'courseUnitRegisteredList'])->name('course.unit.registered.amozeshyar.expert');
            Route::post('score-registration' , [PooyeshController::class , 'scoreRegistration'])->name('score.registration.amozeshyar.expert');
            
        });

        /** protection Routs in User management controller */
        Route::prefix('protection')->middleware('isEmployee'  , 'can:protection')->group( function () {
            Route::get('list/co' , [ProtectionController::class , 'coList'])->name('co.list.protection');
            Route::post('reject/co' , [TicketController::class , 'sendRejectCompany'])->name('co.reject.protection');

            Route::get('list/rejected' , [ProtectionController::class , 'rejectedCoList'])->name('list.rejected.protection');
        });

        Route::prefix('sadra')->group( function () {

            Route::middleware('isEmployee')->group( function () {

                Route::get('index' , [SadraController::class , 'index'])->name('index.sadra');
                Route::get('create' , [SadraController::class , 'create'])->name('create.sadra');
                Route::post('store' , [SadraController::class , 'store'])->name('store.sadra');
                Route::get('edit/{id}' , [SadraController::class , 'edit'])->name('edit.sadra');
                Route::post('update' , [SadraController::class , 'update'])->name('update.sadra');
                Route::post('delete' , [SadraController::class , 'delete'])->name('delete.sadra');

                Route::post('get-sadara-plans' , [SadraController::class , 'getSadraPlans'])->name('get.sadra.plans');

            });

            Route::middleware('isCompany' , 'AcceptedUser')->group( function () {

                Route::get('list' , [SadraController::class , 'list'])->name('list.sadra');

                Route::post('check-company-internships' , [SadraController::class , 'checkCompanyHas3InternShip'])->name('check.company.sadra');

                Route::get('show-register/{event_id}' , [SadraController::class , 'registerShow'])->name('show.register.sadra');
                Route::post('register' , [SadraController::class , 'register'])->name('register.sadra');

                Route::get('registred' , [SadraController::class , 'companyRegistred'])->name('registred.company.sadra');


                Route::post('reject-company-sadra' , [SadraController::class , 'rejectCompanySadra'])->name('co.reject.company.sadra');

            });

        });

        Route::prefix('event')->group( function () {

            Route::middleware('isEmployee')->group( function () {

                Route::get('/' , [EventController::class , 'index'])->name('index.event');
                Route::get('create' , [EventController::class , 'create'])->name('create.event');
                Route::post('store' , [EventController::class , 'store'])->name('store.event');
                Route::get('edit/{id}' , [EventController::class , 'edit'])->name('edit.event');
                Route::post('update' , [EventController::class , 'update'])->name('update.event');
                Route::post('delete' , [EventController::class , 'delete'])->name('delete.event');

                Route::post('active' , [EventController::class , 'active'])->name('active.event');

                Route::get('tickets{eventId}' , [EventController::class , 'tickets'])->name('tickets.event');

                Route::get('verify-ticket' , [EventController::class , 'verifyPage'])->name('verify.page.event');
                Route::get('verify/{code}' , [EventTicketController::class , 'verifyCodeGet'])->name('verify.code.get.event');
                Route::post('verify' , [EventTicketController::class , 'verifyCodePost'])->name('verify.code.event');

                Route::get('log-in-to-event/{codeId}' , [EventTicketController::class , 'login'])->name('login.event');
                Route::get('void-ticket/{codeId}' , [EventTicketController::class , 'voidCode'])->name('void.code.event');


                Route::prefix('sans')->group( function () {

                    Route::get('index/{id}' , [SansController::class , 'eventSans'])->name('sans.event');
                    Route::get('create/{id}' , [SansController::class , 'create'])->name('create.sans.event');
                    Route::post('store' , [SansController::class , 'store'])->name('store.sans.event');
                    Route::get('edit/{id}' , [SansController::class , 'edit'])->name('edit.sans.event');
                    Route::post('update' , [SansController::class , 'update'])->name('update.sans.event');
                    Route::post('delete' , [SansController::class , 'delete'])->name('delete.sans.event');

                });

                Route::prefix('plan')->group( function () {

                    Route::get('index/{id}' , [PlanController::class , 'eventPlans'])->name('plans.event');
                    Route::get('create/{id}' , [PlanController::class , 'create'])->name('create.plan.event');
                    Route::post('store' , [PlanController::class , 'store'])->name('store.plan.event');
                    Route::get('edit/{id}' , [PlanController::class , 'edit'])->name('edit.plan.event');
                    Route::post('update' , [PlanController::class , 'update'])->name('update.plan.event');
                    Route::post('delete', [PlanController::class , 'delete'])->name('delete.plan.event');

                });

            });

            Route::middleware('AcceptedUser')->group( function () {

                Route::get('list' , [EventTicketController::class , 'list'])->name('list.event');

                Route::get('lets-register/{eventId}' , [EventTicketController::class , 'letsRegister'])->name('lets.register.event');
                Route::post('get-event-info' , [EventTicketController::class , 'getEventInfo'])->name('get.info.event');
                Route::post('get-event-info2' , [EventTicketController::class , 'getEventInfo2'])->name('get.info.event2');
                Route::post('apply-discount' , [EventTicketController::class , 'applyDiscount'])->name('apply.discount.event');

                Route::post('register' , [EventTicketController::class , 'register'])->name('register.event');

                Route::get('payment/{gateway}/callback/{paymentId}' , [EventPaymentController::class , 'verify'])->name('payment.varify');

                Route::get('receipt/{paymentId}', [EventTicketController::class , 'receipt'])->name('receipt.event');

                Route::get('print-ticket/{paymentId}' , [EventTicketController::class , 'printTicket'])->name('print.ticket.event');

                Route::get('registred' , [EventTicketController::class , 'registred'])->name('registred.event');

                Route::post('pay-after-register' , [EventTicketController::class , 'payAfterRegister'])->name('pay.after.register.event');

            });

        });

        Route::prefix('discount')->group( function () {

            Route::middleware('isEmployee')->group( function () {

                Route::get('/', [DiscountController::class , 'index'])->name('index.discount');
                Route::get('create' , [DiscountController::class , 'create'])->name('create.discount');
                Route::post('store' , [DiscountController::class , 'store'])->name('store.discount');
                Route::get('edit/{discount}' , [DiscountController::class , 'edit'])->name('edit.discount');
                Route::post('update' , [DiscountController::class , 'update'])->name('update.discount');
                Route::post('destroy' , [DiscountController::class , 'destroy'])->name('destroy.discount');


            });

        });

        Route::prefix('accounting')->middleware('isEmployee')->group( function () {
            Route::get('mbti' , [AccountingController::class , 'mbtiIndex'])->name('mbti.accounting');
            Route::get('mbti-search' , [AccountingController::class , 'mbtiSearch'])->name('search.mbti.accounting');
            Route::post('mbti-export' , [ExportController::class , 'mbtiTransactionList'])->name('export.mbti.accounting');

            Route::get('package' , [AccountingController::class , 'packageIndex'])->name('package.accounting');
            Route::get('package-search' , [AccountingController::class , 'packageSearch'])->name('search.package.accounting');
            Route::post('package-export' , [ExportController::class , 'packageTransactionList'])->name('export.package.accounting');
        });

        Route::middleware('isStudent')->prefix('std')->group(function () {
            Route::get('/profile/edit', [\App\Http\Controllers\StudentController::class, 'edit_profile'])->name('std.profile.edit');
            Route::put('/profile/update', [\App\Http\Controllers\StudentController::class, 'update_profile'])->name('std.profile.update');

            Route::middleware('AcceptedUser')->group( function () {

                Route::get('/companies-list', [\App\Http\Controllers\StudentController::class, 'companies_list'])->name('std.companies-list');
                Route::get('/sent-requests', [\App\Http\Controllers\StudentController::class, 'sent_coo_request'])->name('std.coo-requests-list');
                Route::get('/sent-hire-requests', [\App\Http\Controllers\StudentController::class, 'sent_hire_request'])->name('std.hire-requests-list.sent');
                Route::get('/received-hire-requests', [\App\Http\Controllers\StudentController::class, 'received_hire_requests'])->name('std.hire-requests-list.received');
                Route::get('/pooyesh-intern-requests', [\App\Http\Controllers\StudentController::class, 'received_intern_requests'])->name('std.pooyesh-requests-list');
                //Route::view('/ideas-list','panel.student.ideas-list')->name('std.ideas-list');
                Route::get('/info', [\App\Http\Controllers\StudentController::class, 'info'])->name('std.info');

                Route::view('/std-create-idea', 'panel.student.create-idea')->name('std.idea.create');
                Route::view('/std-tutorial', 'panel.student.tutorial-list')->name('std.tutorial.list');
                Route::view('/std-ticket', 'panel.student.send-ticket')->name('std.ticket.new');



                Route::get('/std-ticket-list', [\App\Http\Controllers\StudentController::class, 'tickets'])->name('students.tickets');
                Route::post('/std-ticket-store', [\App\Http\Controllers\StudentController::class, 'store_ticket'])->name('students.ticket.store');
                Route::post('/std-ticket-reply' , [StudentController::class , 'sendTikcetReply'])->name('students.ticket.reply');

                Route::get('/student-projects-list', [\App\Http\Controllers\StudentController::class, 'projects'])->name('std.projects.list');
                Route::get('/student-projects-create', [\App\Http\Controllers\StudentController::class, 'create_project'])->name('std.projects.create');
                Route::get('/std-projects-cat-create', [\App\Http\Controllers\StudentController::class, 'create_project_cat'])->name('std.project-cat.create');
                Route::post('/std-projects-cat-store', [\App\Http\Controllers\StudentController::class, 'store_project_cat'])->name('std.project-cat.store');
                Route::post('/std-projects-store', [\App\Http\Controllers\StudentController::class, 'store_project'])->name('std.projects.store');

                Route::get('/std-ideas', [\App\Http\Controllers\StdIdeaController::class, 'index'])->name('std.idea.index');
                Route::get('/std-create-idea', [\App\Http\Controllers\StdIdeaController::class, 'create'])->name('std.idea.create');
                Route::post('/std-store-idea', [\App\Http\Controllers\StdIdeaController::class, 'store'])->name('std.idea.store');
                Route::get('/std-edit-idea/{id}', [\App\Http\Controllers\StdIdeaController::class, 'edit'])->name('std.idea.edit');
                Route::post('/std-edit-idea/{idea}', [\App\Http\Controllers\StdIdeaController::class, 'update'])->name('std.idea.update');

                Route::get('/tickets-to-company', [\App\Http\Controllers\StudentController::class, 'sent_tickets_to_company'])->name('std.company_ticket.index');

                Route::get('std-signature/create', [\App\Http\Controllers\StudentController::class, 'show_signature_upload_form'])->name('std.signature.create');
                Route::post('std-signature/store', [\App\Http\Controllers\StudentController::class, 'upload_signature'])->name('std.signature.store');

            });
            Route::get('/create-cv', [\App\Http\Controllers\StudentController::class, 'create_cv'])->name('std.cv.create');

            Route::post('/std-ajax', [\App\Http\Controllers\StudentController::class, 'ajax'])->name('students.ajax');
            Route::post('/std-search-ajax', [\App\Http\Controllers\StudentController::class, 'search_ajax'])->name('students.ajax.search');

            Route::get('/', [\App\Http\Controllers\StudentController::class, 'dashboard'])->name('std.panel');



            Route::post('std-academicinfo', [AcademicInfoController::class , 'store'])->name('std.academicinfo.store');

            Route::post('std-upload-certificate', [UploadController::class , 'uploadUserCertificate'])->name('std.certificate.upload');

        });

        Route::middleware('isCompany')->prefix('company')->group(function () {

            /** */

            Route::get('/profile/edit', [\App\Http\Controllers\CompanyController::class, 'edit_profile'])->name('company.profile.edit');
            Route::put('/profile/update', [\App\Http\Controllers\CompanyController::class, 'update_profile'])->name('company.profile.update');


            // Route::get('std-intro-letter',[\App\Http\Controllers\CompanyController::class,'std_letter'])->name('company.std-letter');
            Route::get('dl-std-intro-letter/{id}', [\App\Http\Controllers\CompanyController::class, 'get_std_letter'])->name('company.get-std-letter');


            Route::post('/company-ajax', [\App\Http\Controllers\CompanyController::class, 'ajax'])->name('company.ajax');
            Route::post('/company-search-ajax', [\App\Http\Controllers\CompanyController::class, 'search_ajax'])->name('company.ajax.search');
            Route::get('/', [\App\Http\Controllers\CompanyController::class, 'dashboard'])->name('company.panel');

            Route::middleware('AcceptedUser')->group( function () {

                Route::get('projects-list', [\App\Http\Controllers\CompanyController::class, 'projects'])->name('company.projects.list');
                Route::get('create-project', [\App\Http\Controllers\CompanyController::class, 'create_project'])->name('company.projects.create');
                Route::get('create-project-cat', [\App\Http\Controllers\CompanyController::class, 'create_project_cat'])->name('company.project-cats.create');
                Route::post('store-project', [\App\Http\Controllers\CompanyController::class, 'store_project'])->name('company.projects.store');
                Route::post('store-project-cat', [\App\Http\Controllers\CompanyController::class, 'store_project_cat'])->name('company.project-cats.store');

                Route::get('samples', function () {
                    return view('panel.company.samples');
                })->name('company.samples');

                Route::get('/coo-requests', [\App\Http\Controllers\CompanyController::class, 'cooperate_requests'])->name('company.cooperate-requests');
                Route::get('applicants-list', [\App\Http\Controllers\CompanyController::class, 'applicants_list'])->name('company.applicants.list');
                Route::get('students-list', [\App\Http\Controllers\CompanyController::class, 'students_list'])->name('company.students.list');
                Route::get('/sent-intern-requests', [\App\Http\Controllers\CompanyController::class, 'sent_intern_requests'])->name('company.sent.intern_requests');
                Route::get('/sent-hire-requests', [\App\Http\Controllers\CompanyController::class, 'sent_hire_requests'])->name('company.sent.hire_requests');

                Route::get('/portfolio', [\App\Http\Controllers\CompanyPortfolioController::class, 'index'])->name('company.portfolio.index');
                Route::get('/create-portfolio', [\App\Http\Controllers\CompanyPortfolioController::class, 'create'])->name('company.portfolio.create');
                Route::post('/store-portfolio', [\App\Http\Controllers\CompanyPortfolioController::class, 'store'])->name('company.portfolio.store');

                Route::get('/company-ideas', [\App\Http\Controllers\CompanyIdeaController::class, 'index'])->name('company.idea.index');
                Route::get('/company-create-idea', [\App\Http\Controllers\CompanyIdeaController::class, 'create'])->name('company.idea.create');
                Route::post('/company-store-idea', [\App\Http\Controllers\CompanyIdeaController::class, 'store'])->name('company.idea.store');
                Route::get('/company-edit-idea/{id}', [\App\Http\Controllers\CompanyIdeaController::class, 'edit'])->name('company.idea.edit');
                Route::post('/company-edit-idea/{idea}', [\App\Http\Controllers\CompanyIdeaController::class, 'update'])->name('company.idea.update');

                Route::get('company-products', [\App\Http\Controllers\CompanyProductController::class, 'index'])->name('company.products.index');
                Route::get('company-products-create', [\App\Http\Controllers\CompanyProductController::class, 'create'])->name('company.products.create');
                Route::post('company-products-store', [\App\Http\Controllers\CompanyProductController::class, 'store'])->name('company.products.store');
                Route::get('/company-products-edit/{product}', [\App\Http\Controllers\CompanyProductController::class, 'edit'])->name('company.products.edit');
                Route::post('/company-products-edit/{product}', [\App\Http\Controllers\CompanyProductController::class, 'update'])->name('company.products.update');

                Route::get('std-resume-list/show' , [CompanyController::class , 'studentResumeListShow'])->name('company.std-resume-list.show');



                Route::get('/sadra', [\App\Http\Controllers\CompanyController::class, 'sadra_events'])->name('company.sadra');
                Route::post('/sadra/reg-show', [CompanyController::class, 'sadraRegisterShow'])->name('company.sadra.register.show');
                Route::get('/sadra/my-requests', [\App\Http\Controllers\CompanyController::class, 'sadra_register_requests'])->name('company.sadra.sent-requests');
                Route::post('/sadra/reg', [\App\Http\Controllers\CompanyController::class, 'sadra_register'])->name('company.sadra.register');

                Route::get('/sadra/prepay/{id}', [PaymentController::class , 'sadraPrepay'])->name('company.sadra.prepay');
                Route::get('/sadra/return-payment-show{id}', [PaymentController::class, 'sadraReturnPaymentShow'])->name('company.sadra.return-payment-show');
                Route::get('/sadra/payment/result-show{id}', [PaymentController::class , 'sadraPaymentResultShow'])->name('company.sadra.payment.result-show');

                Route::get('/sadra/invitation{id}', [CompanyController::class , 'sadraInvitation'])->name('company.sadra.invitation');

                Route::get('/company-tickets', [\App\Http\Controllers\CompanyController::class, 'tickets'])->name('company.tickets');
                Route::get('/company-tickets/read/{ticket}', [\App\Http\Controllers\CompanyController::class, 'MarkTicketAsRead'])->name('company.tickets.read');
                Route::post('/company-tickets/send-reply', [\App\Http\Controllers\CompanyController::class, 'send_reply'])->name('company.tickets.send_reply');

                Route::get('/signature/create', [\App\Http\Controllers\CompanyController::class, 'show_signature_upload_form'])->name('company.signature.create');
                Route::post('/signature/store', [\App\Http\Controllers\CompanyController::class, 'upload_signature'])->name('company.signature.store');

                Route::post('/sadra/final-reg', [\App\Http\Controllers\CompanyController::class, 'sadra_final_register'])->name('company.sadra.register.final');

                // Route student search for companies
                Route::post('std-search', [SearchController::class, 'studentSearch'])->name('std_search');
                Route::post('apl-search', [SearchController::class, 'applicantSearch'])->name('apl_search');
            });


            Route::get('company-info', [\App\Http\Controllers\CompanyController::class, 'company_info'])->name('company.info');
            Route::get('edit-company-info', [\App\Http\Controllers\CompanyController::class, 'edit_company_info'])->name('company.info.edit');
            Route::post('update-company-info', [\App\Http\Controllers\CompanyController::class, 'update_company_info'])->name('company.info.update');



            Route::prefix('ajax')->group(function(){
                // sadra ajax request
                Route::post('get-booth' , [CompanyAjaxController::class , 'getBoothByPlanId'])->name('company.ajax.sadra.get.booth');
                Route::post('cheak-booth-reserved' , [CompanyAjaxController::class , 'checkBoothReserved'])->name('company.ajax.sadra.cheak.booth.reserved');
                // tickets from students routes are here
                Route::post('ticket/delete-ticket' , [CompanyAjaxController::class , 'deleteTicket'])->name('company.ajax.ticket.delete-ticket');

            });

        });

        Route::middleware('isEmployee')->prefix('emp')->group(function () {
            // import extra user routes



            Route::prefix('import')->group(function () {
                Route::get('extra-user/show', [ExtraUserController::class , 'importExcelExtraUserShow'])->name('employee.import.excel.extra-user.show');
                Route::post('extra-user/store', [ExtraUserController::class , 'importExcelExtraUserStore'])->name('employee.import.excel.extra-user.store');
                Route::get('extra-user/update-ncode-user', [ExtraUserController::class , 'updateNcodeUsers'])->name('employee.import.excel.extra-user.update-ncode-user');
                Route::get('extra-user/update-major-user' , [ExtraUserController::class , 'updateMajorUsers'])->name('employee.import.excel.extra-user.update-major-user');
                Route::get('extra-user/update-grade-user' , [ExtraUserController::class , 'updateGradeUsers'])->name('employee.import.excel.extra-user.update-grade-user');
                Route::get('extra-user/export-unregistered-students' , [ExtraUserController::class , 'exportUnregisteredStudents'])->name('employee.import.excel.extra-user.export-unregistered-students');
            });

            // Route::view('contract','contract-pattern');
            Route::get('/profile/edit', [\App\Http\Controllers\EmployeeController::class, 'edit_profile'])->name('employee.profile.edit');
            Route::put('/profile/update', [\App\Http\Controllers\EmployeeController::class, 'update_profile'])->name('employee.profile.update');

            Route::resource('files', \App\Http\Controllers\FilesController::class);
            Route::middleware('can:support')->get('all-tickets', [\App\Http\Controllers\EmployeeController::class, 'tickets'])->name('employee.tickets.list');
            Route::middleware('can:support')->post('ticket/send-reply' , [EmployeeController::class , 'sendTikcetReply'])->name('employee.ticket.send-reply');
            Route::middleware('can:support')->get('ticket/read/{ticket}' , [EmployeeController::class , 'MarkTicketAsRead'])->name('employee.ticket.read');

            Route::post('/emp-ajax', [\App\Http\Controllers\EmployeeController::class, 'ajax'])->name('employee.ajax');
            Route::post('/emp-search-ajax', [\App\Http\Controllers\EmployeeController::class, 'ajax_search'])->name('employee.ajax.search');
            Route::get('/', [\App\Http\Controllers\EmployeeController::class, 'index'])->name('employee.panel');

            Route::get('/students', [\App\Http\Controllers\EmployeeController::class, 'studentsList'])->name('employee.std.list');
            Route::get('/companies', [\App\Http\Controllers\EmployeeController::class, 'companiesList'])->name('employee.co.list');

            // managment user routes ============================================
            Route::middleware('can:createUser')->get('/create-new-user', [\App\Http\Controllers\EmployeeController::class, 'createUser'])->name('employee.create.user');
            Route::middleware('can:unconfirmedUser')->get('/unconfirmed-list', [\App\Http\Controllers\EmployeeController::class, 'unconfirmedList'])->name('employee.unconfirmed-list');
            Route::middleware('can:confirmedUser')->get('/confirmed-list', [\App\Http\Controllers\EmployeeController::class, 'confirmedList'])->name('employee.confirmed-list');
            Route::middleware('can:importedUser')->get('/imported-list', [\App\Http\Controllers\EmployeeController::class, 'importedList'])->name('employee.imported-list');
            Route::middleware('can:testedUser')->get('/testers-list', [\App\Http\Controllers\EmployeeController::class, 'testersList'])->name('employee.testers-list');
            Route::middleware('can:exportAllUser')->get('/export-all-user-show', [EmployeeController::class, 'exportAllUserShow'])->name('employee.export.all.user.show');

            //  =================================================================
            // Route::get('/unconfirmed-companies', [\App\Http\Controllers\EmployeeController::class, 'unconfirmedCompanies'])->name('employee.unconfirmed-companies');
            // Route::get('/unconfirmed-stds', [\App\Http\Controllers\EmployeeController::class, 'unconfirmedStudents'])->name('employee.unconfirmed-stds');

            Route::post('/store-new-user', [\App\Http\Controllers\EmployeeController::class, 'storeUser'])->name('employee.store.user');
            Route::post('/store-new-user-excel', [\App\Http\Controllers\EmployeeController::class, 'storeUserByExcel'])->name('employee.store.user.excel');
            // =================================================================
            Route::prefix('export')->group(function(){
                // all
                Route::get('all-companies' , [EmployeeController::class , 'exportAllCompanies'])->name('employee.export.all.companies');
                Route::get('all-mbti-students' , [EmployeeController::class, 'exportAllMbtiStudents'])->name('employee.export.all.mbti.students');
                Route::get('all-mbti-users' , [EmployeeController::class, 'exportAllMbtiUsers'])->name('employee.export.all.mbti.users');
                // confirmed users
                Route::get('stds', [\App\Http\Controllers\EmployeeController::class, 'exportStds'])->name('employee.std.export-excel');
                Route::get('applicants', [\App\Http\Controllers\EmployeeController::class, 'exportApplicants'])->name('employee.applicant.export-excel');
                Route::get('companies', [\App\Http\Controllers\EmployeeController::class, 'exportCompanies'])->name('employee.company.export-excel');
                // un confirmed users
                Route::get('un-companies', [EmployeeController::class, 'exportUnconfirmedCompanies'])->name('employee.unconfirmed.company.export-excel');
                Route::get('un-applicants', [EmployeeController::class, 'exportUnconfirmedApplicants'])->name('employee.unconfirmed.applicant.export-excel');
                Route::get('un-stds', [EmployeeController::class, 'exportUnconfirmedStds'])->name('employee.unconfirmed.std.export-excel');
                // imported users
                Route::get('imp-companies', [EmployeeController::class, 'exportImportedCompanies'])->name('employee.imported.company.export-excel');
                Route::get('imp-applicants', [EmployeeController::class, 'exportImportedApplicants'])->name('employee.imported.applicant.export-excel');
                Route::get('imp-stds', [EmployeeController::class, 'exportImportedStds'])->name('employee.imported.std.export-excel');
                // tester users
                Route::get('test-mbti', [EmployeeController::class, 'exportMbtiTest'])->name('employee.test.mbti.export-excel');
                Route::get('test-enneagram', [EmployeeController::class, 'exportEnneagramTest'])->name('employee.test.enneagram.export-excel');
                // sadra requests
                Route::get('done-sadra-request/{id}', [EmployeeController::class, 'exportDoneCompanySadra'])->name('employee.done.sadra.export-excel');

            });
            // personality test dashboard routes ===============================
            Route::get('personality-tests/result-show' , [EmployeeController::class , 'personalityTestsResultShow'])->name('employee.personality-tests.result-show');
            Route::post('personality-tests/search' , [SearchController::class , 'personalityTestsSearch'])->name('employee.personality-tests.search');

            Route::middleware('can:pooyeshRequest')->get('/pooyesh-reqs', [EmployeeController::class, 'pooyesh_requests'])->name('employee.pooyesh-reqs');
            Route::middleware('can:hireRequest')->get('/hire-reqs', [EmployeeController::class, 'hire_requests'])->name('employee.hire-reqs');
            Route::get('std-resume-list/show' , [EmployeeController::class , 'studentResumeListShow'])->name('employee.std-resume-list.show');

            // Route::middleware('can:manageEvents')->get('/sadra/list', [EmployeeController::class, 'sadra_index'])->name('employee.sadra.index');
            Route::middleware('can:manageEvents')->get('/sadra/report', [EmployeeController::class, 'sadra_report'])->name('employee.sadra.report');
            Route::middleware('can:manageEvents')->get('/sadra/create', [EmployeeController::class, 'sadra_create_event'])->name('employee.sadra.create');
            // Route::middleware('can:manageEvents')->get('/sadra/edit/{sadra}', [EmployeeController::class, 'sadra_edit'])->name('employee.sadra.edit');
            // Route::post('/sadra/store', [EmployeeController::class, 'sadra_store_event'])->name('employee.sadra.store');
            // Route::post('/sadra/update/{sadra}', [EmployeeController::class, 'sadra_update'])->name('employee.sadra.update');
            // Route::post('/sadra/destroy/', [EmployeeController::class, 'sadra_destroy'])->name('employee.sadra.destroy');

            Route::middleware('can:manageEvents')->get('/sadra/requests/{id}', [EmployeeController::class, 'sadra_requests'])->name('employee.sadra.requests');
            Route::middleware('can:manageEvents')->get('/sadra/done-requests/{id}', [EmployeeController::class, 'company_sadra_register_done'])->name('employee.sadra.done-requests');

            Route::middleware('can:manageEvents')->get('/discount-codes', [EmployeeController::class, 'discounts'])->name('employee.discount.index');
            Route::middleware('can:manageEvents')->get('/discount-codes/create', [EmployeeController::class, 'create_discount'])->name('employee.discount.create');
            Route::middleware('can:manageEvents')->get('/discount-codes/edit/{discount}', [EmployeeController::class, 'edit_discount'])->name('employee.discount.edit');
            Route::middleware('can:manageEvents')->post('/discount-codes/store', [EmployeeController::class, 'store_discount'])->name('employee.discount.store');
            Route::middleware('can:manageEvents')->post('/discount-codes/update/{discount}', [EmployeeController::class, 'update_discount'])->name('employee.discount.update');
            Route::middleware('can:manageEvents')->post('/discount-codes/destroy', [EmployeeController::class, 'delete_discount'])->name('employee.discount.destroy');

            Route::get('/buy-plans/list', [EmployeeController::class, 'buy_plans'])->name('employee.buy-plan.index');
            Route::get('/buy-plans/create/{id}', [EmployeeController::class, 'buy_plan_create'])->name('employee.buy-plan.create');
            Route::get('/buy-plans/edit/{plan}', [EmployeeController::class, 'buy_plan_edit'])->name('employee.buy-plan.edit');
            Route::post('/buy-plans/store', [EmployeeController::class, 'buy_plan_store'])->name('employee.buy-plan.store');
            Route::post('/buy-plans/update/{plan}', [EmployeeController::class, 'buy_plan_update'])->name('employee.buy-plan.update');
            Route::delete('/buy-plans/destroy/{plan}', [EmployeeController::class, 'buy_plan_destroy'])->name('employee.buy-plan.destroy');



            Route::middleware('can:manageNotifications')->get('announce/list', [\App\Http\Controllers\AnnouncementsController::class, 'index'])->name('emp.announce.list');
            Route::middleware('can:manageNotifications')->get('announce/create', [\App\Http\Controllers\AnnouncementsController::class, 'create'])->name('emp.announce.create');
            Route::post('announce/store', [\App\Http\Controllers\AnnouncementsController::class, 'store'])->name('emp.announce.store');

            Route::middleware('can:signature')->get('/uni-signature/create', [EmployeeController::class, 'show_signature_upload_form'])->name('emp.signature.create');
            Route::post('/uni-signature/store', [EmployeeController::class, 'upload_signature'])->name('emp.signature.store');

            Route::post('/pooyesh-requests/search', [EmployeeController::class, 'search_pooyeshReqs_by_sender'])->name('emp.pooyesh-requests.search');

            Route::get('/graders', [EmployeeController::class, 'graders_list'])->name('emp.graders.list');

            Route::get('/export-sadra-registered/{event_id}', [EmployeeController::class, 'exportExhibitionRegisteredCompanies'])->name('employee.exhibition.export-excel');
            Route::get('/export-exhibition-register-requests/', [EmployeeController::class, 'exportExhibitionRegisterRequests'])->name('employee.exhibition-register-req.export-excel');

            Route::middleware('can:readLog')->get('/CompanylogActivity', [EmployeeController::class, 'CompanylogActivity'])->name('employee.co-log-activity.list');
            Route::middleware('can:readLog')->get('/StudentlogActivity', [EmployeeController::class, 'StudentlogActivity'])->name('employee.std-log-activity.list');
            Route::middleware('can:readLog')->get('/EmployeelogActivity', [EmployeeController::class, 'EmployeelogActivity'])->name('employee.emp-log-activity.list');

            Route::get('/export-activity-log/{model}', [EmployeeController::class, 'exportActivityLog'])->name('employee.activity-log.export-excel');

            Route::middleware('can:unconfirmedJobs')->get('/unconfirmed-job-positions', [EmployeeController::class, 'unconfirmedJobPositions'])->name('employee.unconfirmed-job-positions');



            // roles and permissions management routes ============================================================
            Route::middleware('can:managePermission')->prefix('role')->group(function(){
                Route::get('create' , [RoleAssignController::class , 'createRoleShow'])->name('employee.role-create');
                Route::post('store' , [RoleAssignController::class , 'storeRole'])->name('employee.role.store');
                Route::get('show-assign' , [RoleAssignController::class , 'assignRoleShow'])->name('employee.show.role.assign');
                Route::post('assign' , [RoleAssignController::class , 'assignRole'])->name('employee.role.assign');
                Route::post('remove' , [RoleAssignController::class , 'removeRole'])->name('employee.role.remove');
                Route::get('edit' , [RoleAssignController::class , 'editRole'])->name('employee.role.edit');
                Route::post('update' , [RoleAssignController::class , 'updateRole'])->name('employee.role.update');
                Route::post('delete' , [RoleAssignController::class , 'deletePermission'])->name('employee.role.delete');
            });


            //new Ajax Request Route for Employee =================================================================
            Route::prefix('ajax')->group(function(){
                // assign users role from employee panel
                Route::post('/assign-to-company', [EmployeeAjaxController::class , 'assignToCompany'])->name('employee.ajax.assign-to-company');
                Route::post('/assign-to-student', [EmployeeAjaxController::class , 'assignToStudent'])->name('employee.ajax.assign-to-student');
                Route::post('/assign-to-applicant', [EmployeeAjaxController::class , 'assignToApplicant'])->name('employee.ajax.assign-to-applicant');
                // unconfirmed Users search Routes
                Route::post('/unconfirmed-company-search', [EmployeeAjaxController::class , 'unConfirmedCompanySearch'])->name('employee.ajax.unconfirmed-company-search');
                Route::post('/unconfirmed-applicant-search', [EmployeeAjaxController::class , 'unConfirmedApplicantSearch'])->name('employee.ajax.unconfirmed-applicant-search');
                Route::post('/unconfirmed-student-search', [EmployeeAjaxController::class , 'unConfirmedStudentSearch'])->name('employee.ajax.unconfirmed-student-search');
                // confirmed Users search Routes
                Route::post('/confirmed-company-search', [EmployeeAjaxController::class , 'confirmedCompanySearch'])->name('employee.ajax.confirmed-company-search');
                Route::post('/confirmed-applicant-search', [EmployeeAjaxController::class , 'confirmedApplicantSearch'])->name('employee.ajax.confirmed-applicant-search');
                Route::post('/confirmed-student-search', [EmployeeAjaxController::class , 'confirmedStudentSearch'])->name('employee.ajax.confirmed-student-search');
                // imported Users search Routes
                Route::post('/imported-company-search', [EmployeeAjaxController::class , 'importedCompanySearch'])->name('employee.ajax.imported-company-search');
                Route::post('/imported-applicant-search', [EmployeeAjaxController::class , 'importedApplicantSearch'])->name('employee.ajax.imported-applicant-search');
                Route::post('/imported-student-search', [EmployeeAjaxController::class , 'importedStudentSearch'])->name('employee.ajax.imported-student-search');
                // tester Users search Routes
                Route::post('/tester-mbti-search', [EmployeeAjaxController::class , 'testerMbtiSearch'])->name('employee.ajax.tester-mbti-search');
                Route::post('/tester-enneagram-search', [EmployeeAjaxController::class , 'testerEnnagramSearch'])->name('employee.ajax.tester-enneagram-search');
                // tickets from users requests
                Route::post('/ticket/delete-tickets' , [EmployeeAjaxController::class , 'deleteTicket'])->name('employee.ajax.ticket.delete-tickets');
            });
        });

    });

});
