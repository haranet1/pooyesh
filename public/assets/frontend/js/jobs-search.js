

function setValueToInput(inputId , docBtn , value)
{
    let input = document.getElementById(inputId);
    let input2 = '';
    if(inputId == 'intern_input'){
        input2 = document.getElementById('intern_mo');
    }else if (inputId == 'hire_input') {
        input2 = document.getElementById('hire_mo');
    }else if (inputId == 'grade_input'){
        input2 = document.getElementById('grade_mo');
    }else if (inputId == 'work_experience_input'){
        input2 = document.getElementById('work_experience_mo');
    }else if (inputId == 'salary_input'){
        input2 = document.getElementById('salary_mo');
    }else if (inputId == 'age_input'){
        input2 = document.getElementById('age_mo');
    }else if (inputId == 'city_input'){
        input2 = document.getElementById('city_mo');
    }

    let Btn = document.getElementById(docBtn);
    Btn.innerHTML = value;
    
    if(input.value != value){
        input.value = value;
        input2.value = value;
        submitFilterForm();
    }

}

function removeValueInInput(element)
{
    let key = element.dataset.key;
    let value = element.dataset.value;

    if(key == 'type_intern'){
        let intern_input = document.getElementById('intern_input');
        intern_input.checked = false;
        
        let intern_mo = document.getElementById('intern_mo');
        intern_mo.checked = false;

        element.remove();

        submitFilterForm();
    }

    if(key =='type_hire'){
        let hire_input = document.getElementById('hire_input');
        hire_input.checked = false;

        let hire_mo = document.getElementById('hire_mo');
        hire_mo.checked = false;

        element.remove();

        submitFilterForm();
    }

    if(key == 'grade'){
        let grade_input = document.getElementById('grade_input');
        grade_input.value = '';

        let grade_mo = document.getElementById('grade_mo');
        grade_mo.value = '';

        element.remove();

        submitFilterForm();
    }

    if(key == 'workExperience'){
        let work_experience_input = document.getElementById('work_experience_input');
        work_experience_input.value = '';

        let work_experience_mo = document.getElementById('work_experience_mo');
        work_experience_mo.value = '';

        element.remove();

        submitFilterForm();
    }

    if(key == 'salary'){
        let salary_input = document.getElementById('salary_input');
        salary_input.value = '';

        let salary_mo = document.getElementById('salary_mo');
        salary_mo.value = '';

        element.remove();

        submitFilterForm();
    }

    if(key == 'age'){
        let age_input = document.getElementById('age_input')
        age_input.value = '';

        let age_mo = document.getElementById('age_mo')
        age_mo.value = '';

        element.remove();

        submitFilterForm();
    }

    if(key == 'city'){
        let city_input = document.getElementById('city_input')
        city_input.value = '';

        let city_mo = document.getElementById('city_mo')
        city_mo.value = '';

        element.remove();

        submitFilterForm();
    }
    
}

function removeAllValueInInputs()
{
    let intern_input = document.getElementById('intern_input');
    intern_input.checked = false;

    let intern_mo = document.getElementById('intern_mo');
    intern_mo.checked = false;

    let hire_input = document.getElementById('hire_input');
    hire_input.checked = false;

    let hire_mo = document.getElementById('hire_mo');
    hire_mo.checked = false;

    let grade_input = document.getElementById('grade_input');
    grade_input.value = '';

    let grade_mo = document.getElementById('grade_mo');
    grade_mo.value = '';

    let work_experience_input = document.getElementById('work_experience_input');
    work_experience_input.value = '';

    let work_experience_mo = document.getElementById('work_experience_mo');
    work_experience_mo.value = '';

    let salary_input = document.getElementById('salary_input');
    salary_input.value = '';

    let salary_mo = document.getElementById('salary_mo');
    salary_mo.value = '';

    let age_input = document.getElementById('age_input')
    age_input.value = '';

    let age_mo = document.getElementById('age_mo')
    age_mo.value = '';

    let city_input = document.getElementById('city_input')
    city_input.value = '';

    let city_mo = document.getElementById('city_mo')
    city_mo.value = '';


    submitFilterForm();
}

function justOneType(element)
{
    let intern_input = document.getElementById('intern_input');
    let intern_mo = document.getElementById('intern_mo');

    let hire_input = document.getElementById('hire_input');
    let hire_mo = document.getElementById('hire_mo');

    if (element.id == 'intern_input') {
        hire_input.checked = false;
        hire_mo.checked = false;
    }else{
        intern_input.checked = false;
        intern_mo.checked = false;
    }

    submitFilterForm();
 
}

function syncPcAndMobile(element)
{

}


function getCities(){ 
    let getCitiesUrl = document.getElementById('getCitiesUrl').value;

    let provinceValue = document.getElementById('province').value;
    if(provinceValue != 0){
        $(document).ready(function(){
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: getCitiesUrl,
                data: {
                    province: provinceValue ,
                },
                dataType: 'json',
                success: function (response) {
                    let ulCity = document.getElementsByClassName('ulCity');

                    ulCity = Array.from(ulCity);

                    ulCity.forEach((city) => {
                        city.innerHTML = '';
                        response.forEach( function (res){
                            name2 = res['name'];
                            city.innerHTML += `<li role="button" onclick="setValueToInput('city_input' , 'cityBtn' , '${name2}')" class="p-2 item-border">`+ res['name'] +`</li>`;
                        })
                    })
                    
                },
                error: function (response) {
                    console.log(response)
                    console.log('error')
                }
            });
        });
    }
}


function submitFilterForm(){
    
    let filter_form = document.getElementById('searchForm');

    filter_form.submit();
}