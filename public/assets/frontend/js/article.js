addEventListener("DOMContentLoaded", (event) => {

    const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
    const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))

    let CSRF_TOKEN = document.getElementById('CSRF').value;
    let hasScrolled = false;
    var additionViewUrl = document.getElementById('additionViewUrl').value;
    window.addEventListener('scroll', () => {
        let article_id = document.getElementById('article');
        const scrollPosition = window.scrollY + window.innerHeight;
        const articleHeight = document.documentElement.scrollHeight;
        
        if(!hasScrolled && scrollPosition >= articleHeight / 2){
            hasScrolled = true;

            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': CSRF
                },
                url: additionViewUrl,
                data: {
                    article_id: article_id.getAttribute("data"),
                },
                error: function (data) {
                    console.log(data.error)
                }
            })
        };
    })


    var like = document.querySelectorAll('.likeButton');
    var likeUrl = document.getElementById('likeUrl').value;


    like.forEach( function (elemnt) {
        elemnt.addEventListener('click', function () {
            const articleId = elemnt.dataset.articleId;
    
            $.ajax(likeUrl, {
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': CSRF_TOKEN
                },
                data: {
                    article_id: articleId,
                }
            })
            .done(function (response) {
                    if(response.liked){
                        elemnt.classList.add('like');
                    } else {
                        elemnt.classList.remove('like');
                    }
                    document.getElementById('likes-count').textContent = response.likes_count;
            })
            .fail(function (response) {
                console.log(response.msg);
            }) 
            
        })
    })


});

function angleDownRotate(item){
    var open = item.firstElementChild.classList.contains('open');
    if(open){
        item.firstElementChild.classList.remove('open');
    } else{
        item.firstElementChild.classList.add('open');
    }
}