const CSRF = document.getElementById('CSRF').value;

$(document).ready(function() {

    if (typeof applicantCantSendInternRequest !== 'undefined' && applicantCantSendInternRequest == true){
        Swal.fire({
            icon : 'warning',
            title : 'خطا !',
            text : msg,
            confirmButtonText : 'باشه',
        })
    }


    let valueDisplays = document.querySelectorAll(".num");
    const totalDuration = 5000; // Total time in milliseconds

    function startCounters() {
        const startTime = performance.now();

        function updateCounters(currentTime) {
            const elapsed = currentTime - startTime;
            const progress = Math.min(elapsed / totalDuration, 1);

            valueDisplays.forEach((valueDisplay) => {    
                const endValue = parseInt(valueDisplay.getAttribute("data-val"));
                const currentValue = Math.floor(progress * endValue);
                valueDisplay.innerText = currentValue;
            });

            if (progress < 1) {
                requestAnimationFrame(updateCounters);
            }
        }

        requestAnimationFrame(updateCounters);
    }

    startCounters();


    $('.dropdown').hover(function () {
        $(this).find('.dropdown-menu')
            .stop(true, true).delay(100).fadeIn(200);
    }, function () {
        $(this).find('.dropdown-menu')
            .stop(true, true).delay(100).fadeOut(200);
    });

    $("#linkdin").hover(
        function() {$(this).attr("src","../../../assets/frontend/img/home/linkdin (Toggle State).png");},
        function() {$(this).attr("src","../../../assets/frontend/img/home/linkdin.png");
    });
    $("#instagram").hover(
        function() {$(this).attr("src","../../../assets/frontend/img/home/instagram (Hover State).png");},
        function() {$(this).attr("src","../../../assets/frontend/img/home/instagram.png");
    });

    // let screen_size = screen.width;
    $(window).resize(function() {
        if ($(window).width() < 700) {
            $('.svg').attr('viewBox', '0 0 1000 100')
        }
    });



    $('.fanumber').persiaNumber();

});



