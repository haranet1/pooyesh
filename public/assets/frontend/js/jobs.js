document.addEventListener('DOMContentLoaded', function () {
    const categorySelect = document.getElementById('category');
    // const jobTitleInput = document.getElementById('jobTitle');

    function submitFilterForm(){
        let category_form = document.getElementById('searchForm');

        category_form.submit();
    }
    categorySelect.addEventListener('change', submitFilterForm);

    let currentPage = 1;
    const itemsPerPage = 30; 
    

    function renderJobs(jobs) {
        const resultsContainer = document.getElementById('searchResults');
        resultsContainer.innerHTML = '';
    
        if (jobs.length === 0) {
            resultsContainer.innerHTML = `<div class="col-12 text-center fs-5 blue fw-semibold">شغلی یافت نشد</div>`;
            return;
        }
    
        jobs.forEach(job => {
            const jobElement = `
                <div class="col-lg-6 col-12">
                    <a href="${job.url}" class="pointer">
                        <div class="box-shadow-c pb-md-0 bg-white box-hover rounded-4 mb-4 border-blue position-relative">
                            <div class="bg-halfc-2 py-md-4 py-3 pb-4 px-md-3 px-2">
                                <div class="d-flex align-items-start">
                                    <div class="mb-3 mb-md-0">
                                        <div class="bg-gr rounded-4 cat-img">
                                            <img src="${job.categoryPhotoPath}" class="w-100 img-fluid" alt="${job.title}" height="47" width="60">
                                        </div>
                                    </div>
                                    <div class="px-2">
                                        <h5 class="h5 fw-bold blue fs-sm-18">${job.title}</h5>
                                        <p class="fs-6 text-grey fs-sm-14 mb-2">${job.description.substring(0, 100)}...</p>
                                    </div>
                                </div>

                            </div>
                            <div class="position-absolute job-btn bg-blue text-white text-center rounded-4">
                                مشاهده
                            </div>
                        </div>

                    </a>
                </div>
            `;
            resultsContainer.innerHTML += jobElement;
        });
    }

    function paginateJobs(jobs, page) {
        const start = (page - 1) * itemsPerPage;
        const end = start + itemsPerPage;
        return jobs.slice(start, end);
    }

    function renderPagination(totalItems) {
        const paginationContainer = document.querySelector('#pagination');
        paginationContainer.innerHTML = '';
    
        const totalPages = Math.ceil(totalItems / itemsPerPage);

        if (totalPages <= 1) {
            return;
        }
    
        function toPersianNumber(num) {
            return num.toString().replace(/\d/g, d => "۰۱۲۳۴۵۶۷۸۹"[d]);
        }
    
        if (currentPage > 1) {
            paginationContainer.innerHTML += `
                <div class="bg-white rounded-circle page-item-shadow page-item">
                    <a href="#" class="page-arrow" aria-label="previous page" data-page="${currentPage - 1}">
                        <i class="fa-solid fa-arrow-right blue fs-5"></i>
                    </a>
                </div>
            `;
        }
    
        let startPage = Math.max(1, currentPage - 1); 
        let endPage = Math.min(totalPages, currentPage + 1);
    
        if (endPage - startPage < 3) {
            if (startPage === 1) {
                endPage = Math.min(totalPages, startPage + 3);
            } else if (endPage === totalPages) {
                startPage = Math.max(1, endPage - 3);
            }
        }
    

        if (startPage > 1) {
            paginationContainer.innerHTML += `
                <a href="#" class="rounded-circle fanumber page-num fs-4 fw-semibold blue page-link" data-page="1">
                    ${toPersianNumber(1)}
                </a>
            `;
            if (startPage > 2) {
                paginationContainer.innerHTML += `
                    <span class="rounded-circle fanumber page-num fs-4 fw-semibold blue">...</span>
                `;
            }
        }
    
        for (let i = startPage; i <= endPage; i++) {
            paginationContainer.innerHTML += `
                <a href="#" class="rounded-circle fanumber page-num fs-4 fw-semibold blue page-link ${i === currentPage ? 'active' : ''}" data-page="${i}">
                    ${toPersianNumber(i)}
                </a>
            `;
        }
    
        if (endPage < totalPages) {
            if (endPage < totalPages - 1) {
                paginationContainer.innerHTML += `
                    <span class="rounded-circle fanumber page-num fs-4 fw-semibold blue">...</span>
                `;
            }
            paginationContainer.innerHTML += `
                <a href="#" class="rounded-circle fanumber page-num fs-4 fw-semibold blue page-link" data-page="${totalPages}">
                    ${toPersianNumber(totalPages)}
                </a>
            `;
        }
    
        if (currentPage < totalPages) {
            paginationContainer.innerHTML += `
                <div class="bg-white rounded-circle page-item-shadow page-item">
                    <a href="#" class="page-arrow" aria-label="next page" data-page="${currentPage + 1}">
                        <i class="fa-solid fa-arrow-left blue fs-5"></i>
                    </a>
                </div>
            `;
        }
    
        paginationContainer.addEventListener('click', function (e) {
            e.preventDefault();
            const target = e.target.closest('[data-page]');
            if (target) {
                const page = parseInt(target.getAttribute('data-page'));
                goToPage(page);
            }
        });
    }
    

    function goToPage(page) {
        currentPage = page;
        updateJobs();
    }
    
    function updateJobs() {
        const searchQuery = document.querySelector('#search-input').value.trim();
        const filteredJobs = allJobs.filter(job => job.title.includes(searchQuery));
    
        const paginatedJobs = paginateJobs(filteredJobs, currentPage);
        renderJobs(paginatedJobs);
        renderPagination(filteredJobs.length);
    }
    

    document.querySelector('#search-input').addEventListener('input', function () {
        currentPage = 1;
        updateJobs();
    });
    
    updateJobs();
    
    
});
