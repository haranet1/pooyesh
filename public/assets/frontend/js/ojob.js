let star = document.getElementById("star");

star.onclick = function() {
    
    
    if (star.className == "active") {
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN' : CSRF
            },
            type: 'POST',
            url: $('#addToFavoriteUrl').val(),
            data: {
                job_id: $('#job_id').val(),
            },
            dataType: "json",
            success: function (response) {
                star.className = "";
            }
        })
        
    } else {
        
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN' : CSRF
            },
            type: 'POST',
            url: $('#addToFavoriteUrl').val(),
            data: {
                job_id: $('#job_id').val(),
            },
            dataType: "json",
            success: function (response) {
                star.className = "active";
            }
        })

    } 

};