$(document).ready(function() {
    $("#carousel-home").owlCarousel({
        slideSpeed : 2000,
        autoplay: true,
        dots: false,
        loop: true,
        nav : false,
        rtl : true,

        pagination : false,

        responsiveClass:true,
        responsive:{
            0:{
                items:3,
            },
            576:{
                items:4,
            },
            1200:{
                items:5,
            }
        }
    })
});