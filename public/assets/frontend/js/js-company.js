$(document).ready(function() {


    $("#carousel-company-1, #carousel-company-2").owlCarousel({
        // items : 5,
        autoplayTimeout : 2000,
        autoplay: true,
        dots: false,
        loop: false,
        nav : true,
        rtl : true,

        pagination : false,
        // paginationNumbers: false

        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav : true,
            },
            768:{
                items:2,
                nav : true,
            },
            1200:{
                items:3,
                nav : true,
            },
            1400:{
                items:3,
                nav : true,
            }
        }
    })


});

function sendTicket()
{
    let sendTicketUrl = document.getElementById('sendTicketUrl').value;

    // let name = document.getElementById('name').value;
    // let nameErr = document.getElementById('nameErr');

    let email = document.getElementById('email').value;
    let emailErr = document.getElementById('emailErr');

    let subject = document.getElementById('subject').value;
    let subjectErr = document.getElementById('subjectErr');

    // let phone = document.getElementById('phone').value;
    // let phoneErr = document.getElementById('phoneErr');

    let text = document.getElementById('text').value;
    let textErr = document.getElementById('textErr');

    let owner = document.getElementById('owner').value;

    // if(name == ""){
    //     nameErr.innerHTML = 'نام خود را وارد کنید';
    // }

    if(email == ""){
        emailErr.innerHTML = 'ایمیل خود را وارد کنید';
    }

    if(subject == ""){
        subjectErr.innerHTML = 'موضوع پیام را وارد کنید';
    }

    // if(phone == ""){
    //     phoneErr.innerHTML = 'تلفن خود را وارد کنید';
    // }

    if(text == ""){
        textErr.innerHTML = 'پیام مورد نظر خود را وارد کنید';
    }

    if(email != "" && subject != "" && text != ""){
        $.ajax({
            headers:{
                'X-CSRF-TOKEN': CSRF
            },
            url: sendTicketUrl,
            type:'post',
            data:{
                subject:subject,
                owner:owner,
                email:email,
                message:text,
            },
            success:function (response){
                subject.value = ""
                email.value = ""
                text.value = ""
                $('#send-ticket').addClass('disabled')
                swal.fire('پیام شما ارسال شد.')
            },
            error:function (response){
                console.log(response)
            }
        })
    }

}

