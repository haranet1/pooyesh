function acceptInternPosition(id)
{
    let acceptInternPositionUrl = document.getElementById('acceptJobPositionUrl').value;
    Swal.fire({
        title: 'موقعیت کارآموزی تایید خواهد شد!',
        showCancelButton: true,
        confirmButtonText: 'تایید',
        cancelButtonText: `انصراف`,
        customClass: {
            confirmButton: "btn btn-success",
        },
        icon: 'success',
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: acceptInternPositionUrl,
                data: {
                    id : id,
                },
                dataType: "json",
                beforeSend: function () {
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق',
                        'موقعیت کارآموزی مورد نظر تایید شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response);
                }
            })
        }
    })
}
function acceptHirePosition(id)
{
    let acceptInternPositionUrl = document.getElementById('acceptJobPositionUrl').value;
    Swal.fire({
        title: 'موقعیت استخدامی تایید خواهد شد!',
        showCancelButton: true,
        confirmButtonText: 'تایید',
        cancelButtonText: `انصراف`,
        customClass: {
            confirmButton: "btn btn-success",
        },
        icon: 'success',
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: acceptInternPositionUrl,
                data: {
                    id : id,
                },
                dataType: "json",
                beforeSend: function () {
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق',
                        'موقعیت استخدامی مورد نظر تایید شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response);
                }
            })
        }
    })
}