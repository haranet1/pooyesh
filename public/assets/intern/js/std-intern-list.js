function acceptRequest(id)
{
    let studentAcceptRequestUrl = document.getElementById('studentAcceptRequestUrl').value;
    Swal.fire({
        title: 'درخواست همکاری با شرکت تایید خواهد شد!',
        showCancelButton: true,
        confirmButtonText: 'تایید',
        cancelButtonText: `انصراف`,
        customClass: {
            confirmButton: "btn btn-success",
        },
        icon: 'success',
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: studentAcceptRequestUrl,
                data: {
                    id : id,
                },
                dataType: "json",
                beforeSend: function () {
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق',
                        'درخواست همکاری تایید شد!',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response);
                }
            })
        }
    })
}

function rejectRequest(id)
{
    let studentRejectRequestUrl = document.getElementById('studentRejectRequestUrl').value;
    Swal.fire({
        title: 'درخواست کارجو رد خواهد شد!',
        showCancelButton: true,
        confirmButtonText: 'تایید',
        cancelButtonText: `انصراف`,
        customClass: {
            confirmButton: "btn btn-success",
        },
        icon: 'warning',
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: studentRejectRequestUrl,
                data: {
                    id : id,
                },
                dataType: "json",
                beforeSend: function () {
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق',
                        'درخواست همکاری رد شد!',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response);
                }
            })
        }
    })
}
