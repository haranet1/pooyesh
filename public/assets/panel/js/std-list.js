// city ajax

let provinceSelect = document.getElementById('province');
let citySelect = document.getElementById('city');

provinceSelect.addEventListener('change', function () {

    let province = provinceSelect.value;
    
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': csrf
        },
        url: url,
        data: {
            do: 'get-city',
            province: province
        },
        dataType: 'json',
        success: function (response) {
            // Clear existing city options
            citySelect.innerHTML = '';
            let option = document.createElement('option');
            option.value = "0";
            option.text = "شهر"
            citySelect.appendChild(option);
            // Add new city options based on the response
            response.forEach(function (city) {
                let option = document.createElement('option');
                option.value = city.id;
                option.text = city.name;
                citySelect.appendChild(option);
            });
        },
        error: function (response) {
            console.log(response);
        }
    });
});

// ----------------------------------------------------------------

// listener function for on change 

function UpdateVars(){

    let result = {
        province : "*",
        city : "*",
        grade : "*",
        experince : "*",
        sex : "*",
        marige : "*",
        age : "*",
    };

    let provinceVar = document.getElementById('province').value;
    let cityVar = document.getElementById('city').value;
    let gradeVar = document.getElementById('grade').value;
    let sexVar = document.getElementById('sex').value;
    let ageVar = document.getElementById('age').value;
    
    if(urlR == aplUrl){
        let experinceVar = document.getElementById('experince').value;
        let marigeVar = document.getElementById('marige').value;
        if(marigeVar != 0){
            result.marige = marigeVar;
        }
        if(experinceVar != 0){
            result.experince = experinceVar;
        }
    }

    if(provinceVar != 0){
        result.province = provinceVar;
    }
    if(cityVar != 0){
        result.city = cityVar;
    }
    if(gradeVar != 0){
        result.grade = gradeVar;
    }
    
    if(sexVar != 0){
        result.sex = sexVar;
    }
    
    if(ageVar != 0){
        result.age = ageVar;
    }

    sendRequest(result);
}

// ----------------------------------------------------------------

// send request to server 

function sendRequest(result){
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': csrf 
        },
        url: urlR,
        data: { 
            resultObj: result,
        },
        dataType: 'json',
            success: function (response) {
                makeCard(response);
                
        },
        error: function (response) {
            
            console.log(response);
        }
    });
}

function makeCard(students){
    var row = document.getElementById('rowId');
    row.innerHTML = '';
    
    

    students.forEach((student) => {
        var province = ' ';
        var city = ' ';
        var sex = ' ';
        var major = ' ';
        var grade = ' ';
        var university = ' ';
        var age = ' ';
        if(urlR == aplUrl){
            newSinglePage = singlePage.replace(':id', student.id);
            province = student['applicant_infos']['province']['name'];
            city = student['applicant_infos']['city']['name'];
            if(student['academic_infos'] && student['academic_infos'].length > 0){
                grade = student['academic_infos'][student['academic_infos'].length-1]['grade'];
                major = student['academic_infos'][student['academic_infos'].length-1]['major'];
                university = student['academic_infos'][student['academic_infos'].length-1]['university'];
            }else{
                grade = ' ';
                mojor = ' ';
                university = ' ';
            }
            age = student['age'];
        }else if(urlR == stdUrl){
            newSinglePage = singlePage.replace(':id', student.id);
            province = student['student_infos']['province']['name'];
            if(student['student_infos']['city'] != null){
                city = student['student_infos']['city']['name'];
            }else{
                city = ' ';
            }
            grade = student['student_infos']['grade'];
            major = student['student_infos']['major'];
            university = student['student_infos']['university']['name'];
            age = student['student_infos']['age'];
        }
        if(student['sex'] == 'male'){
            sex = 'مرد';
        }else if(student['sex'] == 'female'){
            sex = 'زن';
        }
        const studentCard = document.createElement('div');
        studentCard.className = 'col-12 col-md-6 col-lg-6 col-xl-4';
        studentCard.innerHTML = `
          <div class="card" style="border-radius: 15px;">
            <div class="card-body p-4">
              <div class="d-flex text-black">
                <div class="col-3 flex-shrink-0">
                  <img src="`+imageUrl+`" alt="${student.name} ${student.family}" class="img-fluid" style="border-radius: 10px;">
                </div>
                <div class="flex-grow-1 ms-3">
                  <h4 class="mb-1">${student.name} ${student.family}</h4>
                  <p class="mb-2 pb-1" style="color: #2b2a2a; font-size: 0.8rem;">
                    ${grade} ${major} ${university}
                  </p>
                  <div class="row d-flex justify-content-start rounded-3 p-2 mb-2 ms-1"
                  style="background-color: #efefef;">
                      <div class="col-3 pe-0">
                          <p class="small text-muted mb-1">استان</p>
                          <p class="mb-0">`+ province +`</p>
                      </div>
                      <div class="col-4 pe-0">
                          <p class="small text-muted mb-1">شهر</p>
                          <p class="mb-0">${city}</p>
                      </div>
                      <div class="col-2 pe-0">
                          <p class="small text-muted mb-1">سن</p>
                          <p class="mb-0">${age}</p>
                      </div>
                      <div class="col-3 pe-0">
                          <p class="small text-muted mb-1">جنسیت</p>
                          <p class="mb-0">${sex}</p>
                      </div>
                  </div>
                  <div class="d-flex pt-1">
                    <a href="`+ newSinglePage +`" class="btn btn-primary flex-grow-1">مشاهده پروفایل</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        `;
      
        row.appendChild(studentCard);
      });
}

