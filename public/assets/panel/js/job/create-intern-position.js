$(document).ready(function () {

    ClassicEditor.create( document.querySelector( '#editor' ),{language: 'fa'} ).catch( error => {console.error( error );} );

    var sliders = document.querySelectorAll('.form-range');

    // var feedback = document.querySelector('.feedback-range');
    // console.log(sliders);

    sliders.forEach(changeslider);
    // feedback.forEach(changefeedback);


    function changeslider(item) {
        if($(item).val() == 0) {
            item.style.setProperty('--thumbColor', '#ff0000');
            
            item.value = "0";
        }
        if($(item).val() == 1) {
            item.style.setProperty('--thumbColor', '#ff0000');
            item.value = "1";
        }
        if($(item).val() == 2) {
            item.style.setProperty('--thumbColor', '#ff0000');
            item.value = "2";
        }
        else if($(item).val() == 3){
            item.style.setProperty('--thumbColor', '#ffdc2e');
            item.value = "3";
        } 
        else if($(item).val() == 4){
            item.style.setProperty('--thumbColor', '#0a53ff');
            item.value = "4";
        }
        else if($(item).val() == 5){
            item.style.setProperty('--thumbColor', '#298102');
            item.value = "5";
        }

        item.addEventListener('input', function() {
            // console.log($(this).val());
            if($(this).val() == 0) {
                item.style.setProperty('--thumbColor', '#ff0000');
            }
            if($(this).val() == 1) {
                item.style.setProperty('--thumbColor', '#ff0000');
            }
            if($(this).val() == 2) {
                item.style.setProperty('--thumbColor', '#ff0000');
            }
            else if($(this).val() == 3){
                item.style.setProperty('--thumbColor', '#ffdc2e');
            } 
            else if($(this).val() == 4){
                item.style.setProperty('--thumbColor', '#0a53ff');
            }
            else if($(this).val() == 5){
                item.style.setProperty('--thumbColor', '#298102');
            }
    
        });
    }


    var checkBox = document.querySelectorAll('.form-check-input');
    var activeBox = document.querySelectorAll('.skill-active');

    checkBox.forEach((item, index) => {
        const item2 = activeBox[index];
        item.addEventListener('input', function(){
            if (item.checked == true){
                item2.classList.toggle("checked");
            }
            else if (item.checked == false){
                item2.classList.toggle("checked");
            }
        });

      });

    // activeBox.forEach(borderActive);

    // function borderActive(item) {
    //     if (checkBox.checked == true){
    //         item.style.border = " 2px solid #7d77df";
    //     }
    // }





}); 

function getCities(){
    let getCitiesUrl = document.getElementById('getCitiesUrl').value;
    let provinceValue = document.getElementById('province').value;
    if(provinceValue != 0){
        $(document).ready(function(){
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('#Csrf32').val()
                },
                url: getCitiesUrl,
                data: {
                    province: provinceValue ,
                },
                dataType: 'json',
                success: function (response) {
                    var city = document.getElementById('city')
                    city.innerHTML = ''
                    response.forEach(function (res){
                        city.innerHTML += '<option value="'+res['id']+'" label="'+res['name']+'">'+res['name']+'</option>'
                    })
                },
                error: function (response) {
                    console.log(response)
                    console.log('error')
                }
            });
        });
    }
}

// function getCities(){ 
    
    
//     let provinceValue = document.getElementById('province').value;
//     if(provinceValue != 0){
//         $(document).ready(function(){
//             $.ajax({
//                 type: "POST",
//                 headers: {
//                     'X-CSRF-TOKEN': $('#Csrf32').val()
//                 },
//                 url: getCitiesUrl,
//                 data: {
//                     province: provinceValue ,
//                 },
//                 dataType: 'json',
//                 success: function (response) {
//                     console.log(response);
//                     let ulCity = document.getElementById('city');

//                     ulCity = Array.from(ulCity);

//                     ulCity.forEach((city) => {
//                         city.innerHTML = '';
//                         response.forEach( function (res){
//                             name2 = res['name'];
//                             city.innerHTML += `<li role="button" onclick="setValueToInput('city_input' , 'cityBtn' , '${name2}')" class="p-2 item-border">`+ res['name'] +`</li>`;
//                         })
//                     })
                    
//                 },
//                 error: function (response) {
//                     console.log(response)
//                     console.log('error')
//                 }
//             });
//         });
//     }
// }