
document.addEventListener("DOMContentLoaded", function() {
    
    let hire_or_intern = document.getElementById('hire_or_intern')
    let typeInput = document.getElementById('type')

    if (!canCreateIntern) {
        hire_or_intern.checked = true;
    }

    if(hire_or_intern.checked != true){
        typeInput.classList.add('disable');
        typeInput.setAttribute('disabled' , true);
    }
    
});

function swichHireOrIntern()
{
    let hire_or_intern = document.getElementById('hire_or_intern')
    let typeInput = document.getElementById('type')

    if(!canCreateIntern){
        hire_or_intern.checked = true;

        Swal.fire({
            icon : 'error',
            title : 'خطا !',
            text : 'ظرفیت ثبت موقعیت کارآموزی شما تکمیل شده، برای ثبت موقعیت کارآموزی بیشتر از طریق بسته های خدماتی اقدام به خرید نمایید.', 
            confirmButtonText : 'خرید بسته های خدماتی',
        }).then((result)=>{
            if(result.isConfirmed){
                window.location.href = companyPackageUrl;
            }
        })
    }

    
    if(hire_or_intern.checked != true){
        typeInput.classList.add('disable');
        typeInput.setAttribute('disabled' , true);
    } else{
        typeInput.classList.remove('disable');
        typeInput.removeAttribute('disabled' , true);
    }
}

