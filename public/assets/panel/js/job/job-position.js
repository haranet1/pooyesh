function deleteJobPosition(id)
{
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "موقعیت شغلی انتخاب شده حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله، حذف کن'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': config.csrf
                },
                url: deleteJobPositionUrl,
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'موقعیت شغلی با موفقیت حذف شد.',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })

}

function confirmJobPosition(id) 
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        type: 'POST',
        url: confirmJobPositionUrl,
        data: {
            id: id
        },
        success: function (data) {
            console.log(data)
            Swal.fire({
                icon: 'success',
                title: 'موقعیت شغلی با موفقیت تایید شد',
                showConfirmButton: true,
            })
            location.reload()
        },
        error: function (data) {
            console.log(data.error)
        }

    })
}

function activeJobPosition(id) {
    let swich = document.getElementById('active-' + id);
    let span = document.getElementById('active-span-' + id);
    
    let activeStatus = swich.checked ? 1 : 0;

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        type: 'POST',
        url: activeJobPositionUrl,
        data: {
            id: id,
            activeStatus: activeStatus,
        },
        dataType: "json",
        beforeSend: function() {
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function(data) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);

            span.innerText = data;
            
        },
        error: function(data) {
            console.log(data.error);
        }
    });
}
