function submitForm() {

    async function scrollToElement(elementId) {
        document.getElementById(elementId).scrollIntoView({
            behavior: 'smooth'
        });

        // استفاده از Promise برای ایجاد تأخیر
        await new Promise(resolve => setTimeout(resolve, 500));
        window.scrollBy(0, -150);
    }

    let savedForm = document.getElementById('savedForm');

    if($('input[name="title"]').val() == '' || $('input[name="title"]').val() == null){
        $('#titleErr').html('وارد کردن عنوان الزامی است.');
        scrollToElement("title");
        return;
    }

    if (skill) {
        if(skillNum > 6 && $('input[name^="skill"]').length < 3){
            $('#skillErr').html('حداقل 3 مورد از مهارت ها را انتخاب کنید.');
            scrollToElement("skillBox");
            return;
        }
    }

    if (knowledge) {
        if(knowledgeNum > 6 && $('input[name^="knowledge"]').length < 3){
            $('#knowledgeErr').html('حداقل 3 مورد از دانش ها را انتخاب کنید.');
            scrollToElement("knowledgeBox");
            return;
        }
    }

    if (ability) {
        if(abilityNum > 6 && $('input[name^="ability"]').length < 3){
            $('#abilityErr').html('حداقل 3 مورد از توانایی ها را انتخاب کنید.');
            scrollToElement("abilityBox");
            return;
        }
    }

    if (workStyle) {
        if(workStyleNum > 6 && $('input[name^="workStyle"]').length < 3){
            $('#workStyleErr').html('حداقل 3 مورد از ویژگی های کاری را انتخاب کنید.');
            scrollToElement("workStyleBox");
            return;
        }
    }

    if (technologySkill) {
        if(technologySkillNum > 6 && $('input[name^="technologySkill"]').length < 3){
            $('#technologySkillErr').html('حداقل 3 مورد از مهارت های فنی را انتخاب کنید.');
            scrollToElement("technologySkillBox");
            return;
        }
    }

    if (task) {
        if(taskNum > 6 && $('input[name^="task"]').length < 3){
            $('#taskErr').html('حداقل 3 مورد از مهارت های فنی را انتخاب کنید.');
            scrollToElement("taskBox");
            return;
        }
    }

    if($('select[name="province"]').val() == '' || $('select[name="province"]').val() == null){
        console.log($('select[name="province"]').val());
        
        $('#provinceErr').html('انتخاب استان الزامی است.');
        scrollToElement("workPlaceInfoBox");
        return;
    }

    if($('select[name="city"]').val() == '' || $('select[name="city"]').val() == null){
        $('#cityErr').html('انتخاب شهر الزامی است.');
        scrollToElement("workPlaceInfoBox");
        return;
    }

    if($('input[name="address"]').val() == '' || $('input[name="address"]').val() == null){
        $('#addressErr').html('وارد کردن استان الزامی است.');
        scrollToElement("workPlaceInfoBox");
        return;
    }
    

    savedForm.submit();
}
