
document.addEventListener("DOMContentLoaded", function() {
    // تمام چک‌باکس‌ها را انتخاب کنید
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    // تیک همه چک‌باکس‌ها را بردارید
    checkboxes.forEach(function(checkbox) {
        checkbox.checked = false;
    });
   

});

function setValueToInput(text , value)
{
    let input = document.getElementById('category_input');
    let catText = document.getElementById('category_text');
    let jobname = document.getElementById('job_title');

    catText.innerHTML = text;
    input.value = value;

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf // مطمئن شوید که مقدار myCsrf به درستی تنظیم شده است
        },
        url: jobsByCatIdUrl, // مطمئن شوید که jobNameUrl به درستی تنظیم شده است
        data: {
            cat_id: value,
        },
        dataType: 'json',
        success: function(response) {
            autocomplete(jobname, response);
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function autocompleteAjax()
{
    let jobname = document.getElementById('job_title');
    // let catId = document.getElementById('category_input').value;

    // بررسی اینکه آیا عناصر وجود دارند
    if (!jobname) {
        console.error('Element with id "job_title" not found');
        return;
    }
    // if (!catId) {
    //     console.error('Element with id "category_input" not found');
    //     return;
    // }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf // مطمئن شوید که مقدار myCsrf به درستی تنظیم شده است
        },
        url: jobNameUrl, // مطمئن شوید که jobNameUrl به درستی تنظیم شده است
        data: {
            // cat_id: catId,
            job_title: jobname.value
        },
        dataType: 'json',
        success: function(response) {
            autocomplete(jobname, response);
        },
        error: function(response) {
            console.log(response);
        }
    });

}

function autocomplete(jobname, response) {
    let dropdown = document.getElementById('dropdownElement');

    if (!dropdown) {
        dropdown = document.createElement('div');
        dropdown.id = 'dropdownElement';
        dropdown.className = 'dropdown-menu pb-3 mw-dropdown';
        jobname.parentNode.appendChild(dropdown);
    }

    dropdown.innerHTML = '';

    if (response.length > 0) {
        let ul = document.createElement('ul');

        response.forEach(item => {
            let li = document.createElement('li');
            li.className = 'dropdown-item';
            li.setAttribute("role" , "button");
            li.setAttribute("onclick", "setJobName('" + item['title'] + "', '" + item['id'] + "')");
            li.textContent = item['title']; // نام شغل یا متن دیگر
            ul.appendChild(li);
        });

        dropdown.appendChild(ul);
    } else {
        let p = document.createElement('p');
        p.className = 'ms-4 mt-3 red fs-17';
        p.textContent = 'این شغل وجود ندارد ... !';
        dropdown.appendChild(p);
        
        // let button = document.createElement('button');
        // button.className = 'bg-blue-c text-white fs-6 px-2 py-1 btn-job-p rounded-3 float-end me-3 px-4';
        // button.textContent = '+ اضافه کردن'; 
        // dropdown.appendChild(button);
    }

    if (!dropdown.classList.contains('show')) {
        dropdown.classList.add('show');
    }
}

function setJobName(text, value)
{
    let job_title = document.getElementById('job_title');
    let job_id = document.getElementById('job_id');

    job_title.value = '';
    job_id.value = '';

    job_title.value += text;
    job_id.value += value;
    submitSelectForm(value);
}

function submitSelectForm(job_id)
{
    let selectJobForm = document.getElementById('selectJobForm');
    selectJobForm.submit();
}

// function autocomplete(inp , arr)
// {
//     let existingElement = document.getElementById("autocomplete-list")
//     if (existingElement)
//         existingElement.remove()

//     var currentFocus;

//     var ul, li = null

//     currentFocus = -1;

//     let dropdownElement = document.getElementById('dropdownElement');

//     ul = document.createElement('ul');
//     ul.setAttribute("id" , inp.id + "autocomplete-list");
//     ul.setAttribute("class" , "autocomplete-items");

//     dropdownElement.appendChild(ul);

//     for (var i = 0; i < arr.length; i++) {

//         li = document.createElement("li");
//         li.setAttribute("class" , "dropdown-item");

//         li.innerHTML = arr[i]['title'];
//         li.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['title'] + "'>";

//         li.addEventListener("click" , function (e) {

//             inp.value = this.getElementsByTagName("input")[0].value;

//             // cat = document.getElementById('titleId')


//             closeAllLists();
//         }) 

//     }

//     inp.addEventListener("keydown" , function (e) {

//         let x = document.getElementById(this.id + "autocomplete-list");

//         if (x) x = x.getElementsByTagName("div");
//         if(e.keyCode == 40){

//             currentFocus++;

//             addActive(x);

//         } else if(e.keyCode == 38) {

//             currentFocus--;
//             addActive(x);
//         } else if (e.keyCode == 13) {

//             e.preventDefault();
//             if (currentFocus > -1) {
//                 if (x) x[currentFocus].click();
//             }

//         }

//     });

//     function addActive(x)
//     {
//         if(!x) return false;

//         removeActive(x);
//         if (currentFocus >= x.length) currentFocus = 0;
//         if (currentFocus < 0) currentFocus = (x.length - 1);
//         x[currentFocus].classList.add("autocomplete-active");
//     }

//     function removeActive(x) {
//         for (var i = 0; i < x.length; i++) {
//             x[i].classList.remove("autocomplete-active");
//         }
//     }

//     function closeAllLists(elmnt) {
//         /*close all autocomplete lists in the document,
//         except the one passed as an argument:*/
//         var x = document.getElementsByClassName("autocomplete-items");
//         for (var i = 0; i < x.length; i++) {
//             if (elmnt != x[i] && elmnt != inp) {
//                 x[i].parentNode.removeChild(x[i]);
//             }
//         }
//     }

//     document.addEventListener("click", function (e) {
//         closeAllLists(e.target);
//     });
// }

let savedForm = document.getElementById('savedForm');

function setDataInForm(e , type)
{
    if(e.checked == true){
        let data = document.createElement("input");
        data.setAttribute('type' , 'hidden');
    
        let range = document.getElementById(type + 'Range-' + e.id.split('-')[1]);
    
        data.setAttribute('id' , type + '_' + e.value);
        data.setAttribute('name' , type + '_' + e.value);
        data.setAttribute('value' , range.value);
    
        savedForm.appendChild(data);

    } else {

        let string = '#'+ type +'_' + e.value;

        if($(string).length > 0){

            $(string).remove();

        }
    }
}

function changeDataRange(e , type)
{
    // console.log(e.nextElementSibling.textContent);

    let rangeText = e.nextElementSibling;
    rangeText.innerHTML = '';

    switch (parseInt(e.value)){
        case 0 :
            rangeText.innerHTML = 'ضعیف';
            break;
        case 1 :
            rangeText.innerHTML = 'ضعیف';
            break;
        case 2 :
            rangeText.innerHTML = 'مبتدی';
            break;
        case 3 :
            rangeText.innerHTML = 'متوسط';
            break;
        case 4 :
            rangeText.innerHTML = 'خوب';
            break;
        case 5 :
            rangeText.innerHTML = 'عالی';
            break;

    }

    let Input = document.getElementById(type + 'Input-' + e.id.split('-')[1]);

    if(Input !== null){

        let string = '#'+ type +'_' + Input.value;

        if($(string).length > 0){

            $(string).val(e.value);

        }
    }
}

function setTechnologySkillInForm(e)
{

}


