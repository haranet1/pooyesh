document.addEventListener("DOMContentLoaded", function() {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    checkboxes.forEach(function(checkbox) {
        checkbox.checked = false;
    });

    var sliders = document.querySelectorAll('.form-range');

    sliders.forEach(changeslider);

    function changeslider(item) {
        if($(item).val() == 0) {
            item.style.setProperty('--thumbColor', '#ff0000');
            item.value = "0";
        }
        if($(item).val() == 1) {
            item.style.setProperty('--thumbColor', '#ff0000');
            item.value = "1";
        }
        if($(item).val() == 2) {
            item.style.setProperty('--thumbColor', '#ff0000');
            item.value = "2";
        }
        else if($(item).val() == 3){
            item.style.setProperty('--thumbColor', '#ffdc2e');
            item.value = "3";
        } 
        else if($(item).val() == 4){
            item.style.setProperty('--thumbColor', '#0a53ff');
            item.value = "4";
        }
        else if($(item).val() == 5){
            item.style.setProperty('--thumbColor', '#298102');
            item.value = "5";
        }

        item.addEventListener('input', function() {
            // console.log($(this).val());
            if($(this).val() == 0) {
                item.style.setProperty('--thumbColor', '#ff0000');
            }
            if($(this).val() == 1) {
                item.style.setProperty('--thumbColor', '#ff0000');
            }
            if($(this).val() == 2) {
                item.style.setProperty('--thumbColor', '#ff0000');
            }
            else if($(this).val() == 3){
                item.style.setProperty('--thumbColor', '#ffdc2e');
            } 
            else if($(this).val() == 4){
                item.style.setProperty('--thumbColor', '#0a53ff');
            }
            else if($(this).val() == 5){
                item.style.setProperty('--thumbColor', '#298102');
            }
    
        });
    }

    const element = document.querySelector('#js-choice');
    const choices = new Choices(element, {
        searchEnabled: true, 
    });
    
    $('#js-choice').on('change', function() {
        var selectedOption = $("#js-choice option:selected").val(); 
        
        var selectedText = $("#js-choice option:selected").text(); 
        
        if(selectedOption !== undefined && selectedOption !== ''){
            addTechnologySkill(selectedOption, selectedText);
        }
    });

});

function addTechnologySkill(id, title) {
    selectedExamples = $('input[type="checkbox"][id^="workStyle"]');
    console.log(selectedExamples);
    
    let canBeOption = true;
    if (selectedExamples.length > 0) {
        
        selectedExamples.each(function(index, element) {
            
            let selectedID = element.id.split('-')[1];
            console.log(selectedID);
            
            
            if(id == selectedID){
                canBeOption = false;
            }
        });
    }
    // ایجاد ساختار HTML
    if(canBeOption){
        var html = `
            <div class="col-md-6 col-12">
                <div class="bg-body-2 mt-4 rounded-4 p-3 workStyle-active">
                    <div class="flex-column">
                        <div class="d-flex align-self-start align-self-md-center mb-2">
                            <input onclick="setDataInForm(this , 'workStyle')" checked class="form-check-input" type="checkbox" id="workStyleInput-${id}" value="${id}" />
                            <label for="workStyleInput-${id}" class="text-dark-c mb-0 fs-16 fw-normal">${title}</label>
                        </div>
                        <div class="d-flex width-range gap-3 align-items-center me-md-5 w-100">
                            <input oninput="changeDataRange(this , 'workStyle')" id="workStyleRange-${id}" type="range" class="form-range" min="1" max="5" step="1" value=""/>
                            <div class="text-dark-c mb-0 fs-16 fw-normal fit-content w-25"></div>
                        </div>
                    </div>
                </div>
            </div>`;
            
        $('#workStylesContainer').append(html);
        setDataInForm(document.getElementById(`workStyleInput-${id}`), 'workStyle');
    }
}


let savedForm = document.getElementById('savedForm');

function setDataInForm(e , type)
{
    if(e.checked == true){
        let data = document.createElement("input");
        data.setAttribute('type' , 'hidden');
    
        let range = document.getElementById(type + 'Range-' + e.id.split('-')[1]);
    
        data.setAttribute('id' , type + '_' + e.value);
        data.setAttribute('name' , type + '_' + e.value);
        data.setAttribute('value' , range.value);
    
        savedForm.appendChild(data);

    } else {

        let string = '#'+ type +'_' + e.value;

        if($(string).length > 0){

            $(string).remove();

        }
    }
}

function changeDataRange(e , type)
{

    let rangeText = e.nextElementSibling;
    rangeText.innerHTML = '';

    switch (parseInt(e.value)){
        case 0 :
            rangeText.innerHTML = 'ضعیف';
            e.style.setProperty('--thumbColor', '#ff0000');
            e.value = "0";
            break;
        case 1 :
            rangeText.innerHTML = 'ضعیف';
            e.style.setProperty('--thumbColor', '#ff0000');
            e.value = "1";
            break;
        case 2 :
            rangeText.innerHTML = 'مبتدی';
            e.style.setProperty('--thumbColor', '#ff0000');
            e.value = "2";
            break;
        case 3 :
            rangeText.innerHTML = 'متوسط';
            e.style.setProperty('--thumbColor', '#ffdc2e');
            e.value = "3";
            break;
        case 4 :
            rangeText.innerHTML = 'خوب';
            e.style.setProperty('--thumbColor', '#0a53ff');
            e.value = "4";
            break;
        case 5 :
            rangeText.innerHTML = 'عالی';
            e.style.setProperty('--thumbColor', '#298102');
            e.value = "5";
            break;

    }

    let Input = document.getElementById(type + 'Input-' + e.id.split('-')[1]);
    
    
    if(Input !== null){

        let string = type +'_' + Input.value;

        if($('[name='+string+']').length > 0){

            $('[name='+string+']').val(e.value);

        }
    }
}
function submitForm() {


    let savedForm = document.getElementById('savedForm');

    if ($('input[type="checkbox"][id^="workStyle"]').length < 3) {
        $('#workStyleErr').html('حداقل 3 مورد از ویژگی های کاری را انتخاب کنید.');
        // scrollToElement("skillBox");
        return;
    }

    savedForm.submit();
}


