let choicesInstance2; 
let selectedExamples;
document.addEventListener("DOMContentLoaded", function() {

    var sliders = document.querySelectorAll('.form-range');
    sliders.forEach(changeslider);

    function changeslider(item) {
        if($(item).val() == 0) {
            item.style.setProperty('--thumbColor', '#ff0000');
            
            item.value = "0";
        }
        if($(item).val() == 1) {
            item.style.setProperty('--thumbColor', '#ff0000');
            item.value = "1";
        }
        if($(item).val() == 2) {
            item.style.setProperty('--thumbColor', '#ff0000');
            item.value = "2";
        }
        else if($(item).val() == 3){
            item.style.setProperty('--thumbColor', '#ffdc2e');
            item.value = "3";
        } 
        else if($(item).val() == 4){
            item.style.setProperty('--thumbColor', '#0a53ff');
            item.value = "4";
        }
        else if($(item).val() == 5){
            item.style.setProperty('--thumbColor', '#298102');
            item.value = "5";
        }

        item.addEventListener('input', function() {
            // console.log($(this).val());
            if($(this).val() == 0) {
                item.style.setProperty('--thumbColor', '#ff0000');
            }
            if($(this).val() == 1) {
                item.style.setProperty('--thumbColor', '#ff0000');
            }
            if($(this).val() == 2) {
                item.style.setProperty('--thumbColor', '#ff0000');
            }
            else if($(this).val() == 3){
                item.style.setProperty('--thumbColor', '#ffdc2e');
            } 
            else if($(this).val() == 4){
                item.style.setProperty('--thumbColor', '#0a53ff');
            }
            else if($(this).val() == 5){
                item.style.setProperty('--thumbColor', '#298102');
            }
    
        });
    }
    
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    
    checkboxes.forEach(function(checkbox) {
        checkbox.checked = false;
    });

    var checkBox = document.querySelectorAll('.form-check-input');
    var activeBox = document.querySelectorAll('.technologySkill-active');

    checkBox.forEach((item, index) => {
        const item2 = activeBox[index];
        item.addEventListener('input', function(){
            if (item.checked == true){
                item2.classList.toggle("checked");
            }
            else if (item.checked == false){
                item2.classList.toggle("checked");
            }
        });

    });

    const element = document.querySelector('#js-choice');
    const choices = new Choices(element, {
        searchEnabled: true, 
    });
    
    element.addEventListener(
        'change',
        function(event) {
            let selectedValue = event.target.value;
            var selectedOption = $("#js-choice option:selected").val(); 
        
            var selectedText = $("#js-choice option:selected").text(); 

            
            if(selectedOption !== undefined){
                addTechnologySkill(selectedOption, selectedText);
            }
        },
        false,
    );

    // $('#js-choice2').on('change', function() {
    //     var selectedOption = $("#js-choice2 option:selected").val(); 
        
    //     var selectedText = $("#js-choice2 option:selected").text(); 

        
    //     if(selectedOption !== undefined){
    //         addTechnologySkill(selectedOption, selectedText);
    //     }
    // });

    // var element2 = $('#js-choice2'); 
    // choicesInstance2 = new Choices(element2.get(0), {
    //     searchEnabled: true
    // });
        
});

function fetchDataForSecondSelect(data)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        type: 'POST',
        url: $('#getExamplesCvUrl').val(),
        data: {
            id: data
        },
        dataType: "json",
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            
            updateSecondSelect(response);
            
            
        },
        error: function (data) {
            console.log(data.error)
        }

    })
}


function updateSecondSelect(options) {
    selectedExamples = $('input[type="checkbox"][id^="technologySkill"]');
    
    var element2 = $('#js-choice2'); 
    
    if (choicesInstance2) {
        choicesInstance2.destroy();
    }
    
    element2.empty();

    $.each(options, function(index, option) {
        let canBeOption = true;
        if (selectedExamples.length > 0) {
            
            selectedExamples.each(function(index, element) {
                
                let selectedID = element.id.split('-')[1];
                
                if(option.example_id == selectedID){
                    canBeOption = false;
                }
            });
        }
        if(canBeOption){
            element2.append($('<option>', {
                value: option.example_id,
                text: option.example.example
            }));
        }
    });

    choicesInstance2 = new Choices(element2.get(0), {
        searchEnabled: true
    });

}

function addTechnologySkill(id, title) {
    selectedExamples = $('input[type="checkbox"][id^="technologySkill"]');
    let canBeOption = true;
    if (selectedExamples.length > 0) {
        
        selectedExamples.each(function(index, element) {
            
            let selectedID = element.id.split('-')[1];
            
            if(id == selectedID){
                canBeOption = false;
            }
        });
    }
    // ایجاد ساختار HTML
    if(canBeOption){
        var html = `
            <div class="col-md-6 col-12">
                <div class="bg-body-2 mt-4 rounded-4 p-3 technologySkill-active">
                    <div class="flex-column">
                        <div class="d-flex align-self-start align-self-md-center mb-2">
                            <input onclick="setDataInForm(this , 'technologySkill')" checked class="form-check-input" type="checkbox" id="technologySkillInput-${id}" value="${id}" />
                            <label for="technologySkillInput-${id}" class="text-dark-c mb-0 fs-16 fw-normal">${title}</label>
                        </div>
                        <div class="d-flex width-range gap-3 align-items-center me-md-5 w-100">
                            <input oninput="changeDataRange(this , 'technologySkill')" id="technologySkillRange-${id}" type="range" class="form-range" min="1" max="5" step="1" value=""/>
                            <div class="text-dark-c mb-0 fs-16 fw-normal fit-content w-25"></div>
                        </div>
                    </div>
                </div>
            </div>`;
            
        $('#technologySkillsContainer').append(html);
        setDataInForm(document.getElementById(`technologySkillInput-${id}`), 'technologySkill');
    }
}


let savedForm = document.getElementById('savedForm');

function setDataInForm(e , type)
{
    if(e.checked == true){
        let data = document.createElement("input");
        data.setAttribute('type' , 'hidden');
    
        let range = document.getElementById(type + 'Range-' + e.id.split('-')[1]);
    
        data.setAttribute('name' , type + '_' + e.value);
        data.setAttribute('value' , range.value);
    
        savedForm.appendChild(data);

    } else {

        let string = '#'+ type +'_' + e.value;

        if($(string).length > 0){

            $(string).remove();

        }
    }
}

function changeDataRange(e , type)
{

    let rangeText = e.nextElementSibling;
    rangeText.innerHTML = '';

    switch (parseInt(e.value)){
        case 0 :
            rangeText.innerHTML = 'ضعیف';
            e.style.setProperty('--thumbColor', '#ff0000');
            e.value = "0";
            break;
        case 1 :
            rangeText.innerHTML = 'ضعیف';
            e.style.setProperty('--thumbColor', '#ff0000');
            e.value = "1";
            break;
        case 2 :
            rangeText.innerHTML = 'مبتدی';
            e.style.setProperty('--thumbColor', '#ff0000');
            e.value = "2";
            break;
        case 3 :
            rangeText.innerHTML = 'متوسط';
            e.style.setProperty('--thumbColor', '#ffdc2e');
            e.value = "3";
            break;
        case 4 :
            rangeText.innerHTML = 'خوب';
            e.style.setProperty('--thumbColor', '#0a53ff');
            e.value = "4";
            break;
        case 5 :
            rangeText.innerHTML = 'عالی';
            e.style.setProperty('--thumbColor', '#298102');
            e.value = "5";
            break;

    }

    let Input = document.getElementById(type + 'Input-' + e.id.split('-')[1]);
    
    
    if(Input !== null){

        let string = type +'_' + Input.value;

        if($('[name='+string+']').length > 0){

            $('[name='+string+']').val(e.value);

        }
    }
}


function submitForm() {

    let savedForm = document.getElementById('savedForm');

    if ($('input[type="checkbox"][id^="technologySkill"]').length < 1) {
        
        $('#technologySkillErr').html('حداقل 1 مورد از مهارت های فنی را انتخاب کنید.');
        return;
    }

    savedForm.submit();
}




