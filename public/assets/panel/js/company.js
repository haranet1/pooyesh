function getSkillsByCatId(id){
   let skills = document.getElementById('skills')
    skills.innerHTML = ''
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url:config.routes.ajax,
    data: {
        do:'get-skills-by-cat-id',
            cat_id: id,
    },
    dataType: 'json',
        success: function (response) {
        let data
        response.forEach(function (r){
            data +=' <option value="'+r.id+'">'+r.name+'</option>'
        })
            skills.innerHTML = data
    },
    error: function (response) {
        console.log(response)
    }
});
}

const idArray = []
function addSkill(){
    var selectedText = document.getElementById('skills').value;
    if (selectedText != ""){
        checkSkillExists(selectedText,function(skill){
            if (skill !== null){
                var collection_skill = document.getElementsByClassName('array-skills');
                var elementsArray=[].slice.call(collection_skill)
                    var list = document.getElementById('skill-list')
                    var skill_id = skill
                    var element = document.createElement('label');
                    var skill_level = document.getElementById('skill_level').value;
                    var newInput = `<input type="hidden" name="required_skills[${skill_id}]" value="${skill_level}" class="array-skills selectgroup-input">`
                 if(elementsArray.length < 1){
                    element.classList.add('selectgroup-item');
                    element.innerHTML = newInput + '<span class="selectgroup-button">'+ selectedText +'</span>';
                    list.appendChild(element);
                    idArray.push(skill_id)
                 }else if(!idArray.includes(skill_id)){
                            element.classList.add('selectgroup-item');
                            element.innerHTML = newInput + `<span class="selectgroup-button">${selectedText}</span>`;
                            list.appendChild(element);
                            idArray.push(skill_id)
                 }
                 document.getElementById('skills').value = ""
            }else{
                    console.log('ERROR')
            }
        }) 
    }
}

const langArray = []
function addLang(){
    var selectedlang = document.getElementById('lang')
    var selectedIndex = selectedlang.selectedIndex
    var selectedLangText = selectedlang.options[selectedIndex].text
    var langId = selectedlang.value
    checkLangExists(selectedLangText, function(lang){
        if(lang !== null){
            var collection_lang = document.getElementsByClassName('array-langs');
                var elementsArray=[].slice.call(collection_lang)
                    var list = document.getElementById('lang-list')
                    var element = document.createElement('label');
                    var lang_level = document.getElementById('lang_level').value;
                  var newInput = `<input type="hidden" name="required_langs[${langId}]" value="${lang_level}" class="array-langs selectgroup-input">`
                 if(elementsArray.length < 1){
                    element.classList.add('selectgroup-item');
                    element.innerHTML = newInput + `<span class="selectgroup-button">${selectedLangText}</span>`;
                    list.appendChild(element);
                    langArray.push(langId)
                 }else if(!langArray.includes(langId)){
                            element.classList.add('selectgroup-item');
                            element.innerHTML = newInput + `<span class="selectgroup-button">${selectedLangText}</span>`;
                            list.appendChild(element);
                            langArray.push(langId)
                 }
        }else{
            console.log('ERROR')
        }
    })
}

// function deleteJobPosition(id){
//         Swal.fire({
//             title: 'آیا مطمئن هستید؟',
//             text: "موقعیت شغلی انتخاب شده حذف خواهد شد!",
//             icon: 'warning',
//             showCancelButton: true,
//             confirmButtonColor: '#198f20',
//             cancelButtonColor: '#d33',
//             cancelButtonText: 'انصراف',
//             confirmButtonText: 'بله، حذف کن'
//         }).then((result) => {
//             if (result.isConfirmed) {
//                 $.ajax({
//                     type: "POST",
//                     headers: {
//                         'X-CSRF-TOKEN': config.csrf
//                     },
//                     url: config.routes.ajax,
//                     data: {
//                         do: 'delete-job-position',
//                         id: id,
//                     },
//                     dataType: 'json',
//                     success: function (response) {
//                         Swal.fire(
//                             'موفق!',
//                             'موقعیت شغلی با موفقیت حذف شد.',
//                             'success'
//                         )
//                         location.reload()
//                     },
//                     error: function (response) {
//                         console.log(response)
//                     }
//                 });


//             }
//         })

// }

function separate(Number)
{
    Number+= '';
    Number= Number.replace(',', '');
    x = Number.split('.');
    y = x[0];
    z= x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(y))
        y= y.replace(rgx, '$1' + ',' + '$2');
    return y+ z;
}


function checkSkillExists(skill_name,callback) {
    if (skill_name != ""){
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': config.csrf
            },
            url:config.routes.ajax,
        data: {
            do:'check-skill-exists',
                name: skill_name,
        },
        dataType: 'json',
            success: function (response) {
          skill = response['id']
          callback(skill)
        },
        error: function (response) {
            console.log(response)
            callback(null)
        }
    });
    
    }else{
        callback(null)
    }

}
function checkLangExists(lang_name,callback) {
    if (lang_name != ""){
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': config.csrf
            },
            url:config.routes.ajax,
        data: {
            do:'check-lang-exists',
                name: lang_name,
        },
        dataType: 'json',
            success: function (response) {
          skill = response['id']
          callback(skill)
        },
        error: function (response) {
            console.log(response)
            callback(null)
        }
    });
    
    }else{
        callback(null)
    }

}

function deleteSkill(skillId,jobPositionId,tag){
    var skill_label = tag.parentNode
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "مهارت انتخاب شده حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله، حذف کن'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': config.csrf
                },
                url: config.routes.ajax,
                data: {
                    do: 'delete-skill-from-job-position',
                    skillId: skillId,
                    jobPositionId: jobPositionId
                },
                dataType: 'json',
                success: function (response) {
                    skill_label.remove()
                                    },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}
function deleteSkillNow(tag){
    var skill_label = tag
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "مهارت انتخاب شده حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله، حذف کن'
    }).then((result) => {
        if (result.isConfirmed) {
            skill_label.remove();
        }
    })
}

function deleteLang(langId,jobPositionId,tag){
    var lang_label = tag.parentNode
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "زبان انتخاب شده حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله، حذف کن'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': config.csrf
                },
                url: config.routes.ajax,
                data: {
                    do: 'delete-lang-from-job-position',
                    langId: langId,
                    jobPositionId: jobPositionId
                },
                dataType: 'json',
                success: function (response) {
                    lang_label.remove()
                                    },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}
