function submitInfo(){
    var name = document.getElementById('name').value;
    var family = document.getElementById('family').value;
    var mobile = document.getElementById('mobile').value;
    var n_code = document.getElementById('n_code').value;
    var isStudent = document.getElementById('isStudent').checked;
    var nameError = document.getElementById('nameError');
    var familyError = document.getElementById('familyError');
    var mobileError = document.getElementById('mobileError');
    var n_codeError = document.getElementById('n_codeError');

    nameError.innerHTML = '';
    familyError.innerHTML = '';
    mobileError.innerHTML = '';
    n_codeError.innerHTML = '';

    if (name == '') {
        nameError.innerHTML = 'لطفا نام خود را وارد کنید';
        return;
    }

    if (family == '') {
        familyError.innerHTML = 'نام خانوادگی خود را وارد کنید';
        return;
    }

    if (mobile == '') {
        mobileError.innerHTML = 'لطفا شماره موبایل خود را وارد کنید';
        return;
    }

    if (isNaN(mobile) || mobile.length != 11) {
        mobileError.innerHTML = 'لطفا شماره موبایل خود را به صورت صحیح وارد کنید';
        return;
    }
    if(is_student.checked){
        if (n_code == '') {
        n_codeError.innerHTML = 'لطفا کد ملی خود را وارد کنید';
        return;
        }
        if (isNaN(n_code) || n_code.length != 10) {
            n_codeError.innerHTML = 'لطفا کد ملی خود را به صورت صحیح وارد کنید';
            return;
        }
    }
    if(isStudent == true) {
        isStudent = 1;
    }else{
        isStudent = 0;
    }
    console.log(n_code);
    $.ajax({
        headers: {
            'X-CSRF-Token' : Csrf
        },
        type: "post",
        url: customRegister,
        data: {
            name : name,
            family : family,
            n_code : n_code,
            mobile : mobile,
            password : mobile,
            isStudent : isStudent,
            userId : userId,
            time_out : timeOut,
        },
        dataType: "json",
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            if(response == 'mobile_is_already_exist'){

                $('#modal').modal('hide');

                Swal.fire({
                    icon: 'warning',
                    title: 'توجه!',
                    text: 'موبایل وارد شده، در سامانه ثبت شده است. لطفا با همین شماره وارد شوید',
                    confirmButtonText: 'ورود'
                }).then((result) => {
                    if(result.isConfirmed) {
                        let form = document.getElementById('mbtiForm');
                        form.submit();
                        // window.location.href = loginUrl
                    }
                });
            }
            if(response['result'] == 'verify_mobile'){
                userId = response['userId'];
                verifyelement.classList.remove('d-none');
                formelement.classList.add('d-none');
            }

        },
        timeout:10000,
        error: function (xhr, status, error) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            if(status === "timeout") {
                timeOut = true;
                submitInfo();
            }
            console.log(error);
            if (xhr.status === 422) {
                document.getElementById('mobileError').innerHTML = xhr.responseJSON.errors.mobile;
            }
        }
    });
}

function verifyUser(){
    let verifyCode = document.getElementById('verifyMobile').value;
    var verifyMobileError = document.getElementById('verifyMobileError');
    $.ajax({
        headers: {
            'X-CSRF-Token' : Csrf
        },
        type: "post",
        url: customVerifyUser,
        data: {
            userId : userId,
            verifyCode : verifyCode,
        },
        dataType: "json",
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            if(response == true){
                let form = document.getElementById('mbtiForm');
                form.submit();
            }else{
                $('#global-loader').removeClass('d-block');
                $('#global-loader').addClass('d-none');
                $('#global-loader').css('opacity', 1);
                verifyMobileError.innerHTML = response['error'];
            }
        },
        timeout:10000,
        error: function (xhr, status, error) {
            if(status === "timeout") {
                verifyUser();
            }
            if (xhr.status === 422) {
                document.getElementById('mobileError').innerHTML = xhr.responseJSON.errors.mobile;
            }
        }
    });
}
