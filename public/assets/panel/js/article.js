const deleteurl = document.getElementById('deleteurl').value;
const rankurl = document.getElementById('rankurl').value;

function deleteCategory(category) {
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این دسته بندی حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: deleteurl,
                data: {
                    category: category,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'دسته بندی حذف شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    Swal.fire(
                        'ناموفق!',
                        'امکان حذف وجود ندارد',
                        'error',
                    )
                }
            });


        }
    })
}


function Raising_rank_category(category,rank) {
    var text;
    if(rank == 0){
        text = 'این دسته بندی ویژه خواهد شد';
    }
    else if(rank == 1){
        text = 'این دسته بندی عادی خواهد شد';
    }

    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: text,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: rankurl,
                data: {
                    category: category,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'دسته بندی ویرایش شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    Swal.fire(
                        'ناموفق!',
                        'خطایی رخ داد',
                        'error',
                    )
                }
            });


        }
    })
}

function Raising_rank_article(article,rank) {
    var text;
    if(rank == 0){
        text = 'این مقاله ویژه خواهد شد';
    }
    else if(rank == 1){
        text = 'این مقاله عادی خواهد شد';
    }

    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: text,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: ArticleUrl,
                data: {
                    article: article,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'مقاله ویرایش شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    Swal.fire(
                        'ناموفق!',
                        'خطایی رخ داد',
                        'error',
                    )
                }
            });


        }
    })
}


