function getEventInfos()
{
    if($('#event').val() == 'all'){
        $('#eventInfo').html('');
        $('#eventInfo').attr('disabled' , true);

        
    } else {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': myCsrf
            },
            type: 'POST',
            url: $('#getEventInfos').val(),
            data: {
                event_id: $('#event').val()
            },
            dataType: "json",
            beforeSend: function(){
                $('#global-loader').removeClass('d-none');
                $('#global-loader').addClass('d-block');
                $('#global-loader').css('opacity', 0.6);
            },
            success: function (data) {
                $('#global-loader').removeClass('d-block');
                $('#global-loader').addClass('d-none');
                $('#global-loader').css('opacity', 1);

                
                $('#eventInfo').attr('disabled' , false);
                $('#eventInfo').html('');
                $('#eventInfo').append(
                    '<option value="all"> همه موارد </option>'
                );
                
                data.forEach( function(info) {
                    $('#eventInfo').append(
                        '<option value="'+ info['id'] +'">'+ info['persion_date']+' '+ info['sans']['time'] +' | '+ info['plan']['title'] +' با قیمت '+ info['plan']['price'] +' تومان </option>'
                    );
                })
                
            },
            error: function (data) {
                console.log(data.error)
            }
    
        })
    }
    
}

let eventInfoCounter = 1;

function addEventInfo()
{
    if($('#event').val() == 'all'){

        $('#selectedEventInfo').html('');

        $('#selectedEventInfo').append(
            `<label class="selectgroup-item" id="allEvent">
                <span class="selectgroup-button"> تمام رویداد ها
                    <span class="text-sm text-danger" onclick="deleteLable(this)">X</span>
                </span>
            </label>`
        );

        $('#event_input').val(1);

    } else {

        $('#allEvent').remove();

        $('#event_input').val(0);

        if($('#eventInfo').val() == 'all'){

            allEventInfosInputs = '';

            $("#eventInfo option").each(function(){

                if($(this).val() != 'all'){
                    allEventInfosInputs += `<input type="hidden" name="eventInfoId${eventInfoCounter}" value="`+ $(this).val() +`">`

                    eventInfoCounter++;
                }
                
            });

            $('#selectedEventInfo').append(
                `<label class="selectgroup-item"> `+ allEventInfosInputs +`
                    <span class="selectgroup-button"> همه موارد رویداد `+ $('#event option:selected').text() +`
                        <span class="text-sm text-danger" onclick="deleteLable(this)">X</span>
                    </span>
                </label>`
            );

            $('#eventInfo').html('');
            $('#eventInfo').attr('disabled' , true);

            $('#event option:selected').remove();
            
        } else {

            $('#selectedEventInfo').append(
                `<label class="selectgroup-item">
                    <input type="hidden" name="eventInfoId${eventInfoCounter}" value="`+ $('#eventInfo').val() +`">
                    <span class="selectgroup-button">`+ $('#eventInfo option:selected').text() +`
                        <span class="text-sm text-danger" onclick="deleteLable(this)">X</span>
                    </span>
                </label>`
            );
        
            var selectedValue = $('#eventInfo').val();
        
            $("#eventInfo option[value='" + selectedValue + "']").remove();
            eventInfoCounter++;

        }
    }
    

}

function getSadraPlans()
{
    if($('#sadra').val() == 'all'){
        $('#sadraPlan').html('');
        $('#sadraPlan').attr('disabled' , true);

    } else {

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': myCsrf
            },
            type: 'POST',
            url: $('#getSadraPlans').val(),
            data: {
                sadra_id: $('#sadra').val()
            },
            dataType: "json",
            beforeSend: function(){
                $('#global-loader').removeClass('d-none');
                $('#global-loader').addClass('d-block');
                $('#global-loader').css('opacity', 0.6);
            },
            success: function (data) {
                $('#global-loader').removeClass('d-block');
                $('#global-loader').addClass('d-none');
                $('#global-loader').css('opacity', 1);
                
                $('#sadraPlan').attr('disabled' , false);
                $('#sadraPlan').html('');
                $('#sadraPlan').append(
                    '<option value="all"> همه موارد </option>'
                );

                data.forEach( function(plan) {
                    $('#sadraPlan').append(
                        '<option value="'+ plan['id'] +'">'+ plan['name'] +' با قمیت '+ plan['price'] +' تومان </option>'
                    );
                })
                
            },
            error: function (data) {
                console.log(data.error)
            }
    
        })

    }
}

let sadraPlanCounter = 1;

function addSadraPlan()
{
    if($('#sadra').val() == 'all'){

        $('#selectedSadraPlans').html('');

        $('#selectedSadraPlans').append(
            `<label class="selectgroup-item" id="allSadra">
                <span class="selectgroup-button"> تمام صدرا ها
                    <span class="text-sm text-danger" onclick="deleteLable(this)">X</span>
                </span>
            </label>`
        );

        $('#sadra_input').val(1);

    } else {

        $('#allSadra').remove();

        $('#sadra_input').val(0);

        if($('#sadraPlan').val() == 'all'){

            allSadraPlansInputs = '';

            $('#sadraPlan option').each(function() {

                if($(this).val() != 'all'){

                    allEventInfosInputs += `<input type="hidden" name="sadraPlanId${sadraPlanCounter}" value="`+ $(this).val() +`">`

                    sadraPlanCounter++;
                    
                }

            });

            $('#selectedSadraPlans').append(
                `<label class="selectgroup-item"> `+ allEventInfosInputs +`
                    <span class="selectgroup-button"> همه موارد صدرا `+ $('#sadra option:selected').text() +`
                        <span class="text-sm text-danger" onclick="deleteLable(this)">X</span>
                    </span>
                    
                </label>`
            );

            $('#sadraPlan').html('');
            $('#sadraPlan').attr('disabled' , true);

            $('#sadra option:selected').remove();

        } else {

            $('#selectedSadraPlans').append(
                `<label class="selectgroup-item">
                    <input type="hidden" name="sadraPlanId${sadraPlanCounter}" value="`+ $('#sadraPlan').val() +`">
                    <span class="selectgroup-button">`+ $('#sadraPlan option:selected').text() +`
                        <span class="text-sm text-danger" onclick="deleteLable(this)">X</span>
                    </span>
                    
                </label>`
            );
        
            var selectedValue = $('#sadraPlan').val();
        
            $("#sadraPlan option[value='" + selectedValue + "']").remove();
            sadraPlanCounter++;

        }

    }
    
}

function deleteLable(element)
{
    if(element.parentElement.parentElement.id == 'allEvent'){
        $('#event_input').val(0);
    }
    if(element.parentElement.parentElement.id == 'allSadra'){
        $('#sadra_input').val(0);
    }
    element.parentElement.parentElement.remove();
}