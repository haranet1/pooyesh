document.addEventListener("DOMContentLoaded" , function(){
    mbtiChart();
});
document.querySelector('#grade').addEventListener('change', function() {
    searchPersonalityTest();
});
document.querySelector('#major').addEventListener('change', function() {
    searchPersonalityTest();
});

function searchPersonalityTest(){
    let result = {
        grade : "*",
        major : "*",
    };
    let gradeVar = document.getElementById('grade').value;
    let majorVar = document.getElementById('major').value;

    if(gradeVar != 0){
        result.grade = gradeVar;
    }
    if(majorVar != 0){
        result.major = majorVar;
    }

    // if(gradeVar != 0 || majorVar != 0){
        
    // }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': csrf 
        },
        url: searchUrl,
        data: { 
            resultObj: result,
        },
        dataType: 'json',
            success: function (response) {
                y = response[0]
                x = response[1]
                makeChart(y , x);
                
        },
        error: function (response) {
            
            console.log(response);
        }
    });
    
}


function makeChart(mbtiType , mbtiCountOfType) {
    let x = mbtiCountOfType;
    let y = mbtiType;
    let thisChart = document.querySelector("#mbtiChart")
    thisChart.innerHTML = "";
    console.log(y);
        var options = {
            series: [{
                data: x,
            }],
            chart: {
                offsetX: -20,
                offsetY: 20,
                height: 350,
                type: 'bar',
                events: {
                    click: function(chart, w, e) {
                    // console.log(chart, w, e)
                    }
                },
            },
            series: [
                {
                    name: 'تعداد',
                    data: x
                }
            ],
            // colors: colors,
            plotOptions: {
                bar: {
                    columnWidth: '45%',
                    distributed: true,
                    // horizontal: true,
                    dataLabels:{
                        position: 'center',
                    }
                }
            },
            dataLabels: {
                enabled: true
            },
            legend: {
                show: false
            },
            xaxis: {
                categories: y,
                labels: {
                    style: {
                    //   colors: colors,
                    fontSize: '16px'
                    }
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        show: false,
                    }
                }
            }],
    };

    var chart = new ApexCharts(document.querySelector("#mbtiChart"), options);
    chart.render();
}