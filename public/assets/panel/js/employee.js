const myCsrf = $('meta[name="_token"]').attr('content');

function confirmUser(id) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        type: 'POST',
        url: config.routes.ajax,
        data: {
            do: 'confirm-user',
            id: id
        },
        success: function (data) {
            console.log(data)
            Swal.fire({
                icon: 'success',
                title: 'کاربر با موفقیت تایید شد',
                showConfirmButton: true,
            })
            location.reload()
        },
        error: function (data) {
            console.log(data.error)
        }

    })
}

function selectAll() {
    users = []
    let myCheck = $('#selectAll')
    if (myCheck.is(':checked')) {
        $(':checkbox.checkbox').each(function () {
            this.checked = true;
            users.push(this.id)
        });

    } else {
        $(':checkbox.checkbox').each(function () {
            this.checked = false;
            users.pop(this.id)
        });
    }
}

function checkUncheck(checkbox){
    if (checkbox.checked){
        users.push(checkbox.id)
    }else{
        let indexToRemove = users.indexOf(checkbox.id)
        if (indexToRemove !== undefined) {
            users.splice(indexToRemove, 1);
        }
    }
}

function confirmSelectedUsers() {
    if (users.length > 0) {
        Swal.fire({
            title: 'آیا مطمئن هستید؟',
            text: "تمام کاربران انتخاب شده تایید خواهند شد!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#198f20',
            cancelButtonColor: '#d33',
            cancelButtonText: 'انصراف',
            confirmButtonText: 'بله، تایید کن'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': myCsrf
                    },
                    url: config.routes.ajax,
                    data: {
                        do: 'confirm-all-users',
                        users: users,
                    },
                    dataType: 'json',
                    success: function (response) {
                        Swal.fire(
                            'موفق!',
                            'کاربران انتخاب شده تایید شدند',
                            'success'
                        )
                        location.reload()
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            }
        })
    } else {
        Swal.fire({
            icon: 'error',
            title: 'خطا !',
            text: 'هیچ کاربری انتخاب نشده است',
        })
    }
}

async function checkPooyeshCertificate(request_id) {
    try {
        const response = await $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': myCsrf
            },
            url: config.routes.ajax,
            data: {
                do: 'check-pooyesh-certificate',
                id: request_id
            },
            dataType: 'json'
        });

        return response;
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function sendPooyeshCertificateToStd(request_id) {
    const result = await checkPooyeshCertificate(request_id);
    if (result === 0) {
        Swal.fire({
            title: 'آیا مطمئن هستید؟',
            text: "گواهی اتمام کارآموزی برای این درخواست صادر خواهد شد!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#198f20',
            cancelButtonColor: '#d33',
            cancelButtonText: 'انصراف',
            confirmButtonText: 'بله، صادر کن'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': myCsrf
                    },
                    url: config.routes.ajax,
                    data: {
                        do: 'send-std-pooyesh-certificate',
                        id: request_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        if (response == 1) {
                            Swal.fire(
                                'موفق!',
                                'گواهی با موفقیت صادر شد',
                                'success'
                            )
                            location.reload()
                        }
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            }
        })
    } else {
        Swal.fire({
            icon: 'error',
            title: 'خطا !',
            text: 'برای این درخواست قبلا گواهی صادر شده است.',
        })
    }

}

function deleteSadraEvent(sadra) {
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این رویداد حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.delete_sadra,
                data: {
                    sadra: sadra,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'رویداد حذف شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    Swal.fire(
                        'ناموفق!',
                        'امکان حذف وجود ندارد',
                        'error',
                    )
                }
            });


        }
    })
}

function deleteDiscount(discount) {
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این کد تخفیف حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: $('#destroyDiscountUrl').val(),
                data: {
                    discount: discount,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'کد تخفیف حذف شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });
        }
    })
}

function addSadraBooth(plan_id) {
    const {value: text} = Swal.fire({
        title: 'نام غرفه را وارد کنید',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'ذخیره',
        cancelButtonText: 'انصراف',
        showLoaderOnConfirm: true,
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.isConfirmed) {
            const name = Swal.getInput()
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.ajax,
                data: {
                    do: 'add-booth',
                    name: name.value,
                    plan_id: plan_id
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'غرفه اضافه شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });
        }
    })
}

function confirmSadraReg(req_id) {
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "درخواست ثبت نام تایید خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.ajax,
                data: {
                    do:'confirm-sadra-registration',
                    req_id: req_id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'درخواست  ثبت نام تایید شد',
                        'success'
                    );
                    location.reload();
                },
                error: function (response) {
                    console.log(response);
                }
            });


        }
    });
}

function declineSadraReg(req_id) {
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "درخواست ثبت نام رد خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.ajax,
                data: {
                    do:'decline-sadra-registration',
                    req_id: req_id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'درخواست  ثبت نام رد شد',
                        'success'
                    );
                    location.reload();
                },
                error: function (response) {
                    console.log(response);
                }
            });


        }
    });
}

function checkPayment(req_id) {
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.ajax,
        data: {
            do:'check-payment',
            req_id: req_id,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            if (response['message']==true)
                Swal.fire(
                    'پرداخت شده',
                    'هزینه درخواست توسط کاربر پرداخت شده است.',
                    'success'
                );
            else
                Swal.fire(
                    'پرداخت نشده',
                    'کاربر هنوز پرداخت انجام نداده است',
                    'error'
                );
            // location.reload();
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function printPooyeshContract(req_id){
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "قرار داد چاپ خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed){
            $.ajax({
                type: "POST",
                headers:{
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.ajax,
                data:{
                    do: 'print-pooyesh-contract',
                    req_id: req_id,
                },
                dataType: 'json',
                success: function (response){
                    Swal.fire(
                        'موفق!',
                        'قرارداد درخواست پویش چاپ شد',
                        'success'
                    )
                    location.reload()
                },error: function (response){
                    console.log(response)
                }
            })
        }
    })
}

function acceptPooyeshReq(req_id){
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "درخواست قبول خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.ajax,
                data: {
                    do:'accept-pooyesh-request',
                    req_id: req_id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'درخواست  پویش تایید و قرارداد برای طرفین ارسال شد.',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });

        }
    })
}

function rejectPooyeshReq(req_id){
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "درخواست رد خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.ajax,
                data: {
                    do:'reject-pooyesh-request',
                    req_id: req_id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'درخواست  پویش رد شد.',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });

        }
    })
}

function deleteFile(id){
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "فایل انتخابی حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.ajax,
                data: {
                    do:'delete-file',
                    id: id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'فایل  با موفقیت حذف شد.',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });

        }
    })

}

function deleteAnnouncement(id){
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "اطلاعیه انتخابی حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.ajax,
                data: {
                    do:'delete-announcement',
                    id: id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'اطلاعیه  با موفقیت حذف شد.',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                }
            });

        }
    })

}

function search(){
        var table = document.getElementById('table')
        var key = $('#search').val()

        $('#table tr').remove()
        table.innerHTML = '<tr>\n' +
            '                            <th>فرستنده</th>\n' +
            '                            <th>گیرنده </th>\n' +
            '                            <th>وضعیت اولیه </th>\n' +
            '                            <th>تاریخ </th>\n' +
            '                            <th>عملکردها</th>\n' +
            '                        </tr>'


        $.ajax({
            headers: {
                'X-CSRF-TOKEN':myCsrf
            },
            type: 'POST',
            url: config.routes.search_ajax,
            data: {
                do: 'search-pooyesh-req-by-sender',
                key: key
            },
            success: function (response) {
                // console.log(response)
                response.forEach(function (re) {
                    // var route = "{{route('candidate.single',['id' => ':id'])}}"
                    // route = route.replace(':id', re['id'])
                    var row = table.insertRow(-1);
                    var td0 = row.insertCell(0)
                    var td1 = row.insertCell(1)
                    var td2 = row.insertCell(2)
                    var td3 = row.insertCell(3)
                    var td4 = row.insertCell(4)

                    if (re['sender_type'] == 'std'){
                        td0.innerHTML = '<span class="badge bg-info">کارجو</span>'+re['sender']['name']+" "+re['sender']['family']
                    }else if(re['sender_type'] == 'co'){
                        td0.innerHTML = '<span class="badge bg-warning">شرکت</span>'+ re['sender']['groupable']['name']
                    }

                    if (re['receiver_type'] == 'std'){
                        td1.innerHTML = '<span class="badge bg-info">کارجو</span>'+re['receiver']['name']+" "+re['receiver']['family']
                    }else if(re['receiver_type'] == 'co'){
                        td1.innerHTML = '<span class="badge bg-warning">شرکت</span>'+ re['receiver']['groupable']['name']
                    }

                    if (re['status'] == null){
                        td2.innerHTML = '<span class="badge bg-primary"> در انتظار بررسی توسط شرکت</span>'
                    }else if(re['status'] == 1){
                        td2.innerHTML = ' <span class="badge bg-success">قبول شده</span>'
                    }else if(re['status'] == 0){
                        td2.innerHTML ='<span class="badge bg-danger"> رد شده </span>'
                    }

                    td3.innerHTML= re['created_at']

                    td4.innerHTML = '<div class="btn-group align-top">' +
                        ' <a href="" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل  </a> </div>'
                })
            },
            error: function (data) {
                console.log(data)
            }
        })
}

// function confirmJobPosition(id) {
//     $.ajax({
//         headers: {
//             'X-CSRF-TOKEN': myCsrf
//         },
//         type: 'POST',
//         url: config.routes.ajax,
//         data: {
//             do: 'confirm-job-position',
//             id: id
//         },
//         success: function (data) {
//             console.log(data)
//             Swal.fire({
//                 icon: 'success',
//                 title: 'موقعیت شغلی با موفقیت تایید شد',
//                 showConfirmButton: true,
//             })
//             location.reload()
//         },
//         error: function (data) {
//             console.log(data.error)
//         }

//     })
// }

function confirmSelectedJobPositions() {
    if (users.length > 0) {
        Swal.fire({
            title: 'آیا مطمئن هستید؟',
            text: "تمام موقعیت های شغلی انتخاب شده تایید خواهند شد!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#198f20',
            cancelButtonColor: '#d33',
            cancelButtonText: 'انصراف',
            confirmButtonText: 'بله، تایید کن'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': myCsrf
                    },
                    url: config.routes.ajax,
                    data: {
                        do: 'confirm-all-job-positions',
                        jobs: users,
                    },
                    dataType: 'json',
                    success: function (response) {
                        Swal.fire(
                            'موفق!',
                            'موقعیت های شغلی انتخاب شده تایید شدند',
                            'success'
                        )
                        location.reload()
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            }
        })
    } else {
        Swal.fire({
            icon: 'error',
            title: 'خطا !',
            text: 'هیچ موقعیت شغلی انتخاب نشده است',
        })
    }
}

function assignToCompany(id){
    Swal.fire({
        title: 'انتصاب کاربر به نقش شرکت',
        text: "با انجام انتصاب اطلاعات نقش قبلی کاربر حذف می‌شود!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.assign_to_company,
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'انتصاب نقش به شرکت انجام شد',
                        'success'
                    )
                    console.log(response);
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

function assignToStudent(id){
    Swal.fire({
        title: 'انتصاب کاربر به نقش دانشجو',
        text: "با انجام انتصاب اطلاعات نقش قبلی کاربر حذف می‌شود!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.assign_to_student,
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'انتصاب نقش به دانشجو انجام شد',
                        'success'
                    )
                    console.log(response);
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

function assignToApplicant(id){
    Swal.fire({
        title: 'انتصاب کاربر به نقش کارجو',
        text: "با انجام انتصاب اطلاعات نقش قبلی کاربر حذف می‌شود!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.assign_to_applicant,
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'انتصاب نقش به کارجو انجام شد',
                        'success'
                    )
                    console.log(response);
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

function unConfirmedCompanySearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        // 'province' : '*',
        'coname': '*',
        'workfield': '*',
    }
    let name = document.getElementById('un_co_name_search').value;
    let family = document.getElementById('un_co_family_search').value;
    let mobile = document.getElementById('un_co_mobile_search').value;
    // let province = document.getElementById('un_co_province_search').value;
    let coname = document.getElementById('un_co_coname_search').value;
    let workfield = document.getElementById('un_co_workfield_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    // if(province != ''){
    //     obj.province = province;
    // }
    if(coname != ''){
        obj.coname = coname;
    }
    if(workfield != ''){
        obj.workfield = workfield;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.un_co_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            console.log(response);
            var companies = response;
            var tableBody = document.getElementById('un-company-table-body');
            tableBody.innerHTML = ''; // Clear existing table data
            companies.forEach(function (company , index) {
                var rowNumber = index + 1;
                var coroute = companySingle.replace(':id', company['id']);
                var rowHtml = '<tr>' +
                    '<td><input type="checkbox" id="' + company['id'] + '" class="checkbox" onclick="checkUncheck(this)"></td>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    '<td class="align-middle text-center"><img alt="image" class="avatar avatar-md br-7" src="' + (company['logo'] ? company['logo']['path'] : imageUrl) + '"></td>' +
                    '<td class="text-nowrap align-middle">' + company['company_infos']['name'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['name'] + ' ' + company['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['company_infos']['ceo'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['company_infos']['activity_field'] + '</td>' +
                    '<td class="text-center align-middle">' +
                    '<div class="btn-group align-top">' +
                    '<a href="' + coroute + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>' +
                    '<a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser(' + company['id'] + ')">تایید </a>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });



}

function unConfirmedApplicantSearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        'sex': '*',
        'province' : '*',
        'major': '*',
    }
    let name = document.getElementById('un_app_name_search').value;
    let family = document.getElementById('un_app_family_search').value;
    let mobile = document.getElementById('un_app_mobile_search').value;
    let sex = document.getElementById('un_app_sex_search').value;
    let province = document.getElementById('un_app_province_search').value;
    let major = document.getElementById('un_app_major_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(sex != '0'){
        obj.sex = sex;
    }
    if(province != ''){
        obj.province = province;
    }
    if(major != ''){
        obj.major = major;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.un_app_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            console.log(response);
            var applicants = response;
            var tableBody = document.getElementById('un-applicant-table-body');
            tableBody.innerHTML = ''; // Clear existing table data

            applicants.forEach(function (applicant , index) {
                var rowNumber = index + 1;
                var approute = appSingle.replace(':id', applicant['id']);
                let major = '';
                // let university = '';
                let province = '';
                let sex = '';
                if(applicant['sex'] == 'male') {
                    sex = 'مرد';
                }else if(applicant['sex'] == 'female'){
                    sex = 'زن';
                }else{
                    sex = 'ثبت نشده';
                }
                if(applicant['academic_infos']['province'] && applicant['academic_infos']['province'].length > 0){
                    province = applicant['academic_infos']['province']['name'];
                }else{
                    province = 'ثبت نشده';
                }
                if(applicant['academic_infos'] && applicant['academic_infos'].length > 0){
                    // university = applicant['academic_infos']['university']['name'];
                    major = applicant['academic_infos']['major'];
                }else{
                    university = 'ثبت نشده';
                    major = 'ثبت نشده';
                }
                var rowHtml = '<tr>' +
                    '<td><input type="checkbox" id="' + applicant['id'] + '" class="checkbox" onclick="checkUncheck(this)"></td>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    '<td class="text-nowrap align-middle">' + applicant['name'] + ' ' + applicant['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + applicant['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + applicant['age'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + sex + '</td>' +
                    '<td class="text-nowrap align-middle">' + province + '</td>' +
                    // '<td class="text-nowrap align-middle">' + university + '</td>' +
                    '<td class="text-nowrap align-middle">' + major + '</td>' +
                    '<td class="text-center align-middle">' +
                    '<div class="btn-group align-top">' +
                    '<a href="' + approute + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>' +
                    '<a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser(' + applicant['id'] + ')">تایید </a>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });
}

function unConfirmedStudentSearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        'sex': '*',
        'province' : '*',
        'university' : '*',
        'major': '*',
    }
    let name = document.getElementById('un_std_name_search').value;
    let family = document.getElementById('un_std_family_search').value;
    let mobile = document.getElementById('un_std_mobile_search').value;
    let sex = document.getElementById('un_std_sex_search').value;
    let province = document.getElementById('un_std_province_search').value;
    let university = document.getElementById('un_std_uni_search').value;
    let major = document.getElementById('un_std_major_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(sex != '0'){
        obj.sex = sex;
    }
    if(province != ''){
        obj.province = province;
    }
    if(university != ''){
        obj.university = university;
    }
    if(major != ''){
        obj.major = major;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.un_std_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            console.log(response);
            var students = response;
            var tableBody = document.getElementById('un-student-table-body');
            tableBody.innerHTML = ''; // Clear existing table data

            students.forEach(function (student , index) {
                var rowNumber = index + 1;
                var stdroute = stdSingle.replace(':id', student['id']);
                let major = '';
                let university = '';
                let province = '';
                let sex = '';
                let age = '';
                if(student['sex'] == 'male') {
                    sex = 'مرد';
                }else if(student['sex'] == 'female'){
                    sex = 'زن';
                }else{
                    sex = 'ثبت نشده';
                }
                if(student['province']){
                    province = student['province']['name'];
                }else{
                    province = 'ثبت نشده';
                }
                if(student['student_infos']['university']){
                    university = student['student_infos']['university']['name'];
                }else{
                    university = 'ثبت نشده';
                }
                if(student['student_infos']){
                    age = student['student_infos']['age'];
                    major = student['student_infos']['major'];
                }else{
                    age = 'ثبت نشده';
                    major = 'ثبت نشده';
                }
                var rowHtml = '<tr>' +
                    '<td><input type="checkbox" id="' + student['id'] + '" class="checkbox" onclick="checkUncheck(this)"></td>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    '<td class="text-nowrap align-middle">' + student['name'] + ' ' + student['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + student['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + age + '</td>' +
                    '<td class="text-nowrap align-middle">' + sex + '</td>' +
                    '<td class="text-nowrap align-middle">' + province + '</td>' +
                    '<td class="text-nowrap align-middle">' + university + '</td>' +
                    '<td class="text-nowrap align-middle">' + major + '</td>' +
                    '<td class="text-center align-middle">' +
                    '<div class="btn-group align-top">' +
                    '<a href="' + stdroute + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>' +
                    '<a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser(' + student['id'] + ')">تایید </a>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });



}

function ConfirmedCompanySearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        // 'province' : '*',
        'coname': '*',
        'workfield': '*',
    }
    let name = document.getElementById('co_name_search').value;
    let family = document.getElementById('co_family_search').value;
    let mobile = document.getElementById('co_mobile_search').value;
    // let province = document.getElementById('co_province_search').value;
    let coname = document.getElementById('co_coname_search').value;
    let workfield = document.getElementById('co_workfield_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    // if(province != ''){
    //     obj.province = province;
    // }
    if(coname != ''){
        obj.coname = coname;
    }
    if(workfield != ''){
        obj.workfield = workfield;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.co_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            var companies = response;
            var tableBody = document.getElementById('company-table-body');
            tableBody.innerHTML = ''; // Clear existing table data

            companies.forEach(function (company , index) {
                var rowNumber = index + 1;
                var coroute = c_companySingle.replace(':id', company['id']);
                var rowHtml = '<tr>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    '<td class="align-middle text-center"><img alt="image" class="avatar avatar-md br-7" src="' + (company['logo'] ? company['logo']['path'] : imageUrl) + '"></td>' +
                    '<td class="text-nowrap align-middle">' + company['company_infos']['name'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['name'] + ' ' + company['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['company_infos']['ceo'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['company_infos']['activity_field'] + '</td>' +
                    '<td class="text-center align-middle">' +
                    '<div class="btn-group align-top">' +
                    '<a href="' + coroute + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>' +
                    '<a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser(' + company['id'] + ')">تایید </a>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });



}

function ConfirmedApplicantSearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        'sex': '*',
        'province' : '*',
        'major': '*',
    }
    let name = document.getElementById('app_name_search').value;
    let family = document.getElementById('app_family_search').value;
    let mobile = document.getElementById('app_mobile_search').value;
    let sex = document.getElementById('app_sex_search').value;
    let province = document.getElementById('app_province_search').value;
    let major = document.getElementById('app_major_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(sex != '0'){
        obj.sex = sex;
    }
    if(province != ''){
        obj.province = province;
    }
    if(major != ''){
        obj.major = major;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.app_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            var applicants = response;
            var tableBody = document.getElementById('applicant-table-body');
            tableBody.innerHTML = ''; // Clear existing table data

            applicants.forEach(function (applicant ,index) {
                var rowNumber = index + 1;
                var approute = c_appSingle.replace(':id', applicant['id']);
                let major = '';
                // let university = '';
                let province = '';
                let sex = '';
                if(applicant['sex'] == 'male') {
                    sex = 'مرد';
                }else if(applicant['sex'] == 'female'){
                    sex = 'زن';
                }else{
                    sex = 'ثبت نشده';
                }
                if(applicant['academic_infos']['province'] && applicant['academic_infos']['province'].length > 0){
                    province = applicant['applicant_infos']['province']['name'];
                }else{
                    province = 'ثبت نشده';
                }
                if(applicant['academic_infos'] && applicant['academic_infos'].length > 0){
                    // university = applicant['academic_infos']['university']['name'];
                    major = applicant['academic_infos'][applicant['academic_infos'].length-1]['major'];
                }else{
                    // university = 'ثبت نشده';
                    major = 'ثبت نشده';
                }
                var rowHtml = '<tr>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    '<td class="text-nowrap align-middle">' + applicant['name'] + ' ' + applicant['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + applicant['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + applicant['age'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + sex + '</td>' +
                    '<td class="text-nowrap align-middle">' + province + '</td>' +
                    // '<td class="text-nowrap align-middle">' + university + '</td>' +
                    '<td class="text-nowrap align-middle">' + major + '</td>' +
                    '<td class="text-center align-middle">' +
                    '<div class="btn-group align-top">' +
                    '<a href="' + approute + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });



}

function ConfirmedStudentSearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        'sex': '*',
        'province' : '*',
        'university' : '*',
        'major': '*',
    }
    let name = document.getElementById('std_name_search').value;
    let family = document.getElementById('std_family_search').value;
    let mobile = document.getElementById('std_mobile_search').value;
    let sex = document.getElementById('std_sex_search').value;
    let province = document.getElementById('std_province_search').value;
    let university = document.getElementById('std_uni_search').value;
    let major = document.getElementById('std_major_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(sex != '0'){
        obj.sex = sex;
    }
    if(province != ''){
        obj.province = province;
    }
    if(university != ''){
        obj.university = university;
    }
    if(major != ''){
        obj.major = major;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.std_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            var students = response;
            var tableBody = document.getElementById('student-table-body');
            tableBody.innerHTML = ''; // Clear existing table data

            students.forEach(function (student , index) {
                var rowNumber = index + 1 ;
                var stdroute = c_stdSingle.replace(':id', student['id']);
                let major = '';
                let university = '';
                let province = '';
                let sex = '';
                let age = '';
                if(student['sex'] == 'male') {
                    sex = 'مرد';
                }else if(student['sex'] == 'female'){
                    sex = 'زن';
                }else{
                    sex = 'ثبت نشده';
                }
                if(student['student_infos']['province']){
                    province = student['student_infos']['province']['name'];
                }else{
                    province = 'ثبت نشده';
                }
                if(student['student_infos']['university']){
                    university = student['student_infos']['university']['name'];
                }else{
                    university = 'ثبت نشده';
                }
                if(student['student_infos']){
                    age = student['student_infos']['age'];
                    major = student['student_infos']['major'];
                }else{
                    age = 'ثبت نشده';
                    major = 'ثبت نشده';
                }
                var rowHtml = '<tr>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    '<td class="text-nowrap align-middle">' + student['name'] + ' ' + student['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + student['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + age + '</td>' +
                    '<td class="text-nowrap align-middle">' + sex + '</td>' +
                    '<td class="text-nowrap align-middle">' + province + '</td>' +
                    '<td class="text-nowrap align-middle">' + university + '</td>' +
                    '<td class="text-nowrap align-middle">' + major + '</td>' +
                    '<td class="text-center align-middle">' +
                    '<div class="btn-group align-top">' +
                    '<a href="' + stdroute + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });



}

function importedCompanySearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        // 'province' : '*',
        // 'coname': '*',
        // 'workfield': '*',
    }
    let name = document.getElementById('im_co_name_search').value;
    let family = document.getElementById('im_co_family_search').value;
    let mobile = document.getElementById('im_co_mobile_search').value;
    // let province = document.getElementById('im_co_province_search').value;
    // let coname = document.getElementById('im_co_coname_search').value;
    // let workfield = document.getElementById('im_co_workfield_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    // if(province != ''){
    //     obj.province = province;
    // }
    // if(coname != ''){
    //     obj.coname = coname;
    // }
    // if(workfield != ''){
    //     obj.workfield = workfield;
    // }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.im_co_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },

        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            var companies = response.data;
            var tableBody = document.getElementById('im_company-table-body');
            tableBody.innerHTML = ''; // Clear existing table data

            companies.forEach(function (company, index) {
                var rowNumber = index +1;
                // var coroute = im_companySingle.replace(':id', company['id']);
                var rowHtml = '<tr>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    // '<td class="text-nowrap align-middle">' + company['company_infos']['name'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['name'] + ' ' + company['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + company['mobile'] + '</td>' +
                    // '<td class="text-nowrap align-middle">' + company['company_infos']['ceo'] + '</td>' +
                    // '<td class="text-nowrap align-middle">' + company['company_infos']['activity_field'] + '</td>' +
                    // '<td class="text-center align-middle">' +
                    // '<div class="btn-group align-top">' +
                    // '<a href="' + coroute + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>' +
                    // '<a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser(' + company['id'] + ')">تایید </a>' +
                    // '</div>' +
                    // '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });
}

function importedApplicantSearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
    }
    let name = document.getElementById('im_app_name_search').value;
    let family = document.getElementById('im_app_family_search').value;
    let mobile = document.getElementById('im_app_mobile_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.im_app_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            var applicants = response.data;
            var tableBody = document.getElementById('im_applicant-table-body');
            tableBody.innerHTML = ''; // Clear existing table data
            applicants.forEach(function (applicant , index) {
                var rowNumber = index + 1;
                let sex = 'ثبت نشده';
                let email = 'ثبت نشده';
                if(applicant['sex']){
                    sex = applicant['sex'];
                }
                if(applicant['email']){
                    email = applicant['email'];
                }
                // var coroute = im_companySingle.replace(':id', applicant['id']);
                var rowHtml = '<tr>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    // '<td class="text-nowrap align-middle">' + applicant['company_infos']['name'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + applicant['name'] + ' ' + applicant['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + applicant['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + sex + '</td>' +
                    '<td class="text-nowrap align-middle">' + email + '</td>' +
                    // '<td class="text-center align-middle">' +
                    // '<div class="btn-group align-top">' +
                    // '<a href="' + coroute + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>' +
                    // '<a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser(' + applicant['id'] + ')">تایید </a>' +
                    // '</div>' +
                    // '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });
}

function importedStudentSearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
    }
    let name = document.getElementById('im_std_name_search').value;
    let family = document.getElementById('im_app_family_search').value;
    let mobile = document.getElementById('im_std_mobile_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.im_std_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            var students = response.data;
            var tableBody = document.getElementById('im_student-table-body');
            tableBody.innerHTML = ''; // Clear existing table data
            students.forEach(function (student , index) {
                var rowNumber = index + 1;
                let sex = 'ثبت نشده';
                let email = 'ثبت نشده';
                if(student['sex']){
                    sex = student['sex'];
                }
                if(student['email']){
                    email = student['email'];
                }
                // var coroute = im_companySingle.replace(':id', student['id']);
                var rowHtml = '<tr>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    // '<td class="text-nowrap align-middle">' + student['company_infos']['name'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + student['name'] + ' ' + student['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + student['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + sex + '</td>' +
                    '<td class="text-nowrap align-middle">' + email + '</td>' +
                    // '<td class="text-center align-middle">' +
                    // '<div class="btn-group align-top">' +
                    // '<a href="' + coroute + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>' +
                    // '<a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser(' + student['id'] + ')">تایید </a>' +
                    // '</div>' +
                    // '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });
}

function testerMbtiSearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        'sex' : '*',
        'role': '*',
    }
    let name = document.getElementById('test_mbti_name_search').value;
    let family = document.getElementById('test_mbti_family_search').value;
    let mobile = document.getElementById('test_mbti_mobile_search').value;
    let sex = document.getElementById('test_mbti_sex_search').value;
    let role = document.getElementById('test_mbti_role_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(sex != '0'){
        obj.sex = sex;
    }
    if(role != '0'){
        obj.role = role;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.test_mbti_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            var MBTIs = response;
            var tableBody = document.getElementById('test-mbti-table-body');
            tableBody.innerHTML = ''; // Clear existing table data
            MBTIs.forEach(function (mbti, index) {
                var rowNumber = index + 1;
                let sex = 'ثبت نشده';
                let email = 'ثبت نشده';
                let role = 'ثبت نشده';
                if(mbti['sex'] == 'male'){
                    sex = 'مرد';
                }else if(mbti['sex'] == 'female'){
                    sex = 'زن';
                }
                if(mbti['email']){
                    email = mbti['email'];
                }
                if(mbti['role']){
                    role = mbti['role'];
                }
                // var coroute = im_companySingle.replace(':id', student['id']);
                var rowHtml = '<tr>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    // '<td class="text-nowrap align-middle">' + student['company_infos']['name'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + mbti['name'] + ' ' + mbti['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + mbti['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + sex + '</td>' +
                    '<td class="text-nowrap align-middle">' + email + '</td>' +
                    '<td class="text-nowrap align-middle">' + role + '</td>' +
                    '<td class="text-nowrap align-middle">' + mbti['mbti_test']['personality_type'] + '</td>' +
                    '<td class="text-center align-middle">' +
                    '<div class="btn-group align-top">' +
                    '<div class="btn-group">' +
                    '<button type="button" onclick="confirmUser('+ mbti['id'] +')" class="btn btn-success">تایید</button>'+
                    '<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">' +
                    '<span class="visually-hidden">Toggle Dropdown</span>' +
                    '</button>' +
                    '<ul class="dropdown-menu">' +
                    '<li><a class="dropdown-item" onclick="assignToCompany('+ mbti['id'] +')" href="javascript:void(0)">انتصاب به نقش شرکت</a></li>' +
                    '<li><a class="dropdown-item" onclick="assignToApplicant('+ mbti['id'] +')" href="javascript:void(0)">انتصاب به نقش کارجو</a></li>' +
                    '<li><a class="dropdown-item" onclick="assignToStudent('+ mbti['id'] +')" href="javascript:void(0)">انتصاب به نقش دانشجو</a></li>' +
                    '</ul>' +
                    '</div>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });
}

function testerEnnagramSearch(){
    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        'sex' : '*',
        'role': '*',
    }
    let name = document.getElementById('test_enne_name_search').value;
    let family = document.getElementById('test_enne_family_search').value;
    let mobile = document.getElementById('test_enne_mobile_search').value;
    let sex = document.getElementById('test_enne_sex_search').value;
    let role = document.getElementById('test_enne_role_search').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(sex != '0'){
        obj.sex = sex;
    }
    if(role != '0'){
        obj.role = role;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: config.routes.test_enneagram_search,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },

        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            var enneagrams = response;
            var tableBody = document.getElementById('test-enneagram-table-body');
            tableBody.innerHTML = ''; // Clear existing table data
            enneagrams.forEach(function (enneagram, index) {
                var rowNumber = index + 1;
                let sex = 'ثبت نشده';
                let email = 'ثبت نشده';
                let role = 'ثبت نشده';
                if(enneagram['sex'] == 'male'){
                    sex = 'مرد';
                }else if(enneagram['sex'] == 'female'){
                    sex = 'زن';
                }
                if(enneagram['email']){
                    email = enneagram['email'];
                }
                if(enneagram['main_character']){
                    role = enneagram['main_character'];
                }
                // var coroute = im_companySingle.replace(':id', student['id']);
                var rowHtml = '<tr>' +
                    '<td class="text-nowrap align-middle">'+ rowNumber +'</td>' +
                    // '<td class="text-nowrap align-middle">' + student['company_infos']['name'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + enneagram['name'] + ' ' + enneagram['family'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + enneagram['mobile'] + '</td>' +
                    '<td class="text-nowrap align-middle">' + sex + '</td>' +
                    '<td class="text-nowrap align-middle">' + email + '</td>' +
                    '<td class="text-nowrap align-middle">' + role + '</td>' +
                    '<td class="text-nowrap align-middle">' + enneagram['main_character'] + '</td>' +
                    '<td class="text-center align-middle">' +
                    '<div class="btn-group align-top">' +
                    '<div class="btn-group">' +
                    '<button type="button" onclick="confirmUser('+ enneagram['id'] +')" class="btn btn-success">تایید</button>'+
                    '<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">' +
                    '<span class="visually-hidden">Toggle Dropdown</span>' +
                    '</button>' +
                    '<ul class="dropdown-menu">' +
                    '<li><a class="dropdown-item" onclick="assignToCompany('+ enneagram['id'] +')" href="javascript:void(0)">انتصاب به نقش شرکت</a></li>' +
                    '<li><a class="dropdown-item" onclick="assignToApplicant('+ enneagram['id'] +')" href="javascript:void(0)">انتصاب به نقش کارجو</a></li>' +
                    '<li><a class="dropdown-item" onclick="assignToStudent('+ enneagram['id'] +')" href="javascript:void(0)">انتصاب به نقش دانشجو</a></li>' +
                    '</ul>' +
                    '</div>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });
}


