function checkCompanyHas3InternShip(user_id , event_id)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        type: 'post',
        url: checkCompanySadraUrl,
        data: {
            user_id : user_id,
        },
        dataType: "json",
        beforeSend: function() {
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);

            if(response == 'ok'){
                
                let registerUrl = registerHref.replace(':id', event_id);
                console.log(registerUrl);
                
    
                window.location.href = registerUrl

                // let register_form = document.createElement("form");
                // register_form.setAttribute("id", "registerForm");
                // register_form.setAttribute("method", "POST");
                // register_form.setAttribute("action", registerHref);

                // let csrfTokenField = document.createElement("input");
                // csrfTokenField.setAttribute("type", "hidden");
                // csrfTokenField.setAttribute("name", "_token");
                // csrfTokenField.setAttribute("value", myCsrf);
                // register_form.appendChild(csrfTokenField);

                // let eventIdField = document.createElement("input");
                // eventIdField.setAttribute("type", "hidden");
                // eventIdField.setAttribute("name", "event_id");
                // eventIdField.setAttribute("value", event_id);
                // register_form.appendChild(eventIdField);

                // document.body.appendChild(register_form);

                // register_form.submit();

            } else {
                
                Swal.fire({
                    icon : 'warning',
                    title : 'توجه !',
                    text : 'برای شرکت در رویداد، حداقل 3 موقعیت کارآموزی ثبت شده نیاز دارید.',
                    confirmButtonText : 'ایجاد موقعیت کارآموزی',
                }).then((result)=>{
                    if(result.isConfirmed){
                        window.location.href = createInternPositionUrl;
                    }
                })

            }

        },
        error: function (response) {
            console.log(response);
        }
    })
}