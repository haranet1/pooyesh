let successRowcheck = document.getElementById('successRow');
if(! successRowcheck.classList.contains('d-none')){
    countdown();
}

function codeRequest()
{
    let verifyEmailUrl = document.getElementById('verifyEmailUrl').value;
    let warningRow = document.getElementById('warningRow');
    let successRow = document.getElementById('successRow');

    let emailInput = document.getElementById('emailInput');
    let emailErr = document.getElementById('emailErr');

    let emailBtn = document.getElementById('emailBtn');

    let sendAgainBtn = document.getElementById('sendAgainBtn');

    let timer = document.getElementById('timer');

    if(emailInput.value == '' || emailInput.value == null){
        emailErr.classList.remove('d-none' , 'text-warning');
        emailErr.classList.add('text-danger');
        emailErr.innerHTML = 'ایمیل خود را وارد کنید.'
    }else{
        setTimeout(() => {
            emailBtn.setAttribute('disabled' , '');
            emailBtn.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' + ' درحال ارسال ایمیل ... ';

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: verifyEmailUrl,
                data: {
                    email: emailInput.value,
                },
                dataType: "json",

                success: function(response){
                    setTimeout(() => {
                        if(response == 'emailHasAlreadyTaken'){
                            emailErr.classList.remove('d-none' , 'text-warning');
                            emailErr.classList.add('text-danger');
                            emailErr.innerHTML = 'ایمیل وارد شده قبلا در سامانه ثبت شده است.'
                            emailBtn.removeAttribute('disabled' , '');
                            emailBtn.innerHTML = 'ارسال ایمیل تاییدیه';

                        }else if(response == 'emailVerificationSended'){
                            warningRow.classList.add('d-none');
                            successRow.classList.remove('d-none');
                            countdown();
                        }
                        
                    }, 5000);
                },
                error: function(response){
                    console.log(response);
                }
            })
        }, 100);

        
    }
}

function formatTime(minutes, seconds) {
    return minutes.toString().padStart(2, '0') + ":" + seconds.toString().padStart(2, '0');
}

function countdown() {
    var timerDisplay = document.getElementById("timer");
    var minutes = 5;
    var seconds = 0;

    var countdownInterval = setInterval(function() {
        // نمایش زمان فعلی
        timerDisplay.textContent = formatTime(minutes, seconds);

        // کاهش زمان
        if (seconds > 0) {
            seconds--;
        } else {
            if (minutes > 0) {
                minutes--;
                seconds = 59;
            } else {
                // زمان به پایان رسیده است
                clearInterval(countdownInterval);
                sendAgainBtn.removeAttribute('disabled' , '');
            }
        }
    }, 1000);
}

function sendAgainEmail()
{
    let verifyEmailUrl = document.getElementById('verifyEmailUrl2').value;
    let emailInput = document.getElementById('emailInput');
    console.log(emailInput);

    let sendAgainBtn = document.getElementById('sendAgainBtn');

    setTimeout(() => {
        sendAgainBtn.setAttribute('disabled' , '');
        sendAgainBtn.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' + ' درحال ارسال ایمیل ... ';

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': myCsrf
            },
            type: 'POST',
            url: verifyEmailUrl,
            data: {
                email: emailInput.value,
            },
            dataType: "json",

            success: function(response){
                setTimeout(() => {
                    if(response == 'emailHasAlreadyTaken'){
                        emailErr.classList.remove('d-none' , 'text-warning');
                        emailErr.classList.add('text-danger');
                        emailErr.innerHTML = 'ایمیل وارد شده قبلا در سامانه ثبت شده است.'

                    }else if(response == 'emailVerificationSended'){
                        warningRow.classList.add('d-none'); 
                        successRow.classList.remove('d-none');
                        countdown();
                        sendAgainBtn.setAttribute('disabled' , '');
                        sendAgainBtn.innerHTML = 'ارسال مجدد ایمیل'
                    }
                    
                }, 5000);
            },
            error: function(response){
                console.log(response);
            }
        })
    }, 100);
}