const questionContainer = document.getElementById('question_container');
const answersContainer = document.getElementById('answers_container');
const prevBtn = document.querySelector(".prev-btn");
const nextBtn = document.querySelector(".next-btn");
const endBtn = document.querySelector(".btn-end");
let currentArray = 0;
const mainArray = [];

function createQuestion(){
    let questionElement = '';
    if(0 <= currentArray && currentArray <= 5){
        questionElement = `
            <h4 class="mb-0">
                انجام کدام یک از فعالیت های زیر برای شما لذت بخش هستند؟
            </h4>
        `
    }
    if(6 <= currentArray && currentArray <= 11){
        questionElement = `
            <h4 class="mb-0">
                از بین گزینه های زیر چه مواردی شامل حال شما می‌شود؟  
            </h4>
        `
    }
    if(12 <= currentArray && currentArray <= 17){
        questionElement = `
            <h4 class="mb-0">
                به نظر شما کدام یک از مشاغل زیر جذاب هستند؟
            </h4>
        `
    }
    questionContainer.innerHTML = questionElement;
}

function createAnswer(){
    for (const answer in answers){
        const elementsInAnswer = [];
        for (const element of answers[answer]) {
            const div = document.createElement("div");
            div.classList.add('row','answer-row');
            div.setAttribute('id', element.id);
            div.setAttribute('category', element.category);
            div.innerHTML = `
                <label class="custom-control custom-checkbox-md c-checkbox">
                    <input onClick="setActive(this)" type="checkbox" class="custom-control-input checkbox-input" id="${element.id}" name="${element.id}" value="${element.category}">
                    <span class="custom-control-label ms-1">${element.title}</span>
                </label>
            `;
        
            elementsInAnswer.push(div);
          }
          mainArray.push(elementsInAnswer);
    }
    
}

function addElement(){
    if(currentArray == 0){
        prevBtn.classList.add('d-none');
    }else{
        prevBtn.classList.remove('d-none');
    }
    if(currentArray <= 17){
        answersContainer.innerHTML = '';
        for (const element of mainArray[currentArray]) {
            answersContainer.appendChild(element);
        }
    }
}

// first step ----------------------------------------------------------------
window.addEventListener("DOMContentLoaded" , function(){
    createQuestion();
    createAnswer();
    addElement();
    updateProgressNumbers();
    updateProgress(currentArray + 1);
});
// next step -----------------------------------------------------------------
nextBtn.addEventListener("click", function(){
    currentArray++;
    if(currentArray == 17){
        nextBtn.classList.add('d-none');
        endBtn.classList.remove('d-none');
    }
    createQuestion();
    addElement();
    updateProgressNumbers();
    updateProgress(currentArray + 1);
    window.scroll({
        top: 0,
        behavior: "smooth",
      });
});
// previous step -------------------------------------------------------------
prevBtn.addEventListener("click", function(){
    currentArray--;
    nextBtn.classList.remove('d-none');
    endBtn.classList.add('d-none');
    createQuestion();
    addElement();
    updateProgressNumbers();
    updateProgress(currentArray + 1);
    window.scroll({
        top: 0,
        behavior: "smooth",
      });
});

// ----------------------------------------------------------------------------

function setActive(element){
    parentOfElement = element.parentElement.parentElement
    if(parentOfElement.classList.contains('active')){
        parentOfElement.classList.remove('active'); 
    }else{
        parentOfElement.classList.add('active'); 
    }
}

function finishTest(){
    let results = {
        'R' : 0,
        'I' : 0,
        'A' : 0,
        'S' : 0,
        'E' : 0,
        'C' : 0,
    };
    for(let array of mainArray){
        for(let item of array){
            if(item.classList.contains('active')){
                results[item.getAttribute('category')]+= 1;
            }
        }
        
    }
    console.log(results);
    sendRequest(results);
}

function sendRequest(results){
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': csrf 
        },
        url: haaladUrl,
        data: { 
            result: results,
        },
        dataType: 'json',
        success: function (response) {
            if(response.redirect_url){
                window.location.href = response.redirect_url;
            }
                
        },
        timeout:10000,
        error: function (xhr, status, error) {
            if(status === "timeout") {
                sendRequest();
            } 
            console.log(response);
        }
    });
}

function updateProgressNumbers() {
    var steps = currentArray + 1;
    var prog1 = $('#prog1');
    var prog2 = $('#prog2');
    
    var currentProg1 = parseInt(prog1.text());
    var currentProg2 = parseInt(prog2.text());
    
    prog1.text(steps);
    prog2.text(steps);
}


function updateProgress(step) {
    var progressBar =  $('.progress-bar');
    var width = (step / 18) * 100; // Calculate the percentage width

    progressBar.css('width', width + "%");
}






