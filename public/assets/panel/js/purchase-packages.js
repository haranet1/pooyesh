
function deletePackage(package) {
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این بسته حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: config.routes.delete_package,
                data: {
                    package: package,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'بسته حذف شد',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    Swal.fire(
                        'ناموفق!',
                        'امکان حذف وجود ندارد',
                        'error',
                    )
                }
            });


        }
    })
}

