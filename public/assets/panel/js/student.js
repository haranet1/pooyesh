
// tab 1
const AcademicInfo = [];
let Id = 0;
function addGrade() {
    document.getElementById('gradeError').innerHTML = ''
    document.getElementById('majorError').innerHTML = ''
    document.getElementById('universityError').innerHTML = ''
    document.getElementById('endError').innerHTML = ''
    document.getElementById('avgError').innerHTML = ''

    var grade = document.getElementById('grade').value;
    var major = document.getElementById('major').value;
    var university = document.getElementById('university').value;
    var avg = document.getElementById('avg').value;
    var end = document.getElementById('end').value;

    // validation errors
    if (grade == '') {
        document.getElementById('gradeError').innerHTML = 'لطفا مقطع تحصیلی خود را انتخاب کنید'
        return false;
    }
    if (major == '') {
        document.getElementById('majorError').innerHTML = 'لطفا رشته تحصیلی خود را وارد کنید'
        return false;
    }
    if (university == '') {
        document.getElementById('universityError').innerHTML = 'لطفا دانشگاه خود را وارد کنید'
        return false;
    }
    if (end == '') {
        document.getElementById('endError').innerHTML = 'لطفا سال پایان را وارد کنید'
        return false;
    }

    if (isNaN(end)) {
        document.getElementById('endError').innerHTML = 'لطفا سال پایان را به صورت صحیح وارد کنید'
        return false;

    }

    if (avg == '') {
        document.getElementById('avgError').innerHTML = 'لطفا معدل خود را وارد کنید'
        return false;
    }

    if (isNaN(avg)) {
        document.getElementById('avgError').innerHTML = 'لطفا معدل خود را به صورت صحیح وارد کنید'
        return false;

    }
    if (avg > 20) {
        document.getElementById('avgError').innerHTML = 'لطفا معدل خود را به صورت صحیح وارد کنید'
        return false;
    }

    // validation errors

    Id++;
    var obj = {
        'id': Id,
        'grade': grade,
        'major': major,
        'university': university,
        'end': end,
        'avg': avg
    }

    AcademicInfo.push(obj);
    const table = document.getElementById('gradeTb');
    const newRow = table.insertRow(-1);
    const Cell0 = newRow.insertCell(0);
    const Cell1 = newRow.insertCell(1);
    const Cell2 = newRow.insertCell(2);
    const Cell3 = newRow.insertCell(3);
    const Cell4 = newRow.insertCell(4);
    const Cell5 = newRow.insertCell(5);

    Cell0.innerHTML = grade;
    Cell1.innerHTML = major;
    Cell2.innerHTML = university;
    Cell3.innerHTML = end;
    Cell4.innerHTML = avg;
    Cell5.innerHTML = '<a href="javascript:void(0)" onClick="deleteRow(' + obj.id + ',this)" class="btn btn-danger"><i class="fa fa-trash"></i></a>'

    var btn = document.getElementById('AcademicBtn')
    btn.classList.remove('d-none');
    table.classList.remove('d-none');

    document.getElementById('major').value = ''
    document.getElementById('university').value = ''
    document.getElementById('end').value = ''
    document.getElementById('avg').value = ''
}

function deleteAcInfo(id,element) {

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'delete-academic-info',
            id: id
        },
        dataType: 'json',
        success: function (response) {


            var parent = element.parentNode
            parent = parent.parentNode
            parent.remove()
        },
        error: function (response) {
            console.log(response)
        }
    });
}


function deleteRow(id, element) {
    console.log(element)
    for (var i = 0; i < AcademicInfo.length; i++) {
        if (AcademicInfo[i].id == id) {
            AcademicInfo.splice(i, 1)
        }
    }
    var parent = element.parentNode
    parent = parent.parentNode
    parent.remove()
}

function storeAcademicInfo() {
    if (AcademicInfo.length == 0) {
        return false;
    }
    console.log(AcademicInfo)
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.academicInfoStore,
        data: {
            data: AcademicInfo
        },
        dataType: 'json',
        success: function (response) {
            AcademicInfo.splice(0);
            var btn = document.getElementById('AcademicBtn')
            Swal.fire(
                'موفق!',
                'اطلاعات آکادمیک شما ثبت شد',
                'success'
            )
            $('#tab28').removeClass('active');
            $('#tab25').addClass('active');
            $('#head-tab28').removeClass('active');
            $('#head-tab25').addClass('active');

            
        },
        error: function (response) {
            console.log(response)
        }
    });
}
// tab1
// ----------------------------------------------------------------
// tab 2

const idArray = [];
const skillArray = [];

function addSkill() {
    var selectedText = document.getElementById('skills').value;
    if (selectedText != "") {
        var list = document.getElementById('skill-list')
        var skill_id = document.getElementById('skill').value
        var element = document.createElement('label');
        var skill_level = document.getElementById('skill_level').value;

        if (!idArray.includes(skill_id)) {
            element.classList.add('selectgroup-item');
            element.innerHTML = `<span class="selectgroup-button"> <a href="javascript:void(0)" onClick="deleteSkill(this)" id="${skill_id}"><i class="fa fa-trash text-danger" ></i></a> ${selectedText} | ${skill_level}</span>`;
            list.appendChild(element);
            document.getElementById('skills').value = ""
            var obj = {
                'skill_id': skill_id,
                'skill_level': skill_level,
            }

            skillArray.push(obj);
            idArray.push(skill_id);
            var submitBtn = document.getElementById('submitSkills');
            submitBtn.classList.remove('d-none');
        }

    }
    var skillBtn = document.getElementById('addSkillBtn')
    skillBtn.classList.add('disabled')
}

function deleteExistingSkill(id,element,rel){
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'delete-student-skill',
            id: id,
            rel: rel
        },
        dataType: 'json',
        success: function (response) {


            var parent = element.parentNode
            parent = parent.parentNode
            parent.remove()
        },
        error: function (response) {
            console.log(response)
        }
    });
}

function deleteSkill(element) {
    var skillId = element.id;
    for (var i = 0; i < skillArray.length; i++) {
        if (skillArray[i]['skill_id'] == skillId) {

            skillArray.splice(i, 1);
            parent = element.parentNode;
            parent.remove();
        }
    }
    for (var i = 0; i < idArray.length; i++) {
        if (idArray[i] == skillId) {
            idArray.splice(i, 1);
        }
    }
}

function storeSkills() {

    if (skillArray.length == 0) {
        return false;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'store-skills',
            data: skillArray
        },
        dataType: 'json',
        success: function (response) {
            skillArray.splice(0);
            Swal.fire(
                'موفق!',
                'مهارت های انتخابی شما ثبت شد',
                'success'
            )
            $('#tab25').removeClass('active');
            $('#tab26').addClass('active');
            $('#head-tab25').removeClass('active');
            $('#head-tab26').addClass('active');
        },
        error: function (response) {
            console.log(response)
        }
    });
}

// ----------------------------------------------------------------
// tab 3

const idExpArray = [];
const ExpertiseArray = [];

function addExpertise() {
    var selectedText = document.getElementById('expertises').value;
    if (selectedText != "") {
        var list = document.getElementById('expertise-list')
        var expertise_id = document.getElementById('expertise').value
        var element = document.createElement('label');
        var expertise_level = document.getElementById('expertise_level').value;

        if (!idExpArray.includes(expertise_id)) {
            element.classList.add('selectgroup-item');
            element.innerHTML = `<span class="selectgroup-button"> <a href="javascript:void(0)" onClick="deleteExpertise(this)" id="${expertise_id}"><i class="fa fa-trash text-danger" ></i></a> ${selectedText} | ${expertise_level}</span>`;
            list.appendChild(element);
            document.getElementById('expertises').value = ""
            var obj = {
                'skill_id': expertise_id,
                'skill_level': expertise_level,
            }

            ExpertiseArray.push(obj);
            idExpArray.push(expertise_id);
            var submitBtn = document.getElementById('submitExpertises');
            submitBtn.classList.remove('d-none');
        }

    }
    var skillBtn = document.getElementById('addExpertiseBtn')
    skillBtn.classList.add('disabled')
}

function deleteExpertise(element) {
    var skillId = element.id;
    for (var i = 0; i < ExpertiseArray.length; i++) {
        if (ExpertiseArray[i]['skill_id'] == skillId) {

            ExpertiseArray.splice(i, 1);
            parent = element.parentNode;
            parent.remove();
        }

    }
    for (var i = 0; i < idExpArray.length; i++) {
        if (idExpArray[i] == skillId) {
            idExpArray.splice(i, 1);
        }
    }

}

function storeExpertises() {

    if (ExpertiseArray.length == 0) {
        return false;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'store-skills',
            data: ExpertiseArray
        },
        dataType: 'json',
        success: function (response) {
            ExpertiseArray.splice(0);
            Swal.fire(
                'موفق!',
                'تخصص های انتخابی شما ثبت شد',
                'success'
            )
            $('#tab26').removeClass('active');
            $('#tab27').addClass('active');
            $('#head-tab26').removeClass('active');
            $('#head-tab27').addClass('active');
        },
        error: function (response) {
            console.log(response)
        }
    });
}

// ----------------------------------------------------------------
// tab 4

const idLangArray = [];
const LangsArray = [];

function addLang() {
    var selectedlang = document.getElementById('lang-title')
    var selectedIndex = selectedlang.selectedIndex
    var selectedLangText = selectedlang.options[selectedIndex].text
    var langId = selectedlang.value

    var list = document.getElementById('lang-list')
    var element = document.createElement('label');
    var lang_level = document.getElementById('lang_level').value;

    if (!idLangArray.includes(langId)) {
        element.classList.add('selectgroup-item');
        element.innerHTML = `<span class="selectgroup-button"> <a href="javascript:void(0)" onClick="deleteLang(this)" id="${langId}"><i class="fa fa-trash text-danger" ></i></a> ${selectedLangText} | ${lang_level}</span>`;
        list.appendChild(element);
        var obj = {
            'lang_id': langId,
            'level': lang_level,
        }

        LangsArray.push(obj);
        idLangArray.push(langId);
        var submitBtn = document.getElementById('submitLangs');
        submitBtn.classList.remove('d-none');
    }



}

function deleteExistingLang(id,element){
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'delete-student-lang',
            id: id,
        },
        dataType: 'json',
        success: function (response) {


            var parent = element.parentNode
            parent = parent.parentNode
            parent.remove()
        },
        error: function (response) {
            console.log(response)
        }
    });
}

function deleteLang(element) {
    var langId = element.id;
    for (var i = 0; i < LangsArray.length; i++) {
        if (LangsArray[i]['lang_id'] == langId) {

            LangsArray.splice(i, 1);
            parent = element.parentNode;
            parent.remove();
        }

    }
    for (var i = 0; i < idLangArray.length; i++) {
        if (idLangArray[i] == langId) {
            idLangArray.splice(i, 1);
        }
    }
}

function storeLangs() {

    if (LangsArray.length == 0) {
        return false;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'store-langs',
            data: LangsArray
        },
        dataType: 'json',
        success: function (response) {
            LangsArray.splice(0);
            Swal.fire(
                'موفق!',
                'زبان های انتخابی شما ثبت شد',
                'success'
            )
            $('#tab27').removeClass('active');
            $('#tab30').addClass('active');
            $('#head-tab27').removeClass('active');
            $('#head-tab30').addClass('active');
        },
        error: function (response) {
            console.log(response)
        }
    });
}

// ----------------------------------------------------------------
// tab 5



function deleteCertificate(id) {

    var element = document.getElementById(id);

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'delete-certificate',
            id: id
        },
        dataType: 'json',
        success: function (response) {
            element.remove()
        },
        error: function (response) {
            console.log(response)
        }
    });

}

// ----------------------------------------------------------------
// tab 6

let IdExp = 0;
const idExperienceArray = [];
const ExperienceArray = [];

function addExperience() {

    document.getElementById('experienceError').innerHTML = ''
    document.getElementById('CompanyError').innerHTML = ''
    document.getElementById('startExpError').innerHTML = ''
    document.getElementById('endExpError').innerHTML = ''

    var experienceTitle = document.getElementById('experienceTitle').value
    var experienceCompany = document.getElementById('experienceCompany').value
    var experienceStart = document.getElementById('experienceStart').value
    var experienceEnd = document.getElementById('experienceEnd').value

    if (experienceTitle == '') {
        document.getElementById('experienceError').innerHTML = 'لطفا عنوان شغلی را وارد کنید'
        return false;
    }

    if (experienceCompany == '') {
        document.getElementById('CompanyError').innerHTML = 'لطفا نام سازمان را وارد کنید'
        return false;
    }

    if (experienceStart == '') {
        document.getElementById('startExpError').innerHTML = 'لطفا تاریخ شروع کار را وارد کنید'
        return false;
    }



    if (experienceEnd == '') {
        document.getElementById('endExpError').innerHTML = 'لطفا تاریخ پایان کار را وارد کنید'
        return false;
    }


    IdExp++;
    var obj = {
        'id': IdExp,
        'title': experienceTitle,
        'company': experienceCompany,
        'start': experienceStart,
        'end': experienceEnd
    }

    ExperienceArray.push(obj);

    const table = document.getElementById('experienceTb')
    const newRow = table.insertRow(-1)
    const Cell0 = newRow.insertCell(0)
    const Cell1 = newRow.insertCell(1)
    const Cell2 = newRow.insertCell(2)
    const Cell3 = newRow.insertCell(3)
    const Cell4 = newRow.insertCell(4)

    Cell0.innerHTML = experienceTitle
    Cell1.innerHTML = experienceCompany
    Cell2.innerHTML = experienceStart
    Cell3.innerHTML = experienceEnd
    Cell4.innerHTML = '<a href="javascript:void(0)" onClick="deleteExp(' + obj.id + ',this)" class="btn btn-danger"><i class="fa fa-trash"></i></a>'


    var btn = document.getElementById('submitexperience')
    btn.classList.remove('d-none');
    table.classList.remove('d-none');

    document.getElementById('experienceTitle').value = ''
    document.getElementById('experienceCompany').value = ''
    document.getElementById('experienceStart').value = ''
    document.getElementById('experienceEnd').value = ''
}

function deleteExistingExp(id,element){
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'delete-student-experience',
            id: id,
        },
        dataType: 'json',
        success: function (response) {


            var parent = element.parentNode
            parent = parent.parentNode
            parent.remove()
        },
        error: function (response) {
            console.log(response)
        }
    });
}

function deleteExp(id, element) {
    console.log(element)
    for (var i = 0; i < ExperienceArray.length; i++) {
        if (ExperienceArray[i].id == id) {
            ExperienceArray.splice(i, 1)
        }
    }
    var parent = element.parentNode
    parent = parent.parentNode
    parent.remove()

    if (ExperienceArray.length == 0) {
        const table = document.getElementById('experienceTb');
        table.classList.add('d-none');
    }
}

function storeExperience() {

    console.log(ExperienceArray);

    if (ExperienceArray.length == 0) {
        return false;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'store-experience',
            data: ExperienceArray
        },
        dataType: 'json',
        success: function (response) {
            ExperienceArray.splice(0);
            Swal.fire(
                'موفق!',
                'اطلاعات آکادمیک شما ثبت شد',
                'success'
            )
            $('#tab30').removeClass('active');
            $('#tab31').addClass('active');
            $('#head-tab30').removeClass('active');
            $('#head-tab31').addClass('active');
        },
        error: function (response) {
            console.log(response)
        }
    });
}

// ----------------------------------------------------------------
// tab 7

let id = 1;
const socialNetworkArray = [];

function addSocialNetwork() {

    var selectTitle = document.getElementById('socialNetworkTitle');
    var Title = selectTitle.value;
    var selectedIndex = selectTitle.selectedIndex;
    var selectedOption = selectTitle.options[selectedIndex];
    var name = selectedOption.text;

    var Link = document.getElementById('socialNetworkLink').value;
    var List = document.getElementById('socialNetwork-list');
    var element = document.createElement('lable');

    if (Link == '') {
        document.getElementById('linkError').innerHTML = 'شناسه ی کاربری را وارد کنید'
        return false;
    }


    var isDuplicate = false;
    for (var i = 0; i < socialNetworkArray.length; i++) {
        if (socialNetworkArray[i].title === Title && socialNetworkArray[i].link === Link) {
            isDuplicate = true;
            break;
        }
    }

    if (!isDuplicate) {
        var obj = {
            'id': id,
            'title': Title,
            'link': Link
        };

        socialNetworkArray.push(obj);
        element.classList.add('selectgroup-item')
        element.innerHTML = `<span class="selectgroup-button"> <a href="javascript:void(0)" onClick="deleteSocial(this)" id="${id}"><i class="fa fa-trash text-danger" ></i></a> ${name} | ${Link}</span>`;
        List.appendChild(element);
        id++;
    }
    var submitBtn = document.getElementById('submitSocialNetwork');
        submitBtn.classList.remove('d-none');

}

function deleteExistingSocial(id,element){
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'delete-student-socialNetworks',
            id: id,
        },
        dataType: 'json',
        success: function (response) {


            var parent = element.parentNode
            parent = parent.parentNode
            parent.remove()
        },
        error: function (response) {
            console.log(response)
        }
    });
}


function deleteSocial(element) {
    var socialId = element.id;
    for (var i = 0; i < socialNetworkArray.length; i++) {
        console.log(socialNetworkArray[i].id)
        console.log(socialId)
        if (socialNetworkArray[i].id == socialId) {
            console.log('inja')
            socialNetworkArray.splice(i, 1);
            console.log(socialNetworkArray)
            parent = element.parentNode;
            parent.remove();
            break;
        }
    }

}


function storeSocialNetwork(){
    if (socialNetworkArray.length == 0) {
        return false;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': config.csrf
        },
        url: config.routes.ajax,
        data: {
            do: 'store-socialNetworkArray',
            data: socialNetworkArray
        },
        dataType: 'json',
        success: function (response) {
            socialNetworkArray.splice(0);
            Swal.fire(
                'موفق!',
                'شبکه های اجتماعی انتخابی شما ثبت شد',
                'success'
            )
            $('#tab31').removeClass('active');
            $('#tab29').addClass('active');
            $('#head-tab31').removeClass('active');
            $('#head-tab29').addClass('active');
        },
        error: function (response) {
            console.log(response)
        }
    });
}