function UnCoSearch(){
    let coSearch = document.getElementById('coSearch').value;
    let companySingle = document.getElementById('companySingle').value;
    let coNewsletterUploadUrl = document.getElementById('coNewsletterUploadUrl').value;
    let newsletterErrStatus = document.getElementById('newsletterErrStatus').value;
    let newsletterErrMessage = '';
    if(newsletterErrStatus == 'true'){
        let newsletterErrMessage = document.getElementById('newsletterErrMessage').value;
    }
    let roteIsCompanyUnUser = document.getElementById('roteIsCompanyUnUser').value;

    let obj = {
        'name': '*',
        'family': '*',
        'mobile' : '*',
        'coname': '*',
        'workfield': '*',
    }
    let name = document.getElementById('name').value;
    let family = document.getElementById('family').value;
    let mobile = document.getElementById('mobile').value;
    let coname = document.getElementById('co_name').value;
    let workfield = document.getElementById('workfield').value;

    if(name != ''){
        obj.name = name;
    }
    if(family != ''){
        obj.family = family;
    }
    if(mobile != ''){
        obj.mobile = mobile;
    }
    if(coname != ''){
        obj.coname = coname;
    }
    if(workfield != ''){
        obj.workfield = workfield;
    }

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: coSearch,
        data: {
            obj: obj,
        },
        dataType: 'json',
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (response) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            console.log(response);
            var companies = response;
            var tableBody = document.getElementById('co_table');
            tableBody.innerHTML = ''; // Clear existing table data

            
            companies.forEach(function (company , index) {

                var rowNumber = index + 1;
                var coroute = companySingle.replace(':id', company['id']);

                var newsletterStatus = '';
                var submitBtn = '';
                if(company['company_infos']['newsletter_id'] != null){
                    newsletterStatus = 'disabled';
                    submitBtn = '<button type="submit" class="btn btn-dark" disabled>ثبت شده</button>'
                }else{
                    submitBtn = '<button type="submit" class="btn btn-primary">ثبت</button>'
                }
                
                var newsletterErrTag = '';
                if(newsletterErrStatus == 'true'){
                    newsletterErrTag = '<small class="text-danger">'+ newsletterErrMessage +'</small>'
                }

                var rejectedBtn = '';
                if(roteIsCompanyUnUser = true){
                    rejectedBtn = '<button type="button" class="btn btn-sm btn-danger badge" data-bs-toggle="modal" data-bs-target="#edit-modal-'+ company['id'] +'">رد</button>'
                }
                var rowHtml = 
                '<tr>' +
                    '<td class="text-center align-middle">'+ rowNumber +'</td>' +

                    '<td class="align-middle">'+
                        '<a href="'+ coroute +'" class="d-flex align-items-center text-description text-dark">' +
                            '<div class="d-none d-md-inline p-0 ps-2">' +
                                '<img alt="image" class="avatar avatar-md br-7" src="'+ (company['logo'] ? company['logo']['path'] : imageUrl) +'">' +
                            '</div>' +
                            '<div class="d-md-inline p-md-0 ms-0 ms-md-2">'+ company['company_infos']['name'] +'</div>' +
                        '</a>' +
                    '</td>' +

                    '<td class="align-middle text-center">' + company['company_infos']['n_code'] +'</td>' +

                    '<td class="text-nowrap align-middle">' + company['name'] + ' ' + company['family'] + '</td>' +
                    
                    '<td class="text-nowrap align-middle">' + company['mobile'] + '</td>' +

                    '<td class="text-nowrap align-middle text-center">' +
                        '<form action="'+ coNewsletterUploadUrl +'" method="post" class="row w-100 align-items-center justify-content-center" enctype="multipart/form-data">' +
                            '<input type="hidden" name="_token" value="'+ myCsrf +'">'+
                            '<div class="col-8 text-center">' +
                                '<input type="hidden"  name="company" value="'+ company['id'] +'">' +
                                '<input type="file" name="newsletter'+ company['id'] +'" class="form-control"'+ newsletterStatus +'>' +
                                newsletterErrTag +
                            '</div>' +
                            '<div class="col-3 text-center">' +
                                submitBtn +
                            '</div>' +
                        '</form>'+
                    '</td>' +

                    '<td class="text-center align-middle">' +
                        '<div class="btn-group align-top">' +
                            '<a href="javascript:void(0)" onclick="ConfirmCoInfo('+ company['id'] +')" class="btn btn-sm btn-success badge">تایید </a>'+
                            rejectedBtn +
                        '</div>' +
                    '</td>'+
                '</tr>';

                // Append the row HTML to the table body
                tableBody.innerHTML += rowHtml;
            });

        },
        error: function (response) {
            console.log(response)
        }
    });



}