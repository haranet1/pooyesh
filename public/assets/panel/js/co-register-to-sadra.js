const nameArray = []

function addFacultyOfHumanities()
{
    let selectedText = document.getElementById('facultyOfHumanities').value;
    let facultyOfHumanitiesErr = document.getElementById('facultyOfHumanitiesErr');

    if(selectedText != ""){
        facultyOfHumanitiesErr.innerHTML = '';

        let collection = document.getElementsByClassName('array-facultyOfHumanities');

        let elementsArray = [].slice.call(collection)

        let list = document.getElementById('facultyOfHumanities-list');
        let element = document.createElement('label');
        let facultyOfHumanities_number = document.getElementById('facultyOfHumanitiesNumber').value;
        let facultyOfHumanitiesNumberErr = document.getElementById('facultyOfHumanitiesNumberErr');

        if(facultyOfHumanities_number != ""){
            facultyOfHumanitiesNumberErr.innerHTML = '';
            let newInput = `<input type="hidden" name="required_facultyOfHumanities[${selectedText}]" value="${facultyOfHumanities_number}" class="array-facultyOfHumanities selectgroup-input">`

            if(elementsArray.length < 1){
    
                element.classList.add('selectgroup-item');
                element.innerHTML = newInput + '<span class="selectgroup-button"><span class="text-danger" onclick="deleteElement(this)">X</span> ' + selectedText + '<span class="text-info"> '+ facultyOfHumanities_number +' نفر</span></span>';
                list.appendChild(element);
                nameArray.push(selectedText)
    
            }else if(! nameArray.includes(selectedText)){
    
                element.classList.add('selectgroup-item');
                element.innerHTML = newInput + '<span class="selectgroup-button"><span class="text-danger" onclick="deleteElement(this)">X</span> '+ selectedText +'<span class="text-info"> '+ facultyOfHumanities_number +' نفر</span></span>';
                list.appendChild(element);
                nameArray.push(selectedText)
            }
        }else {
            facultyOfHumanitiesNumberErr.innerHTML = 'نفرات مورد نیاز این رشته را وارد کنید'
        }

    }else {
        facultyOfHumanitiesErr.innerHTML = 'رشته مورد نظر را انتخاب کنید'
    }
}

function addSchoolOfLaw()
{
    let selectedText = document.getElementById('schoolOfLaw').value;
    let textErr = document.getElementById('schoolOfLawErr');

    if(selectedText != ""){
        textErr.innerHTML = '';

        let collection = document.getElementsByClassName('array-schoolOfLaw');

        let elementsArray = [].slice.call(collection)

        let list = document.getElementById('schoolOfLaw-list');
        let element = document.createElement('label');
        let number = document.getElementById('schoolOfLawNumber').value;
        let NumberErr = document.getElementById('schoolOfLawNumberErr');

        if(number != ""){
            NumberErr.innerHTML = '';
            let newInput = `<input type="hidden" name="required_schoolOfLaw[${selectedText}]" value="${number}" class="array-facultyOfHumanities selectgroup-input">`

            if(elementsArray.length < 1){
    
                element.classList.add('selectgroup-item');
                element.innerHTML = newInput + '<span class="selectgroup-button"><span class="text-danger" onclick="deleteElement(this)">X</span> ' + selectedText + '<span class="text-info"> '+ number +' نفر</span></span>';
                list.appendChild(element);
                nameArray.push(selectedText)
    
            }else if(! nameArray.includes(selectedText)){
    
                element.classList.add('selectgroup-item');
                element.innerHTML = newInput + '<span class="selectgroup-button"> <span class="text-danger" onclick="deleteElement(this)">X</span>'+ selectedText +'<span class="text-info"> '+ number +' نفر</span></span>';
                list.appendChild(element);
                nameArray.push(selectedText)
            }

        }else {
            NumberErr.innerHTML = 'نفرات مورد نیاز این رشته را وارد کنید'
        }
 
    } else {
        textErr.innerHTML = 'رشته مورد نظر را انتخاب کنید';
    }
}

function addArt()
{
    let selectedText = document.getElementById('art').value;
    let textErr = document.getElementById('artErr');

    if(selectedText != ""){
        textErr.innerHTML = '';

        let collection = document.getElementsByClassName('array-art');

        let elementsArray = [].slice.call(collection)

        let list = document.getElementById('art-list');
        let element = document.createElement('label');
        let number = document.getElementById('artNumber').value;
        let NumberErr = document.getElementById('artNumberErr');

        if(number != ""){
            NumberErr.innerHTML = '';
            let newInput = `<input type="hidden" name="required_art[${selectedText}]" value="${number}" class="array-art selectgroup-input">`

            if(elementsArray.length < 1){
    
                element.classList.add('selectgroup-item');
                element.innerHTML = newInput + '<span class="selectgroup-button"><span class="text-danger" onclick="deleteElement(this)">X</span> ' + selectedText + '<span class="text-info"> '+ number +' نفر</span></span>';
                list.appendChild(element);
                nameArray.push(selectedText)
    
            }else if(! nameArray.includes(selectedText)){
    
                element.classList.add('selectgroup-item');
                element.innerHTML = newInput + '<span class="selectgroup-button"><span class="text-danger" onclick="deleteElement(this)">X</span> '+ selectedText +'<span class="text-info"> '+ number +' نفر</span></span>';
                list.appendChild(element);
                nameArray.push(selectedText)
            }

        }else {
            NumberErr.innerHTML = 'نفرات مورد نیاز این رشته را وارد کنید'
        }
 
    } else {
        textErr.innerHTML = 'رشته مورد نظر را انتخاب کنید';
    }
}
function addSkillCollege()
{
    let selectedText = document.getElementById('skillCollege').value;
    let textErr = document.getElementById('skillCollegeErr');
    
    if(selectedText != ""){
        textErr.innerHTML = '';

        let collection = document.getElementsByClassName('array-skillCollege');

        let elementsArray = [].slice.call(collection)

        let list = document.getElementById('skillCollege-list');
        let element = document.createElement('label');
        let number = document.getElementById('skillCollegeNumber').value;
        let NumberErr = document.getElementById('skillCollegeNumberErr');

        if(number != ""){
            NumberErr.innerHTML = '';
            let newInput = `<input type="hidden" name="required_skillCollege[${selectedText}]" value="${number}" class="array-skillCollege selectgroup-input">`

            if(elementsArray.length < 1){
    
                element.classList.add('selectgroup-item');
                element.innerHTML = newInput + '<span class="selectgroup-button"><span class="text-danger" onclick="deleteElement(this)">X</span> ' + selectedText + '<span class="text-info"> '+ number +' نفر</span></span>';
                list.appendChild(element);
                nameArray.push(selectedText)
    
            }else if(! nameArray.includes(selectedText)){
    
                element.classList.add('selectgroup-item');
                element.innerHTML = newInput + '<span class="selectgroup-button"><span class="text-danger" onclick="deleteElement(this)">X</span> '+ selectedText +'<span class="text-info"> '+ number +' نفر</span></span>';
                list.appendChild(element);
                nameArray.push(selectedText)
            }

        }else {
            NumberErr.innerHTML = 'نفرات مورد نیاز این رشته را وارد کنید'
        }
 
    } else {
        textErr.innerHTML = 'رشته مورد نظر را انتخاب کنید';
    }
}

function submitForm()
{
    if(logoCheck == true){

        let form = document.getElementById('form');
        form.submit();

    } else {

        let logo = document.getElementById('logo').value;

        if(logo == ""){

            let logoErr = document.getElementById('verifyLogo');

            logoErr.innerHTML = 'لوگو شرکت الزامی می‌باشد';
    
            window.scrollTo({ top: 0, behavior: 'smooth' });

        } else {

            let form = document.getElementById('form');
            form.submit();

        }

    }
}

function deleteElement(element)
{
    let lable = element.parentNode.parentNode;

    lable.innerHTML = '';
}