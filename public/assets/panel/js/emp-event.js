function deleteEvent(event_id)
{
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این رویداد حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: $('#deleteEventUrl').val(),
                data: {
                    event_id: event_id
                },
                dataType: "json",
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (data) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);

                    if(data == 'cantDelete'){
                        Swal.fire({
                            icon: 'warning',
                            title: 'خطا !',
                            text : 'این رویداد، بلیط خریداری شده دارد و امکان حذف کردن این رویداد وجود ندارد.',
                            showConfirmButton: true,
                        })
                    }else{
                        Swal.fire({
                            icon: 'success',
                            title: 'رویداد با موفقیت حذف شد',
                            showConfirmButton: true,
                        })
                        location.reload()
                    }
                    
                    
                    
                },
                error: function (data) {
                    console.log(data.error)
                }
        
            })
        }
    })
}

function deleteEventPlan(plan_id)
{
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این پلن حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: $('#deletePlanUrl').val(),
                data: {
                    plan_id: plan_id
                },
                dataType: "json",
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (data) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    
                    Swal.fire({
                        icon: 'success',
                        title: 'پلن با موفقیت حذف شد',
                        showConfirmButton: true,
                    })
                    location.reload()
                    
                },
                error: function (data) {
                    console.log(data.error)
                }
        
            })
        }
    })
}

function deleteEventSans(sans_id)
{
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این سانس حذف خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: $('#deleteSansUrl').val(),
                data: {
                    sans_id: sans_id
                },
                dataType: "json",
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (data) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    
                    Swal.fire({
                        icon: 'success',
                        title: 'سانس با موفقیت حذف شد',
                        showConfirmButton: true,
                    })
                    location.reload()
                    
                },
                error: function (data) {
                    console.log(data.error)
                }
        
            })
        }
    })
}

function activeEvent(event_id)
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        type: 'POST',
        url: $('#activeEventUrl').val(),
        data: {
            event_id: event_id
        },
        dataType: "json",
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (data) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            
        
            location.reload()
            
        },
        error: function (data) {
            console.log(data.error)
        }

    })
}

function getEventInfo()
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        type: 'POST',
        url: $('#getEventInfoUrl').val(),
        data: {
            event_id: $('#event').val(),
            plan_id: $('#plan').val(),
            sans_id: $('#sans').val(),
            // discount_id: $('#discount').val(),
        },
        dataType: "json",
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (data) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            
        console.log(data['plan']);
            $('#plan_name').text('')
            $('#plan_desc').text('')
            $('#plan_count').text('')

            $('#sans_name').text('')
            $('#sans_desc').text('')
            $('#sans_date').text('')

            let number = data['plan']['count'] - data['plan_counter']

            $('#plan_name').text('پلن ' + data['plan']['title'])
            $('#plan_desc').text(data['plan']['description'])
            $('#plan_count').text('ظرفیت باقی مانده :' + number)

            if(data['sans']['title']){
                $('#sans_name').text('سانس ' + data['sans']['title'])
            }

            if(data['sans']['description']){
                $('#sans_desc').text(data['sans']['description'])
            }
            $('#sans_date').text('تاریخ ' + data['sans']['date']+ ' ' + 'ساعت ' + data['sans']['time'])

            if(data['plan']['price'] == 0){
                $('#price').text('هزینه : رایگان')
            }else{
                $('#price').text(' : رایگان' + data['plan']['price'])
            }

            $('#registerToEvent').removeClass('d-none');

            $('#div2').removeClass('d-none');
            $('#div1').addClass('d-none');
            
        },
        error: function (data) {
            console.log(data.error)
        }

    })
}

function activeTicket(element , info_id){
    var TicketBox = document.querySelectorAll('.ticket-box');
    TicketBox.forEach(removeActive);

    function removeActive(item){
        item.classList.remove("active-box");
    }

    element.classList.add("active-box");

    $('#discount_code').val('')
    $('#discount_id').val('')
    $('#discount_price').text('')
    $('#discount_price_div').removeClass('d-flex')
    $('#discount_price_div').addClass('d-none')


    $.ajax({
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        type: 'POST',
        url: $('#getEventInfoUrl').val(),
        data: {
            info_id: info_id
        },
        dataType: "json",
        beforeSend: function(){
            $('#global-loader').removeClass('d-none');
            $('#global-loader').addClass('d-block');
            $('#global-loader').css('opacity', 0.6);
        },
        success: function (data) {
            $('#global-loader').removeClass('d-block');
            $('#global-loader').addClass('d-none');
            $('#global-loader').css('opacity', 1);
            
            $('#priceDiv').removeClass('d-none');
            $('#info_id').val(data['id']);

            $('#priceElement').text(data['plan']['price']);
            $('#priceInput').val(data['plan']['price']);
            $('#totalPrice').text(data['plan']['price']);
            $('#totalPriceInput').val(data['plan']['price']);

            let multyBuy = document.getElementById('multyBuy');
            multyBuy.innerHTML= '';

            let left_over = data['plan']['count'] - data['plan']['info']['plan_counter'];
            
            for (let index = 1; index <= left_over; index++) {

                multyBuy.innerHTML += '<option value="'+ index +'">'+ index +'</option>'
                
            }
            
        },
        error: function (data) {
            console.log(data.error)
        }

    })

}

function getSubTotal(element)
{
    $('#totalPrice').text(
        element.value * $('#priceInput').val()
    )
    $('#totalPriceInput').val(element.value * $('#priceInput').val())
}

function applyDiscount()
{
    if($('#discount_code').val() == '' || $('#discount_code').val() == null){

        $('#discount_code_err').text('کد تخفیف را وارد کنید.');

    } else {
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': myCsrf
            },
            type: 'POST',
            url: $('#applyDiscountUrl').val(),
            data: {
                info_id: $('#info_id').val(),
                code: $('#discount_code').val(),
                sub_total: $('#totalPriceInput').val()
                
            },
            dataType: "json",
            beforeSend: function(){
                $('#global-loader').removeClass('d-none');
                $('#global-loader').addClass('d-block');
                $('#global-loader').css('opacity', 0.6);
            },
            success: function (data) {
                $('#global-loader').removeClass('d-block');
                $('#global-loader').addClass('d-none');
                $('#global-loader').css('opacity', 1);

                if(data == 'discountNotDefine'){

                    Swal.fire({
                        title: 'خطا!',
                        icon: 'error',
                        text: 'کد وارد شده نا معتبر است',
                    })
                }

                if(data == 'discountNotTime'){

                    Swal.fire({
                        title: 'خطا!',
                        icon: 'error',
                        text: 'زمان استفاده از کد تخفیف نرسیده است',
                    })
                }

                if(data == 'discountTimeout'){

                    Swal.fire({
                        title: 'خطا!',
                        icon: 'error',
                        text: 'زمان استفاده از کد تخفیف گذشته است',
                    })
                }

                if(data == 'CantUseThis'){

                    Swal.fire({
                        title: 'خطا!',
                        icon: 'error',
                        text: 'امکان استفاده این کد برای شما وجود ندارد',
                    })
                }

                if(data == 'discountNotForThisInfo'){

                    Swal.fire({
                        title: 'خطا!',
                        icon: 'error',
                        text: 'امکان استفاده این کد برای این بلیط وجود ندارد',
                    })
                }

                if(data == 'discountIsMax'){

                    Swal.fire({
                        title: 'خطا!',
                        icon: 'error',
                        text: 'ظرفیت کد تخفیف به پایان رسید است',
                    })

                }
                
                if(data['discount_id']){
                    $('#discount_id').val(data['discount_id']);
                    $('#discount_price_div').removeClass('d-none');
                    $('#discount_price_div').addClass('d-flex');
                    $('#discount_price').text(data['subPrice']);
                    $('#totalPrice').text(data['payablePrice']);

                }

                
            },
            error: function (data) {
                console.log(data.error)
            }
    
        })

    }
}
