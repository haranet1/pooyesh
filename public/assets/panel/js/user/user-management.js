// const myCsrf = $('meta[name="_token"]').attr('content');


function ConfirmCoInfo(id)
{
    const coInfoConfirmUserUrl = document.getElementById('coInfoConfirmUserUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این کاربر تایید خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: coInfoConfirmUserUrl,
                data: {
                    id: id
                },
                dataType: "json",
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (data) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    
                    if(data.cantConfirm){
                        Swal.fire({
                            icon: 'error',
                            title: data.cantConfirm,
                            confirmButtonText: 'متوجه شدم'
                        })
                    }else{
                        Swal.fire({
                            icon: 'success',
                            title: 'کاربر با موفقیت تایید شد',
                            showConfirmButton: true,
                        })
                        location.reload()
                    }
                    
                },
                error: function (data) {
                    console.log(data.error)
                }
        
            })
        }
    })
}

function ConfirmUser(id)
{
    const confirmUserUrl = document.getElementById('confirmUserUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "این کاربر تایید خواهد شد!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: confirmUserUrl,
                data: {
                    id: id
                },
                dataType: "json",
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (data) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    
                    Swal.fire({
                        icon: 'success',
                        title: 'کاربر با موفقیت تایید شد',
                        showConfirmButton: true,
                    })
                    location.reload()
                    
                },
                error: function (data) {
                    console.log(data)
                }
        
            })
        }
    })
}

function editToCompany(id)
{
    Swal.fire({
        title: 'تغییر نوع کاربری به شرکت',
        text: "با تغییر، اطلاعات نقش قبلی کاربر حذف می‌شود!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: $('#editCompanyRoleUrl').val(),
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'تغییر نقش به شرکت انجام شد',
                        'success'
                    )
                    console.log(response);
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

function editToApplicant(id)
{
    Swal.fire({
        title: 'تغییر نوع کاربری به کارجو',
        text: "با تغییر، اطلاعات نقش قبلی کاربر حذف می‌شود!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: $('#editApplicantRoleUrl').val(),
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'تغییر نقش به کارجو انجام شد',
                        'success'
                    )
                    console.log(response);
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}

function editToStudent(id)
{
    Swal.fire({
        title: 'تغییر نوع کاربری به دانشجو',
        text: "با تغییر، اطلاعات نقش قبلی کاربر حذف می‌شود!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: $('#editStudentRoleUrl').val(),
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function (response) {
                    Swal.fire(
                        'موفق!',
                        'تغییر نقش به دانشجو انجام شد',
                        'success'
                    )
                    console.log(response);
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });


        }
    })
}
