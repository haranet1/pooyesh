function acceptInternRequest(id)
{
    let companyAcceptRequestUrl = document.getElementById('companyAcceptRequestUrl').value;
    Swal.fire({
        title: 'درخواست کارجو تایید خواهد شد!',
        showCancelButton: true,
        confirmButtonText: 'تایید',
        cancelButtonText: `انصراف`,
        customClass: {
            confirmButton: "btn btn-success",
        },
        icon: 'success',
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: companyAcceptRequestUrl,
                data: {
                    id : id,
                },
                dataType: "json",
                beforeSend: function () {
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق',
                        'درخواست همکاری تایید شد!',
                        'success'
                    )
                    location.reload()
                },
                error: function (data) {                    
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);

                    const errorMessage = data.responseJSON ? data.responseJSON.text : 'خطای ناشناخته';
                    Swal.fire({
                        icon: 'error',
                        title: 'خطا',
                        text: errorMessage,
                        showConfirmButton: true,
                        confirmButtonText: 'خرید بسته خدماتی',
                        showCancelButton: true,
                        cancelButtonText: 'بستن',
                        // timer: 4500
                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location.href = packageUrl;
                        }
                    });
                    // console.log(xhr.responseText);
                }
            })
        }
    })
}

function rejectInternRequest(id)
{
    let companyRejectRequestUrl = document.getElementById('companyRejectRequestUrl').value;
    Swal.fire({
        title: 'درخواست کارجو رد خواهد شد!',
        showCancelButton: true,
        confirmButtonText: 'تایید',
        cancelButtonText: `انصراف`,
        customClass: {
            confirmButton: "btn btn-success",
        },
        icon: 'warning',
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: companyRejectRequestUrl,
                data: {
                    id : id,
                },
                dataType: "json",
                beforeSend: function () {
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق',
                        'درخواست همکاری رد شد!',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response);
                }
            })
        }
    })
}

function cancelInternRequest(id)
{
    let companyCancleRequestUrl = document.getElementById('companyCancleRequestUrl').value;
    Swal.fire({
        title: 'آیا مایل به لغو این درخواست هستید؟',
        showCancelButton: true,
        confirmButtonText: 'بله',
        cancelButtonText: `خیر`,
        icon: 'question',
    }).then((result) => {
        if(result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                type: 'POST',
                url: companyCancleRequestUrl,
                data: {
                    request_id : id,
                },
                dataType: "json",
                beforeSend: function () {
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    Swal.fire(
                        'موفق',
                        'درخواست همکاری لغو شد!',
                        'success'
                    )
                    location.reload()
                },
                error: function (response) {
                    console.log(response);
                }
            })
        }
    })
}