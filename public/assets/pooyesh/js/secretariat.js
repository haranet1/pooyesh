function printContract(id){
    let printContractUrl = document.getElementById('printContractUrl').value;
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': myCsrf
        },
        url: printContractUrl,
        data: {
            id: id,
        },
        dataType: 'json',
        success: function (response) {
            Swal.fire(
                'موفق!',
                'قرارداد با موفقیت چاپ شد.',
                'success'
            ).then((result) => {
                if(result.isConfirmed){
                    location.reload();
                } else {
                    setTimeout(() => {
                        location.reload();
                    } , 1000);
                }
            })
        },
        error: function (response) {
            console.log(response)
        }
    })
}