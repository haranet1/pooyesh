function acceptPooyeshRequest(id){
    let confirmByUniUrl = document.getElementById('confirmByUniUrl').value;
    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        text: "درخواست قبول خواهد شد!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#198f20',
        cancelButtonColor: '#d33',
        cancelButtonText: 'انصراف',
        confirmButtonText: 'بله'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': myCsrf
                },
                url: confirmByUniUrl,
                data: {
                    id: id,
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    
                    Swal.fire(
                        'موفق!',
                        'درخواست  پویش تایید و قرارداد برای دبیرخانه ارسال شد.',
                        'success'
                    ).then((result) => {
                        if(result.isConfirmed) {
                            location.reload()
                        } else {
                            setTimeout(() => {
                                location.reload()
                            } , 1000);
                        }
                    })
                },
                error: function (response) {
                    console.log(response)
                }
            });

        }
    })
}


let request_id = document.getElementById('request_id').value;

let subjectErr = document.getElementById('subjectErr');
let contentErr = document.getElementById('contentErr');
let attachmentErr = document.getElementById('attachmentErr');


var ticketModal = document.getElementById('edit-modal-'+request_id);
var modal = new bootstrap.Modal(ticketModal);


if(subjectErr != null || contentErr != null || attachmentErr != null){
    console.log('hi');
    modal.show();
}

