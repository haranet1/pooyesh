window.Apex = {
    chart: {
        foreColor: '#ccc',
        width:150,
        toolbar: {
            show: false
        },
    },
    stroke: {
        width: 3
    },
    dataLabels: {
        enabled: false
    },
    tooltip: {
        show: false
    },
    grid: {
        borderColor: "#535A6C",
        xaxis: {
        lines: {
            show: true
        }
        }
    }
};

var spark1 = {
    chart: {
        id: 'spark1',
        group: 'sparks',
        type: 'line',
        height: 60,
        width:130,
        sparkline: {
            enabled: true
        },
        dropShadow: {
            enabled: true,
            top: 1,
            left: 1,
            blur: 2,
            opacity: 0.2,
        }
    },
    series: [{
        data: [12, 66, 12, 59, 25, 44, 12, 36, 9, 21]
    }],
    stroke: {
        curve: 'smooth'
    },
    markers: {
        size: 0
    },
    colors: ['#fff'],
    tooltip: {
        enabled: false,
    }
}

var spark2 = {
    chart: {
        id: 'spark2',
        group: 'sparks',
        type: 'line',
        height: 60,
        width:130,
        sparkline: {
        enabled: true
        },
        dropShadow: {
        enabled: true,
        top: 1,
        left: 1,
        blur: 2,
        opacity: 0.2,
        }
    },
    series: [{
        data: [12, 14, 12, 47, 32, 44, 24, 55, 41, 69]
    }],
    stroke: {
        curve: 'smooth'
    },
    markers: {
        size: 0
    },
    colors: ['#fff'],
    tooltip: {
        enabled: false
    }
}

var spark3 = {
    chart: {
        id: 'spark3',
        group: 'sparks',
        type: 'line',
        height: 60,
        width:130,
        sparkline: {
            enabled: true
        },
        dropShadow: {
            enabled: true,
            top: 1,
            left: 1,
            blur: 2,
            opacity: 0.2,
        }
    },
    series: [{
        data: [47, 45, 74, 32, 56, 31, 44, 33, 45, 19]
    }],
    stroke: {
        curve: 'smooth'
    },
    markers: {
        size: 0
    },
    colors: ['#fff'],
    tooltip: {
        enabled:false
    }
}

var spark4 = {
    chart: {
        id: 'spark4',
        group: 'sparks',
        type: 'line',
        height: 60,
        width:130,
        sparkline: {
        enabled: true
        },
        dropShadow: {
        enabled: true,
        top: 1,
        left: 1,
        blur: 2,
        opacity: 0.2,
        }
    },
    series: [{
        data: [15, 75, 47, 65, 14, 32, 19, 54, 44, 61]
    }],
    stroke: {
        curve: 'smooth'
    },
    markers: {
        size: 0
    },
    colors: ['#fff'],
    tooltip: {
        enabled:false
    }
}

new ApexCharts(document.querySelector("#spark1"), spark1).render();
new ApexCharts(document.querySelector("#spark2"), spark2).render();
new ApexCharts(document.querySelector("#spark3"), spark3).render();
new ApexCharts(document.querySelector("#spark4"), spark4).render();