
// -- jobs autocamlete 
let jobname = document.getElementById('titleId');
jobname.addEventListener('input', function(){
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': csrf 
        },
        url: url,
        data: {
            do:'get-jobs-by-cat',
                job_title: jobname.value,
                job_cat: document.getElementById('jobCat').value
                
        },
        dataType: 'json',
            success: function (response) {
                autocomplete(jobname,response);
                
        },
        error: function (response) {
            console.log(response)
        }
    });
})
    
    
function autocomplete(inp, arr) {
    var existingElement = document.getElementById("titleIdautocomplete-list")
    if (existingElement)
        existingElement.remove()

    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;

    var a, b = null
    /*, val = this.value;
                    /!*close any already open lists of autocompleted values*!/
                    closeAllLists();
                    if (!val) {
                        return false;
                    }*/
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", inp.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items border border-primary  bg-light py-2 px-1");
    /*append the DIV element as a child of the autocomplete container:*/
    inp.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (var i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        /*create a DIV element for each matching element:*/
        b = document.createElement("DIV");
        /*make the matching letters bold:*/
        b.innerHTML = "<strong>" + arr[i]['name'] + "</strong>";
        /*insert a input field that will hold the current array item's value:*/
        b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + "'>";
        /*execute a function when someone clicks on the item value (DIV element):*/
        b.addEventListener("click", function (e) {
            /*insert the value for the autocomplete text field:*/
            inp.value = this.getElementsByTagName("input")[0].value;
            
            cat = document.getElementById('titleId')
            // cat.value = this.getElementsByTagName("input")[0].value
            // document.getElementById("titleId").value = cat.value;
            /*close the list of autocompleted values,
            (or any other open lists of autocompleted values:*/
            closeAllLists();
            UpdateVars()
        });
        a.appendChild(b);
    }

    /*execute a function when someone writes in the text field:*/
    /*inp.addEventListener("input", function (e) {

    });*/

    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

// ----------------------------------------------------------------

// city ajax

let provinceSelect = document.getElementById('provinceId');
let citySelect = document.getElementById('cityId');

provinceSelect.addEventListener('change', function () {
    let province = provinceSelect.value;
    
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': csrf
        },
        url: url,
        data: {
            do: 'get-city',
            province: province
        },
        dataType: 'json',
        success: function (response) {
            // Clear existing city options
            citySelect.innerHTML = '';
            let option = document.createElement('option');
            option.value = "0";
            option.text = "شهر"
            citySelect.appendChild(option);
            // Add new city options based on the response
            response.forEach(function (city) {
                let option = document.createElement('option');
                option.value = city.id;
                option.text = city.name;
                citySelect.appendChild(option);
            });
        },
        error: function (response) {
            console.log(response);
        }
    });
});

// ----------------------------------------------------------------

// -- on click = update values
function UpdateVars()
{
    let results = {
        jobCategory : "*",
        jobName : "*",
        province : "*",
        jobType : "*",
        sex : "*",
        academicLevel : "*",
        mariageType : "*",
        workExperience : "*",
        city : "*",
        skills : [],
        expertise : [],
        personalityTest : [],
        age : "*",
    };

    let jobCategoryVAR = document.getElementById('jobCat').value;
    let jobNameVAR = document.getElementById('titleId').value;
    let provinceVAR = document.getElementById('provinceId').value;
    let jobTypeVAR = document.getElementById('jobTypeId').value;
    let sexVAR = document.getElementById('sexId').value;
    let academicLevelVAR = document.getElementById('academicLevelId').value;
    let mariageTypeVAR = document.getElementById('mariageTypeId').value;
    let workExperienceVAR = document.getElementById('workExperienceId').value;
    let cityVAR = document.getElementById('cityId').value;
    // let skillsVAR = document.getElementById('skillsId').value;
    // let expertiseVAR = document.getElementById('expertiseId').value;
    // let personalityTestVAR = document.getElementById('personalityTestId').value;
    let ageVAR = document.getElementById('ageId').value;

    if (jobCategoryVAR != 0){
        results.jobCategory = jobCategoryVAR
    }
    if (jobNameVAR != 0){
        results.jobName = jobNameVAR
    }
    if (provinceVAR != 0){
        results.province = provinceVAR
    }
    if (jobTypeVAR != 0){
        results.jobType = jobTypeVAR
    }
    if (sexVAR != 0){
        results.sex = sexVAR
    }
    if (academicLevelVAR != 0){
        results.academicLevel = academicLevelVAR
    }
    if (mariageTypeVAR != 0){
        results.mariageType = mariageTypeVAR
    }
    if (workExperienceVAR != 0){
        results.workExperience = workExperienceVAR
    }
    if (cityVAR != 0){
        results.city = cityVAR
    }
    // if (skillsVAR != 0){
    //     results.skills = skillsVAR
    // }
    // if (expertiseVAR != 0){
    //     results.expertise = expertiseVAR
    // }
    // if (personalityTestVAR != 0){
    //     results.personality = personalityTestVAR
    // }
    if (ageVAR != 0){
        results.age = ageVAR
    }

    sendRequest(results);
}

// ----------------------------------------------------------------

// -- send Request ajax
function sendRequest(results)
{
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': csrf 
        },
        url: url,
        data: {
            do:'get-job-position',
                resultObj: results
                
        },
        dataType: 'json',
            success: function (response) {
                console.log(response);
                makeCard(response);
                
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function makeCard(data) 
{
    var row = document.getElementById('rowId');
    row.innerHTML = '';

    data.forEach(function(item) {
        // company part
        var coroute = companyRoute.replace(':id' , item['company_id']);
        // company part
        var jobroute = jobRoute.replace(':id' , item['company_id']);
        // logo part
        let logoPath = "'assets/images/default-logo.svg'";
        if (item['company']['groupable']['logo'] != null){
            logoPath = `'${item['company']['groupable']['logo']['path']}'`;
        }
        var cardContainer = document.createElement('div');
        cardContainer.classList.add('col-xxl-4', 'pxp-jobs-card-2-container');

        var card = document.createElement('div');
        card.classList.add('pxp-jobs-card-2', 'pxp-has-border');
        
        card.innerHTML = `
            <div class="pxp-jobs-card-2-top">
                <a href="${coroute}" class="pxp-jobs-card-2-company-logo"
                    style="background-image: url(`+ logoPath +`);"></a>
                <div class="pxp-jobs-card-2-info">
                    <a href="${jobroute}" class="pxp-jobs-card-2-title">`+item['job']['name']+`</a>
                    <div class="pxp-jobs-card-2-details">
                        <span class="pxp-jobs-card-2-location">
                            <span class="fa fa-globe"></span>`+ item['province_and_city'] +`
                        </span>
                        <div class="pxp-jobs-card-2-type">`+ item['type'] +`</div>
                    </div> 
                </div>
            </div>
            <div class="pxp-jobs-card-2-bottom">
                <span class="pxp-jobs-card-2-category">
                    <div class="pxp-jobs-card-2-category-label">`+ item['job']['category']['name'] +`</div>
                </span>
                <div class="pxp-jobs-card-2-bottom-right">
                    <span class="pxp-jobs-card-2-date pxp-text-light">`+ item['created_at_fa'] +` توسط </span><a
                        href="`+ coroute +`" class="pxp-jobs-card-2-company">`+ item['company']['groupable']['name'] +`</a>
                </div>
            </div>
        `;

        cardContainer.appendChild(card);
        row.appendChild(cardContainer);
    });
}