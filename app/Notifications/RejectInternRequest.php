<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\RejectInternRequest as MailRejectInternRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RejectInternRequest extends Notification
{
    use Queueable;

    public $subject;
    public $message;
    public $link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($subject , $message , $link)
    {
        $this->subject = $subject;
        $this->message = $message;
        $this->link = $link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailRejectInternRequest($this->subject , $this->message ,$this->link));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message,
            'link' => $this->link,
        ];
    }
}
