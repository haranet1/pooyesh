<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\AcceptedPooyeshRequest as MailAcceptedPooyeshRequest;
use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AcceptedPooyeshRequest extends Notification
{
    use Queueable;

    public $receiver;
    public $message;
    public $subject;
    public $link;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($receiver)
    {
        $this->receiver = $receiver;
        $this->subject = 'تایید درخواست کارآموزی';
        $this->getMessage();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailAcceptedPooyeshRequest($this->subject , $this->message , $this->link));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message,
            'link' => $this->link,
        ];
    }

    private function getMessage()
    {
        if($this->receiver->groupable_type == CompanyInfo::class){
            $this->message = 'شرکت' . ' ' . $this->receiver->groupable->name . ' ' . 'درخواست کارآموزی را تایید کرد.'; 
            $this->link = route('std-accepted.emp.list.request.intern');
        }elseif($this->receiver->groupable_type == StudentInfo::class){
            $this->message = 'دانشجو' . ' ' . $this->receiver->name . ' ' . $this->receiver->family . ' ' . 'درخواست همکاری ( پویش ) را تایید کرد';
            $this->link = route('co-accepted.emp.list.request.intern');
        }
    }
}
