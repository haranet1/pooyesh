<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\UniAcceptedJobPosition as MailUniAcceptedJobPosition;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Mpdf\Tag\Th;

class UniAcceptedJobPosition extends Notification
{
    use Queueable;

    public $jobPosition;
    public $link;
    public $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($job)
    {
        $this->jobPosition = $job;
        $this->getMessage();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailUniAcceptedJobPosition($this->message , $this->link));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message,
            'link' => $this->link,
        ];
    }

    private function getMessage()
    {
        if($this->jobPosition->type == 'intern'){
            $this->message = 'موقعیت کارآموزی' . ' ' . $this->jobPosition->title . ' ' . 'تایید شد.';
            $this->link = route('job' , $this->jobPosition->id);
        }else{
            $this->message = 'موقعیت استخدامی' . ' ' . $this->jobPosition->title . ' ' . 'تایید شد.';
            $this->link = route('job' , $this->jobPosition->id);
        }
    }
}
