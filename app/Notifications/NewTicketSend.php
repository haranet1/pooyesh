<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\NewTicketSend as MailNewTicketSend;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class NewTicketSend extends Notification 
{
    use Queueable;

    public $link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($link)
    {
        $this->link = $link;
        
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->sendEmail($notifiable);
        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailNewTicketSend($this->link));
    }

    public function toDatabase()
    {
        return [
            'message' => 'تیکت جدید دریافت کرده اید',
            'link' => $this->link,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
