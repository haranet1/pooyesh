<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\UniAcceptInternRequest as MailUniAcceptInternRequest;
use App\Models\CompanyInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UniAcceptInternRequest extends Notification
{
    use Queueable;

    public $user;
    public $req;
    public $jobName;
    public $message;
    public $link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($req,$user,$job)
    {
        $this->jobName = $job->title;
        $this->user = $user;
        $this->req = $req;
        $this->getMessage();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailUniAcceptInternRequest($this->message , $this->link));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message,
            'link' => $this->link,
        ];
    }

    private function getMessage()
    {
        $this->message = 'درخواست کارآموزی '. $this->jobName .' توسط دانشگاه تایید شد و در انتظار مراجعه دانشجو به دبیرخانه می‌باشد';
        if($this->user->groupable_type == CompanyInfo::class){
            $this->link = route('co.accepted.intern.req.job');
        }else{
            if($this->req->sender){
                $this->link = route('std.sent.intern.req.job');
            }else{
                $this->link = route('std.received.list.request.intern');
            }
        }
    }
}
