<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\SentRequest as MailSentRequest;
use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NunoMaduro\Collision\Contracts\Highlighter;

class SentRequest extends Notification implements ShouldQueue
{
    use Queueable;

    public $jobPosition;
    public $sender;
    public $message;
    public $link;
    public $subject;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($sender , $job)
    {
        $this->sender = $sender;
        $this->jobPosition = $job;
        $this->getMessage();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailSentRequest($this->message , $this->link , $this->subject));
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message,
            'link' => $this->link,
        ];
    }

    private function getMessage()
    {
        if($this->sender->groupable_type == CompanyInfo::class){
            if($this->jobPosition->type == 'intern'){
                $this->message = 'شرکت' . ' ' . $this->sender->groupable->name . ' ' . 'با موقعیت کارآموزی' . ' ' . $this->jobPosition->title . ' ' . 'به شما درخواست ارسال کرد.';
                $this->link = route('std.received.intern.req.job');
                $this->subject = 'درخواست کارآموزی';
            }else{
                $this->message = 'شرکت' . ' ' . $this->sender->groupable->name . ' ' . 'با موقعیت استخدامی' . ' ' . $this->jobPosition->title . ' ' . 'به شما درخواست ارسال کرد.';
                $this->link = route('std.received.hire.req.job');
                $this->subject = 'درخواست استخدام';
            }
        }elseif($this->sender->groupable_type == StudentInfo::class){
            if($this->jobPosition->type == 'intern'){
                $this->message = 'دانشجو' . ' ' . $this->sender->name . ' ' . $this->sender->family . ' ' . 'به موقعیت کارآموزی' . ' ' . $this->jobPosition->title. ' ' . 'درخواست ارسال کرد.';
                $this->link = route('co.received-by-id.intern.req.job' , $this->jobPosition->id);
                $this->subject = 'درخواست کارآموزی';
            }else{
                $this->message = 'دانشجو' . ' ' . $this->sender->name . ' ' . $this->sender->family . ' ' . 'به موقعیت استخدامی' . ' ' . $this->jobPosition->title. ' ' . 'درخواست ارسال کرد.';
                $this->link = route('co.received-by-id.hire.req.job' , $this->jobPosition->id);
                $this->subject = 'درخواست استخدام';
            }   
        }else{
            $this->message = 'کارجو' . ' ' . $this->sender->name . ' ' . $this->sender->family . ' ' . 'به موقعیت استخدامی' . ' ' . $this->jobPosition->title. ' ' . 'درخواست ارسال کرد.';
            $this->link = route('co.received-by-id.hire.req.job' , $this->jobPosition->id);
            $this->subject = 'درخواست استخدام';
        }
        
    }
}
