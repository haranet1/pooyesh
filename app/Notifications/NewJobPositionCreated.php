<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\NewJobPositionCreated as MailNewJobPositionCreated;
use App\Models\Job_position;
use App\Models\OjobPosition;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class NewJobPositionCreated extends Notification
{
    use Queueable;
    public $jobPosition;
    public $message;
    public $link;
    public $isIntern;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(OjobPosition $job_position)
    {
        $this->jobPosition = $job_position;
        $this->getMessage();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailNewJobPositionCreated($this->message, $this->link , $this->isIntern));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message,
            'link' => $this->link,
        ];
    }

    private function getMessage()
    {
        if($this->jobPosition->type == 'intern'){
            $this->message = 'موقعیت کارآموزی' . ' ' . $this->jobPosition->title. ' ' . 'توسط شرکت' . ' ' . $this->jobPosition->company->groupable->name . ' ' . 'ثبت شد.';
            $this->link = route('emp.un.intern.pos.job');
            $this->isIntern = true;
        }else{
            $this->message = 'موقعیت استخدامی' . ' ' . $this->jobPosition->title. ' ' . 'توسط شرکت' . ' ' . $this->jobPosition->company->groupable->name . ' ' . 'ثبت شد.';
            $this->link = route('emp.un.hire.pos.job');
            $this->isIntern = false;
        }
    }
}
