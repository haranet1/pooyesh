<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\CancleInternRequestForUni as MailCancleInternRequestForUni;
use App\Models\CompanyInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CancleInternRequestForUni extends Notification
{
    use Queueable;

    public $user;
    public $request;
    public $message;
    public $link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $req)
    {
        $this->user = $user;
        $this->request = $req;
        $this->getMessage();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailCancleInternRequestForUni($this->message , $this->link));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message,
            'link' => $this->link,
        ];
    }

    private function getMessage()
    {
        if($this->user->groupable_type == CompanyInfo::class){
            $this->message = 'شرکت'. ' ' . $this->user->groupable->name . ' ' . 'درخواست کارآموزی' . ' ' . $this->request->job->title . ' ' . 'را لغو کرد';
            $this->link = route('std-canceled.emp.list.request.intern');
        }else{
            $this->message = 'دانشجو' . ' ' . $this->user->name . ' ' . $this->user->family . ' ' . 'درخواست کارآموزی' . ' ' . $this->request->job->title . ' ' . 'را لغو کرد';
            $this->link = route('co-canceled.emp.list.request.intern');
        }
    }
}
