<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\RejectInternRequestForUni as MailRejectInternRequestForUni;
use App\Models\CompanyInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RejectInternRequestForUni extends Notification
{
    use Queueable;

    public $message;
    public $subject;
    public $link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($subject , $message , $link)
    {
        $this->subject = $subject;
        $this->message = $message;
        $this->link = $link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailRejectInternRequestForUni($this->subject , $this->message , $this->link));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->subject,
            'link' => $this->link
        ];
    }
}
