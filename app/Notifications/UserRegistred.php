<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\UserRegistred as MailUserRegistred;
use App\Models\ApplicantInfo;
use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserRegistred extends Notification
{
    use Queueable;
    public $user;
    public $message;
    public $link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->makeMessage();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailUserRegistred($this->message , $this->link));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message,
            'link' => $this->link,
        ];
    }

    private function makeMessage()
    {
        if($this->user->groupable_type === CompanyInfo::class){
            $this->message = 'شرکت'. ' ' . $this->user->groupable->name . ' ' . ' به لیست شرکت های تایید نشده اضافه شد.';
            $this->link = route('company.un.user');

        }elseif($this->user->groupable_type === StudentInfo::class){
            $this->message = ' دانشجو'. ' ' . $this->user->name . ' ' . $this->user->family . ' ' . ' به لیست دانشجویان تایید نشده اضافه شد.';
            $this->link = route('student.un.user');
        }
        elseif($this->user->groupable_type === ApplicantInfo::class){
            $this->message = ' کارجو'. ' ' . $this->user->name . ' ' . $this->user->family . ' ' . ' به لیست کارجویان تایید نشده اضافه شد.';
            $this->link = route('applicant.un.user');

        }else{
            $this->message = ' کارمند'. ' ' . $this->user->name . ' ' . $this->user->family . ' ' . ' وارد سامانه شد.';
            $this->link = route('dashboard');
        }
    }
}
