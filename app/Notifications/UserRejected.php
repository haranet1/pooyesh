<?php

namespace App\Notifications;

use App\Jobs\SendDynamicEmail;
use App\Mail\UserRejected as MailUserRejected;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserRejected extends Notification
{
    use Queueable;
    public $user;
    public $subject;
    public $message;
    public $link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user , $subject , $content , $link)
    {
        $this->user = $user;
        $this->subject = $subject;
        $this->message = $content;
        $this->link = $link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->sendEmail($notifiable);

        return ['database'];
    }

    public function sendEmail($notifiable)
    {
        return SendDynamicEmail::dispatch($notifiable , new MailUserRejected($this->subject , $this->message , $this->link));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->subject,
            'link' => $this->link,
        ];
    }
}
