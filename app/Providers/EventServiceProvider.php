<?php

namespace App\Providers;

use App\Events\CompanyRegistered;
use App\Events\IdeaCreated;
use App\Events\JobPositionCreated;
use App\Events\ProjectCreated;
use App\Events\SadraEventCreated;
use App\Events\UserRegistered;
use App\Listeners\CompanyRegisteredListener;
use App\Listeners\IdeaCreatedListener;
use App\Listeners\JobPositionCreatedListener;
use App\Listeners\LogSendingMessage;
use App\Listeners\ProjectCreatedListener;
use App\Listeners\SadraEventCreatedListener;
use App\Listeners\SendVerificationEmail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        UserRegistered::class => [
            SendVerificationEmail::class,
        ],
        // Registered::class => [
        //     SendEmailVerificationNotification::class,
        // ],
        CompanyRegistered::class => [
            CompanyRegisteredListener::class
        ],
        IdeaCreated::class => [
          IdeaCreatedListener::class
        ],
        JobPositionCreated::class =>[
            JobPositionCreatedListener::class
        ],
        ProjectCreated::class =>[
            ProjectCreatedListener::class
        ],
        SadraEventCreated::class => [
            SadraEventCreatedListener::class
        ],
        MessageSending::class => [
            LogSendingMessage::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
