<?php

namespace App\Providers;

use App\Models\ArticleCategory;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Services\Payment\PaymentGatewayFactory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PaymentGatewayFactory::class, function ($app) {
            return new PaymentGatewayFactory();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer('frontend.layout.master', function ($view) {
            $categories = ArticleCategory::where('rank',1)->get();
            $view->with('categories', $categories);
        });
//        if($this->app->environment('production')) {
//            \URL::forceScheme('https');
//        }

    }
}
