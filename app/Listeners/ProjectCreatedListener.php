<?php

namespace App\Listeners;

use App\Events\ProjectCreated;
use App\Models\News;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ProjectCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ProjectCreated  $event
     * @return void
     */
    public function handle(ProjectCreated $event)
    {
        News::create([
            'title' => 'یک پروژه جدید با عنوان '.$event->project_title.' توسط '.$event->project_owner.' در سامانه ثبت شد.',
            'expired_at'=> now()->addWeeks(2)
        ]);
    }
}
