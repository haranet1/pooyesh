<?php

namespace App\Listeners;

use App\Events\CompanyRegistered;
use App\Models\News;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CompanyRegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CompanyRegistered  $event
     * @return void
     */
    public function handle(CompanyRegistered $event)
    {
        $company =$event->company;
        News::create([
            'title'=>'شرکت '.$company.' به سامانه پویش ملحق شد.',
            'expired_at'=> now()->addMonth()
        ]);
    }
}
