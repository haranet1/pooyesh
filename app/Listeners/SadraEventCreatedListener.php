<?php

namespace App\Listeners;

use App\Events\SadraEventCreated;
use App\Models\News;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SadraEventCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SadraEventCreated  $event
     * @return void
     */
    public function handle(SadraEventCreated $event)
    {
        News::create([
            'title'=>'رویداد صدرا با عنوان '.$event->sadra.' در سامانه ثبت شد.',
            'expired_at'=> $event->expire_date
        ]);
    }
}
