<?php

namespace App\Listeners;

use App\Events\IdeaCreated;
use App\Models\News;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class IdeaCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\IdeaCreated  $event
     * @return void
     */
    public function handle(IdeaCreated $event)
    {
        News::create([
            'title' => 'ایده جدید با عنوان '.$event->idea_title.' در سامانه ثبت شد.',
            'expired_at' => now()->addWeeks(3)
        ]);
    }
}
