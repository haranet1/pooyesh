<?php

namespace App\Listeners;

use App\Events\JobPositionCreated;
use App\Models\News;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class JobPositionCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\JobPositionCreated  $event
     * @return void
     */
    public function handle(JobPositionCreated $event)
    {
        $title = $event->job_title;
        $company = $event->company;
        News::create([
            'title'=>'یک موقعیت شغلی جدید با عنوان '.$title.' توسط شرکت '.$company.' در سامانه ثبت شد.',
            'expired_at'=>now()->addWeeks(3)
        ]);
    }
}
