<?php

namespace App\Enums;
/**
 * @method static self DONE()
 * @method static self EXEMPT()
 * @method static self STUDENT()
 * @method static self DOING()
 * @method static self INCLUDED()
 * @method static self OTHER()
 */
class MilitaryStatus extends \Spatie\Enum\Enum
{
    protected static function values(): array
    {
        return [
            'DONE' => 'پایان خدمت',
            'EXEMPT' => 'معافیت دائم',
            'STUDENT' => 'معافیت تحصیلی',
            'DOING' => 'درحال انجام',
            'INCLUDED' => 'مشمول',
            'OTHER' => 'سایر'
        ];
    }
}
