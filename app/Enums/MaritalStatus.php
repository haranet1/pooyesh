<?php

namespace App\Enums;
/**
 * @method static self SINGLE()
 * @method static self MARRIED()
 * @method static self OTHER()
 */
class MaritalStatus extends \Spatie\Enum\Enum
{
    protected static function values(): array
    {
        return [
            'SINGLE' => 'مجرد',
            'MARRIED' => 'متاهل',
            'OTHER' =>'سایر'
        ];
    }
}
