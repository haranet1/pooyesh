<?php

namespace App\Exports;

use App\Models\company_sadra;
use App\Models\Sadra;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExhibitionRegisteredCompanies implements FromCollection,WithHeadings,WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public $event_id;

    public function __construct($event_id)
    {
        $this->event_id = $event_id;
    }

    public function collection()
    {
        $events = company_sadra::where('sadra_id',$this->event_id)->has('receipt')->with('company','sadra')->get();

        $companies = new Collection();
        foreach ($events as $event){
            $companies->push($event->company);
        }
        return $companies;
    }

    public function map($companies):array
    {
        return [
            $companies->name." ".$companies->family,
            $companies->mobile,
            $companies->groupable->name,
            $companies->groupable->activity_field,
            $companies->groupable->ceo,
            $companies->groupable->phone,
            $companies->email,
            $companies->groupable->website,
        ];
    }

    public function headings():array
    {
        return [
            'نام و نام خانوادگی کاربر',
            'تلفن کاربر',
            'نام شرکت',
            'حوزه فعالیت',
            'مدیرعامل',
            'تلفن شرکت',
            'ایمیل شرکت',
            'وبسایت',
        ];
    }

}
