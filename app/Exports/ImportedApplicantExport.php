<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ImportedApplicantExport implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $applicantUsers = User::Imported()->whereHas('roles', function ($query) {
            $query->where('name', 'applicant');
        })->get();
        
        return $applicantUsers;
    }

    public function map($users):array
    {
        if($users->sex && $users->email){
            return [
                $users->name,
                $users->family,
                $users->mobile,
                $users->sex,
                $users->email,
            ];
        }else{
            return [
                $users->name,
                $users->family,
                $users->mobile,
                'تایین نشده',
                'تایین نشده',
            ];
        }
        
    }

    public function headings():array
    {
        return [
            'name',
            'family',
            'mobile',
            'sex',
            'email',
        ];
    }
}
