<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UnconfirmedApplicantExport implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::Applicant()->UnConfirmed()->with('groupable')->get();
    }

    public function map($users):array
    {
        if($users->groupable->university && $users->groupable->city){
            return [
                $users->name,
                $users->family,
                $users->mobile,
                $users->groupable->age,
                $users->groupable->university->name,
                $users->groupable->province->name,
                $users->groupable->city->name,
                $users->groupable->major,
                $users->groupable->email,
                $users->groupable->skills,
                $users->groupable->about,
    
            ];
        }else{
            return [
                $users->name,
                $users->family,
                $users->mobile,
                $users->groupable->age,
                'تایین نشده',
                $users->groupable->province->name,
                'تایین نشده',
                $users->groupable->major,
                $users->groupable->email,
                $users->groupable->skills,
                $users->groupable->about,
            ];
        }
        
    }

    public function headings():array
    {
        return [
            'name',
            'family',
            'mobile',
            'age',
            'university',
            'province',
            'city',
            'major',
            'email',
            'skills',
            'about',
        ];
    }
}
