<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DynamicCompanyExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize,WithTitle
{
    private $title;
    private $type;

    /**
     * @var $type = un | accepted | rejected
     */
    public function __construct(string $type , string $title)
    {
        $this->title = $title;
        $this->type = $type;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $type = '';
        if($this->type == 'un') $type = 'UnConfirmed';

        elseif($this->type == 'accepted') $type = 'Confirmed';

        elseif($this->type == 'rejected') $type = 'Rejected';

        elseif($this->type == 'RejectCompanyInfo') $type = 'RejectCompanyInfo';
            
        
        return User::company()->$type()->with('groupable')->get();
    }

    public function title(): string
    {
        return $this->title;
    }

    public function map($co): array
    {
        $address = '- -';

        if(! is_null($co->groupable->address)){
            $address = $co->groupable->address;
        }

        return [
            $co->name,
            $co->family,
            $co->mobile,
            $co->groupable->name,
            $co->groupable->n_code,
            $co->groupable->registration_number,
            $co->groupable->activity_field,
            $co->groupable->ceo,
            $co->groupable->year,
            $co->groupable->phone,
            $co->groupable->email,
            $co->groupable->website,
            $address,
        ];
    }

    public function headings(): array
    {
        return [
            'نام (نماینده)',
            'فامیل (نماینده)',
            'موبایل (نماینده)',
            'نام شرکت',
            'کد ملی شرکت',
            'کد ثبت',
            'زمینه فعالیت',
            'مدیر عامل',
            'سال تاسیس',
            'تلفن شرکت',
            'ایمیل شرمت',
            'وب سایت',
            'آدرس',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}
