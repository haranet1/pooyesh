<?php

namespace App\Exports;

use App\Models\OjobPosition;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DynamicInternPositionExport implements FromCollection,WithTitle,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    private $title;
    private $type;

    public function __construct(string $type, string $title)
    {
        $this->title = $title;
        $this->type = $type;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $type = '';
        if($this->type == 'un') $type = 'UnConfirmed';

        elseif($this->type == 'accepted') $type = 'Confirmed';

        elseif($this->type == 'rejected') $type = 'Rejected';

        return OjobPosition::Intern()->$type()->with('job' , 'company')->get();
    }

    public function title(): string
    {
        return $this->title;
    }

    public function map($job): array
    {
        $co_name = '- -';
        if($job->company->groupable){
            $co_name = $job->company->groupable->name;
        }
        return [
            $job->title,
            $co_name,
            $job->location,
            $job->salary,
            verta($job->created_at)->formatDate(),
        ];
    }

    public function headings(): array
    {
        return [
            'عنوان',
            'نام شرکت',
            'محل',
            'حقوق',
            'تاریخ ایجاد',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}
