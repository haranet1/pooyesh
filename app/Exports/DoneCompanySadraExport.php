<?php

namespace App\Exports;

use App\Models\company_sadra;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DoneCompanySadraExport implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $sadra_id;

    public function __construct($sadra_id)
    {
        $this->sadra_id = $sadra_id;
    }
    

    public function collection()
    {
        return company_sadra::where('sadra_id', $this->sadra_id)->has('payedPayment')->with('companyInfo','companyInfo.user','payedPayment','sadra','booth.plan')->get();
    }

    public function map($companySadra):array
    {
        return [
            $companySadra->sadra->title,
            $companySadra->companyInfo->name,
            $companySadra->companyInfo->ceo,
            $companySadra->booth->name,
            $companySadra->booth->plan->price,
            $companySadra->payedPayment->tracking_code,
            $companySadra->payedPayment->receipt,
            route('company.single', $companySadra->companyInfo->user->id),
            asset($companySadra->companyInfo->logo->path),
            $companySadra->inviter,
            $companySadra->coordinator_name,
            $companySadra->coordinator_mobile,
            $companySadra->contract,
            
        ];
    }

    public function headings():array
    {
        return [
            'نام رویداد',
            'نام شرکت',
            'مدیر عامل',
            'نام غرفه',
            'هزینه غرفه',
            'کد پیگیری',
            'رسید پرداخت',
            'لینک حاوی qr code',
            'لینک حاوی لوگو',
            'معرف',
            'نام هماهنگ کننده شرکت',
            'شماره همراه هماهنگ کننده شرکت',
            'نوع قرار داد',
        ];
    }

}
