<?php

namespace App\Exports;

use App\Models\CompanyLogActivity;
use App\Models\EmployeeLogActivity;
use App\Models\StudentLogActivity;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ActivityLog implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }


    public function collection()
    {
        switch ($this->model){
            case'co':
                $model = CompanyLogActivity::class;
                break;

            case'std':
                $model = StudentLogActivity::class;
                break;

            case 'emp':
                $model = EmployeeLogActivity::class;
                break;
        }

       return  $logs = $model::with('user')->orderBy('id','desc')->get()->take(500);

    }

    public function map($logs):array
    {
        return [
            verta($logs->created_at)->formatJalaliDatetime(),
            $logs->user->name." ".$logs->user->family,
            $logs->ip,
            $logs->subject,
            $logs->url,
            $logs->method,
            $logs->agent,

        ];
    }

    public function headings():array
    {
        return [
            'تاریخ',
            'کاربر',
            'آی پی',
            'عنوان',
            'آدرس',
            'متود',
            'اطلاعات دستگاه',
        ];
    }
}
