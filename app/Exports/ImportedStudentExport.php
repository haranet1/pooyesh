<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ImportedStudentExport implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $studentUsers = User::Imported()->whereHas('roles', function ($query) {
            $query->where('name', 'student');
        })->get();
        
        return $studentUsers;
    }

    public function map($users):array
    {
        if($users->sex && $users->email){
            return [
                $users->name,
                $users->family,
                $users->mobile,
                $users->sex,
                $users->email,
            ];
        }else{
            return [
                $users->name,
                $users->family,
                $users->mobile,
                'تایین نشده',
                'تایین نشده',
            ];
        }
        
    }

    public function headings():array
    {
        return [
            'name',
            'family',
            'mobile',
            'sex',
            'email',
        ];
    }
}

    