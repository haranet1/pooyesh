<?php

namespace App\Exports;

use App\Models\CompanyPackage;
use Hekmatinasser\Verta\Facades\Verta;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CompanyPackageTransactionExport implements FromCollection,WithHeadings,WithMapping,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $request;

    public function __construct(array $request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $query = CompanyPackage::query();
        $query = $query->with('payments', 'companyInfo.user', 'package');

        if($this->request['mobile'] != '' || $this->request['mobile'] != null){
            $query->whereRelation('companyInfo.user' , 'mobile' , 'LIKE' , '%'. $this->request['mobile'] .'%');
        }

        if($this->request['type'] != '' || $this->request['type'] != null){
            if($this->request['type'] == 'full'){
                $query->whereRelation('package','additional_internship_positions' , '!=' , 0)
                    ->whereRelation('package','additional_internship_requests' , '!=' , 0)
                    ->whereRelation('package','additional_internship_confirms' , '!=' , 0);
            }else{
                $query->whereRelation('package', $this->request['type'], '!=', 0);
            }
        }

        if($this->request['status'] != '' || $this->request['status'] != null){
            if($this->request['status'] == 1){
                $query->whereNotNull('receipt_id');
            }else{
                $query->whereNull('receipt_id');
            }
        }

        if(!empty($this->request['start_date']) && !empty($this->request['end_date'])){
            $startDate = Verta::parse($this->request['start_date'])->toCarbon();
            $endDate = Verta::parse($this->request['end_date'])->toCarbon();
            
            $query->whereBetween('updated_at' , [$startDate, $endDate]);
        }

        return $query->get();
    }

    public function map($row): array
    {
        $price = 0;
        $message = '';
        $receipt = '';
        $payment_date_time = '';
        foreach($row->payments as $payment){
            if(! is_null($payment->receipt)){
                $price = $payment->price;
                $message = $payment->status;
                $receipt = $payment->receipt;
                $payment_date_time = $payment->payment_date_time;
            }
        }
        return [
            $row->companyInfo->user->name. ' ' .$row->companyInfo->user->family,
            $row->companyInfo->user->mobile,
            $row->package->name,
            $price,
            $row->status == true ? 'پرداخت شده' : 'پرداخت نشده',
            $message ?? '- - -',
            (int)$receipt ?? '- - -',
            $payment_date_time ?? '- - -',
        ];
    }

    public function headings(): array
    {
        return [
            'نام و نام خانوادگی',
            'موبایل',
            'عنوان پکیج',
            'مبلغ (تومان)',
            'وضعیت پرداخت',
            'پیام دریافتی',
            'رسید',
            'تاریخ پرداخت',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $headerStyleArray = [
            'font' => [
                'bold' => true,
                // 'color' => 'ffffff'
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '0096FF', // Blue color
                ],
            ],
        ];

        // Apply the header style
        $sheet->getStyle('A1:L1')->applyFromArray($headerStyleArray);


        return [];
    }
}
