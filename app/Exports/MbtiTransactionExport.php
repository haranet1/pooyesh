<?php

namespace App\Exports;

use App\Models\SuggestedTestJob;
use Hekmatinasser\Verta\Facades\Verta;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class MbtiTransactionExport implements FromCollection,WithHeadings,WithMapping,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $request;

    public function __construct(array $request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $query = SuggestedTestJob::query();
        $query = $query->with('payments', 'buyer', 'discount');

        if($this->request['mobile'] != '' || $this->request['mobile'] != null){
            $query->whereRelation('buyer' , 'mobile' , 'LIKE' , '%'. $this->request['mobile'] .'%');
        }

        if($this->request['option'] != '' || $this->request['option'] != null){
            $query->where('option' , $this->request['option']);
        }

        if($this->request['status'] != '' || $this->request['status'] != null){
            if($this->request['status'] == 1){
                $query->whereNotNull('receipt_id');
            }else{
                $query->whereNull('receipt_id');
            }
        }

        if(!empty($this->request['start_date']) && !empty($this->request['end_date'])){
            $startDate = Verta::parse($this->request['start_date'])->toCarbon();
            $endDate = Verta::parse($this->request['end_date'])->toCarbon();
            
            $query->whereBetween('updated_at' , [$startDate, $endDate]);
        }

        return $query->get();
    }

    public function map($row): array
    {
        $price = 0;
        $message = '';
        $receipt = '';
        $payment_date_time = '';
        foreach($row->payments as $payment){
            if(! is_null($payment->receipt)){
                $price = $payment->price;
                $message = $payment->status;
                $receipt = $payment->receipt;
                $payment_date_time = $payment->payment_date_time;
            }
        }

        return [
            $row->buyer->name. ' ' .$row->buyer->family,
            $row->buyer->mobile,
            getTypeOfSuggestedTestToFa($row->option),
            $price,
            $row->discount_price ?? '- - -',
            $row->status == true ? 'پرداخت شده' : 'پرداخت نشده',
            $message ?? '- - -',
            $receipt ?? '- - -',
            $payment_date_time ?? '- - -',
        ];
    }

    public function headings(): array
    {
        return [
            'نام و نام خانوادگی',
            'موبایل',
            'نوع خرید',
            'مبلغ (تومان)',
            'تخفیف',
            'وضعیت پرداخت',
            'پیام دریافتی',
            'رسید',
            'تاریخ پرداخت',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $headerStyleArray = [
            'font' => [
                'bold' => true,
                // 'color' => 'ffffff'
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '0096FF', // Blue color
                ],
            ],
        ];

        // Apply the header style
        $sheet->getStyle('A1:L1')->applyFromArray($headerStyleArray);


        return [];
    }
}
