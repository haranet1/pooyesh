<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class StdPooyeshReqListExport implements FromCollection,WithMapping,WithHeadings,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::InternRequestSender()
            ->Student()
            ->with('sendRequest.receiver.groupable','sendRequest.job')
            ->get();
    }

    public function map($std): array
    {
        $student = '-';
        $n_code = '-';
        $company = '-';
        $job = '-';
        $date = '-';
        foreach($std->sendRequest as $send){
            
            if($send->type == 'intern'){
                $student = $std->name.' '. $std->family;
                if($send->receiver->groupable){
                    $company = $send->receiver->groupable->name;
                }
                if($send->job){
                    $job = $send->job->title;
                }
                $date = verta($send->created_at)->formatDate();

                return[
                    $student,
                    $n_code => $std->n_code ?? $std->groupable->n_code,
                    $company,
                    $job,
                    $date,
                ] ;
            }
        }
        
    }

    public function headings(): array
    {
        return [
            'دانشجو',
            'کد ملی',
            'شرکت',
            'موقعیت شغلی',
            'تاریخ ارسال',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}
