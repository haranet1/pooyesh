<?php

namespace App\Exports;

use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DoneReqListExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'done_at'){
                array_push($ids , $request->id);
            }
        }
        return CooperationRequest::whereIn('id' , $ids)
            ->with('sender.groupable','receiver','process')
            ->get();
    }

    public function map($req): array
    {
        $sender = '-';
        $sender_type = '';
        $receiver = '-';
        $receiver_type = '';
        $job = '-';
        $date = '-';
        
        if($req->sender->groupable instanceof CompanyInfo){
            $sender_type = 'شرکت';
        }else{
            $sender_type = 'دانشجو';
        }
        $sender = $sender_type.' '.$req->sender->name.' '.$req->sender->family;
        if($req->receiver->groupable){
            
            if($req->receiver->groupable instanceof CompanyInfo){
                $receiver_type = 'شرکت';
            }else{
                $receiver_type = 'دانشجو';
            }
            $receiver = $receiver_type.' '.$req->receiver->groupable->name;
        }
        if($req->job){
            $job = $req->job->title;
        }
        $date = verta($req->created_at)->formatDate();
        return[
            $sender,
            $receiver,
            $job,
            $date,
        ] ;
    }

    public function headings(): array
    {
        return [
            'فرستنده',
            'گیرنده',
            'موقعیت کارآموزی',
            'تاریخ ارسال',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}
