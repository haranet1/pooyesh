<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Spatie\Permission\Traits\HasRoles;

class AllStudentsHasMbtiExport implements FromCollection,WithMapping,WithHeadings
{
    use HasRoles;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $students = collect();
        $users = User::with('groupable','mbti_test')->get();
        foreach ($users as $user){
            if($user->hasRole('student')){
                if(!$user->groupable){
                    $user->n_code = $user->n_code;
                    $user->mobile = $user->mobile;
                }
                if($user->mbti_test){
                    $user->mbti_result = $user->mbti_test->personality_type;
                    $user->mbti_result_fa = get_mbti_prsonality_to_fa($user->mbti_test->personality_type);
                }
                $students->push($user);
            }
        }
        return $students;

    }

    public function map($users):array
    {
        return [
            $users->name,
            $users->family,
            $users->mobile,
            $users->n_code,
            $users->mbti_result,
            $users->mbti_result_fa,
        ];
    }

    public function headings():array
    {
        return [
            'name',
            'family',
            'mobile',
            'n_code',
            'mbti_result',
            'mbti_result_fa',
        ];
    }
}
