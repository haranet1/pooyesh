<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Spatie\Permission\Traits\HasRoles;

class AllCompaniesExport implements FromCollection,WithMapping,WithHeadings
{
    use HasRoles;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $companies = collect();
        $users = User::with('groupable')->get();
        foreach($users as $user){
            if($user->hasRole('company')){
                if($user->groupable){
                    $user->company_name = $user->groupable->name;
                    $user->company_activity_field = $user->groupable->activity_field;
                    $user->company_ceo = $user->groupable->ceo;
                }else{
                    $user->company_name = '';
                    $user->company_activity_field = '';
                    $user->company_ceo = '';
                }
                $companies->push($user);
            }
        }
        return $companies;
    }

    public function map($users):array
    {
        return [
            $users->name,
            $users->family,
            $users->mobile,
            $users->company_name,
            $users->company_activity_field,
            $users->company_ceo
        ];
    }

    public function headings():array
    {
        return [
            'name',
            'family',
            'mobile',
            'company-name',
            'company_activity_field',
            'company_ceo',
        ];
    }
}
