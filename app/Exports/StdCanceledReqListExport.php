<?php

namespace App\Exports;

use App\Models\CooperationRequest;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class StdCanceledReqListExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'canceled_by_co_at'){
                array_push($ids , $request->id);
            }
        }
        return CooperationRequest::whereIn('id' ,$ids)
            ->with('sender' , 'receiver.groupable' , 'process')
            ->get();
    }

    public function map($req): array
    {
        $student = '-';
        $n_code = '-';
        $company = '-';
        $job = '-';
        $date = '-';
        $student = $req->sender->name.' '.$req->sender->family;
        if($req->receiver->groupable){
            $company = $req->receiver->groupable->name;
        }
        if($req->job){
            $job = $req->job->title;
        }
        $date = verta($req->created_at)->formatDate();
        return[
            $student,
            $n_code => $req->sender->n_code ?? $req->sender->groupable->n_code,
            $company,
            $job,
            $date,
        ] ;
    }

    public function headings(): array
    {
        return [
            'دانشجو',
            'کد ملی',
            'شرکت',
            'موقعیت کارآموزی',
            'تاریخ ارسال',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}
