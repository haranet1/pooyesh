<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DynamicStudentExport implements FromCollection,WithTitle,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    private $title;
    private $type;

    public function __construct(string $type, string $title)
    {
        $this->title = $title;
        $this->type = $type;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $type = '';
        if($this->type == 'un') $type = 'UnConfirmed';

        elseif($this->type == 'accepted') $type = 'Confirmed';

        elseif($this->type == 'rejected') $type = 'Rejected';

        return User::Student()->$type()->with('groupable')->get();
    }

    public function title(): string
    {
        return $this->title;
    }

    public function map($std): array
    {
        $n_code = '- -';
        $province = '- -';
        $city = '- -';
        $grade = '- -';
        $major = '- -';
        $email = '- -';


        if($std->n_code){
            $n_code = $std->n_code;
        }elseif($std->groupable->n_code){
            $n_code = $std->groupable->n_code;
        }

        if($std->groupable->province){
            $province = $std->groupable->province->name;
        }
        if($std->groupable->city){
            $city = $std->groupable->city->name;
        }
        if($std->groupable->grade){
            $grade = $std->groupable->grade;;
        }
        if($std->groupable->major){
            $major = $std->groupable->major;
        }
        if(! is_null($std->email)){
            $email = $std->email;
        }

        return [
            $std->name,
            $std->family,
            $std->mobile,
            $n_code,
            $province,
            $city,
            $grade,
            $major,
            $email, 
        ];
    }

    public function headings(): array
    {
        return [
            'نام',
            'نام خانوادگی',
            'موبایل',
            'کد ملی',
            'استان',
            'شهر',
            'مقطع',
            'رشته',
            'ایمیل',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}
