<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class MbtiTestUsersExport implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::unconfirmed()->whereHas('mbti_test')->with('mbti_test')->get();
    }

    public function map($users):array
    {
        $sex = 'ثبت نشده';
        $email = 'ثبت نشده';
        $n_code = 'ثبت نشده';
        $role = 'ثبت نشده';
        $resultTest = 'ثبت نشده';
        if($users->sex){
            $sex = $users->sex;
        }
        if($users->email){
            $email = $users->email;
        }
        if($users->n_code){
            $n_code = $users->n_code;
        }
        if(user_has_role_fa($users)){
            $role = user_has_role_fa($users);
        }
        if($users->mbti_test){
            $resultTest = $users->mbti_test->personality_type;
        }
        return [
            $users->name,
            $users->family,
            $users->mobile,
            $sex,
            $email,
            $n_code,
            $role,
            $resultTest,
        ];
    }

    public function headings():array
    {
        return [
            'name',
            'family',
            'mobile',
            'sex',
            'email',
            'n_code',
            'role',
            'resultTest',
        ];
    }
}
