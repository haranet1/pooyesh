<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DynamicApplicantExport implements FromCollection,WithTitle,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    private $title;
    private $type;

    public function __construct(string $type, string $title)
    {
        $this->title = $title;
        $this->type = $type;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $type = '';
        if($this->type == 'un') $type = 'UnConfirmed';

        elseif($this->type == 'accepted') $type = 'Confirmed';

        elseif($this->type == 'rejected') $type = 'Rejected';

        return User::Applicant()->$type()->with('groupable')->get();
    }

    public  function title(): string
    {
        return $this->title;
    }

    public function map($app): array
    {
        $n_code = '- -';
        $province = '- -';
        $city = '- -';
        $email = '- -';

        if($app->n_code){
            $n_code = $app->n_code;
        }elseif($app->groupable->n_code){
            $n_code = $app->groupable->n_code;
        }

        if($app->groupable->province){
            $province = $app->groupable->province->name;
        }
        if($app->groupable->city){
            $city = $app->groupable->city->name;
        }

        if(! is_null($app->email)){
            $email = $app->email;
        }

        return [
            $app->name,
            $app->family,
            $app->mobile,
            $n_code,
            $province,
            $city,
            $email,
        ];
    }

    public function headings(): array
    {
        return [
            'نام',
            'نام خانوادگی',
            'موبایل',
            'کد ملی',
            'استان',
            'شهر',
            'ایمیل',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}
