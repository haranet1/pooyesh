<?php

namespace App\Exports;

use App\Models\CooperationRequest;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CoRejectedReqListExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'rejected_by_std_at'){
                array_push($ids , $request->id);
            }
        }
        return CooperationRequest::whereIn('id' , $ids)
            ->with('sender.groupable','receiver','process')
            ->get();
    }

    public function map($req): array
    {
        $student = '-';
        $n_code = '-';
        $company = '-';
        $job = '-';
        $date = '-';
        if($req->sender->groupable){
            $company = $req->sender->groupable->name;
        }
        if($req->receiver){
            $student = $req->receiver->name.' '.$req->receiver->family;
        }
        if($req->job){
            $job = $req->job->title;
        }
        $date = verta($req->created_at)->formatDate();
        return[
            $company,
            $student,
            $n_code => $req->receiver->n_code ?? $req->receiver->groupable->n_code,
            $job,
            $date,
        ] ;
    }

    public function headings(): array
    {
        return [
            'شرکت',
            'دانشجو',
            'کد ملی',
            'موقعیت کارآموزی',
            'تاریخ ارسال',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}
