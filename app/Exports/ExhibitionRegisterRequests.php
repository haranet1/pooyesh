<?php

namespace App\Exports;

use App\Models\company_sadra;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExhibitionRegisterRequests implements FromCollection,WithHeadings,WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $requests = company_sadra::with('sadra','company.groupable')->whereNull('status')->get();

    }

    public function map($requests):array
    {
        return [
            $requests->sadra->title,
            $requests->company->name." ".$requests->company->family,
            $requests->company->mobile,
            $requests->company->groupable->name,
            $requests->company->groupable->activity_field,
            $requests->company->groupable->registration_number,
            $requests->company->groupable->phone,
            $requests->company->email,
            $requests->company->groupable->website,
        ];
    }

    public function headings():array
    {
        return [
            'عنوان رویداد',
            'نام و نام خانوادگی',
            'موبایل',
            'نام شرکت',
            'حوزه فعالیت',
            'شماره ثبت',
            'تلفن شرکت',
            'ایمیل شرکت',
            'وبسایت',
        ];
    }
}
