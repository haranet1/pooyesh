<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CoPooyeshReqListExport implements FromCollection,WithMapping,WithHeadings,WithStyles,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::InternRequestSender()
            ->Company()
            ->with('sendRequest.receiver' ,'sendRequest.sender' , 'sendRequest.job')
            ->get();
    }

    public function map($co): array
    {
        $company = '-';
        $student = '-';
        $n_code = '-';
        $job = '-';
        $date = '-';
        foreach($co->sendRequest as $send){
            if($send->type == 'intern'){
                
                if($send->sender->groupable){

                    $company = $send->sender->groupable->name;
                }
                $student = $send->receiver->name.' '. $send->receiver->family;

                $job = $send->job->title;
                $date = verta($send->created_at)->formatDate();
                return([
                    $company,
                    $student,
                    $n_code => $send->receiver->n_code ?? $send->receiver->groupable->n_code,
                    $job,
                    $date,
                ]) ;
            }
        }
    }

    public function headings(): array
    {
        return [
            'شرکت',
            'دانشجو',
            'کد ملی',
            'موقعیت شغلی',
            'تاریخ ارسال',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}
