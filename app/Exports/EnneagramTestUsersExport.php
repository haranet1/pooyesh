<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class EnneagramTestUsersExport implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::unconfirmed()->whereHas('enneagram_test')->with('enneagram_test')->get();
    }

    public function map($users):array
    {
        $sex = 'ثبت نشده';
        $email = 'ثبت نشده';
        $role = 'ثبت نشده';
        $resultTest = 'ثبت نشده';
        if($users->sex){
            $sex = $users->sex;
        }
        if($users->email){
            $email = $users->email;
        }
        if(user_has_role_fa($users)){
            $role = user_has_role_fa($users);
        }
        if($users->enneagram_test){
            $resultTest = enneagram_result_fa($users->enneagram_test->main_character);
        }
        return [
            $users->name,
            $users->family,
            $users->mobile,
            $sex,
            $email,
            $role,
            $resultTest,

        ];
    }

    public function headings():array
    {
        return [
            'name',
            'family',
            'mobile',
            'sex',
            'email',
            'role',
            'resultTest',
        ];
    }
}
