<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class ImportedCompanyExport implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $companyUsers = User::Imported()->whereHas('roles', function ($query) {
            $query->where('name', 'company');
        })->get();
        
        return $companyUsers;
    }

    public function map($users):array
    {
        return [
            $users->name,
            $users->family,
            $users->mobile,
        ];
    }

    public function headings():array
    {
        return [
            'name',
            'family',
            'mobile',
        ];
    }
}


