<?php

namespace App\Exports;

use App\Models\ExtraUser;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class UnregisteredStudentsExport implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $students = collect();
        $extraUsers = ExtraUser::with('userHasNcode')->get();
        foreach($extraUsers as $extra){
            if($extra->userHasNcode == null){
                $students->push($extra);
            }
        }
        return $students;
    }

    public function map($users):array
    {
        return [
            $users->name,
            $users->family,
            $users->sex,
            $users->n_code,
            $users->mobile,
            $users->major,
            $users->grade,
            $users->email,
        ];
    }

    public function headings():array
    {
        return [
            'نام',
            'نام خانوادگی',
            'جنسیت',
            'کد ملی',
            'موبایل',
            'رشته',
            'مقطع',
            'ایمیل',
        ];
    }
    
}
