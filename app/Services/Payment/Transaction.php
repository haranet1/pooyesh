<?php
namespace App\Services\Payment;

use App\Models\Discount;
use App\Models\EventInfo;
use App\Models\EventTicket;
use App\Models\EventTicketCode;
use App\Models\Payment;
use App\Services\Payment\Gatewey\GatewayInterface;
use App\Services\Payment\Gatewey\Iaun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Transaction 
{
    private $request;

    private $basket;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function checkout()
    {
        $eventTicekt = $this->makeEventTicket();

        $payment = $this->makePayment($eventTicekt);

        return $this->gatewayFactory()->pay($eventTicekt , $payment);
        
    }

    protected function makeEventTicket()
    {

        $discount = [null, $this->getSubTotal()];

        if(! is_null($this->request->discount_id)){
            $discount = $this->getDiscountPrice();
        }

        return EventTicket::create([
            'user_id' => Auth::id(),
            'event_id' => $this->request->event_id,
            'event_info_id' => $this->request->info_id,
            'discount_id' => $this->request->discount_id,
            'discount_price' => $discount[0],
            'payable_price' => $discount[1],
            'count' => $this->request->multyBuy,
        ]);
    }

    protected function getDiscountPrice()
    {
        $discount = Discount::find($this->request->discount_id);

        $subTotal = $this->getSubTotal();
        
        if($discount->is_percent){
            
            $discount_price = ((int)$discount->value * (int)$subTotal) / 100;
            
            $payablePrice = (int)$subTotal - $discount_price;
            
        } else {

            $discount_price = (int)$discount->value;
            $payablePrice = (int)$subTotal - (int)$discount->value;

        }

        return [$discount_price,$payablePrice];
    }

    private function getSubTotal()
    {
        $info = EventInfo::with('plan')->find($this->request->info_id);

        $price = $info->plan->price;

        return (int)$price * (int)$this->request->multyBuy;
    }

    protected function makePayment($eventTicekt)
    {
        return Payment::create([
            'user_id' => Auth::id(),
            'order_id' => $eventTicekt->id,
            'order_type' => EventTicket::class,
            'price' => $eventTicekt->payable_price,
        ]);
    }

    private function gatewayFactory()
    {
        $gateway = [
            'iaun' => Iaun::class,
        ][$this->request->gateway];

        return resolve($gateway);
    }

    public function verify($paymentId)
    {
        $payment = Payment::with('order')->find($paymentId);

        $this->request->merge(['payment' => $payment]);
        
        $result = $this->gatewayFactory()->verify($this->request);
        
        if($result['status'] == GatewayInterface::TRANSACTION_FAILED) return false;

        return $this->checkResult($result, $payment);

    }

    private function checkResult($result, $payment)
    {
        if($result['data']['data']['pay_status'] == "2"){

            $payment->update([
                'status' => 'عملیات پرداخت الکترونیک با موفقیت انجام شده است.',
                'tracking_code' => $result['data']['data']['pay_tracenumber'],
                'receipt' => $result['data']['data']['pay_rrn'],
                'card_number' => $result['data']['data']['pay_cardnumber'],
                'payer_bank' => $result['data']['data']['pay_issuerbank'],
                'payment_date_time' => $result['data']['data']['pay_paymentdatetime'],
            ]);
            $payment->order->update([
                'status'=> true,
                'receipt' => $payment->receipt
            ]);
            $payment->order->info->update([
                'plan_counter' => $payment->order->info->plan_counter + $payment->order->count
            ]);

            if($payment->order->info->plan_counter == $payment->order->info->plan->count){
                $payment->order->info->update([
                    'sold_out' => true
                ]);
            }

            $this->makeTicketCodes($payment->order);

            return true;

        }else {

            $payment->update([
                'status' => $result['data']['data']['pay_dscstatus']
            ]);

            return false;

        }
    }

    protected function makeTicketCodes($eventTicekt)
    {
        $planCounter = $eventTicekt->info->plan_counter;
        
        for($i = 1; $i <= $eventTicekt->count; $i++){

            EventTicketCode::create([
                'event_ticket_id' => $eventTicekt->id,
                'code' => $eventTicekt->event->id.$eventTicekt->info->sans->id.$eventTicekt->info->plan->id.$planCounter
            ]);

            $planCounter--;
        }

        return true;
    }

    public function payWithoutCheckout(EventTicket $eventTicekt)
    {
        $payment = $this->makePayment($eventTicekt);

        return $this->gatewayFactory()->pay($eventTicekt , $payment);
    }

}