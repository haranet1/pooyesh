<?php
namespace App\Services\Payment;

use App\Models\EventTicket;
use App\Models\Payment;
use App\Services\Payment\Gatewey\Zarinpal;
use Illuminate\Http\Request;
use InvalidArgumentException;


class PaymentGatewayFactory
{
    public function create(string $gatewayType): PaymentGatewayInterface
    {
        switch ($gatewayType) {
            case 'iaun':
                return new IaunGateway();
            case 'zarinpal':
                return new Zarinpal();
            default:
                throw new InvalidArgumentException("درگاه غیرمجاز: $gatewayType");
        }
    }
}