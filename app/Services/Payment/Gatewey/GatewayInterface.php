<?php
namespace App\Services\Payment\Gatewey;

use App\Models\EventTicket;
use App\Models\Payment;
use Illuminate\Http\Request;

interface GatewayInterface
{
    const TRANSACTION_FAILED = 'transaction.failed';
    const TRANSACTION_SUCCESS = 'transaction.success';


    public function pay(array $config, Payment $payment);
    public function verify(Request $request);
    public function getName():string;
    
}