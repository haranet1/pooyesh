<?php
namespace App\Services\Payment\Gatewey;

use App\Models\EventTicket;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

use function PHPUnit\Framework\callback;

class Iaun implements GatewayInterface
{
    private $merchantID ;
    private $callBack ;
    private $token ;

    public function __construct()
    {
        $this->merchantID = [
            'username' => 'apifanyaruser',
            'password' => 'AapiUser@FanYar',
            'usertype' => 3,
        ];
    }

    public function pay(array $config, Payment $payment)
    {
        $this->getToken();

        $this->callBack = route('payment.varify' , ['gateway' => $this->getName(), 'paymentId' => $payment->id]);

        $authorized = $this->authorizedpayer();

        
        if($authorized['success'] == true){
            
            $started = $this->startPayment($eventTicket);

            if($started['success'] == true){

                $payment->update([
                    'payment_key' => $started['data']['payment_key'],
                ]);

                return $started['data']['payment_key'];
            }

        }

        return $this->transactionFailed();

    }

    private function getToken()
    {
        $response = Http::withOptions([
            'verify' => false
        ])->post('https://api.iaun.ac.ir/api/login' , $this->merchantID);

        $json = $response->json();

        $this->token = $json['data']['token'];
    }

    private function authorizedpayer()
    {
        $response = Http::withOptions([
                'verify' => false
            ])
            ->acceptJson()
            ->withToken($this->token)
            ->post('https://api.iaun.ac.ir/api/payments/authorizedpayer' , [
                'uid' => ''.Auth::id(),
                'paymentkind' => 50,
                'name' => Auth::user()->name,
                'lastname' => Auth::user()->family,
                'mobile' => Auth::user()->mobile
            ]);
        
        return $response->json();
    }

    private function startPayment($eventTicket)
    {
        $response = Http::withOptions([
                'verify' => false
            ])
            ->acceptJson()
            ->withToken($this->token)
            ->post('https://api.iaun.ac.ir/api/payments/startpayment', [
                'Accept' => 'application/json',
                'uid' => ''.Auth::id(),
                'amount' => $eventTicket->payable_price * 10,
                'appid' => 4,    //  3 = Code Application: RahiNoo
                'kinduser' => 3,
                'paymentkind' => 50,
                'paymenttitle' => 'خرید بلیط برای رویداد',
                'returnurl' => $this->callBack,
            ]);

        return $response->json();
    }

    private function transactionFailed()
    {
        return [
            'status' => self::TRANSACTION_FAILED
        ];
    }

    public function verify(Request $request)
    {

        $this->getToken();

        $response = Http::withOptions([
                'verify' => false
            ])
            ->acceptJson()
            ->withToken($this->token)
            ->post('https://api.iaun.ac.ir/api/payments/verify', [
                'Accept' => 'application/json',
                'payment_key' => $request->payment->payment_key,
            ]);
        
        $json = $response->json();

        return (int)$json['data']['pay_amount'] / 10 == (int)$request->payment->price
            
            ? $this->transactionSuccess($json)

            : $this->transactionFailed();
    }

    private function transactionSuccess($response)
    {
        return [
            'status' => self::TRANSACTION_SUCCESS,
            'data' => $response
        ];
    }

    public function getName():string
    {
        return 'iaun';
    }

}