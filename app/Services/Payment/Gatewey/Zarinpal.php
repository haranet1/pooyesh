<?php

namespace App\Services\Payment\Gatewey;

use App\Models\EventTicket;
use App\Models\Payment;
use App\Services\Payment\PaymentGatewayInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class Zarinpal implements PaymentGatewayInterface
{
    private $merchantID ;
    private $callBack ;
    private $token ;
    private $payment ;

    public function __construct()
    {
        $this->merchantID = '62697a96-bee0-4d87-85c0-f8db52150966';
    }

    public function pay(array $config, Payment $payment)
    {
        $this->payment = $payment;

        $this->callBack = route( $config['callback'] ,['gateway' => $this->getName(), 'paymentId' => $payment->id ]);

        $this->getToken(['amount' => $config['amount']]);

        return $this->token;
    }

    public function verify($request)
    {
        $this->getToken(['amount' => $request->price]);
        $response = Http::acceptJson()
            ->post('https://payment.zarinpal.com/pg/v4/payment/verify.json' , [
                'merchant_id' => $this->merchantID,
                'amount' => (int)$request->price * 10,
                'authority' => $this->token
            ]);

        $json = $response->json();
        dd($json);
    }

    public function getToken($amount)
    {
        $response = Http::acceptJson()
            ->post('https://payment.zarinpal.com/pg/v4/payment/request.json', [
                'merchant_id' => $this->merchantID,
                'amount' => (int)$amount['amount'],
                'currency' => 'IRT',
                'description' => 'jobist payment',
                'callback_url' => $this->callBack,
            ]);

        $json = $response->json();

        $this->token = $json['data']['authority'];
    }

    public function getName():string
    {
        return 'zarinpal';
    }
}
