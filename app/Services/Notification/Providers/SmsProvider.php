<?php


namespace App\Services\Notification\Providers;

use App\Models\User;
use App\Services\Notification\Providers\Contracts\Provider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Mpdf\Tag\U;

class SmsProvider implements Provider
{
    private User $user;
    private string $template;
    private array $tokens;

    /**
     * @param User $user
     * @param string $template
     * @param array $tokens
     */
    public function __construct(User $user, string $template, array $tokens)
    {
        $this->user = $user;
        $this->template = $template;
        $this->tokens = ['', '', ''];

        foreach ($tokens as $key => $value) {
            $this->tokens[$key] = $value;
        }
    }

    public function send()
    {
       Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
           'receptor' => $this->user->mobile,
           'token' => str_replace(' ','-',trim($this->tokens[0])),
           'token2' => str_replace(' ','-',trim($this->tokens[1])),
           'token3' => str_replace(' ','-',trim($this->tokens[2])),
           'template' => $this->template,
       ]);
    }
}
