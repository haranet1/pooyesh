<?php

namespace App\Services\Notification\Providers;

use App\Services\Notification\Providers\Contracts\Provider;
use Illuminate\Support\Facades\Http;

class RamakSmsProvider implements Provider
{
    private $mobile;
    private string $template;
    private array $tokens;

    /**
     * @param User $user
     * @param string $template
     * @param array $tokens
     */
    public function __construct($mobile, string $template, array $tokens)
    {
        $this->mobile = $mobile;
        $this->template = $template;
        $this->tokens = ['', ''];

        foreach ($tokens as $key => $value) {
            $this->tokens[$key] = $value;
        }
    }

    public function send()
    {
       Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
           'receptor' => $this->mobile,
           'token' => str_replace(' ','-',trim($this->tokens[0])),
           'token2' => str_replace(' ','-',trim($this->tokens[1])),
           'template' => $this->template,
       ]);
    }
}