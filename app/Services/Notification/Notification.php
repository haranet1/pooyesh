<?php

namespace App\Services\Notification;
use App\Services\Notification\Providers\Contracts\Provider;
class Notification
{
    public function __call($name, $arguments)
    {
        $providerPath = __NAMESPACE__.'\Providers'.'\\'.substr($name,4).'Provider';

        if (!class_exists($providerPath)){
            throw new \Exception('Class Doesnt Exist !!');
        }

        $providerInstance = new $providerPath(...$arguments);

        if (!is_subclass_of($providerInstance,Provider::class)){
            throw new \Exception('Object: '.$providerInstance. 'Must Be an Instance of: App\Services\Notification\Providers\Contracts\Provider');
        }

        return $providerInstance->send();

    }
}
