<?php

namespace App\Filament\Resources;

use App\Filament\Resources\OtechnologySkillExampleResource\Pages;
use App\Filament\Resources\OtechnologySkillExampleResource\RelationManagers;
use App\Models\OtechnologySkillExample;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\OtechnologySkillExampleResource\RelationManagers\JobTechnologySkillsRelationManager;

class OtechnologySkillExampleResource extends Resource
{
    protected static ?string $model = OtechnologySkillExample::class;

    protected static ?string $navigationIcon = 'heroicon-o-sparkles';

    protected static ?string $modelLabel = 'مثال مهارت فنی';

    protected static ?string $pluralModelLabel = 'مثال های مهارت فنی';
    
    protected static ?string $navigationLabel = 'مثال های مهارت فنی';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('english_example')
                    ->required()
                    ->label('عنوان انگلیسی')
                    ->disabled()
                    ->maxLength(191),
                
                Forms\Components\TextInput::make('example')
                    ->required()
                    ->label('عنوان')
                    ->maxLength(191),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('example')
                    ->label('عنوان')
                    ->searchable(),
                TextColumn::make('english_example')
                    ->label('عنوان انگلیسی')
                    ->searchable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageOtechnologySkillExamples::route('/'),
            'create' => Pages\CreateOtechnologySkillExample::route('/create'),
            'edit' => Pages\EditOtechnologySkillExample::route('/{record}/edit')
        ];
    }    

    public static function getRelations(): array
    {
        return [
            JobTechnologySkillsRelationManager::class,
        ];
    }
}
