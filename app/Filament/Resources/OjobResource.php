<?php

namespace App\Filament\Resources;

use App\Filament\Resources\OjobKnowledgeResource\RelationManagers\KnowledgesRelationManager;
use App\Filament\Resources\OjobResource\Pages;
use App\Filament\Resources\OjobResource\RelationManagers;
use App\Filament\Resources\OjobResource\RelationManagers\TasksRelationManager;
use App\Filament\Resources\OjobResource\RelationManagers\TechnologySkillsRelationManager;
use App\Filament\Resources\OjobSkillResource\RelationManagers\SkillsRelationManager;
use App\Models\Ojob;
use Closure;
use Filament\Forms;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ToggleColumn;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\SelectFilter;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class OjobResource extends Resource
{
    protected static ?string $model = Ojob::class;

    protected static ?string $modelLabel = 'شغل';

    protected static ?string $pluralModelLabel = 'شغل ها';

    protected static ?string $navigationLabel = 'شغل ها';

    protected static ?string $navigationIcon = 'heroicon-o-briefcase';


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()
                    ->schema([

                    TextInput::make('english_title')
                        ->label('عنوان انگلیسی')
                        ->disabled()
                        ->maxLength(255), 

                    TextInput::make('title')
                        ->label('عنوان')
                        ->required()
                        ->maxLength(255),

                    
                    Textarea::make('description')
                        ->label('توضیحات')
                        ->required()
                        ->maxLength(65535),

                    Toggle::make('disabled_at')
                        ->label(fn (Closure $get) => $get('disabled_at') ? 'غیر فعال' : 'فعال') // لیبل پویا بر اساس وضعیت
                        ->reactive() 
                        ->afterStateUpdated(function ($state, $record) {
                            if ($state) {
                                $record->disable();  
                            } else {
                                $record->enable();  
                            }
                        })
                        ->inline(false)

                    ])
                    ->columns(2),

                    

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('title')
                    ->label('عنوان')
                    ->color('primary')
                    ->searchable(),

                TextColumn::make('category.title')
                    ->label('دسته بندی')
                    ->searchable(),

                ToggleColumn::make('disabled_at')
                    ->label('وضیعت')
                    ->disabled(),

                TextColumn::make('updated_at')
                    ->label('تاریخ به‌روزرسانی')
                    ->sortable()
                    ->formatStateUsing(function ($state) {
                        return Verta::instance($state)->format('Y/m/d');
                    }),

            ])
            ->filters([
                SelectFilter::make('category_id')
                    ->label('دسته بندی')
                    ->relationship('category','title'),
                    
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            TechnologySkillsRelationManager::class,
            TasksRelationManager::class
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListOjobs::route('/'),
            // 'create' => Pages\CreateOjob::route('/create'),
            'edit' => Pages\EditOjob::route('/{record}/edit'),
        ];
    }    

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->where('description' , '!=' , null)->withoutGlobalScope('withoutDisabled')->orderBy('updated_at');
    }

   
}
