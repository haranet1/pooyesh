<?php

namespace App\Filament\Resources;

use App\Filament\Resources\OjobResource\RelationManagers\TechnologySkillsRelationManager;
use App\Filament\Resources\OtechnologySkillResource\Pages;
use App\Filament\Resources\OtechnologySkillResource\RelationManagers;
use App\Models\OtechnologySkill;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class OtechnologySkillResource extends Resource
{
    protected static ?string $model = OtechnologySkill::class;
    
    protected static ?string $navigationIcon = 'heroicon-o-sparkles';

    protected static ?string $modelLabel = 'مهارت فنی';

    protected static ?string $pluralModelLabel = 'مهارت ها فنی';
    
    protected static ?string $navigationLabel = 'مهارت ها فنی';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('english_title')
                    ->required()
                    ->label('عنوان انگلیسی')
                    ->disabled()
                    ->maxLength(191),
                
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->label('عنوان')
                    ->maxLength(191),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('عنوان')
                    ->searchable(),
                Tables\Columns\TextColumn::make('english_title')
                    ->label('عنوان انگلیسی')
                    ->searchable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->label('تاریخ بروزرسانی')
                    ->dateTime()
                    ->sortable()
                    ->formatStateUsing(function ($state) {
                        return Verta::instance($state)->format('Y/m/d');
                    }),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            TechnologySkillsRelationManager::class
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListOtechnologySkills::route('/'),
            // 'create' => Pages\CreateOtechnologySkill::route('/create'),
            'edit' => Pages\EditOtechnologySkill::route('/{record}/edit'),
        ];
    }    

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->orderBy('updated_at');
    }
}
