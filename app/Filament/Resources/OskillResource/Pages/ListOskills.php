<?php

namespace App\Filament\Resources\OskillResource\Pages;

use App\Filament\Resources\OskillResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListOskills extends ListRecords
{
    protected static string $resource = OskillResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
