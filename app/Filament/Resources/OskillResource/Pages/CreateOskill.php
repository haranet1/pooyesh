<?php

namespace App\Filament\Resources\OskillResource\Pages;

use App\Filament\Resources\OskillResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateOskill extends CreateRecord
{
    protected static string $resource = OskillResource::class;
}
