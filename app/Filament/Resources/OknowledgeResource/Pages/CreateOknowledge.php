<?php

namespace App\Filament\Resources\OknowledgeResource\Pages;

use App\Filament\Resources\OknowledgeResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateOknowledge extends CreateRecord
{
    protected static string $resource = OknowledgeResource::class;
}
