<?php

namespace App\Filament\Resources\OknowledgeResource\Pages;

use App\Filament\Resources\OknowledgeResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Filament\Pages\Actions\Action;

class EditOknowledge extends EditRecord
{
    protected static string $resource = OknowledgeResource::class;

    protected function getActions(): array
    {
        return [
            Action::make('previous')
                ->label('ویرایش دانش قبلی')
                ->icon('heroicon-o-arrow-right')
                ->color('primary')
                ->action(function () {
                    $currentKnowledge = $this->record;
                    if (!$currentKnowledge) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $previousKnowledge = $this->getModel()::where('id', $currentKnowledge->id - 1)->orderBy('id')->first();

                    if ($previousKnowledge) {
                        return redirect()->route('filament.resources.oknowledges.edit' , $previousKnowledge->id);
                    }

                    return redirect()->back()->with('error', 'شغل قبلی پیدا نشد.');
                }),

            // Actions\DeleteAction::make(),

            Action::make('next')
                ->label('ویرایش دانش بعدی')
                ->icon('heroicon-o-arrow-left')
                ->color('primary')
                ->action(function () {
                    $currentKnowledge = $this->record;
                    if (!$currentKnowledge) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $nextKnowledge = $this->getModel()::where('id', $currentKnowledge->id + 1)->orderBy('id')->first();

                    if ($nextKnowledge) {
                        return redirect()->route('filament.resources.oknowledges.edit' , $nextKnowledge->id);
                    }

                    return redirect()->back()->with('error', 'شغل بعدی پیدا نشد.');
                }),
        ];
    }
}
