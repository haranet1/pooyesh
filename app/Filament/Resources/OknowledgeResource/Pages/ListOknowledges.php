<?php

namespace App\Filament\Resources\OknowledgeResource\Pages;

use App\Filament\Resources\OknowledgeResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListOknowledges extends ListRecords
{
    protected static string $resource = OknowledgeResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
