<?php

namespace App\Filament\Resources\OtechnologySkillResource\Pages;

use App\Filament\Resources\OtechnologySkillResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Filament\Pages\Actions\Action;

class EditOtechnologySkill extends EditRecord
{
    protected static string $resource = OtechnologySkillResource::class;

    protected function getActions(): array
    {
        return [
            Action::make('previous')
                ->label('ویرایش مهارت فنی قبلی')
                ->icon('heroicon-o-arrow-right')
                ->color('primary')
                ->action(function () {
                    $currentSkill = $this->record;
                    if (!$currentSkill) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $previousSkill = $this->getModel()::where('id', $currentSkill->id - 1)->orderBy('id')->first();

                    if ($previousSkill) {
                        return redirect()->route('filament.resources.otechnology-skills.edit' , $previousSkill->id);
                    }

                    return redirect()->back()->with('error', 'شغل قبلی پیدا نشد.');
                }),

            // Actions\DeleteAction::make(),

            Action::make('next')
                ->label('ویرایش مهارت فنی بعدی')
                ->icon('heroicon-o-arrow-left')
                ->color('primary')
                ->action(function () {
                    $currentSkill = $this->record;
                    if (!$currentSkill) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $nextSkill = $this->getModel()::where('id', $currentSkill->id + 1)->orderBy('id')->first();

                    if ($nextSkill) {
                        return redirect()->route('filament.resources.otechnology-skills.edit' , $nextSkill->id);
                    }

                    return redirect()->back()->with('error', 'شغل بعدی پیدا نشد.');
                }),
        ];
    }
}
