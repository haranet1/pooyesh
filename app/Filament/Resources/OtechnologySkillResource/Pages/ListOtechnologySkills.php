<?php

namespace App\Filament\Resources\OtechnologySkillResource\Pages;

use App\Filament\Resources\OtechnologySkillResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListOtechnologySkills extends ListRecords
{
    protected static string $resource = OtechnologySkillResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
