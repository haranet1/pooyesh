<?php

namespace App\Filament\Resources\OtechnologySkillResource\Pages;

use App\Filament\Resources\OtechnologySkillResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateOtechnologySkill extends CreateRecord
{
    protected static string $resource = OtechnologySkillResource::class;
}
