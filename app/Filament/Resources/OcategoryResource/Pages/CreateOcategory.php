<?php

namespace App\Filament\Resources\OcategoryResource\Pages;

use App\Filament\Resources\OcategoryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Database\Eloquent\Model;

class CreateOcategory extends CreateRecord
{
    protected static string $resource = OcategoryResource::class;

    protected function handleRecordCreation(array $data): Model
    {
        return OcategoryResource::create($data);
    }
}
