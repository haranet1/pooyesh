<?php

namespace App\Filament\Resources\OcategoryResource\Pages;

use App\Filament\Resources\OcategoryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListOcategories extends ListRecords
{
    protected static string $resource = OcategoryResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
