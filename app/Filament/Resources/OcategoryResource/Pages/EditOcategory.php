<?php

namespace App\Filament\Resources\OcategoryResource\Pages;

use App\Filament\Resources\OcategoryResource;
use Filament\Pages\Actions;
use Filament\Pages\Actions\Action;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Database\Eloquent\Model;

class EditOcategory extends EditRecord
{
    protected static string $resource = OcategoryResource::class;

    protected function handleRecordUpdate(Model $record, array $data): Model
    {
        return OcategoryResource::update($record , $data);
    }

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Action::make('toggleDisabled')
                ->color('secondary')
                ->label(fn () => $this->record->jobs2->every->isDisabled() ? 'فعال کردن' : 'غیر فعال کردن') // لیبل پویا
                ->action(function () {
                    $allDisabled = $this->record->jobs2->every->isDisabled(); // بررسی وضعیت همه شغل‌ها

                    foreach ($this->record->jobs2 as $job) {
                        $allDisabled ? $job->enable() : $job->disable();
                    }

                    $this->emit('refreshPage');

                    $this->notify('success', $allDisabled ? 'همه شغل‌ها فعال شدند' : 'همه شغل‌ها غیرفعال شدند');

                })
                ->requiresConfirmation() // نمایش دیالوگ تأیید
                ->icon(fn () => $this->record->jobs2->every->isDisabled() ? 'heroicon-o-check-circle' : 'heroicon-o-x-circle')

                
                
        ];
    }
}
