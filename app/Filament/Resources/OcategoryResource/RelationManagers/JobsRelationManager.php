<?php

namespace App\Filament\Resources\OcategoryResource\RelationManagers;

use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ToggleColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class JobsRelationManager extends RelationManager
{
    protected static string $relationship = 'jobs2';

    protected static ?string $modelLabel = 'شغل ها';
    protected static ?string $pluralModelLabel = 'شغل ها';

    protected static ?string $recordTitleAttribute = 'title';

    

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->maxLength(255),
            ]);
    }

    public static function table(Table $table): Table
    {
        
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('عنوان')
                    ->limit('40')
                    ->searchable(),

                TextColumn::make('english_title')
                    ->limit('30')
                    ->label('عنوان انگلیسی'),
                
                ToggleColumn::make('disabled_at')
                    ->label('وضیعت')
                    ->disabled()

            ])
            ->filters([
                //
            ])
            ->headerActions([
                // Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                // Tables\Actions\EditAction::make(),
                // Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }    

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->where('description' , '!=' , null)->withoutGlobalScope('withoutDisabled')->orderBy('updated_at');
    }

}
