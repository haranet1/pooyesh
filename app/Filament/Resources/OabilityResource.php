<?php

namespace App\Filament\Resources;

use App\Filament\Resources\OabilityResource\Pages;
use App\Filament\Resources\OabilityResource\RelationManagers;
use App\Models\Oability;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class OabilityResource extends Resource
{
    protected static ?string $model = Oability::class;

    protected static ?string $navigationIcon = 'heroicon-o-sparkles';

    protected static ?string $modelLabel = 'توانایی';

    protected static ?string $pluralModelLabel = 'توانایی ها';
    
    protected static ?string $navigationLabel = 'توانایی ها';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('english_title')
                    ->required()
                    ->label('عنوان انگلیسی')
                    ->disabled()
                    ->maxLength(191),
                
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->label('عنوان')
                    ->maxLength(191),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('عنوان')
                    ->searchable(),
                Tables\Columns\TextColumn::make('english_title')
                    ->label('عنوان انگلیسی')
                    ->searchable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->label('تاریخ بروزرسانی')
                    ->dateTime()
                    ->sortable()
                    ->formatStateUsing(function ($state) {
                        return Verta::instance($state)->format('Y/m/d');
                    }),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListOabilities::route('/'),
            // 'create' => Pages\CreateOability::route('/create'),
            'edit' => Pages\EditOability::route('/{record}/edit'),
        ];
    }    

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->orderBy('updated_at');
    }
}
