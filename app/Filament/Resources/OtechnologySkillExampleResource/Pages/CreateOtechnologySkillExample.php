<?php

namespace App\Filament\Resources\OtechnologySkillExampleResource\Pages;

use App\Filament\Resources\OtechnologySkillExampleResource;
use Filament\Forms\Components\Card;
use Filament\Resources\Pages\CreateRecord;
use Filament\Forms\Components\TextInput;

class CreateOtechnologySkillExample extends CreateRecord
{

    protected static string $resource = OtechnologySkillExampleResource::class;

    protected function getFormSchema(): array
    {
        return [
            Card::make()
                ->columns(2)
                ->schema([
                    TextInput::make('english_example')
                        ->required()
                        ->label('عنوان انگلیسی')
                        ->maxLength(191),
                
                    TextInput::make('example')
                        ->required()
                        ->label('عنوان')
                        ->maxLength(191),
                ])
        ];
    }
}
