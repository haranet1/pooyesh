<?php

namespace App\Filament\Resources\OtechnologySkillExampleResource\Pages;

use App\Filament\Resources\OtechnologySkillExampleResource;
use Filament\Resources\Pages\EditRecord;
use Filament\Pages\Actions\Action;


class EditOtechnologySkillExample extends EditRecord
{
    protected static string $resource = OtechnologySkillExampleResource::class;

    protected function getActions(): array
    {
        return [

            Action::make('previous')
                ->label('ویرایش مثال قبلی')
                ->icon('heroicon-o-arrow-right')
                ->color('primary')
                ->action(function () {
                    $currentKnowledge = $this->record;
                    if (!$currentKnowledge) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $previousKnowledge = $this->getModel()::where('id', $currentKnowledge->id - 1)->orderBy('id')->first();

                    if ($previousKnowledge) {
                        return redirect()->route('filament.resources.otechnology-skill-examples.edit' , $previousKnowledge->id);
                    }

                    return redirect()->back()->with('error', 'شغل قبلی پیدا نشد.');
                }),

            // Actions\DeleteAction::make(),

            Action::make('next')
                ->label('ویرایش مثال بعدی')
                ->icon('heroicon-o-arrow-left')
                ->color('primary')
                ->action(function () {
                    $currentKnowledge = $this->record;
                    if (!$currentKnowledge) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $nextKnowledge = $this->getModel()::where('id', $currentKnowledge->id + 1)->orderBy('id')->first();

                    if ($nextKnowledge) {
                        return redirect()->route('filament.resources.otechnology-skill-examples.edit' , $nextKnowledge->id);
                    }

                    return redirect()->back()->with('error', 'شغل بعدی پیدا نشد.');
                }),
            
        ];
    }
}