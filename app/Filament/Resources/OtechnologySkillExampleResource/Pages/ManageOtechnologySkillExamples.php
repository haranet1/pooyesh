<?php

namespace App\Filament\Resources\OtechnologySkillExampleResource\Pages;

use App\Filament\Resources\OtechnologySkillExampleResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageOtechnologySkillExamples extends ManageRecords
{
    protected static string $resource = OtechnologySkillExampleResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
