<?php

namespace App\Filament\Resources\OtechnologySkillExampleResource\RelationManagers;

use App\Http\Livewire\CreateOjobTechnologySkill;
use App\Models\OjobTechnologySkill;
use App\Models\User;
use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Hidden;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Notifications\Notification;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables\Actions\Action;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Resources\Table;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Livewire;

use Filament\Tables\Actions\BulkAction;
use Illuminate\Database\Eloquent\Collection;
use Yepsua\Filament\Forms\Components\RangeSlider;

class JobTechnologySkillsRelationManager extends RelationManager
{
    protected static string $relationship = 'jobTechnologhSkills';

    protected static ?string $modelLabel = 'رابطه بین شغل و مهارت فنی';
    protected static ?string $pluralModelLabel = 'رابطه بین شغل و مهارت فنی';

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('jobName.title')
                    ->searchable()
                    ->label('شغل'),
                TextColumn::make('technologySkillName.title')
                    ->searchable()
                    ->label('مهارت فنی'),
                TextColumn::make('example.example')
                    ->label('مثال'),
            ])
            ->filters([

            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),

                Action::make('addExample')
                    ->label('افزودن مثال')
                    ->color('primary')
                    ->icon('heroicon-s-plus')
                    ->modalHeading('افزودن مثال جدید به مهارت فنی و شغل')
                    ->modalButton('ذخیره')
                    ->form( fn ($record) => [
                        // فیلدهای فرم داخل مدال

                        Grid::make(4)
                            ->schema([

                                Hidden::make('job_id')
                                    ->default($record->jobName->id),
                        
                                Hidden::make('technology_skill_id')
                                    ->default($record->technologySkillName->id),

                                TextInput::make('job')
                                    ->label('شغل')
                                    ->default($record->jobName->title)
                                    ->columnSpan(2)
                                    ->disabled(),
                                TextInput::make('technologySkill')
                                    ->label('مهارت فنی')
                                    ->default($record->technologySkillName->title)
                                    ->columnSpan(2)
                                    ->disabled(),

                            ]),
                        
                        

                        Select::make('example')
                            ->label('انتخاب مثال')
                            ->searchable()
                            ->options(
                                \App\Models\OtechnologySkillExample::pluck('example', 'id')
                            )
                            ->required(),

                        Grid::make(4)
                                ->schema([
                                    Checkbox::make('hot_technology')
                                        ->default(true)
                                        ->columnSpan(2),
                                    Checkbox::make('in_demand')
                                        ->default(true)
                                        ->columnSpan(2),

                                    // RangeSlider::make('importance')
                                    //     ->label('اهمیت')
                                    //     ->steps([
                                    //         1 => '1',
                                    //         2 => '2',
                                    //         3 => '3',
                                    //         4 => '4',
                                    //         5 => '5',
                                    //     ])
                                    //     ->columnSpan(2)
                                    //     ->step(1)
                                    //     ->default(5),

                                    // RangeSlider::make('level')
                                    //     ->label('سطح')
                                    //     ->steps([
                                    //         1 => '1',
                                    //         2 => '2',
                                    //         3 => '3',
                                    //         4 => '4',
                                    //         5 => '5',
                                    //     ])->step(1)
                                    //     ->columnSpan(2)
                                    //     ->default(5),
                                ]),

                        
                    ])
                    ->action(function ($data) {

                        OjobTechnologySkill::create([
                            'job_id' => $data['job_id'],
                            'technology_skill_id' => $data['technology_skill_id'],
                            'hot_technology' => $data['hot_technology'] == true ? 'Y' : 'N',
                            'in_demand' => $data['in_demand'] == true ? 'Y' : 'N',
                            'example_id' => $data['example'],
                        ]);

                        Notification::make()
                            ->title('مثال با موفقیت اضافه شد')
                            ->icon('heroicon-o-document-text')
                            ->iconColor('success')
                            ->success()
                            ->send(); 
                    })
                    
            ]);
    }

    
}
