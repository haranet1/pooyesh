<?php

namespace App\Filament\Resources\OjobResource\RelationManagers;

use App\Models\OtechnologySkill;
use Filament\Forms;
use Filament\Forms\Components\BelongsToSelect;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Stmt\Static_;

class TechnologySkillsRelationManager extends RelationManager
{
    protected static string $relationship = 'technologySkills';

    protected static ?string $recordTitleAttribute = 'example';

    protected static ?string $modelLabel = 'مهارت های فنی';
    protected static ?string $pluralModelLabel = 'مهارت های فنی';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([

                TextInput::make('english_example')
                    ->label('مثال انگلیسی تکنولوژی')
                    ->maxLength(255)
                    ->required(),

                TextInput::make('example')
                    ->label('مثال تکنولوژی')
                    ->maxLength(255)
                    ->required()
            ]);
    }
    

    public static function table(Table $table): Table
    {
        $request = request();

        // dd($request->route()->getName());    

        // شناسایی منبع از URL یا درخواست
        $columns = [];
        if ($request->route()->getName() === 'filament.resources.ojobs.edit') {
            // dd('true');
            $columns[] = Tables\Columns\TextColumn::make('technologySkillName.title')
                ->label('عنوان تکنولوژی');
        } elseif ($request->route()->getName() === 'filament.resources.otechnology-skills.edit') {
            // dd('true');
            $columns[] = Tables\Columns\TextColumn::make('jobName.title')
                ->label('عنوان شغل');
        }

        $columns[] = Tables\Columns\TextColumn::make('example.example')
            ->label('مثال')
            ->limit(25);

        $columns[] = Tables\Columns\TextColumn::make('example.english_example')
            ->label('مثال انگلیسی')
            ->limit(20);

        $columns[] = Tables\Columns\TextColumn::make('updated_at')
            ->label('تاریخ بروزرسانی')
            ->dateTime()
            ->sortable()
            ->formatStateUsing(function ($state) {
                return Verta::instance($state)->format('Y/m/d');
            });
            

        return $table
            ->columns($columns)
            ->filters([
                //
            ])
            ->headerActions([
                // Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                // Tables\Actions\DeleteAction::make(),
                
                
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }    
}
