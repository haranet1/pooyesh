<?php

namespace App\Filament\Resources\OjobResource\Pages;

use App\Filament\Resources\OjobResource;
use App\Filament\Resources\OjobResource\Actions\NextJobEditAction;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Filament\Pages\Actions\Action;

class EditOjob extends EditRecord
{
    protected static string $resource = OjobResource::class;

    protected function getActions(): array
    {
        return [
            Action::make('previous')
                ->label('ویرایش شغل قبلی')
                ->icon('heroicon-o-arrow-right')
                ->color('primary')
                ->action(function () {
                    $currentJob = $this->record; // استفاده از متد record() برای دسترسی به رکورد فعلی
                    if (!$currentJob) {
                        return redirect()->back()->with('error', 'رکورد فعلی پیدا نشد.');
                    }

                    $nextJob = $this->getModel()::where('id', $currentJob->id - 1)->orderBy('id')->first();

                    if ($nextJob) {
                        return redirect()->route('filament.resources.ojobs.edit', $nextJob->id);
                    }

                    return redirect()->back()->with('error', 'شغل قبلی پیدا نشد.');
                }),
            
            
            // Actions\DeleteAction::make(),

            Action::make('next')
                ->label('ویرایش شغل بعدی')
                ->icon('heroicon-o-arrow-left')
                ->color('primary')
                ->action(function () {
                    $currentJob = $this->record; // استفاده از متد record() برای دسترسی به رکورد فعلی
                    if (!$currentJob) {
                        return redirect()->back()->with('error', 'رکورد فعلی پیدا نشد.');
                    }

                    $nextJob = $this->getModel()::where('id', '>', $currentJob->id)->orderBy('id')->first();

                    if ($nextJob) {
                        return redirect()->route('filament.resources.ojobs.edit', $nextJob->id);
                    }

                    return redirect()->back()->with('error', 'شغل بعدی پیدا نشد.');
                }),

            
        ];
    }

    // protected function getRedirectUrl(): string
    // {
    //     return $this->getResource()::getUrl('index');
    // }
}
