<?php

namespace App\Filament\Resources\OjobResource\Pages;

use App\Filament\Resources\OjobResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListOjobs extends ListRecords
{
    protected static string $resource = OjobResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
    

}
