<?php

namespace App\Filament\Resources\OjobResource\Pages;

use App\Filament\Resources\OjobResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateOjob extends CreateRecord
{
    protected static string $resource = OjobResource::class;
}
