<?php

namespace App\Filament\Resources\OabilityResource\Pages;

use App\Filament\Resources\OabilityResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Filament\Pages\Actions\Action;

class EditOability extends EditRecord
{
    protected static string $resource = OabilityResource::class;

    protected function getActions(): array
    {
        return [

            Action::make('previous')
                ->label('ویرایش توانایی قبلی')
                ->icon('heroicon-o-arrow-right')
                ->color('primary')
                ->action(function () {
                    $currentAbility = $this->record;
                    if (!$currentAbility) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $previousAbility = $this->getModel()::where('id', $currentAbility->id - 1)->orderBy('id')->first();

                    if ($previousAbility) {
                        return redirect()->route('filament.resources.oabilities.edit' , $previousAbility->id);
                    }

                    return redirect()->back()->with('error', 'شغل قبلی پیدا نشد.');
                }),

            // Actions\DeleteAction::make(),

            Action::make('next')
                ->label('ویرایش توانایی بعدی')
                ->icon('heroicon-o-arrow-left')
                ->color('primary')
                ->action(function () {
                    $currentAbility = $this->record;
                    if (!$currentAbility) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $nextAbility = $this->getModel()::where('id', $currentAbility->id + 1)->orderBy('id')->first();

                    if ($nextAbility) {
                        return redirect()->route('filament.resources.oabilities.edit' , $nextAbility->id);
                    }

                    return redirect()->back()->with('error', 'شغل بعدی پیدا نشد.');
                }),
        ];
    }
}
