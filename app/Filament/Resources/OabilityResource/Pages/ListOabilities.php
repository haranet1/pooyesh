<?php

namespace App\Filament\Resources\OabilityResource\Pages;

use App\Filament\Resources\OabilityResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListOabilities extends ListRecords
{
    protected static string $resource = OabilityResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
