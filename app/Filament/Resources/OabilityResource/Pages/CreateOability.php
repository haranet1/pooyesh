<?php

namespace App\Filament\Resources\OabilityResource\Pages;

use App\Filament\Resources\OabilityResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateOability extends CreateRecord
{
    protected static string $resource = OabilityResource::class;
}
