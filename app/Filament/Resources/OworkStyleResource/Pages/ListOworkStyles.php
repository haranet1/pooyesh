<?php

namespace App\Filament\Resources\OworkStyleResource\Pages;

use App\Filament\Resources\OworkStyleResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListOworkStyles extends ListRecords
{
    protected static string $resource = OworkStyleResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
