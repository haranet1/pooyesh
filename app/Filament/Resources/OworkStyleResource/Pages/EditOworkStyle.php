<?php

namespace App\Filament\Resources\OworkStyleResource\Pages;

use App\Filament\Resources\OworkStyleResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Filament\Pages\Actions\Action;

class EditOworkStyle extends EditRecord
{
    protected static string $resource = OworkStyleResource::class;

    protected function getActions(): array
    {
        return [
            Action::make('previous')
                ->label('ویرایش ویژگی کاری قبلی')
                ->icon('heroicon-o-arrow-right')
                ->color('primary')
                ->action(function () {
                    $currentWorkStyle = $this->record;
                    if (!$currentWorkStyle) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $previousWorkStyle = $this->getModel()::where('id', $currentWorkStyle->id - 1)->orderBy('id')->first();

                    if ($previousWorkStyle) {
                        return redirect()->route('filament.resources.owork-styles.edit' , $previousWorkStyle->id);
                    }

                    return redirect()->back()->with('error', 'شغل قبلی پیدا نشد.');
                }),

            // Actions\DeleteAction::make(),

            Action::make('next')
                ->label('ویرایش ویژگی کاری بعدی')
                ->icon('heroicon-o-arrow-left')
                ->color('primary')
                ->action(function () {
                    $currentWorkStyle = $this->record;
                    if (!$currentWorkStyle) {
                        return redirect()->back()->with('error' , 'رکورد فعلی پیدا نشد.');
                    }

                    $nextWorkStyle = $this->getModel()::where('id', $currentWorkStyle->id + 1)->orderBy('id')->first();

                    if ($nextWorkStyle) {
                        return redirect()->route('filament.resources.owork-styles.edit' , $nextWorkStyle->id);
                    }

                    return redirect()->back()->with('error', 'شغل بعدی پیدا نشد.');
                }),
        ];
    }
}
