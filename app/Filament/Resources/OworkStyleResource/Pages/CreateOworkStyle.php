<?php

namespace App\Filament\Resources\OworkStyleResource\Pages;

use App\Filament\Resources\OworkStyleResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateOworkStyle extends CreateRecord
{
    protected static string $resource = OworkStyleResource::class;
}
