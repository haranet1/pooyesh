<?php

namespace App\Filament\Resources;

use App\Filament\Resources\OcategoryResource\Pages;
use App\Filament\Resources\OcategoryResource\RelationManagers\JobsRelationManager;
use App\Models\Ocategory;
use Closure;
use Filament\Forms;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Model;

class OcategoryResource extends Resource
{
    protected static ?string $model = Ocategory::class;

    protected static ?string $modelLabel = 'گروه شغلی';
    protected static ?string $pluralModelLabel = 'گروه‌های شغلی';
    protected static ?string $navigationIcon = 'heroicon-o-briefcase';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()
                    ->schema([
                        TextInput::make('english_title')
                            ->label('عنوان انگلیسی')
                            ->disabled()
                            ->maxLength(255), 

                        TextInput::make('title')
                            ->label('عنوان')
                            ->required()
                            ->maxLength(255),

                        Forms\Components\Placeholder::make('current_photo')
                            ->label('تصویر فعلی')
                            ->content(function ($record) {
                                if ($record && $record->photo) {
                                    
                                    echo '
                                        <img src="' . asset($record->photo->path) . '" alt="تصویر فعلی" style="max-width: 250px;">
                                    ';
                                    return;
                                }
                                echo '<p>تصویری وجود ندارد</p>';
                                return;
                            })
                            ->visible(fn ($livewire) => $livewire instanceof \Filament\Resources\Pages\EditRecord),

                        FileUpload::make('photo')
                            ->image()
                            ->directory('img/onet/category')
                            ->imagePreviewHeight('250')
                            ->label('تصویر')
                            ->helperText('حداکثر حجم: 2MB')
                            ->preserveFilenames(false),
                        
                        
                    ])->columns(2)
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('title')
                    ->searchable()
                    ->label('عنوان'),
                TextColumn::make('english_title')
                    ->searchable()
                    ->label('عنوان انگلیسی'),
                    
                ImageColumn::make('photo.path')
                    ->label('تصویر')
                    ->square()
                    ->defaultImageUrl(url(asset('assets/images/default-image.svg')))
                    ->url(function ($record) {
                        if($record->photo){
                            return asset($record->photo->path);
                        }
                    }),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function create(array $data)
    {
        $category = parent::create($data);

        if (isset($data['photo'])) {
            $uniqueName = time() . '.' . $data['photo']->getClientOriginalExtension();
            $data['photo']->move(asset('img/onet/category'), $uniqueName);

            $category->photo()->create([
                'path' => 'img/onet/category/' . $uniqueName,
                'name' => $uniqueName,
                'groupable_type' => Ocategory::class,
                'groupable_id' => $category->id,
            ]);
        }

        return $category;
    }

    public static function update(Model $record, array $data)
    {
        $record->update($data);

        if(isset($data['photo'])){

            $parts = explode('/', $data['photo']);
            $fileName = end($parts);

            if($record->photo && $record->photo->path != $data['photo']){
                $filePath = public_path($record->photo->path);
                if(file_exists($filePath)){
                    unlink($filePath);
                }

                $record->photo->delete();
            }

            $record->photo()->create([
                'name' => $fileName,
                'path' => $data['photo'],
                'groupable_id' => $record->id,
                'groupable_type' => Ocategory::class,
            ]);
        }

        return $record;
    }


    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListOcategories::route('/'),
            'create' => Pages\CreateOcategory::route('/create'),
            'edit' => Pages\EditOcategory::route('/{record}/edit'),
        ];
    }

    public static function getRelations(): array
    {
        return [
            JobsRelationManager::class
        ];
    }
}
