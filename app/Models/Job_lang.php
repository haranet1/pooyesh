<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job_lang extends Model
{
    use HasFactory;
    protected $fillable = ['lang_id' , 'job_position_id' , 'level'];
}
