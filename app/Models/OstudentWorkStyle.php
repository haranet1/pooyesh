<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OstudentWorkStyle extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'owork_style_id',
        'title',
        'level',
    ];

    public function workStyleName()
    {
        return $this->belongsTo(OworkStyle::class , 'owork_style_id');
    }
}
