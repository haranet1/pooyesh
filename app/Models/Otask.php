<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Otask extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_id',
        'task_code',
        'title',
        'type',
        'english_title',
    ];
}
