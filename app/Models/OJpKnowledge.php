<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OJpKnowledge extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_position_id',
        'knowledge_id',
        'title',
        'importance',
    ];

    public function knowledge()
    {
        return $this->belongsTo(Oknowledge::class);
    }
}
