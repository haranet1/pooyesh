<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExtraUser extends Model
{
    use HasFactory;

    protected $fillable = ['n_code' , 'grade' , 'major' , 'family' , 'name' , 'mobile' , 'sex' , 'email'];

    public function userHasNcode()
    {
        return $this->hasOne(User::class , 'n_code' , 'n_code');
    }

    public function userHasMobie()
    {
        return $this->hasOne(User::class , 'mobile' , 'mobile');
    }
}
