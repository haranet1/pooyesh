<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalityMajor extends Model
{
    use HasFactory;

    protected $fillable = [
        'major_id',
        'test_id',
        'test_type',
    ];
}
