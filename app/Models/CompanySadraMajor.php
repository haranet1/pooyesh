<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanySadraMajor extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_sadra_id',
        'college',
        'major',
        'number'
    ];
}
