<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OJpTechnologySkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_position_id',
        'technology_skill_id',
        'job_technology_skill_id',
        'title',
    ];

    public function technologySkill()
    {
        return $this->belongsTo(OtechnologySkill::class);
    }

    public function jobTechnologySkill()
    {
        return $this->belongsTo(OjobTechnologySkill::class , 'job_technology_skill_id');
    }
}
