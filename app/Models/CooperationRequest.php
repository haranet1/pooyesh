<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CooperationRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_position_id',
        'receiver_id',
        'sender_id',
        'type',
        'salary',
        'status',
        'certificate_id',
        'contract_id',
    ];

    /**
     * cooperation requesst status 
     * 
     *  null = nothing or not confirm
     *  0 = rejected
     *  1 = acepted
     *  2 = canceled
     *  3 = done
     * 
     */

    public function scopeHire($query)
    {
        return $query->where('type' , 'full-time')->orWhere('type' , 'part-time')->orWhere('type','remote');
    }

    public function scopeIntern($query)
    {
        return $query->where('type','intern');
    }

    public function scopeNotRejected($query)
    {
       return $query->where('status',1)->orwhere('status',null);
    }

    public function scopeNotAccepted($query)
    {
        return $query->where('status' , null);
    }

    public function scopeAccepted($query)
    {
        return $query->where('status' , 1)->orWhere('status' , 2)->orWhere('status' , 3);
    }

    public function scopeNotCencled($query)
    {
        return $query->where('status' , 2);
    }

    public function scopeMyRequests($query)
    {
        return $query->where('sender_id' , Auth::id());
    }

    public function scopeHasContract($query)
    {
        return $query->whereHas('contract');
    }

    public function scopeNotPrintedContract($query)
    {
        return $query->HasContract()
            ->whereRelation('process', 'accepted_by_uni_at', '!=' , null)
            ->whereRelation('process' , 'contract_printed_at' , null);
    }

    public function scopePrintedContract($query)
    {
        return $query->HasContract()
            ->whereRelation('process', 'accepted_by_uni_at', '!=' , null)
            ->whereRelation('process' , 'contract_printed_at' , '!=' , null);
    }

    public function scopeCourseUnitNotRegistration($query)
    {
        return $query->HasContract()
            ->PrintedContract()
            ->whereRelation('process' , 'course_unit_registration_at', null);
    }

    public function scopeCourseUnitRegistration($query)
    {
        return $query->HasContract()
            ->PrintedContract()
            ->whereRelation('process' , 'course_unit_registration_at', '!=', null);
    }

    // relations ->

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }

    public function process()
    {
        return $this->hasOne(PooyeshRequestProcess::class,'req_id','id');
    }

    public function certificate()
    {
        return $this->belongsTo(PooyeshCertificate::class,'certificate_id');
    }

    public function contract()
    {
        return $this->belongsTo(PooyeshContract::class,'contract_id');
    }

    public function job()
    {
        return $this->belongsTo(OjobPosition::class,'job_position_id');
    }
}
