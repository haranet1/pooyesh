<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OjobPosition extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'job_id',
        'title',
        'description',
        'type',
        'level',
        'experience',
        'working_hours',
        'salary',
        'age',
        'sex',
        'marige_type',
        'military_service_status',
        'grade',
        'major_id',
        'province_id',
        'city_id',
        'address',
        'about',
        'status',
        'active',
        'deleted',
    ];

    /**
     * scopes
     */
    public function scopeIntern($query)
    {
        return $query->where('type' , 'intern');
    }

    public function scopeHire($query)
    {
        return $query->where('type','!=', 'intern');
    }

    public function scopeConfirmed($query)
    {
        return $query->where('status', 1);
    }

    public function scopeUnConfirmed($query)
    {
        return $query->whereNull('status');
    }

    public function scopeRejected($query)
    {
        return $query->where('status' , 2);
    }

    public function scopeActive($query)
    {
        return $query->where('active' , true);
    }

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted' , false);
    }

    public function scopeDeleted($query)
    {
        return $query->where('deleted' , true);
    }

    public function scopeMyJobPositions($query)
    {
        return $query->where('company_id' , Auth::id());
    }
    
    public function scopeAcceptedOwner($query)
    {
        return $query->whereRelation('company' , 'status' , 1);
    }

    /**
     * relations
     */
    public function company()
    {
        return $this->belongsTo(User::class , 'company_id');
    }

    public function job()
    {
        return $this->belongsTo(Ojob::class , 'job_id' , 'id');
    }
    
    public function skills()
    {
        return $this->hasMany(OJpSkill::class , 'job_position_id' , 'id');
    }

    public function oldSkills()
    {
        return $this->BelongsToMany(Skill::class , 'job_skills' , 'job_position_id')->withPivot('skill_level');
    }

    public function abilities()
    {
        return $this->hasMany(OJpAbility::class , 'job_position_id' , 'id');
    }

    public function knowledges()
    {
        return $this->hasMany(OJpKnowledge::class , 'job_position_id' , 'id');
    }

    public function workStyles()
    {
        return $this->hasMany(OJpWorkStyle::class , 'job_position_id' , 'id');
    }

    public function technologySkills()
    {
        return $this->hasMany(OJpTechnologySkill::class , 'job_position_id' , 'id');
    }

    public function tasks()
    {
        return $this->hasMany(OJpTask::class, 'job_position_id' , 'id');
    }

    public function facilities()
    {
        return $this->hasMany(OJpFacilities::class , 'job_position_id' , 'id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function major()
    {
        return $this->belongsTo(Major::class);
    }

    public function requests()
    {
        return $this->hasMany(CooperationRequest::class , 'job_position_id');
    }

    public function requestProcess()
    {
        return $this->hasOneThrough(PooyeshRequestProcess::class, CooperationRequest::class, 'job_position_id', 'req_id','id','id');
    }
}
