<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class MbtiPersonality extends Model
{
    use HasFactory;

    public function jobs(): MorphToMany
    {
        return $this->morphToMany(Ojob::class, 'test', 'personality_jobs', 'test_id', 'ojob_id');
    }

    public function fields(): MorphToMany
    {
        return $this->morphToMany(Major::class, 'test', 'personality_majors', 'test_id', 'major_id');
    }
}
