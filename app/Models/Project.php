<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['owner_id','title','category_id','required_skills','budget','description'];

    public function owner()
    {
        return $this->belongsTo(User::class,'owner_id');
    }

    public function category()
    {
        return $this->belongsTo(ProjectCategory::class,'category_id');
    }
}
