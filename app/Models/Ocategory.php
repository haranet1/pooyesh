<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ocategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'english_title',
        'priority'
    ];

    public function jobs()
    {
        return $this->hasMany(Ojob::class , 'category_id');
    }

    public function jobs2()
    {
        return $this->hasMany(Ojob::class , 'category_id')->withoutGlobalScope('withoutDisabled');
    }

    public function photo()
    {
        return $this->morphOne(Photo::class , 'groupable');
    }
}
