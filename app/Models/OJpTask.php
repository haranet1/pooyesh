<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OJpTask extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_position_id',
        'task_id',
        'title',
    ];

    public function task()
    {
        return $this->belongsTo(Otask::class);
    }
}
