<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicantInfo extends Model
{
    use HasFactory;
    protected $fillable =['province_id','city_id','military_status','marital_status','requested_salary','birth','address','experience'];

    public function user()
    {
        return $this->morphOne(User::class,'groupable');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    
}
