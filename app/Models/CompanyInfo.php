<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
    use HasFactory;
    protected $fillable= [
        'name',
        'n_code',
        'registration_number',
        'activity_field',
        'ceo',
        'hr_manager',
        'size',
        'about',
        'year',
        'phone',
        'province_id',
        'city_id',
        'city',
        'address',
        'Plaque',
        'post_code',
        'email',
        'inviter',
        'header_id',
        'logo_id',
        'status',
        'newsletter_id',
        'create_internship_limit',
        'request_internship_limit',
        'confirm_internship_limit',
        'create_hire_limit',
        'request_hire_limit',
        'confirm_hire_limit',
    ];

    public function user()
    {
        return $this->morphOne(User::class,'groupable');
    }

    public function logo()
    {
        return $this->belongsTo(Photo::class,'logo_id');
    }

    public function header()
    {
        return $this->belongsTo(Photo::class,'header_id');
    }

    public function signature()
    {
        return $this->morphOne(Photo::class,'groupable');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class,'commentable')->whereNull('parent_id');
    }

    public function company_sadra()
    {
        return $this->hasOne(company_sadra::class , 'company_id');
    }

    public function companyPackage()
    {
        return $this->hasMany(CompanyPackage::class , 'company_id');
    }

    public function companyPaidPackages()
    {
        return $this->companyPackage()->where('status', 1)->where('receipt_id', '!=', null);
    }

    public function newsletter()
    {
        return $this->hasOne(Newsletter::class , 'id' , 'newsletter_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
