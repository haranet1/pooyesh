<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OstudentTechnologySkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'otechnology_skill_id',
        'example_id',
        'title',
        'level',
    ];

    public function technologySkillName()
    {
        return $this->belongsTo(OtechnologySkill::class , 'otechnology_skill_id');
    }

    public function exampleName()
    {
        return $this->belongsTo(OtechnologySkillExample::class , 'example_id');
    }
}
