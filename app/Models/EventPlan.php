<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventPlan extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_id',
        'title',
        'description',
        'price',
        'count',
        'photo_id',
        'deleted',
    ];

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted' , 0);
    }

    public function event()
    {
        return $this->belongsTo(Event::class , 'event_id');
    }

    public function photo()
    {
        return $this->hasOne(Photo::class , 'id' , 'photo_id');
    }

    public function info()
    {
        return $this->hasOne(EventInfo::class , 'plan_id');
    }
}
