<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyProduct extends Model
{
    use HasFactory;
    protected $fillable =['owner_id','photo_id','name','description','type'];

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }
}
