<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'order_id',
        'order_type',
        'price',
        'payment_key',
        'gateway',
        'status',
        'tracking_code',
        'receipt',
        'card_number',
        'payer_bank',
        'payment_date_time'
    ];

    public function orderType()
    {
        return $this->morphTo();
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function order()
    {
        return $this->morphTo();
    }
}

