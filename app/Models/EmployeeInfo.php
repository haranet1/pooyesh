<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        'province_id',
        'city_id',
        'university_id',
        'uni_post',
        'n_code',
        'p_code',
    ];

    public function user()
    {
        return $this->morphOne(User::class, 'groupable');
    }

    public function signature()
    {
        return $this->morphOne(Photo::class,'groupable');
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }


}
