<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    use HasFactory;
    protected $fillable=['owner_id','title','type','category','required_skills','budget','description','required_employees','invested_money'];


    public function owner()
    {
        return $this->belongsTo(User::class,'owner_id');
    }
}
