<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MbtiResult extends Model
{
    use HasFactory;


    public function suggestedJob(){
        return $this->morphMany(SuggestedTestJob::class, 'testable');
    }

}
