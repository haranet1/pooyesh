<?php

namespace App\Models;

use App\Traits\Disableable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ojob extends Model
{
    use HasFactory, Disableable;

    protected $fillable = [
        'category_id',
        'code',
        'title',
        'description',
        'english_title',
        'disabled_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('withoutDisabled', function ($query) {
            $query->withoutDisabled();
        });
    }

    public function category()
    {
        return $this->belongsTo(Ocategory::class);
    }

    public function positions()
    {
        return $this->hasMany(OjobPosition::class , 'job_id');
    }

    // public function skills()
    // {
    //     return $this->belongsToMany(Oskill::class , 'ojob_skills' , 'job_id' , 'skill_id');
    // }

    public function skills()
    {
        return $this->hasMany(OjobSkill::class , 'job_id');
    }

    public function knowledges()
    {
        return $this->hasMany(OjobKnowledge::class , 'job_id');
    }

    public function abilities()
    {
        return $this->hasMany(OabilityJob::class , 'job_id');
    }

    public function tasks()
    {
        return $this->hasMany(Otask::class , 'job_id');
    }

    public function technologySkills()
    {
        return $this->hasMany(OjobTechnologySkill::class , 'job_id');
    }

    public function workStyles()
    {
        return $this->hasMany(OjobWorkStyle::class , 'job_id');
    }

    public function mbti_personalities()
    {
        return $this->morphedByMany(MbtiPersonality::class, 'test', 'personality_jobs', 'ojob_id', 'test_id');
    }

}
