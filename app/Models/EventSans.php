<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventSans extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_id', 
        'title',
        'description',
        'start_register_at',
        'end_register_at',
        'date',
        'time',
        'deleted',
    ];

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted' , 0);
    }

    public function event()
    {
        return $this->belongsTo(Event::class , 'event_id');
    }
}
