<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GhqResult extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','physical','anxiety','social','depression','sum_of_all'];
}
