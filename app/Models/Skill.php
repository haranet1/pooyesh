<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    use HasFactory;
    protected $fillable = ['name','owner_id'];

    public function students()
    {
        return $this->belongsToMany(User::class, 'student_skills');
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class, 'job_skills');
    }

    public function scopeIsSkill($query)
    {
        return $query->whereNull('owner_id');
    }

    public function scopeIsExpertise($query)
    {
        return $query->whereNotNull('owner_id');
    }

    public function scopeIsConfirmed($query)
    {
        return $query->where('status', 1);
    }

}
