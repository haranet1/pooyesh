<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OstudentSkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'oskill_id',
        'title',
        'level',
    ];

    public function skillName()
    {
        return $this->belongsTo(Oskill::class , 'oskill_id');
    }
}
