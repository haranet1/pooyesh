<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_id',
        'plan_id',
        'sans_id',
        'plan_counter',
        'sold_out',
    ];

    public function scopeNotSoldOut($query)
    {
        return $query->where('sold_out' , 0);
    }

    public function scopeSoldOut($query)
    {
        return $query->where('sold_out' , 1);
    }

    public function scopeRegistrationOpen($query)
    {
        $now = Carbon::now();
        return $query->whereHas('sans', function($q) use ($now) {
            $q->where('start_register_at', '<=', $now)
              ->where('end_register_at', '>=', $now);
        });
    }

    public function event()
    {
        return $this->belongsTo(Event::class , 'event_id');
    }

    public function plan()
    {
        return $this->belongsTo(EventPlan::class , 'plan_id');
    }

    public function sans()
    {
        return $this->belongsTo(EventSans::class , 'sans_id');
    }

    public function tickets()
    {
        return $this->hasMany(EventTicket::class , 'event_info_id', 'id');
    }

    public function discounts()
    {
        return $this->morphToMany(Discount::class , 'discountables')->withPivot('counter');
    }
}
