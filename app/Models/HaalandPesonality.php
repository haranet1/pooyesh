<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaalandPesonality extends Model
{
    use HasFactory;

    public function jobs()
    {
        $this->belongsToMany(HaalandJob::class , 'haaland_job_pivots');
    }
    
    public function majors()
    {
        $this->belongsToMany(HaalandMajor::class , 'haaland_major_pivots');
    }

    public function features()
    {
        $this->hasMany(HaalandFeature::class);
    }
}
