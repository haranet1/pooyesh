<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OjobTechnologySkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_id',
        'technology_skill_id',
        'hot_technology',
        'in_demand',
        'example_id',
    ];

    public function jobName()
    {
        return $this->belongsTo(Ojob::class , 'job_id' , 'id');
    }

    public function technologySkillName()
    {
        return $this->belongsTo(OtechnologySkill::class , 'technology_skill_id' , 'id');
    }
    
    public function importance()
    {
        return $this->hasOne(Oimportance::class , 'id' , 'importance_id');
    }

    public function level()
    {
        return $this->hasOne(Olevel::class , 'id' , 'level_id');
    }

    public function example()
    {
        return $this->belongsTo(OtechnologySkillExample::class , 'example_id' , 'id');
    }
}
