<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'category_id', 'company_id'];

    public function category()
    {
        return $this->belongsTo(Job_Category::class, 'category_id');
    }

    public function skills()
    {
        return $this->belongsToMany(Skill::class, 'job_skills');
    }
}
