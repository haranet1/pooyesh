<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LangUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'lang_id',
        'level'
    ];
}
