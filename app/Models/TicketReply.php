<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketReply extends Model
{
    use HasFactory;

    protected $fillable = ['parent_id','sender_id','content','attachment_id'];


    public function parent()
    {
        return $this->belongsTo(Ticket::class , 'id');
    }

    public function attachment()
    {
        return $this->hasOne(Attachment::class , 'id' , 'attachment_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class , 'receiver_id');
    }
}
