<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NeoResult extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','N','E','O','A','C'];
}
