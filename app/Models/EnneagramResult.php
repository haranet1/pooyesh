<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EnneagramResult extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','A','B','C','main_character','main_character_score','second_character','second_character_score','weak_character','weak_character_score'];
}
