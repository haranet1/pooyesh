<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grader extends Model
{
    use HasFactory;
    protected $fillable = ['national_code','name','family','mobile','parent_mobile','grade','edu_branch','gender','school','suggestion'];
}
