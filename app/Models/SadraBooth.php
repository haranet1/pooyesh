<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SadraBooth extends Model
{
    use HasFactory;
    protected $fillable=['name','plan_id','reserved'];

    public function plan()
    {
        return $this->belongsTo(BuyPlan::class,'plan_id');
    }
}
