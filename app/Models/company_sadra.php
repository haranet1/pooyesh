<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class company_sadra extends Model
{
    use HasFactory;
    protected $fillable=['company_id',
        'sadra_id',
        'booth_id',
        'status',
        'receipt_id',
        'coordinator_name',
        'coordinator_mobile',
        'contract',
        'inviter',
        'dont_want_engineer',
        'electrical_engineer',
        'mechanical_engineer',
        'material_engineer',
        'industriall_engineer',
        'civil_engineer',
        'computer_engineer',
        'medical_engineer',
        'other_cases_engineer',
        'dont_want_humanities',
        'management',
        'rights',
        'accounting',
        'literature',
        'english',
        'history_and_geography',
        'other_cases_humanities',
        'dont_want_medical',
        'medical',
        'nursing',
        'midwifery',
        'surgeryRoom',
        'hygiene',
        'psychology',
        'other_cases_medical',
        'dont_want_science',
        'math',
        'physics',
        'chemistry',
        'biology',
        'other_cases_science',
        'dont_want_marketing',
        'general',
        'expertise',
        'student',
        'graduate',
        'advice_from_professors',
        'other_cases_cooperation',
    ];


    public function booth()
    {
        return $this->belongsTo(SadraBooth::class,'booth_id');
    }

    public function sadra()
    {
        return $this->belongsTo(Sadra::class,'sadra_id');
    }

    public function companyInfo()
    {
        return $this->belongsTo(CompanyInfo::class,'company_id');
    }

    public function receipt()
    {
        return $this->morphOne(Photo::class,'groupable');
    }

    public function scopeRegisterDone($query)
    {
        return $query->whereHas('receipt');
    }

    public function payments()
    {
        return $this->morphMany(Payment::class , 'orderType' , 'order_type' , 'order_id');
    }

    public function payedPayment()
    {
        return $this->hasOne(Payment::class , 'id' , 'receipt_id');
    }

    public function pay()
    {
        return $this->morphOne(Payment::class, 'order','order_type','order_id');
    }

}
