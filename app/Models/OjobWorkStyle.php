<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OjobWorkStyle extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'job_id',
        'work_style_id',
        'importance_id',
    ];

    public function workStyleName()
    {
        return $this->belongsTo(OworkStyle::class , 'work_style_id' , 'id');
    }

    public function importance()
    {
        return $this->hasOne(Oimportance::class , 'id' , 'importance_id');
    }

    public function level()
    {
        return $this->hasOne(Olevel::class , 'id' , 'level_id');
    }

}
