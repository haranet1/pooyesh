<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OjobSkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_id',
        'skill_id',
        'level_id',
        'importance_id',
    ];

    public function skillname()
    {
        return $this->belongsTo(Oskill::class , 'skill_id' , 'id');
    }

    public function level()
    {
        return $this->hasOne(Olevel::class , 'id' , 'level_id');
    }

    public function importance()
    {
        return $this->hasOne(Oimportance::class , 'id' , 'importance_id');
    }
}
