<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'is_percent',
        'value',
        'extra_time',
        'begin',
        'end',
        'max',
        'description',
        'company_acceptance',
        'student_acceptance',
        'applicant_acceptance',
        'deleted'
    ];

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted' , 0);
    }

    public function eventInfos()
    {
        return $this->morphedByMany(EventInfo::class , 'discountable')->withPivot('counter');
    }

    public function sadraPlans()
    {
        return $this->morphedByMany(BuyPlan::class , 'discountable')->withPivot('counter');
    }

}
