<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job_Category extends Model
{
    use HasFactory;

    protected $fillable=['name','company_id'];

    public function jobs()
    {
        return $this->hasMany(Job::class,'job_id');
    }

    public function skills()
    {
        return $this->hasMany(Skill::class);
    }

    public function job_positions()
    {
        return $this->HasManyThrough(Job_position::class , Job::class , 'category_id');
    }

}
