<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OabilityJob extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_id',
        'ability_id',
        'level_id',
        'importance_id',
    ];

    public function abilityName()
    {
        return $this->belongsTo(Oability::class , 'ability_id' , 'id');
    }

    public function importance()
    {
        return $this->hasOne(Oimportance::class , 'id' , 'importance_id');
    }

    public function level()
    {
        return $this->hasOne(Olevel::class , 'id' , 'level_id');
    }
}
