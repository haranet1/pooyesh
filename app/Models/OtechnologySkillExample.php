<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtechnologySkillExample extends Model
{
    use HasFactory;

    protected $fillable = [
        'example',
        'english_example',
    ];

    public function jobTechnologhSkills()
    {
        return $this->hasMany(OjobTechnologySkill::class ,'example_id', 'id');
    }
}
