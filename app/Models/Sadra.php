<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sadra extends Model
{
    use HasFactory;
    protected $fillable = ['title','photo_id','start_at','end_at','description'];

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function map()
    {
        return $this->belongsTo(Photo::class,'map_id');
    }

    public function companies()
    {
        return $this->belongsToMany(User::class,'company_sadras','sadra_id','company_id');
    }

    public function buy_plans()
    {
        return $this->hasMany(BuyPlan::class,'sadra_id','id');
    }

    public function banner()
    {
        return $this->belongsTo(Photo::class,'banner_id');
    }

    public function scopeNotExpired($query)
    {
        return $query->whereDate('end_register_at','>',now());
    }

}
