<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;

    protected $fillable = ['name','path','groupable_id','groupable_type'];

    public function groupable()
    {
        return $this->morphTo();
    }
}
