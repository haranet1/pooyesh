<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OJpAbility extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_position_id',
        'ability_id',
        'title',
        'importance',
    ];

    public function ability()
    {
        return $this->belongsTo(Oability::class);
    }
}
