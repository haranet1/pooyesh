<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PooyeshContract extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'path',
        'has_path'
    ];
}
