<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OstudentAbility extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'oability_id',
        'title',
        'level',
    ];

    public function abilityName()
    {
        return $this->belongsTo(Oability::class , 'oability_id');
    }
}
