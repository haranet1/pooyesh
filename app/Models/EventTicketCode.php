<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventTicketCode extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_ticket_id',
        'code',
        'status'
    ];

    public function ticket()
    {
        return $this->belongsTo(EventTicket::class , 'event_ticket_id');
    }
}
