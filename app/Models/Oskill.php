<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Oskill extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'title',
        'english_title',
    ];

    public function jobs()
    {
        return $this->belongsToMany(Ojob::class, 'ojob_skills', 'skill_id', 'job_id');
    }
}
