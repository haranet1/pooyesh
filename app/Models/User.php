<?php

namespace App\Models;

use App\Jobs\SendEmail;
use App\Mail\VerificationEmail;
use App\Mail\VerifyEmail;
use Filament\Models\Contracts\FilamentUser;
use Illuminate\Console\Application;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;

/**
 * 
 * @method sendEmailVerificationNotification()
 */
class User extends Authenticatable implements FilamentUser
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles , HasPermissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'family',
        'sex',
        'n_code',
        'mobile',
        'mobile_code',
        'mobile_code_created_at',
        'mobile_verified_at',
        'city_id',
        'province_id',
        'status',
        'groupable_id',
        'groupable_type'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // protected $attributes = [
    //     'family' => '-',
    //     'mobile' => '-',
    // ];

    public function canAccessFilament(): bool
    {
        return $this->can('manageOnet');
    }

    
    public function sendEmailVerificationNotification()
    {
        SendEmail::dispatch($this , new VerificationEmail($this));
    }

    public function IsCompany(): bool
    {
        return $this->groupable_type === CompanyInfo::class;
    }

    public function IsApplicant():bool
    {
        return $this->groupable_type === ApplicantInfo::class;
    }
    
    /**
     * Scopes =>
     */

    public function scopeApplicantOpenToWork($query)
    {
        return $query
            ->with('groupable')
            ->Applicant()
            ->where(function ($subquery) {
                $subquery
                    ->whereHasMorph('groupable', ApplicantInfo::class, function ($q) {
                        $q->where('open_to_work', 1);
                    });
            });
    }

    public function getUnreadTickets()
    {
        if($this->groupable_type == EmployeeInfo::class){
            $unreadReceivedTickets = Ticket::where('receiver_id' , null)
                ->where('status' , 'send')
                ->get();

        } else {
            $unreadReceivedTickets = $this->ticketReceiver()
                ->where('status', 'send')
                ->get();
        }
    
        return $unreadReceivedTickets->sortByDesc('created_at');
    }

    public function scopeConfirmed($query)
    {
        return $query->where('status', 1);
    }

    public function scopeUnConfirmed($query)
    {
        return $query->where('status', 0);
    }

    public function scopeRejected($query)
    {
        return $query->where('status' , 2);
    }

    public function scopeIsConfirmed()
    {
        if ($this->status == 1) 
        {
            return true;
        }
        return false;
    }

    public function scopeImported($query)
    {
        return $query->whereNull('mobile_code')->whereNull('mobile_code_created_at');
    }

    public function scopeStudentAndApplicant($query)
    {
        return $query->where('groupable_type', StudentInfo::class)
            ->orwhere('groupable_type', ApplicantInfo::class);  
    }

    public function scopeStudent($query)
    {
        return $query->where('groupable_type', StudentInfo::class);
    }

    public function scopeCompany($query)
    {
        return $query->where('groupable_type', CompanyInfo::class);
    }

    public function scopeCompanyRole($query)
    {
        return $query->hasRole('company');
    }

    public function scopeEmployee($query)
    {
        return $query->where('groupable_type', EmployeeInfo::class);
    }

    public function scopeApplicant($query)
    {
        return $query->where('groupable_type', ApplicantInfo::class);
    }

    public function scopeUnConfirmedCompany($query)
    {
        return $query->where('status' , 0)
            ->whereHas('groupable')
            ->where('groupable_type' , CompanyInfo::class)
            ->whereRelation('groupable' , 'status' , null);
    }

    public function scopeRejectCompanyInfo($query)
    {
        return $query->where('status' , 0)
            ->whereHas('groupable')
            ->where('groupable_type' , CompanyInfo::class)
            ->whereRelation('groupable' , 'status' , 0);
    }

    public function scopeConfirmedCompany($query)
    {
        return $query->where('status' , 0)
        ->whereHas('groupable')
        ->where('groupable_type' , CompanyInfo::class)
        ->whereRelation('groupable' , 'status' , 1);
    }

    public function scopeRejectedCompany($query)
    {
        return $query->where('status' , 2)
        ->whereHas('groupable')
        ->where('groupable_type' , CompanyInfo::class)
        ->whereRelation('groupable' , 'status' , 1);
    }

    /**
     * requests scops
     */
    public function scopeRequestSender($query)
    {
        return $this->whereHas('sendRequest');
    }

    public function scopeInternRequestSender($query)
    {
        return $this->RequestSender()->whereRelation('sendRequest' , 'type' , 'intern');
    }

    public function scopeInternRequestSender2($query)
    {
        return $this->whereHas('sendRequest')->whereRelation('sendRequest' , 'type' , 'intern');
    }

    public function scopeRequestReceiver($query)
    {
        return $this->whereHas('receivedRequest');
    }

    

    /**
     *  Relations => 
     */

    public function groupable()
    {
        return $this->morphTo();
    }
    
    // just return company information
    public function companyInfos()
    {
        return $this->belongsTo(CompanyInfo::class,'groupable_id');
    }

    // just return applicants information
    public function applicantInfos()
    {
        return $this->belongsTo(ApplicantInfo::class,'groupable_id');
    }
    
    // just return students information
    public function studentInfos()
    {
        return $this->belongsTo(StudentInfo::class,'groupable_id');
    }

    public function job_positions()
    {
        return $this->hasMany(Job_position::class, 'company_id');
    }

    public function ojobPositions()
    {
        return $this->hasMany(OjobPosition::class , 'company_id');
    }


    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function ideas()
    {
        return $this->hasMany(Idea::class, 'owner_id');
    }

    public function products()
    {
        return $this->hasMany(CompanyProduct::class, 'owner_id');
    }

    public function sadra_events()
    {
        return $this->belongsToMany(Sadra::class, 'company_sadras', 'company_id', 'sadra_id');
    }

    public function sadra_booth()
    {
        return $this->belongsToMany(SadraBooth::class, 'company_sadras', 'company_id', 'booth_id');
    }

    public function ticketSender()
    {
        return $this->hasMany(Ticket::class , 'sender_id');
    }

    public function ticketReceiver()
    {
        return $this->hasMany(Ticket::class , 'receiver_id');
    }

    

    /**
     *
     *for users which has Company Info relation
     */
    public function company_tickets()
    {
        return $this->hasMany(CompanyTicket::class, 'receiver_id', 'id');
    }

    public function receivedResumes()
    {
        return $this->hasMany(ResumeSent::class , 'receiver_id');
    }

    public function sentResumes()
    {
        return $this->hasMany(ResumeSent::class , 'sender_id');
    }

    // public function skills()
    // {
    //     return $this->belongsToMany(Skill::class, 'student_skills', 'student_id')
    //     ->whereNull('owner_id')->withPivot('skill_level');
    // }

    public function skills()
    {
        return $this->hasMany(OstudentSkill::class , 'student_id');
    }

    public function knowledges()
    {
        return $this->hasMany(OstudentKnowledge::class , 'student_id');
    }

    public function abilities()
    {
        return $this->hasMany(OstudentAbility::class , 'student_id');
    }

    public function technologySkills()
    {
        return $this->hasMany(OstudentTechnologySkill::class , 'student_id');
    }

    public function workStyles()
    {
        return $this->hasMany(OstudentWorkStyle::class , 'student_id');
    }

    public function languages()
    {
        return $this->belongsToMany(Lang::class , 'lang_users')->withPivot('level');
    }

    public function work_experiences()
    {
        return $this->hasMany(WorkExperience::class);
    }

    public function social_networks()
    {
        return $this->hasMany(SocialNetwork::class);
    }

    public function academicInfos()
    {
        return $this->hasMany(AcademicInfo::class);
    }

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }

    public function expertises()
    {
        return $this->belongsToMany(Skill::class, 'student_skills', 'student_id')
        ->whereNotNull('owner_id')->withPivot('skill_level');
    }

    public function mbti_test()
    {
        return $this->hasOne(MbtiResult::class);
    }

    public function enneagram_test()
    {
        return $this->hasOne(EnneagramResult::class);
    }

    public function neo_test()
    {
        return $this->hasOne(NeoResult::class);
    }

    public function ghq_test()
    {
        return $this->hasOne(GhqResult::class);
    }

    public function haaland_test()
    {
        return $this->hasOne(HaalandResult::class);
    }
    
    public function extraUserHasNcode()
    {
        return $this->belongsTo(ExtraUser::class , 'n_code' , 'n_code');
    }

    public function sendRequest()
    {
        return $this->hasMany(CooperationRequest::class , 'sender_id');
    }

    public function receivedRequest()
    {
        return $this->hasMany(CooperationRequest::class , 'receiver_id');
    }

    public function jobFavorites()
    {
        return $this->hasMany(OJpFavorite::class);
    }

    public function likes()
    {
        return $this->hasMany(ArticleLike::class);
    }

    public function suggestedTestJob()
    {
        return $this->hasMany(SuggestedTestJob::class , 'user_id');
    }
}
