<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EventTicket extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'event_id',
        'event_info_id',
        'discount_id',
        'discount_price',
        'payable_price',
        'count',
        'receipt',
        'status',
        'deleted',
    ];

    protected $attributes = [
        'status' => 0
    ];

    protected static function booted()
    {
        static::created(function ($eventTicket) {

            if($eventTicket->discount_id && ! is_null($eventTicket->receipt)) {
                
                DB::table('discountables')
                    ->where('discount_id' , $eventTicket->discount_id)
                    ->where('discountable_id', $eventTicket->event_info_id)
                    ->where('discountable_type', EventInfo::class)
                    ->increment('counter');
            }

        });
    }

    public function scopeMyTicket($query)
    {
        return $query->where('user_id' , Auth::id());
    }

    public function scopePayed($query)
    {
        return $query->whereNotNull('receipt')->where('status' , 1);
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function info()
    {
        return $this->belongsTo(EventInfo::class, 'event_info_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function payments()
    {
        return $this->morphMany(Payment::class , 'orderType' , 'order_type' , 'order_id');
    }

    public function codes()
    {
        return $this->hasMany(EventTicketCode::class , 'event_ticket_id' , 'id');
    }
}
