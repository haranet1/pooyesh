<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuyPlan extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'price', 'dimensions', 'tables_qty', 'chairs_qty', 'deleted', 'sadra_id'];

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted', 0);
    }

    public function sadra()
    {
        return $this->hasOne(Sadra::class, 'id', 'sadra_id');
    }

    public function map()
    {
        return $this->belongsTo(Photo::class, 'map_id');
    }

    public function booths()
    {
        return $this->hasMany(SadraBooth::class,'plan_id','id');
    }

    public function discounts()
    {
        return $this->morphToMany(Discount::class , 'discountables');
    }
}
