<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Job_position extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'title',
        'job_id',
        'required_skills',
        'location',
        'type',
        'description',
        'salary',
        'experience',
        'level',
        'status',
        'active',
        'province_id',
        'city_id',
        'age',
        'sex',
        'academic_level',
        'marige_type'
    ];

    public function scopeInternship($query)
    {
        return $query->where('type', 'intern');
    }

    public function scopeHire($query)
    {
        return $query->where('type','!=', 'intern');
    }

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted' , false);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('status',1);
    }

    public function scopeUnConfirmed($query)
    {
        return $query->where('status',0);
    }

    public function scopeIsConfirmed()
    {
        if ($this->status == 1) 
        {
            return true;
        }
        return false;
    }

    public function scopeActive($query)
    {
        return $query->where('active' , true);
    }
    
    public function scopeRejected($query)
    {
        return $query->where('status' , 2);
    }

    public function scopeMyJobPositions($query)
    {
        return $query->where('company_id' , Auth::id());
    }

    public function scopeAcceptedOwner($query)
    {
        return $query->whereRelation('company' , 'status' , 1);
    }

    public function category()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }

    public function company()
    {
        return $this->belongsTo(User::class, 'company_id');
    }

    public function skills(){
        return $this->BelongsToMany(Skill::class , 'job_skills')->withPivot('skill_level');;
    }

    public function getCategoryAttribute(){ // category relation
        return $this->job->category;
    }

    public function languages(){
        return $this->BelongsToMany(Lang::class , 'job_langs')->withPivot('level');
    }

    public function job()
    {
        return $this->belongsTo(Job::class,'job_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function requests()
    {
        return $this->hasMany(CooperationRequest::class);
    }

    public function request_process()
    {
        return $this->hasOneThrough(PooyeshRequestProcess::class, CooperationRequest::class, 'job_position_id', 'req_id','id','id');
    }

}
