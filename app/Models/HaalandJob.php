<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaalandJob extends Model
{
    use HasFactory;

    public function personalities()
    {
        $this->belongsToMany(HaalandPesonality::class , 'haaland_job_pivots');
    }
}

