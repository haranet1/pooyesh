<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    protected $fillable = ['name','path','type','std_can_dl','co_can_dl'];

    public function scopeForCompanies($query)
    {
        return $query->where('co_can_dl',1);
    }

    public function scopeForStudents($query)
    {
        return $query->where('std_can_dl',1);
    }

    public function groupable()
    {
        return $this->morphTo();
    }
}
