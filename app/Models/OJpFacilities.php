<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OJpFacilities extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_position_id',
        'title',
    ];
}
