<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResumeSent extends Model
{
    use HasFactory;

    protected $fillable = ['receiver_id' , 'sender_id' , 'status'];

    public function receiver()
    {
        return $this->belongsTo(User::class ,'receiver_id','id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class ,'sender_id' ,'id');
    }
}
