<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'title',
        'description',
    ];

    public function mbtiPersonalities()
    {
        return $this->morphedByMany(MbtiPersonality::class, 'test', 'personality_majors', 'major_id', 'test_id');
    }
}
