<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyTicket extends Model
{
    use HasFactory;

    protected $fillable=[
        'receiver_id',
        'sender_id',
        'sender_email',
        'subject',
        'content',
        'deleted',
        'read_at'
    ];

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted',0);
    }

    public function scopeUnread($query)
    {
        return $query->whereNull('read_at');
    }

    public function MarkAsRead()
    {
        $this->update(['read_at'=>now()]);

        return redirect()->back();
    }

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }

    public function replies()
    {
        return $this->hasMany(CompanyTicket::class,'parent_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }
}
