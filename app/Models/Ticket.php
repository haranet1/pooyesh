<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Ticket extends Model
{
    use HasFactory;
    protected $fillable =[
        'category',
        'sender_id',
        'receiver_id',
        'subject',
        'status',
        'groupable_id',
        'groupable_type',
        'updated_at'
    ];

    public function scopeSupportTickets($query)
    {
        return $query->whereNull('receiver_id');
    }

    public function scopeMyInbox($query)
    {
        return $query->where('receiver_id' , Auth::id());
    }

    public function scopeMyTickets($query)
    {
        return $query->where('sender_id' , Auth::id());
    }

    public function scopeUnSeen($query)
    {
        return $query->where('status' , 'send');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class , 'receiver_id');
    }

    public function groupable()
    {
        return $this->morphTo();
    }

    public function replies()
    {
        return $this->hasMany(TicketReply::class , 'parent_id');
    }

    // public function scopeNotDeleted($query)
    // {
    //     return $query->where('deleted',0);
    // }

    // public function scopeUnread($query)
    // {
    //     return $query->whereNull('read_at');
    // }

    // public function scopeOrginalMessage($query)
    // {
    //     return $query->whereNull('parent_id');
    // }

    // public function MarkAsRead()
    // {
    //     $this->update(['read_at'=>now()]);

    //     return redirect()->back();
    // }

    

    
}
