<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalityJob extends Model
{
    use HasFactory;

    protected $fillable = [
        'ojob_id',
        'test_id',
        'test_type',
    ];

    public function jobs()
    {
        return $this->belongsTo(Ojob::class , 'ojob_id' ,'id');
    }

}
