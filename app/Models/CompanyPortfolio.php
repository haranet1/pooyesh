<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyPortfolio extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'company_id', 'skills_used', 'description', 'photo_id'];

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }
}
