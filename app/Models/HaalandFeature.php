<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaalandFeature extends Model
{
    use HasFactory;

    public function personalites()
    {
        $this->belongsTo(HaalandPesonality::class);
    }
}   
