<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtechnologySkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'title',
        'english_title',
    ];

    public function technologySkills()
    {
        return $this->hasMany(OjobTechnologySkill::class , 'technology_skill_id');
    }
}
