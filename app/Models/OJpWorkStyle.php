<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OJpWorkStyle extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_position_id',
        'work_style_id',
        'title',
        'importance',
    ];

    public function workStyle()
    {
        return $this->belongsTo(OworkStyle::class);
    }
}
