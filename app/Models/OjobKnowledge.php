<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OjobKnowledge extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_id',
        'knowledge_id',
        'level_id',
        'importance_id',
    ];

    public function knowledgeName()
    {
        return $this->belongsTo(Oknowledge::class , 'knowledge_id' , 'id');
    }

    public function importance()
    {
        return $this->hasOne(Oimportance::class , 'id' , 'importance_id');
    }

    public function level()
    {
        return $this->hasOne(Olevel::class , 'id' , 'level_id');
    }
}
