<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaalandMajor extends Model
{
    use HasFactory;

    public function personalities()
    {
        $this->belongsToMany(HaalandPesonality::class, 'haaland_major_pivots');
    }
}
