<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OstudentKnowledge extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'oknowledge_id',
        'title',
        'level',
    ];

    public function knowledgeName()
    {
        return $this->belongsTo(Oknowledge::class , 'oknowledge_id');
    }
}
