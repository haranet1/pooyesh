<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PurchasePackage extends Model
{
    use HasFactory; 
    use SoftDeletes;

    protected $fillable = [
        'name', 
        'additional_internship_positions', 
        'additional_internship_requests',
        'additional_internship_confirms',
        'additional_hire_positions', 
        'additional_hire_requests',
        'additional_hire_confirms',
        'type',
        'price'
    ];

}
