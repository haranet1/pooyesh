<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OJpSkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_position_id',
        'skill_id',
        'title',
        'importance',
    ];

    public function skill()
    {
        return $this->belongsTo(Oskill::class , 'skill_id' , 'id');
    }
}
