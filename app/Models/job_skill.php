<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class job_skill extends Model
{
    use HasFactory;
    protected $fillable = ['job_position_id','skill_id','skill_level' ,'job_id'];
}
