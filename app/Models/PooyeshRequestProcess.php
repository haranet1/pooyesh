<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PooyeshRequestProcess extends Model
{
    use HasFactory;
    protected $fillable = [
        'req_id',
        'accepted_by_co_at',
        'accepted_by_std_at',
        'canceled_by_co_at',
        'canceled_by_std_at',
        'rejected_by_co_at',
        'rejected_by_std_at',
        'accepted_by_uni_at',
        'rejected_by_uni_at',
        'contract_printed_at',
        'course_unit_registration_at',
        'score',
        'done_at',
        'score_registration_at'
    ];


    public function getMostRecentlyUpdatedColumn(): ?string
    {
        $columns = [
            'accepted_by_co_at',
            'accepted_by_std_at',
            'canceled_by_co_at',
            'canceled_by_std_at',
            'rejected_by_co_at',
            'rejected_by_std_at',
            'accepted_by_uni_at',
            'rejected_by_uni_at',
            'contract_printed_at',
            'course_unit_registration_at',
            'done_at',
            'score_registration_at',
        ];

        $latestUpdatedColumn = null;
        $latestUpdatedTimestamp = null;

        foreach ($columns as $column) {
            $timestamp = $this->{$column};

            if (!is_null($timestamp)) {
                if (is_null($latestUpdatedTimestamp) || $timestamp > $latestUpdatedTimestamp) {
                    $latestUpdatedColumn = $column;
                    $latestUpdatedTimestamp = $timestamp;
                }
            }
        }

        return $latestUpdatedColumn;
    }


    public function request(){
        return $this->belongsTo(CooperationRequest::class,'req_id');
    }

}

