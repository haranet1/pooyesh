<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentInfo extends Model
{
    use HasFactory;
    protected $fillable = [
        'age',
        'province_id',
        'city_id',
        'university_id',
        'major','about','skills','email','grade','duration','n_code','std_number',
        'address','plaque','post_code'
    ];

    public function user()
    {
        return $this->morphOne(User::class,'groupable');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function signature()
    {
        return $this->morphOne(Photo::class,'groupable');
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }
}
