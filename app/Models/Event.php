<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'poster_id',
        'banner_id',
        'map_id',
        'status',
        'is_active',
        'is_free',
        'can_multy_buy',
        'company_acceptance',
        'student_acceptance',
        'applicant_acceptance',
        'deleted',
    ];

    public function scopeIsActive($query)
    {
        return $query->where('is_active' , 1);
    }

    public function scopeNotActive($query)
    {
        return $query->where('is_active' , 0);
    }

    public function scopeIsFree($query)
    {
        return $query->where('is_free' , 1);
    }

    public function scopeNotFree($query)
    {
        return $query->where('is_free' , 0);
    }

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted' , 0);
    }

    public function scopeDeleted($query)
    {
        return $query->where('deleted' , 1);
    }

    public function scopeHasInfo($query)
    {
        return $query->has('infos');
    }

    // relations

    public function poster()
    {
        return $this->hasOne(Photo::class , 'id' , 'poster_id');
    }

    public function banner()
    {
        return $this->hasOne(Photo::class , 'id' , 'banner_id');
    }

    public function map()
    {
        return $this->hasOne(Photo::class , 'id' , 'map_id');
    }

    public function sans()
    {
        return $this->hasMany(EventSans::class , 'event_id');
    }

    public function plans()
    {
        return $this->hasMany(EventPlan::class , 'event_id');
    }

    public function infos()
    {
        return $this->hasMany(EventInfo::class , 'event_id');
    }

    public function tickets()
    {
        return $this->hasMany(EventTicket::class , 'event_id');
    }

}
