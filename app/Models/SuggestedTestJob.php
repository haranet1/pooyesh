<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SuggestedTestJob extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'testable_id',
        'testable_type',
        'option',
        'status',
        'receipt_id',
        'discount_id',
        'discount_price',
        'updated_result',
        'payable_price'
    ];

    public function buyer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function testable(): MorphTo
    {
        return $this->morphTo(__FUNCTION__, 'testable_type', 'testable_id');
    }

    public function payments()
    {
        return $this->morphMany(Payment::class , 'order' , 'order_type' , 'order_id');
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount_id');
    }
}
