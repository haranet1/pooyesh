<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaalandResult extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'R', 'I', 'A', 'S', 'E' , 'C'];
}
