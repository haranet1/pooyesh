<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    protected $table ='news';

    protected $fillable = ['title','expired_at','content'];

    public function scopeActive($query)
    {
        return $query->where('expired_at','>',now());
    }
}
