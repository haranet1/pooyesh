<?php

namespace App\Models;

use App\Models\CompanyInfo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CompanyPackage extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'package_id',
        'status',
        'receipt_id'
    ];

    public function package()
    {
        return $this->belongsTo(PurchasePackage::class,'package_id');
    }

    public function companyInfo()
    {
        return $this->belongsTo(CompanyInfo::class, 'company_id');
    }

    public function receipt()
    {
        return $this->morphOne(Photo::class,'groupable');
    }

    public function pay()
    {
        return $this->morphOne(Payment::class, 'order','order_type','order_id');
    }

    public function payments()
    {
        return $this->morphMany(Payment::class, 'order', 'order_type', 'order_id');
    }
}
