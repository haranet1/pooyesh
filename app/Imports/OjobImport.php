<?php

namespace App\Imports;

use App\Models\Ojob;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OjobImport implements ToModel,WithHeadingRow
{
    public function model(array $row)
    {
        return new Ojob([
            'code' => $row['code'],
            'title' => $row['title'],
            'description' => $row['description'],
        ]); 
    }

    public function headingRow() : int
    {
        return 1;
    }
}
