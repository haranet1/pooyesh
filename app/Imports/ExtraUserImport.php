<?php

namespace App\Imports;

use App\Models\ExtraUser;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ExtraUserImport implements ToModel,WithHeadingRow    
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ExtraUser([
            'n_code' => $row['n_code'],
            'grade' => $row['grade'],
            'major' => $row['major'],
            'family' => $row['family'],
            'name' => $row['name'],
            'mobile'=>'0'.$row['mobile'],
            'sex' => $row['sex'],
            'email' => $row['email'],
        ]);
    }
}
