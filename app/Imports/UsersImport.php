<?php

namespace App\Imports;

use App\Models\User;
use App\Services\Notification\Notification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function  __construct($role , $sms)
    {
        $this->role = $role;
        $this->sms = $sms;
    }

    public function model(array $row)
    {

        Validator::make($row, [
            'name' => 'required',
            'family' => 'required',
            'password' => 'required',
            'mobile' => 'required|unique:users',
            'email' => 'required|unique:users|email'
        ])->validate();

        $user = new User([
            'name'=>$row['name'],
            'family'=>$row['family'],
            'mobile'=>'0'.$row['mobile'],
            'password'=>Hash::make('0'.$row['mobile']),
            'role'=>$this->role,
            'email' => $row['email'],
            'sex' => change_sex_fa_to_en($row['gender'])
        ]);
        $user->assignRole($this->role);
        $notif = new Notification();
        $name = $user->name ." ".$user->family;
        if($this->sms){
            $notif->sendSms($user,'fanyarImportedUser',[$name,$user->mobile]);
        }
        return $user;
    }
}
