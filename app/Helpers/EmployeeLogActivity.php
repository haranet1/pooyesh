<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Request;
use App\Models\EmployeeLogActivity as LogActivityModel;

class EmployeeLogActivity
{
    public static function addToLog($subject)
    {
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_id'] = auth()->check() ? auth()->user()->id : null;
        LogActivityModel::create($log);

    }


    public static function logActivityLists()
    {
        return LogActivityModel::latest()->get();
    }

    public static function paginate($limit)
    {
        return LogActivityModel::orderBydesc('id')->paginate($limit);
    }

}


