<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class JobPositionCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $job_title;
    public $company;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($job_title,$company)
    {
        $this->job_title = $job_title;
        $this->company = $company;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

}
