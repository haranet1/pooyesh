<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SadraEventCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $sadra;
    public $expire_date;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($sadra,$expire_date)
    {
       $this->sadra = $sadra;
       $this->expire_date = $expire_date;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

}
