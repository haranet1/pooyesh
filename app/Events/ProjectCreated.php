<?php

namespace App\Events;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ProjectCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $project_title;
    public $project_owner;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($project_title,$project_owner)
    {
        $this->project_title = $project_title;
        $this->project_owner = $project_owner;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

}
