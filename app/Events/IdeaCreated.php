<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class IdeaCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $idea_title;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($idea_title)
    {
        $this->idea_title = $idea_title;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

}
