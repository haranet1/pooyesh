<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UniAcceptedJobPosition extends Mailable
{
    use Queueable, SerializesModels;

    public $message;
    public $link;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message , $link)
    {
        $this->message = $message;
        $this->link = $link;
        $this->subject = 'تایید موقعیت شغلی';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user-registred')
            ->subject($this->subject)
            ->with([
                'messages' => $this->message,
                'link' => $this->link,
                'subject' => $this->subject,
            ]);
    }
}
