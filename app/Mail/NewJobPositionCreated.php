<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewJobPositionCreated extends Mailable
{
    use Queueable, SerializesModels;
    public $message;
    public $link;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message, $link , $isIntern)
    {
        $this->message = $message;
        $this->link = $link;
        if($isIntern){
            $this->subject = 'موقعیت کارآموزی جدید';
        }else{
            $this->subject = 'موقعیت استخدامی جدید';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user-registred')
            ->subject($this->subject)
            ->with([
                'messages' => $this->message,
                'link' => $this->link,
                'subject' => $this->subject,
            ]);
    }
}
