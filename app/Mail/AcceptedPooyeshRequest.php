<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AcceptedPooyeshRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $message;
    public $subject;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject , $message , $link)
    {
        $this->subject = $subject;
        $this->message = $message;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user-registred')
            ->subject($this->subject)
            ->with([
                'subject' => $this->subject,
                'messages' => $this->message,
                'link' => $this->link
            ]);
    }
}
