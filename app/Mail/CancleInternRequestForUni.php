<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CancleInternRequestForUni extends Mailable
{
    use Queueable, SerializesModels;

    public $message;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message , $link)
    {
        $this->message = $message;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user-registred')
            ->subject('لغو درخواست کارآموزی')
            ->with([
                'messages' => $this->message,
                'link' => $this->link,
                'subject' => 'لغو درخواست کارآموزی',
            ]);
    }
}
