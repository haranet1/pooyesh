<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserConfirmed extends Mailable
{
    use Queueable, SerializesModels;

    public $link;
    public $messages;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message , $link)
    {
        $this->link = $link;
        $this->messages = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user-registred')
            ->subject('تایید کاربر')
            ->with([
                'subject' => 'تایید کاربر',
                'link' => $this->link,
                'messages' => $this->messages
            ]);
    }
}
