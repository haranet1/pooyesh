<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewTicketSend extends Mailable
{
    use Queueable, SerializesModels;

    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user-registred')
            ->subject('تیکت جدید دریافت کرده اید')
            ->with([
                'subject' => 'تیکت جدید دریافت کرده اید',
                'messages' =>  'شما در سامانه جابیست پیامی به صورت تیکت دریافت کرده اید',
                'link' => $this->link,
            ]);
    }
}
