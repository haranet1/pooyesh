<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegistred extends Mailable
{
    use Queueable, SerializesModels;

    public $link;
    public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message , $link)
    {
        $this->link = $link;
        $this->message = $message;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user-registred')
            ->subject('کاربر جدید')
            ->with([
                'subject' => 'کاربر جدید',
                'link' => $this->link,
                'messages' => $this->message
            ]);
    }
}
