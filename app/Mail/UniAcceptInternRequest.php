<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UniAcceptInternRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $message;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message , $link)
    {
        $this->subject = 'تایید درخواست کارآموزی';
        $this->message = $message;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user-registred')
            ->subject($this->subject)
            ->with([
                'messages' => $this->message,
                'subject' => $this->subject,
                'link' => $this->link
            ]);
    }
}
