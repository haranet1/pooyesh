<?php

namespace App\Traits;

use Carbon\Carbon;

trait Disableable
{
    public function disable()
    {
        $this->disabled_at = Carbon::now();
        return $this->save();
    }

    public function enable()
    {
        $this->disabled_at = null;
        return $this->save();
    }

    public function isDisabled()
    {
        return !is_null($this->disabled_at);
    }

    public function scopeOnlyDisabled($query)
    {
        return $query->whereNotNull('disabled_at');
    }

    public function scopeWithoutDisabled($query)
    {
        return $query->whereNull('disabled_at');
    }
}
