<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class storeApplicantInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'province'=>'required|integer',
            'city'=>'required|integer',
            'marital_status'=>'required|string',
            'military_status'=>'required|string',
            'birth'=>['required' ,'regex:/^(1[2-9]\d{2}|20[0-9]{2})\/(0[1-9]|1[0-2])\/(0[1-9]|[12]\d|3[01])$/'],
            'experience' => 'required|numeric',
            'address'=>'required',
            'requested_salary' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'birth.required' => 'لطفا تاریخ تولد خود را وارد کنید',
            'birth.regex' => 'لطفا تاریخ تولد خود را به صورت صحیح وارد کنید',
            'address.required' => 'لطفا آدرس خود را وارد کنید',
            'province.integer' => 'لطفا استان محل سکونت خود را انتخاب کنید',
            'city.integer' => 'لطفا شهر محل سکونت خود را انتخاب کنید',
            'marital_status.required' => 'لطفا وضعیت تاهل خود را انتخاب کنید',
            'military_status.required' => 'لطفا وضعیت نظام وظیفه خود را انتخاب کنید',
            'requested_salary.required' => 'لطفا حقوق درخواستی خود را وارد کنید',
            'experience.required' => 'لطفا سابقه کاری خود را وارد کنید',
            'experience.numeric' => 'لطفا سابقه کاری را به صورت عدد وارد کنید',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'birth' => persian_to_eng_num($this->input('birth'))
        ]);
    }
}
