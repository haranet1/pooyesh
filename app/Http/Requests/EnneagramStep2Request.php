<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnneagramStep2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        for($col = 1; $col<= 48 ; $col++)
            {
                $name = $col;
                $rules[$name] = 'required';
            }

        return $rules;
    }

    public function messages()
    {
        $customMessage = [];
        for($col = 1; $col<= 48 ; $col++)
        {
            $name = $col;
            $customMessage[$name.'.required'] = 'لطفا به این سوال پاسخ دهید';
        }
        return $customMessage;
    }

    public function prepareForValidation()
    {
       
        foreach($this->all() as $key => $value){
            $newValue = preg_replace('/\d/','',$value);
            $this->merge([$key,$newValue]);
        }
        dd($this->all());
    }

}