<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyIdeaStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'category'=>'required',
            'type'=>'required',
            'budget'=>'required|int',
            'description'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' =>'عنوان الزامی است',
            'category.required' =>'دسته بندی الزامی است',
            'type.required' =>'نوع ایده الزامی است',
            'budget.required' =>'بودجه الزامی است',
            'description.required' =>'توضیحات الزامی است',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'budget' => persian_to_eng_num($this->input('budget'))
        ]);
    }
}
