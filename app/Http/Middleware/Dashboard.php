<?php

namespace App\Http\Middleware;

use App\Models\ApplicantInfo;
use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use Closure;
use Illuminate\Http\Request;

class Dashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(! $request->user()->hasRole(['student' , 'company' , 'applicant' , 'employee'])){
            return redirect()->route('select.role');
        }

        if($request->user()->groupable_type == StudentInfo::class || $request->user()->groupable_type == ApplicantInfo::class){
            return redirect()->route('std.panel');
        }elseif($request->user()->groupable_type == CompanyInfo::class){
            return redirect()->route('company.panel');
        }
        return $next($request);
    }
}
