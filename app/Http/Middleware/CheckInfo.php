<?php

namespace App\Http\Middleware;

use App\Models\CompanyInfo;
use App\Models\EmployeeInfo;
use App\Models\StudentInfo;
use App\Models\University;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckInfo
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::user()->groupable) {
            
            if (Auth::user()->hasRole('student')) return redirect(route('create.std.info'));

            if (Auth::user()->hasRole('company')) return redirect(route('create.co.info'));


            if (Auth::user()->hasRole('employee')) return redirect(route('set.emp.info'));

            if (Auth::user()->hasRole('applicant')) return redirect(route('set.applicant.info'));
        }
        
        return $next($request);
    }
}
