<?php

namespace App\Http\Middleware;

use App\Services\Notification\Notification;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

class VerifyMobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (Route::CurrentRouteName()=='verifyMobile' || Route::CurrentRouteName()=='change_mobile' || Route::CurrentRouteName()=='update_mobile'){
            return $next($request);
        }else{
            if (Auth::check() && Auth::user()->mobile_verified_at == null){
                if (Auth::user()->mobile_code_created_at < now()->subMinute(2) || Auth::user()->mobile_code_created_at == null){
                    if (has_internet()){
                        $rand = rand(10000,99999);
                        Auth::user()->mobile_code = $rand;
                        Auth::user()->mobile_code_created_at = now();
                        Auth::user()->save();
                        $notif = new Notification();
                        $notif->sendSms(Auth::user(),'fanyarcoMobileVerify',[Auth::user()->name,Auth::user()->family,$rand]);
                        return redirect(\route('user.showVerifyMobile'));
                    }else{
                        return redirect(\route('user.showVerifyMobile'))->with('error','اتصال اینترنت خود را بررسی کنید');
                    }

                }else{
                    return redirect(\route('user.showVerifyMobile'));
                }
            }else{
                return $next($request);
            }
        }
    }
}
