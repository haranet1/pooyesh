<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\CompanyInfo;
use Illuminate\Http\Request;
use App\Models\CooperationRequest;
use App\Models\OjobPosition;
use Illuminate\Support\Facades\Auth;

class CheckRequestInternshipLimit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $job = OjobPosition::find($request->job);
        $company = Auth::user();
        if($job->type == 'intern'){
            if(Auth::check() && Auth::user()->groupable_type == CompanyInfo::class){
                $sent_requests_intern = CooperationRequest::where('sender_id', Auth::id())->where('type' , 'intern')->count();
                if($sent_requests_intern >= $company->groupable->request_internship_limit){
                    return response()->json(['errorText' => 'کارفرمای گرامی، تعداد مجاز درخواست شما به کارآموز به پایان رسیده است.'], 400);
                }
            }
        } else {
            if(Auth::check() && Auth::user()->groupable_type == CompanyInfo::class){
                $sent_requests_hire = CooperationRequest::where('sender_id', Auth::id())->where('type', '!=', 'intern')->count();
                if($sent_requests_hire >= $company->groupable->request_hire_limit){
                    return response()->json(['errorText' => 'کارفرمای گرامی، تعداد مجاز درخواست شما به کارجو به پایان رسیده است.'], 400);
                }
            }
        }
        
        return $next($request);
    }
}
