<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AcceptedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::user()->status == 1){
            return $next($request);
        }

        if(Auth::user()->status == 2){
            $request->session()->put('error','حساب کاربری شما رد شده است. جهت اطلاعات بیشتر و رفع محدودیت ها به قسمت پشتیبانی مراجعه کنید.');
        }else{
            $request->session()->put('error','حساب کاربری شما تایید نشده است. منتظر بررسی حساب کاربریتان توسط کارشناس بمانید.');
        }

        return redirect()->route('dashboard');
    }
}
