<?php

namespace App\Http\Middleware;

use App\Models\EmployeeInfo;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class VerifyEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user() && !$request->user()->hasVerifiedEmail()){
            if(Route::CurrentRouteName() != 'company.panel' && Route::CurrentRouteName() != 'employee.panel' && Route::CurrentRouteName() != 'std.panel'){
                return redirect()->route('dashboard');
            }else{
                $time = $request->user()->updated_at->addMinutes(5);
                if($time > now()){
                    session()->flash('emailVerificationSended' , true);
                }else{
                    session()->flash('mustVerifyEmail' , true);
                }
            }
        }
        return $next($request);
    }
}
