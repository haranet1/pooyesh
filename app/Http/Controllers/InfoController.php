<?php

namespace App\Http\Controllers;

use App\Events\CompanyRegistered;
use App\Events\UserRegistered;
use App\Helpers\CompanyLogActivity;
use App\Helpers\StudentLogActivity;
use App\Http\Controllers\Controller;
use App\Http\Requests\storeApplicantInfoRequest;
use App\Models\ApplicantInfo;
use App\Models\CompanyInfo;
use App\Models\Photo;
use App\Models\Province;
use App\Models\Setting;
use App\Models\StudentInfo;
use App\Models\University;
use App\Models\User;
use App\Notifications\UserRegistred;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Http;

class InfoController extends Controller
{
    public function coCreate()
    {
        $provinces = Province::all();

        return view('information.company.create' , compact('provinces'));
    }

    public function coStore(Request $request)
    {
        $n_code = persian_to_eng_num($request->n_code);
        $year = persian_to_eng_num($request->year);
        $r_number = persian_to_eng_num($request->r_number);
        $phone = persian_to_eng_num($request->phone);

        $request->merge([
            'n_code'=>$n_code,
            'year' => $year,
            'r_number' =>$r_number,
            'phone' =>$phone
        ]);

        $request->validate([
            'name' => 'required',
            'n_code' => 'required|numeric|unique:company_infos,n_code',
            'year' => 'required|numeric',
            'r_number' => 'required|numeric',
            'phone' => 'required|numeric',
            'field' => 'required',
            'size' => 'required',
            'about' => 'required',
            'province' => 'required',
            'city' => 'required',
            'address' => 'required',
            'Plaque' => 'required',
            'post_code' => 'required',
            'header_img' => 'mimes:jpeg,jpg,png|max:2048',
            'logo' => 'required|mimes:jpeg,jpg,png|max:512',
            ], [
            'name.required' => 'نام شرکت را وارد کنید',
            'n_code.required' => 'کد ملی شرکت را وارد کنید',
            'n_code.unique' => 'کد ملی شرکت تکراری است',
            'year.required' => 'سال تاسیس شرکت را وارد کنید',
            'r_number.required' => 'شماره ثبت شرکت را وارد کنید',
            'r_number.numeric' => 'شماره ثبت شرکت باید به صورت عددی باشد',
            'phone.required' => 'تلفن شرکت را وارد کنید',
            'field.required' => 'حوزه فعالیت شرکت را وارد کنید',
            'size.required' => 'تعداد کارکنان شرکت را وارد کنید',
            'province.required' => 'استان محل شرکت خود را وارد کنید',
            'city.required' => 'شهر محل شرکت خود را وارد کنید',
            'address.required' => 'آدرس شرکت به صورت دقیق الزامی است',
            'Plaque.required' => 'پلاک شرکت الزامی است',
            'post_code.required' => 'کد پستی شرکت الزامی است',
            'about.required' => 'توضیحاتی در مورد شرکت ارائه دهید',

            'header_img.mimes' => 'فرمت تصویر انتخاب شده صحیح نمی باشد',
            'header_img.max' => 'حجم تصویر پس زمینه نباید بیشتر از 2 مگابایت باشد',
            'logo.mimes' => 'فرمت تصویر انتخاب شده صحیح نمی باشد',
            'logo.max' => 'حجم لوگو نباید بیشتر از 1 مگابایت باشد',
            'logo.required' => 'بارگذاری لوگو الزامی می باشد.',
        ]);

        $info = CompanyInfo::create([
            'name' => $request->input('name'),
            'year' => $request->input('year'),
            'phone' => $request->input('phone'),
            'n_code' => $request->input('n_code'),
            'registration_number' => $request->input('r_number'),
            'activity_field' => $request->input('field'),
            'ceo' => $request->input('ceo'),
            'hr_manager' => $request->input('hr'),
            'size' => $request->input('size'),
            'province_id' => $request->input('province'),
            'city_id' => $request->input('city'),
            'address' => $request->input('address'),
            'Plaque' => $request->input('Plaque'),
            'post_code' => $request->input('post_code'),
            'about' => $request->input('about'),
            'inviter' =>$request->input('inviter'),
            'create_internship_limit' => Setting::getValue('create_internship_limit'),
            'request_internship_limit' => Setting::getValue('request_internship_limit'),
            'confirm_internship_limit' => Setting::getValue('confirm_internship_limit'),
            'create_hire_limit' => Setting::getValue('create_hire_limit'),
            'request_hire_limit' => Setting::getValue('request_hire_limit'),
            'confirm_hire_limit' => Setting::getValue('confirm_hire_limit'),
        ]);

        if($request->has('header_img')){
            $name = time() . $request->header_img->getClientOriginalName();
            $path = $request->header_img->storeAs('img/company', $name, 'public');

            $header = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $info->id,
                'groupable_type' => CompanyInfo::class,
            ]);

            $info->header_id = $header->id;
            $info->save();
        }

        if($request->has('logo')){
            $name = time() . $request->logo->getClientOriginalName();
            $path = $request->logo->storeAs('img/company', $name, 'public');

            $logo = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $info->id,
                'groupable_type' => CompanyInfo::class,
            ]);

            $info->logo_id = $logo->id;
            $info->save();
        }

        Auth::user()->groupable_id = $info->id;
        Auth::user()->groupable_type = CompanyInfo::class;
        Auth::user()->save();


        $employees = User::Employee()->get();
        foreach($employees as $emp){
            if($emp->hasAnyPermission(['unconfirmedUser', 'confirmedUser' , 'pooyeshRequest' , 'hireRequest'])){
                $emp->notify(new UserRegistred(Auth::user()));
            };
        }

        event(new CompanyRegistered($info->name));

        CompanyLogActivity::addToLog('اطلاعات تکمیلی شرکت را ثبت کرد');

        return redirect(route('company.panel'))->with('success',__('public.user_registered'));
    }

    public function stdCreate()
    {
        $provinces = Province::all();
        $universities = University::all();

        return view('information.student.create' , compact('universities' , 'provinces'));
    }

    public function stdStore(Request $request)
    {
        $age = persian_to_eng_num($request->age);
        $n_code = persian_to_eng_num($request->n_code);
        $duration = persian_to_eng_num($request->duration);
        $std_number = persian_to_eng_num($request->std_number);



        $request->merge([
            'age'=>$age,
            'n_code' => $n_code,
            'duration' =>$duration,
            'std_number' =>$std_number
        ]);

        if (is_null(Auth::user()->sex) && is_null(Auth::user()->email)){
            $request->validate([
               'sex' => 'required',
                'email'=> ['required', 'unique:users', 'email']
            ],[
                'sex.required' => 'وارد کردن جنسیت الزامی است',
                'email.required'=> 'وارد کردن ایمیل الزامی است',
                'email.unique'=> 'این ایمیل قبلا وارد شده است',
                'email.email'=> 'ایمیل را بصورت صحیح وارد کنید'
            ]);
        }

        $request->validate([
            'age'=> 'required|int',
            'grade'=> 'required',
            'n_code'=> 'required|numeric',
            // 'duration'=> 'required|numeric',
            'major'=> 'required',
            'university_id'=> 'required',
            'province' => 'required',
            'city'=> 'required',
            'std_number'=> 'required|numeric',
            'plaque' => 'required',
            'post_code' => 'required',
            'address' => 'required',
            ],[
            'age.required'=>'لطفا سن خود را وارد کنید',
            'age.int'=>'لطفا سن خود را به صورت صحیح وارد کنید',

            'grade.required'=>'لطفا مقطع تحصیلی خود را وارد کنید',

            'n_code.required'=>'لطفا شماره ملی خود را وارد کنید',
            'n_code.numeric'=>'لطفا شماره ملی خود را به صورت عدد وارد کنید',

            // 'duration.required'=>'لطفا تعداد ساعت دوره کارآموزی خود را وارد کنید',
            // 'duration.numeric'=>'لطفا تعداد ساعت دوره کارآموزی خود رابه صورت عدد وارد کنید',

            'major.required'=>'لطفا رشته تحصیلی خود را وارد کنید',

            'university_id.required'=>'لطفا دانشگاه محل تحصیل خود را وارد کنید',

            'province.required' => 'لطفا استان محل سکونت خود را انتخاب کنید',
            'city.required'=>'لطفا شهر محل سکونت خود را وارد کنید',

            'std_number.required'=>'لطفا شماره دانشجویی خود را وارد کنید',
            'std_number.numeric'=>'لطفا شماره دانشجویی خود را به صورت عدد وارد کنید',

            'plaque.required' => 'پلاک الزامی است',
            'post_code.required' => 'کد پستی الزامی است',

            'address.required' => 'آدرس الزامی است'
        ]);

       $info = StudentInfo::create([
            'age' => $request->input('age'),
            'province_id' => $request->input('province'),
            'city_id' => $request->input('city'),
            'university_id' => $request->input('university_id'),
            'major' => $request->input('major'),
            'n_code' => $request->input('n_code'),
            'grade' => $request->input('grade'),
            'duration' => null,
            'std_number' => $request->input('std_number'),
        ]);

       if ($request->has('sex')){
           Auth::user()->sex = $request->sex;
       }
       if ($request->has('email')){
           Auth::user()->email = $request->email;
       }

        Auth::user()->groupable_id = $info->id;
        Auth::user()->groupable_type = StudentInfo::class;
        Auth::user()->save();

        if ($request->has('email')){
            event(new UserRegistered(Auth::user()));
        }
        $employees = User::Employee()->get();
        foreach($employees as $emp){
            if($emp->hasAnyPermission(['unconfirmedUser', 'confirmedUser' , 'pooyeshRequest']) && !is_null($emp->email)){
                $emp->notify(new UserRegistred(Auth::user()));
            };
        }

        StudentLogActivity::addToLog('اطلاعات تکمیلی خود را ثبت کرد');

        $this->sendInfosToSadraSite();

       return redirect(route('std.cv.create'))->with('success',__('public.user_registered'));
    }

    public function sendInfosToSadraSite()
    {
        $user = User::with('groupable.city', 'groupable.province')->find(Auth::id());

        $student = $user->groupable;

        if(! is_null($user->n_code)){

            $user_n_code = $user->n_code;

        } else {

            $user_n_code = $student->n_code;

        }

        $response = Http::acceptJson()
            ->post('https://sadra.iau.ir/api/v1/store_student_full_process' , [

                'user_name' => $user->name,
                'user_family' => $user->family,
                'user_sex' => $user->sex,
                'user_n_code' => $user_n_code,
                'user_birth' => now()->subYears($student->age),
                'user_mobile' => $user->mobile,
                'user_email' => $user->email,
                'user_role' => 'student',
                'user_password' => $user->password,

                'std_province_id' => $student->province_id,
                'std_city_id' => $student->city_id,
                'std_student_code' => $student->std_number,
                'std_grade' => $student->grade,
                'std_major' => $student->major,
                'std_avg' => rand(13,20),
                'std_end_date' => Carbon::now()->addYears(rand(1,4))->toDateString(),

            ]);

        $res_json = $response->json();
    }

    public function storeApplicantInfo(storeApplicantInfoRequest $request)
    {
        if (!is_null(Auth::user()->groupable_id)) {

            return redirect(route('home'));

        }
        if (is_null(Auth::user()->sex) && is_null(Auth::user()->email)){
            $request->validate([
                'sex'=> ['required'],
                'email'=> ['required', 'unique:users', 'email']
            ],[
                'sex.required'=> 'لطفا جنسیت را وارد کنید',
                'email.required'=> 'لطفا ایمیل خود را وارد کنید',
                'email.unique'=> 'این ایمیل قبلا وارد شده است',
                'email.email'=> 'لطفا ایمیل خود را بصورت صحیح وارد کنید'
            ]);
        }
         $request->validate([
             'province' => 'required',
             'city' => 'required',
             'military_status' => 'required',
             'marital_status' => 'required',
             'requested_salary' => 'required',
             'birth' => 'required',
             'experience' => 'required',
             'address' => 'required',
         ],[
             'province:required' => 'استان خود را وارد کنید',
             'city:required' => 'شهر خود را وارد کنید',
             'military_status' => 'وضعیت نظام وضیفه خود را وارد کنید',
             'marital_status' => 'وضعیت تاهل خود را وارد کنید',
             'requested_salary' => 'حقوق درخواستی خود را وارد کنید',
             'birth' => 'تاریخ تولد خود را وارد کنید',
             'experience' => 'سابقه کاری خود را وارد کنید',
             'address' => 'آدرس محل زندگی خود را وارد کنید',
         ]);

        $info = ApplicantInfo::create([
            'province_id' => $request->province,
            'city_id' => $request->city,
            'military_status' => $request->military_status,
            'marital_status' => $request->marital_status,
            'requested_salary' => $request->requested_salary,
            'birth' => persian_to_eng_date($request->birth),
            'address' => $request->address,
            'experience' => $request->experience
        ]);

        if ($request->has('sex')){
            Auth::user()->sex = $request->sex;
        }
        if($request->has('email')){
            Auth::user()->email = $request->email;
        }
        Auth::user()->groupable_id = $info->id;
        Auth::user()->groupable_type = ApplicantInfo::class;

        Auth::user()->save();

        if ($request->has('email')){
            event(new UserRegistered(Auth::user()));
        }
        return redirect(route('std.cv.create'))->with('success',__('public.user_registered'));

    }

    public function selectRole()
    {
        if(Auth::user()->hasRole(['student' , 'company' , 'applicant' , 'employee'])){
            return redirect()->route('dashboard');
        }

        return view('auth.select-role');
    }

    public function storeRole(Request $request)
    {
        $request->validate([
            'role' => 'required'
        ],[
            'role.required' => 'نوع کاربری را انتخاب کنید'
        ]);

        $user = User::find(Auth::id());

        $user->assignRole($request->role);

        return redirect()->route('dashboard');
    }
}
