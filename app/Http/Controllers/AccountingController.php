<?php

namespace App\Http\Controllers;

use App\Models\CompanyInfo;
use App\Models\CompanyPackage;
use App\Models\SuggestedTestJob;
use App\Models\User;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;

class AccountingController extends Controller
{
    public function mbtiIndex()
    {
        $students = User::whereHas('suggestedTestJob')
            ->whereRelation('suggestedTestJob' , 'status' , 1)
            ->count();

        $suggestedTestJobs = SuggestedTestJob::where('status' , 1)
            ->with('payments')
            ->get();

        $suggestedTestJobCount = $suggestedTestJobs->count();
        
        $totalReceived = $this->getMbtiTotalReceived($suggestedTestJobs);

        $suggestedTestJobs = SuggestedTestJob::with('payments', 'buyer', 'discount')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('accounting.mbti-index', compact(
            'students',
            'suggestedTestJobs',
            'suggestedTestJobCount',
            'totalReceived',
        ));
    }

    public function mbtiSearch(Request $request)
    {
        $oldData = [
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'option' => $request->option,
            'status' => $request->status,
            'mobile' => $request->mobile,
        ];

        $query = SuggestedTestJob::query();
        $query = $query->with('payments', 'buyer', 'discount');

        if($request->mobile != '' || $request->mobile != null){
            $query->whereRelation('buyer' , 'mobile' , 'LIKE' , '%'. $request->mobile .'%');
        }

        if($request->option != '' || $request->option != null){
            $query->where('option' , $request->option);
        }

        if($request->status != '' || $request->status != null){
            if($request->status == 1){
                $query->whereNotNull('receipt_id');
            }else{
                $query->whereNull('receipt_id');
            }
        }

        if(!empty($request->start_date) && !empty($request->end_date)){
            $startDate = Verta::parse($request->start_date)->toCarbon();
            $endDate = Verta::parse($request->end_date)->toCarbon();
            
            $query->whereBetween('updated_at' , [$startDate, $endDate]);
        }

        $students = User::whereHas('suggestedTestJob')
            ->whereRelation('suggestedTestJob' , 'status' , 1)
            ->count();

        $suggestedTestJobs = SuggestedTestJob::where('status' , 1)
            ->with('payments')
            ->get();

        $suggestedTestJobCount = $suggestedTestJobs->count();

        $totalReceived = $this->getMbtiTotalReceived($suggestedTestJobs);

        $suggestedTestJobs = $query->orderByDesc('updated_at')
            ->paginate(20)
            ->appends([
                'mobile' => $oldData['mobile'],
                'option' => $oldData['option'],
                'status' => $oldData['status'],
                'start_date' => $oldData['start_date'],
                'end_date' => $oldData['end_date'],
            ]);

        return view('accounting.mbti-index', compact(
            'students',
            'suggestedTestJobs',
            'suggestedTestJobCount',
            'totalReceived',
            'oldData'
        ));
    }

    protected function getMbtiTotalReceived($suggestedTestJobs)
    {
        $totalReceived = 0;
        
        foreach($suggestedTestJobs as $suggestedTestJob){
            foreach($suggestedTestJob->payments as $payment){
                if(! is_null($payment->receipt)){
                    $totalReceived += $payment->price;
                }
            }
        }

        return $totalReceived;
    }

    public function packageIndex()
    {
        $companies = CompanyInfo::whereHas('companyPackage')
            ->whereRelation('companyPackage' , 'status' , 1)
            ->count();

        $packages = CompanyPackage::where('status' , 1)
            ->with('payments')
            ->get();

        $packagesCount = $packages->count();

        $totalReceived = $this->getCompanyPackageTotalReceived($packages);

        $packages = CompanyPackage::with('payments', 'companyInfo.user', 'package')
            ->orderByDesc('updated_at')
            ->paginate(20);

        // dd($packages);

        return view('accounting.package-index', compact(
            'companies',
            'packages',
            'packagesCount',
            'totalReceived',
        ));

    }

    public function packageSearch(Request $request)
    {
        $oldData = [
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'type' => $request->type,
            'status' => $request->status,
            'mobile' => $request->mobile,
        ];

        $query = CompanyPackage::query();
        $query = $query->with('payments', 'companyInfo.user', 'package');

        if($request->mobile != '' || $request->mobile != null){
            $query->whereRelation('companyInfo.user' , 'mobile' , 'LIKE' , '%'. $request->mobile .'%');
        }

        if($request->type != '' || $request->type != null){
            if($request->type == 'full'){
                $query->whereRelation('package','additional_internship_positions' , '!=' , 0)
                    ->whereRelation('package','additional_internship_requests' , '!=' , 0)
                    ->whereRelation('package','additional_internship_confirms' , '!=' , 0);
            }else{
                $query->whereRelation('package', $request->type, '!=', 0);
            }
        }

        if($request->status != '' || $request->status != null){
            if($request->status == 1){
                $query->whereNotNull('receipt_id');
            }else{
                $query->whereNull('receipt_id');
            }
        }

        if(!empty($request->start_date) && !empty($request->end_date)){
            $startDate = Verta::parse($request->start_date)->toCarbon();
            $endDate = Verta::parse($request->end_date)->toCarbon();
            
            $query->whereBetween('updated_at' , [$startDate, $endDate]);
        }

        $companies = CompanyInfo::whereHas('companyPackage')
            ->whereRelation('companyPackage' , 'status' , 1)
            ->count();

        $packages = CompanyPackage::where('status' , 1)
            ->with('payments')
            ->get();

        $packagesCount = $packages->count();

        $totalReceived = $this->getCompanyPackageTotalReceived($packages);

        $packages = $query->orderByDesc('updated_at')
            ->paginate(20)
            ->appends([
                'mobile' => $oldData['mobile'],
                'type' => $oldData['type'],
                'status' => $oldData['status'],
                'start_date' => $oldData['start_date'],
                'end_date' => $oldData['end_date'],
            ]);

        return view('accounting.package-index', compact(
            'companies',
            'packages',
            'packagesCount',
            'totalReceived',
            'oldData'
        ));

    }

    protected function getCompanyPackageTotalReceived($packages)
    {
        $totalReceived = 0;

        foreach($packages as $packeage){
            foreach($packeage->payments as $payment){
                if(! is_null($payment->receipt)){
                    $totalReceived += $payment->price;
                }
            }
        }

        return $totalReceived;
    }

}
