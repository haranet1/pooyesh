<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function verifyMobile(Request $request)
    {
        $user = Auth::user();
        if ($request->code == $user->mobile_code) {
            $user->update([
                'mobile_verified_at' => Carbon::now(),
            ]);
            return redirect()->route('dashboard');

        } else {
            Session::put('error', 'کد وارد شده صحیح نمی باشد');
            return response()->view('auth.verify-mobile');
        }
    }

    public function showVerifyMobile()
    {
        return view('auth.verify-mobile');
    }



}
