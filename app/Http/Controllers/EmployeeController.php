<?php

namespace App\Http\Controllers;

use App\Events\SadraEventCreated;
use App\Exports\ActivityLog;
use App\Exports\AllCompaniesExport;
use App\Exports\AllMbtiResult;
use App\Exports\AllStudentsHasMbtiExport;
use App\Exports\ApplicantExport;
use App\Exports\CompanyExport;
use App\Exports\DoneCompanySadraExport;
use App\Exports\EnneagramTestUsersExport;
use App\Exports\ExhibitionRegisteredCompanies;
use App\Exports\ExhibitionRegisterRequests;
use App\Exports\ImportedApplicantExport;
use App\Exports\ImportedCompanyExport;
use App\Exports\ImportedStudentExport;
use App\Exports\MbtiTestUsersExport;
use App\Exports\StudentsExport;
use App\Exports\UnconfirmedApplicantExport;
use App\Exports\UnconfirmedCompanyExport;
use App\Exports\UnconfirmedStudentExport;
use App\Imports\UsersImport;
use App\Mail\TestEmail;
use App\Mail\VerificationEmail;
use App\Models\Announcement;
use App\Models\ApplicantInfo;
use App\Models\BuyPlan;
use App\Models\company_sadra;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\Discount;
use App\Models\EmployeeInfo;
use App\Models\EmployeeLogActivity;
use App\Models\File;
use App\Models\Grader;
use App\Models\OjobPosition;
use App\Models\CompanyLogActivity;
use App\Models\CompanyTicket;
use App\Models\MbtiPersonality;
use App\Models\MbtiResult;
use App\Models\NeoResult;
use App\Models\News;
use App\Models\Payment;
use App\Models\Photo;
use App\Models\PooyeshCertificate;
use App\Models\PooyeshContract;
use App\Models\ResumeSent;
use App\Models\Sadra;
use App\Models\SadraBooth;
use App\Models\StudentInfo;
use App\Models\StudentLogActivity;
use App\Models\Ticket;
use App\Models\University;
use App\Models\User;
use App\Notifications\UniAcceptedPooyeshRequest;
use App\Notifications\UniRejectedPooyeshRequest;
use App\Services\Notification\Notification;
use Hekmatinasser\Verta\Facades\Verta;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Morilog\Jalali\Jalalian;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use niklasravnsborg\LaravelPdf\PdfWrapper;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Exp;

use function Ramsey\Uuid\v1;

class EmployeeController extends Controller
{

    public function ajax(Request $request)
    {
        switch ($request->do) {
            case'confirm-user':
               $user = User::findOrFail($request->id);
               $user->update(['status' => 1]);
                \EmployeeLogActivity::addToLog('اکانت کاربر '.$user->name.''. $user->family.' را تایید کرد');
                $notif = new Notification();
                $notif->sendSms($user,'fanyarAcceptUserInfo',[$user->name,$user->family]);
                return response()->json(true);
                break;

            case'confirm-all-users':
               $users = User::wherein('id', $request->users)->get();
               if (is_null($users) || empty($users) || count($users)<1){
                   return response()->json('Empty Array');
               }

                $notif = new Notification();
               foreach ($users as $user){
                   $user->update(['status' => 1]);
                   $name = $user->name." ".$user->family;
                   $notif->sendSms($user,'fanyarAcceptUserInfo',[$name]);
               }
                \EmployeeLogActivity::addToLog('اکانت چند کاربر را تایید کرد ');

                return response()->json(true);

            case'check-pooyesh-certificate':
                $request = CooperationRequest::where('id',$request->id)->with('certificate')->first();
                if ($request->certificate) return response()->json(1);
                else return response()->json(0);

            case'send-std-pooyesh-certificate':
                $request = CooperationRequest::find($request->id);
                if ($request){
                    $pdf = new PdfWrapper();
                    $file_name = $request->id . time() . 'certificate';
                    $file_path = 'files/pooyesh-certificates/' . $file_name . '.pdf';

                    $pdf->loadView('certificate-pattern',[],[],[
                        'mode' => 'utf-8'
                    ])->save($file_path);

                    $certificate = PooyeshCertificate::create([
                        'name'=>$file_name,
                        'path'=>$file_path
                    ]);
                    $request->certificate_id = $certificate->id;
                    $request->save();
                    \EmployeeLogActivity::addToLog('گواهی پایان دوره کارآموزی پویش صادر کرد');

                //    $pdf = PDF::loadView('introduction-letter-pattern',[]);
                    return true;
                }
                return response()->json('error');

            case'add-booth':
                $names = explode('.' , $request->name);
                $array = [];
                foreach($names as $item){
                    array_push($array , ['plan_id' => $request->plan_id , 'name' => $item , 'created_at' => Carbon::now() , 'updated_at' => Carbon::now()]);
                }
               return response()->json( SadraBooth::insert($array));

            case'confirm-sadra-registration':
                $request = company_sadra::find($request->req_id);
                $request->update(['status'=>1]);
                $company = $request->company;

                Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
                    'receptor' => $company->mobile,
                    'token' => str_replace(' ','-',trim($request->sadra->title)),
                    'template' => 'fanyarcoEventReqAccept',
                ]);
                \EmployeeLogActivity::addToLog('درخواست ثبت نام در نمایشگاه را تایید کرد');

                return response()
                    ->json();

            case'decline-sadra-registration':
                $sadra = company_sadra::with('booth')->find($request->req_id);
                \EmployeeLogActivity::addToLog('درخواست ثبت نام در نمایشگاه را رد کرد');
                $sadra->booth->update([
                    'reserved' => false,
                ]);
                return response()
                    ->json($sadra->update(['status'=>0]));

            case'check-payment':

                $sadra=company_sadra::with('payments')->find($request->req_id);
                if (count($sadra->payments)==0)
                    return response()->json(['success' => true,'message'=>'no-payment']);

                $response = Http::withOptions([
                    'verify' => false
                ])
                ->post('https://api.iaun.ac.ir/api/login',[
                    'username' => 'apifanyaruser',
                    'password' => 'AapiUser@FanYar',
                    'usertype' => 3,
                ]);
                $dataArray = $response->json();
                $token = $dataArray['data']['token'];
                $successPay=false;
                foreach ($sadra->payments as $pay){
                    $response = Http::withOptions([
                        'verify' => false
                    ])
                    ->withHeaders([
                        'accept' => 'application/json',
                    ])->withToken($token)
                        ->accept('application/json')
                        ->post('https://api.iaun.ac.ir/api/payments/verify',[
                            'Accept' => 'application/json',
                            'payment_key' => $pay->payment_key,
                        ]);
                    if(isset($response['success']) && $response['success'] == true){
                        if($response['data']['pay_status'] == "2"){
                            $pay->update([
                                'status' => 'عملیات پرداخت الکترونیک با موفقیت انجام شده است.',
                                'tracking_code' => $response['data']['pay_tracenumber'],
                                'receipt' => $response['data']['pay_rrn'],
                                'card_number' => $response['data']['pay_cardnumber'],
                                'payer_bank' => $response['data']['pay_issuerbank'],
                                'payment_date_time' => $response['data']['pay_paymentdatetime'],
                            ]);
                            $sadra->update([
                                'status' => true,
                                'receipt_id' => $pay->id,
                            ]);
                            $successPay=true;
                        }elseif($response['data']['pay_status'] == "3"){
                            $pay->update([
                                'status' => 'عملیات پرداخت الکترونیک با مشکل روبرو شده است.'
                            ]);
                        }elseif($response['data']['pay_status'] == "4"){
                            $pay->update([
                                'status' => 'تراکنش تکراری است.',
                            ]);
                        }elseif($response['data']['pay_status'] == "5"){
                            $pay->update([
                                'status' => 'تراکنش توسط کاربر لغو شده است.',
                            ]);
                        }
                    }
                }

                return response()->json(['success' => true,'message'=>$successPay,'data'=>$sadra]);

            case'accept-pooyesh-request':
                $request = CooperationRequest::find($request->req_id);
                $request->process->accepted_by_uni_at = now();
                $request->process->save();
                $company = User::company()->with('groupable')->find($request->receiver_id);
                $user = User::StudentAndApplicant()->with('groupable')->find($request->sender_id);

                $pdf = new PdfWrapper();
                $file_name = $request->req_id . time() . '-contract';
                $file_path = 'files/pooyesh-contracts/' . $file_name . '.pdf';

                $pdf->loadView('contract-pattern',['company'=>$company,'user'=>$user],[],[
                    'mode' => 'utf-8'
                ])->save($file_path);

                $contract = new PooyeshContract();
                $contract->name = $file_name;
                $contract->path = $file_path;
                $contract->save();
                $request->contract_id = $contract->id;
                $request->sender->notify(new UniAcceptedPooyeshRequest());
                $request->receiver->notify(new UniAcceptedPooyeshRequest());
                \EmployeeLogActivity::addToLog('درخواست پویش با شناسه'.$request->req_id.'را تایید کرد ');

                return response()
                    ->json( $request->save());

            case'reject-pooyesh-request':
                $request = CooperationRequest::find($request->req_id);
                $request->process->rejected_by_uni_at = now();
                $request->sender->notify(new UniRejectedPooyeshRequest());
                $request->receiver->notify(new UniRejectedPooyeshRequest());
                \EmployeeLogActivity::addToLog('درخواست پویش با شناسه '.$request->id.' را رد کرد');

                return response()
                    ->json(  $request->process->save());
            // case'print-pooyesh-contract':
            //     $request = CooperationRequest::find($request->req_id);
            //     $request->process->contract_printed_at = now();
            //     return response()->json($request->process->save());


            case'delete-file':
                $file = File::findorfail($request->id);
                unlink($file->path);
                \EmployeeLogActivity::addToLog('فایل با شناسه '.$file->id.' را حذف کرد');

                return  response()->json($file->delete());

            case'delete-announcement':
                $announce = Announcement::findorfail($request->id);
                if ($announce->file){
                    unlink($announce->file->path);
                }
                \EmployeeLogActivity::addToLog('اطلاعیه با شناسه '.$announce->id.' را حذف کرد');

                return response()->json($announce->delete());

            case'confirm-job-position':
                $job = OjobPosition::with('company')->findOrFail($request->id);
                $job->update(['status' => 1]);
                \EmployeeLogActivity::addToLog('موقعیت شغلی با شناسه '.$request->id.' را تایید کرد');
                $notif = new Notification();
                $notif->sendSms($job->company,'fanyarAcceptNewPosition',[job_type_persian($job->type),$job->title]);
                return response()->json(true);

            case'confirm-all-job-positions':
                $jobs = OjobPosition::wherein('id', $request->jobs)->get();
                if (is_null($jobs) || empty($jobs) || count($jobs)<1){
                    return response()->json('Empty Array');
                }

                $notif = new Notification();
                foreach ($jobs as $job){
                    $job->update(['status' => 1]);
                    $notif->sendSms($job->company,'fanyarAcceptNewPosition',[job_type_persian($job->type),$job->title]);
                }
                \EmployeeLogActivity::addToLog('چند موقعیت شغلی را تایید کرد');
                return response()->json(true);

        }
    }

    public function ajax_search(Request $request)
    {
        switch ($request->do){
            case'search-companies':
                $companies = User::company()->confirmed()
                    ->with('groupable')
                    ->wherehas('groupable',function ($query) use($request){
                        $query->where('name','like','%'.$request->key.'%');
                    })
                    ->get();
                return response()->json(['data'=>$companies]);

            case'search-students':
                $students = User::StudentAndApplicant()->confirmed()
                    ->with('groupable','groupable.city')
                    ->where(function ($query) use ($request){
                        $query->where('name','like','%'.$request->key.'%')
                            ->orwhere('family','like','%'.$request->key.'%');
                    })
                    ->get();
                return response()->json(['data'=>$students]);

            case 'search-tickets':
                $tickets = Ticket::where('subject','like','%'.$request->key.'%')->with('sender')->get();
                if (count($tickets) > 0){
                    foreach ($tickets as $ticket){
                        $ticket->sender_name = getUserNameByID($ticket->sender_id);
                    }
                }

                return response()->json(['data'=>$tickets]);

            case 'search-pooyesh-req-by-sender':
                $requests = CooperationRequest::where('type','intern')->with('sender','receiver','process')
                    ->whereHas('sender',function ($query) use ($request){
                        $query->where('name','like','%'.$request->key.'%')
                            ->orwhere('family','like','%'.$request->key.'%');
                    })
                    ->get();
                foreach ($requests as $request){
                    $request->created_at = verta($request->created_at)->formatDate();
                    if ($request->sender->groupable instanceof CompanyInfo){
                        $request->sender_type = 'co';
                    }elseif ($request->sender->groupable instanceof StudentInfo){
                        $request->sender_type = 'std';
                    }

                    if ($request->receiver->groupable instanceof CompanyInfo){
                        $request->receiver_type = 'co';
                    }elseif ($request->receiver->groupable instanceof StudentInfo){
                        $request->receiver_type = 'std';
                    }
                }
                return response()->json($requests);

            case'search-logs-by-user-name':
                $model = null ;
                switch ($request->model){
                    case'co':
                        $model = CompanyLogActivity::class;
                        break;

                    case'std':
                        $model = StudentLogActivity::class;
                        break;

                    case 'emp':
                        $model = EmployeeLogActivity::class;
                        break;
                }

                $logs = $model::with('user')->whereHas('user',function ($query) use($request){
                    $query->where('name','like','%'.$request->key.'%')
                        ->orwhere('family','like','%'.$request->key.'%')
                        ->orwhere([['family','like','%'.$request->key.'%'],['name','like','%'.$request->key.'%']]);
                })->get();
                return response()->json($logs);

        }
    }

    public function storeEmployeeInfo(Request $request)
    {
        $n_code = persian_to_eng_num($request->n_code);
        $p_code = persian_to_eng_num($request->p_code);

        $request->merge([
            'n_code'=>$n_code,
            'p_code' => $p_code,
        ]);

        $request->validate([
            'province' =>'required',
            'city' =>'required',
            'university_id' =>'required',
            'n_code' =>'required|numeric',
            'p_code' =>'required|numeric',
            'uni_post' =>'required',
        ]);
        $info = EmployeeInfo::create([
            'province_id' => $request->input('province'),
            'city_id' => $request->input('city'),
            'university_id' => $request->input('university_id'),
            'n_code' => $request->input('n_code'),
            'p_code' => $request->input('p_code'),
            'uni_post' => $request->input('uni_post'),
        ]);

        Auth::user()->groupable_id = $info->id;
        Auth::user()->groupable_type = EmployeeInfo::class;
        Auth::user()->save();
        \EmployeeLogActivity::addToLog('اطلاعات تکمیلی پروفایل خود را ثبت کرد');

        return redirect(route('employee.panel'));
    }

    public function index()
    {
        $students = User::StudentAndApplicant()->confirmed()->count();
        $companies = User::Company()->confirmed()->count();
        $hire_positions = OjobPosition::hire()->count();
        $intern_positions = OjobPosition::Intern()->count();
        $files = File::whereNull('groupable_id')->get();
        $tickets = Ticket::with('sender')->orderByDesc('id')->take(5)->get();
        $announcements = Announcement::orderByDesc('id')->take(20)->get();
        $news = News::Active()->orderByDesc('id')->take(20)->get();
        $mbti_test_user = User::whereHas('mbti_test')->count();
        $enneagram_test_user = User::whereHas('enneagram_test')->count();
        $neo_test_user = User::whereHas('neo_test')->count();
        $haaland_test_user = User::whereHas('haaland_test')->count();
        $ghq_test_user = User::whereHas('ghq_test')->count();

        $mbtiCount = DB::table('mbti_results')
            ->select('personality_type', DB::raw('COUNT(*) as count'))
            ->groupBy('personality_type')
            ->get();
        $mbtiType = [];
        $mbtiCountOfType = [];
        foreach($mbtiCount as $mbti){
            array_push( $mbtiType , $mbti->personality_type);
            array_push( $mbtiCountOfType , $mbti->count);
        }
        $mbtiType = mbti_personality_to_fa($mbtiType);
        $mbtiType = json_encode($mbtiType);
        $mbtiCountOfType = json_encode($mbtiCountOfType);

        $enneagramCount = DB::table('enneagram_results')->select('main_character' ,DB::raw('COUNT(*) as count'))->whereNotNull('main_character')->groupBy('main_character')->get();
        $enneagramType = [];
        $enneagramCountOfType = [];
        foreach($enneagramCount as $enneagram){
            array_push( $enneagramType , $enneagram->main_character);
            array_push( $enneagramCountOfType , $enneagram->count);
        }
        $enneagramType = enneagram_personality_to_fa($enneagramType);
        $enneagramType = json_encode($enneagramType);
        $enneagramCountOfType = json_encode($enneagramCountOfType);

        $neoAverages = DB::table('neo_results')
            ->selectRaw('AVG(N) as N, AVG(E) as E, AVG(O) as O, AVG(A) as A, AVG(C) as C')
            ->first();

        $neoType = []; $neoAvg = [];
        foreach($neoAverages as $key => $value){
            array_push( $neoType , $key);
            array_push( $neoAvg , $value);
        }
        $neoType = neo_personality_to_fa($neoType);
        $neoType = json_encode($neoType);
        $neoAvg = json_encode($neoAvg);

        $haalandAverages = DB::table('haaland_results')
            ->selectRaw('AVG(R) as R, AVG(I) as I, AVG(A) as A, AVG(S) as S, AVG(E) as E, AVG(C) as C')
            ->first();
        $haalandType = []; $haalandAvg = [];
        foreach($haalandAverages as $key => $value){
            array_push( $haalandType , $key);
            array_push( $haalandAvg , $value);
        }
        $haalandType = haaland_personality_to_fa($haalandType);
        $haalandType = json_encode($haalandType);
        $haalandAvg = json_encode($haalandAvg);

        $ghqAverages = DB::table('ghq_results')
            ->selectRaw('AVG(physical) as physical, AVG(anxiety) as anxiety, AVG(social) as social, AVG(depression) as depression, AVG(sum_of_all) as sum_of_all')
            ->first();
        $ghqType = []; $ghqAvg = [];
        foreach($ghqAverages as $key => $value){
            array_push( $ghqType , $key);
            array_push( $ghqAvg , $value);
        }
        $ghqType = ghq_personality_to_fa($ghqType);
        $ghqType = json_encode($ghqType);
        $ghqAvg = json_encode($ghqAvg);
        return view('panel.employee.dashboard',
            compact(['students', 'companies', 'hire_positions', 'intern_positions','files',
            'tickets','announcements','news','mbtiType','mbtiCountOfType','enneagramType','enneagramCountOfType',
            'neoType', 'neoAvg','haalandType','haalandAvg','mbti_test_user','enneagram_test_user','neo_test_user','haaland_test_user',
            'ghqAvg', 'ghqType','ghq_test_user']));


    }

    public function unconfirmeddepressiontudents()
    {
        $students = User::student()->unconfirmed()->orderbyDesc('id')->paginate(20);
        return view('panel.employee.unconfirmed-stds', compact('students'));
    }

    public function unconfirmedCompanies()
    {
        $companies = User::company()->unconfirmed()->orderbyDesc('id')->paginate(20);
        return view('panel.employee.unconfirmed-companies',compact('companies'));
    }

    public function studentsList()
    {
        $students = User::student()->confirmed()->paginate(20);
        return view('panel.employee.students',compact('students'));
    }

    public function companiesList()
    {
        $companies = User::company()->confirmed()->paginate(20);
        return view('panel.employee.companies',compact('companies'));
    }

    public function createUser()
    {
        return view('panel.employee.create-user');
    }

    public function storeUser(Request $request)
    {
       $request->validate([
           'name' => ['required', 'string', 'max:25'],
           'family' => ['required', 'string', 'max:40'],
           'mobile' => ['required', 'string', 'max:11', 'unique:users'],
           'password' => ['required', 'string', 'min:8'],
           'role' => ['required'],
           'email' => ['required', 'unique']
       ],[
           'role.required'=>'نوع کاربری الزامی است',
           'name.required'=>'نام الزامی است',
           'name.string'=>'نام را به صورت صحیح وارد کنید',
           'name.max'=>'طول نام بیش از حد مجاز است',
           'family.required'=>'نام خانوادگی الزامی است',
           'family.string'=>'نام خانوادگی را به صورت صحیح وارد کنید',
           'family.max'=>'طول نام خانوادگی بیش از حد مجاز است',
           'mobile.required'=>'موبایل الزامی است',
           'mobile.max'=>'موبایل را به صورت صحیح وارد کنید',
           'mobile.unique'=>'موبایل تکراری است',
           'password.required'=>'کلمه عبور الزامی است',
           'password.min'=>'کلمه عبور حداقل باید 8 کاراکتر باشد',
           'email.required' => 'وارد کردن ایمیل اجباری است',
           'email.unique' => 'ایمیل تکراری است'
       ]);

        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'mobile' => $request->mobile,
            'sex' => $request->sex,
            'password' => Hash::make($request->password),
            'email' => $request->email,
        ]);
        $user->assignRole($request->role);
        \EmployeeLogActivity::addToLog('یک کاربر با نام '.$user->name." ".$user->family.' ثبت کرد');

        return redirect(route('employee.create.user'))->with('success',__('public.user_added'));
    }

    public function storeUserByExcel(Request $request)
    {
        if (is_null($request->role)) return back()->with('error','لطفا نوع کاربران را انتخاب کنید');
        Excel::import(new UsersImport($request->role , $request->sms),\request()->file('users'));
        \EmployeeLogActivity::addToLog('تعدادی کاربر از طریق فایل اکسل ثبت کرد');

        return back()->with('success',__('public.users_added'));
    }

    public function exportAllUserShow()
    {
        return view('panel.employee.export-users');
    }

    public function exportAllMbtiUsers()
    {
        \EmployeeLogActivity::addToLog('از همه کاربرانی که تست mbti داده اند خروجی اکسل گرفت.');
        return Excel::download(new AllMbtiResult , 'all-mbti-users.xlsx');
    }

    public function exportAllCompanies()
    {
        \EmployeeLogActivity::addToLog('از همه شرکت ها خروجی اکسل گرفت');

        return Excel::download(new AllCompaniesExport,'companies.xlsx');
    }

    public function exportAllMbtiStudents()
    {
        \EmployeeLogActivity::addToLog('از همه شرکت ها خروجی اکسل گرفت');

        return Excel::download(new AllStudentsHasMbtiExport,'mbti-students.xlsx');
    }

    public function exportStds()
    {
        \EmployeeLogActivity::addToLog('از لیست دانشجویان تایید شده خروجی اکسل گرفت');

        return Excel::download(new StudentsExport,'students.xlsx');
    }

    public function exportApplicants()
    {
        \EmployeeLogActivity::addToLog('از لیست کارجویان تایید شده خروجی اکسل گرفت');

        return Excel::download(new ApplicantExport,'applicants.xlsx');
    }

    public function exportCompanies()
    {
        \EmployeeLogActivity::addToLog('از لیست شرکت ها تایید شده خروجی اکسل گرفت');

        return Excel::download(new CompanyExport,'companies.xlsx');
    }

    public function exportUnconfirmedCompanies()
    {
        \EmployeeLogActivity::addToLog('از لیست شرکت ها تایید نشده خروجی اکسل گرفت');

        return Excel::download(new UnconfirmedCompanyExport,'UnConfirmedCompanies.xlsx');
    }

    public function exportUnconfirmedApplicants()
    {
        \EmployeeLogActivity::addToLog('از لیست کارجویان تایید نشده خروجی اکسل گرفت');

        return Excel::download(new UnconfirmedApplicantExport,'UnConfirmedApplicants.xlsx');
    }

    public function exportUnconfirmedStds()
    {
        \EmployeeLogActivity::addToLog('از لیست دانشجویان تایید نشده خروجی اکسل گرفت');

        return Excel::download(new UnconfirmedStudentExport,'UnConfirmedStudents.xlsx');
    }

    public function exportImportedCompanies()
    {
        \EmployeeLogActivity::addToLog('از لیست شرکت های ایمپورت شده خروجی اکسل گرفت');

        return Excel::download(new ImportedCompanyExport,'ImportedCompanies.xlsx');
    }

    public function exportImportedApplicants()
    {
        \EmployeeLogActivity::addToLog('از لیست کارجویان ایمپورت شده خروجی اکسل گرفت');

        return Excel::download(new ImportedApplicantExport,'ImportedApplicants.xlsx');
    }

    public function exportImportedStds()
    {
        \EmployeeLogActivity::addToLog('از لیست دانشجویان ایمپورت شده خروجی اکسل گرفت');

        return Excel::download(new ImportedStudentExport,'ImportedStudents.xlsx');
    }

    public function exportMbtiTest()
    {
        \EmployeeLogActivity::addToLog('از لیست کاربرانی که تست MBTI داده اند خروجی اکسل گرفت');

        return Excel::download(new MbtiTestUsersExport,'MbtiStudents.xlsx');
    }

    public function exportEnneagramTest()
    {
        \EmployeeLogActivity::addToLog('از لیست کاربرانی که تست MBTI داده اند خروجی اکسل گرفت');

        return Excel::download(new EnneagramTestUsersExport,'EnneagramStudents.xlsx');
    }

    public function pooyesh_requests()
    {
        $requests = CooperationRequest::where('type','intern')
        ->with('sender','receiver','process')
        ->orderByDesc('updated_at')
        ->paginate(20);
        foreach ($requests as $request){
            if ($request->sender->groupable instanceof CompanyInfo){
                $request->sender_type = 'co';
            }elseif ($request->sender->groupable instanceof StudentInfo){
                $request->sender_type = 'std';
            }elseif ($request->sender->groupable instanceof ApplicantInfo){
                $request->sender_type = 'apl';
            }

            if ($request->receiver->groupable instanceof CompanyInfo){
                $request->receiver_type = 'co';
            }elseif ($request->receiver->groupable instanceof StudentInfo){
                $request->receiver_type = 'std';
            }elseif ($request->receiver->groupable instanceof ApplicantInfo){
                $request->receiver_type = 'apl';
            }
        }
        return view('panel.employee.pooyesh-requests',compact('requests'));
    }

    public function hire_requests()
    {
        $requests = CooperationRequest::where('type','full-time')
        ->orWhere('type','part-time')
        ->orWhere('type','remote')
        ->with('sender','receiver.groupable','process')
        ->orderByDesc('created_at')
        ->paginate(20);
        // dd($requests[12]);
        // foreach($requests as $request){
        //     dump($request->sender->groupable_type);
        // }
        foreach ($requests as $request){
            if ($request->sender->groupable instanceof CompanyInfo){
                $request->sender_type = 'co';
            }elseif ($request->sender->groupable instanceof StudentInfo){
                $request->sender_type = 'std';
            }elseif ($request->sender->groupable instanceof ApplicantInfo){
                $request->sender_type = 'app';
            }

            if ($request->receiver->groupable instanceof CompanyInfo){
                $request->receiver_type = 'co';
            }elseif ($request->receiver->groupable instanceof StudentInfo){
                $request->receiver_type = 'std';
            }elseif ($request->receiver->groupable instanceof ApplicantInfo){
                $request->receiver_type = 'app';
            }
        }
        // foreach($requests as $request){
        //     dd($request->sender_type);
        // }
        return view('panel.employee.hire-requests',compact('requests'));
    }

    // shode
    public function studentResumeListShow()
    {
        $requests = ResumeSent::with('sender.groupable.province' , 'sender.groupable.city','receiver.groupable')->orderBy('updated_at','desc')->paginate(20);
        return view('panel.employee.sent-resume' , compact('requests'));
    }

    // public function sadra_create_event()
    // {
    //     return view('panel.employee.create-sadra-event');
    // }

    // public function sadra_index()
    // {
    //     $events = Sadra::with('buy_plans')->paginate(20);

    //     return view('panel.employee.sadra-events-list',compact('events'));
    // }

    // public function sadra_edit(Sadra $sadra)
    // {
    //     return view('panel.employee.edit-sadra-event',compact('sadra'));
    // }

    // public function sadra_update(Request $request,Sadra $sadra)
    // {
    //     $sadra->title = $request->title;
    //     $sadra->description = $request->description;
    //     if (strpos($request->start, '/') !== false) {
    //         $startDate = explode('/', $request->start);

    //         $month = (int)persian_to_eng_num($startDate[1]);
    //         $day = (int)persian_to_eng_num($startDate[2]);
    //         $year = (int)persian_to_eng_num($startDate[0]);

    //         $jalaliDate = \verta();
    //         $jalaliDate->year($year);
    //         $jalaliDate->month($month);
    //         $jalaliDate->day($day);
    //         $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
    //         $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
    //     } else {
    //         $startDateGre = Carbon::instance(Verta::parse($request->start)->datetime());
    //     }
    //     if (strpos($request->end, '/') !== false) {
    //         $endDate = explode('/', $request->end);

    //         $month = (int)persian_to_eng_num($endDate[1]);
    //         $day = (int)persian_to_eng_num($endDate[2]);
    //         $year = (int)persian_to_eng_num($endDate[0]);

    //         $jalaliDate = \verta();
    //         $jalaliDate->year($year);
    //         $jalaliDate->month($month);
    //         $jalaliDate->day($day);
    //         $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
    //         $endDateGre = $jalaliDate->toCarbon('Asia/Tehran');
    //     } else {
    //         $endDateGre = Carbon::instance(Verta::parse($request->end)->datetime());
    //     }
    //     if (strpos($request->start_register, '/') !== false) {
    //         $startRegDate = explode('/', $request->start_register);

    //         $month = (int)persian_to_eng_num($startRegDate[1]);
    //         $day = (int)persian_to_eng_num($startRegDate[2]);
    //         $year = (int)persian_to_eng_num($startRegDate[0]);

    //         $jalaliDate = \verta();
    //         $jalaliDate->year($year);
    //         $jalaliDate->month($month);
    //         $jalaliDate->day($day);
    //         $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
    //         $startRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
    //     } else {
    //         $startRegDateGre = Carbon::instance(Verta::parse($request->start_register)->datetime());
    //     }

    //     if (strpos($request->end_register, '/') !== false) {
    //         $endRegDate = explode('/', $request->end_register);

    //         $month = (int)persian_to_eng_num($endRegDate[1]);
    //         $day = (int)persian_to_eng_num($endRegDate[2]);
    //         $year = (int)persian_to_eng_num($endRegDate[0]);

    //         $jalaliDate = \verta();
    //         $jalaliDate->year($year);
    //         $jalaliDate->month($month);
    //         $jalaliDate->day($day);
    //         $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
    //         $endRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
    //     } else {
    //         $endRegDateGre = Carbon::instance(Verta::parse($request->end_register)->datetime());
    //     }
    //     $sadra->start_at = $startDateGre;
    //     $sadra->end_at = $endDateGre;
    //     $sadra->start_register_at = $startRegDateGre;
    //     $sadra->end_register_at = $endRegDateGre;
    //     $sadra->save();
    //     if ($request->file('photo')) {
    //        if ($sadra->photo){
    //            unlink($sadra->photo->path);
    //            $sadra->photo->delete();
    //        }
    //         $name = time() . $request->photo->getClientOriginalName();
    //         $path = $request->photo->storeAs(
    //             'img/sadra',
    //             $name,
    //             'public'
    //         );
    //         $photo = new Photo();
    //         $photo->name = $name;
    //         $photo->path = $path;
    //         $photo->groupable_id = $sadra->id;
    //         $photo->groupable_type = Sadra::class;
    //         $photo->save();
    //         $sadra->photo_id = $photo->id;
    //         $sadra->save();
    //     }
    //     if ($request->file('map')) {
    //         if ($sadra->map){
    //             unlink($sadra->map->path);
    //             $sadra->map->delete();
    //         }
    //         $name = time() . $request->map->getClientOriginalName();
    //         $path = $request->map->storeAs(
    //             'img/sadra',
    //             $name,
    //             'public'
    //         );
    //         $photo = new Photo();
    //         $photo->name = $name;
    //         $photo->path = $path;
    //         $photo->groupable_id = $sadra->id;
    //         $photo->groupable_type = Sadra::class;
    //         $photo->save();
    //         $sadra->map_id = $photo->id;
    //         $sadra->save();
    //     }
    //     if ($request->file('banner')) {
    //         $name = time() . $request->banner->getClientOriginalName();
    //         $path = $request->banner->storeAs(
    //             'img/sadra',
    //             $name,
    //             'public'
    //         );
    //         $photo = new Photo();
    //         $photo->name = $name;
    //         $photo->path = $path;
    //         $photo->groupable_id = $sadra->id;
    //         $photo->groupable_type = Sadra::class;
    //         $photo->save();
    //         $sadra->banner_id = $photo->id;
    //         $sadra->save();
    //     }
    //     \EmployeeLogActivity::addToLog(' رویداد/نمایشگاه '.$sadra->title.' را ویرایش کرد');

    //     return redirect(route('index.sadra'))->with('success',__('public.data_added'));
    // }

    // public function sadra_store_event(Request $request)
    // {
    //     $request->validate([
    //         'title'=>'required',
    //         'start'=>'required',
    //         'start_register'=>'required',
    //         'end'=>'required',
    //         'end_register'=>'required',
    //         'description'=>'required',
    //         'photo'=>'image',
    //         'map'=>'image',
    //         'banner'=>'image',
    //     ]);

    //     $sadra = new Sadra();
    //     $sadra->title = $request->title;
    //     $sadra->description = $request->description;
    //     if (strpos($request->start, '/') !== false) {
    //         $startDate = explode('/', $request->start);

    //         $month = (int)persian_to_eng_num($startDate[1]);
    //         $day = (int)persian_to_eng_num($startDate[2]);
    //         $year = (int)persian_to_eng_num($startDate[0]);

    //         $jalaliDate = \verta();
    //         $jalaliDate->year($year);
    //         $jalaliDate->month($month);
    //         $jalaliDate->day($day);
    //         $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
    //         $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
    //     } else {
    //         $startDateGre = Carbon::instance(Verta::parse($request->start)->datetime());
    //     }
    //     if (strpos($request->end, '/') !== false) {
    //         $endDate = explode('/', $request->end);

    //         $month = (int)persian_to_eng_num($endDate[1]);
    //         $day = (int)persian_to_eng_num($endDate[2]);
    //         $year = (int)persian_to_eng_num($endDate[0]);

    //         $jalaliDate = \verta();
    //         $jalaliDate->year($year);
    //         $jalaliDate->month($month);
    //         $jalaliDate->day($day);
    //         $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
    //         $endDateGre = $jalaliDate->toCarbon('Asia/Tehran');
    //     } else {
    //         $endDateGre = Carbon::instance(Verta::parse($request->end)->datetime());
    //     }

    //     if (strpos($request->start_register, '/') !== false) {
    //         $startRegDate = explode('/', $request->start_register);

    //         $month = (int)persian_to_eng_num($startRegDate[1]);
    //         $day = (int)persian_to_eng_num($startRegDate[2]);
    //         $year = (int)persian_to_eng_num($startRegDate[0]);

    //         $jalaliDate = \verta();
    //         $jalaliDate->year($year);
    //         $jalaliDate->month($month);
    //         $jalaliDate->day($day);
    //         $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
    //         $startRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
    //     } else {
    //         $startRegDateGre = Carbon::instance(Verta::parse($request->start_register)->datetime());
    //     }

    //     if (strpos($request->end_register, '/') !== false) {
    //         $endRegDate = explode('/', $request->end_register);

    //         $month = (int)persian_to_eng_num($endRegDate[1]);
    //         $day = (int)persian_to_eng_num($endRegDate[2]);
    //         $year = (int)persian_to_eng_num($endRegDate[0]);

    //         $jalaliDate = \verta();
    //         $jalaliDate->year($year);
    //         $jalaliDate->month($month);
    //         $jalaliDate->day($day);
    //         $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
    //         $endRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
    //     } else {
    //         $endRegDateGre = Carbon::instance(Verta::parse($request->end_register)->datetime());
    //     }

    //     $sadra->start_at = $startDateGre;
    //     $sadra->end_at = $endDateGre;
    //     $sadra->start_register_at = $startRegDateGre;
    //     $sadra->end_register_at = $endRegDateGre;
    //     $sadra->save();
    //     if ($request->file('photo')) {
    //         $name = time() . $request->photo->getClientOriginalName();
    //         $path = $request->photo->storeAs(
    //             'img/sadra',
    //             $name,
    //             'public'
    //         );
    //         $photo = new Photo();
    //         $photo->name = $name;
    //         $photo->path = $path;
    //         $photo->groupable_id = $sadra->id;
    //         $photo->groupable_type = Sadra::class;
    //         $photo->save();
    //         $sadra->photo_id = $photo->id;
    //         $sadra->save();
    //     }
    //     if ($request->file('map')) {
    //         $name = time() . $request->map->getClientOriginalName();
    //         $path = $request->map->storeAs(
    //             'img/sadra',
    //             $name,
    //             'public'
    //         );
    //         $photo = new Photo();
    //         $photo->name = $name;
    //         $photo->path = $path;
    //         $photo->groupable_id = $sadra->id;
    //         $photo->groupable_type = Sadra::class;
    //         $photo->save();
    //         $sadra->map_id = $photo->id;
    //         $sadra->save();
    //     }

    //     if ($request->file('banner')) {
    //         $name = time() . $request->banner->getClientOriginalName();
    //         $path = $request->banner->storeAs(
    //             'img/sadra',
    //             $name,
    //             'public'
    //         );
    //         $photo = new Photo();
    //         $photo->name = $name;
    //         $photo->path = $path;
    //         $photo->groupable_id = $sadra->id;
    //         $photo->groupable_type = Sadra::class;
    //         $photo->save();
    //         $sadra->banner_id = $photo->id;
    //         $sadra->save();
    //     }

    //     event(new SadraEventCreated($sadra->title,$sadra->end_at));
    //     \EmployeeLogActivity::addToLog('نمایشگاه / رویداد '.$sadra->title.' را ثبت کرد');

    //     return redirect(route('index.sadra'))->with('success',__('public.sadra_added'));
    // }

    // public function sadra_destroy(Request $request)
    // {
    //     $sadra = Sadra::find($request->sadra);
    //     if ($sadra->photo){
    //         unlink($sadra->photo->path);
    //         $sadra->photo->delete();
    //     }
    //     \EmployeeLogActivity::addToLog('نمایشگاه / رویداد '.$sadra->title.' را حذف کرد');

    //     return response()->json($sadra->delete());
    // }

    public function sadra_report()
    {
        $events = company_sadra::has('receipt')->with('company','sadra')->get()->groupBy(function ($item){
           return $item->sadra->title;
        });
        return view('panel.employee.sadra-reports',compact('events'));
    }

    public function sadra_requests($id)
    {
        $requests = company_sadra::with('sadra','companyInfo','companyInfo.user','booth')
            ->where('sadra_id' , $id)
            ->whereNull('status')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('panel.employee.sadra-reg-requests',compact('requests'));
    }

    public function discounts()
    {
        $discounts = Discount::paginate(20);
        return view('panel.employee.discount-list',compact('discounts'));
    }

    public function create_discount()
    {
        return view('panel.employee.create-discount');
    }

    public function edit_discount(Discount $discount)
    {
        return view('panel.employee.edit-discount',compact('discount'));
    }

    public function store_discount(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'is_percent'=>'required',
            'begin'=>'required',
            'end'=>'required',
            'value'=>'required',
            'description'=>'required',
        ]);

        if (strpos($request->begin, '/') !== false) {
            $startDate = explode('/', $request->begin);

            $month = (int)persian_to_eng_num($startDate[1]);
            $day = (int)persian_to_eng_num($startDate[2]);
            $year = (int)persian_to_eng_num($startDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startDateGre = Carbon::instance(Verta::parse($request->begin)->datetime());
        }
        if (strpos($request->end, '/') !== false) {
            $endDate = explode('/', $request->end);

            $month = (int)persian_to_eng_num($endDate[1]);
            $day = (int)persian_to_eng_num($endDate[2]);
            $year = (int)persian_to_eng_num($endDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endDateGre = Carbon::instance(Verta::parse($request->end)->datetime());
        }
        $request->merge(['begin'=>$startDateGre]);
        $request->merge(['end'=>$endDateGre]);
        Discount::create($request->all());
        \EmployeeLogActivity::addToLog('کد تخفیف '.$request->name.' را ثبت کرد');

        return redirect(route('employee.discount.index'))->with('success',__('public.data_added'));
    }

    public function update_discount(Request $request, Discount $discount)
    {
        if (strpos($request->begin, '/') !== false) {
            $startDate = explode('/', $request->begin);

            $month = (int)persian_to_eng_num($startDate[1]);
            $day = (int)persian_to_eng_num($startDate[2]);
            $year = (int)persian_to_eng_num($startDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startDateGre = Carbon::instance(Verta::parse($request->begin)->datetime());
        }
        if (strpos($request->end, '/') !== false) {
            $endDate = explode('/', $request->end);

            $month = (int)persian_to_eng_num($endDate[1]);
            $day = (int)persian_to_eng_num($endDate[2]);
            $year = (int)persian_to_eng_num($endDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endDateGre = Carbon::instance(Verta::parse($request->end)->datetime());
        }
        $request->merge(['begin'=>$startDateGre]);
        $request->merge(['end'=>$endDateGre]);
        $discount->update($request->all());
        \EmployeeLogActivity::addToLog('کد تخفیف '.$request->name.' را ویرایش کرد');

        return redirect(route('employee.discount.index'))->with('success',__('public.data_added'));
    }

    public function delete_discount(Request $request)
    {
        $discount = Discount::find($request->discount);
        \EmployeeLogActivity::addToLog('کد تخفیف '.$discount->name.' را حذف کرد');

        return response()->json($discount->delete());
    }

    public function buy_plans()
    {
        $plans = BuyPlan::notdeleted()->with('sadra')->paginate(20);
        return view('panel.employee.buy-plans-list',compact('plans'));
    }

    public function buy_plan_create($id)
    {
        return view('panel.employee.create-buy-plan',compact('id'));
    }

    public function buy_plan_edit(BuyPlan $plan)
    {
        return view('panel.employee.edit-buy-plan',compact('plan'));
    }

    public function buy_plan_update(Request $request,BuyPlan $plan)
    {
        $price = persian_to_eng_num($request->price);
        $request->merge(['price'=>$price]);

        $request->validate([
            'name'=>'required',
            'price'=>'required|numeric',
            'dimensions'=>'required|numeric',
            'tables_qty'=>'required|numeric',
            'chairs_qty'=>'required|numeric',
            'map'=>'nullable|image',
        ]);

        $plan->name = $request->name;
        $plan->price = $request->price;
        $plan->dimensions = $request->dimensions;
        $plan->tables_qty = $request->tables_qty;
        $plan->chairs_qty = $request->chairs_qty;
        $plan->save();

        if ($request->file('map')) {
            if ($plan->map){
                unlink($plan->map->path);
                $plan->map->delete();
            }
            $name = time() . $request->map->getClientOriginalName();
            $path = $request->map->storeAs(
                'img/sadra',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $plan->id;
            $photo->groupable_type = BuyPlan::class;
            $photo->save();
            $plan->map_id = $photo->id;
            $plan->save();
        }
        \EmployeeLogActivity::addToLog('پلن'.$plan->name.' را ویرایش کرد');

        return redirect(route('employee.buy-plan.index'))->with('success',__('public.data_added'));

    }

    public function buy_plan_store(Request $request)
    {
        $price = persian_to_eng_num($request->price);
        $request->merge(['price'=>$price]);

        $request->validate([
            'name'=>'required',
            'price'=>'required|numeric',
            'dimensions'=>'required|numeric',
            'tables_qty'=>'required|numeric',
            'chairs_qty'=>'required|numeric',
            'map'=>'nullable|image',
        ]);

        $plan = new BuyPlan();
        $plan->name = $request->name;
        $plan->price = $request->price;
        $plan->dimensions = $request->dimensions;
        $plan->tables_qty = $request->tables_qty;
        $plan->chairs_qty = $request->chairs_qty;
        $plan->sadra_id = $request->sadra_id;
        $plan->save();

        if ($request->file('map')) {
            $name = time() . $request->map->getClientOriginalName();
            $path = $request->map->storeAs(
                'img/sadra',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $plan->id;
            $photo->groupable_type = BuyPlan::class;
            $photo->save();
            $plan->map_id = $photo->id;
            $plan->save();
        }
        \EmployeeLogActivity::addToLog('پلن'.$plan->name.' را ثبت کرد');

        return redirect(route('employee.buy-plan.index'))->with('success',__('public.buy-plan_added'));
    }

    public function buy_plan_destroy(BuyPlan $plan)
    {
        $plan->update(['deleted'=>1]);
        \EmployeeLogActivity::addToLog('پلن'.$plan->name.' را حذف کرد');

        return redirect(route('employee.buy-plan.index'));
    }

    public function sadra_register_requests()
    {
    }

    public function tickets()
    {
        $tickets = Ticket::with('sender')->notDeleted()->OrginalMessage()->paginate(20);
        return view('panel.employee.tickets-list',compact('tickets'));

    }

    public function sendTikcetReply(Request $request)
    {
        $request->validate([
            'content' => 'required'
        ]);

        $ticket = Ticket::findorfail($request->parent_id);
        $ticket->update(['read_at' => now()]);
        $ticket->replies()->create([
            'parent_id' => $request->parent_id,
            'sender_id' => Auth::id(),
            'receiver_id' => $ticket->sender_id,
            'subject' => 'reply',
            'content' => $request->input('content')
        ]);

        return redirect()->route('employee.tickets.list')->with('success', __('public.reply-sent'));
    }

    public function MarkTicketAsRead(Ticket $ticket)
    {
        return $ticket->MarkAsRead();
    }

    public function show_signature_upload_form()
    {
        return view('panel.employee.upload-signature');
    }

    public function upload_signature(Request $request)
    {
        $request->validate([
            'file'=>'required|image'
        ]);

        if ($request->file('file')){
            if (Auth::user()->groupable->university->signature){
                unlink(Auth::user()->groupable->university->signature->path);
                Auth::user()->groupable->university->signature->delete();
            }
            $name =time().$request->file->getClientOriginalName();
            $path = $request->file->storeAs('files/signatures',$name,'public');
            $signature = new Photo();
            $signature->name = $name;
            $signature->path = $path;
            $signature->groupable_type = University::class;
            $signature->groupable_id = Auth::user()->groupable->university_id;
            $signature->save();
            \EmployeeLogActivity::addToLog('امضای دانشگاه را آپلود کرد');

            return redirect(route('emp.panel'))->with('success',__('public.signature_added'));
        }
    }

    public function edit_profile()
    {
        $user = Auth::user();
        return view('panel.employee.edit-profile',compact('user'));
    }

    public function update_profile(Request $request)
    {
        $request->validate([
            'mobile' => 'unique:users,mobile,'. Auth::user()->id,
            ]);

        if (filled($request->password)){
            Auth::user()->password = Hash::make($request->password);
        }
        if ($request->mobile != Auth::user()->mobile){
            Auth::user()->update(['mobile_verified_at'=>null]);
        }
        Auth::user()->name = $request->name;
        Auth::user()->family = $request->family;
        Auth::user()->mobile = $request->mobile;
        Auth::user()->save();
        \EmployeeLogActivity::addToLog('پروفایل خود را ویرایش کرد');


        return redirect(route('employee.panel'))->with('success',__('public.data_added'));
    }

    public function search_pooyeshReqs_by_sender(Request $request)
    {

        if ($request->has('sender')){
            $requests = CooperationRequest::where('type','intern')->with('sender','receiver','process')
                ->whereHas('sender',function ($query) use ($request){
                    $query->where('name','like','%'.$request->sender.'%')
                        ->orwhere('family','like','%'.$request->sender.'%');
                })
                ->paginate(30);
        }elseif ($request->has('receiver')){
            $receivers = User::company()->wherehas('groupable',function ($query) use ($request){
                $query->where('name','like','%'.$request->receiver.'%');
            })->pluck('id');
            $receivers = $receivers->toArray();

            $requests = CooperationRequest::where('type','intern')->with('sender','receiver','process')
                ->whereIn('receiver_id',$receivers)
            ->paginate(30);

        }

        foreach ($requests as $request){
            $request->created_at = verta($request->created_at)->formatDate();
            if ($request->sender->groupable instanceof CompanyInfo){
                $request->sender_type = 'co';
            }elseif ($request->sender->groupable instanceof StudentInfo){
                $request->sender_type = 'std';
            }

            if ($request->receiver->groupable instanceof CompanyInfo){
                $request->receiver_type = 'co';
            }elseif ($request->receiver->groupable instanceof StudentInfo){
                $request->receiver_type = 'std';
            }
        }
        return view('panel.employee.pooyesh-requests',compact('requests'));
    }

    public function graders_list()
    {
        $students = Grader::paginate(30);
        return view('panel.employee.graders',compact('students'));
    }

    public function company_sadra_register_done($id)
    {
        $sadra_id = $id;
        $requests = company_sadra::has('payedPayment')->where('sadra_id' , $id)->with('companyInfo.logo','payedPayment')->paginate(20);
        // dd($requests);
        return view('panel.employee.sadra-reg-done-requests',compact('requests','sadra_id'));

    }

    public function exportDoneCompanySadra($sadra_id)
    {
        \EmployeeLogActivity::addToLog('از لیست درخواست های ثبت نام نمایشگاه خروجی اکسل گرفت');

        return  Excel::download(new DoneCompanySadraExport($sadra_id),'companySadra.xlsx');
    }

    public function exportExhibitionRegisteredCompanies($event_id)
    {
        \EmployeeLogActivity::addToLog('از لیست ثبت نامی های نمایشگاه خروجی اکسل گرفت');

        return Excel::download(new ExhibitionRegisteredCompanies($event_id),'registered-companies.xlsx');
    }

    public function exportExhibitionRegisterRequests()
    {
        \EmployeeLogActivity::addToLog('از لیست درخواست های ثبت نام نمایشگاه خروجی اکسل گرفت');

        return  Excel::download(new ExhibitionRegisterRequests(),'companies.xlsx');
    }

    public function CompanylogActivity()
    {
        $logs = \CompanyLogActivity::paginate(30);
        $model = 'co';
        return view('panel.employee.log-activities',compact(['logs','model']));
    }

    public function StudentlogActivity()
    {
        $logs = \StudentLogActivity::paginate(30);
        $model = 'std';

        return view('panel.employee.log-activities',compact(['logs','model']));
    }

    public function EmployeelogActivity()
    {
        $logs = \EmployeeLogActivity::paginate(30);
        $model = 'emp';

        return view('panel.employee.log-activities',compact(['logs','model']));
    }

    public function exportActivityLog($model)
    {
        \EmployeeLogActivity::addToLog('از لیست گزارش فعالیت کاربران خروجی اکسل گرفت');


        return  Excel::download(new ActivityLog($model),'activity-log.xlsx');
    }

    public function unconfirmedJobPositions()
    {
        $job_positions = OjobPosition::where('status',0)->paginate(30);
        return view('panel.employee.unconfirmed-job-positions',compact('job_positions'));
    }

    public function unconfirmedList()
    {
        $students = User::student()->unconfirmed()->orderbyDesc('updated_at')->paginate(20);
        $applicants = User::applicant()->unconfirmed()->orderbyDesc('updated_at')->paginate(20);
        foreach($applicants as $applicant){
            if($applicant->groupable->birth){
                $sen = Carbon::parse($applicant->groupable->birth);
                $sen = $sen->diffInYears(Carbon::now());
                $applicant->age = $sen;
            }
        }
        $companies = User::company()->unconfirmed()->orderbyDesc('updated_at')->paginate(20);
        return view('panel.employee.unconfirmed-list' ,compact(['companies','students','applicants']));
    }

    public function confirmedList()
    {
        $students = User::student()->Confirmed()->orderbyDesc('id')->paginate(20);
        $applicants = User::applicant()->Confirmed()->orderbyDesc('id')->paginate(20);
        foreach($applicants as $applicant){
            if($applicant->groupable->birth){
                $sen = Carbon::parse($applicant->groupable->birth);
                $sen = $sen->diffInYears(Carbon::now());
                $applicant->age = $sen;
            }
        }
        $companies = User::company()->Confirmed()->orderbyDesc('id')->paginate(20);
        return view('panel.employee.confirmed-list' ,compact(['companies','students','applicants']));
    }

    public function importedList()
    {
        $companies = [];
        $students = [];
        $applicants = [];
        $users = User::Imported()->orderbyDesc('id')->get();
        foreach($users as $user){
            if($user->hasRole('company')){
                array_push($companies,$user);
            }elseif($user->hasRole('student')){
                array_push($students,$user);
            }elseif($user->hasRole('applicant')){
                array_push($applicants,$user);
            }
        }
        $companies = collect($companies);
        $students = collect($students);
        $applicants = collect($applicants);

        $companies = new LengthAwarePaginator(
            $companies->forPage(LengthAwarePaginator::resolveCurrentPage(),20),
            $companies->count(),
            20,
            1,
            ['path' => LengthAwarePaginator::resolveCurrentPath()]
        );
        $students = new LengthAwarePaginator(
            $students->forPage(LengthAwarePaginator::resolveCurrentPage(),20),
            $students->count(),
            20,
            1,
            ['path' => LengthAwarePaginator::resolveCurrentPath()]
        );
        $applicants = new LengthAwarePaginator(
            $applicants->forPage(LengthAwarePaginator::resolveCurrentPage(),20),
            $applicants->count(),
            20,
            1,
            ['path' => LengthAwarePaginator::resolveCurrentPath()]
        );

        return view('panel.employee.imported-list' ,compact(['companies','students','applicants']));
    }

    public function testersList()
    {
        $mbtiUsers = User::unconfirmed()
            ->whereHas('mbti_test')
            ->where('sex', null)
            ->orderbyDesc('id')
            ->with('mbti_test')
            ->paginate(20);
        $enneagramUsers = User::unconfirmed()
            ->whereHas('enneagram_test')
            ->where('sex', null)
            ->orderbyDesc('id')
            ->with('enneagram_test')
            ->paginate(20);

        return view('panel.employee.testers-list' , compact(['mbtiUsers','enneagramUsers']));
    }

    public function personalityTestsResultShow()
    {
        $majors = DB::table('extra_users')->select('major', DB::raw('COUNT(*) as count'))
        ->groupBy('major')
        ->get();
        $majorTypes = [];
        foreach($majors as $major){
            array_push($majorTypes , $major->major);
        }
        $grades = DB::table('extra_users')->select('grade', DB::raw('COUNT(*) as count'))
        ->groupBy('grade')
        ->get();
        $gradeTypes = [];
        foreach($grades as $grade){
            array_push($gradeTypes , $grade->grade);
        }

        // dd($majorTypes);
        $mbti_test_user = User::whereHas('mbti_test')->count();
        $enneagram_test_user = User::whereHas('enneagram_test')->count();
        $neo_test_user = User::whereHas('neo_test')->count();
        $haaland_test_user = User::whereHas('haaland_test')->count();
        $ghq_test_user = User::whereHas('ghq_test')->count();

        $mbtiCount = DB::table('mbti_results')
        ->select('personality_type', DB::raw('COUNT(*) as count'))
        ->groupBy('personality_type')
        ->get();
        $mbtiType = [];
        $mbtiCountOfType = [];
        foreach($mbtiCount as $mbti){
            array_push( $mbtiType , $mbti->personality_type);
            array_push( $mbtiCountOfType , $mbti->count);
        }
        $mbtiType = mbti_personality_to_fa($mbtiType);
        $mbtiType = json_encode($mbtiType);
        $mbtiCountOfType = json_encode($mbtiCountOfType);

        $enneagramCount = DB::table('enneagram_results')->select('main_character' ,DB::raw('COUNT(*) as count'))->whereNotNull('main_character')->groupBy('main_character')->get();
        $enneagramType = [];
        $enneagramCountOfType = [];
        foreach($enneagramCount as $enneagram){
            array_push( $enneagramType , $enneagram->main_character);
            array_push( $enneagramCountOfType , $enneagram->count);
        }
        $enneagramType = enneagram_personality_to_fa($enneagramType);
        $enneagramType = json_encode($enneagramType);
        $enneagramCountOfType = json_encode($enneagramCountOfType);

        $neoAverages = DB::table('neo_results')
            ->selectRaw('AVG(N) as N, AVG(E) as E, AVG(O) as O, AVG(A) as A, AVG(C) as C')
            ->first();

        $neoType = []; $neoAvg = [];
        foreach($neoAverages as $key => $value){
            array_push( $neoType , $key);
            array_push( $neoAvg , $value);
        }
        $neoType = neo_personality_to_fa($neoType);
        $neoType = json_encode($neoType);
        $neoAvg = json_encode($neoAvg);

        $haalandAverages = DB::table('haaland_results')
            ->selectRaw('AVG(R) as R, AVG(I) as I, AVG(A) as A, AVG(S) as S, AVG(E) as E, AVG(C) as C')
            ->first();
        $haalandType = []; $haalandAvg = [];
        foreach($haalandAverages as $key => $value){
            array_push( $haalandType , $key);
            array_push( $haalandAvg , $value);
        }
        $haalandType = haaland_personality_to_fa($haalandType);
        $haalandType = json_encode($haalandType);
        $haalandAvg = json_encode($haalandAvg);

        $ghqAverages = DB::table('ghq_results')
            ->selectRaw('AVG(physical) as physical, AVG(anxiety) as anxiety, AVG(social) as social, AVG(depression) as depression, AVG(sum_of_all) as sum_of_all')
            ->first();
        $ghqType = []; $ghqAvg = [];
        foreach($ghqAverages as $key => $value){
            array_push( $ghqType , $key);
            array_push( $ghqAvg , $value);
        }
        $ghqType = ghq_personality_to_fa($ghqType);
        $ghqType = json_encode($ghqType);
        $ghqAvg = json_encode($ghqAvg);

        return view('panel.employee.personality-test-results' ,
            compact(['majorTypes', 'gradeTypes',
            'mbti_test_user' ,'enneagram_test_user' , 'neo_test_user', 'haaland_test_user', 'ghq_test_user',
            'mbtiType' , 'mbtiCountOfType' ,'enneagramType' ,'enneagramCountOfType' ,'neoType' ,'neoAvg' ,'haalandType' ,
            'haalandAvg' ,'ghqType' ,'ghqAvg'])
            );

    }

    public function showSecretariatContracts()
    {
        $requests = CooperationRequest::whereHas('contract')
        ->with('sender' , 'receiver' , 'process')
        ->whereRelation('process', 'accepted_by_uni_at', '!=' , null)
        ->whereRelation('process' , 'contract_printed_at' , null)
        ->orderByDesc('updated_at')
        ->paginate(20);

        foreach($requests as $request){
            if ($request->sender->groupable instanceof CompanyInfo){
                $request->sender_type = 'co';
            }elseif ($request->sender->groupable instanceof StudentInfo){
                $request->sender_type = 'std';
            }elseif ($request->sender->groupable instanceof ApplicantInfo){
                $request->sender_type = 'apl';
            }

            if ($request->receiver->groupable instanceof CompanyInfo){
                $request->receiver_type = 'co';
            }elseif ($request->receiver->groupable instanceof StudentInfo){
                $request->receiver_type = 'std';
            }elseif ($request->receiver->groupable instanceof ApplicantInfo){
                $request->receiver_type = 'apl';
            }
        }

        $printedRequests = CooperationRequest::whereHas('contract')
        ->with('sender', 'receiver' , 'process')
        ->whereRelation('process' , 'contract_printed_at' , '!=' , null)
        ->orderByDesc('updated_at')
        ->paginate(20);

        foreach($printedRequests as $request){
            if ($request->sender->groupable instanceof CompanyInfo){
                $request->sender_type = 'co';
            }elseif ($request->sender->groupable instanceof StudentInfo){
                $request->sender_type = 'std';
            }elseif ($request->sender->groupable instanceof ApplicantInfo){
                $request->sender_type = 'apl';
            }

            if ($request->receiver->groupable instanceof CompanyInfo){
                $request->receiver_type = 'co';
            }elseif ($request->receiver->groupable instanceof StudentInfo){
                $request->receiver_type = 'std';
            }elseif ($request->receiver->groupable instanceof ApplicantInfo){
                $request->receiver_type = 'apl';
            }
        }
        return view('panel.employee.secretariat-contracts' , compact('requests' , 'printedRequests'));
    }

}
