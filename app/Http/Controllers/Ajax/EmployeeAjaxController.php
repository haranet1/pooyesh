<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Traits\HasRoles;

class EmployeeAjaxController extends Controller
{
    public function assignToCompany(Request $request)
    {
        $user = User::findOrFail($request->id);
        if(user_has_role($user)){
            $user->syncRoles('company');
        }else{
            $user->assignRole('company');
        }

        return true;
    }

    public function assignToStudent(Request $request)
    {
        $user = User::findOrFail($request->id);
        if(user_has_role($user)){
            $user->syncRoles('student');
        }else{
            $user->assignRole('student');
        }

        return true;
    }

    public function assignToApplicant(Request $request)
    {
        $user = User::findOrFail($request->id);
        if(user_has_role($user)){
            $user->syncRoles('applicant');
        }else{
            $user->assignRole('applicant');
        }

        return true;
    }

    public function unConfirmedCompanySearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->company();
        $query = $query->unconfirmed();
        $query = $query->with('companyInfos');
        if($req['name'] != '*'){
            $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        // if($req['province'] != '*'){
        //     $query->whereRelation('companyInfos','name', 'LIKE', '%'.$req['company'].'%');
        // }
        if($req['coname'] != '*'){
            $query->whereRelation('companyInfos','name', 'LIKE', '%'.$req['coname'].'%');
        }
        if($req['workfield'] != '*'){
            $query->whereRelation('companyInfos','activity_field', 'LIKE', '%'.$req['workfield'].'%');
        }

        $result = $query->get();
        return response()->json($result);
    }

    public function unConfirmedApplicantSearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->Applicant();
        $query = $query->unconfirmed();
        $query = $query->with('applicantInfos','applicantInfos.province','academicInfos');
        
        if($req['name'] != '*'){
            $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        if($req['sex'] != '*'){
            $query->where('sex', 'LIKE', '%'.change_sex_fa_to_en($req['sex']).'%');
        }
        if($req['province'] != '*'){
            $query->whereRelation('applicantInfos','province_id', 'LIKE', '%'.$req['province'].'%');
        }
        if($req['major'] != '*'){
            $query->whereRelation('academicInfos','major', 'LIKE', '%'.$req['major'].'%');
        }
        
        $results = $query->get();
        foreach($results as $result){
            if($result->applicantInfos->birth){
                $sen = Carbon::parse($result->applicantInfos->birth);
                $sen = $sen->diffInYears(Carbon::now());
                $result->age = $sen;
            }
        }
        return response()->json($results);
    }

    public function unConfirmedStudentSearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->Student();
        $query = $query->unconfirmed();
        $query = $query->with('studentInfos','studentInfos.province','studentInfos.university');
        
        if($req['name'] != '*'){
            $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        if($req['sex'] != '*'){
            $query->where('sex', 'LIKE', '%'.change_sex_fa_to_en($req['sex']).'%');
        }
        if($req['province'] != '*'){
            $query->whereRelation('studentInfos','province_id', 'LIKE', '%'.$req['province'].'%');
        }
        if($req['university'] != '*'){
            $query->whereRelation('studentInfos.university','name', 'LIKE', '%'.$req['university'].'%');
        }
        if($req['major'] != '*'){
            $query->whereRelation('studentInfos','major', 'LIKE', '%'.$req['major'].'%');
        }
        $results = $query->get();
        // foreach($results as $row){
        //     dd($row->studentInfos->university->name);
        // }
        return response()->json($results);
    }
    
    public function confirmedCompanySearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->company();
        $query = $query->Confirmed();
        $query = $query->with('companyInfos');
        if($req['name'] != '*'){
            $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        // if($req['province'] != '*'){
        //     $query->whereRelation('companyInfos','name', 'LIKE', '%'.$req['company'].'%');
        // }
        if($req['coname'] != '*'){
            $query->whereRelation('companyInfos','name', 'LIKE', '%'.$req['coname'].'%');
        }
        if($req['workfield'] != '*'){
            $query->whereRelation('companyInfos','activity_field', 'LIKE', '%'.$req['workfield'].'%');
        }

        $result = $query->get();
        return response()->json($result);
    }

    public function confirmedApplicantSearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->Applicant();
        $query = $query->Confirmed();
        $query = $query->with('applicantInfos','applicantInfos.province','academicInfos');
        
        if($req['name'] != '*'){
            $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        if($req['sex'] != '*'){
            $query->where('sex', 'LIKE', '%'.change_sex_fa_to_en($req['sex']).'%');
        }
        if($req['province'] != '*'){
            $query->whereRelation('applicantInfos','province_id', 'LIKE', '%'.$req['province'].'%');
        }
        if($req['major'] != '*'){
            $query->whereRelation('academicInfos','major', 'LIKE', '%'.$req['major'].'%');
        }
        
        $results = $query->get();
        foreach($results as $result){
            if($result->applicantInfos->birth){
                $sen = Carbon::parse($result->applicantInfos->birth);
                $sen = $sen->diffInYears(Carbon::now());
                $result->age = $sen;
            }
        }
        return response()->json($results);
    }

    public function confirmedStudentSearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->Student();
        $query = $query->Confirmed();
        $query = $query->with('studentInfos','studentInfos.province','studentInfos.university');
        
        if($req['name'] != '*'){
            $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        if($req['sex'] != '*'){
            $query->where('sex', 'LIKE', '%'.change_sex_fa_to_en($req['sex']).'%');
        }
        if($req['province'] != '*'){
            $query->whereRelation('studentInfos','province_id', 'LIKE', '%'.$req['province'].'%');
        }
        if($req['university'] != '*'){
            $query->whereRelation('studentInfos.university','name', 'LIKE', '%'.$req['university'].'%');
        }
        if($req['major'] != '*'){
            $query->whereRelation('studentInfos','major', 'LIKE', '%'.$req['major'].'%');
        }
        $results = $query->get();
        // foreach($results as $row){
        //     dd($row->studentInfos->university->name);
        // }
        return response()->json($results);
    }

    public function importedCompanySearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->Imported();

        if($req['name'] != '*'){
            $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        // // if($req['province'] != '*'){
        // //     $query->whereRelation('companyInfos','name', 'LIKE', '%'.$req['company'].'%');
        // // }
        // if($req['coname'] != '*'){
        //     $query->whereRelation('companyInfos','name', 'LIKE', '%'.$req['coname'].'%');
        // }
        // if($req['workfield'] != '*'){
        //     $query->whereRelation('companyInfos','activity_field', 'LIKE', '%'.$req['workfield'].'%');
        // }
        $results = $query->get();
        $companies = [];
        foreach($results as $result){
            if($result->hasRole('company')){
                array_push($companies,$result);
            }
        }
        $companies = collect($companies);
        $companies = new LengthAwarePaginator(
            $companies->forPage(LengthAwarePaginator::resolveCurrentPage(),20),
            $companies->count(),
            20,
            1,
            ['path' => LengthAwarePaginator::resolveCurrentPath()]
        );
        return response()->json($companies);
    }

    public function importedApplicantSearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->Imported();

        if($req['name'] != '*'){
            $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        $results = $query->get();
        $applicants = [];
        foreach($results as $result){
            if($result->hasRole('applicant')){
                array_push($applicants,$result);
            }
        }
        $applicants = collect($applicants);
        $applicants = new LengthAwarePaginator(
            $applicants->forPage(LengthAwarePaginator::resolveCurrentPage(),20),
            $applicants->count(),
            20,
            1,
            ['path' => LengthAwarePaginator::resolveCurrentPath()]
        );
        return response()->json($applicants);
    }

    public function importedStudentSearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->Imported();

        if($req['name'] != '*'){
            $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        $results = $query->get();
        $students = [];
        foreach($results as $result){
            if($result->hasRole('student')){
                array_push($students,$result);
            }
        }
        $students = collect($students);
        $students = new LengthAwarePaginator(
            $students->forPage(LengthAwarePaginator::resolveCurrentPage(),20),
            $students->count(),
            20,
            1,
            ['path' => LengthAwarePaginator::resolveCurrentPath()]
        );
        return response()->json($students);

    }

    public function testerMbtiSearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->unconfirmed();
        $query = $query->whereHas('mbti_test');
        $query = $query->with('mbti_test');
        
        if($req['name'] != '*'){
            $query = $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query = $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query = $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        if($req['sex'] != '*'){
            $query = $query->where('sex', 'LIKE', '%'.$req['sex'].'%');
        }
        if ($req['role'] != '*') {
            $query = $query->role($req['role']);
        }
        $results = $query->get();
        foreach($results as $row){
            $row->role = user_has_role_fa($row);
        }
        return response()->json($results);
    }

    public function testerEnnagramSearch(Request $request)
    {
        $req = $request->obj;
        $query = User::query();
        $query = $query->unconfirmed();
        $query = $query->whereHas('enneagram_test');
        $query = $query->with('enneagram_test');
        
        if($req['name'] != '*'){
            $query = $query->where('name', 'LIKE', '%'.$req['name'].'%');
        }
        if($req['family'] != '*'){
            $query = $query->where('family', 'LIKE', '%'.$req['family'].'%');
        }
        if($req['mobile'] != '*'){
            $query = $query->where('mobile', 'LIKE', '%'.$req['mobile'].'%');
        }
        if($req['sex'] != '*'){
            $query = $query->where('sex', 'LIKE', '%'.$req['sex'].'%');
        }
        if ($req['role'] != '*') {
            $query = $query->role($req['role']);
        }
        $results = $query->get();
        foreach($results as $result){
            $result->main_character = enneagram_result_fa($result->enneagram_test->main_character);
        }
        return response()->json($results);
    }

    public function deleteTicket(Request $request) // delete a ticket from students tickets
    {
        
    }

}
