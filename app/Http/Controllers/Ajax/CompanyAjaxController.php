<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\BuyPlan;
use App\Models\CompanyTicket;
use App\Models\SadraBooth;
use Illuminate\Http\Request;
use App\Models\CompanyLogActivity;
class CompanyAjaxController extends Controller
{
    public function getBoothByPlanId(Request $request)
    {  

        $booths = SadraBooth::where('plan_id', $request->id)->where('reserved', false)->get();
        $plan = BuyPlan::with('map')->find($request->id);
        $map = null;
        if($plan->map){
            $map = $plan->map->path;
        }
        if(count($booths) > 0){
            return response()->json([
                'booths' => $booths,
                'map' => $map,
                'price' => $plan->price
            ], 200);
        }else return response()->json([false,]);
    }

    public function checkBoothReserved(Request $request)
    {
        $booth = SadraBooth::find($request->boothId);
        if($booth->reserved){
            return response()->json(false);
        }else{
            return response()->json(true);
        }
    }

    public function deleteTicket(Request $request)
    {
        $ticket = CompanyTicket::find($request->id);
        \CompanyLogActivity::addToLog('یک تیکت پویش را حذف کرد');
        return response()->json($ticket->update(['deleted' => 1]));
    }

}
