<?php

namespace App\Http\Controllers\Ajax;

use Carbon\Carbon;
use App\Models\User;
use App\Models\ResumeSent;
use App\Models\CompanyInfo;
use App\Models\OjobPosition;
use Illuminate\Http\Request;
use App\Models\ApplicantInfo;
use App\Models\CompanyTicket;
use App\Models\CooperationRequest;
use App\Notifications\SentRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\PooyeshRequestProcess;
use Illuminate\Database\Query\Builder;
use App\Notifications\CoAcceptedResume;
use Illuminate\Notifications\Notifiable;

class FrontAjaxController extends Controller
{
    use Notifiable;

    public function getLatestJobPositions(Request $request)
    {
        $jobs = OjobPosition::Confirmed()->with('job','job.category','company.groupable','province','city')->take($request->count)->orderByDesc('id')->get();
        if(count($jobs) > 0){
            foreach($jobs as $job){
                $job->created_at_fa = verta($job->created_at)->formatDifference();
                $job->type = job_type_persian($job->type);
            }
        }
        return response()->json($jobs);
    }

    public function cooperationRequest(Request $request)
    {
        $job = OjobPosition::findorfail($request->job_id);
        if(Auth::user()->status == 2){
            return redirect()->back()->with('error', 'شما مجاز به ارسال رزومه نیستید');
        }
        if (Auth::user()->groupable_type == CompanyInfo::class || ($job->type == 'intern' && Auth::user()->groupable_type == ApplicantInfo::class)){
            return redirect()->back()->with('error', 'امکان ارسال رزومه برای موقعیت کارآموزی را ندارید');
        }

        if($job->type == 'intern'){
            $pooyesh_req = CooperationRequest::where('status',1)
                ->where('type','intern')
                ->where(function ($query) {
                    $query->where('receiver_id',Auth::id())
                        ->orWhere('sender_id',Auth::id());
            })->latest('updated_at')
            ->first();

            if(!is_null($pooyesh_req)){
                $threeMonthsAgo = Carbon::now()->subMonths(3);
                if($pooyesh_req->updated_at->greaterThan($threeMonthsAgo)){
                    return redirect()->back()->with('error', 'در بازه ی طرح پویش نمیتوانید رزومه برای کارآموزی ارسال کنید');
                }
            }
        }




        $req = CooperationRequest::create([
            'receiver_id'=>$job->company_id,
            'sender_id'=> Auth::id(),
            'type'=>$job->type,
            'job_position_id' => $job->id
        ]);

        $receiver = User::find($job->company_id);

        if ($job->type == 'intern'){
            PooyeshRequestProcess::create([
                'req_id'=>$req->id
            ]);
        }

        $receiver->notify(new SentRequest(Auth::user(), $job));

        return redirect()->back();
    }

    public function companyCooperationRequest(Request $request)
    {
        $job = OjobPosition::find($request->job);

        $company = Auth::user();
        if(Auth::check() && Auth::user()->groupable_type == CompanyInfo::class){
            if($job->type == 'intern'){
                $sent_requests_intern = CooperationRequest::where('sender_id', Auth::id())->where('type' , 'intern')->count();
                if($sent_requests_intern >= $company->groupable->request_internship_limit){
                    return response()->json(['errorText' => 'کارفرمای گرامی، تعداد مجاز درخواست شما به کارآموز به پایان رسیده است.'], 403);
                }
            }

            if($job->type != 'intern'){
                $sent_requests_hire = CooperationRequest::where('sender_id', Auth::id())->where('type', '!=', 'intern')->count();
                if($sent_requests_hire >= $company->groupable->request_hire_limit){
                    return response()->json(['errorText' => 'کارفرمای گرامی، تعداد مجاز درخواست شما به کارجو به پایان رسیده است.'], 403);
                }
            }
        }

        $req = CooperationRequest::create([
            'receiver_id' => $request->receiver,
            'sender_id' => Auth::id(),
            'type' => $job->type,
            'job_position_id' => $job->id,
        ]);

        if ($job->type == 'intern'){
            $req->update([
                'salary' => $request->proposed_salary,
            ]);

            PooyeshRequestProcess::create([
                'req_id'=>$req->id
            ]);
        }

        $receiver = User::find($request->receiver);

        $receiver->notify(new SentRequest(Auth::user() , $job));

        return response()->json('success',200);
    }

    public function sendResume(Request $request)
    {
        // dd($request->all());
        $resume = ResumeSent::where('receiver_id' , $request->receiver)->where('sender_id' , $request->sender)->first();

        if($resume){
            return response()->json(['status' => false]);
        }

        ResumeSent::create([
            'receiver_id' => $request->receiver,
            'sender_id' => $request->sender,
        ]);

        return response()->json(['status' => true]);
    }

    public function acceptResume(Request $request)
    {
        $resume = ResumeSent::where('id' , $request->id)->with('sender','receiver.groupable')->first();
        $resume->status = true;
        $resume->save();
        $receiver = User::where('id' , $resume->sender->id)->first();
        $receiver->notify(new CoAcceptedResume($resume->receiver->groupable->name));
        // dd($receiver);
        return response()->json('success');
    }

    public function rejectResume(Request $request)
    {

    }

    public function storeCompanyTicket(Request $request)
    {
        CompanyTicket::create([
            'sender_id'=>Auth::id(),
            'receiver_id'=>$request->owner,
            'sender_email'=>$request->email,
            'subject'=>$request->subject,
            'content'=>$request->message
        ]);
        return response()->json('success');
    }

    public function getCities(Request $request)
    {
        $cities = DB::table('cities')->where('province', $request->province)->get(['name', 'id']);

        return response()->json($cities);
    }
}
