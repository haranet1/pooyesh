<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ResumeSent;
use Illuminate\Http\Request;

class ResumeController extends Controller
{
    public function index()
    {
        $requests = ResumeSent::with('sender.groupable','receiver.groupable')
        ->orderByDesc('updated_at')
        ->paginate(21);

        return view('admin.resume.index' , compact('requests'));
    }
}
