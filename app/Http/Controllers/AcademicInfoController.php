<?php

namespace App\Http\Controllers;

use App\Models\AcademicInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AcademicInfoController extends Controller
{
    public function store(Request $request){

        if($request->has('data')){
            for($i=0;$i<count($request->data);$i++){
                $info = AcademicInfo::create([
                    'user_id' => Auth::id(),
                    'grade' => $request->data[$i]['grade'],
                    'major' => $request->data[$i]['major'],
                    'university' => $request->data[$i]['university'],
                    'avg' => $request->data[$i]['avg'],
                    'end_date' => $request->data[$i]['end']
                ]);
            }
            
        }
        return response()->json(true);
    }
}
