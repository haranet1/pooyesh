<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Sadra\SadraController;
use App\Models\company_sadra;
use App\Models\CompanyInfo;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class PaymentController extends Controller
{
    public function sadraPrepay($id)
    {
        $requests = company_sadra::where('id',$id)
            ->where('company_id', Auth::user()->groupable_id)
            ->with('booth', 'sadra')
            ->first();

        $SadraController = new SadraController();
        $requests = $SadraController->checkCompanyPayments($requests);

        if(! is_null($requests->status) ){
            return redirect()->back()->with('success' , 'پرداخت شما انجام شده است.');
        }
        
        $price = (float)$requests->booth->plan->price;
        $response= Http::withOptions([
            'verify' => false
        ])
            ->post('https://api.iaun.ac.ir/api/login',[
                'username' => 'apifanyaruser',
                'password' => 'AapiUser@FanYar',
                'usertype' => 3,

        ]);

        $dataArray =$response->json();
        $token = $dataArray['data']['token'];

        $response = Http::withOptions([
            'verify' => false
        ])
        ->withHeaders([
            'accept' => 'application/json',
        ])->withToken($token)
            ->accept('application/json')
            ->post( 'https://api.iaun.ac.ir/api/payments/authorizedpayer' , [
                'uid' => ''.Auth::id(),
                'paymentkind' => 50,
                'name' => Auth::user()->name,
                'lastname' => Auth::user()->family,
                'mobile' => Auth::user()->mobile
            ]);
        $dataArray = $response->json();

        $payment = Payment::create([
            'user_id' => Auth::id(),
            'order_id' => $id,
            'order_type' => company_sadra::class,
            'price' => $price,
            'gateway' => 'iaun-getway',
        ]);

        if($dataArray['success']== true){
            $response = Http::withOptions([
                'verify' => false
            ])
            ->withHeaders([
                'accept' => 'application/json',
            ])->withToken($token)
                ->accept('application/json')
                ->post('https://api.iaun.ac.ir/api/payments/startpayment', [
                    'Accept' => 'application/json',
                    'uid' => ''.Auth::id(),
                    'amount' => $price * 10,
                    'appid' => 4,    //  3 = Code Application: RahiNoo
                    'kinduser' => 3,
                    'paymentkind' => 50,
                    'paymenttitle' => 'اجاره غرفه نمایشگاه صدرا',
                    'returnurl' => route('company.sadra.return-payment-show',$payment->id),
                ]);
            $dataArray = $response->json();

            if ($dataArray['success'] == true) {
                $payment->update([
                    'payment_key' => $dataArray['data']['payment_key'],
                ]);
                return redirect('https://api.iaun.ac.ir/payment-gateway/'.$dataArray['data']['payment_key']);
            }
            dd($dataArray) ;
        }

    }

    public function sadraReturnPaymentShow($id)
    {
        $payment = Payment::where('id' , $id)->with('order')->first();
        $company_sadra = company_sadra::with('booth')->find($payment->order->id);

        $response = Http::withOptions([
            'verify' => false
        ])
        ->post('https://api.iaun.ac.ir/api/login',[
            'username' => 'apifanyaruser',
            'password' => 'AapiUser@FanYar',
            'usertype' => 3,
        ]);

        $dataArray = $response->json();
        $token = $dataArray['data']['token'];

        $response = Http::withOptions([
            'verify' => false
        ])
        ->withHeaders([
            'accept' => 'application/json',
        ])->withToken($token)
            ->accept('application/json')
            ->post('https://api.iaun.ac.ir/api/payments/verify',[
                'Accept' => 'application/json',
                'payment_key' => $payment->payment_key,
            ]);

        if(isset($response['success']) && $response['success'] == true){
            if($response['data']['pay_status'] == "2"){
                $payment->update([
                    'status' => 'عملیات پرداخت الکترونیک با موفقیت انجام شده است.',
                    'tracking_code' => $response['data']['pay_tracenumber'],
                    'receipt' => $response['data']['pay_rrn'],
                    'card_number' => $response['data']['pay_cardnumber'],
                    'payer_bank' => $response['data']['pay_issuerbank'],
                    'payment_date_time' => $response['data']['pay_paymentdatetime'],
                ]);
                $company_sadra->update([
                    'status' => true,
                    'receipt_id' => $payment->id,
                ]);

                $this->registerCompanyToSadraSite($company_sadra , $payment);

                return redirect()->route('company.sadra.payment.result-show',['id' => $payment]);
                
            }elseif($response['data']['pay_status'] == "3"){
                $payment->update([
                    'status' => 'عملیات پرداخت الکترونیک با مشکل روبرو شده است.'
                ]);
                return redirect()->route('registred.company.sadra')->with('error', $payment->status);
            }elseif($response['data']['pay_status'] == "4"){
                $payment->update([
                    'status' => 'تراکنش تکراری است.',
                ]);
                return redirect()->route('registred.company.sadra')->with('error', $payment->status);
            }elseif($response['data']['pay_status'] == "5"){
                $payment->update([
                    'status' => 'تراکنش توسط کاربر لغو شده است.',
                ]);
                return redirect()->route('registred.company.sadra')->with('error', $payment->status);
            }
        }
        dd($response->json());
    }

    public function sadraPaymentResultShow($id)
    {
        $payment = Payment::where('id', $id)->firstOrFail();
        return view('panel.company.sadra-payment-result', compact('payment'));
    }

    protected function registerCompanyToSadraSite($jobistCompanySadra , $payment)
    {
        $user = User::with('groupable.logo')->find(Auth::id());

        $company = $user->groupable;

        $path = public_path($company->logo->path);
        $format = pathinfo($path, PATHINFO_EXTENSION);

        $booth_name = $jobistCompanySadra->booth->name;

        $price = (float)$jobistCompanySadra->booth->plan->price;

        $province_id = $company->province_id;
        $city_id = $company->city_id;

        if(is_null($company->province_id)){

            $province_id = 4;
            $city_id = 45;

        }

        $response = Http::acceptJson()
            ->retry(5, 150)
            ->attach('co_logo', file_get_contents($path), $company->name . 'company' . '.'. $format)
            ->post('https://sadra.iau.ir/api/v1/store_company_full_process' , [
                'user_name' => $user->name,
                'user_family' => $user->family,
                'user_sex' => $user->sex,
                'user_n_code' => $user->groupable->n_code,
                'user_birth' => now()->subYears(rand(20, 50)),
                'user_mobile' => $user->mobile,
                'user_email' => $user->email,
                'user_role' => 'company',
                'user_password' => $user->password,

                'co_name' => $company->name,
                'co_n_code' => $company->n_code,
                'co_registration_number' => $company->registration_number,
                'co_phone' => $company->phone,
                'co_activity_field' => $company->activity_field,
                'co_year' => Carbon::now(),
                'co_ceo' => $company->ceo,
                'co_email' => $user->email,
                'co_about' => $company->about,
                'co_province_id' => $province_id,
                'co_city_id' => $city_id,

                'booth_name' => $booth_name,

                'price' => $price,
                'status' => $payment->status,
                'tracking_code' => $payment->tracking_code,
                'receipt' => $payment->receipt,
                'card_number' => $payment->card_number,
                'payer_bank' => $payment->payer_bank,
                'payment_date_time' => $payment->payment_date_time
            ]);

        $res_json = $response->serverError();

    }
}
