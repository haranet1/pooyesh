<?php

namespace App\Http\Controllers\Pooyesh;

use App\Helpers\EmployeeLogActivity;
use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\PooyeshContract;
use App\Models\PooyeshRequestProcess;
use App\Models\User;
use App\Notifications\UniAcceptedPooyeshRequest;
use Illuminate\Http\Request;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use niklasravnsborg\LaravelPdf\PdfWrapper;

class PooyeshController extends Controller
{
    
    public function showNotPrintedContracts()
    {
        $requests = CooperationRequest::NotPrintedContract()
            ->with('sender.groupable', 'receiver.groupable' , 'process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'لیست قراردادهای کارآموزی قابل چاپ',
        ];

        return view('admin.secret.contracts' , compact(
            'requests',
            'config'
        ));
    }

    public function showPrintedContracts()
    {
        $requests = CooperationRequest::PrintedContract()
            ->with('sender.groupable', 'receiver.groupable' , 'process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'لیست قراردادهای کارآموزی چاپ شده',
        ];

        return view('admin.secret.contracts' , compact(
            'requests',
            'config'
        ));
    }

    public function waitForReviewAmozeshyarExpert()
    {
        $requests = CooperationRequest::CourseUnitNotRegistration()
            ->with('sender.groupable', 'receiver.groupable' , 'process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'لیست دانشجویان در انتظار اخذ واحد درسی پویش'
        ];

        return view('admin.connections.wait-for-review' , compact(
            'requests',
            'config'
        ));
    }

    public function courseUnitRegisteredList()
    {
        $requests = CooperationRequest::Intern()
            ->CourseUnitRegistration()
            ->with('sender.groupable', 'receiver.groupable' , 'process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'لیست نمره دانشجویان طرح پویش'
        ];

        return view('admin.connections.course-unit-registered' , compact(
            'requests',
            'config'
        ));
    }

    public function registerCourseUnit(Request $req)
    {
        $request = CooperationRequest::with('process')->find($req->req_id);

        $request->process->update([
            'course_unit_registration_at' => now()
        ]);

        return redirect()->back()->with('success' , 'شما واحد درسی پویش را برای دانشجو مورد نظر ثبت کردید.');
    }

    public function scoreRegistration(Request $req)
    {
        $request = CooperationRequest::with('process')->find($req->req_id);

        if(is_null($request->process->score)){
            return redirect()->back()->with('error' , 'برای دانشجو مورد نظر نمره ای ثبت نشده است.');
        }

        $request->process->update([
            'score_registration_at' => now()
        ]);

        return redirect()->back()->with('success' , 'شما واحد درسی پویش را برای دانشجو مورد نظر ثبت کردید.');
    }
}
