<?php

namespace App\Http\Controllers\Pooyesh;

use App\Helpers\CompanyLogActivity;
use App\Helpers\EmployeeLogActivity;
use App\Http\Controllers\Controller;
use App\Models\ApplicantInfo;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\OjobPosition;
use App\Models\PooyeshContract;
use App\Models\PooyeshRequestProcess;
use App\Models\StudentInfo;
use App\Models\User;
use App\Notifications\AcceptedPooyeshRequest;
use App\Notifications\AcceptInternRequest;
use App\Notifications\CoCanceledPooyeshRequest;
use App\Notifications\CoRejectedPooyeshRequest;
use App\Notifications\NewPooyeshRequest;
use App\Notifications\RejectInternRequest;
use App\Notifications\UniAcceptedPooyeshRequest;
use App\Notifications\UniAcceptInternRequest;
use App\Services\Number2Word\Number2Word;
use Carbon\Carbon;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\View\View;

class RequestsController extends Controller
{
    public function hire(): View
    {
        $requests = CooperationRequest::Hire()
        ->with('sender','receiver', 'job')
        ->orderByDesc('updated_at')
        ->paginate(20);

        foreach ($requests as $request){
            if ($request->sender->groupable instanceof CompanyInfo){
                $request->sender_type = 'co';
            }elseif ($request->sender->groupable instanceof StudentInfo){
                $request->sender_type = 'std';
            }elseif ($request->sender->groupable instanceof ApplicantInfo){
                $request->sender_type = 'app';
            }

            if ($request->receiver->groupable instanceof CompanyInfo){
                $request->receiver_type = 'co';
            }elseif ($request->receiver->groupable instanceof StudentInfo){
                $request->receiver_type = 'std';
            }elseif ($request->receiver->groupable instanceof ApplicantInfo){
                $request->receiver_type = 'app';
            }
        }

        return view('admin.pooyesh.requests.hire' ,compact('requests'));
    }

    /**
     * employee options
     */
    public function confirmByUni(Request $request)
    {
        $request = CooperationRequest::find($request->id);
        $request->process->accepted_by_uni_at = now();
        $request->process->save();

        // $user = User::Student()->with('groupable')->find($request->sender_id);
        // $company = User::company()->with('groupable')->find($request->receiver_id);

        // $time = verta()->year;
        // $path = public_path().'\\uploads\\pooyesh-contracts\\';
        // if(!File::exists($path.$time)){
        //     File::makeDirectory($path.$time);
        // }
        

        $todayContracts = PooyeshContract::whereBetween('created_at' , [Carbon::now()->startOfDay() , Carbon::now()->endOfDay()])->count();
        
        $todayContracts++;
        $file_name = verta()->now()->year.'/'.'پ'.'/'.verta()->now()->month.verta()->now()->day.$todayContracts;
        

        // $file_path = $path . $time .'\\'. $file_name . '.pdf';

        // $pdf = Pdf::loadView('contract-pattern', ['company'=>$company,'user'=>$user],[],[
        //     'mode' => 'utf-8'
        // ])->save($file_path);

        $contract = PooyeshContract::create([
            'name' => $file_name,
            'has_path' => false,
            'path' => 'null',
        ]);

        $request->contract_id = $contract->id;
        $request->sender->notify(new UniAcceptInternRequest($request,$request->sender,$request->job));
        $request->receiver->notify(new UniAcceptInternRequest($request,$request->sender,$request->job));
        EmployeeLogActivity::addToLog('درخواست پویش با شناسه'.$request->id.'را تایید کرد ');

        return response()
            ->json( $request->save());
    }

    public function printContract(Request $request)
    {
        $request = CooperationRequest::find($request->id);
        $request->process->contract_printed_at = now();
        return response()->json($request->process->save());
    }

    public function showContract($requestId)
    {
        
        $req = CooperationRequest::with('job' , 'contract')->find($requestId);
        
        if($req->sender->groupable_type == StudentInfo::class){
            $student = User::Student()->with('groupable')->find($req->sender_id);
            $company = User::company()->with('groupable')->find($req->receiver_id);
        }else{
            $student = User::Student()->with('groupable')->find($req->receiver_id);
            $company = User::company()->with('groupable')->find($req->sender_id);
        }

        $contractName = $req->contract->name;

        $salary = array();

        if($req->salary){
            $number2word = new Number2Word;
        
            $salary = [
                'num' =>  $req->salary.'0',
                'word' => $number2word->numberToWords($req->salary.'0'),
            ];
        }else{
            $salary = [
                'num' =>  '0',
                'word' => 'صفر',
            ];
        }

        return view('intern-contract-pattern' , compact(
            'contractName',
            'company',
            'student',
            'salary'
        ));
    }

}
