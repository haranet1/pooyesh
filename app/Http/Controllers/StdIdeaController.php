<?php

namespace App\Http\Controllers;

use App\Events\IdeaCreated;
use App\Http\Requests\CompanyIdeaStoreRequest;
use App\Models\Idea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StdIdeaController extends Controller
{
    public function index()
    {
        $ideas = Idea::where('owner_id',Auth::id())->paginate(30);
        return view('panel.student.ideas-list',compact('ideas'));
    }

    public function create()
    {
        return view('panel.student.create-idea');
    }

    public function edit($id)
    {
        $idea = Idea::findorfail($id);
        return view('panel.student.edit-idea',compact('idea'));
    }

    public function store(CompanyIdeaStoreRequest $request)
    {
        Auth::user()->ideas()->create($request->all());

        event(new IdeaCreated($request->title));
        \StudentLogActivity::addToLog('یک ایده/استارتاپ جدید ثبت کرد');

        return redirect(route('std.idea.index'))->with('success',__('public.data_added'));
    }

    public function update(Request $request, Idea $idea)
    {
        $budget = persian_to_eng_num($request->budget);
        $request->merge(['budget'=>$budget]);

        $request->validate([
            'budget' =>'int'
        ]);

        $idea->update($request->all());

        \StudentLogActivity::addToLog(' ایده/استارتاپ '.$idea->title.' را  ویرایش کرد');

        return redirect(route('std.idea.index'))->with('success',__('public.data_added'));
    }
}
