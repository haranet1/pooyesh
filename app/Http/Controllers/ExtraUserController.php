<?php

namespace App\Http\Controllers;

use App\Exports\UnregisteredStudentsExport;
use App\Imports\ExtraUserImport;
use App\Models\ExtraUser;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExtraUserController extends Controller
{

    public function importExcelExtraUserShow()
    {
        return view('panel.employee.import-extra-user');
    }

    public function importExcelExtraUserStore(Request $request)
    {
        Excel::import(new ExtraUserImport(),\request()->file('users'));
        return back()->with('success', 'کاربران خارجی با موفقیت ثبت شد');
    }

    public function updateNcodeUsers()
    {
        $extraUsers = ExtraUser::with('userHasMobie.studentInfos')->get();
        foreach($extraUsers as $extra){
            if($extra->userHasMobie != null){
                if($extra->userHasMobie->n_code == null){
                    $extra->userHasMobie->update([
                        'n_code' => $extra->n_code,
                    ]);
                    $extra->save();
                }
            }
        }
        return back()->with('success', 'کد ملی دانشجویان با موفقیت بروزرسانی شد');
    }

    public function updateMajorUsers()
    {
        $extraUsers = ExtraUser::with('userHasNcode.studentInfos')->get();
        foreach($extraUsers as $extra){
            if($extra->userHasNcode != null){
                if($extra->userHasNcode->studentInfos){
                    $extra->userHasNcode->studentInfos->update([
                        'major' => $extra->major,
                    ]);
                    $extra->save();
                }
            }
        }
        return back()->with('success', 'رشته تحصیلی دانشجویان با موفقیت بروزرسانی شد');
    }

    public function updateGradeUsers()
    {
        $extraUsers = ExtraUser::with('userHasNcode.studentInfos')->get();
        foreach($extraUsers as $extra){
            if($extra->userHasNcode != null){
                if($extra->userHasNcode->studentInfos){
                    $extra->userHasNcode->studentInfos->update([
                        'grade' => $extra->grade,
                    ]);
                    $extra->save();
                }
            }
        }
        return back()->with('success', 'مقطع تحصیلی دانشجویان با موفقیت بروزرسانی شد');
    }

    public function exportUnregisteredStudents()
    {
        return Excel::download(new UnregisteredStudentsExport, 'unregistered-student.xlsx');
    }
}
