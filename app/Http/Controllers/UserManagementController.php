<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Setting;
use App\Models\Newsletter;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use App\Helpers\EmployeeLogActivity;
use App\Notifications\UserConfirmed;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use App\Services\Notification\Notification;

class UserManagementController extends Controller
{
    public function index()
    {
        $title = 'مدیریت کاربران';

        $companies = User::Company()->count();
        $students = User::Student()->count();
        $applicants = User::Applicant()->count();
        $all = User::count();

        return view('user-management.index' , compact(
            'title',
            'companies',
            'students',
            'applicants',
            'all',
        ));
    }

    public function all()
    {
        $title = 'لیست تمام کاربران';

        $users = User::orderByDesc('created_at')
            ->paginate(20);
        // dd($users);

        return view('user-management.all', compact(
            'title',
            'users'
        ));
    }

    public function unCompany()
    {
        $title = 'لیست شرکت های در انتظار بررسی';
        $companies = User::UnConfirmedCompany()
            ->with('groupable.logo')
            ->orderbyDesc('updated_at')
            ->paginate(20);

        return view('user-management.uncompany-list' , compact('companies' , 'title'));
    }

    public function unStudent()
    {
        $title = 'لیست دانشجویان در انتظار بررسی';
        $canReject = true;
        $students = User::student()
        ->unconfirmed()
        ->with('groupable.province' , 'groupable.city')
        ->orderbyDesc('updated_at')
        ->paginate(20);

        return view('user-management.unstudent-list' , compact('students' , 'title' , 'canReject'));
    }

    public function unApplicant()
    {
        $title = 'لیست کارجویان تایید نشده';
        $applicants = User::applicant()
        ->unconfirmed()
        ->orderbyDesc('updated_at')
        ->paginate(20);

        foreach($applicants as $applicant){
            if($applicant->groupable->birth){
                $sen = Carbon::parse($applicant->groupable->birth);
                $sen = $sen->diffInYears(Carbon::now());
                $applicant->age = $sen;
            }
        }

        return view('user-management.unapplicant-list' , compact('applicants' , 'title'));
    }

    public function acceptedCompany()
    {
        $title = 'لیست شرکت های تایید شده';

        $company = User::Company()
            ->whereRelation('groupable' , 'status' , 1)
            ->orderByDesc('updated_at')
            ->get();

        $user = User::Company()->where('status' , 1)->get();

        $companies = $company->merge($user);
        $ids = [];
        foreach ($companies as $co){
            array_push($ids , $co->id);
        }

        $companies = User::whereIn('id' , $ids)
            ->with('groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('user-management.accepted-company-list' , compact(
            'companies',
            'title',
        ));
    }

    public function acceptedStudent()
    {
        $title = 'لیست دانشجویان تایید شده';
        $students = User::student()
            ->Confirmed()
            ->with('groupable.province' , 'groupable.city')
            ->orderbyDesc('updated_at')
            ->paginate(20);

        return view('user-management.accepted-student-list' , compact('students' , 'title'));
    }

    public function acceptedApplicant()
    {
        $title = 'لیست کارجویان تایید شده';
        $applicants = User::Applicant()
            ->Confirmed()
            ->orderbyDesc('updated_at')
            ->paginate(20);

        foreach($applicants as $applicant){
            if($applicant->groupable->birth){
                $sen = Carbon::parse($applicant->groupable->birth);
                $sen = $sen->diffInYears(Carbon::now());
                $applicant->age = $sen;
            }
        }

        return view('user-management.accepted-applicant-list' , compact('applicants' , 'title'));
    }


    public function rejectedCompany()
    {
        $title = 'لیست شرکت های رد شده';
        $companies = User::Company()
            ->with('groupable.logo')
            ->orderbyDesc('updated_at')
            ->get();

        $ids = [];
        
        foreach($companies as $company){

            $s = $company->status;

            $gs = $company->groupable->status;

            if(($s == 0 && $gs === 0) || ($s == 2 && $gs === 1) || ($s == 2 && $gs === 0)){
                array_push($ids , $company->id);
            }
        }

        $companies = User::whereIn('id' , $ids)->with('groupable')
            ->orderbyDesc('updated_at')
            ->paginate(20);

        return view('user-management.rejected-company-list' , compact('companies' , 'title'));
    }

    public function rejectedStudent()
    {
        $title = 'لیست دانشجویان رد شده';
        $students = User::student()
        ->Rejected()
        ->with('groupable.province' , 'groupable.city')
        ->orderbyDesc('updated_at')
        ->paginate(20);

        return view('user-management.rejected-student-list' , compact('students' , 'title'));
    }

    public function rejectedApplicant()
    {
        $title = 'لیست کارجویان رد شده';
        $applicants = User::applicant()
        ->Rejected()
        ->orderbyDesc('updated_at')
        ->paginate(20);

        foreach($applicants as $applicant){
            if($applicant->groupable->birth){
                $sen = Carbon::parse($applicant->groupable->birth);
                $sen = $sen->diffInYears(Carbon::now());
                $applicant->age = $sen;
            }
        }

        return view('user-management.rejected-applicant-list' , compact('applicants' , 'title'));
    }

    public function allSearch(Request $req)
    {
        $title = $req->title;
        $canReject = true;

        $oldData = [
            'name' => $req->name,
            'family' => $req->family,
            'n_code' => $req->n_code,
            'mobile' => $req->mobile,
        ];

        $query = User::query();

        if($req->name != null || $req->name != ''){
            $query->where('name' , 'LIKE' , '%'. $req->name .'%');
        }

        if($req->family != null || $req->family != ''){
            $query->where('family' , 'LIKE' , '%'. $req->family .'%');
        }

        if($req->n_code != null || $req->n_code != ''){
            $query->where('n_code' , 'LIKE' , '%'. $req->n_code .'%');
        }

        if($req->mobile != null || $req->mobile != ''){
            $query->where('mobile' , 'LIKE' , '%'. $req->mobile .'%');
        }

        // dd($req);

        $users = $query->paginate(20)->appends([
            'title' => $req->title,
            'name' => $req->name,
            'family' => $req->family,
            'n_code' => $req->n_code,
            'mobile' => $req->mobile
        ]);

        return view('user-management.all' , compact(
            'users',
            'title',
            'oldData',
            'canReject'
        ));
    }

    public function studentSearch(Request $req)
    {
        $title = $req->title;
        $canReject = true;

        $type = $req->type;

        $oldData = [
            'name' => $req->name,
            'family' => $req->family,
            'n_code' => $req->n_code,
            'mobile' => $req->mobile,
        ];

        $query = User::query();
        $query->Student();

        if($type == 'un_std'){
            $query->UnConfirmed();
            $view = 'user-management.unstudent-list';
        }elseif($type == 're_std'){
            $query->Rejected();
            $view = 'user-management.rejected-student-list';
        }else{
            $query->Confirmed();
            $view = 'user-management.accepted-student-list';
        }

        if($req->name != null || $req->name != ''){
            $query->where('name' , 'LIKE' , '%'. $req->name .'%');
        }

        if($req->family != null || $req->family != ''){
            $query->where('family' , 'LIKE' , '%'. $req->family .'%');
        }

        if($req->n_code != null || $req->n_code != ''){
            $query->where('n_code' , 'LIKE' , '%'. $req->n_code .'%');
        }

        if($req->mobile != null || $req->mobile != ''){
            $query->where('mobile' , 'LIKE' , '%'. $req->mobile .'%');
        }

        // dd($req);

        $students = $query->paginate(20)->appends([
            'title' => $req->title,
            'type' => $req->type,
            'name' => $req->name,
            'family' => $req->family,
            'n_code' => $req->n_code,
            'mobile' => $req->mobile
        ]);

        return view($view , compact(
            'students',
            'title',
            'oldData',
            'canReject'
        ));
    }

    public function companySearch(Request $req)
    {
        $title = $req->title;
        $type = $req->type;

        $oldData = [
            'coname' => $req->coname,
            'name' => $req->name,
            'family' => $req->family,
            'n_code' => $req->n_code,
            'mobile' => $req->mobile,
        ];

        $query = User::query();
        $query->Company();

        if($type == 'un_co'){
            $query->UnConfirmed();
            $view = 'user-management.uncompany-list';
        }elseif($type == 're_co'){
            $query->RejectCompanyInfo();
            $view = 'user-management.rejected-company-list';
        }else{
            $query->whereRelation('groupable' , 'status' , 1);
            $view = 'user-management.accepted-company-list';
        }

        if($req->coname != null || $req->coname != ''){
            $query->whereRelation('groupable' ,'name' , 'LIKE' , '%'. $req->coname .'%');
        }

        if($req->name != null || $req->name != ''){
            $query->where('name' , 'LIKE' , '%'. $req->name .'%');
        }

        if($req->family != null || $req->family != ''){
            $query->where('family' , 'LIKE' , '%'. $req->family .'%');
        }

        if($req->n_code != null || $req->n_code != ''){
            $query->where('n_code' , 'LIKE' , '%'. $req->n_code .'%');
        }

        if($req->mobile != null || $req->mobile != ''){
            $query->where('mobile' , 'LIKE' , '%'. $req->mobile .'%');
        }

        $companies = $query->paginate(20)->appends([
            'title' => $req->title,
            'type' => $req->type,
            'coname' => $req->coname,
            'name' => $req->name,
            'family' => $req->family,
            'n_code' => $req->n_code,
            'mobile' => $req->mobile
        ]);

        return view($view , compact(
            'companies',
            'title',
            'oldData'
        ));
    }

    public function applicantSearch(Request $req)
    {
        $title = $req->title;

        $type = $req->type;

        $oldData = [
            'name' => $req->name,
            'family' => $req->family,
            'n_code' => $req->n_code,
            'mobile' => $req->mobile,
        ];

        $query = User::query();
        $query->Applicant();

        if($type == 'un_app'){
            $query->UnConfirmed();
            $view = 'user-management.unapplicant-list';
        }elseif($type == 're_app'){
            $query->Rejected();
            $view = 'user-management.rejected-applicant-list';
        }else{
            $query->Confirmed();
            $view = 'user-management.accepted-applicant-list';
        }

        if($req->name != null || $req->name != ''){
            $query->where('name' , 'LIKE' , '%'. $req->name .'%');
        }

        if($req->family != null || $req->family != ''){
            $query->where('family' , 'LIKE' , '%'. $req->family .'%');
        }

        if($req->n_code != null || $req->n_code != ''){
            $query->where('n_code' , 'LIKE' , '%'. $req->n_code .'%');
        }

        if($req->mobile != null || $req->mobile != ''){
            $query->where('mobile' , 'LIKE' , '%'. $req->mobile .'%');
        }

        $applicants = $query->paginate(20)->appends([
            'title' => $req->title,
            'type' => $req->type,
            'name' => $req->name,
            'family' => $req->family,
            'n_code' => $req->n_code,
            'mobile' => $req->mobile
        ]);

        foreach($applicants as $applicant){
            if($applicant->groupable->birth){
                $sen = Carbon::parse($applicant->groupable->birth);
                $sen = $sen->diffInYears(Carbon::now());
                $applicant->age = $sen;
            }
        }

        return view($view , compact(
            'applicants',
            'title',
            'oldData',
        ));
    }

    public function createUser()
    {
        return view('user-management.create-user');
    }

    public function storeUser(Request $request)
    {
//        dd($request->all());
        $request->validate([
            'name' => 'required|max:25',
            'family' => 'required|string|max:40',
            'role' => 'required',
            'email' => 'required|unique:users|email',
            'mobile' => 'required|string|max:11|unique:users',
            'password' => 'required|string|min:8',
        ],[
            'role.required'=>'نوع کاربری الزامی است',
            'name.required'=>'نام الزامی است',
            'name.string'=>'نام را به صورت صحیح وارد کنید',
            'name.max'=>'طول نام بیش از حد مجاز است',
            'family.required'=>'نام خانوادگی الزامی است',
            'family.string'=>'نام خانوادگی را به صورت صحیح وارد کنید',
            'family.max'=>'طول نام خانوادگی بیش از حد مجاز است',
            'mobile.required'=>'موبایل الزامی است',
            'mobile.max'=>'موبایل را به صورت صحیح وارد کنید',
            'mobile.unique'=>'موبایل تکراری است',
            'password.required'=>'کلمه عبور الزامی است',
            'password.min'=>'کلمه عبور حداقل باید 8 کاراکتر باشد',
            'email.email' => 'لطفا ایمیل را بصورت صحیح وارد کنید',
            'email.unique' => 'ایمیل تکراری است',
            'email.required' => 'ایمیل الزامی است'
        ]);
//        dd($request->all());
        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'mobile' => $request->mobile,
            'sex' => $request->sex,
            'password' => Hash::make($request->password),
            'email' => $request->email,
        ]);
        $user->assignRole($request->role);
        \EmployeeLogActivity::addToLog('یک کاربر با نام '.$user->name." ".$user->family.' ثبت کرد');

        return redirect(route('admin.create.user'))->with('success',__('public.user_added'));
    }

    public function storeUserByExcel(Request $request)
    {
        if (is_null($request->role)) return back()->with('error','لطفا نوع کاربران را انتخاب کنید');
        Excel::import(new UsersImport($request->role , $request->sms),\request()->file('users'));
        \EmployeeLogActivity::addToLog('تعدادی کاربر از طریق فایل اکسل ثبت کرد');

        return back()->with('success',__('public.users_added'));
    }



    public function uploadCoNewsletter(Request $request)
    {
        $request->validate([
                'newsletter'.$request->company => 'required|max:1024|mimes:png,jpg,jpeg,pdf',
            ], [
                'newsletter'. $request->company .'.required' => 'فایل روزنامه شرکت الزامی است',
                'newsletter'. $request->company .'.max' => 'حجم فایل باید حداکثر 1 مگابایت باشد',
                'newsletter'. $request->company .'.mimes' => 'فرمت فایل نادرست است',
        ]);

        $company = User::with('groupable')
        ->findOrFail($request->company);

        if(is_null($company->groupable->newsletter_id) && $request->file('newsletter'.$request->company)){
            $name = time() . $request->file('newsletter'.$request->company)->getClientOriginalName();
            $path = $request->file('newsletter'.$request->company)->storeAs('img/emp/co-newsletter', $name, 'public');

            $newsletter = Newsletter::create([
                'name' => $name,
                'format' => $request->file('newsletter'.$request->company)->extension(),
                'path' => $path,
            ]);

            $company->groupable->newsletter_id = $newsletter->id;
            $company->groupable->save();
        }

        EmployeeLogActivity::addToLog(' کارشناس فایل روزنامه شرکت '. $company->groupable->name .' را آپلود کرد ');
        return redirect()->back()->with('success' , ' فایل روزنامه شرکت '. $company->groupable->name . ' با موفقیت آپلود شد ');

    }

    public function confirmCoInfo(Request $request)
    {
        $user = User::with('groupable')->findOrFail($request->id);
        if(is_null($user->groupable->newsletter_id)){
            return response()->json(['cantConfirm' => 'فایل روزنامه شرکت ثبت نشده است و مجاز به تایید نمی‌باشد']);
        }
        $user->groupable->update(['status' => 1]);
        EmployeeLogActivity::addToLog('کارشناس اکانت کاربر'.$user->name.''. $user->family.' را تایید کرد');

        return response()->json(true);
    }

    public function confirmUser(Request $request)
    {
        $user = User::findOrFail($request->id);

        $user->update(['status' => 1]);
        EmployeeLogActivity::addToLog('اکانت کاربر '.$user->name.''. $user->family.' را تایید کرد');

        if($user->hasVerifiedEmail()){
            $user->notify(new UserConfirmed($user));
        }else{
            $notif = new Notification();
            $notif->sendSms($user,'fanyarAcceptUserInfo',[$user->name,$user->family]);
        }

        return response()->json(true);
    }

    public function editCompanyRole(Request $request)
    {
        $user = User::find($request->id);

        $user->update([
            'groupable_id' => null,
            'groupable_type' => null,
        ]);

        $roles = $user->getRoleNames();

        $user->removeRole($roles[0]);

        $user->assignRole('company');

        return response()->json(true);
    }

    public function editApplicantRole(Request $request)
    {
        $user = User::find($request->id);

        $roles = $user->getRoleNames();

        if($roles[0] == 'applicant') return response()->json(false);

        if($roles[0] == 'student') {

            if($user->sendRequest()){

                foreach($user->sendRequest()->get() as $s_request){

                    if($s_request->type == 'intern'){

                        $s_request->update([
                            'status' => 2
                        ]);

                    }

                }
                
            }

            if($user->receivedRequest()){

                foreach($user->receivedRequest()->get() as $r_request){

                    if($r_request->type == 'intern'){

                        $r_request->update([
                            'status' => 2
                        ]);

                    }

                }

            }

        }

        $user->update([
            'groupable_id' => null,
            'groupable_type' => null,
        ]);

        $user->removeRole($roles[0]);

        $user->assignRole('applicant');

        return response()->json(true);
    }

    public function editStudentRole(Request $request)
    {
        $user = User::find($request->id);

        $roles = $user->getRoleNames();

        if($roles[0] == 'student') return response()->json(false);

        $user->update([
            'groupable_id' => null,
            'groupable_type' => null,
        ]);

        

        $user->removeRole($roles[0]);

        $user->assignRole('student');

        return response()->json(true);
    }

}
