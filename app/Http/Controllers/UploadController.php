<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UploadController extends Controller
{



    public function uploadUserCertificate(Request $request)
    {
//        dd($request);
        $request->validate([
            'photo' => 'mimes:jpg,jpeg,png,svg|max:1024'
        ]);
        $certphoto = $request->file('photo');
        if($_FILES['photo']){
            $certificate = Certificate::create([
                'user_id' => Auth::id(),
                'title' => $request->title,
            ]);
            $name = time() . $certphoto->getClientOriginalName();
            $path = $certphoto->storeAs('img/certificate/', $name, 'public');

            $photo = Photo::create([
                'name'=> $name,
                'path'=> $path,
                'groupable_id'=> $certificate->id,
                'groupable_type'=> Certificate::class
            ]);
            $certificate->updateOrFail([
                'photo_id'=> $photo->id,
            ]);
            return response()->json(['path' => $path , 'title' => $certificate->title , 'id' => $certificate->id]);
        }
    }
}
