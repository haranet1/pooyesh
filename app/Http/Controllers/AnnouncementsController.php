<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\File;
use Illuminate\Http\Request;

class AnnouncementsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isEmployee']);
    }

    public function index()
    {
        $announces = Announcement::paginate(20);
        return view('panel.employee.announcements',compact('announces'));
    }

    public function create()
    {
        return view('panel.employee.create-announcement');

    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'content'=>'required'
        ]);

        $announce = new Announcement();
        $announce->title = $request->input('title');
        $announce->content = $request->input('content');
        $announce->save();
        if ($request->file('file')){
            $name = time() . $request->file->getClientOriginalName();
            $path = $request->file->storeAs(
                'files/announcements/',
                $name,
                'public'
            );
            $file = new File();
            $file->name = $name;
            $file->path = $path;
            $file->groupable_id = $announce->id;
            $file->groupable_type = Announcement::class;
            $file->save();
            $announce->file_id =$file->id;
            $announce->save();
        }
        \EmployeeLogActivity::addToLog('یک اطلاعیه جدید ثبت کرد');

        return redirect(route('emp.announce.list'))->with('success',__('public.data_added'));
    }
}
