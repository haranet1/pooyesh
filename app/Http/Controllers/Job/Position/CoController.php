<?php

namespace App\Http\Controllers\Job\Position;

use App\Helpers\CompanyLogActivity;
use App\Http\Controllers\Controller;
use App\Models\CooperationRequest;
use App\Models\EmployeeInfo;
use App\Models\Major;
use App\Models\OabilityJob;
use App\Models\Ocategory;
use App\Models\Ojob;
use App\Models\OjobKnowledge;
use App\Models\OjobPosition;
use App\Models\OjobSkill;
use App\Models\OjobTechnologySkill;
use App\Models\OjobWorkStyle;
use App\Models\OJpAbility;
use App\Models\OJpFacilities;
use App\Models\OJpFavorite;
use App\Models\OJpKnowledge;
use App\Models\OJpSkill;
use App\Models\OJpTask;
use App\Models\OJpTechnologySkill;
use App\Models\OJpWorkStyle;
use App\Models\Otask;
use App\Models\Province;
use App\Models\User;
use App\Notifications\NewJobPositionCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class CoController extends Controller
{
    public function hireIndex()
    {
        CompanyLogActivity::addToLog('به صفحه لیست موقعیت های استخدامی آمد');

        $jobPositions = OjobPosition::NotDeleted()
            ->Hire()
            ->with('job.category.photo')
            ->where('company_id', Auth::id())
            ->paginate(20);

        $config = [
            'title' => 'لیست موقعیت های استخدامی',
            'alert' => 'در این صفحه لیست موقعیت های شغلی استخدامی ثبت شده توسط شما، نمایش داده میشود.',
            'create' => 'ایجاد موقعیت استخدامی جدید',
        ];

        return view('job.position.co.index' , compact(
            'jobPositions',
            'config'
        ));
    }

    public function internIndex()
    {
        CompanyLogActivity::addToLog('به صفحه لیست موقعیت های کارآموزی آمد');

        $jobPositions = OjobPosition::NotDeleted()
            ->Intern()
            ->with('job')
            ->where('company_id', Auth::id())
            ->paginate(20);

        $config = [
            'title' => 'لیست موقعیت های کارآموزی',
            'alert' => 'در این صفحه لیست موقعیت های شغلی استخدامی ثبت شده توسط شما نمایش داده میشود.',
            'create' => 'ایجاد موقعیت کارآموزی جدید',
        ];

        return view('job.position.co.index' , compact(
            'jobPositions',
            'config'
        ));
    }

    public function create()
    {
        $routeType = explode('/' , URL::previous());
        $title = '';
        if(isset($routeType[7]) && $routeType[7] == 'intern'){
            $title = 'ثبت موقعیت کارآموزی';
            $internCount = OjobPosition::where('company_id', Auth::id())->where('type', 'intern')->count();
            if($internCount >= Auth::user()->companyInfos->create_internship_limit){
               return redirect()->back()->with('limit', 'کاربر گرامی، تعداد مجاز موقعیت های کارآموزی شما به پایان رسیده است');
            }
        } else {
            $title = 'ثبت موقعیت استخدامی';
            $hireCount = OjobPosition::where('company_id', Auth::id())->where('type','!=' ,'intern')->count();
            if($hireCount >= Auth::user()->companyInfos->create_hire_limit){
               return redirect()->back()->with('limit', 'کاربر گرامی، تعداد مجاز موقعیت های استخدامی شما به پایان رسیده است');
            }
        }
        
        $categories = Ocategory::all();

        $jobs = Ojob::with('category')
            ->whereNotNull('description')
            ->get();

        return view('job.position.co.create' , compact(
            'categories',
            'jobs',
            'title'
        ));
    }

    public function show(Request $request)
    {
        $categories = Ocategory::all();

        $job = Ojob::with('category')->find($request->job_id);

        $jobskills = OjobSkill::where('job_id' , $job->id)->with('skillname','importance', 'level')->get();

        $skills = $jobskills->sortByDesc(function ($jobskill){
            return $jobskill->importance->data_value;
        });

        $jobKnowledges = OjobKnowledge::where('job_id', $job->id)->with('knowledgeName' , 'importance' , 'level')->get();

        $knowledges = $jobKnowledges->sortByDesc( function ($jobKnowledge) {
            return $jobKnowledge->importance->data_value;
        });

        $jobAbilities = OabilityJob::where('job_id', $job->id)->with('abilityName' , 'importance' , 'level')->get();

        $abilities = $jobAbilities->sortByDesc( function ($jobAbility) {
            return $jobAbility->importance->data_value;
        });

        $jobWorkStyles = OjobWorkStyle::where('job_id', $job->id)->with('workStyleName', 'importance' , 'level')->get();

        $workStyles = $jobWorkStyles->sortByDesc( function ($jobWorkStyle) {
            return $jobWorkStyle->importance->data_value;
        });

        $jobTechnologySkills = OjobTechnologySkill::where('job_id' , $job->id)
            ->where('in_demand' , "Y")->with('technologySkillName' , 'importance' , 'example')->orderByDesc('id')->get();

        $technologySkills = $jobTechnologySkills->sortByDesc( function ($TechnologySkill) {
            if(! is_null($TechnologySkill->importance)){
                return $TechnologySkill->importance->data_value;
            }
        });

        $tasks = Otask::where('job_id' , $job->id)->get();

        $provinces = Province::all();
        $majors = Major::all();

        $title = 'ایجاد موقعیت '. $job->title ;

        $canCreateIntern = true;
        $internCount = OjobPosition::where('company_id', Auth::id())->where('type', 'intern')->count();
        if($internCount >= Auth::user()->companyInfos->create_internship_limit){
           $canCreateIntern = false;
        }

        return view('job.position.co.create' , compact(
            'categories',
            'job',
            'skills',
            'knowledges',
            'abilities',
            'workStyles',
            'technologySkills',
            'tasks',
            'provinces',
            'majors',
            'title',
            'canCreateIntern'
        ));
    }

    public function store(Request $request)
    {
        if(isset($request->type)) $jobType = $request->type;

        else $jobType = 'intern';
        
        $jobPosition = OjobPosition::create([
            'company_id' => Auth::id(),
            'job_id' => $request->input('job_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),

            'type' => $jobType,
            'level' => $request->input('level'),
            'experience' => $request->input('experience'),
            'working_hours' => $request->input('working_hours'),
            'salary' => $request->input('salary'),
            'age' => $request->input('age'),
            'sex' => $request->input('sex'),
            'marige_type' => $request->input('marige_type'),
            'military_service_status' => $request->input('military_service_status'),
            'grade' => $request->input('grade'),
            'major_id' => $request->input('major') == 'unmatter' ? null : $request->input('major'),
            'province_id' => $request->input('province'),
            'city_id' => $request->input('city'),
            'address' => $request->input('address'),
            'about' => $request->input('about'),
        ]);

        foreach($request->all() as $key => $value){

            if (strpos($key, 'skill_') === 0) {
                $skill_id_or_title  = str_replace('skill_', '', $key);

                if(is_numeric($skill_id_or_title)) {
                    OJpSkill::create([
                        'job_position_id' => $jobPosition->id,
                        'skill_id' => $skill_id_or_title,
                        'importance' => $value,
                    ]);
                } else {
                    $skill_id_or_title = str_replace('_', ' ', $skill_id_or_title);
                    OJpSkill::create([
                        'job_position_id' => $jobPosition->id,
                        'title' => $skill_id_or_title,
                        'importance' => $value,
                    ]);
                }
                
            }

            if(strpos($key , 'knowledge_') === 0) {
                $knowledge_id_or_title = str_replace('knowledge_', '', $key);

                if(is_numeric($knowledge_id_or_title)) {
                    OJpKnowledge::create([
                        'job_position_id' => $jobPosition->id,
                        'knowledge_id' => $knowledge_id_or_title,
                        'importance' => $value,
                    ]);
                } else {
                    $knowledge_id_or_title = str_replace('_', ' ', $knowledge_id_or_title);
                    OJpKnowledge::create([
                        'job_position_id' => $jobPosition->id,
                        'title' => $knowledge_id_or_title,
                        'importance' => $value,
                    ]);
                }
                
            }

            if(strpos($key , 'ability_') === 0) {
                $ability_id_or_title = str_replace('ability_', '', $key);

                if(is_numeric($ability_id_or_title)) {
                    OJpAbility::create([
                        'job_position_id' => $jobPosition->id,
                        'ability_id' => $ability_id_or_title,
                        'importance' => $value,
                    ]);
                } else {
                    $ability_id_or_title = str_replace('_', ' ', $ability_id_or_title);
                    OJpAbility::create([
                        'job_position_id' => $jobPosition->id,
                        'title' => $ability_id_or_title,
                        'importance' => $value,
                    ]);
                }
            }

            if(strpos($key, 'workStyle_') === 0) {
                $workstyle_id_or_title = str_replace('workStyle_', '', $key);

                if(is_numeric($workstyle_id_or_title)) {
                    OJpWorkStyle::create([
                        'job_position_id' => $jobPosition->id,
                        'work_style_id' => $workstyle_id_or_title,
                        'importance' => $value,
                    ]);
                } else {
                    $workstyle_id_or_title = str_replace('_', ' ', $workstyle_id_or_title);
                    OJpWorkStyle::create([
                        'job_position_id' => $jobPosition->id,
                        'title' => $workstyle_id_or_title,
                        'importance' => $value,
                    ]);
                }
            }

            if(strpos($key, 'technologySkill_') === 0) {
                $technologySkill_id_or_title = str_replace('technologySkill_', '', $key);

                if(is_numeric($technologySkill_id_or_title)) {
                    OJpTechnologySkill::create([
                        'job_position_id' => $jobPosition->id,
                        'technology_skill_id' => $technologySkill_id_or_title,
                        'job_technology_skill_id' => $value
                    ]);
                } else {
                    $technologySkill_id_or_title = str_replace('_', ' ', $technologySkill_id_or_title);
                    OJpTechnologySkill::create([
                        'job_position_id' => $jobPosition->id,
                        'title' => $technologySkill_id_or_title,
                        'job_technology_skill_id' => $value
                    ]);
                }
            }

            if(strpos($key, 'task_') === 0) {
                $task_id_or_title = str_replace('task_', '', $key);

                if(is_numeric($task_id_or_title)) {
                    OJpTask::create([
                        'job_position_id' => $jobPosition->id,
                        'task_id' => $task_id_or_title,
                    ]);
                } else {
                    $task_id_or_title = str_replace('_', ' ', $task_id_or_title);
                    OJpTask::create([
                        'job_position_id' => $jobPosition->id,
                        'title' => $task_id_or_title,
                    ]);
                }
            }

            if(strpos($key, 'facilities_') === 0){
                $facilities_title = str_replace('facilities_', '',$key);
                $facilitiesTitle = str_replace('_',' ',$facilities_title);

                OJpFacilities::create([
                    'job_position_id' => $jobPosition->id,
                    'title' => $facilitiesTitle,
                ]);
            }

        }

        $emps = User::where('groupable_type' , EmployeeInfo::class)->get();

        foreach($emps as $emp){
            $emp->notify(new NewJobPositionCreated($jobPosition));
        }

        if($jobPosition->type == 'intern'){
            return redirect()->route('co.intern.pos.job')->with('success' , 'موقعیت کارآموزی جدید اضافه شد.');
        }

        return redirect()->route('co.hire.pos.job')->with('success' , 'موقعیت استخدامی جدید اضافه شد.');
    }

    public function edit($id)
    {
        $categories = Ocategory::all();
        $provinces = Province::all();
        $majors = Major::all();

        $jp = OjobPosition::with(
            'job.category',
            'skills.skill',
            'abilities.ability',
            'knowledges.knowledge',
            'workStyles.workStyle',
            'technologySkills.technologySkill',
            'tasks.task',
            'province',
            'city',
            'major',
        )->find($id);

        $job = Ojob::with('category')->find($jp->job_id);

        $jobskills = OjobSkill::where('job_id' , $job->id)->with('skillName', 'importance' , 'level')->get();

        $skills = $jobskills->sortByDesc(function ($jobskill){
            return $jobskill->importance->data_value;
        });

        $jobKnowledges = OjobKnowledge::where('job_id', $job->id)->with('knowledgeName' , 'importance' , 'level')->get();

        $knowledges = $jobKnowledges->sortByDesc( function ($jobKnowledge) {
            return $jobKnowledge->importance->data_value;
        });

        $jobAbilities = OabilityJob::where('job_id', $job->id)->with('abilityName' , 'importance' , 'level')->get();

        $abilities = $jobAbilities->sortByDesc( function ($jobAbility) {
            return $jobAbility->importance->data_value;
        });

        $jobWorkStyles = OjobWorkStyle::where('job_id', $job->id)->with('workStyleName', 'importance' , 'level')->get();

        $workStyles = $jobWorkStyles->sortByDesc( function ($jobWorkStyle) {
            return $jobWorkStyle->importance->data_value;
        });

        $jobTechnologySkills = OjobTechnologySkill::where('job_id' , $job->id)
            ->where('in_demand' , "Y")->with('technologySkillName' , 'importance' , 'example')->orderByDesc('id')->get();

        $technologySkills = $jobTechnologySkills->sortByDesc( function ($TechnologySkill) {
            if(! is_null($TechnologySkill->importance)){
                return $TechnologySkill->importance->data_value;
            }
        });

        $tasks = Otask::where('job_id' , $job->id)->get();

        $isIntern = false;
        $internCount = OjobPosition::where('company_id', Auth::id())->where('type', 'intern')->count();
        if($jp->type == 'intern'){
            $canCreateIntern = true;
            $isIntern = true;
        }

        return view('job.position.co.edit' , compact(
            'jp',
            'categories',
            'job',
            'skills',
            'knowledges',
            'abilities',
            'workStyles',
            'technologySkills',
            'tasks',
            'provinces',
            'majors',
            'isIntern'
        ));
    }

    public function update(Request $request)
    {
        $jp = OjobPosition::find($request->jp_id);
        

        $jp->update([
            // 'company_id' => Auth::id(),
            // 'job_id' => $request->input('job_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),

            'type' => $request->input('type'),
            'level' => $request->input('level'),
            'experience' => $request->input('experience'),
            'working_hours' => $request->input('working_hours'),
            'salary' => $request->input('salary'),
            'age' => $request->input('age'),
            'sex' => $request->input('sex'),
            'marige_type' => $request->input('marige_type'),
            'military_service_status' => $request->input('military_service_status'),
            'grade' => $request->input('grade'),
            'major_id' => $request->input('major') == 'unmatter' ? null : $request->input('major'),
            'province_id' => $request->input('province'),
            'city_id' => $request->input('city'),
            'address' => $request->input('address'),
            'about' => $request->input('about'),
        ]);

        foreach($request->all() as $key => $value){
            if(strpos($key , 'skill_') === 0) $jp->skills()->delete();
            if(strpos($key , 'knowledge_') === 0) $jp->knowledges()->delete();
            if(strpos($key , 'ability_') === 0) $jp->abilities()->delete();
            if(strpos($key , 'workStyle_') === 0) $jp->workStyles()->delete();
            if(strpos($key , 'technologySkill_') === 0) $jp->technologySkills()->delete();
            if(strpos($key , 'task_') === 0) $jp->tasks()->delete();
            if(strpos($key , 'facilities_') === 0) $jp->facilities()->delete();
        }

        foreach($request->all() as $key => $value){
            if(strpos($key, 'skill_') === 0) {
                
                $skill_id_or_title  = str_replace('skill_', '', $key);

                if(is_numeric($skill_id_or_title)) {
                    OJpSkill::create([
                        'job_position_id' => $jp->id,
                        'skill_id' => $skill_id_or_title,
                        'importance' => $value,
                    ]);
                } else {
                    $skill_id_or_title = str_replace('_', ' ', $skill_id_or_title);
                    OJpSkill::create([
                        'job_position_id' => $jp->id,
                        'title' => $skill_id_or_title,
                        'importance' => $value,
                    ]);
                }
                
            }

            if(strpos($key , 'knowledge_') === 0) {
                
                $knowledge_id_or_title = str_replace('knowledge_', '', $key);
                if(is_numeric($knowledge_id_or_title)) {
                    OJpKnowledge::create([
                        'job_position_id' => $jp->id,
                        'knowledge_id' => $knowledge_id_or_title,
                        'importance' => $value,
                    ]);
                } else {
                    $knowledge_id_or_title = str_replace('_', ' ', $knowledge_id_or_title);
                    OJpKnowledge::create([
                        'job_position_id' => $jp->id,
                        'title' => $knowledge_id_or_title,
                        'importance' => $value,
                    ]);
                }
            }

            if(strpos($key , 'ability_') === 0) {
                
                $ability_id_or_title = str_replace('ability_', '', $key);

                if(is_numeric($ability_id_or_title)) {
                    OJpAbility::create([
                        'job_position_id' => $jp->id,
                        'ability_id' => $ability_id_or_title,
                        'importance' => $value,
                    ]);
                } else {
                    $ability_id_or_title = str_replace('_', ' ', $ability_id_or_title);
                    OJpAbility::create([
                        'job_position_id' => $jp->id,
                        'title' => $ability_id_or_title,
                        'importance' => $value,
                    ]);
                }
            }

            if(strpos($key, 'workStyle_') === 0) {
                
                $workstyle_id_or_title = str_replace('workStyle_', '', $key);
                if(is_numeric($workstyle_id_or_title)) {
                    OJpWorkStyle::create([
                        'job_position_id' => $jp->id,
                        'work_style_id' => $workstyle_id_or_title,
                        'importance' => $value,
                    ]);
                } else {
                    $workstyle_id_or_title = str_replace('_', ' ', $workstyle_id_or_title);
                    OJpWorkStyle::create([
                        'job_position_id' => $jp->id,
                        'title' => $workstyle_id_or_title,
                        'importance' => $value,
                    ]);
                }
            }
            
            if(strpos($key, 'technologySkill_') === 0) {

                $technologySkill_id_or_title = str_replace('technologySkill_', '', $key);
                if(is_numeric($technologySkill_id_or_title)){
                    OJpTechnologySkill::create([
                        'job_position_id' => $jp->id,
                        'technology_skill_id' => $technologySkill_id_or_title,
                        'example' => $value
                    ]);
                } else {
                    $technologySkill_id_or_title = str_replace('_', ' ', $technologySkill_id_or_title);
                    OJpTechnologySkill::create([
                        'job_position_id' => $jp->id,
                        'title' => $technologySkill_id_or_title,
                        'example' => $value
                    ]);
                }
            }

            if(strpos($key, 'task_') === 0) {
                
                $task_id_or_title = str_replace('task_', '', $key);

                if(is_numeric($task_id_or_title)) {
                    OJpTask::create([
                        'job_position_id' => $jp->id,
                        'task_id' => $task_id_or_title,
                    ]);
                } else {
                    $task_id_or_title = str_replace('_', ' ', $task_id_or_title);
                    OJpTask::create([
                        'job_position_id' => $jp->id,
                        'title' => $task_id_or_title,
                    ]);
                }
            }

            if(strpos($key, 'facilities_') === 0) {
                
                $facilities_title = str_replace('facilities_', '',$key);
                $facilitiesTitle = str_replace('_',' ',$facilities_title);
                OJpFacilities::create([
                    'job_position_id' => $jp->id,
                    'title' => $facilitiesTitle,
                ]);
            }
        }

        if($jp->type == 'intern'){
            return redirect()->route('co.intern.pos.job')->with(' موقعیت کارآموزی ' . $jp->title . ' ویرایش شد ');    
        }

        return redirect()->route('co.hire.pos.job')->with(' موقعیت استخدامی ' . $jp->title . ' ویرایش شد ');


    }
    
    public function delete(Request $request)
    {
        $jp = OjobPosition::find($request->id);

        return response()->json($jp->update([
            'deleted' => 1
        ]));
    }

    public function active(Request $request)
    {
        $job = OjobPosition::find($request->id);

        $job->update(['active' => $request->activeStatus]);
        
        $response = '';
        if($job->active == 1){
            $response = 'فعال';
        }else{
            $response = 'غیرفعال';
        }

        return response()->json($response);
    }
}
