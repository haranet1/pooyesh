<?php

namespace App\Http\Controllers\Job\Position;

use App\Helpers\EmployeeLogActivity;
use App\Http\Controllers\Controller;
use App\Mail\RejectJobPosition;
use App\Models\Attachment;
use App\Models\OjobPosition;
use App\Models\Ticket;
use App\Models\TicketReply;
use App\Notifications\RejectJobPosition as NotificationsRejectJobPosition;
use App\Notifications\UniAcceptedJobPosition;
use App\Services\Notification\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmpController extends Controller
{
    public function hireIndex()
    {
        $config = [
            'type' => 'استخدامی',
            'title' => 'موقعیت های استخدامی',
            'un' => OjobPosition::Hire()->UnConfirmed()->NotDeleted()->count(),
            'ac' => OjobPosition::Hire()->Confirmed()->NotDeleted()->count(),
            're' => OjobPosition::Hire()->Rejected()->NotDeleted()->count(),
        ];

        $links = [
            'un' => route('emp.un.hire.pos.job'),
            'ac' => route('emp.ac.hire.pos.job'),
            're' => route('emp.re.hire.pos.job'),
        ];

        return view('job.position.emp.index' , compact(
            'config',
            'links'
        ));
    }

    public function internIndex()
    {
        $config = [
            'type' => 'کارآموزی',
            'title' => 'موقعیت های کارآموزی',
            'un' => OjobPosition::Intern()->UnConfirmed()->NotDeleted()->count(),
            'ac' => OjobPosition::Intern()->Confirmed()->NotDeleted()->count(),
            're' => OjobPosition::Intern()->Rejected()->NotDeleted()->count(),
        ];

        $links = [
            'un' => route('emp.un.intern.pos.job'),
            'ac' => route('emp.ac.intern.pos.job'),
            're' => route('emp.re.intern.pos.job'),
        ];

        return view('job.position.emp.index' , compact(
            'config',
            'links'
        ));
    }

    public function unConfirmedIntern()
    {
        $config = [
            'title' => 'موقعیت های کارآموزی در انتظار بررسی', 
            'mainPage' => 'emp.intern.pos.job',
            'searchType' => 'un_intern_position',
            'thisRoute' => 'emp.un.intern.pos.job',
            'export' => 'export.emp.un.intern.pos.job',
            'intern' => true,
            'rejectSubject' => 'رد موقعیت کارآموزی',
        ];

        $job_positions = OjobPosition::UnConfirmed()
            ->NotDeleted()
            ->Intern()
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('job.position.emp.unconfirmed-list',compact(
            'config',
            'job_positions',
        ));
    }

    public function unConfirmedHire()
    {
        $config = [
            'title' => 'موقعیت های استخدامی در انتظار بررسی', 
            'mainPage' => 'emp.hire.pos.job',
            'searchType' => 'un_hire_position',
            'thisRoute' => 'emp.un.hire.pos.job',
            'export' => 'export.emp.un.hire.pos.job',
            'intern' => false,
            'rejectSubject' => 'رد موقعیت استخدامی',
        ];

        $job_positions = OjobPosition::UnConfirmed()
            ->NotDeleted()
            ->Hire()
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('job.position.emp.unconfirmed-list',compact(
            'config',
            'job_positions',
        ));
    }

    public function acceptedIntern()
    {
        $config = [
            'title' => 'موقعیت های کارآموزی تایید شده', 
            'mainPage' => 'emp.intern.pos.job',
            'searchType' => 'intern_position',
            'thisRoute' => 'emp.ac.intern.pos.job',
            'export' => 'export.emp.ac.intern.pos.job',
            'intern' => true,
        ];
        
        $job_positions = OjobPosition::Confirmed()
            ->NotDeleted()
            ->Intern()
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('job.position.emp.confirmed-list',compact(
            'config',
            'job_positions',
        ));
    }

    public function acceptedHire()
    {
        $config = [
            'title' => 'موقعیت های استخدامی تایید شده', 
            'mainPage' => 'emp.hire.pos.job',
            'searchType' => 'hire_position',
            'thisRoute' => 'emp.ac.hire.pos.job',
            'export' => 'export.emp.ac.hire.pos.job',
            'intern' => false,
        ];
        
        $job_positions = OjobPosition::Confirmed()
            ->NotDeleted()
            ->Hire()
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('job.position.emp.confirmed-list',compact(
            'config',
            'job_positions',
        ));
    }

    public function rejectedIntern()
    {
        $config = [
            'title' => 'موقعیت های کارآموزی رد شده', 
            'mainPage' => 'emp.intern.pos.job',
            'searchType' => 're_intern_position',
            'thisRoute' => 'emp.re.intern.pos.job',
            'export' => 'export.emp.re.intern.pos.job',
            'intern' => true,
        ];
        
        $job_positions = OjobPosition::Rejected()
            ->NotDeleted()
            ->Intern()
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('job.position.emp.rejected-list',compact(
            'config',
            'job_positions',
        ));
    }

    public function rejectedHire()
    {
        $config = [
            'title' => 'موقعیت های استخدامی رد شده', 
            'mainPage' => 'emp.hire.pos.job',
            'searchType' => 're_hire_position',
            'thisRoute' => 'emp.re.hire.pos.job',
            'export' => 'export.emp.re.hire.pos.job',
            'intern' => false,
        ];
        
        $job_positions = OjobPosition::Rejected()
            ->NotDeleted()
            ->Hire()
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('job.position.emp.rejected-list',compact(
            'config',
            'job_positions',
        ));
    }

    public function accept(Request $request)
    {
        $jp = OjobPosition::with('company')->find($request->id);
        $jp->update(['status' => 1]);

        EmployeeLogActivity::addToLog('موقعیت استخدامی با شناسه '. $request->id . ' را تایید کرد.');

        if($jp->company->hasVerifiedEmail()){
            $jp->company->notify(new UniAcceptedJobPosition($jp));
        }else{
            $notif = new Notification();
            $notif->sendSms($jp->company, 'fanyarAcceptNewPosition',[job_type_persian($jp->type),$jp->title]);
        }

        $notifForRamak = new Notification();
        $notifForRamak->sendRamakSms('09908834717' , 'jobistAcceptedJobForStory' , [job_type_persian($jp->type) , $jp->id]);

        return response()->json(true);
    }

    public function reject(Request $request)
    {
        $jp = OjobPosition::with('company')->find($request->job_id);
        $jp->status = 2;

        if($jp->type == 'intern'){
            $category = 'iPos';
        } else {
            $category = 'hPos';
        }

        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->input('receiver_id'),
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('job_id'),
            'groupable_type' => OjobPosition::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment' , $name , 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        $link = route('index.ticket' , $mainTicket->id);



        $jp->company->notify(new NotificationsRejectJobPosition(
            $mainTicket->subject ,
            $replyTicket->content,
            $link,
        ));

        $jp->save();

        if($jp->type == 'intern'){
            EmployeeLogActivity::addToLog('موقعیت کارآموزی با شناسه '.$request->job_id.' را رد کرد');
            return redirect()->back()->with('success' , 'موقعیت کارآموزی مورد نظر رد شد');
        } else {
            EmployeeLogActivity::addToLog('موقعیت استخدامی با شناسه '.$request->job_id.' را رد کرد');
            return redirect()->back()->with('success' , 'موقعیت استخدامی مورد نظر رد شد');
        }
    }

    public function search(Request $req)
    {
        
        $title = $req->title;

        $type = $req->type;

        $oldData = [
            'job_name' => $req->job_name,
            'co_name' => $req->co_name,
        ];

        $query = OjobPosition::query();

        if($req->type == 'un_intern_position'){
            $query->Intern();
            $query->UnConfirmed();
            $view = 'job.position.emp.unconfirmed-list';
            $config = [
                'title' => 'موقعیت های کارآموزی در انتظار بررسی', 
                'mainPage' => 'emp.intern.pos.job',
                'searchType' => 'un_intern_position',
                'thisRoute' => 'emp.un.intern.pos.job',
                'export' => 'export.emp.un.intern.pos.job',
                'intern' => true,
                'rejectSubject' => 'رد موقعیت کارآموزی',
            ];
        }elseif($req->type == 're_intern_position'){
            $query->Intern();
            $query->Rejected();
            $view = 'job.position.emp.rejected-list';
            $config = [
                'title' => 'موقعیت های کارآموزی رد شده', 
                'mainPage' => 'emp.intern.pos.job',
                'searchType' => 're_intern_position',
                'thisRoute' => 'emp.re.intern.pos.job',
                'export' => 'export.emp.re.intern.pos.job',
                'intern' => true,
            ];
        }elseif($req->type == 'intern_position'){
            $query->Intern();
            $query->Confirmed();
            $view = 'job.position.emp.confirmed-list';
            $config = [
                'title' => 'موقعیت های کارآموزی تایید شده', 
                'mainPage' => 'emp.intern.pos.job',
                'searchType' => 'intern_position',
                'thisRoute' => 'emp.ac.intern.pos.job',
                'export' => 'export.emp.ac.intern.pos.job',
                'intern' => true,
            ];
        }elseif($req->type == 'un_hire_position'){
            $query->Hire();
            $query->UnConfirmed();
            $view = 'job.position.emp.unconfirmed-list';
            $config = [
                'title' => 'موقعیت های استخدامی در انتظار بررسی', 
                'mainPage' => 'emp.hire.pos.job',
                'searchType' => 'un_hire_position',
                'thisRoute' => 'emp.un.hire.pos.job',
                'export' => 'export.emp.un.hire.pos.job',
                'intern' => false,
                'rejectSubject' => 'رد موقعیت استخدامی',
            ];
        }elseif($req->type == 're_hire_position'){
            $query->Hire();
            $query->Rejected();
            $view = 'job.position.emp.rejected-list';
            $config = [
                'title' => 'موقعیت های استخدامی رد شده', 
                'mainPage' => 'emp.hire.pos.job',
                'searchType' => 're_hire_position',
                'thisRoute' => 'emp.re.hire.pos.job',
                'export' => 'export.emp.re.hire.pos.job',
                'intern' => false,
            ];
        }elseif($req->type == 'hire_position'){
            $query->Hire();
            $query->Confirmed();
            $view = 'job.position.emp.confirmed-list';
            $config = [
                'title' => 'موقعیت های استخدامی تایید شده', 
                'mainPage' => 'emp.hire.pos.job',
                'searchType' => 'hire_position',
                'thisRoute' => 'emp.ac.hire.pos.job',
                'export' => 'export.emp.ac.hire.pos.job',
                'intern' => false,
            ];
        }

        if($req->job_name != null || $req->job_name != ''){
            $query->whereRelation('job' , 'title' , 'LIKE' , '%'. $req->job_name .'%');
        }

        if($req->co_name != null || $req->co_name != ''){
            $query->whereRelation('company.groupable' , 'name' , 'LIKE' , '%'. $req->co_name .'%');
        }

        $job_positions = $query->paginate(20)->appends([
            'title' => $req->title,
            'type' => $req->type,
            'job_name' => $req->job_name,
            'co_name' => $req->co_name,
        ]);

        return view($view , compact(
            'job_positions',
            'title',
            'oldData',
            'config',
        ));
    }
}
