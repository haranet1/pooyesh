<?php

namespace App\Http\Controllers\Job\Request;

use App\Http\Controllers\Controller;
use App\Notifications\RejectInternRequestForUni;
use App\Models\Attachment;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\PooyeshRequestProcess;
use App\Models\Ticket;
use App\Models\TicketReply;
use App\Models\User;
use App\Notifications\AcceptedPooyeshRequest;
use App\Notifications\AcceptInternRequest;
use App\Notifications\CancleInternRequest;
use App\Notifications\CancleInternRequestForUni;
use App\Notifications\RejectInternRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StdController extends Controller
{
    public function listOfInternSent()
    {
        $requests = CooperationRequest::Intern()
            ->where('sender_id' , Auth::id())
            ->with('job' , 'receiver.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'درخواست های کارآموزی ارسال شده',
            'type' => 'موقعیت کارآموزی'
        ];

        return view('job.request.std.sent-list' , compact(
            'requests',
            'config',
        ));
    }

    public function listOfHireSent()
    {
        $requests = CooperationRequest::Hire()
            ->where('sender_id' , Auth::id())
            ->with('job' , 'receiver.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);


        $config = [
            'title' => 'درخواست های استخدامی ارسال شده',
            'type' => 'موقعیت استخدامی'
        ];

        return view('job.request.std.sent-list' , compact(
            'requests',
            'config',
        ));
    }

    public function listOfInternReceived()
    {
        $requests = CooperationRequest::Intern()
            ->where('receiver_id' , Auth::id())
            ->with('job' , 'sender.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'درخواست های کارآموزی دریافت شده',
            'type' => 'موقعیت کارآموزی'
        ];

        return view('job.request.std.received-list' , compact(
            'requests',
            'config'
        ));
    }

    public function listOfHireReceived()
    {
        $requests = CooperationRequest::Hire()
            ->where('receiver_id' , Auth::id())
            ->with('job' , 'sender.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'درخواست های استخدامی دریافت شده',
            'type' => 'موقعیت استخدامی'
        ];

        return view('job.request.std.received-list' , compact(
            'requests',
            'config'
        ));
    }

    public function accept(Request $request)
    {
        $req = CooperationRequest::find($request->id);
        $req->status = true;

        if($req->process){
            $req->process->update([
                'accepted_by_std_at' => now(),
            ]);
        }else{
            $process = PooyeshRequestProcess::create([
                'req_id' => $req->id,
                'accepted_by_std_at' => now()
            ]);
        }

        $employee = User::Employee()->get();
        foreach($employee as $emp){
            if($emp->hasPermissionTo('pooyeshRequest')){
                $emp->notify(new AcceptedPooyeshRequest($req->receiver));
            }
        }

        $req->sender->notify(new AcceptInternRequest($req->receiver));

        return response()->json($req->save());
    }

    public function reject(Request $request)
    {
        // dd($request->all());
        $req = CooperationRequest::find($request->request_id);
        $req->status = false;

        if($req->process){
            $req->process->update([
                'rejected_by_std_at' => now(),
            ]);
        } else {
            $process = PooyeshRequestProcess::create([
                'req_id' => $req->id,
                'rejected_by_std_at' => now()
            ]);
        }

        if($req->type == 'intern'){
            $category = 'stdiReq';
        } else {
            $category = 'stdhReq';
        }


        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->input('receiver_id'),
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('request_id'),
            'groupable_type' => CooperationRequest::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        $link = route('co-rejected.emp.list.request.intern');

        $employee = User::Employee()->get();
        foreach($employee as $emp){
            if($emp->hasPermissionTo('pooyeshRequest')){
                $emp->notify(new RejectInternRequestForUni($mainTicket->subject , $replyTicket->content, $link));
            }
        }

        $link = route('index.ticket' , $mainTicket->id);

        $req->sender->notify(new RejectInternRequest($mainTicket->subject , $replyTicket->content , $link));
        $req->save();

        return redirect()->back()->with('success' , 'درخواست همکاری مورد نظر رد شد');
    }

    public function cancle(Request $request)
    {
        $req = CooperationRequest::with('job' , 'process')->find($request->request_id);

        $req->update([
            'status' => 2
        ]);

        if($req->job->type == 'intern'){
            $req->process->update(['canceled_by_std_at' => now()]);
        }

        $receiver = null;

        if($req->sender->groupable_type == CompanyInfo::class){
            $receiver = $req->sender;
        }else{
            $receiver = $req->receiver;
        }

        $category = 'stdCancleIReq';

        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $receiver->id,
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('request_id'),
            'groupable_type' => CooperationRequest::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        if($req->job->type == 'intern'){
            $employees = User::Employee()->get();
            foreach($employees as $emp){
                if($emp->hasPermissionTo('pooyeshRequest')){
                    $emp->notify(new CancleInternRequestForUni(Auth::user() , $req));
                }
            }

            $receiver->notify(new CancleInternRequest($mainTicket->subject , $replyTicket->content , route('index.ticket' , $mainTicket->id)));

            return redirect()->back()->with('success' , 'درخواست کارآموزی مورد نظر لغو شد');
        }

        return redirect()->back()->with('success' , 'درخواست استخدامی مورد نظر لغو شد');
    }
}
