<?php

namespace App\Http\Controllers\Job\Request;

use App\Helpers\CompanyLogActivity;
use App\Http\Controllers\Controller;
use App\Models\ApplicantInfo;
use App\Models\Attachment;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\OjobPosition;
use App\Models\PooyeshRequestProcess;
use App\Models\Ticket;
use App\Models\TicketReply;
use App\Models\User;
use App\Notifications\AcceptedPooyeshRequest;
use App\Notifications\AcceptInternRequest;
use App\Notifications\CancleInternRequest;
use App\Notifications\CancleInternRequestForUni;
use App\Notifications\RejectInternRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoController extends Controller
{
    public function listOfInternReceivedByPos()
    {
        $company = Auth::user();
        $confirmed_request = CooperationRequest::Intern()
            ->where('receiver_id' , Auth::id())
            ->Accepted()->count();
        $notAllowed = null;
        if($confirmed_request >= $company->groupable->confirm_internship_limit){
            $notAllowed = 'notAllowed';
        }

        $positions = OjobPosition::Intern()
            ->NotDeleted()
            ->with('job' , 'requests')
            ->MyJobPositions()
            ->paginate(20);

        $config = [
            'title' => 'درخواست های پویش بر اساس موقعیت کارآموزی',
            'alertDescription' => 'در این صفحه لیست درخواست های کارآموزی(پویش) که برای شما فرستاده شده است نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت کارآموزی',
            'reqByIdLink' => 'co.received-by-id.intern.req.job',
        ];

        return view('job.request.co.received-index' , compact(
            'positions',
            'config',
            'notAllowed'
        ));
    }

    public function coReceivedIntetnById($id)
    {
        $jobName = OjobPosition::where('id' , $id)->first('title');

        $requests = CooperationRequest::Intern()
            ->NotAccepted()
            ->where('job_position_id' , $id)
            ->where('sender_id' , '!=' , Auth::id())
            ->whereRelation('sender' , 'groupable_type' , '!=' , ApplicantInfo::class)
            ->with('job' , 'sender.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'درخواست های پویش موقعیت'.' '. $jobName->title,
            'alertDescription' => 'در این صفحه لیست درخواست های کارآموزی(پویش) که برای شما فرستاده شده است نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت کارآموزی'
        ];

        return view('job.request.co.received-list' , compact(
            'requests',
            'config'
        ));
    }

    public function listOfInternSentByPos()
    {
        $company = Auth::user();
        $confirmed_request = CooperationRequest::Intern()
            ->where('sender_id' , Auth::id())
            ->Accepted()->get()->count();
        $notAllowed = null;
        if($confirmed_request >= $company->groupable->confirm_internship_limit){
            $notAllowed = 'notAllowed';
        }
        $positions = OjobPosition::Intern()
            ->MyJobPositions()
            ->with('job' , 'requests', 'requestProcess')
            ->whereRelation('requests' , 'sender_id' , Auth::id())
            ->paginate(20);

        $config = [
            'title' => 'درخواست های پویش من براساس موقعیت کارآموزی',
            'alertDescription' => 'در این صفحه لیست درخواست های کارآموزی (پویش) که شما به دانشجویان فرستاده اید نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت کارآموزی',
            'reqByIdLink' => 'co.sent-by-id.intern.req.job',
        ];

        return view('job.request.co.sent-index' , compact(
            'positions',
            'config',
            'notAllowed'
        ));
    }

    public function coSentInternById($id)
    {
        $jobName = OjobPosition::where('id' , $id)->first('title');

        $requests = CooperationRequest::Intern()
            ->NotAccepted()
            ->MyRequests()
            ->where('job_position_id' , $id)
            ->with('job' , 'receiver.groupable' , 'process')
            ->whereRelation('process', 'canceled_by_co_at' , null)
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'درخواست های پویش من به موقعیت کارآموزی'.' '. $jobName->title,
            'alertDescription' => 'در این صفحه لیست درخواست های کارآموزی (پویش) که شما به دانشجویان فرستاده اید نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت کارآموزی'
        ];

        return view('job.request.co.sent-list' , compact(
            'requests',
            'config',
        ));
    }

    public function newInternByPos()
    {
        
        $receiverPositions = OjobPosition::Intern()
            ->NotDeleted()
            ->MyJobPositions()
            ->with(['job.category.photo', 'requests' => function ($query) {
                $query->where('receiver_id' , Auth::id())
                    ->whereNull('status');
            }])
            ->whereHas('requests', function ($query) {
                $query->where('receiver_id' , Auth::id())
                    ->whereNull('status');
            })
            ->paginate(30);

        $senderPositions = OjobPosition::Intern()
            ->NotDeleted()
            ->MyJobPositions()
            ->with(['job.category.photo', 'requests' => function ($query) {
                $query->where('sender_id' , Auth::id())
                    ->whereNull('status');
            }])
            ->whereHas('requests', function ($query) {
                $query->where('sender_id' , Auth::id())
                    ->whereNull('status');
            })
            ->paginate(30);


        $config = [
            'title' => 'درخواست های پویش جدید براساس موقعیت کارآموزی',
            'alertDescription' => 'در این صفحه لیست درخواست های کارآموزی (پویش) جدید نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت کارآموزی',
            'reqByIdLink' => 'co.sent-by-id.intern.req.job',
        ];

        return view('job.request.co.new-index' , compact(
            'receiverPositions',
            'senderPositions',
            'config',
        ));
    }

    public function coAcceptedIntern()
    {
        $requests = CooperationRequest::Intern()
            ->where('sender_id' , Auth::id())
            ->orWhere('receiver_id' , Auth::id())
            ->Accepted()
            ->with('job' , 'sender.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $activeRequests = CooperationRequest
            ::where(function ($query) {
                $query->Intern()
                    ->where('sender_id', Auth::id())
                    ->where('status', 1);
            })
            ->orWhere(function ($query) {
                $query->Intern()
                    ->where('receiver_id', Auth::id())
                    ->where('status', 1);
            })
            ->with('job', 'sender.groupable', 'receiver.groupable', 'process')
            ->orderByDesc('created_at')
            ->paginate(20);
            
        $canceldRequests = CooperationRequest
            ::where(function ($query) {
                $query->Intern()
                    ->where('sender_id', Auth::id())
                    ->where('status', 2);
            })
            ->orWhere(function ($query) {
                $query->Intern()
                    ->where('receiver_id', Auth::id())
                    ->where('status', 2);
            })
            ->with('job', 'sender.groupable', 'receiver.groupable', 'process')
            ->orderByDesc('created_at')
            ->paginate(20);
            
        $rejecedRequests = CooperationRequest
            ::where(function ($query) {
                $query->Intern()
                    ->where('sender_id', Auth::id())
                    ->where('status', 0);
            })
            ->orWhere(function ($query) {
                $query->Intern()
                    ->where('receiver_id', Auth::id())
                    ->where('status', 0);
            })
            ->with('job', 'sender.groupable', 'receiver.groupable', 'process')
            ->orderByDesc('created_at')
            ->paginate(20);


        $config = [
            'title' => 'سابقه درخواست های پویش',
            'alertDescription' => 'در این صفحه لیست درخواست های کارآموزی (پویش) مرتبط با شما به تفکیک وضعیت هر درخواست نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت کارآموزی',
        ];

        return view('job.request.co.accepted-list' , compact(
            'requests',
            'config',
            'activeRequests',
            'canceldRequests',
            'rejecedRequests'
        ));
    }

    public function listOfHireReceivedByPos()
    {
        $positions = OjobPosition::Hire()
            ->MyJobPositions()
            ->with('job' , 'requests')
            ->paginate(20);

        $notAllowed = null;

        $config = [
            'title' => 'درخواست های استخدام بر اساس موقعیت شغلی',
            'alertDescription' => 'در این صفحه لیست درخواست های استخدامی که برای شما فرستاده شده است نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت استخدامی',
            'reqByIdLink' => 'co.received-by-id.hire.req.job',
        ];

        return view('job.request.co.received-index' , compact(
            'config',
            'positions',
            'notAllowed'
        ));
    }

    public function coReceivedHireById($id)
    {
        $jobName = OjobPosition::where('id' , $id)->first('title');
        $requests = CooperationRequest::Hire()
            ->NotAccepted()
            ->where('job_position_id' , $id)
            ->where('sender_id' , '!=' , Auth::id())
            ->with('job' , 'sender.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'درخواست های استخدام موقعیت شغلی'.' '. $jobName->title,
            'alertDescription' => 'در این صفحه لیست درخواست های استخدامی که برای شما فرستاده شده است نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت استخدامی',
        ];

        return view('job.request.co.received-list' , compact(
            'requests',
            'config',
        ));
    }

    public function listOfHireSentByPos()
    {
        $positions = OjobPosition::Hire()
            ->MyJobPositions()
            ->with('job' , 'requests')
            ->whereRelation('requests' , 'sender_id' , Auth::id())
            ->paginate(20);

        // dd($positions);

        $notAllowed = null;

        $config = [
            'title' => 'درخواست های استخدام من بر اساس موقعیت شغلی',
            'alertDescription' => 'در این صفحه لیست درخواست های استخدامی که شما به کارجویان فرستاده اید نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت استخدامی',
            'reqByIdLink' => 'co.sent-by-id.hire.req.job',
        
        ];

        return view('job.request.co.sent-index' , compact(
            'config',
            'positions',
            'notAllowed'
        ));
    }

    public function coSentHireById($id)
    {
        $jobName = OjobPosition::where('id' , $id)->first('title');
        $requests = CooperationRequest::Hire()
            ->NotAccepted()
            ->MyRequests()
            ->where('job_position_id' , $id)
            ->with('job' , 'sender.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'title' => 'درخواست های استخدام من به موقعیت شغلی'.' '. $jobName->title,
            'alertDescription' => 'در این صفحه لیست درخواست های استخدامی که شما به کارجویان فرستاده اید نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت استخدامی',
        ];

        return view('job.request.co.sent-list' , compact(
            'requests',
            'config',
        ));
    }

    public function newHireByPos()
    {
        $receiverPositions = OjobPosition::Hire()
            ->NotDeleted()
            ->MyJobPositions()
            ->with(['job', 'requests' => function ($query) {
                $query->where('receiver_id' , Auth::id())
                    ->whereNull('status');
            }])
            ->whereHas('requests', function ($query) {
                $query->where('receiver_id' , Auth::id())
                    ->whereNull('status');
            })
            ->paginate(30);

        $senderPositions = OjobPosition::Hire()
            ->NotDeleted()
            ->MyJobPositions()
            ->with(['job', 'requests' => function ($query) {
                $query->where('sender_id' , Auth::id())
                    ->whereNull('status');
            }])
            ->whereHas('requests', function ($query) {
                $query->where('sender_id' , Auth::id())
                    ->whereNull('status');
            })
            ->paginate(30);


        $config = [
            'title' => 'درخواست های استخدامی جدید براساس موقعیت کارآموزی',
            'alertDescription' => 'در این صفحه لیست درخواست های استخدامی جدید نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت کارآموزی',
            'reqByIdLink' => 'co.sent-by-id.hire.req.job',
        ];

        return view('job.request.co.new-index' , compact(
            'receiverPositions',
            'senderPositions',
            'config',
        ));
    }

    public function coAcceptedHire()
    {
        $requests = CooperationRequest::Hire()
            ->Accepted()
            ->where('sender_id' , Auth::id())
            ->orWhere('receiver_id' , Auth::id())
            ->with('job' , 'sender.groupable')
            ->orderByDesc('created_at')
            ->paginate(20);

        $activeRequests = CooperationRequest
            ::where(function ($query) {
                $query->Hire()
                    ->where('sender_id', Auth::id())
                    ->where('status', 1);
            })
            ->orWhere(function ($query) {
                $query->Hire()
                    ->where('receiver_id', Auth::id())
                    ->where('status', 1);
            })
            ->with('job', 'sender.groupable', 'receiver.groupable', 'process')
            ->orderByDesc('created_at')
            ->paginate(20);
            
        $canceldRequests = CooperationRequest
            ::where(function ($query) {
                $query->Hire()
                    ->where('sender_id', Auth::id())
                    ->where('status', 2);
            })
            ->orWhere(function ($query) {
                $query->Hire()
                    ->where('receiver_id', Auth::id())
                    ->where('status', 2);
            })
            ->with('job', 'sender.groupable', 'receiver.groupable', 'process')
            ->orderByDesc('created_at')
            ->paginate(20);
            
        $rejecedRequests = CooperationRequest
            ::where(function ($query) {
                $query->Hire()
                    ->where('sender_id', Auth::id())
                    ->where('status', 0);
            })
            ->orWhere(function ($query) {
                $query->Hire()
                    ->where('receiver_id', Auth::id())
                    ->where('status', 0);
            })
            ->with('job', 'sender.groupable', 'receiver.groupable', 'process')
            ->orderByDesc('created_at')
            ->paginate(20);


        $config = [
            'title' => 'سابقه درخواست های استخدامی',
            'alertDescription' => 'در این صفحه لیست درخواست های استخدامی مرتبط با شما به تفکیک وضعیت هر درخواست نمایش داده میشود.',
            'jobTypeTitle' => 'موقعیت استخدامی',
        ];

        return view('job.request.co.accepted-list' , compact(
            'requests',
            'config',
            'activeRequests',
            'canceldRequests',
            'rejecedRequests'
        ));
    }

    public function accept(Request $request)
    {
        $req = CooperationRequest::find($request->id);
        $company = Auth::user();
        if($req->type == 'intern'){
            $confirmed_request = CooperationRequest::Intern()
                ->where('receiver_id' , Auth::id())
                ->Accepted()->get()->count();
            if($confirmed_request >= $company->groupable->confirm_internship_limit){
                return response()->json(['text' => 'بیش از حد مجاز درخواست کارآموزی تایید شده دارید.'], 400);
            }
        } else {
            $confirmed_request = CooperationRequest::Hire()
                ->where('receiver_id' , Auth::id())
                ->Accepted()->get()->count();
            if($confirmed_request >= $company->groupable->confirm_hire_limit){
                return response()->json(['text' => 'بیش از حد مجاز درخواست استخدامی تایید شده دارید.'], 400);
            }
        }




        $employees = User::Employee()->get();

        $req->status = 1;
        foreach($employees as $employee){
            if($employee->hasPermissionTo('pooyeshRequest')){
                $employee->notify(new AcceptedPooyeshRequest($req->receiver));
            }
        }
        if ($req->process) {
            $req->process->accepted_by_co_at = now();
            $req->process->save();
        } else {
            $process = PooyeshRequestProcess::create([
                'req_id' => $req->id,
                'accepted_by_co_at' => now()
            ]);
        }
        $req->sender->notify(new AcceptInternRequest($req->receiver));
        CompanyLogActivity::addToLog('یک درخواست پویش را تایید کرد');

        $req->save();
        return response()->json('success');
    }

    public function reject(Request $request)
    {
        $req = CooperationRequest::find($request->request_id);
        $req->status = 0;

        if($req->process){
            $req->process->rejected_by_co_at = now();
            $req->process->save();
        } else {
            $process = PooyeshRequestProcess::create([
                'req_id' => $req->id,
                'rejected_by_co_at' => now()
            ]);
        }

        $category = 'coiReq';

        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->input('receiver_id'),
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('request_id'),
            'groupable_type' => CooperationRequest::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }


        $link = route('index.ticket' , $mainTicket->id);

        $req->sender->notify(new RejectInternRequest($mainTicket->subject , $replyTicket->content , $link));
        $req->save();

        return redirect()->back()->with('success' , 'درخواست کارآموزی مورد نظر رد شد');
    }

    public function cancel(Request $request)
    {
        $req = CooperationRequest::with('job' , 'process')->find($request->request_id);

        $req->update([
            'status' => 2
        ]);

        if($req->job->type == 'intern'){
            $req->process->update(['canceled_by_co_at' => now()]);

            $receiver = null;

            if($req->sender->groupable_type == CompanyInfo::class){
                $receiver = $req->receiver;
            }else{
                $receiver = $req->sender;
            }

            $category = 'coCancleIReq';

            $mainTicket = Ticket::create([
                'category' => $category,
                'sender_id' => Auth::id(),
                'receiver_id' => $receiver->id,
                'subject' => $request->input('subject'),
                'status' => 'send',
                'groupable_id' => $request->input('request_id'),
                'groupable_type' => CooperationRequest::class,
            ]);

            $replyTicket = TicketReply::create([
                'parent_id' => $mainTicket->id,
                'sender_id' => Auth::id(),
                'content' => $request->input('content'),
            ]);

            if($request->file('attachment')){
                $name = time() . $request->file('attachment')->getClientOriginalName();
                $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

                $attachment = Attachment::create([
                    'name' => $name,
                    'format' => $request->file('attachment')->extension(),
                    'path' => $path,
                ]);

                $replyTicket->attachment_id = $attachment->id;
                $replyTicket->save();
            }

            $employees = User::Employee()->get();
            foreach($employees as $emp){
                if($emp->hasPermissionTo('pooyeshRequest')){
                    $emp->notify(new CancleInternRequestForUni(Auth::user() , $req));
                }
            }

            $receiver->notify(new CancleInternRequest($mainTicket->subject , $replyTicket->content , route('index.ticket' , $mainTicket->id)));

            return redirect()->back()->with('success' , 'درخواست کارآموزی مورد نظر لغو شد');
        }

        return response()->json(true);
    }
}
