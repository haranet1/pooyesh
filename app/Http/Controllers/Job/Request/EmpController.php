<?php

namespace App\Http\Controllers\Job\Request;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\OjobPosition;
use App\Models\StudentInfo;
use App\Models\Ticket;
use App\Models\TicketReply;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmpController extends Controller
{
    public function index()
    {
        $title = 'صفحه اصلی درخواست های پویش';

        $reqCount = $this->getPooyeshRequestStatus();

        $data = [ 
            'stdHasRequestToCo' => User::InternRequestSender()
                ->Student()
                ->count(),
            'acceptedStdReq' => $reqCount['acceptedStdReq'],
            'canceledStdReq' => $reqCount['canceledStdReq'],
            'rejectedStdReq' => $reqCount['rejectedStdReq'],

            'coHasRequestToStd' => User::InternRequestSender()
                ->Company()
                ->count(),
            'acceptedCoReq' => $reqCount['acceptedCoReq'],
            'canceledCoReq' => $reqCount['canceledCoReq'],
            'rejectedCoReq' => $reqCount['rejectedCoReq'],
            
            'acceptedByUni' => $reqCount['acceptedByUni'],
            'rejectedByUni' => $reqCount['rejectedByUni'],
            'contractPrinted' => $reqCount['contractPrinted'],
            'done' => $reqCount['done'],
        ];

        return view('job.request.emp.index' , compact(
            'title',
            'data',
        ));
    }
    /**
     * get pooyesh status count and return array
     */
    public function getPooyeshRequestStatus()
    {
        $requests = CooperationRequest::Intern()->whereHas('process')
            ->whereHas('sender' , function ($q) {
                $q->where('groupable_type' , StudentInfo::class)
                ->orWhere('groupable_type' , CompanyInfo::class);
            })
            ->with('process')
            ->get();

        $data = [
            'acceptedStdReq' => 0,
            'acceptedCoReq' => 0,
            'canceledStdReq' => 0,
            'canceledCoReq' => 0,
            'rejectedStdReq' => 0,
            'rejectedCoReq' => 0,
            'rejectedByUni' => 0,
            'acceptedByUni' => 0,
            'contractPrinted' => 0,
            'done' => 0,
        ];

        foreach($requests as $request){
            $status = $request->process->getMostRecentlyUpdatedColumn();
            switch ($status) {
                case 'accepted_by_co_at':
                    $data['acceptedStdReq']++;
                    break;
                case 'accepted_by_std_at':
                    $data['acceptedCoReq']++;
                    break;
                case 'canceled_by_co_at':
                    $data['canceledStdReq']++;
                    break;
                case 'canceled_by_std_at':
                    $data['canceledCoReq']++;
                    break;
                case 'rejected_by_co_at':
                    $data['rejectedStdReq']++;
                    break;
                case 'rejected_by_std_at':
                    $data['rejectedCoReq']++;
                    break;
                case 'rejected_by_uni_at':
                    $data['rejectedByUni']++;
                    break;
                case 'accepted_by_uni_at':
                    $data['acceptedByUni']++;
                    break;
                case 'contract_printed_at':
                    $data['contractPrinted']++;
                    break;
                case 'done_at':
                    $data['done']++;
                    break;
            }
        }

        return $data;
    }
    /**
     * list of students has intern request
     */
    public function stdRequests()
    {
        $title = 'دانشجویان درخواست داده';

        $users = User::InternRequestSender()
            ->Student()
            ->with('sendRequest.receiver.groupable','sendRequest.job')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.std-requests' , compact(
            'title',
            'users',
        ));
    }
    /**
     * list of companies has intern request
     */
    public function coRequests()
    {
        $title = 'شرکت های درخواست داده';

        $users = User::InternRequestSender()
            ->Company()
            ->with('sendRequest.receiver' , 'sendRequest.job')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.co-requests' , compact(
            'title',
            'users',
        ));
    }
    /**
     * list of request when company accept student's request
     */
    public function stdAccepted()
    {
        $title = 'درخواست های تایید شده دانشجو';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()
            ->whereHas('process')
            ->whereRelation('sender' , 'groupable_type' , StudentInfo::class)
            ->with('process')
            ->get();

        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'accepted_by_co_at'){
                array_push($ids , $request->id);
            }
        }

        $requests = CooperationRequest::whereIn('id' ,$ids)
            ->with('sender' , 'receiver.groupable' , 'process' , 'job')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.std-accepted' , compact(
            'title',
            'requests',
        ));
    }
    /**
     * list of request when student accept company's request
     */
    public function coAccepted()
    {
        $title = 'درخواست های تایید شده شرکت';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'accepted_by_std_at'){
                array_push($ids , $request->id);
            }
        }
        $requests = CooperationRequest::whereIn('id' , $ids)
            ->with('sender.groupable','receiver','process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.co-accepted' , compact(
            'title',
            'requests',
        ));
    }
    /**
     * list of request when university accept any requests
     */
    public function uniAccepted()
    {
        $title = 'تایید شده توسط دانشگاه';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()
            ->whereHas('sender' , function ($q) {
                $q->where('groupable_type' , StudentInfo::class)
                ->orWhere('groupable_type' , CompanyInfo::class);
            })
            ->whereHas('process')
            ->with('process')
            ->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'accepted_by_uni_at'){
                array_push($ids , $request->id);
            }
        }
        $requests = CooperationRequest::whereIn('id' , $ids)
            ->with('sender.groupable','receiver','process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        // dd($requests);

        return view('job.request.emp.lists.uni-accepted' , compact(
            'title',
            'requests',
        ));
    }
    /**
     * list of request when company cancel student's request
     */
    public function stdCanceled()
    {
        $title = 'درخواست های لغو شده دانشجو ';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'canceled_by_co_at'){
                array_push($ids , $request->id);
            }
        }
        $requests = CooperationRequest::whereIn('id' ,$ids)
            ->with('sender' , 'receiver.groupable' , 'process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.std-canceled' , compact(
            'title',
            'requests',
        ));
    }
    /**
     * list of request when student cancel company's request
     */
    public function coCanceled()
    {
        $title = 'درخواست های لغو شده شرکت ';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'canceled_by_std_at'){
                array_push($ids , $request->id);
            }
        }
        $requests = CooperationRequest::whereIn('id' , $ids)
            ->with('sender.groupable','receiver','process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.co-canceled' , compact(
            'title',
            'requests',
        ));
    }
    /**
     * list of request when company reject student's request
     */
    public function stdRejected()
    {
        $title = 'درخواست های رد شده دانشجو';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'rejected_by_co_at'){
                array_push($ids , $request->id);
            }
        }
        $requests = CooperationRequest::whereIn('id' ,$ids)
            ->with('sender' , 'receiver.groupable' , 'process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.std-rejected' , compact(
            'title',
            'requests',
        ));
    }
    /**
     * list of request when student reject company's request
     */
    public function coRejected()
    {
        $title = 'درخواست های رد شده شرکت';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'rejected_by_std_at'){
                array_push($ids , $request->id);
            }
        }
        $requests = CooperationRequest::whereIn('id' , $ids)
            ->with('sender.groupable','receiver','process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.co-rejected' , compact(
            'title',
            'requests',
        ));
    }
    /**
     * list of requst when university reject any request
     */
    public function uniRejected()
    {
        $title = 'رد شده توسط دانشگاه';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'rejected_by_uni_at'){
                array_push($ids , $request->id);
            }
        }
        $requests = CooperationRequest::whereIn('id' , $ids)
            ->with('sender.groupable','receiver','process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.uni-rejected' , compact(
            'title',
            'requests',
        ));
    }
    /**
     * list of request when university print any request
     */
    public function uniPrinted()
    {
        $title = 'چاپ شده توسط دبیرخانه';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'contract_printed_at'){
                array_push($ids , $request->id);
            }
        }
        $requests = CooperationRequest::whereIn('id' , $ids)
            ->with('sender.groupable','receiver','process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.uni-printed' , compact(
            'title',
            'requests',
        ));
    }
    /**
     * list of request when student or company done pooyesh process
     */
    public function done()
    {
        $title = 'لیست درخواست های پویش به پایان رسیده';
        $ids = array();

        $cooperationRequests = CooperationRequest::Intern()->whereHas('process')->with('process')->get();
        foreach($cooperationRequests as $request){
            if($request->process->getMostRecentlyUpdatedColumn() == 'done_at'){
                array_push($ids , $request->id);
            }
        }
        $requests = CooperationRequest::whereIn('id' , $ids)
            ->with('sender.groupable','receiver','process')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('job.request.emp.lists.done' , compact(
            'title',
            'requests',
        ));
    }

    public function hireIndex()
    {
        $title = 'لیست درخواست های استخدام';
        
        $requests = CooperationRequest::Hire()
            ->with('sender.groupable','receiver','process')
            ->orderByDesc('updated_at')
            ->paginate(20);


        // dd($requests);

        return view('job.request.emp.hire-index' , compact(
            'title',
            'requests',
        ));
    }

    public function acceptHire(Request $request)
    {
        $req = CooperationRequest::find($request->id);
        $req->update([
            'status' => 1
        ]);

        return response()->json(true);
    }

    public function rejectHire(Request $request)
    {
        $req = CooperationRequest::find($request->request_id);

        $category = 'hReq';

        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->input('receiver_id'),
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('request_id'),
            'groupable_type' => CooperationRequest::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment' , $name , 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        $req->update([
            'status' => 0
        ]);

        return redirect()->back()->with('success' , 'درخواست استخدام مورد نظر رد شد');
    }
}
