<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasPermissions;

class RoleAssignController extends Controller
{
    use HasPermissions;
    
    public function createRoleShow()
    {
        $permissions = Permission::all();
        return view('panel.employee.create-role', compact('permissions'));
    }

    public function storeRole(Request $request)
    {
        $this->validateRole($request);
        $role = $this->createRole($request);
        $roleHasPermission = $this->givePermissionToRole($request , $role);
        return redirect(route('employee.role-create'))->with('success', 'نقش '. $role->name .' با موفقیت ثبت شد');
    }

    public function validateRole($data)
    {
        $data->validate([
            'name' => 'required|unique:permissions,name',
        ],[
            'name.required' => 'نام نقش را وارد کنید',
            'name.unique' => 'نقش مورد نظر قبلا ثبت شده است',
        ]);
    }

    public function createRole($request)
    {
        $role = Role::create(['name' => $request->name , 'guard_name' => 'web']);
        return $role;
    }

    public function givePermissionToRole($request , $role)
    {
        $data = $request->all();
        unset($data['_token']);
        unset($data['name']);

        foreach($data as $key => $value){
            $role->givePermissionTo($value);
        }
    }

    public function assignRoleShow()
    {
        $users = User::Employee()->get();
        $oldroles = Role::whereNotNull('created_at')->get();
        $roles = [];
        foreach($oldroles as $oldrole){
            $roles[$oldrole->id] = $oldrole->name;
        }
        return view('panel.employee.assign-role-user' , compact('users' , 'roles'));
    }

    public function assignRole(Request $request)
    {
        $this->validateAssignRole($request);
        $user = User::Employee()->where('id' , $request->user)->first();
        $role = Role::where('id', $request->role)->first();
        $user->assignRole($role->name);
        return redirect(route('employee.show.role.assign'))->with('success', 'نقش '. $role->name .' با موفقیت به کاربر '. $user->name .' '. $user->family .' انتصاب داده شد ');
    }

    public function validateAssignRole($request)
    {
        $request->validate([
            'role' => 'required'
        ],[
            'role.required' => 'نقش مورد نظر خود را وارد کنید',
        ]);
    }

    public function removeRole(Request $request)
    {
        // dd($request->all());
        $this->validateRemoveRole($request);
        $user = User::Employee()->where('id' , $request->user)->first();
        // $deletedRole = Role::where('id', $request->deleteRole)->first();
        $user->removeRole($request->deleteRole);
        return redirect(route('employee.show.role.assign'))->with('success', 'نقش '. $request->deleteRole .' با موفقیت از کاربر '. $user->name .' '. $user->family .' حذف شد ');
    }

    public function validateRemoveRole($request)
    {
        $request->validate([
            'deleteRole' => 'required'
        ],[
            'deleteRole.required' => 'نقش مورد نظر خود را وارد کنید',
        ]);
    }

    public function editRole()
    {
        $roles = Role::where('name' , '!=' , 'employee')
        ->where('name', '!=' , 'company')
        ->where('name', '!=' , 'student')
        ->where('name', '!=' , 'applicant')
        ->get();

        $permissions = Permission::all();

        // foreach($roles as $role){
        //     dump($role->getPermissionNames());
        // }
        // dd('hi');
        return view('panel.employee.edit-role' , compact('roles' , 'permissions'));
    }

    public function updateRole(Request $request)
    {
        $request->validate([
            'permission' => 'required',
        ],[
            'permission.required' => 'دسترسی مورد نظر خود را وارد کنید',
        ]);

        $role = Role::find($request->role);
        $per = Permission::find($request->permission);
        $role->givePermissionTo($per);
        return redirect()->back()->with('success',' دسترسی ' . $per->name . ' با موفقیت به نقش '. $role->name .' اضافه شد ');
    }

    public function deletePermission(Request $request)
    {
        $request->validate([
            'permission' => 'required',
        ],[
            'permission.required' => 'دسترسی مورد نظر خود را وارد کنید',
        ]);
        $role = Role::find($request->role);
        $per = Permission::find($request->permission);
        $role->revokePermissionTo($per);
        return redirect()->back()->with('success',' دسترسی ' . $per->name . ' با موفقیت به نقش '. $role->name .' حذف شد ');
    }
}
