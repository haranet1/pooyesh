<?php

namespace App\Http\Controllers\Intern\Request;

use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\StudentInfo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OptionController extends Controller
{
    public function userSearch(Request $req)
    {
        $title = $req->title;

        $type = $req->type;

        $oldData = [
            'std' => $req->std,
            'co' => $req->co,
            'job' => $req->job,
        ];

        $query = User::query();
        $query = $query->InternRequestSender2();

        if($type == 'std'){
            $query = $query->Student();
            $view = 'job.request.emp.lists.std-requests';
        }elseif($type == 'co'){
            $query = $query->Company();
            $view = 'job.request.emp.lists.co-requests';
        }

        if($req->std != null || $req->std != ''){
            $query->where( function ($q) use ($req) {
                $q->where('name', 'LIKE', '%'.$req->std.'%')
                  ->orWhere('family', 'LIKE', '%'.$req->std.'%')
                  ->orWhere(DB::raw("CONCAT(name, ' ', family)"), 'LIKE', '%'.$req->std.'%');
            });
        }

        if($req->co != null || $req->co != ''){
            $query->whereRelation('groupable' , 'name' , 'LIKE' , '%'.$req->co.'%');
        }

        if($req->job != null || $req->job != ''){
            $query->whereRelation('job_positions' , 'title' , 'LIKE' , '%'.$req->job.'%');
        }

        $users = $query->orderByDesc('updated_at')
            ->with('sendRequest.receiver.groupable','sendRequest.job')
            ->paginate(20)->appends([
                'title' => $req->title,
                'type' => $req->type,
                'std' => $req->std,
                'co' => $req->co,
                'job' => $req->job,
            ]);

        return view($view , compact(
            'title',
            'users',
            'oldData'
        ));
    }

    public function search(Request $req)
    {
        $title = $req->title;

        $type = $req->type;
        
        $student = $req->student;
        $company = $req->company;

        $oldData = [
            'std' => $req->std,
            'co' => $req->co,
            'job' => $req->job,
        ];

        $cooperationRequests = CooperationRequest::Intern()
            ->whereHas('process')
            ->get();

        $arr = $this->getTypeRequest($req->type);
        
        $ids = $this->getRequestProcess($cooperationRequests , $arr['type']);

        $query = CooperationRequest::query();
        $query = $query->whereIn('id' , $ids);

        if($req->std != null || $req->std != ''){
            $query->whereHas($student, function ($q) use ($req) {
                $q->where('name', 'LIKE', '%'.$req->std.'%')
                  ->orWhere('family', 'LIKE', '%'.$req->std.'%')
                  ->orWhere(DB::raw("CONCAT(name, ' ', family)"), 'LIKE', '%'.$req->std.'%');
            });
        }

        if($req->co != null || $req->co != ''){
            $query->whereRelation($company .'.'. 'groupable' , 'name' , 'LIKE' , '%'.$req->co.'%');
        }

        if($req->job != null || $req->job != ''){
            $query->whereRelation('job' , 'title' , 'LIKE' , '%'.$req->job.'%');
        }


        $requests = $query->orderByDesc('updated_at')
            ->paginate(20)
            ->appends([
                'title' => $req->title,
                'type' => $req->type,
                'student' => $req->student,
                'company' => $req->company,
                'std' => $req->std,
                'co' => $req->co,
                'job' => $req->job,
            ]);

        return view($arr['view'] , compact(
            'title',
            'requests',
            'oldData'
        ));
    }

    public function uniSearch(Request $req)
    {
        // dd($req->all());
        $title = $req->title;

        $type = $req->type;
        
        $student = $req->student;
        $company = $req->company;


        $oldData = [
            'std' => $req->std,
            'co' => $req->co,
            'job' => $req->job,
        ];

        $cooperationRequests = CooperationRequest::Intern()
            ->whereHas('process')
            ->get();

        $arr = $this->getTypeRequest($req->type);

        $ids = $this->getRequestProcess($cooperationRequests , $arr['type']);


        $query = CooperationRequest::query();
        $query = $query->whereIn('id', $ids);
        $query = $query->whereRelation('sender' , 'groupable_type' , StudentInfo::class);

        $query = $this->uniFiler($req ,$query);

        $query2 = CooperationRequest::query();
        $query2 = $query2->whereIn('id', $ids);
        $query2 = $query2->whereRelation('sender' , 'groupable_type' , CompanyInfo::class);

        $query2 = $this->uniFiler($req ,$query2);


        $combinedQuery = $query->union($query2);


        $requests = $combinedQuery->orderByDesc('updated_at')
            ->paginate(20)
            ->appends([
                'title' => $req->title,
                'type' => $req->type,
                'student' => $req->student,
                'company' => $req->company,
                'std' => $req->std,
                'co' => $req->co,
                'job' => $req->job,
            ]);

        return view($arr['view'] , compact(
            'title',
            'requests',
            'oldData'
        ));
    }

    public function uniHireSearch(Request $req)
    {
        // dd($req->all());
        $title = $req->title;

        $type = $req->type;
        
        $student = $req->student;
        $company = $req->company;


        $oldData = [
            'std' => $req->std,
            'co' => $req->co,
            'job' => $req->job,
        ];


        $query = CooperationRequest::query();
        $query = $query->Hire();
        $query = $query->whereRelation('sender' , 'groupable_type' , StudentInfo::class);

        $query = $this->uniFiler($req ,$query);

        $query2 = CooperationRequest::query();
        $query2 = $query2->Hire();
        $query2 = $query2->whereRelation('sender' , 'groupable_type' , CompanyInfo::class);

        $query2 = $this->uniFiler($req ,$query2);


        $combinedQuery = $query->union($query2);


        $requests = $combinedQuery->orderByDesc('updated_at')
            ->paginate(20)
            ->appends([
                'title' => $req->title,
                'type' => $req->type,
                'student' => $req->student,
                'company' => $req->company,
                'std' => $req->std,
                'co' => $req->co,
                'job' => $req->job,
            ]);

        return view('job.request.emp.hire-index' , compact(
            'title',
            'requests',
            'oldData'
        ));
    }

    private function getTypeRequest($type)
    {
        $arr = array();

        switch($type){
            case 'stdAc':
                $arr['type'] = 'accepted_by_co_at';
                $arr['view'] = 'job.request.emp.lists.std-accepted';
                break;
            case 'coAc':
                $arr['type'] = 'accepted_by_std_at';
                $arr['view'] = 'job.request.emp.lists.co-accepted';
                break;
            case 'uniAc':
                $arr['type'] = 'accepted_by_uni_at';
                $arr['view'] = 'job.request.emp.lists.uni-accepted';
                break;
            case 'stdCa':
                $arr['type'] = 'canceled_by_co_at';
                $arr['view'] = 'job.request.emp.lists.std-canceled';
                break;
            case 'coCa':
                $arr['type'] = 'canceled_by_std_at';
                $arr['view'] = 'job.request.emp.lists.co-canceled';
                break;
            case 'stdRe':
                $arr['type'] = 'rejected_by_co_at';
                $arr['view'] = 'job.request.emp.lists.std-rejected';
                break;
            case 'coRe':
                $arr['type'] = 'rejected_by_std_at';
                $arr['view'] = 'job.request.emp.lists.co-rejected';
                break;
            case 'uniRe':
                $arr['type'] = 'rejected_by_uni_at';
                $arr['view'] = 'job.request.emp.lists.uni-rejected';
                break;
            case 'uniPr':
                $arr['type'] = 'contract_printed_at';
                $arr['view'] = 'job.request.emp.lists.uni-printed';
                break;
            case 'uniPr':
                $arr['type'] = 'done_at';
                $arr['view'] = 'job.request.emp.lists.done';
                break;
        }

        return $arr;
    }

    private function getRequestProcess($requests , $type)
    {
        $ids = array();
        foreach($requests as $req){
            if($req->process->getMostRecentlyUpdatedColumn() == $type){
                array_push($ids , $req->id);
            }
        }

        return $ids;
    }

    private function uniFiler($req , $query)
    {
        if($req->std != null || $req->std != ''){
            $query->whereHas('sender', function ($q) use ($req) {
                $q->where('name', 'LIKE', '%'.$req->std.'%')
                  ->orWhere('family', 'LIKE', '%'.$req->std.'%')
                  ->orWhere(DB::raw("CONCAT(name, ' ', family)"), 'LIKE', '%'.$req->std.'%');
            });
        }

        if($req->co != null || $req->co != ''){
            $query->whereRelation('receiver' .'.'. 'groupable' , 'name' , 'LIKE' , '%'.$req->co.'%');
        }

        if($req->job != null || $req->job != ''){
            $query->whereRelation('job' , 'title' , 'LIKE' , '%'.$req->job.'%');
        }

        return $query;
    }
}
