<?php

namespace App\Http\Controllers\Sadra;

use App\Events\SadraEventCreated;
use App\Helpers\CompanyLogActivity;
use App\Helpers\EmployeeLogActivity;
use App\Http\Controllers\Controller;
use App\Models\BuyPlan;
use App\Models\City;
use App\Models\company_sadra;
use App\Models\CompanyInfo;
use App\Models\CompanySadraMajor;
use App\Models\OjobPosition;
use App\Models\Photo;
use App\Models\Province;
use App\Models\Sadra;
use App\Models\SadraBooth;
use App\Models\User;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Morilog\Jalali\Jalalian;

class SadraController extends Controller
{
    public function index()
    {
        $events = Sadra::with('buy_plans')
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('sadra.emp-index' , compact(
            'events'
        ));
    }

    public function create()
    {
        return view('sadra.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'start'=>'required',
            'start_register'=>'required',
            'end'=>'required',
            'end_register'=>'required',
            'description'=>'required',
            'photo'=>'image',
            'map'=>'image',
            'banner'=>'image',
        ]);

        $sadra = new Sadra();
        $sadra->title = $request->title;
        $sadra->description = $request->description;
        if (strpos($request->start, '/') !== false) {
            $startDate = explode('/', $request->start);

            $month = (int)persian_to_eng_num($startDate[1]);
            $day = (int)persian_to_eng_num($startDate[2]);
            $year = (int)persian_to_eng_num($startDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startDateGre = Carbon::instance(Verta::parse($request->start)->datetime());
        }
        if (strpos($request->end, '/') !== false) {
            $endDate = explode('/', $request->end);

            $month = (int)persian_to_eng_num($endDate[1]);
            $day = (int)persian_to_eng_num($endDate[2]);
            $year = (int)persian_to_eng_num($endDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endDateGre = Carbon::instance(Verta::parse($request->end)->datetime());
        }

        if (strpos($request->start_register, '/') !== false) {
            $startRegDate = explode('/', $request->start_register);

            $month = (int)persian_to_eng_num($startRegDate[1]);
            $day = (int)persian_to_eng_num($startRegDate[2]);
            $year = (int)persian_to_eng_num($startRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startRegDateGre = Carbon::instance(Verta::parse($request->start_register)->datetime());
        }

        if (strpos($request->end_register, '/') !== false) {
            $endRegDate = explode('/', $request->end_register);

            $month = (int)persian_to_eng_num($endRegDate[1]);
            $day = (int)persian_to_eng_num($endRegDate[2]);
            $year = (int)persian_to_eng_num($endRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endRegDateGre = Carbon::instance(Verta::parse($request->end_register)->datetime());
        }

        $sadra->start_at = $startDateGre;
        $sadra->end_at = $endDateGre;
        $sadra->start_register_at = $startRegDateGre;
        $sadra->end_register_at = $endRegDateGre;
        $sadra->save();
        if ($request->file('photo')) {
            $name = time() . $request->photo->getClientOriginalName();
            $path = $request->photo->storeAs(
                'img/sadra',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $sadra->id;
            $photo->groupable_type = Sadra::class;
            $photo->save();
            $sadra->photo_id = $photo->id;
            $sadra->save();
        }
        if ($request->file('map')) {
            $name = time() . $request->map->getClientOriginalName();
            $path = $request->map->storeAs(
                'img/sadra',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $sadra->id;
            $photo->groupable_type = Sadra::class;
            $photo->save();
            $sadra->map_id = $photo->id;
            $sadra->save();
        }

        if ($request->file('banner')) {
            $name = time() . $request->banner->getClientOriginalName();
            $path = $request->banner->storeAs(
                'img/sadra',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $sadra->id;
            $photo->groupable_type = Sadra::class;
            $photo->save();
            $sadra->banner_id = $photo->id;
            $sadra->save();
        }

        event(new SadraEventCreated($sadra->title,$sadra->end_at));
        EmployeeLogActivity::addToLog('نمایشگاه / رویداد '.$sadra->title.' را ثبت کرد');

        return redirect(route('index.sadra'))->with('success',__('public.sadra_added'));
    }

    public function edit($id)
    {
        $sadra = Sadra::find($id);

        return view('sadra.edit' , compact(
            'sadra'
        ));
    }

    public function update(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'start'=>'required',
            'start_register'=>'required',
            'end'=>'required',
            'end_register'=>'required',
            'description'=>'required',
            'photo'=>'image',
            'map'=>'image',
            'banner'=>'image',
        ]);

        $sadra = Sadra::find($request->sadra_id);
        $sadra->title = $request->title;
        $sadra->description = $request->description;

        if (strpos($request->start, '/') !== false) {
            $startDate = explode('/', $request->start);

            $month = (int)persian_to_eng_num($startDate[1]);
            $day = (int)persian_to_eng_num($startDate[2]);
            $year = (int)persian_to_eng_num($startDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startDateGre = Carbon::instance(Verta::parse($request->start)->datetime());
        }
        if (strpos($request->end, '/') !== false) {
            $endDate = explode('/', $request->end);

            $month = (int)persian_to_eng_num($endDate[1]);
            $day = (int)persian_to_eng_num($endDate[2]);
            $year = (int)persian_to_eng_num($endDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endDateGre = Carbon::instance(Verta::parse($request->end)->datetime());
        }
        if (strpos($request->start_register, '/') !== false) {
            $startRegDate = explode('/', $request->start_register);

            $month = (int)persian_to_eng_num($startRegDate[1]);
            $day = (int)persian_to_eng_num($startRegDate[2]);
            $year = (int)persian_to_eng_num($startRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startRegDateGre = Carbon::instance(Verta::parse($request->start_register)->datetime());
        }

        if (strpos($request->end_register, '/') !== false) {
            $endRegDate = explode('/', $request->end_register);

            $month = (int)persian_to_eng_num($endRegDate[1]);
            $day = (int)persian_to_eng_num($endRegDate[2]);
            $year = (int)persian_to_eng_num($endRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endRegDateGre = Carbon::instance(Verta::parse($request->end_register)->datetime());
        }
        $sadra->start_at = $startDateGre;
        $sadra->end_at = $endDateGre;
        $sadra->start_register_at = $startRegDateGre;
        $sadra->end_register_at = $endRegDateGre;
        $sadra->save();
        if ($request->file('photo')) {
           if ($sadra->photo){
               unlink($sadra->photo->path);
               $sadra->photo->delete();
           }
            $name = time() . $request->photo->getClientOriginalName();
            $path = $request->photo->storeAs(
                'img/sadra',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $sadra->id;
            $photo->groupable_type = Sadra::class;
            $photo->save();
            $sadra->photo_id = $photo->id;
            $sadra->save();
        }
        if ($request->file('map')) {
            if ($sadra->map){
                unlink($sadra->map->path);
                $sadra->map->delete();
            }
            $name = time() . $request->map->getClientOriginalName();
            $path = $request->map->storeAs(
                'img/sadra',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $sadra->id;
            $photo->groupable_type = Sadra::class;
            $photo->save();
            $sadra->map_id = $photo->id;
            $sadra->save();
        }
        if ($request->file('banner')) {
            $name = time() . $request->banner->getClientOriginalName();
            $path = $request->banner->storeAs(
                'img/sadra',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $sadra->id;
            $photo->groupable_type = Sadra::class;
            $photo->save();
            $sadra->banner_id = $photo->id;
            $sadra->save();
        }
        EmployeeLogActivity::addToLog(' رویداد/نمایشگاه '.$sadra->title.' را ویرایش کرد');

        return redirect(route('index.sadra'))->with('success',__('public.data_added'));

    }

    public function delete(Request $request)
    {
        $sadra = Sadra::find($request->sadra);

        if($sadra->has('companies')){
            $message = 'امکان حذف وجود ندارد';
            
            return response()->json('error' , 'امکان حذف وجود ندارد');
        }

        if ($sadra->photo){
            unlink($sadra->photo->path);
            $sadra->photo->delete();
        }

        if($sadra->has('buy_plans')){

            foreach($sadra->buy_plans as $plan){
                if($plan->has('booths')){
                    foreach($plan->booths as $booth){
                        $booth->delete();
                    }
                }
                $plan->delete();
            }

        }

        EmployeeLogActivity::addToLog('نمایشگاه / رویداد '.$sadra->title.' را حذف کرد');

        return response()->json($sadra->delete());
    }

    public function list()
    {
        $events = Sadra::with('map')
            ->orderByDesc('created_at')
            ->paginate(10);

        $company_sadra = company_sadra::where('company_id', Auth::user()->groupable_id)->get();

        CompanyLogActivity::addToLog('به صفحه لیست رویدادها / نمایشگاه ها آمد');

        return view('sadra.co-list', compact(
            'events',
            'company_sadra'
        ));
    }

    public function registerShow($event_id)
    {
        $event = Sadra::with('map')->find($event_id);

        return view('sadra.register' , compact(
            'event'
        ));
    }

    public function register(Request $request)
    {
        $request->validate([
            'plan' => 'required',
            'booth' => 'required',
            'coordinator' => 'required',
            'coordinatorMobile' => 'required',
            'contract' => 'required',
            'inviter' => 'required',
        ] , [
            'plan.required' => 'لطفا پلن مورد نظر خود را انتخاب کنید',
            'booth.required' => 'لطفا غرفه مورد نظر خود را انتخاب کنید',
            'coordinator.required' => 'لطفا نام مسئول هماهنگی شرکت را وارد کنید',
            'coordinatorMobile.required' => 'لطفا تلفن همراه مسئول هماهنگی را وارد کنید',
            'contract.required' => 'لطفا وضعیت تفاهم‌نامه را انتخاب کنید',
            'inviter.required' => 'لطفا مسئول هماهنگ کننده را انتخاب کنید',
        ]); 


        $booth = SadraBooth::find($request->booth);
        $company = CompanyInfo::find(Auth::user()->groupable_id);

        $company_sadras = company_sadra::where('company_id', $company->id)
            ->where('sadra_id', $request->sadra)->get();
        
        // dd($company_sadras);

        $finalCs = null;

        // if($company_sadras->count() > 0){

        //     foreach($company_sadras as $company_sadra){

        //         if( is_null($company_sadra->status) ){

        //             return redirect()->route('registred.company.sadra')
        //                 ->with('error', 'شما قبلا در این رویداد ثبت نام کرده اید، در صورتی که قصد تغییر اطلاعات را دارید ابتدا از درخواست قبلی انصراف دهید، سپس درخواست جدید ثبت نمایید.');

        //         }

        //         if( $company_sadra->status == 1 ){

        //             return redirect()->route('registred.company.sadra')
        //                 ->with('success', 'ثبت نام شما در این رویداد انجام، پرداخت شده و قطعی می‌باشد. شما مجاز به ثبت نام مجدد در این رویداد نیستید.');

        //         } 

        //     }
        // }

        if(! is_null($finalCs)){

            if ($request->booth != $finalCs->booth_id){
                $last_booth = SadraBooth::find($finalCs->booth_id);

                $last_booth->update([
                    'reserved' => false,
                ]);
            }

            $booth->update([
                'reserved' => true
            ]);

            $finalCs->company_id = $company->id;
            $finalCs->sadra_id = $request->sadra;
            $finalCs->booth_id = $booth->id;
            $finalCs->coordinator_name = $request->coordinator;
            $finalCs->coordinator_mobile = $request->coordinatorMobile;
            $finalCs->contract = $request->contract;
            $finalCs->inviter = $request->inviter;

            $finalCs->save();

        } else {

            $booth->update([
                'reserved' => true
            ]);

            $companySadra = new company_sadra();
            $companySadra->company_id = $company->id;
            $companySadra->sadra_id = $request->sadra;
            $companySadra->booth_id = $booth->id;
            $companySadra->coordinator_name = $request->coordinator;
            $companySadra->coordinator_mobile = $request->coordinatorMobile;
            $companySadra->contract = $request->contract;
            $companySadra->inviter = $request->inviter;

            $companySadra->save();
        }

        // $company_sadra = company_sadra::where('company_id', $company->id)
        //     ->where('sadra_id', $request->sadra)->where('status' , '!=' , 0)
        //     ->first();

        // if (! $company_sadra){
        //     $booth->update([
        //        'reserved' => true
        //     ]);
        //     $companySadra = new company_sadra();
        //     $companySadra->company_id = $company->id;
        //     $companySadra->sadra_id = $request->sadra;
        //     $companySadra->booth_id = $booth->id;
        //     $companySadra->coordinator_name = $request->coordinator;
        //     $companySadra->coordinator_mobile = $request->coordinatorMobile;
        //     $companySadra->contract = $request->contract;
        //     $companySadra->inviter = $request->inviter;

        //     $companySadra->save();

        // }else{
        //     if ($request->booth != $company_sadra->booth_id){
        //         $last_booth = SadraBooth::find($company_sadra->booth_id);

        //         $last_booth->update([
        //             'reserved' => false,
        //         ]);
        //     }

        //     $booth->update([
        //         'reserved' => true
        //     ]);

        //     $company_sadra->company_id = $company->id;
        //     $company_sadra->sadra_id = $request->sadra;
        //     $company_sadra->booth_id = $booth->id;
        //     $company_sadra->coordinator_name = $request->coordinator;
        //     $company_sadra->coordinator_mobile = $request->coordinatorMobile;
        //     $company_sadra->contract = $request->contract;
        //     $company_sadra->inviter = $request->inviter;

        //     $company_sadra->save();
        // }

        if($request->has('required_facultyOfHumanities') || $request->has('required_schoolOfLaw') || $request->has('required_art') || $request->has('required_skillCollege')){

            if($request->has('required_facultyOfHumanities')){

                foreach($request->required_facultyOfHumanities as $key => $value){

                    CompanySadraMajor::create([

                        'company_sadra_id' => $company_sadra->id ?? $companySadra->id,
                        'college' => 'علوم انسانی',
                        'major' => $key,
                        'number' => $value
                        
                    ]);
    
                }
    
            }

            if( $request->has('required_schoolOfLaw') ){

                foreach($request->required_schoolOfLaw as $key => $value){

                    CompanySadraMajor::create([

                        'company_sadra_id' => $company_sadra->id ?? $companySadra->id,
                        'college' => 'حقوق',
                        'major' => $key,
                        'number' => $value
                        
                    ]);
                    
                }

            }

            if( $request->has('required_art') ){

                foreach($request->required_art as $key => $value){

                    CompanySadraMajor::create([

                        'company_sadra_id' => $company_sadra->id ?? $companySadra->id,
                        'college' => 'هنر، معماری و شهرسازی',
                        'major' => $key,
                        'number' => $value
                        
                    ]);

                }
                
            }

            if( $request->has('required_skillCollege') ){

                foreach( $request->required_skillCollege as $key => $value ){

                    CompanySadraMajor::create([

                        'company_sadra_id' => $company_sadra->id ?? $companySadra->id,
                        'college' => 'مهارت و کارآفرینی',
                        'major' => $key,
                        'number' => $value
                        
                    ]);
                    
                }

            }

        }

        if ($request->has('logo'))
        {
            $name = time() . $request->logo->getClientOriginalName();
            $path = $request->logo->storeAs('img/company', $name, 'public');

            $logo = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $company->id,
                'groupable_type' => CompanyInfo::class,
            ]);

            $company->logo_id = $logo->id;
            $company->save();
        }

        


        return redirect()->route('registred.company.sadra')
            ->with('success', __('public.sadra-pre-register'));

    }

    public function companyRegistred(Request $request)
    {
        CompanyLogActivity::addToLog('به صفحه درخواست های ثبت نام رویداد/نمایشگاه آمد');

        $requests = company_sadra::where('company_id', Auth::user()->groupable_id)
            ->with('booth', 'sadra','payedPayment')
            ->orderByDesc('updated_at')
            ->paginate(20);

        foreach( $requests as $request ) {

            $company_sadra_checked = $this->checkCompanyPayments($request);

        }
        
        return view('panel.company.sadra-reg-requests', compact('requests'));
    }

    public function rejectCompanySadra(Request $request)
    {
        $company_sadra = company_sadra::findOrFail($request->company_sadra_id);

        $company_sadra = $this->checkCompanyPayments($company_sadra);

        if( is_null($company_sadra->status) ) {

            $last_booth = SadraBooth::findOrFail($company_sadra->booth_id);

            $last_booth->update([
                'reserved' => false
            ]);
    
            $company_sadra->update([
                'status' => 0
            ]);
    
            return response()->json(true);

        }else {

            return response()->json(false);

        }

    }

    public function checkCompanyHas3InternShip(Request $request)
    {
        $job_positions = OjobPosition::Intern()
            ->whereRelation('company' , 'id' , $request->user_id)
            ->count();

        if ($job_positions >= 3) {

            return response()->json('ok');

        } else {

            return response()->json('error');

        }
    }

    public function checkCompanyPayments($request)
    {
        $sadra = company_sadra::with('payments')
            ->find($request->id);
        
        if( is_null($sadra->status) and $sadra->payments->count() > 0 ) {
                

            $response = Http::withOptions([

                'verify' => false

            ])->post('https://api.iaun.ac.ir/api/login',[
                'username' => 'apifanyaruser',
                'password' => 'AapiUser@FanYar',
                'usertype' => 3,
            ]);
            
            $dataArray = $response->json();
            $token = $dataArray['data']['token'];
            $successPay=false;

            foreach( $sadra->payments as $pay) {

                $response = Http::withOptions([

                    'verify' => false,

                ])->withToken($token)
                    ->acceptJson()
                    ->post('https://api.iaun.ac.ir/api/payments/verify',[
                        'Accept' => 'application/json',
                        'payment_key' => $pay->payment_key,
                    ]);

                if( isset($response['success']) and $response['success'] == true ) {

                    if( $response['data']['pay_status'] == "2") {

                        $pay->update([
                            'status' => 'عملیات پرداخت الکترونیک با موفقیت انجام شده است.',
                            'tracking_code' => $response['data']['pay_tracenumber'],
                            'receipt' => $response['data']['pay_rrn'],
                            'card_number' => $response['data']['pay_cardnumber'],
                            'payer_bank' => $response['data']['pay_issuerbank'],
                            'payment_date_time' => $response['data']['pay_paymentdatetime'],
                        ]);

                        $sadra->update([
                            'status' => true,
                            'receipt_id' => $pay->id,
                        ]);
                        $successPay = true;

                    }elseif( $response['data']['pay_status'] == "3" ) {

                        $pay->update([
                            'status' => 'عملیات پرداخت الکترونیک با مشکل روبرو شده است.'
                        ]);

                    }elseif( $response['data']['pay_status'] == "4" ) {

                        $pay->update([
                            'status' => 'تراکنش تکراری است.',
                        ]);

                    }elseif( $response['data']['pay_status'] == "5" ) {

                        $pay->update([
                            'status' => 'تراکنش توسط کاربر لغو شده است.',
                        ]);

                    }

                }

            }

        }

        return $sadra;
    }

    public function getSadraPlans(Request $request)
    {
        $plans = BuyPlan::where('sadra_id' , $request->sadra_id)->get();
        
        return response()->json($plans);
    }
}
