<?php

namespace App\Http\Controllers;

use App\Models\BuyPlan;
use App\Models\Discount;
use App\Models\Event;
use App\Models\EventInfo;
use App\Models\Sadra;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discounts = Discount::NotDeleted()
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('panel.employee.discount-list', compact(
            'discounts'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = Event::NotDeleted()->with('infos.plan' , 'infos.sans')->get();
        $sadras = Sadra::with('buy_plans')->get();

        return view('panel.employee.create-discount' , compact(
            'events',
            'sadras'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        if (strpos($request->input('begin'), '/') !== false){

            $startDate = explode('/', $request->input('begin'));

            $month = (int)persian_to_eng_num($startDate[1]);
            $day = (int)persian_to_eng_num($startDate[2]);
            $year = (int)persian_to_eng_num($startDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $startDateGre = Carbon::instance(Verta::parse($request->input('begin'))->datetime());
        }

        if (strpos($request->input('end'), '/') !== false) {
            $endDate = explode('/', $request->input('end'));

            $month = (int)persian_to_eng_num($endDate[1]);
            $day = (int)persian_to_eng_num($endDate[2]);
            $year = (int)persian_to_eng_num($endDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endDateGre = $jalaliDate->toCarbon('Asia/Tehran');
        } else {
            $endDateGre = Carbon::instance(Verta::parse($request->input('end'))->datetime());
        }

        $company_acceptance = false;
        if($request->has('company_acceptance') && $request->input('company_acceptance') == true){
            $company_acceptance = true;
        }

        $student_acceptance = false;
        if($request->has('student_acceptance') && $request->input('student_acceptance') == true){
            $student_acceptance = true;
        }

        $applicant_acceptance = false;
        if($request->has('applicant_acceptance') && $request->input('applicant_acceptance') == true){
            $applicant_acceptance = true;
        }

        $discount = Discount::create([
            'name' => $request->input('name'),
            'is_percent' => $request->input('is_percent'),
            'value' => $request->input('value'),
            'begin' => $startDateGre,
            'end' => $endDateGre,
            'max' => $request->input('max'),
            'description' => $request->input('description'),
            'company_acceptance' => $company_acceptance,
            'student_acceptance' => $student_acceptance,
            'applicant_acceptance' => $applicant_acceptance,
        ]);

        if($request->has('isAllEvent')){

            if($request->input('isAllEvent') == '1'){

                $eventInfos = EventInfo::all();
                
            } else {

                $counter = 1;
                $eventInfoIds = [];

                while($request->has('eventInfoId'.$counter)){

                    array_push($eventInfoIds , $request->input('eventInfoId'.$counter));

                    $counter++;

                }

                $eventInfos = EventInfo::whereIn('id' , $eventInfoIds)->get();

            }

            $discount->eventInfos()->sync($eventInfos->pluck('id')->toArray());
        }

        if($request->has('isAllSadra')){

            if($request->input('isAllSadra') == '1'){

                $sadraPlans = BuyPlan::all();

            } else {

                $counter = 1;
                $sadraPlanIds = [];

                while($request->has('sadraPlanId'.$counter)){

                    array_push($sadraPlanIds , $request->input('sadraPlanId'.$counter));

                    $counter++;

                }

                $sadraPlans = BuyPlan::whereIn('id' , $sadraPlanIds)->get();

            }

            $discount->sadraPlans()->sync($sadraPlans->pluck('id')->toArray());

        }

        
        return redirect()->route('index.discount')->with('تخفیف جدید ایجاد شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount $discount)
    {
        return view('panel.employee.edit-discount',compact(
            'discount'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $discount = Discount::find($request->discount);

        return response()->json(
            $discount->update([
                'deleted' => true
            ])
        );
    }
}
