<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use App\Models\ApplicantInfo;
use App\Models\StudentInfo;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function studentSearch(Request $request)
    {
        $req = $request->resultObj;
        $query = User::query();
        $query = $query->Confirmed();
        $query = $query->where('groupable_type',StudentInfo::class)->whereNotNull('groupable_id');
        $query = $query->with('studentInfos','studentInfos.province','studentInfos.city','studentInfos.university','work_experiences');

        if($req['province'] != '*'){
            $query->whereRelation('studentInfos','province_id', $req['province']);
        }
        
        if($req['city'] != '*'){
            $query->whereRelation('studentInfos','city_id', $req['city']);
        }
        
        if($req['grade'] != '*'){
            $query->whereRelation('studentInfos','grade', $req['grade']);
        }

        // if($req['experince'] != '*'){
        //     $query->whereRelation('work_experiences' , '')
        // }
        
        if($req['sex'] != '*'){
            $query->where('sex', $req['sex']);
        }

        if ($req['age'] != '*') {
            if ($req['age'] == 20) {
                $query->whereHas('studentInfos', function ($query) {
                    $query->where('age', '>=', 0)->where('age', '<=', 20);
                });
            } elseif ($req['age'] == 30) {
                $query->whereHas('studentInfos', function ($query) {
                    $query->where('age', '>=', 21)->where('age', '<=', 30);
                });
            } elseif ($req['age'] == 40) {
                $query->whereHas('studentInfos', function ($query) {
                    $query->where('age', '>=', 31)->where('age', '<=', 40);
                });
            }
        }

        $result = $query->get();

        return response()->json($result);

    }

    public function applicantSearch(Request $request)
    {
        $req = $request->resultObj;
        $query = User::query();
        $query = $query->Confirmed();
        $query = $query->where('groupable_type',ApplicantInfo::class)->whereNotNull('groupable_id');
        $query = $query->with('applicantInfos.province','academicInfos','applicantInfos.city');

        if($req['province'] != '*'){
            $query->whereRelation('applicantInfos','province_id', $req['province']);
        }

        if($req['city'] != '*'){
            $query->whereRelation('applicantInfos','city_id', $req['city']);
        }
        
        if($req['grade'] != '*'){
            $query->whereRelation('academicInfos','grade', $req['grade']);
        }
        
        if($req['sex'] != '*'){
            $query->where('sex', $req['sex']);
        }
        
        if($req['marige'] != '*'){
            $query->whereRelation('applicantInfos','marital_status', $req['marige']);
        }
        
        if($req['experince'] != '*'){
            if($req['experince']=='2')
                $query->whereRelation('applicantInfos','experience','>',1);
        
            if($req['experince']=='5')
            $query->whereRelation('applicantInfos','experience','>',2);

            if($req['experince']=='10')
                $query->whereRelation('applicantInfos','experience','>',4);

        }

        if ($req['age'] != '*') {
            $query->whereHas('applicantInfos', function ($applicantQuery) use ($req) {
                if ($req['age'] == "20") {
                    $applicantQuery->whereDate('birth', '>', Carbon::now()->subYears(21));
                }
                if ($req['age'] == "30") {
                    $applicantQuery->whereDate('birth', '<=', Carbon::now()->subYears(21));
                    $applicantQuery->whereDate('birth', '>=', Carbon::now()->subYears(30));
                }
                if ($req['age'] == "40") {
                    $applicantQuery->whereDate('birth', '<=', Carbon::now()->subYears(31));
                    $applicantQuery->whereDate('birth', '>=', Carbon::now()->subYears(40));
                }
            });
        }
        

        // if($req['age'] != '*'){
        //     if($req['age']=="20"){
        //         // $query->whereRelation('applicantInfos','birth',[Carbon::now()->subYears(0)->format('Y-m-d'),Carbon::now()->subYears(21)->format('Y-m-d')]);
        //         $query->whereDate('applicantInfos.birth', '>=', Carbon::now()->subYears(20)->format('Y-m-d'));
        //         $query->whereDate('applicantInfos.birth', '<=', Carbon::now()->subYears(0)->format('Y-m-d'));
        //     }
        //     if($req['age']=="30"){
        //         $query->whereRelation('applicantInfos','birth',[Carbon::now()->subYears(21)->format('Y-m-d'),Carbon::now()->subYears(30)->format('Y-m-d')]);
        //     }
        //     if($req['age']=="40"){
        //         $query->whereRelation('applicantInfos','birth',[Carbon::now()->subYears(31)->format('Y-m-d'),Carbon::now()->subYears(40)->format('Y-m-d')]);
        //     }
        // }

        $result = $query->get();

        foreach($result as $item){
            $sen = Carbon::parse($item->applicantInfos->birth);
            $sen = $sen->diffInYears(Carbon::now());
            $item->age = $sen;
        }

        return response()->json($result);
    }

    public function personalityTestsSearch(Request $request)
    {
        $req = $request->resultObj;
        $query = User::query();
        $query = $query->whereHas('mbti_test');
        $query = $query->whereHas('extraUserHasNcode');
        $query = $query->with('extraUserHasNcode','mbti_test');

        if($req['grade'] != '*'){
            $query->whereRelation('extraUserHasNcode','grade', $req['grade']);
        }
        if($req['major'] != '*'){
            $query->whereRelation('extraUserHasNcode','major', $req['major']);
        }

        $results = $query->get();
        $mbtiType = [];
        $mbtiCountOfType = [];
        $mbti = [
            'ENFJ' => 0,
            'ENFP' => 0,
            'ESFJ' => 0,
            'ESFP' => 0,
            'INFJ' => 0,
            'INFP' => 0,
            'INTJ' => 0,
            'INTP' => 0,
            'ISFJ' => 0,
            'ISFP' => 0,
            'ISTJ' => 0,
            'ISTP' => 0,
            'ESTJ' => 0,
            'ENTJ' => 0,
            'ENTP' => 0,
            'ESTP' => 0,
        ];
        foreach($results as $result){
            foreach($mbti as $key => $value){
                if($result->mbti_test->personality_type == $key){
                    $mbti[$key]++;
                }
            }
        }

        foreach($mbti as $key => $value){
            if($value != 0){
                array_push($mbtiType , $key);
                array_push($mbtiCountOfType , $value);
            }
        }
        $mbtiType = mbti_personality_to_fa($mbtiType);
        $response = [$mbtiType , $mbtiCountOfType];
        return response()->json($response);
    }
}
