<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use Illuminate\Http\Request;
use App\Models\ApplicantInfo;
use App\Models\CompanyPackage;
use App\Models\PurchasePackage;
use App\Services\Payment\Gatewey\Zarinpal;
use App\Services\Payment\IaunGateway;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Services\Payment\PaymentGatewayFactory;
use App\Services\Payment\PaymentGatewayInterface;

class GeneralPaymentController extends Controller
{
    protected $paymentGatewayFactory;
    protected $paymentGateway;
    private $request;

    public function __construct(PaymentGatewayFactory $paymentGatewayFactory)
    {
        $this->paymentGatewayFactory = $paymentGatewayFactory;
    }

    
    public function Pay(array $request)
    {
        $this->request = $request;
        $gatewayType = $request['gateway'];
        $paymentGateway = $this->paymentGatewayFactory->create($gatewayType);
        $this->paymentGateway = $paymentGateway;

        $payment_key = $paymentGateway->pay($request['config'], $request['payment']);

        if($paymentGateway instanceof IaunGateway){
            return redirect('https://api.iaun.ac.ir/payment-gateway/'.$payment_key);
        } 

        if($paymentGateway instanceof Zarinpal){
            return redirect('https://payment.zarinpal.com/pg/StartPay/'.$payment_key);
        }
        
    }


    public function verify($paymentId)
    {
        
        $paymentId = (int)$paymentId;
        $payment = Payment::with('order')->find($paymentId);

        // if($_GET['Status'] == 'NOK'){
        //     $payment->update([
        //         'status' => 'تراکنش شما با خطا مواجه شد.'
        //     ]);
        //     return false;
        // }
        
        $paymentGateway = $this->paymentGatewayFactory->create($payment->gateway);


        $result = $paymentGateway->verify($payment);
        
        if($result['status'] == PaymentGatewayInterface::TRANSACTION_FAILED)
            return false;

        return $this->checkResult($result, $payment);

    }


    private function checkResult($result, $payment)
    {
        if($result['data']['data']['pay_status'] == "2"){

            $payment->update([
                'status' => 'عملیات پرداخت الکترونیک با موفقیت انجام شده است.',
                'tracking_code' => $result['data']['data']['pay_tracenumber'],
                'receipt' => $result['data']['data']['pay_rrn'],
                'card_number' => $result['data']['data']['pay_cardnumber'],
                'payer_bank' => $result['data']['data']['pay_issuerbank'],
                'payment_date_time' => $result['data']['data']['pay_paymentdatetime'],
            ]);

            return true;

        }else {

            $payment->update([
                'status' => $result['data']['data']['pay_dscstatus']
            ]);

            return false;
        }
    }


    public function getpanel()
    {
        if(Auth::user()->groupable_type == CompanyInfo::class){
            return 'panel.company.master';
        }
        elseif(Auth::user()->groupable_type == StudentInfo::class){
            return 'panel.student.master';
        }
        elseif(Auth::user()->groupable_type == ApplicantInfo::class){
            return 'panel.student.master';
        }
    }



}
