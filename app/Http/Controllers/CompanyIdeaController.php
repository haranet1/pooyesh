<?php

namespace App\Http\Controllers;

use App\Events\IdeaCreated;
use App\Http\Requests\CompanyIdeaStoreRequest;
use App\Models\Idea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyIdeaController extends Controller
{
    public function index()
    {
        $ideas = Idea::where('owner_id', Auth::id())->paginate(20);
        return view('panel.company.ideas-list', compact('ideas'));
    }

    public function create()
    {
        return view('panel.company.create-idea');
    }

    public function edit($id)
    {
        $idea = Idea::findorfail($id);
        return view('panel.company.edit-idea', compact('idea'));
    }

    public function store(CompanyIdeaStoreRequest $request)
    {
        Auth::user()->ideas()->create($request->all());
        event(new IdeaCreated($request->title));
        \CompanyLogActivity::addToLog('یک ایده/استارتاپ جدید ثبت کرد');

        return redirect(route('company.idea.index'))->with('success', __('public.data_added'));
    }

    public function update(Request $request, Idea $idea)
    {
        $budget = persian_to_eng_num($request->budget);
        $request->merge(['budget' => $budget]);

        $request->validate([
            'budget' => 'int'
        ]);
        $idea->update($request->all());
        \CompanyLogActivity::addToLog('یک ایده/استارتاپ را ویرایش کرد');

        return redirect(route('company.idea.index'))->with('success', __('public.data_added'));
    }
}
