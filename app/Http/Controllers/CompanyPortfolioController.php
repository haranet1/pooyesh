<?php

namespace App\Http\Controllers;

use App\Models\CompanyPortfolio;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyPortfolioController extends Controller
{
    public function __construct()
    {
        $this->middleware(['isCompany']);
    }

    public function index()
    {
        $samples = CompanyPortfolio::where('company_id', Auth::id())->paginate(30);
        return view('panel.company.portfolio', compact('samples'));
    }

    public function create()
    {
        return view('panel.company.create-portfolio');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'skills' => 'required',
            'photo' => 'nullable|image',
            'desc' => 'required',
        ], [
            'name.required' => 'عنوان الزامی است',
            'skills.required' => 'مهارت های مورد استفاده الزامی است',
            'photo.image' => 'نوع فایل انتخابی صحیح نیست',
            'desc.required' => 'توضیحات الزامی است',
        ]);

        $portfolio = new CompanyPortfolio();
        $portfolio->title = $request->input('name');
        $portfolio->company_id = Auth::id();

        $portfolio->skills_used = $request->input('skills');
        $portfolio->description = $request->input('desc');
        $portfolio->save();

        if ($request->file('photo')) {
            $name = time() . $request->photo->getClientOriginalName();
            $path = $request->photo->storeAs(
                'img/company',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $portfolio->id;
            $photo->groupable_type = CompanyPortfolio::class;
            $photo->save();
            $portfolio->photo_id = $photo->id;
            $portfolio->save();
        }
       \CompanyLogActivity::addToLog('یک نمونه پروژه برای شرکت ثبت کرد');

        return redirect(route('company.portfolio.index'));

    }
}
