<?php

namespace App\Http\Controllers;

use App\Models\AcademicInfo;
use App\Models\Certificate;
use App\Models\Lang;
use App\Models\LangUser;
use App\Models\Oability;
use App\Models\OjobTechnologySkill;
use App\Models\Oknowledge;
use App\Models\Oskill;
use App\Models\OstudentAbility;
use App\Models\OstudentKnowledge;
use App\Models\OstudentSkill;
use App\Models\OstudentTechnologySkill;
use App\Models\OstudentWorkStyle;
use App\Models\OtechnologySkill;
use App\Models\OtechnologySkillExample;
use App\Models\OworkStyle;
use App\Models\Photo;
use App\Models\SocialNetwork;
use App\Models\User;
use App\Models\WorkExperience;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;

class CvController extends Controller
{
    public function academicInfo()
    {
        $academinInfos = AcademicInfo::where('user_id', Auth::id())
            ->orderByDesc('updated_at')
            ->get();

        return view('cv.academic-info', compact(
            'academinInfos'
        ));
    }

    public function storeAcademicInfo(Request $request)
    {
        $request->validate([
            'major' => 'required',
            'grade' => 'required',
            'university' => 'required',
            'avg' => 'required',
            'end_date' => 'required',
        ],[
            'major.required' => 'رشته الزامی است',
            'grade.required' => 'مقطع الزامی است',
            'university.required' => 'دانشگاه | موسسه آموزشی الزامی است',
            'avg.required' => 'معدل الزامی است',
            'end_date.required' => 'تاریخ اخذ مدرک الزامی است',
        ]);
        
        $year = (int)persian_to_eng_num($request->input('end_date'));

        $academinInfo = AcademicInfo::create([
            'user_id' => Auth::id(),
            'major' => $request->major,
            'grade' => $request->grade,
            'university' => $request->university,
            'avg' => $request->avg,
            'end_date' => $year
        ]);

        return redirect()->back()->with('success' , 'اطلاعات اکادمیک جدید اضافه شد.');
    }

    public function deleteAcademicInfo(Request $request)
    {
        $academicInfo = AcademicInfo::find($request->academicInfoId);
        $academicInfo->delete();

        return redirect()->back()->with('success' , 'اطلاعات مورد نظر حذف شد');
    }

    public function skills()
    {
        $skills = Oskill::all();
        $mySkills = OstudentSkill::with('skillName')
            ->where('student_id' , Auth::id())
            ->get();
        
        // dd($mySkills);

        return view('cv.skills', compact(
            'skills',
            'mySkills'
        ));
    }

    public function storeSkills(Request $request)
    {
        foreach($request->all() as $key => $value){
            if(strpos($key , 'skill_') === 0) {
                $skill_id_or_title  = str_replace('skill_', '', $key);
                if(is_numeric($skill_id_or_title)) {
                    OstudentSkill::create([
                        'student_id' => Auth::id(),
                        'oskill_id' => $skill_id_or_title,
                        'level' => $value,
                    ]);
                }
            }
        }

        return redirect()->back()->with('success' , 'مهارت های شما با موفقیت ثبت شد');
    }

    public function deleteSkill($id)
    {
        $skill = OstudentSkill::find($id);
        
        $skill->delete();
        return redirect()->back()->with('success' , 'مهارت مورد نظر حذف شد');
    }

    public function knowledgs()
    {
        $knowledgs = Oknowledge::all();
        $myKnowledges = OstudentKnowledge::with('knowledgeName')
            ->where('student_id' , Auth::id())
            ->get();

        return view('cv.knowledges', compact(
            'knowledgs',
            'myKnowledges'
        ));
    }

    public function storeKnowledg(Request $request)
    {
        foreach($request->all() as $key => $value) {
            if(strpos($key , 'knowledge_') === 0) {
                $knowledge_id_or_title = str_replace('knowledge_', '', $key);

                if(is_numeric($knowledge_id_or_title)) {
                    OstudentKnowledge::create([
                        'student_id' => Auth::id(),
                        'oknowledge_id' => $knowledge_id_or_title,
                        'level' => $value,
                    ]);
                } 
                
            }
        }

        return redirect()->back()->with('success' , 'دانش شما با موفقیت ثبت شد');
    }

    public function deleteKnowledge($id)
    {
        $knowledge = OstudentKnowledge::find($id);
        $knowledge->delete();
        return redirect()->back()->with('success' , 'دانش مورد نظر حذف شد');
    }

    public function abilities()
    {
        $abilities = Oability::all();
        $myAbilities = OstudentAbility::with('abilityName')
            ->where('student_id' , Auth::id())
            ->get();

        return view('cv.abilities', compact(
            'abilities',
            'myAbilities'
        ));
    }

    public function storeAbility(Request $request)
    {
        foreach($request->all() as $key => $value){
            if(strpos($key , 'ability_') === 0) {
                $ability_id_or_title = str_replace('ability_', '', $key);
                if(is_numeric($ability_id_or_title)) {
                    OstudentAbility::create([
                        'student_id' => Auth::id(),
                        'oability_id' => $ability_id_or_title,
                        'level' => $value,
                    ]);
                }
            }
        }

        return redirect()->back()->with('success' , 'توانایی های شما با موفقیت ثبت شد');
    }

    public function deleteAbilitiy($id)
    {
        $ability = OstudentAbility::find($id);
        $ability->delete();
        return redirect()->back()->with('success' , 'توانایی مورد نظر حذف شد');
    }

    public function technologySkills()
    {
        $techExamples = OtechnologySkillExample::all();

        $technologySkills = OtechnologySkill::all();
        $myTechnologySkills = OstudentTechnologySkill::with('exampleName')
            ->where('student_id' , Auth::id())
            ->get();

        return view('cv.technology-skills', compact(
            'techExamples',
            'technologySkills',
            'myTechnologySkills'
        ));
    }

    public function getExmapeles(Request $request)
    {
        $technologySkill = OtechnologySkill::find($request->id);

        $jts = OjobTechnologySkill::with('example','technologySkillName')
            ->where('technology_skill_id', $technologySkill->id)
            ->select('example_id')  
            ->distinct()  
            ->get();

        return response()->json($jts);
    }

    public function storeTechnologySkills(Request $request)
    {
        foreach($request->all() as $key => $value){
            if($key == 'technologySkillId'){
                $techValue = $value;
            } else {
                $examValue = $value;
            }
            if(strpos($key, 'technologySkill_') === 0) {
                $technologySkill_id_or_title = str_replace('technologySkill_', '', $key);

                if(is_numeric($technologySkill_id_or_title)) {
                    OstudentTechnologySkill::create([
                        'student_id' => Auth::id(),
                        'otechnology_skill_id' => $techValue,
                        'example_id' => $technologySkill_id_or_title,
                        'level' => $examValue,
                    ]);
                }
            }
        }

        return redirect()->back()->with('success' , 'مهارت های فنی شما ثبت شد');
    }

    public function deleteTechnologySkill($id)
    {
        $technologySkill = OstudentTechnologySkill::find($id);
        $technologySkill->delete();
        return redirect()->back()->with('success' , 'مهارت فنی مورد نظر حذف شد');
    }

    public function workStyles()
    {
        $workStyles = OworkStyle::all();
        $myWorkStyles = OstudentWorkStyle::with('workStyleName')
            ->where('student_id' , Auth::id())
            ->get();

        return view('cv.work-styles', compact(
            'workStyles',
            'myWorkStyles'
        ));
    }
    
    public function storeWorkStyles(Request $request)
    {
        foreach($request->all() as $key => $value){
            if(strpos($key, 'workStyle_') === 0) {
                $workstyle_id_or_title = str_replace('workStyle_', '', $key);

                if(is_numeric($workstyle_id_or_title)) {
                    OstudentWorkStyle::create([
                        'student_id' => Auth::id(),
                        'owork_style_id' => $workstyle_id_or_title,
                        'level' => $value,
                    ]);
                }
            }
        }

        return redirect()->back()->with('success' , 'ویژگی های کاری شما با موفقیت ثبت شد');
    }

    public function deleteWorkStyles($id)
    {
        $workStyle = OstudentWorkStyle::find($id);
        $workStyle->delete();
        return redirect()->back()->with('success' , 'ویژگی کاری مورد نظر حذف شد');
    }

    public function lang()
    {
        $langs = Lang::all();
        $user = User::find(Auth::id());
        $myLangs = $user->languages()->get();

        return view('cv.langs' , compact(
            'langs',
            'myLangs'
        ));
    }

    public function storeLang(Request $request)
    {
        foreach($request->all() as $key => $value) {
            if(strpos($key, 'lang_') === 0) {
                $lang_id_or_title = str_replace('lang_', '', $key);

                if(is_numeric($lang_id_or_title)) {
                    LangUser::create([
                        'user_id' => Auth::id(),
                        'lang_id' => $lang_id_or_title,
                        'level' => changeLevelToFaLevel($value),
                    ]);
                }
            }
        }

        return redirect()->back()->with('success' , 'زبان های خارجی شما با موفقیت ثبت شد');
    }

    public function deleteLang($id)
    {
        $user = User::find(Auth::id());
        $user->languages()->detach($id);
        return redirect()->back()->with('success' , 'زبان مورد نظر حذف شد');
    }

    public function workExperiences()
    {
        $user = User::find(Auth::id());
        $myExperiences = $user->work_experiences()->orderByDesc('updated_at')->get();

        return view('cv.work-experience', compact(
            'myExperiences'
        ));
    }

    public function storeWorkExperiences(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'company' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ],[
            'title.required' => 'عنوان الزامی است',
            'company.required' => 'نام سازمان الزامی است',
            'start_date.required' => 'تاریخ شروع الزامی است',
            'end_date.required' => 'تاریخ پایان الزامی است',
        ]);

        if(strpos($request->input('start_date'), '/') !== false) {

            $startRegDate = explode('/' , $request->input('start_date'));

            $month = (int)persian_to_eng_num($startRegDate[1]);
            $day = (int)persian_to_eng_num($startRegDate[2]);
            $year = (int)persian_to_eng_num($startRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);

            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');

        } else {
            $startRegDateGre = Carbon::instance(Verta::parse($request->input('start_date'))->datetime());
        }

        if(strpos($request->input('end_date') , '/') !== false) {

            $endRegDate = explode('/' , $request->input('end_date'));

            $month = (int)persian_to_eng_num($endRegDate[1]);
            $day = (int)persian_to_eng_num($endRegDate[2]);
            $year = (int)persian_to_eng_num($endRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');

        } else {
            $endRegDateGre = Carbon::instance(Verta::parse($request->input('end_date'))->datetime());
        }

        $experiens = WorkExperience::create([
            'user_id' => Auth::id(),
            'title' => $request->title,
            'company' => $request->company,
            'start_date' => $startRegDateGre,
            'end_date' => $endRegDateGre
        ]);

        return redirect()->back()->with('success' , 'سابقه کاری شما با موفقیت ثبت شد');
    }

    public function deleteWorkExperiences(Request $request)
    {
        $work = WorkExperience::find($request->workId);
        $work->delete();

        return redirect()->back()->with('success' , 'سابقه کاری مورد نظر حذف شد');
    }

    public function socials()
    {
        $user = User::find(Auth::id());
        $mySocials = $user->social_networks()->get();

        return view('cv.social-network', compact(
            'mySocials'
        ));
    }

    public function storeSocials(Request $request)
    {
        $request->validate([
            'social' => 'required',
            'link' => 'required',
        ], [
            'social.required' => 'این فیلد الزامی است',
            'link.required' => 'این فیلد الزامی است',
        ]);

        $user = User::find(Auth::id());
        $mySocials = $user->social_networks()->get();

        foreach($mySocials as $so){
            if($request->social == $so->name){
                return redirect()->back()->with('error' , 'نمی‌توانید شبکه اجتماعی تکراری ثبت کنید');
            }
        }

        SocialNetwork::create([
            'user_id' => Auth::id(),
            'name' => $request->social,
            'link' => $request->link
        ]);

        return redirect()->back()->with('success' , 'شبکه اجتماعی شما با موفقیت ثبت شد');
    }

    public function deleteSocials(Request $request)
    {
        $user = User::find(Auth::id());
        $social = $user->social_networks()->find($request->soId); 

        $social->delete(); 

        return redirect()->back()->with('success' , 'شبکه مورد نظر حذف شد');
    }

    public function certificates()
    {
        $user = User::find(Auth::id());
        $myCertificates = $user->certificates()->with('photo')->get();

        return view('cv.certificate', compact(
            'myCertificates'
        ));
    }

    public function storeCertificates(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image_file' => 'required|image|max:512',
        ],[
            'title.required' => 'عنوان الزامی است',
            'image_file.required' => 'بارگزاری تصویر الزامی است',
            'image_file.image' => 'تصویر با فرمت صیحیح بارگذاری کنید',
            'image_file.max' => 'حجم تصویر نباید بیشتر از 500 کیلوبایت باشد',
        ]);

        $certificate = Certificate::create([
            'user_id' => Auth::id(),
            'title' => $request->title,
        ]);

        if($request->file('image_file')){

            $name = time() . $request->image_file->getClientOriginalName();
            $path = $request->image_file->storeAs(
                'img/certificate',
                $name,
                'public'
            );

            $photo = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id'=> $certificate->id,
                'groupable_type'=> Certificate::class
            ]);

            $certificate->updateOrFail([
                'photo_id'=> $photo->id,
            ]);
        }

        return redirect()->back()->with('success' , 'مدارک شما با موفقیت ثبت شد');
    }

    public function deleteCertificates(Request $request)
    {
        $user = User::find(Auth::id());
        $myCertificate = $user->certificates()->with('photo')->find($request->certificateId);
        if ($myCertificate) {
            if ($myCertificate->photo && file_exists($myCertificate->photo->path)) {
                unlink($myCertificate->photo->path); 
            }
            $myCertificate->delete(); 
        }

        return redirect()->back()->with('success' , 'مدرک مورد نظر حذف شد');
    }
}
