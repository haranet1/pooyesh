<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Setting;
use App\Models\CompanyInfo;
use App\Models\StudentInfo;
use Illuminate\Http\Request;
use App\Models\ApplicantInfo;
use App\Models\CompanyPackage;
use App\Models\PurchasePackage;
use Illuminate\Support\Facades\Auth;
use App\Services\Payment\PaymentGatewayFactory;

class PackageController extends Controller
{

    private $PaymentController;

    public function __construct(GeneralPaymentController $PaymentController)
    {
        $this->PaymentController = $PaymentController;
    }


    public function index()
    {
        $title = 'بسته های خدماتی';
        $packages = PurchasePackage::orderByDesc('updated_at')->get();
        return view('admin.packages.index', compact('packages','title'));
    }


    public function create()
    {
        $title = 'افزودن بسته خدماتی';
        return view('admin.packages.create', compact('title'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'price'=>'required',
        ]);

        $request->internship_positions = $request->internship_positions ?? 0;
        $request->internship_requests = $request->internship_requests ?? 0;
        $request->internship_confirms = $request->internship_confirms ?? 0;
        $request->hire_confirms = $request->hire_confirms ?? 0;
        $request->hire_requests = $request->hire_requests ?? 0;
        $request->hire_positions = $request->hire_positions ?? 0;

        $package = PurchasePackage::create([
            'name' => $request->name,
            'additional_internship_positions' => $request->internship_positions,
            'additional_internship_requests' => $request->internship_requests,
            'additional_internship_confirms' => $request->internship_confirms,
            'additional_hire_confirms' => $request->hire_confirms,
            'additional_hire_requests' => $request->hire_requests,
            'additional_hire_positions' => $request->hire_positions,
            'type' => $request->type,
            'price' => $request->price,
        ]);

        return redirect(route('index.package.user'))->with('success', 'بسته خدماتی با موفقیت اضافه شد.');
    }


    public function edit($id)
    {
        $title = 'ویرایش بسته خدماتی';
        $package = PurchasePackage::find($id);

        return view('admin.packages.edit', compact('title', 'package'));
    }


    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'price' => 'required',
        ]);

        $package = PurchasePackage::find($request->package_id);
        $package->name = $request->name;
        $package->additional_internship_positions = $request->additional_internship_positions;
        $package->additional_internship_requests = $request->additional_internship_requests;
        $package->additional_internship_confirms = $request->additional_internship_confirms;
        $package->additional_hire_positions = $request->additional_hire_positions;
        $package->additional_hire_requests = $request->additional_hire_requests;
        $package->additional_hire_confirms = $request->additional_hire_confirms;
        $package->type = $request->type;
        $package->price = $request->price;

        $package->save();

        return redirect(route('index.package.user'))->with('success', 'بسته خدماتی با موفقیت بروزرسانی شد');

    }


    public function delete(Request $request)
    {
        $package = PurchasePackage::find($request->package);
        return response()->json($package->delete());
    }


    // company functions 

    public function company_index()
    {
        $title = 'بسته های خدماتی';
        $intern_packages = PurchasePackage::where('type','company')->where([
            ['additional_hire_confirms', 0],
            ['additional_hire_requests', 0],
            ['additional_hire_positions', 0]
        ])->orderByDesc('updated_at')->get();
        $hire_packages = PurchasePackage::where('type','company')->where([
            ['additional_internship_positions', 0],
            ['additional_internship_requests', 0],
            ['additional_internship_confirms', 0]
        ])->orderByDesc('updated_at')->get();
        $combined_packages = PurchasePackage::where('type','company')->where( function($query){
            $query->where('additional_internship_positions','>', 0)
            ->orWhere('additional_internship_requests','>', 0)
            ->orWhere('additional_internship_confirms','>', 0);
        })->where(function($query){
            $query->where('additional_hire_confirms','>', 0)
            ->orWhere('additional_hire_requests','>', 0)
            ->orWhere('additional_hire_positions','>', 0);
        })->orderByDesc('updated_at')->get();
        return view('panel.company.package-index', compact('intern_packages','hire_packages','combined_packages','title'));
    }


    public function pay_config(Request $request)
    {
        $request->validate([
            'gateway' => 'required'
        ]);
        $package = PurchasePackage::find($request->package_id);
        $config = [
            'callback' => 'company.package.return-url',
            'amount' => $package->price,
            'paymenttitle' => 'خرید بسته خدماتی',
        ];
        $gateway = $request->gateway;
        $campany_package = $this->makeCompanyPackage($request);
        $payment = $this->makePayment($campany_package, $package, $gateway);

        $pay_info = [
            'config' => $config,
            'payment' => $payment,
            'gateway' => $gateway,
        ];

        return $this->PaymentController->Pay($pay_info);
    }

    public function makePayment($campany_package, $package, $gateway)
    {
        return Payment::create([
            'user_id' => Auth::id(),
            'order_id' => $campany_package->id,
            'order_type' => CompanyPackage::class,
            'price' => $package->price,
            'gateway' => $gateway
        ]);


    }

    public function makeCompanyPackage($request)
    {
        $campany_package = CompanyPackage::create([
            'company_id' => Auth::user()->groupable_id,
            'package_id' => $request->package_id,
        ]);

        return $campany_package;
    }

    public function receipt($paymentId){
        $payment = Payment::with('order')->find($paymentId);
        $campany_package = $payment->order;
        $campany_package->update([
            'status' => true,
            'receipt_id' => $payment->id,
        ]);

        $company = $payment->order->companyInfo;
        $package = $payment->order->package;
        $company->update([
            'confirm_internship_limit' => $package->additional_internship_confirms + $company->confirm_internship_limit,
            'request_internship_limit' => $package->additional_internship_requests + $company->request_internship_limit,
            'create_internship_limit' => $package->additional_internship_positions + $company->create_internship_limit,
            'confirm_hire_limit' => $package->additional_hire_confirms + $company->confirm_hire_limit,
            'request_hire_limit' => $package->additional_hire_requests + $company->request_hire_limit,
            'create_hire_limit' => $package->additional_hire_positions + $company->create_hire_limit,
        ]);
        
        $panel = $this->getpanel();

        return view('package.receipt' , compact(
            'payment',
            'panel',
        ));
    }

    protected function getpanel()
    {
        if(Auth::user()->groupable_type == CompanyInfo::class){
            return 'panel.company.master';
        }
        elseif(Auth::user()->groupable_type == StudentInfo::class){
            return 'panel.student.master';
        }
        elseif(Auth::user()->groupable_type == ApplicantInfo::class){
            return 'panel.student.master';
        }
    }


    public function verify($paymentId)
    {
        $resault = $this->PaymentController->verify($paymentId);
        if ($resault == true){
            $payment = Payment::find($paymentId);
            return redirect()->route('company.package.receipt', $payment->id )->with('success' , $payment->status);
        } else {
            $payment = Payment::find($paymentId);
            return redirect()->route('index.package.company')->with('error' , $payment->status);
        }

    }

}
