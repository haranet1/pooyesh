<?php

namespace App\Http\Controllers;

use App\Exports\CoAcceptedReqListExport;
use App\Exports\CoCanceledReqListExport;
use App\Exports\CompanyPackageTransactionExport;
use App\Exports\CoPooyeshReqListExport;
use App\Exports\CoRejectedReqListExport;
use App\Exports\DoneReqListExport;
use App\Exports\DynamicApplicantExport;
use App\Exports\DynamicCompanyExport;
use App\Exports\DynamicInternPositionExport;
use App\Exports\DynamicStudentExport;
use App\Exports\MbtiTransactionExport;
use App\Exports\StdAcceptedReqListExport;
use App\Exports\StdCanceledReqListExport;
use App\Exports\StdPooyeshReqListExport;
use App\Exports\StdRejectedReqListExport;
use App\Exports\UnconfirmedCompanyExport;
use App\Exports\UniAcceptedReqListExport;
use App\Exports\UniPrintedReqListExport;
use App\Exports\UniRejectedReqListExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function stdReqList()
    {
        return Excel::download(new StdPooyeshReqListExport , 'student-pooyesh-requests-list.xlsx');
    }

    public function stdAcceptedReqList()
    {
        return Excel::download(new StdAcceptedReqListExport , 'student-accepted-pooyesh-requests-list.xlsx');
    }

    public function stdCanceledReqList()
    {
        return Excel::download(new StdCanceledReqListExport , 'student-canceled-pooyesh-requests-list.xlsx');
    }

    public function stdRejectedReqList()
    {
        return Excel::download(new StdRejectedReqListExport , 'student-rejected-pooyesh-requests-list.xlsx');
    }

    public function coReqList()
    {
        return Excel::download(new CoPooyeshReqListExport , 'company-pooyesh-requests-list.xlsx');
    }

    public function coAcceptedReqList()
    {
        return Excel::download(new CoAcceptedReqListExport , 'company-accepted-pooyesh-requests-list.xlsx');
    }

    public function coCanceledReqList()
    {
        return Excel::download(new CoCanceledReqListExport , 'company-canceled-pooyesh-requests-list.xlsx');
    }

    public function coRejectedReqList()
    {
        return Excel::download(new CoRejectedReqListExport , 'company-rejected-pooyesh-requests-list.xlsx');
    }

    public function uniAcceptedReqList()
    {
        return Excel::download(new UniAcceptedReqListExport , 'university-accepted-pooyesh-requests-list.xlsx');
    }

    public function uniRejectedReqList()
    {
        return Excel::download(new UniRejectedReqListExport , 'university-rejected-pooyesh-requests-list.xlsx');
    }

    public function uniPrintedReqList()
    {
        return Excel::download(new UniPrintedReqListExport , 'university-printed-pooyesh-requests-list.xlsx');
    }

    public function doneReqList()
    {
        return Excel::download(new DoneReqListExport , 'done-pooyesh-requests-list.xlsx');
    }
    /**
     * un confirmed company export
     */
    public function unCompany()
    {
        return Excel::download(new DynamicCompanyExport('un' , 'شرکت های در انتظار بررسی') , 'unconfirmed-company.xlsx');
    }
    /**
     * accepted company export
     */
    public function acceptedCompany()
    {
        return Excel::download(new DynamicCompanyExport('accepted' , 'شرکت های تایید شده') , 'accepted-company.xlsx');
    }
    /**
     * rejected company export
     */
    public function rejectedCompany()
    {
        return Excel::download(new DynamicCompanyExport('RejectCompanyInfo' , 'شرکت های رد شده') , 'rejected-company.xlsx');
    }
    /**
     * un confirmed student export
     */
    public function unStudent()
    {
        return Excel::download(new DynamicStudentExport('un' , 'دانشجویان در انتظار بررسی') , 'unconfirmed-student.xlsx');
    }
    /**
     * accepted student export
     */
    public function acceptedStudent()
    {
        return Excel::download(new DynamicStudentExport('accepted' , 'دانشجویان تایید شده') , 'accepted-student.xlsx');
    }
    /**
     * rejected student export
     */
    public function rejectedStudent()
    {
        return Excel::download(new DynamicStudentExport('rejected' , 'دانشجویان رد شده') , 'rejected-student.xlsx');
    }
    /**
     * un confirmed applicant export
     */
    public function unApplicant()
    {
        return Excel::download(new DynamicApplicantExport('un' , 'کارجویان درانتظار بررسی') , 'unconfirmed-applicant.xlsx');
    }
    /**
     * accepted applicant export
     */
    public function acceptedApplicant()
    {
        return Excel::download(new DynamicApplicantExport('accepted' , 'کارجویان تایید شده') , 'accepted-applicant.xlsx');
    }
    /**
     * rejected applicant export
     */
    public function rejectedApplicant()
    {
        return Excel::download(new DynamicApplicantExport('rejected' , 'کارجویان رد شده') , 'rejected-applicant.xlsx');
    }
    /**
     * un confirmed Intern Posotion export
     */
    public function unInternPosition()
    {
        return Excel::download(new DynamicInternPositionExport('un' , 'موقعیت های کارآموزی در انتظار بررسی') , 'unconfirmed-intern-position.xlsx');
    }
    /**
     * accepted Intern Position export
     */
    public function acceptedInternPosition()
    {
        return Excel::download(new DynamicInternPositionExport('accepted' , 'موقعیت های کارآموزی تایید شده') , 'accepted-intern-position.xlsx');
    }
    /**
     * rejected Intern Position export
     */
    public function rejectedInternPosition()
    {
        return Excel::download(new DynamicInternPositionExport('rejected' , 'موقعیت های کارآموزی رد شده') , 'rejected-intern-position.xlsx');
    }

    public function mbtiTransactionList(Request $request)
    {
        return Excel::download(new MbtiTransactionExport($request->all()) , 'mbti-transaction-list.xlsx');
    }
    
    public function packageTransactionList(Request $request)
    {
        return Excel::download(new CompanyPackageTransactionExport($request->all()) , 'company-package-transaction-list.xlsx');
    }

}

