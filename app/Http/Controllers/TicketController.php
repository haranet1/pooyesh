<?php

namespace App\Http\Controllers;

use App\Helpers\EmployeeLogActivity;
use App\Models\ApplicantInfo;
use App\Models\Attachment;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\EmployeeInfo;
use App\Models\StudentInfo;
use App\Models\Ticket;
use App\Models\TicketReply;
use App\Models\User;
use App\Notifications\NewTicketSend;
use App\Notifications\UniRejectedPooyeshRequest;
use App\Notifications\UserRejected;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{
    public function compose()
    {
        $config = [
            'panel' => $this->getPanelType(),
            'title' => 'ارسال تیکت به پشتیبانی',
            'sendCount' => $this->getAllTicketsCount(),
        ];

        return view('tickets.compose' , compact(
            'config'
        ));
    }

    public function sendSup(Request $request)
    {
        $this->requestValidate($request);

        $category = 'Sup';

        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => null,
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => Auth::id(),
            'groupable_type' => User::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        $link = route('index.ticket' , $mainTicket->id);
        // $mainTicket->sender->notify(new NewTicketSend($link));

        return redirect()->route('inbox.ticket')->with('پیام مورد نظر با موفقعیت به پشتیبانی ارسال شد');
    }

    public function index($id)
    {
        
        $title = 'لیست تیکت ها';
        $ticket = Ticket::with('replies.attachment' , 'replies.sender.groupable')->find($id);

        if($ticket->status == 'send'){
            $ticket->status = 'seen';
            $ticket->save();
        }

        $config = [
            'panel' => $this->getPanelType(),
            'sendCount' => $this->getAllTicketsCount(),
        ];
        
        return view('tickets.index' , compact(
            'title',
            'ticket',
            'config',
        ));
    }

    public function inbox()
    {
        if(Auth::user()->groupable_type == EmployeeInfo::class){
            $title = 'لیست تیکت های پشتیبانی';
            $tickets = Ticket::SupportTickets()
            ->with('replies' , 'sender.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);
        }else{
            $title = 'لیست تیکت ها';
            $tickets = Ticket::MyInbox()
            ->with('replies' , 'sender.groupable')
            ->orderByDesc('updated_at')
            ->paginate(20);

        }
        

        $config = [
            'panel' => $this->getPanelType(),
            'sendCount' => $this->getAllTicketsCount(),
        ];


        return view('tickets.inbox' , compact(
            'title',
            'tickets',
            'config',
        ));
    }

    public function reg1Sent()
    {
        $title = 'لیست تیکت های رد کاربر';
        
        $tickets1 = Ticket::where('category' , 'Reg1')
            ->with('replies')
            ->orderByDesc('updated_at')
            ->get();

        
        $tickets2 = $tickets1->reject(function ($ticket) {
            return $ticket->receiver->groupable_type == CompanyInfo::class;
        });

        
        $ids = [];
        
        foreach($tickets2 as $ticket){
            array_push($ids, $ticket->id);
        }
        
        $tickets = Ticket::whereIn('id' , $ids)
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'panel' => $this->getPanelType(),
            'sendCount' => $this->getAllTicketsCount(),
        ];

        return view('tickets.inbox' , compact(
            'title',
            'tickets',
            'config',
        ));
    }

    public function reg2Sent()
    {
        $title = 'لیست تیکت های رد کاربر';
        
        $tickets = Ticket::where('category' , 'Reg2')
            ->with('replies')
            ->orderByDesc('updated_at')
            ->get();

        $tickets1 = Ticket::where('category' , 'Reg1')
            ->with('replies')
            ->orderByDesc('updated_at')
            ->get();

        
        $tickets2 = $tickets1->filter(function ($ticket) {
            return $ticket->receiver->groupable_type == CompanyInfo::class;
        });

        $tickets = $tickets->merge($tickets2);

        $ids = [];
        
        foreach($tickets as $ticket){
            array_push($ids, $ticket->id);
        }

        $tickets = Ticket::whereIn('id' , $ids)
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'panel' => $this->getPanelType(),
            'sendCount' => $this->getAllTicketsCount(),
        ];

        return view('tickets.inbox' , compact(
            'title',
            'tickets',
            'config',
        ));
    }

    public function iReqSent()
    {
        $title = 'لیست تیکت های رد درخواست های پویش';
        
        $tickets = Ticket::where('category' , 'iReq')
            ->with('replies')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'panel' => $this->getPanelType(),
            'sendCount' => $this->getAllTicketsCount(),
        ];

        return view('tickets.inbox' , compact(
            'title',
            'tickets',
            'config',
        ));
    }

    public function hReqSent()
    {
        $title = 'لیست تیکت های رد درخواست های استخدام';
        
        $tickets = Ticket::where('category' , 'hReq')
            ->with('replies')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'panel' => $this->getPanelType(),
            'sendCount' => $this->getAllTicketsCount(),
        ];

        return view('tickets.inbox' , compact(
            'title',
            'tickets',
            'config',
        ));
    }

    public function iPosSent()
    {
        $title = 'لیست تیکت های رد موقعیت های کارآموزی';
        
        $tickets = Ticket::where('category' , 'iPos')
            ->with('replies')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'panel' => $this->getPanelType(),
            'sendCount' => $this->getAllTicketsCount(),
        ];

        return view('tickets.inbox' , compact(
            'title',
            'tickets',
            'config',
        ));
    }

    public function hPosSent()
    {
        $title = 'لیست تیکت های رد موقعیت های شغلی';
        
        $tickets = Ticket::where('category' , 'hPos')
            ->with('replies')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $config = [
            'panel' => $this->getPanelType(),
            'sendCount' => $this->getAllTicketsCount(),
        ];

        return view('tickets.inbox' , compact(
            'title',
            'tickets',
            'config',
        ));
    }

    public function sentSup()
    {
        $title = 'لیست تیکت های ارسال شده';

        $tickets = Ticket::MyTickets()
            ->with('replies')
            ->orderByDesc('updated_at')
            ->paginate(20);

        // dd($tickets);
        
        $config = [
            'panel' => $this->getPanelType(),
            'sendCount' => $this->getAllTicketsCount(),
        ];

        return view('tickets.inbox' , compact(
            'title',
            'tickets',
            'config',
        ));
    }

    public function sendRelpy(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'attachment' => 'nullable|max:1024|mimes:png,jpg,jpeg,pdf',
        ],[
            'content.required' => 'متن الزامی است',
            'attachment.max' => 'حداکثر حجم 1 مگابایت است',
            'attachment.mimes' => 'فرمت فایل ارسال شده صحیح نیست',
        ]);

        $mainTicket = Ticket::find($request->ticket_id);
        $mainTicket->updated_at = now();
        $mainTicket->save();

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
        }
        $replyTicket->save();

        $link = route('index.ticket' , $mainTicket->id);
        
        if($mainTicket->receiver_id == Auth::id()){
            $receiver = $mainTicket->sender;
        }else{
            $receiver = $mainTicket->receiver;
        }

        // $receiver->notify(new NewTicketSend($link));
        
        return redirect()->back()->with('success' , 'پیام با موفقیت ارسال شد');
    }

    public function sendRejectCompany(Request $request)
    {
        $this->requestValidate($request);

        $company = User::with('groupable')->findOrFail($request->input('receiver_id'));
        $company->update(['status' => 2]);

        EmployeeLogActivity::addToLog(' حراست شرکت '. $company->groupable->name .' را رد کرد ');

        $category = 'Reg2';

        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->input('receiver_id'),
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('receiver_id'),
            'groupable_type' => User::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        $mainTicket->receiver->notify(new UserRejected(
            $mainTicket->receiver , $mainTicket->subject , $replyTicket->content , route('index.ticket', $mainTicket->id)
        ));

        EmployeeLogActivity::addToLog(' حراست به شرکت '. $company->groupable->name .' تیکت ارسال کرد ');
        return redirect()->back()->with('success' , ' شرکت ' . $company->groupable->name . ' رد شد و پیام مورد نظر ارسال شد ');
    }

    public function sendRejectCompanyInfo(Request $request)
    {
        
        $this->requestValidate($request);
        
        $company = User::with('groupable')->findOrFail($request->input('receiver_id'));
        $company->groupable->update(['status' => 0]);

        EmployeeLogActivity::addToLog(' کارشناس پویش شرکت '. $company->groupable->name .' را رد کرد ');

        $category = 'Reg1';

        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->input('receiver_id'),
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('receiver_id'),
            'groupable_type' => User::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        $mainTicket->receiver->notify(new UserRejected(
            $mainTicket->receiver , $mainTicket->subject , $replyTicket->content , route('index.ticket', $mainTicket->id)
        ));

        EmployeeLogActivity::addToLog(' کارشناس پویش به شرکت '. $company->groupable->name .' تیکت ارسال کرد ');
        return redirect()->back()->with('success' , ' شرکت ' . $company->groupable->name . ' رد شد و پیام مورد نظر ارسال شد ');
    }

    public function sendRejectCompanyAfterAccept(Request $request)
    {
        
        $this->requestValidate($request);
        
        $company = User::with('groupable')->findOrFail($request->input('receiver_id'));
        $company->groupable->update(['status' => 0]);
        $company->update(['status' => 2]);

        EmployeeLogActivity::addToLog(' کارشناس پویش شرکت '. $company->groupable->name .' را پس از تایید رد کرد ');

        $category = 'Reg2';

        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->input('receiver_id'),
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('receiver_id'),
            'groupable_type' => User::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        $mainTicket->receiver->notify(new UserRejected(
            $mainTicket->receiver , $mainTicket->subject , $replyTicket->content , route('index.ticket', $mainTicket->id)
        ));

        EmployeeLogActivity::addToLog(' کارشناس پویش به شرکت '. $company->groupable->name .' تیکت ارسال کرد ');
        return redirect()->back()->with('success' , ' شرکت ' . $company->groupable->name . ' رد شد و پیام مورد نظر ارسال شد ');
    }

    public function sendRejectCandidate(Request $request)
    {
        $this->requestValidate($request);
        
        $candidate = User::with('groupable')->findOrFail($request->input('receiver_id'));
        $candidate->update(['status' => 2]);

        EmployeeLogActivity::addToLog(' کارشناس پویش ثبت نام '. $candidate->name .' '. $candidate->family .' را رد کرد ');

        $category = 'Reg1';

        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->input('receiver_id'),
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('receiver_id'),
            'groupable_type' => User::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        $mainTicket->receiver->notify(new UserRejected(
            $mainTicket->receiver , $mainTicket->subject , $replyTicket->content , route('index.ticket', $mainTicket->id)
        ));

        EmployeeLogActivity::addToLog(' کارشناس پویش به کاربر '. $candidate->name .' '. $candidate->family .' تیکت ارسال کرد ');
        return redirect()->back()->with('success' , ' کاربر ' . $candidate->name .' '. $candidate->family . ' رد شد و پیام مورد نظر ارسال شد ');
    }

    public function sendRejectRequestByUni(Request $request)
    {
        $this->requestValidate($request);
        
        $Cooperationrequest = CooperationRequest::with('sender.groupable' , 'receiver.groupable')->find($request->request_id);

        $Cooperationrequest->process->rejected_by_uni_at = now();
        $Cooperationrequest->process->save();
        
        $Cooperationrequest->sender->notify(new UniRejectedPooyeshRequest());
        $Cooperationrequest->receiver->notify(new UniRejectedPooyeshRequest());
        EmployeeLogActivity::addToLog('درخواست پویش با شناسه '.$request->id.' را رد کرد');

        $category = '';
        
        if($Cooperationrequest->type = 'intern'){
            $category = 'iReq';
        }else{
            $category = 'hReq';
        }
       
        $mainTicket = Ticket::create([
            'category' => $category,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->input('receiver_id'),
            'subject' => $request->input('subject'),
            'status' => 'send',
            'groupable_id' => $request->input('request_id'),
            'groupable_type' => CooperationRequest::class,
        ]);

        $replyTicket = TicketReply::create([
            'parent_id' => $mainTicket->id,
            'sender_id' => Auth::id(),
            'content' => $request->input('content'),
        ]);

        if($request->file('attachment')){
            $name = time() . $request->file('attachment')->getClientOriginalName();
            $path = $request->file('attachment')->storeAs('img/ticket-attachment', $name, 'public');

            $attachment = Attachment::create([
                'name' => $name,
                'format' => $request->file('attachment')->extension(),
                'path' => $path,
            ]);

            $replyTicket->attachment_id = $attachment->id;
            $replyTicket->save();
        }

        $message = '';

        if($request->userType == 'std'){
            $message = ' درخواست دانشجو ' . $Cooperationrequest->sender->name .' '. $Cooperationrequest->sender->family . ' به شرکت ' . $Cooperationrequest->receiver->groupable->name . ' رد شد و پیام مورد نظر ارسال شد ';
        }else{
            $message = ' درخواست شرکت ' . $Cooperationrequest->sender->groupable->name . ' به دانشجو ' . $Cooperationrequest->receiver->name .' '. $Cooperationrequest->receiver->family . ' رد شد و پیام مورد نظر ارسال شد ';
        }

        return redirect()->back()->with('success' , $message);
    }

    protected function requestValidate($request)
    {
        $request->validate([
            'subject' => 'required',
            'content' => 'required',
            'attachment' => 'nullable|max:1024|mimes:png,jpg,jpeg,pdf',
        ],[
            'subject.required' => 'موضوع الزامی است',
            'content.required' => 'متن الزامی است',
            'attachment.max' => 'حداکثر حجم 1 مگابایت است',
            'attachment.mimes' => 'فرمت فایل ارسال شده صحیح نیست',
        ]);
    }

    protected function getPanelType()
    {
        if(Auth::user()->groupable instanceof CompanyInfo){
            return 'panel.company.master';
        }elseif(Auth::user()->groupable instanceof StudentInfo || Auth::user()->groupable instanceof ApplicantInfo){
            return 'panel.student.master';
        }elseif(Auth::user()->groupable instanceof EmployeeInfo){
            return 'admin.layouts.master';
        }
    }

    protected function getTicketSenderName($ticket)
    {
        if($ticket->sender->groupable_type == EmployeeInfo::class){
            return 'کارشناس پویش';
        }elseif($ticket->sender->groupable_type == CompanyInfo::class){
            return 'شرکت'. $ticket->sender->groupable->name;
        }
    }

    protected function getSendTicketCount($tickets)
    {
        $count = 0;
        foreach($tickets as $ticket){
            if($ticket->sender_id != Auth::id() && $ticket->status == 'send'){
                $count++;
            }
        }
        return $count;
    }

    protected function getAllTicketsCount()
    {
        $arr = array();

        $arr = [
            'MyInbox' => Ticket::MyInbox()->UnSeen()->count(),
            'sentSup' => Ticket::MyTickets()->UnSeen()->count(),
            'SupTickets' => Ticket::SupportTickets()->UnSeen()->count(),
            'reg1Sent' => $this->getReq1Count(),
            'reg2Sent' => $this->getReq2Count(),
            'iReqSent' => Ticket::where('category' , 'iReq')->UnSeen()->count(),
            'hReqSent' => Ticket::where('category' , 'hReq')->UnSeen()->count(),
            'iPosSent' => Ticket::where('category' , 'iPos')->UnSeen()->count(),
            'hPosSent' => Ticket::where('category' , 'hPos')->UnSeen()->count(),
        ];

        return $arr;
    }

    protected function getReq1Count()
    {
        $tickets1 = Ticket::where('category' , 'Reg1')->get();

        $tickets2 = $tickets1->reject(function ($ticket) {
            return $ticket->receiver->groupable_type == CompanyInfo::class;
        });

        
        $ids = [];
        
        foreach($tickets2 as $ticket){
            array_push($ids, $ticket->id);
        }
        
        return Ticket::whereIn('id' , $ids)->UnSeen()->count();
    }

    protected function getReq2Count()
    {
        $tickets = Ticket::where('category' , 'Reg2')
            ->with('replies')
            ->orderByDesc('updated_at')
            ->get();

        $tickets1 = Ticket::where('category' , 'Reg1')
            ->with('replies')
            ->orderByDesc('updated_at')
            ->get();

        
        $tickets2 = $tickets1->filter(function ($ticket) {
            return $ticket->receiver->groupable_type == CompanyInfo::class;
        });

        $tickets = $tickets->merge($tickets2);

        $ids = [];
        
        foreach($tickets as $ticket){
            array_push($ids, $ticket->id);
        }

        return Ticket::whereIn('id' , $ids)->UnSeen()->count();
    }

}
