<?php

namespace App\Http\Controllers;

use App\Http\Requests\storeApplicantInfoRequest;
use App\Models\ApplicantInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplicantController extends Controller
{
    public function storeApplicantInfo(storeApplicantInfoRequest $request)
    {
       if (!is_null(Auth::user()->groupable_id)) return redirect(route('home'));
        // $request->validate([
        //     'province' => 'required',
        //     'city' => 'required',
        //     'military_status' => 'required',
        //     'marital_status' => 'required',
        //     'requested_salary' => 'required',
        //     'birth' => 'required',
        //     'experience' => 'required',
        //     'address' => 'required',
        // ],[
        //     'province:required' => 'استان خود را وارد کنید',
        //     'city:required' => 'شهر خود را وارد کنید',
        //     'military_status' => 'وضعیت نظام وضیفه خود را وارد کنید',
        //     'marital_status' => 'وضعیت تاهل خود را وارد کنید',
        //     'requested_salary' => 'حقوق درخواستی خود را وارد کنید',
        //     'birth' => 'تاریخ تولد خود را وارد کنید',
        //     'experience' => 'سابقه کاری خود را وارد کنید',
        //     'address' => 'آدرس محل زندگی خود را وارد کنید',
        // ]);

        $info = ApplicantInfo::create([
            'province_id' => $request->province,
            'city_id' => $request->city,
            'military_status' => $request->military_status,
            'marital_status' => $request->marital_status,
            'requested_salary' => $request->requested_salary,
            'birth' => persian_to_eng_date($request->birth),
            'address' => $request->address,
            'experience' => $request->experience
        ]);

        Auth::user()->groupable_id = $info->id;
        Auth::user()->groupable_type = ApplicantInfo::class;
        Auth::user()->save();

        return redirect(route('std.cv.create'))->with('success',__('public.user_registered'));

    }
}
