<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProtectionController extends Controller
{
    /** company list for protection */
    public function coList()
    {
        $title = 'لیست شرکت های تایید نشده توسط حراست';

        $companies = User::ConfirmedCompany()
        ->with('groupable.logo' , 'groupable.newsletter')
        ->orderByDesc('updated_at')
        ->paginate(20);

        return view('admin.protection.company-list' , compact('companies' , 'title'));
    }

    public function rejectedCoList()
    {
        $title = 'لیست شرکت های رد شده توسط حراست';

        $companies = User::RejectedCompany()
        ->with('groupable.logo' , 'groupable.newsletter')
        ->orderByDesc('updated_at')
        ->paginate(20);

        return view('admin.protection.company-list' , compact('companies' , 'title'));
    }
}
