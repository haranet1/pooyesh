<?php

namespace App\Http\Controllers;

use App\Events\ProjectCreated;
use App\Helpers\StudentLogActivity;
use App\Models\AcademicInfo;
use App\Models\Announcement;
use App\Models\Certificate;
use App\Models\Comment;
use App\Models\CompanyInfo;
use App\Models\CompanyTicket;
use App\Models\CooperationRequest;
use App\Models\EnneagramPersonality;
use App\Models\Event;
use App\Models\File;
use App\Models\HaalandPesonality;
use App\Models\Idea;
use App\Models\Job_Category;
use App\Models\OjobPosition;
use App\Models\Lang;
use App\Models\MbtiPersonality;
use App\Models\News;
use App\Models\Photo;
use App\Models\PooyeshRequestProcess;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\Skill;
use App\Models\SocialNetwork;
use App\Models\student_skill;
use App\Models\StudentInfo;
use App\Models\Ticket;
use App\Models\User;
use App\Models\WorkExperience;
use App\Notifications\PooyeshProcessDone;
use App\Notifications\StdCanceledPooyeshRequest;
use App\Notifications\UniAcceptedPooyeshRequest;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Morilog\Jalali\Jalalian;

class StudentController extends Controller
{
    public function dashboard()
    {
        $companies = User::Company()->Confirmed()->count();
        $students = User::Student()->Confirmed()->count();
        $students+= User::Applicant()->Confirmed()->count();
        $ideas = Idea::get()->count();
        $projects = Project::get()->count();
        $files = File::whereNull('groupable_id')->ForStudents()->get();
        $announcements = Announcement::orderByDesc('id')->take(20)->get();
        $news = News::Active()->orderByDesc('id')->take(20)->get();
        $tickets = Ticket::where('sender_id',Auth::id())->orderByDesc('id')->take(20)->get();
        $job_position = OjobPosition::where('type','!=','intern')->get()->count();
        $intern =  OjobPosition::where('type','intern')->get()->count();

        

        $events = Event::IsActive()
            ->HasInfo()
            ->with(['infos' => function ($query) {
                $query->NotSoldOut()->RegistrationOpen();
            }])
            ->orderByDesc('updated_at')
            ->get();

        return view('panel.student.dashboard',
            compact(['companies','students','ideas','projects','announcements','news','tickets','files','job_position','intern' , 'events']));

    }

    public function ajax(Request $request)
    {
    
        switch ($request->do){
            case 'cancel-pooyesh-request-status':
                $req = CooperationRequest::findorfail($request->id);
                $req->process->update(['canceled_by_std_at'=>now()]);
                $req->sender->notify(new StdCanceledPooyeshRequest());
                StudentLogActivity::addToLog('درخواست پویش با شناسه '.$req->id.' را لغو کرد');

                return response()
              ->json('success');

            case 'done-pooyesh-request-status':
                $req = CooperationRequest::findorfail($request->id);

                Comment::create([
                    'user_id' =>Auth::id(),
                    'body' => $request->comment,
                    'commentable_id' =>$req->receiver->groupable_id,
                    'commentable_type' => CompanyInfo::class
                ]);

                $req->process->update(['done_at'=>now()]);
                $req->sender->notify(new PooyeshProcessDone());
                $req->receiver->notify(new PooyeshProcessDone());
                StudentLogActivity::addToLog('وضعیت درخواست پویش با شناسه '.$req->id.' را به اتمام تغییر داد');

                return response()
                    ->json('success');
            
            case 'get-skills':
                $skills = Skill::isSkill()->where('name','like', '%' . $request->skill_title . '%')->get();
                return response()->json($skills);

            case 'store-skills':
                if($request->has('data')){
                    Auth::user()->skills()->attach($request->data);
                    return response()->json(true);
                }
                
            case 'get-expertises':
                $expertises = Skill::isExpertise()->where('name','like', '%' . $request->expertises_title . '%')->get();
                return response()->json($expertises);

            case 'store-langs':
                if($request->has('data')){
                    Auth::user()->languages()->attach($request->data);
                    return response()->json(true);
                }

            case 'delete-certificate':
                $cert = Certificate::with('photo')->findorfail($request->id);
                if($cert->photo){
                    unlink($cert->photo->path);
                    $cert->photo->delete();
                }
                return response()->json($cert->delete());

            case 'store-experience':
                if($request->has('data')){
                    for($i=0;$i<count($request->data);$i++){
                        $info = WorkExperience::create([
                            'user_id' => Auth::id(),
                            'title' => $request->data[$i]['title'],
                            'company' => $request->data[$i]['company'],
                            'start_date' => persian_to_eng_date($request->data[$i]['start']),
                            'end_date' => persian_to_eng_date($request->data[$i]['end'])
                        ]);
                    }
                    
                }
                return response()->json(true);


            case 'store-socialNetworkArray':
                if($request->has('data')){
                    for($i=0;$i<count($request->data);$i++){
                        $info = SocialNetwork::create([
                            'user_id' => Auth::id(),
                            'name' => $request->data[$i]['title'],
                            'link' => $request->data[$i]['link'],
                        ]);
                    }
                }
                return response()->json(true);


            case 'delete-academic-info':
                return response()->json(AcademicInfo::find($request->id)->delete());


            case 'delete-student-skill':
                if($request->rel == 'skills'){
                    return response()->json(Auth::user()->skills()->detach($request->id));
                }elseif($request->rel == 'expertises'){
                    return response()->json(Auth::user()->expertises()->detach($request->id));
                }
                

            case 'delete-student-lang':
                return response()->json(Auth::user()->languages()->detach($request->id));


            case 'delete-student-experience':
                return response()->json(WorkExperience::find($request->id)->delete());


            case 'delete-student-socialNetworks':
                return response()->json(SocialNetwork::find($request->id)->delete());

            case 'set-open-work-status':
                $user = Auth::user();
                $user->groupable->open_to_work = $request->status;
                $user->groupable->save();
                
                return response()->json($user->groupable->open_to_work);  
                // return response()->json(Auth::user()->groupable->open_to_work = $request->status);
        }
    }

    public function search_ajax(Request $request)
    {
        switch ($request->do){
            case'search-companies':
                $companies = CompanyInfo::with('user','user.job_positions')
                    ->where('name','like','%'.$request->key.'%')
                    ->has('user.job_positions')
                    ->get();
                return response()->json(['data'=>$companies,200]);
        }
    }

    // public function storeStudentInfo(Request $request)
    // {
    //     $age = persian_to_eng_num($request->age);
    //     $n_code = persian_to_eng_num($request->n_code);
    //     $duration = persian_to_eng_num($request->duration);
    //     $std_number = persian_to_eng_num($request->std_number);

    //     $request->merge([
    //         'age'=>$age,
    //         'n_code' => $n_code,
    //         'duration' =>$duration,
    //         'std_number' =>$std_number
    //     ]);

    //     $request->validate([
    //         'age'=>'required|int',
    //         'grade'=>'required',
    //         'n_code'=>'required|numeric',
    //         'duration'=>'required|numeric',
    //         'major'=>'required',
    //         'university_id'=>'required',
    //         'province' => 'required',
    //         'city'=>'required',
    //         'email'=>'required|email',
    //         'std_number'=>'required|numeric'
    //         ],[
    //         'std_number.required'=>'لطفا شماره دانشجویی خود را وارد کنید',
    //         'std_number.numeric'=>'لطفا شماره دانشجویی خود را به صورت عدد وارد کنید',
    //         'age.required'=>'لطفا سن خود را وارد کنید',
    //         'n_code.numeric'=>'لطفا شماره ملی خود را به صورت عدد وارد کنید',
    //         'n_code.required'=>'لطفا شماره ملی خود را وارد کنید',
    //         'grade.required'=>'لطفا مقطع تحصیلی خود را وارد کنید',
    //         'duration.required'=>'لطفا تعداد ساعت دوره کارآموزی خود را وارد کنید',
    //         'duration.numeric'=>'لطفا تعداد ساعت دوره کارآموزی خود رابه صورت عدد وارد کنید',
    //         'major.required'=>'لطفا رشته تحصیلی خود را وارد کنید',
    //         'university_id.required'=>'لطفا دانشگاه محل تحصیل خود را وارد کنید',
    //         'province.required' => 'لطفا استان محل سکونت خود را انتخاب کنید',
    //         'city.required'=>'لطفا شهر محل سکونت خود را وارد کنید',
    //         'about.required'=>'لطفا اطلاعات خود را وارد کنید',
    //         'skills.required'=>'لطفا مهارتهای خود را وارد کنید',
    //         'email.required'=>'لطفا ایمیل خود را وارد کنید',
    //         'email.email'=>'لطفا ایمیل را به صورت صحیح وارد کنید',
    //     ]);

    //    $info = StudentInfo::create([
    //         'age' => $request->input('age'),
    //         'province_id' => $request->input('province'),
    //         'city_id' => $request->input('city'),
    //         'university_id' => $request->input('university_id'),
    //         'major' => $request->input('major'),
    //         'email' => $request->input('email'),
    //         'n_code' => $request->input('n_code'),
    //         'grade' => $request->input('grade'),
    //         'duration' => $request->input('duration'),
    //         'std_number' => $request->input('std_number'),
    //     ]);

    //     Auth::user()->groupable_id = $info->id;
    //     Auth::user()->groupable_type = StudentInfo::class;
    //    Auth::user()->save();

    //     \StudentLogActivity::addToLog('اطلاعات تکمیلی خود را ثبت کرد');

    //    return redirect(route('std.cv.create'))->with('success',__('public.user_registered'));
    // }

    public function create_cv()
    {
        \StudentLogActivity::addToLog('به صفحه ساخت رزومه آمد');
        // dd(Auth::user()->expertises);
        $langs = Lang::all();
        return view('panel.student.create-cv',compact(['langs']));
    }

    public function store_ticket(Request $request)
    {
        $request->validate([
            'subject'=>'required',
            'content'=>'required'
        ],[
            'subject.required'=>'لطفا موضوع را وارد کنید',
            'content.required'=>'لطفا متن را وارد کنید',

        ]);

        ticket::create([
            'sender_id'=>Auth::id(),
            'subject'=>$request->input('subject'),
            'content'=>$request->input('content'),
        ]);

        \StudentLogActivity::addToLog('یک تیکت پشتیبانی ارسال کرد');

        return redirect(route('students.tickets'))->with('success',__('public.ticket_sent'));


    }

    public function tickets()
    {
        \StudentLogActivity::addToLog('به صفحه لیست تیکت ها آمد');
        $tickets = ticket::where('sender_id',Auth::id())->notDeleted()->OrginalMessage()->paginate(20);
        // dd($tickets);
        return view('panel.student.tickets-list',compact('tickets'));
    }

    public function sendTikcetReply(Request $request)
    {
        $request->validate([
            'content' => 'required'
        ]);

        $ticket = ticket::findorfail($request->parent_id);
        $ticket->update(['read_at' => now()]);
        $ticket->replies()->create([
            'parent_id' => $request->parent_id,
            'sender_id' => Auth::id(),
            'receiver_id' => $ticket->sender_id,
            'subject' => 'reply',
            'content' => $request->input('content')
        ]);

        return redirect()->route('students.tickets')->with('success', __('public.reply-sent'));
    }

    public function companies_list()
    {
        \StudentLogActivity::addToLog('به صفحه لیست شرکت ها آمد');
        $companies = User::Company()->has('job_positions')->with('groupable','job_positions')->paginate(20);
        return view('panel.student.companies-list',compact('companies'));
    }

    public function sent_coo_request()
    {
        \StudentLogActivity::addToLog('به صفحه لیست درخواست های پویش آمد');

        $requests = CooperationRequest::where([['sender_id',Auth::id()],['type','intern']])->with('receiver','job')->paginate(20);
        return view('panel.student.sent-coo-request-list',compact('requests'));
    }

    public function sent_hire_request()
    {
        \StudentLogActivity::addToLog('به صفحه لیست درخواست های استخدام آمد');

        $requests = CooperationRequest::where([['sender_id',Auth::id()],['type','!=','intern']])->with('receiver','job')->paginate(20);
        return view('panel.student.sent-hire-request-list',compact('requests'));
    }

    public function received_intern_requests()
    {
        \StudentLogActivity::addToLog('به صفحه لیست درخواست های کارآموزی دریافت شده آمد');

        $requests = CooperationRequest::where([['receiver_id',Auth::id()],['type','intern']])->with('sender')->paginate(20);
        return view('panel.student.received-intern-requests-list',compact('requests'));
    }

    public function received_hire_requests()
    {
        \StudentLogActivity::addToLog('به صفحه لیست درخواست های استخدامی دریافت شده آمد');

        $requests = CooperationRequest::where([['receiver_id',Auth::id()],['type','!=','intern']])->with('sender','job')->paginate(20);
        return view('panel.student.received-intern-requests-list',compact('requests'));
    }

    public function create_project()
    {
        \StudentLogActivity::addToLog('به صفحه ایجاد پروژه آمد');

        $cats = ProjectCategory::where('owner_id',Auth::id())->get();
        if (count($cats) == 0)
            return redirect(route('std.projects.list'))->with('error','ابتدا باید یک یا چند دسته بندی برای پروژه ها ایجاد کنید');
        return view('panel.student.create-project',compact('cats'));
    }

    public function projects()
    {
        \StudentLogActivity::addToLog('به صفحه لیست پروژه ها آمد');

        $projects = Project::where('owner_id',Auth::id())->paginate(20);
        return view('panel.student.projects-list',compact('projects'));
    }

    public function create_project_cat()
    {
        \StudentLogActivity::addToLog('به صفحه ساخت گروه پروژه آمد');

        $cats = ProjectCategory::where('owner_id',Auth::id())->get();
        return view('panel.student.create-project-cat',compact('cats'));
    }

    public function store_project_cat(Request $request)
    {
        $cat =ProjectCategory::create([
            'owner_id'=>Auth::id(),
            'parent_id'=>$request->input('category'),
            'title'=>$request->input('title'),
        ]);

        \StudentLogActivity::addToLog('یک گروه پروژه جدید به نام '.$cat->title.' ثبت کرد');

        return redirect(route('std.projects.list'))->with('success','دسته بندی با موفقیت ذخیره شد');
    }

    public function store_project(Request $request)
    {
        $budget = persian_to_eng_num($request->budget);
        $request->merge(['budget'=>$budget]);
        $request->validate([
            'title'=>'required',
            'skills'=>'required',
            'budget'=>'required|int',
            'desc'=>'required',
        ],[
            'title.required' =>'عنوان پروژه الزامی است',
            'skills.required' =>'مهارت های مورد نیاز الزامی است',
            'budget.required' =>'بودجه الزامی است',
            'desc.required' =>'توضیحات الزامی است',
        ]);

        $project= Project::create([
            'owner_id'=>Auth::id(),
            'title'=>$request->input('title'),
            'category_id'=>$request->input('category'),
            'required_skills'=>$request->input('skills'),
            'budget'=>$request->input('budget'),
            'description'=>$request->input('desc'),
        ]);

        $owner = getUserNameByID(Auth::id());

        event( new ProjectCreated($request->title,$owner));

        \StudentLogActivity::addToLog('یک پروژه جدید به نام '.$project->title.' ثبت کرد');


        return redirect(route('std.projects.list'))->with('success','پروژه جدید با موفقیت اضافه شد');
    }

    public function info()
    {
        \StudentLogActivity::addToLog('به صفحه مشاهده مشخصات خود آمد');
        return view('panel.student.info');
    }

    public function sent_tickets_to_company()
    {
        \StudentLogActivity::addToLog('به صفحه لیست تیکت های ارسالی به شرکت ها آمد');

        $tickets = CompanyTicket::where('sender_id',Auth::id())->with('receiver')->paginate(20);
        
        return view('panel.student.company-tickets-list',compact('tickets'));
    }
    
    public function show_signature_upload_form()
    {
        \StudentLogActivity::addToLog('به صفحه آپلود امضا آمد');

        return view('panel.student.upload-signature');
    }

    public function upload_signature(Request $request)
    {
        $request->validate([
            'file'=>'required|image'
        ]);

        if ($request->file('file')){
            if (Auth::user()->groupable->signature){
                unlink(Auth::user()->groupable->signature->path);
                Auth::user()->groupable->signature->delete();
            }
            $name =time().$request->file->getClientOriginalName();
            $path = $request->file->storeAs('files/signatures',$name,'public');
            $signature = new Photo();
            $signature->name = $name;
            $signature->path = $path;
            $signature->groupable_type = StudentInfo::class;
            $signature->groupable_id = Auth::user()->groupable_id;
            $signature->save();
            \StudentLogActivity::addToLog('امضای خود را ثبت کرد');

            return redirect(route('std.panel'))->with('success',__('public.signature_added'));
        }
    }

    public function edit_profile()
    {
        \StudentLogActivity::addToLog('به صفحه ویرایش پروفایل آمد');

        $user = Auth::user();
        return view('panel.student.edit-profile',compact('user'));
    }

    public function update_profile(Request $request)
    {
        $request->validate([
            'mobile' => 'unique:users,mobile,'. Auth::user()->id,
            'email' => 'email,unique:users,email,'. Auth::user()->id
        ]);

        if (filled($request->password)){
            Auth::user()->password = Hash::make($request->password);
        }
        if (isset($request->mobile) && $request->mobile != Auth::user()->mobile){
            Auth::user()->update(['mobile_verified_at'=>null]);
            Auth::user()->mobile = $request->mobile;
        }
        if (isset($request->email) && $request->email != Auth::user()->email){
            Auth::user()->update(['email_verified_at'=>null]);
            Auth::user()->email = $request->email;
        }
        Auth::user()->name = $request->name;
        Auth::user()->family = $request->family;
        Auth::user()->save();

        \StudentLogActivity::addToLog('پروفایل خود را ویرایش کرد');

        return redirect(route('std.panel'))->with('success',__('public.data_added'));
    }
    // result of exams
    public function show_result_exams() 
    {
        if(Auth::user()->mbti_test){
            $mbtiResult = Auth::user()->mbti_test->personality_type;
            $personality_type = MbtiPersonality::where('title' , $mbtiResult)->first();
            Auth::user()->personality_type_mbti = $personality_type;
        }
        
        if(Auth::user()->enneagram_test){
            $enneagramResult = Auth::user()->enneagram_test->main_character;
            $personality_type = EnneagramPersonality::where('type' , $enneagramResult)->first();
            Auth::user()->personality_type_enneagram = $personality_type;
        }
        if(Auth::user()->haaland_test){
            $hallandResult = Auth::user()->haaland_test;
            $data = [
                'R' => $hallandResult['R'],
                'I' => $hallandResult['I'],
                'A' => $hallandResult['A'],
                'S' => $hallandResult['S'],
                'E' => $hallandResult['E'],
                'C' => $hallandResult['C'],
            ];
            arsort($data);

            $counter = 0;
            $personality = [];
            foreach($data as $key => $value){
                if($counter <= 2){
                    $personality[$counter] = HaalandPesonality::where('title' , $key)->first();
                    $counter++;
                }
            }
            Auth::user()->personality_type_haaland = $personality;
        }
        
        return view('panel.student.personalityTests.index-result');
    }
}
