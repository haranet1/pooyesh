<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'body' => 'required',
            'parent_id' => 'nullable|exists:comments,id',
        ]);

        $comment = new Comment();
        $comment->body = $request->body;
        $comment->user_id = auth()->id();
        $comment->commentable_id = $request->commentable_id;
        
        if (strpos($_SERVER['REQUEST_URI'],'mag')) {
            $comment->commentable_type = Article::class;
        }


        if ($request->has('parent_id')) {
            $comment->parent_id = $request->parent_id;
        }

        $comment->save();

        return redirect()->back()->with('message', 'Comment added successfully.');
    }

    public function index(){
        $title = 'مدیریت کامنت ها';
        $inProgress = Comment::where('status',0)->orderbyDesc('updated_at')->paginate(20);
        $rejected = Comment::where('status', 2)->orderbyDesc('updated_at')->paginate(20);
        $confirmed = Comment::where('status', 1)->orderbyDesc('updated_at')->paginate(20);
        return view('admin.magazine.comments.index', compact('title','inProgress','rejected','confirmed'));
    }

    public function reject(Request $request)
    {
        $comment = Comment::find($request->commentId);
        $comment->status = 2;
        $comment->save();
        return response()->json(['success',200]);
    }

    public function confirm(Request $request)
    {
        $comment = Comment::find($request->commentId);
        $comment->status = 1;
        $comment->save();
        return response()->json(['success',200]);
    }

}
