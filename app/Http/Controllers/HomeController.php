<?php

namespace App\Http\Controllers;

use App\Models\AcademicInfo;
use App\Models\ApplicantInfo;
use App\Models\City;
use App\Models\CompanyInfo;
use App\Models\CompanyTicket;
use App\Models\CooperationRequest;
use App\Models\Grader;
use App\Models\Idea;
use App\Models\Job;
use App\Models\Job_Category;
use App\Models\OjobPosition;
use App\Models\PooyeshRequestProcess;
use App\Models\Project;
use App\Models\Province;
use App\Models\Sadra;
use App\Models\StudentInfo;
use App\Models\User;
use App\Notifications\StdSentHireRequest;
use App\Notifications\StdSentInternRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $job_cats = Job_Category::has('job_positions')->with('job_positions')
        ->whereRelation('job_positions','status',1)->take(5)->get();

        $jobs = OjobPosition::Confirmed()->with('job','job.category','province','city')->take(10)->orderByDesc('updated_at')->get();
        $companies = User::Company()->with('groupable.logo')->Confirmed()->take(8)->orderByDesc('id')->get();
        $students_count = User::Confirmed()->where(function ($query) {
                $query->where('groupable_type', StudentInfo::class)
                ->orWhere('groupable_type', ApplicantInfo::class);
            })->get();
        $company_count = User::Company()->Confirmed()->get();
        $jobs_count = OjobPosition::Confirmed()->get();
        $mbti_test_user = User::whereHas('mbti_test')->count();
        $enneagram_test_user = User::whereHas('enneagram_test')->count();

        $intern_requests = CooperationRequest::where('type','intern')->with('sender','sender.groupable')->
        whereRelation('sender.groupable','groupable_type',StudentInfo::class)->get();

        $hire_requests = CooperationRequest::where('type','hire')->with('sender','sender.groupable')->
        whereRelation('sender.groupable','groupable_type',CompanyInfo::class)->get();
        $events = Sadra::NotExpired()->whereHas('banner')->with('banner')->get();

        return view('front.home',compact(['job_cats','jobs','companies','students_count','company_count','intern_requests','hire_requests','jobs_count','events','mbti_test_user','enneagram_test_user']));

    }

    public function registerAjax(Request $request)
    {
        switch ($request->do) {
            case'get-cities':
                $cities = DB::table('cities')->where('province', $request->province)->get(['name', 'id']);
                return response()->json($cities);
        }
    }

    public function jobs_ajax(Request $request)
    {
        
        switch ($request->do)
        {
            case 'get-jobs':
                $jobs = Job::where([['name', 'like', '%' . $request->job_title . '%']])->get();
                return response()->json($jobs);

            case 'get-jobs-by-cat':
                if($request->job_cat == 0){
                    $jobs = Job::where([['name', 'like', '%' . $request->job_title . '%']])->get();
                    return response()->json($jobs);
                }
                $jobs = Job::where([['category_id', $request->job_cat],['name', 'like', '%' . $request->job_title . '%']])->get();
                return response()->json($jobs);
            
            case'get-city' : 
                if($request->filled('province'))
                    return response()->json(City::where('province', $request->province)->get());
                
            case 'get-job-position':
                $req = $request->resultObj;
                $query = OjobPosition::query();
                $query = $query->Confirmed();
                $query = $query->with('job','job.category','company.groupable','company.groupable.logo','province','city');

                if($req['jobCategory'] != '*'){
                    $query->whereRelation('job.category', 'id', $req['jobCategory']);
                }
                if($req['jobName'] != '*'){
                    $query->where('title', $req['jobName']);
                }
                if($req['province'] != '*'){
                    $query->where('province_id', $req['province']);
                }
                if($req['city'] != '*'){
                    $query->where('city_id', $req['city'])->orWhere('city_id', '0');
                }
                if($req['jobType'] != '*'){
                    $query->where('type',$req['jobType']);
                }
                if($req['sex'] != '*'){
                    $query->where('sex',$req['sex'])->orWhere('sex', '0');
                }
                if($req['academicLevel'] != '*'){
                    $query->where('academic_level',$req['academicLevel'])->orWhere('academic_level', '0');
                }
                if($req['workExperience'] != '*'){
                    $workExperience = intval($req['workExperience']);
                    if($workExperience == 2){
                        $query->whereBetween('experience', [0 , 2]);
                    }
                    elseif($workExperience == 5){
                        $query->whereBetween('experience', [3 , 5]);
                    }
                    elseif($workExperience == 10){
                        $query->whereBetween('experience', [6 , 10]);
                    }
                    elseif($workExperience == 11){
                        $query->where('experience', '>' , 10);
                    }
                    else{
                        $query->where('experience',$req['workExperience'])->orWhere('experience', '0');
                    }
                }
                if($req['mariageType'] != '*'){
                    $query->where('marige_type',$req['mariageType'])->orWhere('marige_type', '0');
                }
                if($req['age'] != '*'){
                    $query->where('age',$req['age'])->orWhere('age', 0);
                }

                $result = $query->get();

                if(count($result) > 0){
                    foreach($result as $item){
                        $item->province_and_city = job_province_and_city_name($item->province,$item->city);
                        $item->created_at_fa = verta($item->created_at)->formatDifference();
                        $item->type = job_type_persian($item->type);
                    }
                }
                return response()->json($result);

        }
    }

    public function jobs_list(Request $request)
    {
        if (is_null($request->title))
            $jobs = OjobPosition::Confirmed()->with('city','province')->orderByDesc('updated_at')->paginate(30);
        else
            $jobs = OjobPosition::Confirmed()->with('city','province')->where('title', 'like', '%'.$request->title.'%')->orderByDesc('updated_at')->paginate(30);

        $job_cats = Job_Category::get();
        $provinces = Province::all();
        return view('front.jobs-list',compact(['jobs','job_cats','provinces']));

    }

    // public function jobs_list_by_category($category)
    // {
    //     $jobs = OjobPosition::Confirmed()->with('job','job.category','province','city')->whereRelation('job.category','name',$category)->paginate(30);
    //     $job_cats = Job_Category::get();

    //     $provinces = Province::all();
    //     return view('front.jobs-list',compact(['jobs','job_cats','provinces']));
    // }

    // public function jobs_list_by_type($type)
    // {
    //     $jobs = OjobPosition::Confirmed()->with('job','job.category','province','city')->where('type',$type)->paginate(30);
    //     $job_cats = Job_Category::get();
    //     $provinces = Province::all();
    //     return view('front.jobs-list',compact(['jobs','job_cats','provinces']));
    // }

    public function single_job($id)
    {
        $job = OjobPosition::with('skills','languages')->findorfail($id);
        $hasCooperationRequest = false;
        $cooperationRequests = CooperationRequest::where('job_position_id' , $job->id)->where('sender_id' , Auth::id())->get();
        if($cooperationRequests->count() > 0){
            $hasCooperationRequest = true;
        }else{
            $hasCooperationRequest = false;
        }
        $hasPermission = false;
        if(Auth::check()){
            if(Auth::user()->hasRole('employee')){
                
                $hasPermission = true;
            }
            if($job->IsConfirmed()){
                $hasPermission = true;
            }
        }
        $similar_jobs = OjobPosition::confirmed()->with('job.category')->where('id','<>',$id)
            ->whereHas('job.category',function ($query) use ($job){
                $query->where('id',$job->job->category_id);
            })
            ->get();
        return view('front.single-job',compact(['job','similar_jobs','hasPermission' , 'hasCooperationRequest']));
    }

    

    public function companies_list()
    {
        $companies = User::company()->confirmed()->with('job_positions')->orderByDesc('updated_at')->paginate(20);
        return view('front.companies-list',compact('companies'));
    }

    public function search_companies(Request $request)
    {
        $companies = User::company()->confirmed()->with('job_positions','groupable')
            ->whereRelation('groupable','name','like','%'.$request->name.'%')
            ->paginate(20);
        return view('front.companies-list',compact('companies'));
    }

    public function search_jobs(Request $request)
    {
        $jobs = null;
        if ($request->category == 0){
            $jobs = OjobPosition::Confirmed()->where('title','like','%'.$request->title.'%')->paginate(30);
        }else{
            $jobs = OjobPosition::Confirmed()->with('job','job.category')
                ->where('title','like','%'.$request->title.'%')
            ->whereHas('job.category',function ($query) use ($request){
                $query->where('id',$request->category);
            } )->paginate(30);
        }
        $job_cats = Job_Category::get();
        $provinces = Province::all();
        return view('front.jobs-list',compact(['jobs','job_cats','provinces']));
    }

    public function single_candidate($id)
    {
        $user = User::StudentAndApplicant()->with([
            'groupable',
            'skills',
            'academicInfos' => function ($query) {
                $query->orderBy('end_date', 'asc'); // بر اساس تاریخ شروع به صورت صعودی مرتب کنید
            },
            'expertises',
            'languages',
            'work_experiences' => function ($query) {
                $query->orderBy('start_date', 'asc'); //
            },
            'social_networks',
            'certificates'
        ])->findOrFail($id);

        $user_request = null;
        $job_positions = null;
        if (Auth::check() && Auth::user()->groupable_type == CompanyInfo::class){
            $user_request = CooperationRequest::where([['sender_id',Auth::id()],['receiver_id',$id]])->first();
            $job_positions = OjobPosition::where('company_id' , Auth::user()->id)->get();
        }
        // dd($job_positions);
        $show_info = show_student_info($id);
        $show_resume = show_student_resume($id);

        $isConfirmed = true;
        if($user->status == 0){
            $isConfirmed = false;
        }
        return view('front.candidates-single',compact(['user','user_request','show_info', 'show_resume','isConfirmed' ,'job_positions']));
    }

    public function projects_list()
    {
        $projects = Project::paginate(20);
        return view('front.project-list',compact('projects'));
    }

    public function single_project($id,$title)
    {
        $project = Project::findorfail($id);
        return view('front.project-single',compact('project'));
    }

    public function search_projects_by_cat($id)
    {
        $projects = Project::where('category_id',$id)->paginate(30);
        return view('front.project-list',compact('projects'));

    }

    public function search_projects(Request $request)
    {
        $projects = Project::where('title','like','%'.$request->key.'%')->paginate(30);
        return view('front.project-list',compact('projects'));
    }

    public function candidate_list()
    {
        $candidates = User::Confirmed()
        ->Student()
        ->with('groupable')
        ->orWhere(function($query){
            $query->ApplicantOpenToWork();
        })
        ->orderByDesc('updated_at')
        ->paginate(20);
        return view('front.candidates-list',compact('candidates'));
    }

    public function singleIdea($id)
    {
        $idea = Idea::with('owner')->findorfail($id);
        if ($idea->owner->groupable instanceof CompanyInfo){
            $idea->owner_name = $idea->owner->groupable->name;
            $idea->owner_type = 'company';
        }elseif ( $idea->owner->groupable  instanceof StudentInfo){
            $idea->owner_name = $idea->owner->name." ".$idea->owner->family;
            $idea->owner_type = 'student';
        }

        $idea->invested_percent = ($idea->invested_money * 100) / $idea->budget;
        return view('front.idea-single',compact('idea'));
    }

    public function ideas()
    {
        $ideas = Idea::paginate(20);
        if (count($ideas) > 0){
            foreach ($ideas as $idea){
                if ($idea->owner->groupable instanceof CompanyInfo){
                    $idea->owner_name = $idea->owner->groupable->name;
                    $idea->owner_type = 'company';
                }elseif ( $idea->owner->groupable  instanceof StudentInfo){
                    $idea->owner_name = $idea->owner->name." ".$idea->owner->family;
                    $idea->owner_type = 'student';
                }
            }
        }
        return view('front.idea-list',compact('ideas'));
    }

    public function search_ideas(Request $request)
    {
        $ideas = Idea::where('title','like','%'.$request->key.'%')->get();
        if (count($ideas) > 0){
            foreach ($ideas as $idea){
                if ($idea->owner->groupable instanceof CompanyInfo){
                    $idea->owner_name = $idea->owner->groupable->name;
                    $idea->owner_type = 'company';
                }elseif ( $idea->owner->groupable  instanceof StudentInfo){
                    $idea->owner_name = $idea->owner->name." ".$idea->owner->family;
                    $idea->owner_type = 'student';
                }
            }
        }
        return view('front.idea-list',compact('ideas'));
    }

    public function job_list_by_experience_type(Request $request)
    {
        $jobs = OjobPosition::where(function ($query) use ($request) {
            if (!empty($request->type)) {
                $query->whereIn('type', $request->type);
            }

            if (!empty($request->experience)) {
                $query->orWhere(function ($query) use ($request) {
                    $query->whereIn('experience', $request->experience);
                });
            }
        })->paginate(30);
        $job_cats = Job_Category::get();
        return view('front.jobs-list',compact(['jobs','job_cats']));
    }

    public function show_academic_guidance_form()
    {
        return view('front.academic-guidance-form');
    }

    public function store_academic_guidance(Request $request)
    {
        $mobile = persian_to_eng_num($request->mobile);
        $parent_mobile = persian_to_eng_num($request->parent_mobile);
        $national_code = persian_to_eng_num($request->national_code);

        $request->merge([
            'mobile'=>$mobile,
            'parent_mobile' => $parent_mobile,
            'national_code' =>$national_code
        ]);

        $request->validate([
            'name' => 'required',
            'family' => 'required',
            'mobile' => 'required|numeric',
            'parent_mobile' => 'required|numeric',
            'national_code' => 'required|numeric',
            'school' => 'required|string',
        ],[
            'family.required' => 'تکمیل فیلد نام خانوادگی الزامی است',
            'parent_mobile.required' => 'تکمیل فیلد تلفن همراه پدر یا مادر الزامی است',
            'school.required' => 'تکمیل فیلد نام مدرسه الزامی است',
            'school.string' => ' فیلد نام مدرسه باید به صورت حروف نوشته شود',
            'national_code.required' => 'تکمیل فیلد کد ملی الزامی است',
            'national_code.numeric' => ' فیلد کد ملی باید از نوع عددی باشد',
        ]);

        $student = Grader::create($request->all());
        return redirect(route('home'))->with('success',__('public.data_added'));
    }

}
