<?php

namespace App\Http\Controllers\PersonalityTests;

use PDF;
use App\Models\Payment;
use App\Models\Setting;
use App\Models\MbtiAnswer;
use App\Models\MbtiResult;
use Illuminate\Http\Request;
use App\Models\MbtiPersonality;
use Illuminate\Validation\Rule;
use App\Models\SuggestedTestJob;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\GeneralPaymentController;

class MBTIController extends Controller
{
    private $PaymentController;

    public function __construct(GeneralPaymentController $PaymentController)
    {
        $this->PaymentController = $PaymentController;
    }


    public function index()
    {
        return view('frontend.tests.mbti-info');
    }

    public function takeTest()
    {
        $que = MbtiAnswer::all();
        $questions = $que->mapWithKeys(function($value , $key){
            return [($key+1) => $value];
        });
            return view('panel.student.personalityTests.mbti-test' , compact('questions'));
    }

    public function testResult(Request $request)
    {
        $this->validateResult($request);
        $types = $this->calculateResult($request);
        $result = $this->getPersonalityType($types);
        $mbtiResult = $this->saveResult($result,$types);
        $personality_type = MbtiPersonality::where('title' ,$mbtiResult->personality_type)->first();
        $personality_type->other = explode(".",$personality_type->other);
        $price_career = Setting::getValue('career_matches_price');
        $price_major = Setting::getValue('major_matches_price');
        $vip = [];
        $jobs = null;
        $fields = null;
        return view('panel.student.personalityTests.mbti-result', compact(['mbtiResult' ,'personality_type','price_career','price_major',
        'vip', 'jobs', 'fields']));
    }

    public function validateResult($data)
    {
        $customMessage = [];
        $rules = [];
        for($i = 1; $i <= 60;$i++){
            $rules["q".$i] = 'required';
            $customMessage["q$i.required"] = 'لطفا به این سوال پاسخ دهید';
        }

       return $data->validate($rules,$customMessage);
    }

    public function calculateResult($data)
    {
        $types = [
            'I'=> 0,
            'E'=> 0,
            'N'=> 0,
            'S'=> 0,
            'P'=> 0,
            'J'=> 0,
            'F'=> 0,
            'T'=> 0,
        ];
        for($i = 1;$i <= 60;$i++){
            switch($data->input('q'.$i)){
                case 'I': $types['I']++;
                break;
                case 'E': $types['E']++;
                break;
                case 'N': $types['N']++;
                break;
                case 'S': $types['S']++;
                break;
                case 'P': $types['P']++;
                break;
                case 'J': $types['J']++;
                break;
                case 'F': $types['F']++;
                break;
                case 'T': $types['T']++;
                break;

            }
        }
        return $types;
    }

    public function getPersonalityType($types)
    {
        $result = [];
        if($types['I'] > $types['E']){
            array_push($result,'I');
        }else{
            array_push($result,'E');
        }
        if($types['N'] > $types['S']){
            array_push($result,'N');
        }else{
            array_push($result,'S');
        }
        if($types['T'] > $types['F']){
            array_push($result,'T');
        }else{
            array_push($result,'F');
        }
        if($types['P'] > $types['J']){
            array_push($result,'P');
        }else{
            array_push($result,'J');
        }
        return $result;
    }

    public function saveResult($result,$types)
    {
        $mbtiResult = null;
        if(Auth::user()->mbti_test()->exists()){
            $mbtiResult = Auth::user()->mbti_test;
        }else{
            $mbtiResult = new MbtiResult();
            $mbtiResult->user_id = Auth::id();
        }

        $strResult = '';
        foreach($result as $res){
            $strResult .= $res;
        }
        $mbtiResult->personality_type = $strResult;
        $mbtiResult->I = $types['I'];
        $mbtiResult->E = $types['E'];
        $mbtiResult->N = $types['N'];
        $mbtiResult->S = $types['S'];
        $mbtiResult->T = $types['T'];
        $mbtiResult->F = $types['F'];
        $mbtiResult->J = $types['J'];
        $mbtiResult->P = $types['P'];
        $mbtiResult->save();
        return $mbtiResult;
    }

    public function generatePDF($id)
    {
        $mbtiResult = MbtiResult::find($id);
        $personality_type = MbtiPersonality::where('title' ,$mbtiResult->personality_type)->first();
        $personality_type->other = explode(".",$personality_type->other);
        // return view('panel.student.personalityTests.mbti-result-pdf',compact(['mbtiResult','personality_type']));
        $view = view('panel.student.personalityTests.mbti-result-pdf' , compact(['mbtiResult','personality_type']));
        $pdf = PDF::loadHTML($view);
        return $pdf->download('mbti_result.pdf');
    }

    // public function generatePDFtest(Request $request)
    // {
    //     $img = $request->file;
    //     $folderPath = public_path().'/img/'; //path location
        
    //     $image_parts = explode(";base64,", $img);
    //     $image_type_aux = explode("image/", $image_parts[0]);
    //     $image_type = $image_type_aux[1];
    //     $image_base64 = base64_decode($image_parts[1]);
    //     $uniqid = uniqid();
    //     $file = $folderPath . $uniqid . '.'.$image_type;
    //     file_put_contents($file, $image_base64);
    //     // return Response::json( $response  );
    //     // dd($request);
    //     // $chartImageBase64 = $request->input('chartImage');
    //     // $chartImage = base64_decode($chartImageBase64);

    //     // $fileName = 'chart_image_' . time() . '.png';
    //     // $filePath = public_path('img/') . $fileName;

    //     // Storage::disk('public')->put('img/'. $fileName, $chartImage);
    //     // file_put_contents($filePath, $chartImage);
    //     // $name =time().$request->file->getClientOriginalName();
    //     // $path = $request->file->storeAs('img',$name,'public');
    //     return response()->json(true);
    // }

    public function showResult()
    {
        $mbtiResult = Auth::user()->mbti_test;
        $personality_type = MbtiPersonality::where('title' ,$mbtiResult->personality_type)->first();
        $personality_type->other = explode(".",$personality_type->other);
        $price_career = Setting::getValue('career_matches_price');
        $price_major = Setting::getValue('major_matches_price');

        $vip = [];
        $jobs = null;
        $fields = null;
        $result = MbtiResult::where('user_id', Auth::id())->with('suggestedJob')->first();
        if(!is_null($result)){
            foreach ($result->suggestedJob as $suggestedJob){
                if($result->updated_at <= $suggestedJob->updated_result){
                    if($suggestedJob->status == 1 && $suggestedJob->receipt_id != null){
                        if($suggestedJob->option == 'major'){
                            array_push($vip, 'major');
                        }
                        if($suggestedJob->option == 'career'){
                            array_push($vip, 'career');
                        }
                        if($suggestedJob->option == 'full'){
                            array_push($vip, 'major');
                            array_push($vip, 'career');
                        }
                        $jobs = $personality_type->jobs;
                        $fields = $personality_type->fields;
                    }
                }

            }
        }

        return view('panel.student.personalityTests.mbti-result', compact(['mbtiResult' ,'personality_type',
            'price_career', 'price_major', 'vip', 'jobs', 'fields' ]));
    }

    public function config(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'gateway' => 'required'
        ],[
            'type.required' => 'لطفا نوع خرید خود را انتخاب کنید',
            'gateway.required' => 'لطفا یک درگاه پرداخت انتخاب کنید'
        ]);

        $price_career = Setting::getValue('career_matches_price');
        $price_major = Setting::getValue('major_matches_price');
        $type = $request->type;

        if($type == 'career'){
            $total_price = $price_career;
        } 
        elseif($type == 'major'){
            $total_price = $price_major;
        }
        elseif($type == 'full'){
            $total_price = $price_major + $price_career;
        }

        $config = [
            'callback' => 'test.mbti.return-url',
            'amount' => $total_price,
            'paymenttitle' => 'خرید نتیجه تکمیلی تست mbti',
        ];
        $gateway = $request->gateway;
        $mbtiResult = Auth::user()->mbti_test;
        $suggested_test_job = $this->makeSuggestedTestJob($mbtiResult, $type);
        $payment = $this->makePayment($suggested_test_job, $total_price, $gateway);

        $pay_info = [
            'config' => $config,
            'payment' => $payment,
            'gateway' => $gateway,
        ];

        return $this->PaymentController->Pay($pay_info);
    }

    public function makeSuggestedTestJob($mbtiResult , $type)
    {
        $suggested_test_job = SuggestedTestJob::create([
            'user_id' => Auth::id(),
            'testable_id' => $mbtiResult->id,
            'testable_type' => MbtiResult::class,
            'option' => $type,
            'updated_result' => $mbtiResult->updated_at
        ]);

        return $suggested_test_job;
    }

    public function makePayment($suggested_test_job, $total_price, $gateway){
        return Payment::create([
            'user_id' => Auth::id(),
            'order_id' => $suggested_test_job->id,
            'order_type' => SuggestedTestJob::class,
            'price' => $total_price,
            'gateway' => $gateway
        ]);
    }

    public function verify($paymentId)
    {
        $resault = $this->PaymentController->verify($paymentId);
        if ($resault == true){
            $payment = Payment::find($paymentId);
            return redirect()->route('test.mbti.receipt', $payment->id )->with('success' , $payment->status);
        } else {
            $payment = Payment::find($paymentId);
            return redirect()->route('mbti.show.result')->with('error' , $payment->status);
        }

    }

    public function receipt($paymentId){
        $payment = Payment::with('order')->find($paymentId);
        $suggested_test_job = $payment->order;
        $suggested_test_job->update([

            'status' => true,
            'receipt_id' => $payment->id,
        ]);

        $panel = 'panel.student.master';

        return view('payment.receipt' , compact(
            'payment',
            'panel',
        ));
    }


}
