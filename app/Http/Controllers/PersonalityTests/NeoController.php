<?php

namespace App\Http\Controllers\PersonalityTests;

use App\Http\Controllers\Controller;
use App\Models\NeoPersonality;
use App\Models\NeoQuestion;
use App\Models\NeoResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NeoController extends Controller
{
    public function index()
    {
        return view('frontend.tests.neo-info');
    }

    public function takeTest()
    {
        $que = NeoQuestion::all();
        $questions = $que->mapWithKeys(function($value , $key){
            return [($key+1) => $value];
        });
        return view('panel.student.personalityTests.neo-test' , compact('questions'));
    }

    public function testResult(Request $request)
    {
        $this->validateResult($request);
        $types = $this->calculateResult($request);
        $results = $this->getPersonalityType($types); // text
        $saveResult = $this->saveResult($types); 
        $neoResult = Auth::user()->neo_test; // numbers
        return view('panel.student.personalityTests.neo-result' , compact(['results','neoResult']));
    }

    public function validateResult($data)
    {
        $customMessage = [];
        $rules = [];
        unset($data['_token']);

        $categories = [
            'N' => [1, 6, 11, 16, 21, 26, 31, 36, 41, 46, 51, 56],
            'E' => [2, 7, 12, 17, 22, 27, 32, 37, 42, 47, 52, 57],
            'O' => [3, 8, 13, 18, 23, 28, 33, 38, 43, 48, 53, 58],
            'A' => [4, 9, 14, 19, 24, 29, 34, 39, 44, 49, 54, 59],
            'C' => [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60],
        ];

        foreach ($categories as $category => $numbers) {
            foreach ($numbers as $col) {
                $name = $category . $col;
                $rules[$name] = 'required';
                $customMessage[$name . '.required'] = 'لطفا به این سوال پاسخ دهید';
            }
        }

        return $data->validate($rules, $customMessage);
    }

    public function calculateResult($data)
    {
        $types = [
            'N' => 0,
            'E' => 0,
            'O' => 0,
            'A' => 0,
            'C' => 0,
        ];

        $all = $data->all();
        unset($all['_token']);
        foreach ($all as $key => $value) {
            if (str_contains($key, 'N')) {
                $types['N'] += $value;
            }
            if (str_contains($key, 'E')) {
                $types['E'] += $value;
            }
            if (str_contains($key, 'O')) {
                $types['O'] += $value;
            }
            if (str_contains($key, 'A')) {
                $types['A'] += $value;
            }
            if (str_contains($key, 'C')) {
                $types['C'] += $value;
            }
        }
        return $types;
    }

    public function getPersonalityType($types)
    {
        $results = [
            'N' => '',
            'E' => '',
            'O' => '',
            'A' => '',
            'C' => '',
        ];
        $personalities = NeoPersonality::all();
        foreach($personalities as $personality){
            if( 0 <= $types[$personality['type']] && $types[$personality['type']] <= 12){
                $result[$personality['type']] = $personality['0_12'];
            }
            if( 13 <= $types[$personality['type']] && $types[$personality['type']] <= 24){
                $result[$personality['type']] = $personality['12_24'];
            }
            if( 25 <= $types[$personality['type']] && $types[$personality['type']] <= 48){
                $result[$personality['type']] = $personality['24_48'];
            }
        }
        return $result;
    }

    public function saveResult($types)
    {
        if(Auth::user()->neo_test()->exists()){
            $neoResult = Auth::user()->neo_test->update([
                'N' => $types['N'],
                'E' => $types['E'],
                'O' => $types['O'],
                'A' => $types['A'],
                'C' => $types['C'],
            ]);
        }else{
            $neoResult = NeoResult::create([
                'user_id' => Auth::id(),
                'N' => $types['N'],
                'E' => $types['E'],
                'O' => $types['O'],
                'A' => $types['A'],
                'C' => $types['C'],
            ]);
        }
        return $neoResult;
    }

    public function showResult()
    {
        $neoResult = Auth::user()->neo_test;
        $results = $this->getPersonalityType($neoResult);
        return view('panel.student.personalityTests.neo-result' , compact(['results','neoResult']));
    }
}
