<?php

namespace App\Http\Controllers\PersonalityTests;

use App\Http\Controllers\Controller;
use App\Models\GhqQuestion;
use App\Models\GhqResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GhqController extends Controller
{
    public function index()
    {
        return view('frontend.tests.ghq-info');
    }

    public function takeTest()
    {
        $que = GhqQuestion::all();
        $questions = $que->mapWithKeys(function($value , $key){
            return [($key+1) => $value];
        });
        return view('panel.student.personalityTests.ghq-test', compact('questions'));
    }

    public function testResult(Request $request)
    {
        $this->validateResult($request);
        $types = $this->calculateResult($request);
        $all = $this->calculateAll($types);
        $results = $this->getPersonalityType($types,$all); // text
        $saveResult = $this->saveResult($types,$all); // numbers
        $ghqResult = Auth::user()->ghq_test;
        return view('panel.student.personalityTests.ghq-result' , compact(['results','ghqResult']));
    }

    public function validateResult($data)
    {
        $customMessage = [];
        $rules = [];
        unset($data['_token']);

        $row = 'A';
        for ($col = 1; $col <= 28; $col++) {
            if ($col >= 8 && $col <= 14) {
                $row = 'B';
            }elseif ($col >= 15 && $col <= 21){
                $row = 'C';
            }elseif ($col >= 22 && $col <= 28){
                $row = 'D';
            }
            $name = $row . $col;
            $rules[$name] = 'required';
            $customMessage[$name . '.required'] = 'لطفا به این سوال پاسخ دهید';
        }

        return $data->validate($rules, $customMessage);
    }

    public function calculateResult($data)
    {
        $types = [
            'A' => 0,
            'B' => 0,
            'C' => 0,
            'D' => 0,
        ];

        $all = $data->all();
        unset($all['_token']);
        foreach ($all as $key => $value) {
            if($value == 'z') $value = 0;

            if (str_contains($key, 'A')) {
                $types['A'] += $value;
            }
            if (str_contains($key, 'B')) {
                $types['B'] += $value;
            }
            if (str_contains($key, 'C')) {
                $types['C'] += $value;
            }
            if (str_contains($key, 'D')) {
                $types['D'] += $value;
            }
        }
        return $types;
    }

    public function calculateAll($data)
    {
        return $data['A'] + $data['B'] + $data['C'] + $data['D'];
    }

    public function getPersonalityType($types,$all)
    {
        $typeRange = get_range_ghq_test($types);
        $all = get_range_of_all_ghq($all);
        $typeRange['all'] = $all;
        return $typeRange;
    }

    public function saveResult($types , $all)
    {
        if(Auth::user()->ghq_test()->exists()){
            $ghqResult = Auth::user()->ghq_test->update([
                'physical' => $types['A'],
                'anxiety' => $types['B'],
                'social' => $types['C'],
                'depression' => $types['D'],
                'sum_of_all' => $all,
            ]);
        }else{
            $ghqResult = GhqResult::create([
                'user_id' => Auth::id(),
                'physical' => $types['A'],
                'anxiety' => $types['B'],
                'social' => $types['C'],
                'depression' => $types['D'],
                'sum_of_all' => $all,
            ]);
        }
        return $ghqResult;
    }

    public function showResult()
    {
        $ghqResult = Auth::user()->ghq_test;
        $results = get_range_ghq_array($ghqResult,$ghqResult->sum_of_all);
        return view('panel.student.personalityTests.ghq-result' , compact(['results','ghqResult']));
    }
}
