<?php

namespace App\Http\Controllers\PersonalityTests;

use App\Http\Controllers\Controller;
use App\Http\Requests\EnneagramStep2Request;
use App\Models\EnneagramPersonality;
use App\Models\EnneagramQuestion;
use App\Models\EnneagramResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use PhpOffice\PhpSpreadsheet\Calculation\Category;

class EnneagramController extends Controller
{
    public function index()
    {
        return view('frontend.tests.enneagram-info');
    }

    public function takeTest()
    {
        $que = EnneagramQuestion::where('parent', null)->get();
        $questions = $que->mapWithKeys(function ($value, $key) {
            return [($key + 1) => $value];
        });
        return view('panel.student.personalityTests.enneagram-test', compact('questions'));
    }

    public function myValidate($data)
    {
        $customMessage = [];
        $rules = [];
        unset($data['_token']);

        $row = 'A';
        for ($col = 1; $col <= 60; $col++) {
            if ($col > 20 && $col < 40) {
                $row = 'B';
            }elseif ($col > 40){
                $row = 'C';
            }
            $name = $row . $col;
            $rules[$name] = 'required';
            $customMessage[$name . '.required'] = 'لطفا به این سوال پاسخ دهید';
        }

        return $data->validate($rules, $customMessage);
    }

    public function step_2(Request $request)
    {
        $this->myValidate($request);

        $types = [
            'A' => 0,
            'B' => 0,
            'C' => 0,
        ];

        $all = $request->all();
        unset($all['_token']);
        foreach ($all as $key => $value) {
            if($value == 'z') $value = 0;

            if (str_contains($key, 'A')) {
                $types['A'] += $value;
            }
            if (str_contains($key, 'B')) {
                $types['B'] += $value;
            }
            if (str_contains($key, 'C')) {
                $types['C'] += $value;
            }
        }

        //savedata from step 1
        if (Auth::user()->enneagram_test) {
            Auth::user()->enneagram_test->update([
                'A' => $types['A'],
                'B' => $types['B'],
                'C' => $types['C'],
            ]);
        } else {
            $enneagram_result = EnneagramResult::create([
                'user_id' => Auth::id(),
                'A' => $types['A'],
                'B' => $types['B'],
                'C' => $types['C'],
            ]);
        }


        $parent = array_keys($types, max($types));
        $que = EnneagramQuestion::where('parent', $parent[0])->get();
        $questions = $que->mapWithKeys(function ($value, $key) {
            return [($key + 1) => $value];
        });
        return view('panel.student.personalityTests.enneagram-test-2', compact('questions'));
    }

    public function result(Request $request)
    {
        $answers = $request->all();
        unset($answers['_token']);
        $types = [
            'perfectionist' => 0,
            'helpful' => 0,
            'progressive' => 0,
            'artist' => 0,
            'curious' => 0,
            'loyal' => 0,
            'optimist' => 0,
            'leader' => 0,
            'peaceful' => 0,
        ];
        foreach ($answers as $key => $value) {
            $type = preg_replace('/\d+/', '', $key);
            $types[$type] += intval($value);
        }
        $types = array_filter($types, function ($value) {
            return $value !== 0;
        });
        asort($types);
        $types_keys = array_keys($types);
        $enneagram = Auth::user()->enneagram_test;
        $enneagram->main_character = $types_keys[2];
        $enneagram->second_character = $types_keys[1];
        $enneagram->weak_character = $types_keys[0];
        $enneagram->main_character_score = $types[$enneagram->main_character];
        $enneagram->second_character_score = $types[$enneagram->second_character];
        $enneagram->weak_character_score = $types[$enneagram->weak_character];
        $enneagram->save();

        $personality = EnneagramPersonality::where('type', $enneagram->main_character)->first();
        return view('panel.student.personalityTests.enneagram-result', compact(['enneagram', 'personality']));
    }

    public function showResult()
    {
        $enneagram = Auth::user()->enneagram_test;
        $personality = EnneagramPersonality::where('type', $enneagram->main_character)->first();
        return view('panel.student.personalityTests.enneagram-result', compact(['enneagram', 'personality']));
    }

}
