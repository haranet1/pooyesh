<?php

namespace App\Http\Controllers\PersonalityTests;

use App\Http\Controllers\Controller;
use App\Models\HaalandAnswer;
use App\Models\HaalandPesonality;
use App\Models\HaalandResult;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HaalandController extends Controller
{
    public function index()
    {
        return view('frontend.tests.haaland-info');
    }

    public function takeTest()
    {
        $answers = [
            1 => [],
            2 => [],
            3 => [],
            4 => [],
            5 => [],
            6 => [],
            7 => [],
            8 => [],
            9 => [],
            10 => [],
            11 => [],
            12 => [],
            13 => [],
            14 => [],
            15 => [],
            16 => [],
            17 => [],
            18 => [],
        ];
        $all = HaalandAnswer::all();
        $total_items = $all->mapWithKeys(function($value , $key){
            return [($key+1) => $value];
        });
        foreach($total_items as $item){
            array_push($answers[$item['question_number']], $item);
        }
        return view('panel.student.personalityTests.haaland-test' , ['answers' => json_encode($answers)]);
    }

    public function testResult(Request $request)
    {
        $data = $this->sortData($request);
        $haalandResult = $this->saveResult($data);
        return response()->json(['redirect_url' => route('haaland.show.result')]);
    }

    public function sortData($request)
    {
        $result = $request['result'];
        arsort($result);
        return $result;
    }

    public function getPersonalityType($data)
    {
        $counter = 0;
        $personality = [];
        foreach($data as $key => $value){
            if($counter <= 2){
                $personality[$counter] = HaalandPesonality::where('title' , $key)->first();
                $counter++;
            }
        }
        return $personality;
    }

    public function saveResult($data)
    {
        if(Auth::user()->haaland_test()->exists()){
            $haalandResult = Auth::user()->haaland_test->update([
                'R' => $data['R'],
                'I' => $data['I'],
                'A' => $data['A'],
                'S' => $data['S'],
                'E' => $data['E'],
                'C' => $data['C'],
            ]);
        }else{
            $haalandResult = HaalandResult::create([
                'user_id' => Auth::id(),
                'R' => $data['R'],
                'I' => $data['I'],
                'A' => $data['A'],
                'S' => $data['S'],
                'E' => $data['E'],
                'C' => $data['C'],
            ]);
        }

        return $haalandResult;
        
    }

    public function sortResult($savedData)
    {
        $data = [
            'R' => $savedData['R'],
            'I' => $savedData['I'],
            'A' => $savedData['A'],
            'S' => $savedData['S'],
            'E' => $savedData['E'],
            'C' => $savedData['C'],
        ];
        arsort($data);
        return $data;
    }

    public function showResult(Request $request)
    {
        $haalandResult = Auth::user()->haaland_test;
        $sortResult = $this->sortResult($haalandResult);
        $personality = $this->getPersonalityType($sortResult);
        return view('panel.student.personalityTests.haaland-result', compact('personality', 'haalandResult'));
    }
}
