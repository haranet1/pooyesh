<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    

    public function edit()
    {
        $title = 'تنظیمات';
        $create_internship_limit = Setting::getValue('create_internship_limit');
        $request_internship_limit = Setting::getValue('request_internship_limit');
        $confirm_internship_limit = Setting::getValue('confirm_internship_limit');
        $create_hire_limit = Setting::getValue('create_hire_limit');
        $request_hire_limit = Setting::getValue('request_hire_limit');
        $confirm_hire_limit = Setting::getValue('confirm_hire_limit');
        $career_matches_price = Setting::getValue('career_matches_price');
        $major_matches_price = Setting::getValue('major_matches_price');

        return view('admin.settings.edit' , compact('create_internship_limit' , 
            'title', 
            'request_internship_limit', 
            'confirm_internship_limit' , 
            'career_matches_price',
            'major_matches_price',
            'create_hire_limit',
            'request_hire_limit',
            'confirm_hire_limit'
        ));
    }

    public function update(Request $request)
    {
        $request->validate([
            'create_internship_limit' => 'required|integer',
            'request_internship_limit' => 'required|integer',
            'confirm_internship_limit' => 'required|integer',
        ]);

        Setting::upsert([
            ['key' => 'create_internship_limit', 'value' => $request->input('create_internship_limit')],
            ['key' => 'request_internship_limit', 'value' => $request->input('request_internship_limit')],
            ['key' => 'confirm_internship_limit', 'value' => $request->input('confirm_internship_limit')],
            ['key' => 'career_matches_price', 'value' => $request->input('career_matches_price')],
            ['key' => 'major_matches_price', 'value' => $request->input('major_matches_price')],
        ], ['key'], ['value']);

        return redirect()->route('edit.setting.user')->with('success', 'تنظیمات با موفقیت به روزرسانی شد.');
    }
}

