<?php

namespace App\Http\Controllers;

use App\Events\JobPositionCreated;
use App\Events\ProjectCreated;
use App\Helpers\CompanyLogActivity;
use App\Models\Announcement;
use App\Models\ApplicantInfo;
use App\Models\BuyPlan;
use App\Models\City;
use App\Models\company_sadra;
use App\Models\CompanyInfo;
use App\Models\CompanyTicket;
use App\Models\CooperationRequest;
use App\Models\Event;
use App\Models\File;
use App\Models\Idea;
use App\Models\Job;
use App\Models\Job_Category;
use App\Models\Job_lang;
use App\Models\OjobPosition;
use App\Models\job_skill;
use App\Models\Lang;
use App\Models\News;
use App\Models\Payment;
use App\Models\Photo;
use App\Models\PooyeshRequestProcess;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\Province;
use App\Models\ResumeSent;
use App\Models\Sadra;
use App\Models\SadraBooth;
use App\Models\Skill;
use App\Models\StudentInfo;
use App\Models\User;
use App\Notifications\CoAcceptedPooyeshRequest;
use App\Notifications\CoCanceledPooyeshRequest;
use App\Notifications\CoRejectedPooyeshRequest;
use App\Notifications\NewJobPositionCreated;
use App\Notifications\NewPooyeshRequest;
use App\Notifications\PooyeshProcessDone;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use niklasravnsborg\LaravelPdf\PdfWrapper;

use function PHPUnit\Framework\isNull;

class CompanyController extends Controller
{
    public function dashboard()
    {
        $companies = User::Company()->Confirmed()->count();
        $students = User::StudentAndApplicant()->Confirmed()->count();
        $ideas = Idea::get()->count();
        $projects = Project::get()->count();
        $files = File::whereNull('groupable_id')->ForCompanies()->get();
        $announcements = Announcement::orderByDesc('id')->take(20)->get();
        $news = News::Active()->orderByDesc('id')->take(20)->get();
        $tickets = CompanyTicket::where('receiver_id', Auth::id())->Unread()->orderByDesc('id')->take(20)->get();
        $sadras = Sadra::NotExpired()->whereHas('banner')->with('banner')->get();

        $events = Event::IsActive()
            ->HasInfo()
            ->with(['infos' => function ($query) {
                $query->NotSoldOut()->RegistrationOpen();
            }])
            ->orderByDesc('updated_at')
            ->get();

        // dd($events);

        // foreach($events as $event){
        //     dd(asset($event->banner->path));
        // }

        $job_position = OjobPosition::where('type' , '!=', 'intern')->get()->count();
        $intern = OjobPosition::where('type' , 'intern')->get()->count();

        $company = Auth::user()->groupable;
        $internCount = OjobPosition::where('company_id', Auth::id())->where('type', 'intern')->count();
        $confirmed_request_intern = CooperationRequest::Intern()
        ->where('receiver_id' , Auth::id())
        ->Accepted()->get()->count();
        // dd($confirmed_request_intern);
        $sent_requests_intern = CooperationRequest::where('sender_id', Auth::id())->where('type' , 'intern')->count();

        $hireCount = OjobPosition::where('company_id', Auth::id())->where('type','!=' ,'intern')->count();
        $confirmed_request_hire = CooperationRequest::Hire()
        ->where('receiver_id' , Auth::id())
        ->Accepted()->get()->count();
        $sent_requests_hire = CooperationRequest::where('sender_id', Auth::id())->where('type', '!=', 'intern')->count();

        // dd(valueToPercent($internCount, $company->create_internship_limit ));
        $chartsData = [
            ['label' => 'ثبت موقعیت کارآموزی', 'series' => valueToPercent($internCount, $company->create_internship_limit ), 'value'=> max(0,$company->create_internship_limit - $internCount) . '/' . $company->create_internship_limit],
            ['label' => 'تایید درخواست کارآموز', 'series' => valueToPercent($confirmed_request_intern, $company->confirm_internship_limit), 'value'=> max(0,$company->confirm_internship_limit - $confirmed_request_intern) . '/' . $company->confirm_internship_limit],
            ['label' => 'ارسال درخواست به کارآموز', 'series' => valueToPercent($sent_requests_intern, $company->request_internship_limit), 'value' => max(0,$company->request_internship_limit - $sent_requests_intern) . '/' . $company->request_internship_limit],
            ['label' => 'ثبت موقعیت استخدامی', 'series' => valueToPercent($hireCount, $company->create_hire_limit), 'value' => max(0,$company->create_hire_limit - $hireCount) . '/' . $company->create_hire_limit],
            ['label' => 'تایید درخواست کارجو', 'series' => valueToPercent($confirmed_request_hire, $company->confirm_hire_limit), 'value' => max(0,$company->confirm_hire_limit - $confirmed_request_hire) . '/' . $company->confirm_hire_limit],
            ['label' => 'ارسال درخواست به کارجو', 'series' => valueToPercent($sent_requests_hire, $company->request_hire_limit), 'value' => max(0,$company->request_hire_limit - $sent_requests_hire) . '/' . $company->request_hire_limit],
        ];

        return view('panel.company.dashboard',
            compact([
                'companies', 'students', 'ideas', 'projects', 'files', 'announcements', 'news', 'tickets','sadras','job_position','intern',
                'events', 'chartsData'
            ]));
    }

    public function ajax(Request $request)
    {
        switch ($request->do) {
            // case'set-cooperation-request-status':
            //     $req = CooperationRequest::findorfail($request->id);
            //     $employees = User::Employee()->get();

            //     $req->status = $request->status;
            //     if ($request->status == 1) {
            //         foreach ($employees as $employee) {
            //             $employee->notify(new NewPooyeshRequest());
            //         }
            //         if ($req->process) {
            //             $req->process->accepted_by_co_at = now();
            //             $req->process->save();
            //         } else {
            //             $process = PooyeshRequestProcess::create([
            //                 'req_id' => $req->id,
            //                 'accepted_by_co_at' => now()
            //             ]);
            //         }
            //         $req->sender->notify(new CoAcceptedPooyeshRequest());
            //         \CompanyLogActivity::addToLog('یک درخواست پویش را تایید کرد');
            //     } else {
            //         if ($req->process) {
            //             $req->process->rejected_by_co_at = now();
            //             $req->process->save();
            //         } else {
            //             $process = PooyeshRequestProcess::create([
            //                 'req_id' => $req->id,
            //                 'rejected_by_co_at' => now()
            //             ]);
            //         }
            //         $req->sender->notify(new CoRejectedPooyeshRequest());
            //         \CompanyLogActivity::addToLog('یک درخواست پویش را رد کرد');
            //     }
            //     return response()->json($req->save());
            //     break;

            // case'cancel-pooyesh-request-status':
            //     $request = CooperationRequest::findorfail($request->id);
            //     $request->process->update(['canceled_by_co_at' => now()]);
            //     $request->sender->notify(new CoCanceledPooyeshRequest());
            //     \CompanyLogActivity::addToLog('یک درخواست پویش را لغو کرد');

            //     return response()
            //         ->json('success');

            case'done-pooyesh-request-status':
                $request = CooperationRequest::findorfail($request->id);
                $request->process->update(['done_at' => now()]);
                $request->sender->notify(new PooyeshProcessDone());
                $request->receiver->notify(new PooyeshProcessDone());
                \CompanyLogActivity::addToLog('وضعیت یک درخواست پویش را به اتمام تغییر داد');

                return response()
                    ->json('success');

            case'get-boot-by-plan-id' :
                $booths = SadraBooth::where('plan_id', $request->plan_id)->get();
                $plan = BuyPlan::with('map')->find($request->plan_id);
                $map = null;
                if ($plan->map) {
                    $map = $plan->map->path;
                }
                if (count($booths) > 0) return response()->json([
                    'booths' => $booths,
                    'map' => $map,
                    'price'=> $plan->price
                ], 200);
                else return response()->json(false);

            case'get-skills-by-cat-id':
                $skills = Skill::where('job_category_id', $request->cat_id)->get();
                return response()->json($skills);

            case'delete-job-position':
                \CompanyLogActivity::addToLog('یک موقعیت شغلی را حذف کرد');
                $jobPositoin = OjobPosition::find($request->id);
                $jobPositoin->skills()->detach();
                $jobPositoin->languages()->detach();
                return response()->json($jobPositoin->delete());
            case'get-jobs-by-cat-id':
                $jobs = Job::where([['category_id', $request->cat_id],['name','like', '%' . $request->job_title . '%']])->get();
                return response()->json($jobs);

            case'get-skills':
                $skills = Skill::where('name','like', '%' . $request->skill_title . '%')->get();
                return response()->json($skills);

            case'check-skill-exists':
                if($skill = Skill::where('name',$request->name)->first()){
                    return response()->json($skill);
                }else{
                   $skill = Skill::create([
                        'name' => $request->name,
                        'owner_id' => Auth::id(),
                    ]);
                    return response()->json($skill);
                }
            case'check-lang-exists':
                if($lang = Lang::where('name',$request->name)->first()){
                    return response()->json($lang);
                }else{
                    $lang = Lang::create([
                        'name' => $request->name,
                        'owner_id' => Auth::id(),
                    ]);
                    return response()->json($lang);
                }
            case'delete-skill-from-job-position' :
                if($request->filled('jobPositionId') && $request->filled('skillId')){
                    $jobPosition = OjobPosition::find($request->jobPositionId);
                    return response()->json($jobPosition->skills()->detach($request->skillId));
                }
                return response()->json('error');

            case'delete-lang-from-job-position' :
                if($request->filled('jobPositionId') && $request->filled('langId')){
                    $jobPosition = OjobPosition::find($request->jobPositionId);
                    return response()->json($jobPosition->languages()->detach($request->langId));
                }
                return response()->json('error');

            case'get-city' :
                if($request->filled('province'))
                    return response()->json(City::where('province', $request->province)->get());

            }


    }

    public function search_ajax(Request $request)
    {
        switch ($request->do) {
            case 'search-students':
                $students = User::StudentAndApplicant()
                    ->where(function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request->key . '%')
                            ->orwhere('family', 'like', '%' . $request->key . '%');
                    })
                    ->with('groupable')->get();
                return response()->json([
                    'data' => $students
                ]);

            case 'get-city':
                if($request->filled('province'))
                    return response()->json(City::where('province', $request->province)->get());

        }

    }

    public function studentResumeListShow()
    {
        $requests = ResumeSent::where([['receiver_id' , Auth::id()],['status', null]])->with('sender.groupable.province' , 'sender.groupable.city')->orderBy('updated_at','desc')->paginate(20);
        $acceptedRequests = ResumeSent::where([['receiver_id' , Auth::id()],['status', 1]])->orderBy('updated_at','desc')->paginate(20);
        return view('panel.company.sent-resume' , compact(['requests' , 'acceptedRequests']));
    }

    public function cooperate_requests()
    {
        \CompanyLogActivity::addToLog('به صفحه لیست درخواست های استخدام آمد');
        $requests = CooperationRequest::where([['receiver_id', Auth::id()], ['status', null], ['type','!=','intern']])->with('job')->paginate(20);
        $newRequests = CooperationRequest::where([['receiver_id', Auth::id()], ['status', 1], ['type','!=','intern']])->with('job')->paginate(20);

        return view('panel.company.coo-requests', compact(['requests','newRequests']));
    }

    public function company_info()
    {
        \CompanyLogActivity::addToLog('به صفحه مشخصات شرکت آمد');
        $info = Auth::user()->groupable;
        $user = Auth::user();
        $info->load('signature');
        return view('panel.company.info', compact('info','user'));

    }

    public function edit_company_info()
    {
        CompanyLogActivity::addToLog('به صفحه ویرایش اطلاعات شرکت آمد');
        
        $user = User::with('groupable')->find(Auth::id());
        // dd($user);
        return view('panel.company.edit-info', compact('user'));
    }

    public function update_company_info(Request $request)
    {

        $request->validate([
            'header' => 'mimes:jpeg,jpg,png|max:2048',
            'logo' => 'mimes:jpeg,jpg,png|max:1024',
            'phone' => 'required',
            'email' => 'required|email',
        ]);

        $info = Auth::user()->groupable;
        if ($request->about != "")
            $info->about = $request->input('about');
        $info->name = $request->input('name');
        $info->website = $request->input('website');
        $info->phone = $request->input('phone');
        $info->team = $request->input('team');
        $info->rewards = $request->input('rewards');
        $info->save();

        if(! $info->user->hasVerifiedEmail()){

            $info->user->email = $request->input('email');
            $info->user->save();
            
        }

        if($request->has('header')){
            $name = time() . $request->header->getClientOriginalName();
            $path = $request->header->storeAs('img/company', $name, 'public');

            $header = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $info->id,
                'groupable_type' => CompanyInfo::class,
            ]);

            $info->header_id = $header->id;
            $info->save();
        }

        if($request->has('logo')){
            $name = time() . $request->logo->getClientOriginalName();
            $path = $request->logo->storeAs('img/company', $name, 'public');

            $logo = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $info->id,
                'groupable_type' => CompanyInfo::class,
            ]);

            $info->logo_id = $logo->id;
            $info->save();
        }
        \CompanyLogActivity::addToLog('اطلاعات شرکت را ویرایش کرد');
        return redirect(route('company.info'))->with('success', 'اطلاعات شرکت بروزرسانی شد');
    }

    public function projects()
    {
        $projects = Project::where('owner_id', Auth::id())->paginate(20);
        \CompanyLogActivity::addToLog('به صفحه لیست پروه ها آمد');

        return view('panel.company.projects-list', compact('projects'));
    }

    public function create_project()
    {
        \CompanyLogActivity::addToLog('به صفحه ایجاد پروژه جدید آمد');

        $cats = ProjectCategory::where('owner_id', Auth::id())->get();
        if (count($cats) == 0)
            return redirect(route('company.projects.list'))->with('error', 'ابتدا باید یک یا چند دسته بندی برای پروژه ها ایجاد کنید');
        return view('panel.company.create-project', compact('cats'));
    }

    public function store_project(Request $request)
    {
        $budget = persian_to_eng_num($request->budget);
        $request->merge(['budget'=>$budget]);

        $request->validate([
            'title' => 'required',
            'skills' => 'required',
            'budget' => 'required|int',
            'desc' => 'required',
        ], [
            'title.required' => 'عنوان پروژه الزامی است',
            'skills.required' => 'مهارت های مورد نیاز الزامی است',
            'budget.required' => 'بودجه الزامی است',
            'desc.required' => 'توضیحات الزامی است',
        ]);

        Project::create([
            'owner_id' => Auth::id(),
            'title' => $request->input('title'),
            'category_id' => $request->input('category'),
            'required_skills' => $request->input('skills'),
            'budget' => $request->input('budget'),
            'description' => $request->input('desc'),
        ]);

        $owner = getUserNameByID(Auth::id());

        event(new ProjectCreated($request->title, $owner));
        \CompanyLogActivity::addToLog('یک پروژه جدید ثبت کرد');


        return redirect(route('company.projects.list'))->with('success', 'پروژه جدید با موفقیت اضافه شد');
    }

    public function create_project_cat()
    {
        \CompanyLogActivity::addToLog('به صفحه ایجاد گروه پروژه آمد');

        $cats = ProjectCategory::where('owner_id', Auth::id())->get();
        return view('panel.company.create-project-cat', compact('cats'));
    }

    public function store_project_cat(Request $request)
    {
        ProjectCategory::create([
            'owner_id' => Auth::id(),
            'parent_id' => $request->input('category'),
            'title' => $request->input('title'),
        ]);
        \CompanyLogActivity::addToLog('یک دسته بندی پروژه ایجاد کرد');

        return redirect(route('company.projects.list'))->with('success', 'دسته بندی با موفقیت ذخیره شد');
    }

    public function applicants_list()
    {
        \CompanyLogActivity::addToLog('به صفحه لیست کارجویان آمد');

        $students = User::Confirmed()
        ->ApplicantOpenToWork()
        ->with(['groupable.city','groupable.province','academicInfos'])
        ->paginate(20);

        // dont need this ||>
        // foreach ($students as $std){
        //     if($std->academicInfos->isNotEmpty()){
        //         $latestAcademicInfo= $std->academicInfos->sortByDesc(function ($academicInfo){
        //             return intval($academicInfo->end_date);
        //         })->first();
        //         // last grade
        //         $std->lastGrade = $latestAcademicInfo->grade;
        //         $std->lastEndDate = $latestAcademicInfo->end_date;
        //         // last major
        //         $std->lastMajor = $latestAcademicInfo->major;
        //         $std->lastUniversity = $latestAcademicInfo->university;
        //         // age
        //         $birthDate = Carbon::parse($std->groupable->birth);
        //         if($std->hasRole('student')){
        //             $std->newAge = $std->age;
        //         }else{
        //             $std->newAge = $birthDate->diffInYears(Carbon::now());
        //         }

        //     }else{
        //         $std->LastGrade = '';
        //         $std->lastEndDate = null;
        //     }
        // }

        $provinces = Province::all();
        $citys = City::all();
        return view('panel.company.applicants-list', compact(['students','provinces','citys']));
    }

    public function students_list()
    {
        \CompanyLogActivity::addToLog('به صفحه لیست کارجویان آمد');

        $students = User::student()->Confirmed()
        ->with(['groupable.city','groupable.province','academicInfos'])->paginate(20);


        foreach ($students as $std){
            if($std->academicInfos->isNotEmpty()){
                $latestAcademicInfo= $std->academicInfos->sortByDesc(function ($academicInfo){
                    return intval($academicInfo->end_date);
                })->first();
                // last grade
                $std->lastGrade = $latestAcademicInfo->grade;
                $std->lastEndDate = $latestAcademicInfo->end_date;
                // last major
                $std->lastMajor = $latestAcademicInfo->major;
                $std->lastUniversity = $latestAcademicInfo->university;
                // age
                $birthDate = Carbon::parse($std->groupable->birth);
                if($std->hasRole('student')){
                    $std->newAge = $std->age;
                }else{
                    $std->newAge = $birthDate->diffInYears(Carbon::now());
                }

            }else{
                $std->LastGrade = '';
                $std->lastEndDate = null;
            }
        }

        $provinces = Province::all();
        $citys = City::all();
        return view('panel.company.students-list', compact(['students','provinces','citys']));
    }

    public function sadra_events()
    {
        $events = Sadra::with('map')->orderByDesc('created_at')->paginate(20);
        $company_sadra = company_sadra::where('company_id', Auth::user()->groupable_id)->get();
        \CompanyLogActivity::addToLog('به صفحه لیست رویدادها / نمایشگاه ها آمد');
        return view('panel.company.sadra-events', compact('events','company_sadra'));
    }

    public function sadraRegisterShow(Request $request)
    {
        // dd($request->all());
        $event = Sadra::where('id' , $request->event)->with('map')->first();
        return view('panel.company.sadra-reg', compact('event'));
    }

    public function sadra_register(Request $request)
    {
        $allData = [
            'sadra' => '',
            'plan' => '',
            'booth' => '',
            'coordinator' => '',
            'coordinatorMobile' => '',
            'contract' => '',
            'inviter' => '',
            'dontWantEngineer' => 0,
            'electricalEngineerCount' => 0,
            'mechanicalEngineerCount' => 0,
            'materialEngineerCount' => 0,
            'industriallEngineerCount' => 0,
            'civilEngineerCount' => 0,
            'computerEngineerCount' => 0,
            'medicalEngineerCount' => 0,
            'otherCasesEngineerCount' => 0,
            'dontWantHumanities' => 0,
            'managementCount' => 0,
            'rightsCount' => 0,
            'accountingCount' => 0,
            'literatureCount' => 0,
            'englishCount' => 0,
            'historyAndGeographyCount' => 0,
            'otherCasesHumanitiesCount' => 0,
            'dontWantMedical' => 0,
            'medicalCount' => 0,
            'nursingCount' => 0,
            'midwiferyCount' => 0,
            'surgeryRoomCount' => 0,
            'hygieneCount' => 0,
            'psychologyCount' => 0,
            'otherCasesMedicalCount' => 0,
            'dontWantScience' => 0,
            'mathCount' => 0,
            'physicsCount' => 0,
            'chemistryCount' => 0,
            'biologyCount' => 0,
            'otherCasesScienceCount' => 0,
            'dontWantMarketing' => 0,
            'generalCount' => 0,
            'expertiseCount' => 0,
            'student' => 0,
            'graduate' => 0,
            'adviceFromProfessors' => 0,
            'otherCasesCooperation' => 0,
        ];
        $data = $request->all();
        $booth = SadraBooth::where('id', $data['booth'])->first();
        // dd($booth);
        $company = CompanyInfo::where('id', Auth::user()->groupable_id)->first();
        $allData['sadra'] = $data['sadra'];
        $allData['plan'] = $data['plan'];
        $allData['booth'] = $data['booth'];
        $allData['coordinator'] = $data['coordinator'];
        $allData['coordinatorMobile'] = $data['coordinatorMobile'];
        $allData['contract'] = $data['contract'];
        $allData['inviter'] = $data['inviter'];

        if(!isset($data['dontWantEngineer'])){
            if($data['electricalEngineerCount'] != 0){
                $allData['electricalEngineerCount'] = $data['electricalEngineerCount'];
            }
            if($data['mechanicalEngineerCount'] != 0){
                $allData['mechanicalEngineerCount'] = $data['mechanicalEngineerCount'];
            }
            if($data['materialEngineerCount'] != 0){
                $allData['materialEngineerCount'] = $data['materialEngineerCount'];
            }
            if($data['industriallEngineerCount'] != 0){
                $allData['industriallEngineerCount'] = $data['industriallEngineerCount'];
            }
            if($data['civilEngineerCount'] != 0){
                $allData['civilEngineerCount'] = $data['civilEngineerCount'];
            }
            if($data['computerEngineerCount'] != 0){
                $allData['computerEngineerCount'] = $data['computerEngineerCount'];
            }
            if($data['medicalEngineerCount'] != 0){
                $allData['medicalEngineerCount'] = $data['medicalEngineerCount'];
            }
            if($data['otherCasesEngineerCount'] != 0){
                $allData['otherCasesEngineerCount'] = $data['otherCasesEngineerCount'];
            }
        }else{
            $allData['dontWantEngineer'] = 1;
        }
        if(!isset($data['dontWantHumanities'])){
            if($data['managementCount'] != 0){
                $allData['managementCount'] = $data['managementCount'];
            }
            if($data['rightsCount'] != 0){
                $allData['rightsCount'] = $data['rightsCount'];
            }
            if($data['accountingCount'] != 0){
                $allData['accountingCount'] = $data['accountingCount'];
            }
            if($data['literatureCount'] != 0){
                $allData['literatureCount'] = $data['literatureCount'];
            }
            if($data['englishCount'] != 0){
                $allData['englishCount'] = $data['englishCount'];
            }
            if($data['historyAndGeographyCount'] != 0){
                $allData['historyAndGeographyCount'] = $data['historyAndGeographyCount'];
            }
            if($data['otherCasesHumanitiesCount'] != 0){
                $allData['otherCasesHumanitiesCount'] = $data['otherCasesHumanitiesCount'];
            }
        }else{
            $allData['dontWantHumanities'] = 1;
        }
        if(!isset($data['dontWantMedical'])){
            if($data['medicalCount'] != 0){
                $allData['medicalCount'] = $data['medicalCount'];
            }
            if($data['nursingCount'] != 0){
                $allData['nursingCount'] = $data['nursingCount'];
            }
            if($data['midwiferyCount'] != 0){
                $allData['midwiferyCount'] = $data['midwiferyCount'];
            }
            if($data['surgeryRoomCount'] != 0){
                $allData['surgeryRoomCount'] = $data['surgeryRoomCount'];
            }
            if($data['hygieneCount'] != 0){
                $allData['hygieneCount'] = $data['hygieneCount'];
            }
            if($data['psychologyCount'] != 0){
                $allData['psychologyCount'] = $data['psychologyCount'];
            }
            if($data['otherCasesMedicalCount'] != 0){
                $allData['otherCasesMedicalCount'] = $data['otherCasesMedicalCount'];
            }
        }else{
            $allData['dontWantMedical'] = 1;
        }
        if(!isset($data['dontWantScience'])){
            if($data['mathCount'] != 0){
                $allData['mathCount'] = $data['mathCount'];
            }
            if($data['physicsCount'] != 0){
                $allData['physicsCount'] = $data['physicsCount'];
            }
            if($data['chemistryCount'] != 0){
                $allData['chemistryCount'] = $data['chemistryCount'];
            }
            if($data['biologyCount'] != 0){
                $allData['biologyCount'] = $data['biologyCount'];
            }
            if($data['otherCasesScienceCount'] != 0){
                $allData['otherCasesScienceCount'] = $data['otherCasesScienceCount'];
            }
        }else{
            $allData['dontWantScience'] = 1;
        }
        if(!isset($data['dontWantMarketing'])){
            if($data['generalCount'] != 0){
                $allData['generalCount'] = $data['generalCount'];
            }
            if($data['expertiseCount'] != 0){
                $allData['expertiseCount'] = $data['expertiseCount'];
            }
        }else{
            $allData['dontWantMarketing'] = 1;
        }
        if(isset($data['student'])){
            $allData['student'] = 1;
        }
        if(isset($data['graduate'])){
            $allData['graduate'] = 1;
        }
        if(isset($data['adviceFromProfessors'])){
            $allData['adviceFromProfessors'] = 1;
        }
        if(isset($data['otherCasesCooperation'])){
            $allData['otherCasesCooperation'] = 1;
        }

        $company_sadra = company_sadra::where('company_id', Auth::user()->groupable_id)->where('sadra_id',$data['sadra'])->first();
        // dd($company_sadra);
        if(!$company_sadra){
            $booth->update([
                'reserved' => true,
            ]);

            if($request->has('logo')){
                $name = time() . $request->logo->getClientOriginalName();
                $path = $request->logo->storeAs('img/company', $name, 'public');

                $logo = Photo::create([
                    'name' => $name,
                    'path' => $path,
                    'groupable_id' => $company->id,
                    'groupable_type' => CompanyInfo::class,
                ]);

                $company->logo_id = $logo->id;
                $company->save();
            }
            $companySadra = company_sadra::create([
                'company_id' => Auth::user()->groupable_id,
                'sadra_id' => $allData['sadra'],
                'booth_id' => $allData['booth'],
                'coordinator_name' => $allData['coordinator'],
                'coordinator_mobile' => $allData['coordinatorMobile'],
                'contract' => $allData['contract'],
                'inviter' => $allData['inviter'],
                'dont_want_engineer' => $allData['dontWantEngineer'],
                'electrical_engineer' => $allData['electricalEngineerCount'],
                'mechanical_engineer' => $allData['mechanicalEngineerCount'],
                'material_engineer' => $allData['materialEngineerCount'],
                'industriall_engineer' => $allData['industriallEngineerCount'],
                'civil_engineer' => $allData['civilEngineerCount'],
                'computer_engineer' => $allData['computerEngineerCount'],
                'medical_engineer' => $allData['medicalEngineerCount'],
                'other_cases_engineer' => $allData['otherCasesEngineerCount'],
                'dont_want_humanities' => $allData['dontWantHumanities'],
                'management' => $allData['managementCount'],
                'rights' => $allData['rightsCount'],
                'accounting' => $allData['accountingCount'],
                'literature' => $allData['literatureCount'],
                'english' => $allData['englishCount'],
                'history_and_geography' => $allData['historyAndGeographyCount'],
                'other_cases_humanities' => $allData['otherCasesHumanitiesCount'],
                'dont_want_medical' => $allData['dontWantMedical'],
                'medical' => $allData['medicalCount'],
                'nursing' => $allData['nursingCount'],
                'midwifery' => $allData['midwiferyCount'],
                'surgeryRoom' => $allData['surgeryRoomCount'],
                'hygiene' => $allData['hygieneCount'],
                'psychology' => $allData['psychologyCount'],
                'other_cases_medical' => $allData['otherCasesMedicalCount'],
                'dont_want_science' => $allData['dontWantScience'],
                'math' => $allData['mathCount'],
                'physics' => $allData['physicsCount'],
                'chemistry' => $allData['chemistryCount'],
                'biology' => $allData['biologyCount'],
                'other_cases_science' => $allData['otherCasesScienceCount'],
                'dont_want_marketing' => $allData['dontWantMarketing'],
                'general' => $allData['generalCount'],
                'expertise' => $allData['expertiseCount'],
                'student' => $allData['student'],
                'graduate' => $allData['graduate'],
                'advice_from_professors' => $allData['adviceFromProfessors'],
                'other_cases_cooperation' => $allData['otherCasesCooperation'],
            ]);

        }else{
            if($data['booth'] != $company_sadra->booth_id){
                $lastBooth = SadraBooth::where('id', $company_sadra->booth_id)->first();
                $lastBooth->update(['reserved' => false]);
                $booth->update([
                    'reserved' => true,
                ]);
            }

            $company_sadra->update([
                'company_id' => Auth::user()->groupable_id,
                'sadra_id' => $allData['sadra'],
                'booth_id' => $allData['booth'],
                'status' => null,
                'coordinator_name' => $allData['coordinator'],
                'coordinator_mobile' => $allData['coordinatorMobile'],
                'contract' => $allData['contract'],
                'inviter' => $allData['inviter'],
                'dont_want_engineer' => $allData['dontWantEngineer'],
                'electrical_engineer' => $allData['electricalEngineerCount'],
                'mechanical_engineer' => $allData['mechanicalEngineerCount'],
                'material_engineer' => $allData['materialEngineerCount'],
                'industriall_engineer' => $allData['industriallEngineerCount'],
                'civil_engineer' => $allData['civilEngineerCount'],
                'computer_engineer' => $allData['computerEngineerCount'],
                'medical_engineer' => $allData['medicalEngineerCount'],
                'other_cases_engineer' => $allData['otherCasesEngineerCount'],
                'dont_want_humanities' => $allData['dontWantHumanities'],
                'management' => $allData['managementCount'],
                'rights' => $allData['rightsCount'],
                'accounting' => $allData['accountingCount'],
                'literature' => $allData['literatureCount'],
                'english' => $allData['englishCount'],
                'history_and_geography' => $allData['historyAndGeographyCount'],
                'other_cases_humanities' => $allData['otherCasesHumanitiesCount'],
                'dont_want_medical' => $allData['dontWantMedical'],
                'medical' => $allData['medicalCount'],
                'nursing' => $allData['nursingCount'],
                'midwifery' => $allData['midwiferyCount'],
                'surgeryRoom' => $allData['surgeryRoomCount'],
                'hygiene' => $allData['hygieneCount'],
                'psychology' => $allData['psychologyCount'],
                'other_cases_medical' => $allData['otherCasesMedicalCount'],
                'dont_want_science' => $allData['dontWantScience'],
                'math' => $allData['mathCount'],
                'physics' => $allData['physicsCount'],
                'chemistry' => $allData['chemistryCount'],
                'biology' => $allData['biologyCount'],
                'other_cases_science' => $allData['otherCasesScienceCount'],
                'dont_want_marketing' => $allData['dontWantMarketing'],
                'general' => $allData['generalCount'],
                'expertise' => $allData['expertiseCount'],
                'student' => $allData['student'],
                'graduate' => $allData['graduate'],
                'advice_from_professors' => $allData['adviceFromProfessors'],
                'other_cases_cooperation' => $allData['otherCasesCooperation'],
            ]);
        }



            // return view


        // $response= Http::post('http://api.iaun.ac.ir/api/login',[
        //     'username' => 'apifanyaruser',
        //     'password' => 'AapiUser@FanYar',
        //     'usertype' => 3,
        // ]);
        // $token = $response['data']['token'];
        // $response= Http::post('http://api.iaun.ac.ir/api/payments/startpayment',[
        //     'uid' => Auth::id(),
        //     'amount' => Auth::user(),
        //     'usertype' => 3,
        // ]);

        // dd($response['data']['token']);
        // \CompanyLogActivity::addToLog('یک درخواست ثبت نام در نمایشگاه ثبت کرد');

        return redirect(route('company.sadra.sent-requests'))->with('success', __('public.sadra-pre-register'));
    }

    public function sadra_register_requests()
    {
        \CompanyLogActivity::addToLog('به صفحه درخواست های ثبت نام رویداد/نمایشگاه آمد');
        $requests = company_sadra::where('company_id', Auth::user()->groupable_id)->with('booth', 'sadra','payedPayment')->orderByDesc('updated_at')->paginate(20);
        return view('panel.company.sadra-reg-requests', compact('requests'));
    }

    public function sadraInvitation($id)
    {
        $payment = Payment::where('id' , $id)->with('order.sadra')->first();
        $company = company_sadra::where('company_id', Auth::user()->groupable_id)->with('companyInfo.logo','companyInfo.user','sadra')->first();
        return view('panel.company.sadra-invitation', compact('company' , 'payment'));
    }

    public function std_letter()
    {
        return view('introduction-letter-pattern');
    }

    public function get_std_letter($id)
    {
        $pdf = new PdfWrapper();
        $file_name = $id . time() . 'introduction-letter';
        $file_path = 'files/' . $file_name . '.pdf';
        $company = Auth::user()->groupable->name;
        $student = User::findorfail($id);
        $data = [
            'company' => $company,
            'student' => $student
        ];
        $pdf->loadView('introduction-letter-pattern', ['data' => $data], [], [
            'mode' => 'utf-8'
        ])->save($file_path);

        $pdf = PDF::loadView('introduction-letter-pattern', ['data' => $data]);
        \CompanyLogActivity::addToLog('معرفی نامه کارآموزی را دانلود کرد');
        return $pdf->download($file_name);
    }

    public function tickets()
    {
        \CompanyLogActivity::addToLog('به صفحه لیست تیکت ها آمد');
        $tickets = CompanyTicket::notDeleted()->where('receiver_id', Auth::id())->paginate(20);
        return view('panel.company.tickets-list', compact('tickets'));
    }

    public function MarkTicketAsRead(CompanyTicket $ticket)
    {
        \CompanyLogActivity::addToLog('یک تیکت را به عنوان خوانده شده علامت گذاری کرد');
        return $ticket->MarkAsRead();
    }

    public function send_reply(Request $request)
    {
        $request->validate([
            'content' => 'required'
        ]);

        $ticket = CompanyTicket::findorfail($request->parent_id);
        $ticket->update(['read_at' => now()]);
        $ticket->replies()->create([
            'parent_id' => $request->parent_id,
            'sender_id' => Auth::id(),
            'receiver_id' => $ticket->sender_id,
            'subject' => 'reply',
            'content' => $request->input('content')
        ]);
        \CompanyLogActivity::addToLog('یک پاسخ برای تیکت ثبت کرد');


        return redirect(route('company.tickets'))->with('success', __('public.reply-sent'));
    }

    public function show_signature_upload_form()
    {
        \CompanyLogActivity::addToLog('به صفحه آپلود امضا آمد');
        return view('panel.company.upload-signature');
    }

    public function upload_signature(Request $request)
    {
        $request->validate([
            'file' => 'required|image'
        ]);

        if ($request->file('file')) {
            if (Auth::user()->groupable->signature) {
                unlink(Auth::user()->groupable->signature->path);
                Auth::user()->groupable->signature->delete();
            }
            $name = time() . $request->file->getClientOriginalName();
            $path = $request->file->storeAs('files/signatures', $name, 'public');
            $signature = new Photo();
            $signature->name = $name;
            $signature->path = $path;
            $signature->groupable_type = CompanyInfo::class;
            $signature->groupable_id = Auth::user()->groupable_id;
            $signature->save();
            return redirect(route('company.panel'))->with('success', __('public.signature_added'));
        }
        \CompanyLogActivity::addToLog('امضای شرکت راآپلود کرد');

    }

    public function sent_intern_requests()
    {
        $requests = CooperationRequest::where([['type', 'intern'], ['sender_id', Auth::id()]])->get();
        return view('panel.company.sent-intern-requests', compact('requests'));
    }

    public function sent_hire_requests()
    {
        \CompanyLogActivity::addToLog(' به صفحه لیست درخواست های استخدامی فرستاده شده آمد');

        $requests = CooperationRequest::where([['type', '!=', 'intern'], ['sender_id', Auth::id()]])->get();
        return view('panel.company.sent-hire-requests', compact('requests'));
    }

    public function edit_profile()
    {
        \CompanyLogActivity::addToLog('به صفحه ویرایش پروفایل آمد');
        $user = Auth::user();
        return view('panel.company.edit-profile',compact('user'));
    }

    public function update_profile(Request $request)
    {
        $request->validate([
            'mobile' => 'unique:users,mobile,'. Auth::user()->id,
        ]);

        if (filled($request->password)){
            Auth::user()->password = Hash::make($request->password);
        }
        if ($request->mobile != Auth::user()->mobile){
            Auth::user()->update(['mobile_verified_at'=>null]);
        }
        Auth::user()->name = $request->name;
        Auth::user()->family = $request->family;
        Auth::user()->mobile = $request->mobile;
        Auth::user()->save();
        \CompanyLogActivity::addToLog('پروفایل خود را ویرایش کرد');


        return redirect(route('company.panel'))->with('success',__('public.data_added'));
    }

    public function sadra_final_register(Request $request)
    {
        $request->validate([
            'receipt' =>'required|image'
        ]);
        $sadra_req = company_sadra::findorfail($request->sadra_req);
        if ($request->has('receipt')){
            $name =uniqid().$request->file('receipt')->getClientOriginalName();
            $path = $request->receipt->storeAs('files/sadra/receipts', $name, 'public');
            $receipt = new Photo();
            $receipt->name = $name;
            $receipt->path = $path;
            $receipt->groupable_type = company_sadra::class;
            $receipt->groupable_id = $sadra_req->id;
            $receipt->save();
            $sadra_req->receipt_id = $receipt->id;
            $sadra_req->save();
        }
        \CompanyLogActivity::addToLog('درخواست ثبت نام در نمایشگاه را ثبت نهایی کرد');

        return redirect(route('company.sadra.sent-requests'))->with('success','ثبت نام شما با موفقیت انجام شد.');

    }

}
