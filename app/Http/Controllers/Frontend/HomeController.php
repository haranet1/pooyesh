<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\Job;
use App\Models\Job_Category;
use App\Models\Ocategory;
use App\Models\OjobPosition;
use App\Models\PersonalityJob;
use App\Models\Province;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $lastJobPositions = OjobPosition::Confirmed()
            ->NotDeleted()
            ->Active()
            ->with('job', 'company.groupable' , 'job.category','province','city')
            ->orderByDesc('updated_at')
            ->take(6)
            ->get();

        $counts = [
            'companies' =>User::Company()->Confirmed()->count(),
            'jobs' => OjobPosition::NotDeleted()->Confirmed()->Active()->count(),
            'students' => User::Confirmed()->StudentAndApplicant()->count(),
            'mbtis' => User::whereHas('mbti_test')->count(),
            'enneagram' => User::whereHas('enneagram_test')->count(),
        ];

        $companies = CompanyInfo::with('logo', 'user')
            ->whereNotNull('logo_id')
            ->whereRelation('user','status' ,1)
            ->orderByDesc('updated_at')
            ->take(9)
            ->get();

        // $categories = Ocategory::with('jobs.positions','photo')
        //     ->whereHas('jobs.positions')
        //     ->take(4)->get();

        $categories = Ocategory::select('ocategories.id', 'ocategories.title') 
            ->join('ojobs', 'ojobs.category_id', '=', 'ocategories.id') 
            ->join('ojob_positions', 'ojob_positions.job_id', '=', 'ojobs.id') 
            ->groupBy('ocategories.id', 'ocategories.title') 
            ->orderByRaw('COUNT(ojob_positions.id) DESC') 
            ->take(4) 
            ->get();

        return view('frontend.home.index' , compact(
            'lastJobPositions',
            'counts',
            'companies',
            'categories'
        ));
    }

    public function companies()
    {
        $companies = User::company()
            ->confirmed()
            ->with('groupable.logo' , 'ojobPositions')
            ->orderByDesc('updated_at')
            ->paginate(15);

        foreach($companies as $company){
            if(count($company->ojobPositions) > 0){
                foreach ($company->ojobPositions as $job) {
                    if($job->deleted == false && $job->status == 1){
                        if($job->type == 'intern'){
                            $company->intern_job++;
                        }else {
                            $company->hire_job++;
                        }
                    }
                }
            }
        }

        return view('frontend.list.companies' , compact(
            'companies'
        ));
    }

    public function jobs()
    {
        $categories = Ocategory::all();

        $provinces = Province::all();

        $jobs = OjobPosition::Confirmed()
            ->NotDeleted()
            ->Active()
            ->AcceptedOwner()
            ->with('company.groupable')
            ->orderByDesc('updated_at')
            ->paginate(10);
        
        $id = request()->route()->parameter('id');

        $oldData = [];

        return view('frontend.list.jobs' , compact(
            'categories',
            'provinces',
            'id',
            'jobs',
            'oldData'
        ));
    }

    public function job($id)
    {
        $job = OjobPosition::with('company.groupable' , 'oldSkills')->find($id);

        $hasCooperationRequest = false;
        $cooperationRequests = CooperationRequest::where('job_position_id' , $job->id)->where('sender_id' , Auth::id())->get();
        if($cooperationRequests->count() > 0){
            $hasCooperationRequest = true;
        }else{
            $hasCooperationRequest = false;
        }

        // dd($job);

        return view('frontend.job' , compact(
            'job',
            'hasCooperationRequest'
        ));
    }

    public function contactUs()
    {
        return view('frontend.contact-us');
    }

    public function aboutUs()
    {
        return view('frontend.about-us');
    }

    public function searchCompany(Request $request)
    {
        $name = $request->name;
        $companies = User::Company()
            ->Confirmed()
            ->whereRelation('groupable','name','like','%'.$name.'%')
            ->with('job_positions','groupable')
            ->orderByDesc('updated_at')
            ->paginate(16)->appends([
                'name' => $name
            ]);

        foreach($companies as $company){
            if(count($company->job_positions) > 0){
                foreach ($company->job_positions as $job) {
                    if($job->deleted == false && $job->status == true){
                        if($job->type == 'intern'){
                            $company->intern_job++;
                        }else {
                            $company->hire_job++;
                        }
                    }
                }
            }
        }
        
        return view('frontend.list.companies' , compact(
            'name',
            'companies'
        ));
    }

    public function searchJobs(Request $request)
    {

        // dd($request->all());
        $categories = Ocategory::all();

        $provinces = Province::all();

        $typeForReturn = 's';

        if($request->type_intern == "on"){
            $type_intern = 'on';
            $typeForReturn = 'type_intern';
        }else {
            $type_intern = '';
        }

        if($request->type_hire == "on"){
            $type_hire = 'on';
            $typeForReturn = 'type_hire';
        }else {
            $type_hire = '';
        }


        $oldData = [
            'category' => $request->category,
            'jobTitle' => $request->jobTitle,
            'province' => $request->province,
            'type_intern' => $type_intern,
            'type_hire' => $type_hire,

            'grade' => $request->grade,
            'workExperience' => $request->workExperience,
            'salary' => $request->salary,
            'age' => $request->age,
            'city' => $request->city,
            's' => ''
        ];

        $query = OjobPosition::query();
        $query = $query->Confirmed();
        $query = $query->NotDeleted();
        $query = $query->AcceptedOwner();
        $query = $query->Active();
        $query = $query->with('job','job.category','company.groupable','company.groupable.logo','province','city');

        if($request->category != null || $request->category != ''){
            $query->whereRelation('job.category', 'id', $request->category);
        }

        if($request->jobTitle != null || $request->jobTitle != ''){
            $query->where('title', 'LIKE' , '%'. $request->jobTitle . '%');
        }

        if($request->province != null || $request->province != ''){
            $query->where('province_id' , $request->province);
        }

        if($type_intern == "on"){
            $query->where('type' , 'intern');
        }

        if($type_hire == "on"){
            $query->where('type', '!=', 'intern');
        }

        if($request->grade != null || $request->grade != ''){
            $query->where('grade' , $request->grade);
        }

        if($request->workExperience != null || $request->workExperience != ''){
            if($request->workExperience == 'تا ۲ سال'){
                $query->whereBetween('experience', [0 , 2]);
            }
            elseif($request->workExperience == '۳ تا ۵ سال'){
                $query->whereBetween('experience', [3 , 5]);
            }
            elseif($request->workExperience == '۶ تا ۱۰ سال'){
                $query->whereBetween('experience', [6 , 10]);
            }
            elseif($request->workExperience == 'بالای ۱۰ سال'){
                $query->where('experience', '>' , 10);
            }
            else{
                $query->where('experience',$request->workExperience)->orWhere('experience', '0');
            }
        }

        if($request->salary != null || $request->salary != ''){
            if($request->salary == 'زیر ۴ میلیون'){
                $query->whereBetween('salary', [0 , 4000000]);
            }
            elseif($request->salary == 'بین ۴ تا ۸ میلیون'){
                $query->whereBetween('salary', [4000001 , 8000000]);
            }
            elseif($request->salary == 'بین ۸ تا ۱۶ میلیون'){
                $query->whereBetween('salary', [8000001 , 16000000]);
            }
            elseif($request->salary == 'بین ۱۶ تا ۲۵ میلیون'){
                $query->whereBetween('salary', [16000001 , 25000000]);
            }elseif($request->salary == 'بالای ۲۵ میلیون'){
                $query->where('salary', '>' , 25000000);
            }
            else{
                $query->where('salary',$request->salary)->orWhere('salary', '0');
            }
        }

        if($request->age != null || $request->age != ''){
            if($request->age == 'زیر ۲۰ سال'){
                $query->whereBetween('age' , [0 , 20]);
            }elseif($request->age == '۲۱ تا ۳۰ سال'){
                $query->whereBetween('age' , [21 , 30]);
            }elseif($request->age == '۳۱ تا ۴۰ سال'){
                $query->whereBetween('age' , [31 , 40]);
            }elseif($request->age == 'بالای ۴۱ سال'){
                $query->where('age', '>' , 41);
            }else{
                $query->where('age' , $request->age)->orWhere('age' , '0');
            }
        }

        // militaryOrder

        if($request->city != null || $request->city != ''){
            $query->whereRelation('city' , 'name' , $request->city);
        }

        $jobs = $query->with('company.groupable')
            ->orderByDesc('updated_at')
            ->paginate(10)
            ->appends([
                'category' => $oldData['category'],
                'jobTitle' => $oldData['jobTitle'],
                'province' => $oldData['province'],
                $typeForReturn => $oldData[$typeForReturn],
                'grade' => $oldData['grade'],
                'workExperience' => $oldData['workExperience'],
                'salary' => $oldData['salary'],
                'age' => $oldData['age'],
                'city' => $oldData['city'],
            ]);
        
        $singleJob = $jobs->first();

        $id = request()->route()->parameter('id');

        // dd($jobs);

        return view('frontend.list.jobs' , compact(
            'oldData',
            'categories',
            'provinces',
            'singleJob',
            'id',
            'jobs'
        ));
    }

    public function searchJob(Request $request)
    {
        $oldData = [
            'jobTitle' => $request->input('jobTitle'),
        ];

        $categories = Ocategory::all();

        $provinces = Province::all();

        $query = OjobPosition::query();
        $query = $query->Confirmed();
        $query = $query->NotDeleted();
        
        if($request->input('jobTitle') != '' || $request->input('jobTitle') != null){
            $query = $query->where('title' ,'LIKE' , '%'.  $request->input('jobTitle') . '%' );
        }
        
        $jobs = $query->with('company.groupable')
            ->orderByDesc('updated_at')
            ->paginate(6)
            ->appends([
                'oldData'
            ]);

        $singleJob = $jobs->first();

        $id = request()->route()->parameter('id');

        return view('frontend.list.jobs' , compact(
            'oldData',
            'categories',
            'provinces',
            'jobs',
            'singleJob',
            'id',
        ));
    }
}
