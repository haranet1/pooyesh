<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\EmployeeInfo;
use App\Models\OjobPosition;
use App\Models\OJpAbility;
use App\Models\OJpFavorite;
use App\Models\OJpKnowledge;
use App\Models\OJpSkill;
use App\Models\OJpTask;
use App\Models\OJpTechnologySkill;
use App\Models\OJpWorkStyle;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;

class SinglePageController extends Controller
{
    public function candidate($id): View
    {
        $user = User::StudentAndApplicant()
        ->with([
            'groupable',
            'skills.skillName',
            'academicInfos' => function ($query) {
                $query->orderBy('end_date', 'asc'); // بر اساس تاریخ شروع به صورت صعودی مرتب کنید
            },
            'expertises',
            'languages',
            'work_experiences' => function ($query) {
                $query->orderBy('start_date', 'asc'); //
            },
            'social_networks',
            'certificates'
        ])->findOrFail($id);    

        $user_requests = null;
        $companyCanSendRequest = false;
        $job_positions = null;

        if (Auth::check() && Auth::user()->groupable_type == CompanyInfo::class){
            $user_requests = CooperationRequest::with('job', 'process')->where([['sender_id',Auth::id()],['receiver_id',$id]])->get();

            if($user_requests->count() > 0){
                
                foreach($user_requests as $user_request){

                    if(is_null($user_request->status)){
    
                        $companyCanSendRequest = false;
    
                    }elseif($user_request->status == 1){
                        
                        if($user_request->job->type == 'intern'){
                            $date = new Carbon($user_request->process->accepted_by_std_at);
    
                            if($date->diffInMonths(Carbon::now()) >= 3){
                                $companyCanSendRequest = true;
                            }
                        }
    
                    }else{
                        $companyCanSendRequest = true;
                    }
                }

            }else {
                $companyCanSendRequest = true;
            }

            $job_positions = OjobPosition::where('company_id' , Auth::user()->id)->get();
        }
        $show_info = show_student_info($id);
        $show_resume = show_student_resume($id);

        $isConfirmed = true;
        if($user->status == 0){
            $isConfirmed = false;
        }

        return view('frontend.single.candidate' , compact(['user','companyCanSendRequest','show_info', 'show_resume','isConfirmed' ,'job_positions']));
    }

    public function company($id)
    {
        $company = User::company()->with('ojobPositions')->findorfail($id);

        $intern_positions = new Collection();
        $hire_positions = new Collection();

        $jobIsConfirmed = false;

        foreach ($company->ojobPositions as $job){
            if($job->deleted == false){
                if($job->status == 1){
                    $jobIsConfirmed = true;
                    if ($job->type == 'intern')

                    $intern_positions->push($job);
                else
                    $hire_positions->push($job);
                }
            }
        }

        $thisCompanyisMe = false;
        if(Auth::check() && (Auth::id() == $company->id || Auth::user()->IsCompany())){
            $thisCompanyisMe = true;
        }

        $user_request = null;
        if (Auth::check())
            $user_request = CooperationRequest::where([['sender_id',Auth::id()],['receiver_id',$id]])->first();

        if($company->groupable->header){
            $company->header = $company->groupable->header->path;
        }else{
            $company->header = 'assets/images/default-header.svg';
        }
        if($company->groupable->logo){
            $company->logo = $company->groupable->logo->path;
        }else{
            $company->logo = 'assets/images/default-logo.svg';
        }

        return view('frontend.single.company',compact(
            'company',
            'user_request',
            'jobIsConfirmed',
            'thisCompanyisMe',
            'intern_positions',
            'hire_positions'
        ));
    }

    public function job($id)
    {
        $job = OjobPosition::with('skills','languages')->findorfail($id);
        $hasCooperationRequest = false;
        $cooperationRequests = CooperationRequest::where('job_position_id' , $job->id)->where('sender_id' , Auth::id())->get();
        if($cooperationRequests->count() > 0){
            $hasCooperationRequest = true;
        }else{
            $hasCooperationRequest = false;
        }
        $hasPermission = false;
        if(Auth::check()){
            if(Auth::user()->hasRole('employee')){
                
                $hasPermission = true;
            }
            if($job->IsConfirmed()){
                $hasPermission = true;
            }
        }
        $my_job = false;
        if(Auth::id() == $job->company_id){
            $my_job = true;
        }
        $similar_jobs = OjobPosition::confirmed()->with('job.category')->where('id','<>',$id)
            ->whereHas('job.category',function ($query) use ($job){
                $query->where('id',$job->job->category_id);
            })
            ->get();

        return view('frontend.single.job',compact(['job','similar_jobs','hasPermission' , 'hasCooperationRequest' , 'my_job']));
    }

    public function ojob($id)
    {
        $job = OjobPosition::with('job.category' , 'company' , 'province' , 'city' , 'major' , 'oldSkills')->find($id);

        if($job->status == 1 || (Auth::check() && (Auth::user()->groupable_type == EmployeeInfo::class || $job->company_id == Auth::id()))){
            $skills = OJpSkill::with('skill')->where('job_position_id' , $id)
                ->orderByDesc('importance')
                ->get();

            $knowledges = OJpKnowledge::with('knowledge')
                ->where('job_position_id', $id)
                ->orderByDesc('importance')
                ->get();

            $abilities = OJpAbility::with('ability')
                ->where('job_position_id', $id)
                ->orderByDesc('importance')
                ->get();

            $workStyles = OJpWorkStyle::with('workStyle')
                ->where('job_position_id', $id)
                ->orderByDesc('importance')
                ->get();

            $technologySkills = OJpTechnologySkill::with('technologySkill','jobTechnologySkill.example')
                ->where('job_position_id', $id)
                ->get();
                
            $tasks = OJpTask::with('task')
                ->where('job_position_id', $id)
                ->get();

            $hasCooperationRequest = false;
            $cooperationRequests = CooperationRequest::where('job_position_id' , $job->id)->where('sender_id' , Auth::id())->get();

            if($cooperationRequests->count() > 0){
                $hasCooperationRequest = true;
            }else{
                $hasCooperationRequest = false;
            }
            
            $favoriteJob = OJpFavorite::where('user_id' , Auth::id())
                ->where('job_position_id' , $job->id)
                ->first();

            // dd($workStyles);
            

            return view('frontend.ojob' , compact(
                'job',
                'skills',
                'knowledges',
                'abilities',
                'workStyles',
                'technologySkills',
                'tasks',
                'hasCooperationRequest',
                'favoriteJob',
            ));
        }

        return redirect(404);
    }
}
