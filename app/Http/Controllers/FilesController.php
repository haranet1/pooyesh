<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = File::whereNull('groupable_id')->paginate(20);
        return view('panel.employee.files-list',compact('files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.employee.create-file');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'name' =>'required',
           'file' =>'required',
        ]);

        if ($request->file('file')){
            $name = time() . $request->file->getClientOriginalName();
            $path = $request->file->storeAs(
                'files',
                $name,
                'public'
            );
            $file = new File();
            $file->name = $request->name;
            $file->path = $path;

            if ($request->filled('std_can_dl'))
                $file->std_can_dl = 1;

            if ($request->filled('co_can_dl'))
                $file->co_can_dl = 1;

            $file->save();

        }
       \EmployeeLogActivity::addToLog('یک فایل (فرم-آیین نامه) ثبت کرد');

        return redirect()->route('files.index')->with(__('public.data_added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json($id);
    }
}
