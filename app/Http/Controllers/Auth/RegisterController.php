<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Services\Notification\Notification;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::PANEL;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:25'],
            'family' => ['required', 'string', 'max:40'],
            'mobile' => ['required', 'numeric', 'digits:11', 'unique:users'],
            'email' => ['required' , 'email' , 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'role' => ['required'],
            'sex' => ['required'],
        ],[
            'role.required'=>'نوع کاربری الزامی است',
            'name.required'=>'نام الزامی است',
            'name.string'=>'نام را به صورت صحیح وارد کنید',
            'name.max'=>'طول نام بیش از حد مجاز است',
            'family.required'=>'نام خانوادگی الزامی است',
            'family.string'=>'نام خانوادگی را به صورت صحیح وارد کنید',
            'family.max'=>'طول نام خانوادگی بیش از حد مجاز است',
            'email.required' => 'ایمیل الزامی است',
            'email.email' => 'ایمیل را به صورت صحیح وارد کنید',
            'email.unique' => 'ایمیل تکراری است',
            'mobile.required'=>'موبایل الزامی است',
            'mobile.digits'=>'شماره موبایل باید 11 رقم باشد',
            'mobile.unique'=>'موبایل تکراری است',
            'mobile.numeric'=>'صفحه کلید خود را به زبان انگلیسی تغییر دهید و شماره را وارد کنید.',
            'password.required'=>'کلمه عبور الزامی است',
            'password.min'=>'کلمه عبور حداقل باید 8 کاراکتر باشد',
            'sex.required'=>'لطفا جنسیت خود را انتخاب کنید',
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'family' => $data['family'],
            'mobile' => $data['mobile'],
            'email' => $data['email'],
            'sex' => $data['sex'],
            'password' => Hash::make($data['password']),
        ]);
        
        $user->assignRole($data['role']);

        event(new UserRegistered($user));

        return $user;
    }

    protected function registered(\Illuminate\Http\Request $request, $user)
    {
        $path = "";
        if ($user->hasRole('student') || $user->hasRole('applicant'))
            $path = 'panel/std';
        elseif ($user->hasRole('employee'))
            $path = 'panel/emp';
        elseif ($user->hasRole('company'))
            $path = 'panel/company';

        return redirect($path);
    }

    public function customRegister(\Illuminate\Http\Request $request)
    {
        $userExist = User::where('mobile' , $request->mobile)->first();

        if(! is_null($userExist)){

            return response()->json('mobile_is_already_exist');

        }

        $isStudent = $request->isStudent;
        $user = null;
        $n_code = null;
        if($request->n_code){
            $n_code = $request->n_code;
        }

        if($request->time_out){
            $user = User::where('mobile', $request->mobile)->whereNull('mobile_verified_at')->first();
            if($user){
                return response()->json(['result' => 'verify_mobile','userId' => $user->id]);
            }
        }else{
            $request->validate([
                'mobile' => 'unique:users',
            ],[
                'mobile.unique' => 'شماره موبایل وارد شده تکراری است'
            ]);
        }

        if($request->userId == 0){
            $user = User::create([
                'name' => $request->name,
                'family' => $request->family,
                'n_code' => $n_code,
                'mobile' => $request->mobile,
                'password' => Hash::make($request->password),
            ]);
        }else{
            $user = User::find($request->userId);
            $user->Update([
                'name' => $request->name,
                'family' => $request->family,
                'n_code' => $n_code,
                'mobile' => $request->mobile,
                'password' => Hash::make($request->password),
            ]);
        }

        if ($isStudent) {
            $user->syncRoles('student');
        } else {
            $user->syncRoles('applicant');
        }

        if ($user->mobile_code_created_at == null || $user->mobile_code_created_at < now()->subMinute(2)){
            if (has_internet()){
                $rand = rand(10000,99999);
                $user->mobile_code = $rand;
                $user->mobile_code_created_at = now();
                $user->save();
                $notif = new Notification();
                $notif->sendSms($user,'fanyarcoMobileVerify',[$user->name,$user->family,$rand]);
                return response()->json(['result' => 'verify_mobile','userId' => $user->id]);
            }else{
                return response()->json('اتصال اینترنت خود را بررسی کنید');
            }

        }else{
            return response()->json(['result' => 'verify_mobile','userId' => $user->id]);
        }



    }

    public function customRegisterLogin(\Illuminate\Http\Request $request)
    {
        $user = User::find($request->userId);
        if ($request->verifyCode == $user->mobile_code) {
            $user->update([
                'mobile_verified_at' => Carbon::now(),
            ]);
            $this->guard()->login($user);
            //todo : send sms to user ;-)
            return true;
        } else {
            return response()->json(['error' => 'کد تایید وارد شده نادرست است']);
        }

    }
}
