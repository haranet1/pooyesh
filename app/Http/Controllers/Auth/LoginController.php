<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\CompanyLogActivity;
use App\Helpers\EmployeeLogActivity;
use App\Helpers\StudentLogActivity;
use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use App\Models\EmployeeInfo;
use App\Models\StudentInfo;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::PANEL;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        return redirect()->intended(RouteServiceProvider::PANEL);
    }

    public function username()
    {
        return 'mobile';
    }

    public function logout(Request $request)
    {

        if (Auth::user()->groupable instanceof StudentInfo){
            StudentLogActivity::addToLog('از سایت خارج شد');
        }
        elseif (Auth::user()->groupable instanceof EmployeeInfo){
            EmployeeLogActivity::addToLog('از سایت خارج شد');
        }
        elseif(Auth::user()->groupable instanceof CompanyInfo){
            CompanyLogActivity::addToLog('از سایت خارج شد');
        }

        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }
}
