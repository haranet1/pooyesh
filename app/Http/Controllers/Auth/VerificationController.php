<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\VerificationEmail;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\Notification\Notification;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'send');
    }

    public function send(Request $request)
    {
        $user = User::find(Auth::id());

        if($user->email and !$user->hasVerifiedEmail()){

            $secondUser = User::where('email' , $request->email)->where('id' ,'!=' ,$user->id)->first();

            if((isset($secondUser) && $secondUser->hasVerifiedEmail())){
                return response()->json('emailHasAlreadyTaken');
            }
        }

        if($user->hasVerifiedEmail()){
            return response()->json('emailHasAlreadyTaken');
        }
        
        $time = $user->updated_at->addMinutes(5);

        if($time < now()){
            if(! is_null($request->email)){
                $user->update(['email' => $request->email]);
            }

            $user->sendEmailVerificationNotification();
            

            return response()->json('emailVerificationSended');
            
        }else{

            return response()->json('emailVerificationSended');
        }
    }

    public function verify(Request $request)
    {
        if($request->user()->hasVerifiedEmail()){
            return redirect()->route('dashboard');
        }

        $request->user()->markEmailAsVerified();
        

        session()->put('emailHasVerified' , true);

        return redirect()->route('dashboard');
    }
    
}
