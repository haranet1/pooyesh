<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ajax(Request $request)
    {
        switch ($request->do){
            case'mark-as-read':
                $notification = Auth::user()->notifications()->findorfail($request->notification);
                $this->MarkAsRead($notification);
                return response()->json(['success']);
        }
    }

    public function MarkAsRead( $notification)
    {
        return  $notification->update(['read_at'=>now()]);
    }
}
