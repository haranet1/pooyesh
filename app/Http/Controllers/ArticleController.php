<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\Article;
use App\Models\Comment;
use App\Models\ArticleLike;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\TemporaryFile;
use App\Models\ArticleCategory;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ArticleController extends Controller
{

    // public routes 
    public function index()
    {
        $categories = ArticleCategory::all();
        $top_categories = ArticleCategory::where('rank',1)->get();
        $articles = Article::orderbyDesc('created_at')->paginate(20);
        return view('frontend.list.articles', compact('articles', 'categories'));
    }

    public function searchArticle(Request $request)
    {
        $queryArticle = $request->input('queryArticle');

        if($queryArticle != '' && $queryArticle != null){
            $query = Article::where('title','LIKE', '%'.  $queryArticle . '%')
                ->orWhere('content','LIKE', '%'.  $queryArticle . '%');
        } else {
            return redirect(route('articles.blog'));
        }
        $articles = $query->orderByDesc('updated_at')
            ->paginate(10);
        $categories = ArticleCategory::all();

        return view('frontend.list.articles', compact('articles','categories','queryArticle'));
    }


    public function filterCategory($categorySlug)
    {
        $categories = ArticleCategory::all();
        if($categorySlug == ''){
            return redirect(route('articles.blog'));
        }
        $category = ArticleCategory::where('slug',$categorySlug)->firstOrFail();
        $articles = Article::where('category_id', $category->id)->paginate(20);
        $categoryId = $category->id;
        return view('frontend.list.articles', compact('articles','categories','categoryId'));
    }

    
    public function show($articleSlug)
    {
        $article = Article::where('slug' ,$articleSlug)->with('photo','category','likes')->firstOrFail();
        $comments = Comment::where('commentable_id', $article->id)->where('status', 1)->where('parent_id', null)->get();
        $toc = $this->parseContent($article->content);
        $suggestion = Article::where('category_id', $article->category_id)->whereNotIn('id',[$article->id])->orderBy('created_at', 'desc')->take(3)->get();

        return view('frontend.single.article', [
            'article' => $article,
            'comments' => $comments,
            'html' => $toc['html'],
            'index' => $toc['index'],
            'suggestion' => $suggestion
        ]);
    }

    public function parseContent($html)
    {
        $pattern = '/<h[2-5](?:\s+[^>]*)?>.*?<\/h[2-5]>/i';
        preg_match_all($pattern, $html, $matches);

        $index = [];
        foreach ($matches[0] as $i => $match){

            $text = strip_tags($match);
            
            $slug = Str::slug($text);
            $patternHtml = '/<h([2-5])(?![^>]*\sid=[\'"])/i';

            $replacement = '<h$1 id="' . $slug . '"';
        
            $heading = preg_replace($patternHtml, $replacement, $match);
            $html = str_replace($match, $heading, $html);

            array_push($index , [
                'text' => $text,
                'slug' => $slug,
                'h' => substr($match, 2 , 1)
            ]);
        }

        return ["html" => $html, "index" => $index];
    }

    public function additionView(Request $request)
    {
        $article = Article::find($request->article_id);
        $article->increment('views');
        return response()->json(['status' => 'success'], 200);
    }

    public function likeOrUnlike(Request $request)
    {
        if(!Auth::check()){
            response()->json('error', 404);
        }
        $article = Article::findOrFail($request->article_id);
        $user = Auth::user();

        $like = ArticleLike::where('user_id', $user->id)->where('article_id', $article->id)->first();

        if ($like) {
            $like->delete();
            $liked = false;
        } else {
            ArticleLike::create([
                'user_id' => $user->id,
                'article_id' => $article->id,
            ]);
            $liked = true;
        }

        return response()->json([
            'liked' => $liked,
            'likes_count' => $article->likes()->count()
        ]);
    }



    // admin routes 
    public function index_admin()
    {
        $title = 'وبلاگ';
        $articles = Article::with('photo')->paginate(20);

        return view('admin.magazine.index', compact('articles','title'));
    }

    public function create_article()
    {
        $title = 'ساخت مقاله جدید';
        $categories = ArticleCategory::all();
        return view('admin.magazine.create', compact('title','categories'));
    }

    public function store(Request $request)
    {
        
        $request->validate([
            'editorData' => 'required',
            'title' => 'required',
            'category' => 'required',
            'image' => 'required',
        ],[
            'title.required' => 'عنوان مقاله الزامی است',
            'image.required' => 'پوستر مقاله الزامی است',
            'editorData.required' => 'متن مقاله را وارد کنید',
            'category.required' => 'دسته بندی مقاله را انتخاب کنید',
        ]);

        $editorData = $request->editorData;
        $titleInput = $request->input('title');

        $article = new Article();
        $article->content = $editorData;
        $article->title = $titleInput;

        $category = ArticleCategory::where('title',$request->category)->first();
        $article->category_id = $category->id;
        $article->save();

        $temporaryFile = TemporaryFile::where('folder', $request->image)->first();

        if($temporaryFile){
            $sourcePath = public_path('img/blog/tmp/'). $request->image . '/' . $temporaryFile->filename;
            $destinationDirectory = public_path('img/blog/main/');
            $destinationPath = $destinationDirectory . $temporaryFile->filename;
        }

        if (!is_dir($destinationDirectory)) {
            mkdir($destinationDirectory, 0755, true);
        }

        if(File::exists($sourcePath)){
            File::move($sourcePath, $destinationPath);
            $path = 'img/blog/main/'.$temporaryFile->filename;
            $imageName = $temporaryFile->filename;

            $photo = new Photo();
            $photo->name = $imageName;
            $photo->path = $path;
            $photo->groupable_id = $article->id;
            $photo->groupable_type = Article::class;
            $photo->save();

            $article->photo_id = $photo->id;
            $article->save();

            if (is_dir(public_path('img/blog/tmp/'. $request->image))) {
                rmdir(public_path('img/blog/tmp/'. $request->image));
            }

            $temporaryFile->delete();

            return redirect()->route('index.article.blog')->with('success','مقاله با موفقیت ساخته شد');
        }
        
        return redirect()->back()->with('error' , 'خطایی رخ داده است');
    }

    public function storeImage(Request $request)
    {
        if($request->hasFile('image')){
            $imageName = time() . $request->file('image')->getClientOriginalName();
            $image = $request->file('image');
            $folder = uniqid() . '_' . now()->timestamp;
            $path = $image->storeAs('img/blog/tmp/'.$folder, $imageName, 'public');

            TemporaryFile::create([
                'folder' => $folder,
                'filename' => $imageName
            ]);

            return $folder;
        }

        return '';
    }

    public function revert(Request $request){
        $temporaryFile = TemporaryFile::where('folder', $request->getContent())->first();

        if($temporaryFile){
            File::deleteDirectory(public_path('img/blog/tmp/'. $temporaryFile->folder));
            $temporaryFile->delete();
            return response('');
        }
    }

    // upload inside editor 
    public function upload(Request $request)
    {
        if ($request->hasFile('upload')) {
            $fileName = time() . $request->file('upload')->hashName();
            $file = $request->file('upload');
            $path = $file->storeAs('img/blog', $fileName, 'public'); // store in the public disk

            return response()->json([
                'url' => asset($path),
            ]);
        }

        return response()->json(['error' => 'No file uploaded'], 400);
    }




    public function edit($articleId)
    {
        $title = 'ویرایش مقاله';
        $article = Article::with('photo')->find($articleId);
        $categories = ArticleCategory::all();

        return view('admin.magazine.edit',compact('article','title','categories'));
    }


    public function update(Request $request)
    {
        $request->validate([
            'editorData' => 'required',
            'title' => 'required',
            'category' => 'required',
            'image' => 'required',
        ],[
            'editorData.required' => 'عنوان مقاله الزامی است',
            'image.required' => 'پوستر مقاله الزامی است',
            'titleInput.required' => 'متن مقاله را وارد کنید',
            'category.required' => 'دسته بندی مقاله را انتخاب کنید',
        ]);

        $editorData = $request->editorData;
        $titleInput = $request->input('title');
        $article = Article::find($request->article_id);
        $article->content = $editorData;
        $article->title = $titleInput;
        $category = ArticleCategory::where('title',$request->category)->first();
        $article->category_id = $category->id;
        $article->save();
        
        if($article->photo->name != $request->image){
            File::delete(public_path($article->photo->path));
            $temporaryFile = TemporaryFile::where('folder', $request->image)->first();
        
            if($temporaryFile){
                $sourcePath = public_path('img/blog/tmp/'). $request->image . '/' . $temporaryFile->filename;
                $destinationDirectory = public_path('img/blog/main/');
                $destinationPath = $destinationDirectory . $temporaryFile->filename;
                    
                if (!is_dir($destinationDirectory)) {
                    mkdir($destinationDirectory, 0755, true);
                }
    
                if(File::exists($sourcePath)){
                    File::move($sourcePath, $destinationPath);
                    $path = 'img/blog/main/'.$temporaryFile->filename;
                    $imageName = $temporaryFile->filename;
        
                    $photo = $article->photo;
                    $photo->name = $imageName;
                    $photo->path = $path;
                    $photo->groupable_id = $article->id;
                    $photo->groupable_type = Article::class;
                    $photo->save();
        
                    $article->photo_id = $photo->id;
                    $article->save();
        
                    if (is_dir(public_path('img/blog/tmp/'. $request->image))) {
                        rmdir(public_path('img/blog/tmp/'. $request->image));
                    }
        
                    $temporaryFile->delete();
                }
            } else {
                return redirect()->route('index.article.blog')->with('error','خطایی رخ داد');
            }

        }
       
        return redirect()->route('index.article.blog')->with('success','مقاله با موفقیت بروزرسانی شد');
    }

    public function load($fileId){
        $photo = Photo::where('name',$fileId)->first();

        $filePath = public_path($photo->path);

        return response()->file($filePath, [
            'Content-Disposition' => 'inline; filename="' . $fileId . '"'
        ]);
    }

    public function delete($articleId)
    {
        $article = Article::find($articleId);
        $article->delete();
        return redirect()->back()->with('success', 'مقاله با موفقیت حذف شد');
    }

    // categories 

    public function category_index()
    {
        $title = 'دسته بندی مقالات';
        $categories = ArticleCategory::with('articles')->get();
        return view('admin.magazine.categories.index', compact('categories','title'));
    }

    public function category_store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'rank' => 'required',
        ],[
            'title.required' => 'عنوان الزامی است',
            'rank.required' => 'انتخاب نوع دسته بندی الزامی است',
        ]);

        $category = new ArticleCategory();
        $category->title = $request->title;
        if($request->rank == 'ordinary'){
            $category->rank = 0;
        }
        elseif($request->rank == 'special'){
            $category->rank = 1;
        }

        $category->save();

        return redirect(route('all.category.mag'))->with('success', 'دسته بندی با موفقیت اضافه شد');
    }


    public function category_update(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'rank' => 'required',
        ],[
            'title.required' => 'عنوان الزامی است',
            'rank.required' => 'انتخاب نوع دسته بندی الزامی است',
        ]);

        $category = ArticleCategory::find($request->category_id);
        $category->title = $request->title;
        if($request->rank == 'ordinary'){
            $category->rank = 0;
        }
        elseif($request->rank == 'special'){
            $category->rank = 1;
        }

        $category->save();

        return redirect(route('all.category.mag'))->with('success', 'دسته بندی با موفقیت بروزرسانی شد');
    }

    public function categoryـdelete(Request $request)
    {
        $category = ArticleCategory::find($request->category);
        return response()->json($category->delete());
    }

    public function Raising_rank_category(Request $request)
    {
        $category = ArticleCategory::find($request->category);
        if($category->rank == 0){
            $category->rank = 1;
        }
        elseif($category->rank == 1){
            $category->rank = 0;
        }
        $category->save();
        return response()->json('success',200);
    }

    public function Raising_rank_article(Request $request)
    {
        $article = Article::find($request->article);
        if($article->rank == 0){
            $article->rank = 1;
        }
        elseif($article->rank == 1){
            $article->rank = 0;
        }
        $article->save();
        return response()->json('success',200);
    }
}

