<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventInfo;
use App\Models\EventPlan;
use App\Models\EventSans;
use App\Models\Photo;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;

class SansController extends Controller
{
    public function eventSans($id)
    {
        $event = Event::find($id);
        $eventSans = EventSans::NotDeleted()
            ->with('event')->where('event_id' , $id)
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('event.sans.index' , compact(
            'event',
            'eventSans'
        ));
    }

    public function create($id)
    {
        $event = Event::find($id);

        $plans = EventPlan::where('event_id' , $event->id)->get();

        return view('event.sans.create' , compact(
            'event',
            'plans'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'start_register_at' => 'required',
            'end_register_at' => 'required',
            'date' => 'required',
            'time' => 'required',
        ],[
            'start_register_at.required' => 'تاریخ شروع ثبت نام این سانس را وارد کنید',
            'end_register_at.required' => 'تاریخ پایان ثبت نام این سانس را وارد کنید',
            'date.required' => 'تاریخ برگزاری را وارد کنید',
            'time.required' => 'ساعت شروع سانس را وارد کنید',
        ]);

        $eventSans = new EventSans();
        $eventSans->event_id = $request->input('event_id');

        if($request->input('title')){
            $eventSans->title = $request->input('title');
        }
        if($request->input('description')){
            $eventSans->description = $request->input('description');
        }

        if(strpos($request->input('start_register_at'), '/') !== false) {

            $startRegDate = explode('/' , $request->input('start_register_at'));

            $month = (int)persian_to_eng_num($startRegDate[1]);
            $day = (int)persian_to_eng_num($startRegDate[2]);
            $year = (int)persian_to_eng_num($startRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);

            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');

        } else {
            $startRegDateGre = Carbon::instance(Verta::parse($request->input('start_register'))->datetime());
        }

        if(strpos($request->input('end_register_at') , '/') !== false) {

            $endRegDate = explode('/' , $request->input('end_register_at'));

            $month = (int)persian_to_eng_num($endRegDate[1]);
            $day = (int)persian_to_eng_num($endRegDate[2]);
            $year = (int)persian_to_eng_num($endRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');

        } else {
            $endRegDateGre = Carbon::instance(Verta::parse($request->input('end_register'))->datetime());
        }

        if(strpos($request->input('date') , '/') !== false) {

            $date = explode('/' , $request->input('date'));

            $month = (int)persian_to_eng_num($date[1]);
            $day = (int)persian_to_eng_num($date[2]);
            $year = (int)persian_to_eng_num($date[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $dateGre = $jalaliDate->toCarbon('Asia/Tehran');

        } else {
            $dateGre = Carbon::instance(Verta::parse($request->input('date'))->datetime());
        }


        $eventSans->start_register_at = $startRegDateGre;
        $eventSans->end_register_at = $endRegDateGre;
        $eventSans->date = $dateGre;
        $eventSans->time = $request->input('time');

        $eventSans->save();

        $counter = 1;

        while($request->has('planId'.$counter)){
            
            if(is_null($request->input('planId'.$counter))){

                $plan = EventPlan::create([
                    'event_id' => $request->input('event_id'),
                    'title' => $request->input('planTitle'.$counter),
                    'description' => $request->input('planDescription'.$counter),
                    'price' => $request->input('planPrice'.$counter),
                    'count' => $request->input('planCount'.$counter),
                ]);

                if($request->hasFile('planPhoto'.$counter)){

                    $name = time() . $request->input('planPhoto'.$counter)->getClientOriginalName();
                    $path = $request->input('planPhoto'.$counter)->storeAs(
                        'img/event/plan/photo',
                        $name,
                        'public'
                    );
        
                    $photo = Photo::create([
                        'name' => $name,
                        'path' => $path,
                        'groupable_id' => $plan->id,
                        'groupable_type' => EventPlan::class,
                    ]);
        
                    $plan->photo_id = $photo->id;
                    $plan->save();

                }

                EventInfo::create([
                    'event_id' => $request->input('event_id'),
                    'plan_id' => $plan->id,
                    'sans_id' => $eventSans->id,
                ]);

                $counter++;

            } else {

                EventInfo::create([
                    'event_id' => $request->input('event_id'),
                    'plan_id' => $request->input('planId'.$counter),
                    'sans_id' => $eventSans->id,
                ]);

                $counter++;

            }

        }

        return redirect()
            ->route('sans.event' , $request->input('event_id'))
            ->with('success' , 'سانس جدید با موفقیت ایجاد شد');
    }
    
    public function edit($id)
    {
        $sans = EventSans::with('event')->find($id);

        return view('event.sans.edit' , compact(
            'sans'
        ));
    }

    public function update(Request $request)
    {
        $request->validate([
            'start_register_at' => 'required',
            'end_register_at' => 'required',
            'date' => 'required',
            'time' => 'required',
        ],[
            'start_register_at.required' => 'تاریخ شروع ثبت نام این سانس را وارد کنید',
            'end_register_at.required' => 'تاریخ پایان ثبت نام این سانس را وارد کنید',
            'date.required' => 'تاریخ برگزاری را وارد کنید',
            'time.required' => 'ساعت شروع سانس را وارد کنید',
        ]);

        $eventSans = EventSans::find($request->input('sans_id'));

        if($request->input('title')){
            $eventSans->title = $request->input('title');
        }
        if($request->input('description')){
            $eventSans->description = $request->input('description');
        }

        if(strpos($request->input('start_register_at'), '/') !== false) {

            $startRegDate = explode('/' , $request->input('start_register_at'));

            $month = (int)persian_to_eng_num($startRegDate[1]);
            $day = (int)persian_to_eng_num($startRegDate[2]);
            $year = (int)persian_to_eng_num($startRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);

            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $startRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');

        } else {
            $startRegDateGre = Carbon::instance(Verta::parse($request->input('start_register'))->datetime());
        }

        if(strpos($request->input('end_register_at') , '/') !== false) {

            $endRegDate = explode('/' , $request->input('end_register_at'));

            $month = (int)persian_to_eng_num($endRegDate[1]);
            $day = (int)persian_to_eng_num($endRegDate[2]);
            $year = (int)persian_to_eng_num($endRegDate[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $endRegDateGre = $jalaliDate->toCarbon('Asia/Tehran');

        } else {
            $endRegDateGre = Carbon::instance(Verta::parse($request->input('end_register'))->datetime());
        }

        if(strpos($request->input('date') , '/') !== false) {

            $date = explode('/' , $request->input('date'));

            $month = (int)persian_to_eng_num($date[1]);
            $day = (int)persian_to_eng_num($date[2]);
            $year = (int)persian_to_eng_num($date[0]);

            $jalaliDate = \verta();
            $jalaliDate->year($year);
            $jalaliDate->month($month);
            $jalaliDate->day($day);
            $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
            $dateGre = $jalaliDate->toCarbon('Asia/Tehran');

        } else {
            $dateGre = Carbon::instance(Verta::parse($request->input('date'))->datetime());
        }


        $eventSans->start_register_at = $startRegDateGre;
        $eventSans->end_register_at = $endRegDateGre;
        $eventSans->date = $dateGre;
        $eventSans->time = $request->input('time');

        $eventSans->save();

        return redirect()
            ->route('sans.event' , $request->input('event_id'))
            ->with('success' , 'سانس مورد نظر با موفقیت ویرایش شد');
    }

    public function delete(Request $request)
    {
        $sans = EventSans::find($request->sans_id);

        return response()->json(
            $sans->update([
                'deleted' => true
            ])
        );
    }
}
