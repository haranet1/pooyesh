<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventTicket;
use App\Models\Photo;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index()
    {
        $events = Event::NotDeleted()
            ->orderByDesc('updated_at')
            ->paginate(20);

        return view('event.index' , compact(
            'events'
        ));
    }

    public function create()
    {
        return view('event.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'poster' => 'required|mimes:png,jpg|max:1024',
            'banner' => 'required|mimes:png,jpg|max:2048',
            'map' => 'mimes:png,jpg|max:1024',
        ],[
            'title.required' => 'عنوان رویداد را وارد کنید',
            'description.required' => 'توضیحات را وارد کنید',
            'poster.required' => 'پوستر رویداد را بارگذاری کنید',
            'poster.mimes' => 'پسوند پوستر باید png یا jpg باشد',
            'poster.max' => 'حجم پوستر نباید بیشتر از 1 مگابایت باشد',
            'banner.required' => 'بنر رویداد را بارگذاری کنید',
            'banner.mimes' => 'پسوند بنر باید png یا jpg باشد',
            'banner.max' => 'حجم بنر نباید بیشتر از 2 مگابایت باشد',
            'map.mimes' => 'پسوند نقشه رویداد باید png یا jpg باشد',
            'map.max' => 'حجم نقشه نباید بیشتر از 1 مگابایت باشد',
        ]);

        $is_free = false;
        if($request->has('is_free') && $request->input('is_free') == true){
            $is_free = true;
        }

        $can_multy_buy = false;
        if($request->has('can_multy_buy') && $request->input('can_multy_buy') == true){
            $can_multy_buy = true;
        }

        $company_acceptance = false;
        if($request->has('company_acceptance') && $request->input('company_acceptance') == true){
            $company_acceptance = true;
        }

        $student_acceptance = false;
        if($request->has('student_acceptance') && $request->input('student_acceptance') == true){
            $student_acceptance = true;
        }

        $applicant_acceptance = false;
        if($request->has('applicant_acceptance') && $request->input('applicant_acceptance') == true){
            $applicant_acceptance = true;
        }


        $event = Event::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'is_free' => $is_free,
            'can_multy_buy' => $can_multy_buy,
            'company_acceptance' => $company_acceptance,
            'student_acceptance' => $student_acceptance,
            'applicant_acceptance' => $applicant_acceptance,
        ]);

        if($request->file('poster')){

            $name = time() . $request->poster->getClientOriginalName();
            $path = $request->poster->storeAs(
                'img/event/poster',
                $name,
                'public'
            );

            $poster = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $event->id,
                'groupable_type' => Event::class,
            ]);

            $event->poster_id = $poster->id;
            $event->save();

        }

        if($request->file('banner')){
            
            $name = time() . $request->banner->getClientOriginalName();
            $path = $request->banner->storeAs(
                'img/event/banner',
                $name,
                'public'
            );

            $banner = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $event->id,
                'groupable_type' => Event::class,
            ]);

            $event->banner_id = $banner->id;
            $event->save();

        }

        if($request->file('map')){

            $name = time() . $request->map->getClientOriginalName();
            $path = $request->map->storeAs(
                'img/event/map',
                $name,
                'public'
            );

            $map = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $event->id,
                'groupable_type' => Event::class,
            ]);

            $event->map_id = $map->id;
            $event->save();

        }

        return redirect()->route('create.sans.event' , $event->id)->with('success' , 'رویداد جدید با موفقیت ایجاد شد. لطفا برای این رویداد سانس و پلن تعریف کنید.');
    }

    public function edit($id)
    {
        $event = Event::with('poster' , 'banner' , 'map' , 'tickets')->find($id);

        return view('event.edit' , compact(
            'event'
        ));
    }

    public function update(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'poster' => 'mimes:png,jpg|max:1024',
            'banner' => 'mimes:png,jpg|max:2048',
            'map' => 'mimes:png,jpg|max:1024',
        ],[
            'title.required' => 'عنوان رویداد را وارد کنید',
            'description.required' => 'توضیحات را وارد کنید',
            'poster.mimes' => 'پسوند پوستر باید png یا jpg باشد',
            'poster.max' => 'حجم پوستر نباید بیشتر از 1 مگابایت باشد',
            'banner.mimes' => 'پسوند بنر باید png یا jpg باشد',
            'banner.max' => 'حجم بنر نباید بیشتر از 2 مگابایت باشد',
            'map.mimes' => 'پسوند نقشه رویداد باید png یا jpg باشد',
            'map.max' => 'حجم نقشه نباید بیشتر از 1 مگابایت باشد',
        ]);

        $event = Event::find($request->event_id);

        if($request->has('is_free') && $request->input('is_free') == true){
            $event->is_free = true;
        }elseif ($request->input('is_free') == false){
            $event->is_free = false;
        }

        if($request->has('can_multy_buy') && $request->input('can_multy_buy') == true){
            $event->can_multy_buy = true;
        }elseif ($request->input('can_multy_buy') == false){
            $event->can_multy_buy = false;
        }

        if($request->has('company_acceptance') && $request->input('company_acceptance') == true){
            $event->company_acceptance = true;
        }elseif ($request->input('company_acceptance') == false){
            $event->company_acceptance = false;
        }

        if($request->has('student_acceptance') && $request->input('student_acceptance') == true){
            $event->student_acceptance = true;
        }elseif ($request->input('student_acceptance') == false){
            $event->student_acceptance = false;
        }

        if($request->has('applicant_acceptance') && $request->input('applicant_acceptance') == true){
            $event->applicant_acceptance = true;
        }elseif ($request->input('applicant_acceptance') == false){
            $event->applicant_acceptance = false;
        }

        if($request->file('poster')){

            if($event->poster){
                unlink($event->poster);
            }

            $name = time() . $request->poster->getClientOriginalName();
            $path = $request->poster->storeAs(
                'img/event/poster',
                $name,
                'public'
            );

            $poster = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $event->id,
                'groupable_type' => Event::class,
            ]);

            $event->poster_id = $poster->id;
            $event->save();

        }

        if($request->file('banner')){
            
            if($event->banner){
                unlink($event->banner);
            }
            
            $name = time() . $request->banner->getClientOriginalName();
            $path = $request->banner->storeAs(
                'img/event/banner',
                $name,
                'public'
            );

            $banner = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $event->id,
                'groupable_type' => Event::class,
            ]);

            $event->banner_id = $banner->id;
            $event->save();

        }

        if($request->file('map')){

            if($event->map){
                unlink($event->map);
            }

            $name = time() . $request->map->getClientOriginalName();
            $path = $request->map->storeAs(
                'img/event/map',
                $name,
                'public'
            );

            $map = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $event->id,
                'groupable_type' => Event::class,
            ]);

            $event->map_id = $map->id;
            $event->save();

        }

        if($request->input('title')){
            $event->title = $request->input('title');
        }

        if($request->input('description')){
            $event->description = $request->input('description');
        }

        $event->save();

        return redirect()->route('index.event')->with('success' , 'رویداد' .' '. $event->title . ' ' . 'با موفقیت ویرایش شد');
    }

    public function delete(Request $request)
    {
        $event = Event::with([
                'tickets' => function ($ticket){
                    return $ticket->Payed();
                }
            ])
            ->find($request->event_id);

        if($event->tickets->count() > 0){

            return response()->json('cantDelete');

        }

        return response()->json(
            $event->update([
                'deleted' => true
            ])
        );
    }

    public function active(Request $request)
    {
        $event = Event::find($request->event_id);
        
        if($event->is_active == true){
            $activeChecked = false;
        }else{
            $activeChecked = true;
        }

        return response()->json(
            $event->update([
                'is_active' => $activeChecked
            ])
        );
    }

    public function tickets($eventId)
    {
        $tickets = EventTicket::where('event_id',$eventId)
            ->Payed()
            ->with('user' , 'discount' , 'info.plan' , 'info.sans' , 'codes')
            ->orderByDesc('updated_at')
            ->paginate(20);


        $event = Event::find($eventId);

        return view('event.tickets-list' , compact(
            'tickets',
            'event'
        ));
    }

    public function verifyPage()
    {
        return view('event.verify');
    }
}
