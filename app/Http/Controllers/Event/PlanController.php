<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventPlan;
use App\Models\Photo;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    public function eventPlans($id)
    {
        $event = Event::find($id);
        $plans = EventPlan::NotDeleted()
            ->with('event' , 'photo')
            ->where('event_id' , $id)
            ->orderByDesc('updated_at')
            ->paginate(20);
        
        return view('event.plan.index' , compact(
            'event',
            'plans'
        ));
    }

    public function create($id)
    {
        $event = Event::find($id);

        return view('event.plan.create' , compact(
            'event'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'photo' => 'mimes:png,jpg|max:1024',
            'price' => 'required|numeric',
            'count' => 'required|numeric',
        ],[
            'title.required' => 'عنوان الزامی است',
            'photo.mimes' => 'پسوند پوستر باید png یا jpg باشد',
            'photo.max' => 'حجم پوستر نباید بیشتر از 1 مگابایت باشد',
            'price.required' => 'قیمت پلن الزامی است',
            'price.numeric' => 'قیمت را به صورت عددی و به تومان وارد کنید',
            'count.required' => 'ظرفیت پلن الزامی است',
            'count.numeric' => 'ظرفیت را به صورت عددی وارد کنید',
        ]);

        $plan = EventPlan::create([
            'event_id' => $request->input('event_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description') ?? null,
            'price' => $request->input('price'),
            'count' => $request->input('count'),
        ]);

        if($request->file('photo')){

            $name = time() . $request->photo->getClientOriginalName();
            $path = $request->photo->storeAs(
                'img/event/plan/photo',
                $name,
                'public'
            );

            $photo = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $plan->id,
                'groupable_type' => EventPlan::class,
            ]);

            $plan->photo_id = $photo->id;
            $plan->save();

        }

        return redirect()->route('plans.event' , $request->input('event_id'))->with('پلن جدید با موفقیت ایجاد شد');
    }

    public function edit($id)
    {
        $plan = EventPlan::with('event' , 'photo')->find($id);

        return view('event.plan.edit' , compact(
            'plan'
        ));
    }

    public function update(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'photo' => 'mimes:png,jpg|max:1024',
            'price' => 'required|numeric',
            'count' => 'required|numeric',
        ],[
            'title.required' => 'عنوان الزامی است',
            'photo.mimes' => 'پسوند پوستر باید png یا jpg باشد',
            'photo.max' => 'حجم پوستر نباید بیشتر از 1 مگابایت باشد',
            'price.required' => 'قیمت پلن الزامی است',
            'price.numeric' => 'قیمت را به صورت عددی و به تومان وارد کنید',
            'count.required' => 'ظرفیت پلن الزامی است',
            'count.numeric' => 'ظرفیت را به صورت عددی وارد کنید',
        ]);

        $plan = EventPlan::with('photo')->find($request->plan_id);

        if($request->file('photo')){

            if($plan->photo){
                unlink($plan->photo);
            }
            
            $name = time() . $request->photo->getClientOriginalName();
            $path = $request->photo->storeAs(
                'img/event/plan/photo',
                $name,
                'public'
            );

            $photo = Photo::create([
                'name' => $name,
                'path' => $path,
                'groupable_id' => $plan->id,
                'groupable_type' => EventPlan::class,
            ]);

            $plan->photo_id = $photo->id;
            $plan->save();

        }

        if($request->input('title')){
            $plan->title = $request->input('title');
        }

        if($request->input('price')){
            $plan->price = $request->input('price');
        }

        if($request->input('count')){
            $plan->count = $request->input('count');
        }

        if($request->input('description')){
            $plan->description = $request->input('description');
        }

        $plan->save();
        
        return redirect()->route('plans.event' , $request->input('event_id'))->with('success' , 'پلن مورد نظر با موفقیت ویرایش یافت');
    }

    public function delete(Request $request)
    {
        $plan = EventPlan::find($request->plan_id);

        return response()->json(
            $plan->update([
                'deleted' => true
            ])
        );
    }
}
