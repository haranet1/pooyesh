<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Services\Notification\Providers\SmsProvider;
use App\Services\Payment\Transaction;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    private $transaction ;


    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }


    public function verify($gateway , $payment)
    {
        return $this->transaction->verify($payment)

            ? $this->sendSuccessResponse($payment)

            : $this->sendErrorResponse($payment);
    }

    private function sendSuccessResponse($payment)
    {
        $payment = Payment::find($payment);
        
        return redirect()->route('receipt.event' , $payment->id)->with('success' , $payment->status);
    }

    private function sendErrorResponse($payment)
    {
        $payment = Payment::find($payment);

        return redirect()->route('registred.event')->with('error' , $payment->status);
    }
}

