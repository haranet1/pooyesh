<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\ApplicantInfo;
use App\Models\CompanyInfo;
use App\Models\Discount;
use App\Models\Event;
use App\Models\EventInfo;
use App\Models\EventTicket;
use App\Models\EventTicketCode;
use App\Models\Payment;
use App\Models\StudentInfo;
use App\Services\Payment\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\HtmlString;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TicketController extends Controller
{

    private $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    public function list()
    {
        $events = Event::NotDeleted()
            ->HasInfo()
            ->with(['infos' => function ($query) {
                $query->NotSoldOut();
            }])
            ->orderByDesc('updated_at')
            ->paginate(20);

        $panel = $this->getpanel();

        return view('event.ticket.list' , compact(
            'events',
            'panel'
        ));
    }

    public function letsRegister($eventId)
    {
        $event = Event::with([
                'infos' => function ($query) {
                    $query->RegistrationOpen();
                },
                'infos.plan' => function ($query) {
                    $query->NotDeleted();
                } , 
                'infos.sans' => function ($query) {
                    $query->NotDeleted();
                },
                'infos.plan.info'
            ])
            ->find($eventId);

        if($event->can_multy_buy == false){
            $eventTickets = EventTicket::where('user_id' , Auth::id())->get();
            foreach($event->infos as $info){
                if(! is_null($eventTickets)){
                    foreach($eventTickets as $ticket){
                        if($info->id == $ticket->event_info_id){
                            $info->cantRegisterThisInfo = true;
                        }
                    }
                }
            }
        }

        $panel = $this->getpanel();

        return view('event.ticket.lets-register' , compact(
            'event',
            'panel'
        ));
    }

    public function register(Request $request)
    {
        $info = EventInfo::with('plan')->find($request->info_id);

        if($info->sold_out){

            return Redirect::back()
                ->with('error' , 'ظرفیت سانس مورد نظر پر شده است');

        }

        $payment_key = $this->transaction->checkout();

        return redirect('https://api.iaun.ac.ir/payment-gateway/'.$payment_key);

    }

    public function registred()
    {
        $eventTickets = EventTicket::MyTicket()
            ->with('event', 'payments', 'info.plan', 'info.sans')
            ->orderByDesc('updated_at')
            ->paginate(20);

        $panel = $this->getpanel();

        return view('event.ticket.registred' , compact(
            'eventTickets',
            'panel'
        ));
    }

    public function payAfterRegister(Request $request)
    {
        $eventTicket = EventTicket::with('info.sans' , 'info.plan' , 'discount')->find($request->input('event_ticket_id'));

        if($eventTicket->info->sans->end_register_at <= now()){

            return redirect()->back()->with('error' , 'تاریخ ثبت نام سانس مورد نظر گذشته است');

        }

        $sumPlanCounter = $eventTicket->info->plan_counter + $eventTicket->count;

        if($eventTicket->info->sold_out == true || $sumPlanCounter > $eventTicket->info->plan->count){

            return redirect()->back()->with('error' , 'ظرفیت پلن مورد نظر تکمیل شده است');

        }

        if($eventTicket->has('discount') && ! is_null($eventTicket->discount)){

            if($eventTicket->discount->end <= now()){

                return redirect()->back()->with('error' , 'کد تخفیف استفاده شده منقضی شده است');

            }

            $currentCount = DB::table('discountables')
                ->where('discount_id', $eventTicket->discount->id)
                ->sum('counter');

            if($currentCount >= $eventTicket->discount->max){

                return redirect()->back()->with('error' , 'ظرفیت کد تخفیف تکمیل شده است');

            }

        }

        $payment_key = $this->transaction->payWithoutCheckout($eventTicket);

        return redirect('https://api.iaun.ac.ir/payment-gateway/'.$payment_key);
    }

    public function getEventInfo(Request $request)
    {
        $info = EventInfo::with('plan.info')
            ->find($request->info_id);

        return response()->json($info);
    }

    public function getEventInfo2(Request $request)
    {
        $infos = EventInfo::with('plan.info')
            ->where('event_id' , $request->event_id)
            ->get();

        foreach($infos as $info){
            $info->persion_date = verta($info->sans->date)->formatJalaliDate();
        }

        return response()->json($infos);
    }

    public function applyDiscount(Request $request)
    {
        $discount = Discount::where('name', $request->code)->first();
        
        if(is_null($discount)){

            return response()->json('discountNotDefine');

        }

        if (!($discount->begin <= now())) {
            
            return response()->json('discountNotTime');

        }

        if (!($discount->end >= now())) {
            
            return response()->json('discountTimeout');

        }

        if(! authUserCanUseDiscount($discount)){

            return response()->json('CantUseThis');

        }
        
        $canUseDiscount = $discount->eventInfos()->where('event_infos.id', $request->info_id)->exists();

        if(! $canUseDiscount){

            return response()->json('discountNotForThisInfo');

        }

        $currentCount = DB::table('discountables')
            ->where('discount_id', $discount->id)
            ->sum('counter');

        if($currentCount >= $discount->max){

            return response()->json('discountIsMax');

        }

        if($discount->is_percent){

            $percent = ((int)$discount->value * (int)$request->sub_total) / 100;

            $payablePrice = (int)$request->sub_total - $percent;

            $response = [
                'discount_id' => $discount->id,
                'payablePrice' => $payablePrice,
                'subPrice' => $percent
            ];  

            return response()->json($response);

        } else {

            $response = [
                'discount_id' => $discount->id,
                'payablePrice' => (int)$request->sub_total - (int)$discount->value,
                'subPrice' => (int)$discount->value,
            ];

            return response()->json($response);

        }


    }

    public function receipt($paymentId)
    {
        $payment = Payment::with('order.info.event' , 'order.info.plan' , 'order.info.sans')->find($paymentId);

        $panel = $this->getpanel();

        return view('event.ticket.receipt' , compact(
            'payment',
            'panel'
        ));
    }

    public function printTicket($paymentId)
    {
        $payment = Payment::with('order.info.event' , 'order.info.plan' , 'order.info.sans' , 'order.codes')->find($paymentId);

        foreach($payment->order->codes as $code){
            $qrCodeHtmlString = QrCode::size(100)->generate(route('verify.code.get.event', $code->code));
            $qrCodeHtmlString = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $qrCodeHtmlString);

            $code->qr = new HtmlString($qrCodeHtmlString);
        }

        
        // $pdf = \PDF::loadView('ticket-pattern', compact(
        // 'payment',
        // ), [] , [
        //     'margin_top' => 0,
        //     'margin_left' => 0,
        //     'margin_right' => 0,
        //     'margin_botton' => 0
        // ]);
        // return $pdf->save(public_path('uploads/pdf/') . $payment->id . '.pdf');
            
        // return view('ticket-pattern' , compact('payment'));
        return view('event.ticket.tickets' , compact(
            'payment',
        ));
    }

    public function verifyCodePost(Request $request)
    {
        $request->validate([
            'code' => 'required'
        ]);
        
        $code = EventTicketCode::where('code' , $request->code)->first();

        if(is_null($code)){

            return redirect()->route('verify.page.event')->with('error' , 'کد مورد نظر وجود ندارد');

        }

        $date = Carbon::parse($code->ticket->info->sans->date);

        if($date->lt(Carbon::today())){

            return redirect()->route('verify.page.event')->with('error' , 'تاریخ بلیط برای شرکت در رویداد گذشته است');

        }

        if($date->gt(Carbon::today())){

            return redirect()->route('verify.page.event')->with('error' , 'بلیط امروز قابل استفاده نیست');

        }

        if($code->status == 0 && ! is_null($code->status)){

            return redirect()->route('verify.page.event')->with('error' , 'کد مورد نظر منقضی شده است');

        }

        if($code->status == 1){

            return redirect()->route('verify.page.event')->with('success' , 'کد مورد نظر از قبل تایید شده است');

        }

        $ticket = EventTicket::with('event' , 'info.plan' , 'info.sans' , 'user')->find($code->ticket->id);

        return view('event.verify' , compact(
            'ticket',
            'code'
        ));
    }

    public function login($codeId)
    {
        $code = EventTicketCode::find($codeId);

        $code->update([
            'status' => 1
        ]);

        return redirect()->route('verify.page.event')->with('success' , 'ورود بلیط در رویداد ثبت شد و قادر به استفاده مجدد وجود ندارد.');
    }

    public function voidCode($codeId)
    {
        $code = EventTicketCode::find($codeId);

        $code->update([
            'status' => 0
        ]);

        return redirect()->route('verify.page.event')->with('success' , 'بلیط مورد نظر باطل شد و دیگر امکان ورود در رویداد وجود ندارد. ');
    }

    protected function getpanel()
    {
        if(Auth::user()->groupable_type == CompanyInfo::class){
            return 'panel.company.master';
        }
        elseif(Auth::user()->groupable_type == StudentInfo::class){
            return 'panel.student.master';
        }
        elseif(Auth::user()->groupable_type == ApplicantInfo::class){
            return 'panel.student.master';
        }
    }


}
