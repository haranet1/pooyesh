<?php

namespace App\Http\Controllers\Onet;

use App\Models\Ojob;
use App\Models\Olevel;
use App\Models\Ocategory;
use App\Models\OjobSkill;
use App\Models\OabilityJob;
use App\Models\Oimportance;
use Illuminate\Http\Request;
use App\Models\OjobKnowledge;
use App\Models\OjobWorkStyle;
use App\Models\OjobTechnologySkill;
use App\Http\Controllers\Controller;

class JobsController extends Controller
{
    public function standardIndex()
    {
        $categories = Ocategory::select('ocategories.id', 'ocategories.title') 
        ->join('ojobs', 'ojobs.category_id', '=', 'ocategories.id') 
        ->join('ojob_positions', 'ojob_positions.job_id', '=', 'ojobs.id') 
        ->groupBy('ocategories.id', 'ocategories.title') 
        ->orderByRaw('COUNT(ojob_positions.id) DESC') 
        ->get();

        return view('frontend.ojobs.index', compact('categories'));
    }

    public function categoryJob(Request $request)
    {

        $title = 'دسته بندی شغل ها';
        $categories = Ocategory::select('ocategories.id', 'ocategories.title') 
        ->join('ojobs', 'ojobs.category_id', '=', 'ocategories.id') 
        ->join('ojob_positions', 'ojob_positions.job_id', '=', 'ojobs.id') 
        ->groupBy('ocategories.id', 'ocategories.title') 
        ->orderByRaw('COUNT(ojob_positions.id) DESC') 
        ->get();
        $query = Ojob::query();
        $query = $query->where('description', '!=', null)->with('category');

        if($request->filled('category')){
            $query->where('category_id', $request->category);
            $category = Ocategory::find($request->category);
            $title = $category->title;
        }

        $allJobs = $query->get();
        $jobs = $query->paginate(30);
        $jobs->appends(['category' => $request->category]);

        $oldData = $request->all();
    
        return view('frontend.ojobs.category-single', compact('title','categories','jobs','oldData','allJobs'));
    }


    public function singleJob($jobId)
    {
        $job = Ojob::with('category','tasks')->findOrFail($jobId);

        $skills = OjobSkill::where('job_id' , $jobId)->with('skillname','importance', 'level')->get()->sortByDesc(function ($query){
            return $query->importance->data_value;
        });
        $knowledges = OjobKnowledge::where('job_id' , $jobId)->with('knowledgeName','importance', 'level')->get()->sortByDesc(function ($query){
            return $query->importance->data_value;
        });
        $abilities = OabilityJob::where('job_id' , $jobId)->with('abilityName','importance', 'level')->get()->sortByDesc(function ($query){
            return $query->importance->data_value;
        });
        $workStyles = OjobWorkStyle::where('job_id' , $jobId)->with('workStyleName','importance', 'level')->get()->sortByDesc(function ($query){
            return $query->importance->data_value;
        });
        $technologySkills = OjobTechnologySkill::where('job_id' , $jobId)->with('technologySkillName','importance', 'example')->get()->sortByDesc( function ($TechnologySkill) {
            if(! is_null($TechnologySkill->importance)){
                return $TechnologySkill->importance->data_value;
            }
        });

        return view('frontend.ojobs.job-single', compact('job','skills','knowledges','abilities','workStyles','technologySkills'));
    }

}
