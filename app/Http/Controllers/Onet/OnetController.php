<?php

namespace App\Http\Controllers\Onet;

use App\Helpers\CompanyLogActivity;
use App\Http\Controllers\Controller;
use App\Models\CooperationRequest;
use App\Models\Job_position;
use App\Models\Major;
use App\Models\OabilityJob;
use App\Models\Ocategory;
use App\Models\Ojob;
use App\Models\OjobKnowledge;
use App\Models\OjobPosition;
use App\Models\OjobSkill;
use App\Models\OjobTechnologySkill;
use App\Models\OjobWorkStyle;
use App\Models\OJpAbility;
use App\Models\OJpFacilities;
use App\Models\OJpFavorite;
use App\Models\OJpKnowledge;
use App\Models\OJpSkill;
use App\Models\OJpTask;
use App\Models\OJpTechnologySkill;
use App\Models\OJpWorkStyle;
use App\Models\Otask;
use App\Models\Province;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;

class OnetController extends Controller
{
    public function getJobsByCatId(Request $request)
    {
        $jobs = Ojob::where('category_id' , $request->cat_id)
            ->get();

        return response()->json($jobs);
    }

    public function getJobsByCatIdAndName(Request $request)
    {
        // $jobs = Ojob::where('category_id' , $request->cat_id)
        //     ->where('title' , 'LIKE' , '%'.$request->job_title.'%')
        //     ->get();
        $jobs = Ojob::where('title' , 'LIKE' , '%'.$request->job_title.'%')
            ->whereNotNull('description')
            ->get();

        return response()->json($jobs);
    }

    public function addFavorite(Request $request)
    {
        $favoriteJob = OJpFavorite::where('user_id' , Auth::id())
            ->where('job_position_id' , $request->job_id)
            ->first();

            
        if(! is_null($favoriteJob)){
            // dd($favoriteJob);
            $favoriteJob->delete();
            // $favoriteJob->save();

            return response()->json(true);
        }

        $favoriteJob = OJpFavorite::create([
            'user_id' => Auth::id(),
            'job_position_id' => $request->job_id
        ]);

        return response()->json(true);
    }
}
