<?php

namespace App\Http\Controllers;

use App\Models\CompanyProduct;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CompanyProductController extends Controller
{
    public function index()
    {
        $products = CompanyProduct::where('owner_id',Auth::id())->get();
        return view('panel.company.products',compact('products'));

    }

    public function create()
    {
        return view('panel.company.create-product');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'type'=>'required',
            'description'=>'required',
            'photo'=>'image',
        ]);

        $product = new CompanyProduct();
        $product->owner_id = Auth::id();
        $product->name = $request->name;
        $product->type = $request->type;
        $product->description = $request->description;
        $product->save();

        if ($request->file('photo')){
            $name = time() . $request->photo->getClientOriginalName();
            $path = $request->photo->storeAs(
                'img/company',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $product->id;
            $photo->groupable_type = CompanyProduct::class;
            $photo->save();
        }
        $product->photo_id = $photo->id;
        $product->save();

       \CompanyLogActivity::addToLog('یک محصول برای شرکت ثبت کرد');

        return redirect(route('company.products.index'))->with('success',__('public.data_added'));
    }

    public function edit(CompanyProduct $product)
    {
        return view('panel.company.edit-product',compact('product'));
    }

    public function update(Request $request,CompanyProduct $product)
    {
        if ($request->file('photo')){
            if ($product->photo){
                Storage::disk('public')->delete($product->photo->path);
                $product->photo->delete();
            }

            $name = time() . $request->photo->getClientOriginalName();
            $path = $request->photo->storeAs(
                'img/company',
                $name,
                'public'
            );
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->groupable_id = $product->id;
            $photo->groupable_type = CompanyProduct::class;
            $photo->save();
            $product->photo_id = $photo->id;
            $product->save();
        }
        $product->update([
            'name'=>$request->name,
            'type'=>$request->type,
            'description'=>$request->description,
        ]);
       \CompanyLogActivity::addToLog('یک محصول را ویرایش کرد');

        return redirect(route('company.products.index'))->with('success',__('public.data_added'));

    }
}
