<?php

use App\Models\ApplicantInfo;
use App\Models\CompanyInfo;
use App\Models\CooperationRequest;
use App\Models\PooyeshRequestProcess;
use App\Models\ResumeSent;
use App\Models\StudentInfo;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;
use Mpdf\Image\Svg;

use function PHPUnit\Framework\returnSelf;

function has_internet()
{
    $connected = @fsockopen("www.google.com", 80);
    //website, port  (try 80 or 443)
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}

function job_type_persian($type)
{
    switch ($type) {
        case 'full-time':
            return 'تمام وقت';
        case 'part-time':
            return 'پاره وقت';
        case 'remote':
            return 'دورکاری';
        case 'intern':
            return 'کارآموزی';
    }
}

function job_type_persian_to_english($type)
{
    switch ($type) {
        case 'تمام وقت':
            return 'full-time';
        case 'پاره وقت':
            return 'part-time';
        case 'دورکاری':
            return 'remote';
        case 'کارآموزی':
            return 'intern';
    }
}

function job_type_persian_badge($type)
{
    switch ($type) {
        case 'full-time':
            echo '<span class="badge bg-primary">تمام وقت</span>';
            break;
        case 'part-time':
            echo '<span class="badge bg-info">پاره وقت</span>';
            break;
        case 'remote':
            echo '<span class="badge bg-warning">دورکاری</span>';
            break;
        case 'intern':
            echo '<span class="badge bg-success">کارآموزی</span>';
            break;
    }
}

function job_level_persian($level)
{
    switch ($level) {
        case 'unmatter':
            return 'فرقی ندارد';
        case 'senior':
            return 'حرفه ای';
        case 'mid-level':
            return 'متوسط';
        case 'junior':
            return 'تازه کار';
    }
}

function job_experience_to_fa($experience)
{
    switch($experience){
        case 'unmatter':
            return 'فرقی ندارد';
        case '2':
            return 'تا ۲ سال';
        case '5':
            return '3 تا 5 سال';
        case '10':
            return '6 تا 10 سال';
        case '11':
            return 'بالای 10 سال';
    }
}

function show_student_info($student)
{
    if(Auth::check() && Auth::id() == $student) return true;

    if (Auth::check() && Auth::user()->groupable_type == \App\Models\EmployeeInfo::class) return true;

    if (Auth::check() && Auth::user()->groupable_type == \App\Models\CompanyInfo::class) {
        $request = CooperationRequest::where([['sender_id', $student], ['receiver_id', Auth::id()]])->count();
        
        // dd($request);
        if ($request > 0) return true;
        else return false;
        // $pooyesh = CooperationRequest::where([['type' , 'intern'],['sender_id', $student], ['receiver_id', \Illuminate\Support\Facades\Auth::id()]])->count();
        // dd($pooyesh);
    }
    return false;
}

function show_student_resume($student)
{
    if(Auth::check() && Auth::id() == $student) return true;

    if (Auth::check() && Auth::user()->groupable_type == \App\Models\EmployeeInfo::class) return true;

    if(Auth::check() && Auth::user()->groupable_type == \App\Models\CompanyInfo::class) {
        $resume = ResumeSent::where([['sender_id', $student], ['receiver_id', Auth::id()]])->count();
        
        if($resume > 0 ) return true;
        else return false;
    }
}

function persian_to_eng_num($string)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

    $output = str_replace($persian, $english, $string);
    return $output;
}

function getPooyeshProcessStatus($request)
{
    $model = \App\Models\PooyeshRequestProcess::find($request);

    $recentlyUpdatedColumn = $model->getMostRecentlyUpdatedColumn();

    switch ($recentlyUpdatedColumn) {
        case 'accepted_by_co_at':
            return 'قبول توسط شرکت';
        case 'accepted_by_std_at':
            return 'قبول توسط کارجو';
        case 'canceled_by_co_at':
            return 'لغو توسط شرکت';
        case 'canceled_by_std_at':
            return 'لغو توسط کارجو';
        case 'rejected_by_co_at':
            return 'رد توسط شرکت';
        case 'rejected_by_std_at':
            return 'رد توسط کارجو';
        case 'rejected_by_uni_at':
            return 'رد توسط دانشگاه';
        case 'accepted_by_uni_at':
            return 'قبول توسط دانشگاه';
        case 'contract_printed_at':
            return 'چاپ شده';
        case 'done_at':
            return 'اتمام';
    }
}

function sortDatetimeArray($array)
{
    uksort($array, function ($key1, $key2) use ($array) {
        $value1 = $array[$key1];
        $value2 = $array[$key2];

        if ($value1 === null && $value2 === null) {
            return 0;
        } elseif ($value1 === null) {
            return 1;
        } elseif ($value2 === null) {
            return -1;
        }

        $datetime1 = Carbon::parse($value1);
        $datetime2 = Carbon::parse($value2);

        return $datetime1 <=> $datetime2;
    });

    return $array;
}

function getPooyeshProcess($request)
{
    $request = \App\Models\PooyeshRequestProcess::where('req_id', $request)->get(['accepted_by_co_at', 'accepted_by_std_at', 'canceled_by_co_at', 'canceled_by_std_at', 'rejected_by_co_at', 'rejected_by_std_at', 'rejected_by_uni_at', 'accepted_by_uni_at', 'contract_printed_at', 'done_at'])->toArray();
    $process = [];
    for ($i = 0; $i < count($request); $i++) {
        if (!is_null($request[0]['accepted_by_co_at'])) {
            $process += [
                'قبول توسط شرکت' => $request[0]['accepted_by_co_at']
            ];
        }
        if (!is_null($request[0]['accepted_by_std_at'])) {
            $process += [
                'قبول توسط کارجو' => $request[0]['accepted_by_std_at']
            ];
        }
        if (!is_null($request[0]['canceled_by_co_at'])) {
            $process += [
                'لغو توسط شرکت' => $request[0]['canceled_by_co_at']
            ];
        }
        if (!is_null($request[0]['canceled_by_std_at'])) {
            $process += [
                'لغو توسط کارجو' => $request[0]['canceled_by_std_at']
            ];
        }
        if (!is_null($request[0]['rejected_by_co_at'])) {
            $process += [
                'رد توسط شرکت' => $request[0]['rejected_by_co_at']
            ];
        }
        if (!is_null($request[0]['rejected_by_std_at'])) {
            $process += [
                'رد توسط کارجو' => $request[0]['rejected_by_std_at']
            ];
        }
        if (!is_null($request[0]['rejected_by_uni_at'])) {
            $process += [
                'رد توسط دانشگاه' => $request[0]['rejected_by_uni_at']
            ];
        }
        if (!is_null($request[0]['accepted_by_uni_at'])) {
            $process += [
                'قبول توسط دانشگاه' => $request[0]['accepted_by_uni_at']
            ];
        }
        if(!is_null($request[0]['accepted_by_uni_at']) && is_null($request[0]['contract_printed_at'])){
            $process += [
                'مراجعه دانشجو به دبیرخانه' => $request[0]['accepted_by_uni_at']
            ];
        }
        if(!is_null($request[0]['contract_printed_at'])) {
            $process += [
                'قرارداد به دانشجو تحویل داده شد' => $request[0]['accepted_by_uni_at']
            ];
        }
        if (!is_null($request[0]['done_at'])) {
            $process += [
                'اتمام' => $request[0]['done_at']
            ];
        }
    }
    $sortedArray = sortDatetimeArray($process);
    return $sortedArray;
}

function getNextPooyeshProcess($request)
{
    switch($request){
        case 'accepted_by_std_at':
        case 'accepted_by_co_at':
            return 'درانتظار تایید دانشگاه';
            break;
        case 'accepted_by_uni_at':
            return 'در انتظار مراجعه دانشجو به دبیرخانه';
            break;
        case 'contract_printed_at':
        case 'course_unit_registration_at':
            return 'در انتظار پایان کارآموزی';
            break;
        case 'done_at':
            return 'در انتظار ثبت نمره برای کارآموزی';
            break;
    }
}

function getBuyPlanBooths($id)
{
    $booths = \App\Models\SadraBooth::where('plan_id', $id)->get();
    if (count($booths) > 0) return $booths;
    else return false;
}

function getUserNameByID($id)
{

   $user = \App\Models\User::with('groupable')->findorfail($id);

    if ($user->groupable instanceof \App\Models\CompanyInfo ){
       return $user->groupable->name;
    }elseif ($user->groupable instanceof \App\Models\StudentInfo){
        return  $user->name." ".$user->family;
    }
    return null;
}

function persian_to_eng_date($date)
{

    if (strpos($date, '/') !== false) {
        $startDate = explode('/', $date);
    
        $month = (int)persian_to_eng_num($startDate[1]);
        $day = (int)persian_to_eng_num($startDate[2]);
        $year = (int)persian_to_eng_num($startDate[0]);
    
        $jalaliDate = \verta();
        $jalaliDate->year($year);
        $jalaliDate->month($month);
        $jalaliDate->day($day);
        $jalali = Jalalian::fromFormat('Y-m-d', $jalaliDate->formatDate());
        $DateGre = $jalaliDate->toCarbon('Asia/Tehran');
    } else {
        $DateGre = Carbon::instance(Verta::parse($date)->datetime());
    }

    return $DateGre;

}

function level_badge($data)
{
    if($data == 'عالی'){
        echo '<span class="badge rounded-pill bg-success">' . $data . '</span>';
    }elseif($data == 'خوب'){
        echo '<span class="badge rounded-pill bg-primary">'. $data . '</span>';

    }elseif($data == 'متوسط'){
        echo '<span class="badge rounded-pill bg-warning">'. $data . '</span>';

    }elseif($data == 'ضعیف'){
        echo '<span class="badge rounded-pill bg-danger">'. $data . '</span>';

    }
}

function social_network_icons($data)
{
    if($data == 'instagram'){
        echo '<img class="me-1" src="'. asset('assets/front/images/instagram.svg') .'">';
    }elseif($data == 'github'){
        echo '<img class="me-1" src="'. asset('assets/front/images/github.svg') .'">';
    }elseif($data == 'telegram'){
        echo '<img class="me-1" src="'. asset('assets/front/images/telegram.svg') .'">';
    }elseif($data == 'twitter'){
        echo '<img class="me-1" src="'. asset('assets/front/images/twitter.svg') .'">';
    }elseif($data == 'linkedin'){
        echo '<img class="me-1" src="'. asset('assets/front/images/linkedin.svg') .'">';
    }elseif($data == 'eitaa'){
        echo '<img class="me-1" src="'. asset('assets/front/images/eitaa.svg') .'">';
    }
    
}

function social_network_link($social)
{
    if($social->name == 'instagram'){
        echo '<a href="https://instagram.com/' .trim($social->link).'">' .$social->link. '</a>';
    }
    if($social->name == 'github'){
        echo '<a href="https://github.com/' .trim($social->link).'">' .$social->link. '</a>';
    }
    if($social->name == 'telegram'){
        echo '<a href="https://t.me/' .trim($social->link).'">' .$social->link. '</a>';
    }
    if($social->name == 'twitter'){
        echo '<a href="https://twitter.com/' .trim($social->link).'">' .$social->link. '</a>';
    }
    if($social->name == 'linkedin'){
        echo '<a href="https://linkedin.com/in/' .trim($social->link).'">' .$social->link. '</a>';
    }
    if($social->name == 'eitaa'){
        echo '<a href="https://eitaa.com/' .trim($social->link).'">' .$social->link. '</a>';
    }
}

function percent(float $max ,float $target): float
{
    $orgNumber = ($target*100)/$max;
    $nextNum = number_format($orgNumber,2);
    return $nextNum;
}

function enneagram_result_fa($data)
{
    if($data==null)
        return "ثبت نشده";
    switch($data)
    {
        case 'perfectionist':
            return 'اصلاح طلب و کمالگرا';
        case 'helpful':
            return 'مهر طلب یا کمک گرا';
        case 'progressive':
            return 'پیشرفت گرا یا موفقیت طلب';
        case 'artist':
            return 'رمانتیک یا هنرمند';
        case 'curious':
            return 'کنجکاو و فکور';
        case 'loyal':
            return 'وفاخو، وفادار و شکاک';
        case 'optimist':
            return 'خوش بین و خوشگذران';
        case 'leader':
            return 'رهبر و مدیر';
        case 'peaceful':
            return 'صلح طلب و میناجی';
    }
}

function enneagram_personality_to_fa($types)
{
    $fa_types = [];
    foreach($types as $type){
        if($type == 'perfectionist'){
            array_push($fa_types , 'اصلاح طلب و کمالگرا');
        }
        if($type == 'helpful'){
            array_push($fa_types ,'مهر طلب یا کمک گرا');
        }
        if($type == 'progressive'){
            array_push($fa_types ,'پیشرفت گرا یا موفقیت طلب');
        }
        if($type == 'artist'){
            array_push($fa_types ,'رمانتیک یا هنرمند');
        }
        if($type == 'curious'){
            array_push($fa_types ,'کنجکاو و فکور');
        }
        if($type == 'loyal'){
            array_push($fa_types ,'وفاخو، وفادار و شکاک');
        }
        if($type == 'optimist'){
            array_push($fa_types ,'خوش بین و خوشگذران');
        }
        if($type == 'leader'){
            array_push($fa_types ,'رهبر و مدیر');
        }
        if($type == 'peaceful'){
            array_push($fa_types ,'صلح طلب و میناجی');
        }
    }
    return $fa_types;
}

function change_sex_fa_to_en($sex)
{
    if($sex == 'مرد'){
        return 'male';
    }elseif($sex == 'زن'){
        return 'female';
    }
}
function change_sex_to_fa($sex)
{
    if($sex == 'male'){
        return 'مرد';
    }else{
        return 'زن';
    }
}


function change_marige_type_fa_to_en($marige_type)
{
    if($marige_type == 'مجرد'){
        return 'single';
    }elseif($marige_type == 'متاهل'){
        return 'married';
    }
}

function change_marige_type_to_fa($marige_type)
{
    if($marige_type == 'single'){
        return 'مجرد';
    }elseif($marige_type == 'married'){
        return 'متاهل';
    }else{
        return 'فرقی ندارد';
    }
}

function job_province_and_city_name($province , $city)
{
    if($province && $city){
        return 'در '.$province->name.'، '.$city->name;
    }
    if($province && !$city){
        return 'در استان '.$province->name;
    }
    if(!$province && $city){
        return 'در شهر '.$city->name;
    }
    if(!$province && !$city){
        return 'مکان تایین نشده';
    }
}

function user_has_role($user)
{
    if($user->hasRole('company')){
        return true;
    }elseif($user->hasRole('student')){
        return true;
    }elseif($user->hasRole('applicant')){
        return true;
    }elseif($user->hasRole('employee')){
        return true;
    }else{
        return false;
    }
}
function user_has_role_fa($user)
{
    if($user->hasRole('company')){
        return 'شرکت';
    }elseif($user->hasRole('student')){
        return 'دانشجو';
    }elseif($user->hasRole('applicant')){
        return 'کارجو';
    }elseif($user->hasRole('employee')){
        return 'کارمند';
    }else{
        return 'ثبت نشده';
    }
}

function neo_result_fa(string $result)
{
    if($result == 'N'){
        return 'بی ثباتی ،هیجانی';
    }
    if($result == 'E'){
        return 'برون گرایی ،درون گرایی';
    }
    if($result == 'O'){
        return 'انعطاف پذیری';
    }
    if($result == 'A'){
        return 'توافق با دیگران';
    }
    if($result == 'C'){
        return 'وظیفه شناسی';
    }
}

function neo_personality_to_fa($types)
{
    $fa_types = [];
    foreach($types as $type){
        if($type == 'N'){
            array_push($fa_types,'بی ثباتی ،هیجانی');
        }
        if($type == 'E'){
            array_push($fa_types,'برون گرایی ،درون گرایی');
        }
        if($type == 'O'){
            array_push($fa_types,'انعطاف پذیری');
        }
        if($type == 'A'){
            array_push($fa_types,'توافق با دیگران');
        }
        if($type == 'C'){
            array_push($fa_types,'وظیفه شناسی');
        }
    }
    return $fa_types;
}

function get_range_ghq_test($types)
{
    $data = [
        'A' => '',
        'B' => '',
        'C' => '',
        'D' => '',
    ];
    foreach($types as $key => $value){
        if(0 <= $value && $value <= 6){
            $data[$key] = 'هیچ یا کمترین حد';
        }
        if(7 <= $value && $value <= 11){
            $data[$key] = 'خفیف';
        }
        if(12 <= $value && $value <= 16){
            $data[$key] = 'متوسط';
        }
        if(17 <= $value && $value <= 21){
            $data[$key] = 'شدید';
        }
    }
    $result= [
        'physical' => $data['A'],
        'anxiety' => $data['B'],
        'social' => $data['C'],
        'depression' => $data['D'],
    ];
    
    return $result;
}
function get_range_of_all_ghq($data)
{
    if(0 <= $data && $data <= 22){
        return  'هیچ یا کمترین حد';
    }
    if(23 <= $data && $data <= 40){
        return 'خفیف';
    }
    if(41 <= $data && $data <= 60){
        return 'متوسط';
    }
    if(61 <= $data && $data <= 84){
        return 'شدید';
    }
}
function get_range_ghq_array($types,$all)
{
    $data= [
        'physical' => $types['physical'],
        'anxiety' => $types['anxiety'],
        'social' => $types['social'],
        'depression' => $types['depression'],
        'all' => $all,
    ];
    foreach($data as $key => $value){
        if(0 <= $value && $value <= 6){
            $data[$key] = 'هیچ یا کمترین حد';
        }
        if(7 <= $value && $value <= 11){
            $data[$key] = 'خفیف';
        }
        if(12 <= $value && $value <= 16){
            $data[$key] = 'متوسط';
        }
        if(17 <= $value && $value <= 21){
            $data[$key] = 'شدید';
        }
    }
    if(0 <= $all && $all <= 22){
        $data['all'] = 'هیچ یا کمترین حد';
    }
    if(23 <= $all && $all <= 40){
        $data['all'] = 'خفیف';
    }
    if(41 <= $all && $all <= 60){
        $data['all'] = 'متوسط';
    }
    if(61 <= $all && $all <= 84){
        $data['all'] = 'شدید';
    }
    return $data;
}

function set_ghq_header($data)
{
    if(0 <= $data && $data <= 22){
        return  'ghq-header1.svg';
    }
    if(23 <= $data && $data <= 40){
        return 'ghq-header2.svg';
    }
    if(41 <= $data && $data <= 60){
        return 'ghq-header3.svg';
    }
    if(61 <= $data && $data <= 84){
        return 'ghq-header4.svg';
    }
}

function get_haaland_photo($item)
{
    if($item['title'] == 'R'){
        return 'r.svg';
    }
    elseif($item['title'] == 'I'){
        return 'i.svg';
    }
    elseif($item['title'] == 'A'){
        return 'a.svg';
    }
    elseif($item['title'] == 'S'){
        return 's.svg';
    }
    elseif($item['title'] == 'E'){
        return 'e.svg';
    }
    elseif($item['title'] == 'C'){
        return 'c.svg';
    }
}

function mbti_personality_to_fa($types)
{
    $fa_types = [];
    foreach($types as $type){
        if($type == 'ENFJ'){
            array_push($fa_types,'قهرمان');
        }
        if($type == 'ENFP'){
            array_push($fa_types,'پیکارگر');
        }
        if($type == 'ESFJ'){
            array_push($fa_types,'سفیر');
        }
        if($type == 'ESFP'){
            array_push($fa_types,'سرگرم کننده');
        }
        if($type == 'INFJ'){
            array_push($fa_types,'حامی');
        }
        if($type == 'INFP'){
            array_push($fa_types,'میانجی');
        }
        if($type == 'INTJ'){
            array_push($fa_types,'معمار');
        }
        if($type == 'INTP'){
            array_push($fa_types,'منطق دان');
        }
        if($type == 'ISFJ'){
            array_push($fa_types,'مدافع');
        }
        if($type == 'ISFP'){
            array_push($fa_types,'ماجراجو');
        }
        if($type == 'ISTJ'){
            array_push($fa_types,'تدارکاتچی');
        }
        if($type == 'ISTP'){
            array_push($fa_types,'چیره دست');
        }
        if($type == 'ESTJ'){
            array_push($fa_types,'مجری');
        }
        if($type == 'ENTJ'){
            array_push($fa_types,'فرمانده');
        }
        if($type == 'ENTP'){
            array_push($fa_types,'مجادله گر');
        }
        if($type == 'ESTP'){
            array_push($fa_types,'کارآفرین');
        }
        
    }
    return $fa_types;
}

function get_mbti_prsonality_to_fa($type)
{
    if($type == 'ENFJ'){
        return 'قهرمان';
    }
    if($type == 'ENFP'){
        return 'پیکارگر';
    }
    if($type == 'ESFJ'){
        return 'سفیر';
    }
    if($type == 'ESFP'){
        return 'سرگرم کننده';
    }
    if($type == 'INFJ'){
        return 'حامی';
    }
    if($type == 'INFP'){
        return 'میانجی';
    }
    if($type == 'INTJ'){
        return 'معمار';
    }
    if($type == 'INTP'){
        return 'منطق دان';
    }
    if($type == 'ISFJ'){
        return 'مدافع';
    }
    if($type == 'ISFP'){
        return 'ماجراجو';
    }
    if($type == 'ISTJ'){
        return 'تدارکاتچی';
    }
    if($type == 'ISTP'){
        return 'چیره دست';
    }
    if($type == 'ESTJ'){
        return 'مجری';
    }
    if($type == 'ENTJ'){
        return 'فرمانده';
    }
    if($type == 'ENTP'){
        return 'مجادله گر';
    }
    if($type == 'ESTP'){
        return 'کارآفرین';
    }
}

function haaland_personality_to_fa($types)
{
    $fa_types = [];
    foreach($types as $type){
        if($type == 'R'){
            array_push($fa_types,'واقع گرا');
        }
        if($type == 'I'){
            array_push($fa_types,'جستجوگر');
        }
        if($type == 'A'){
            array_push($fa_types,'هنری');
        }
        if($type == 'S'){
            array_push($fa_types,'اجتماعی');
        }
        if($type == 'E'){
            array_push($fa_types,'سازنده');
        }
        if($type == 'C'){
            array_push($fa_types,'قاعده‌مند');
        }
        
    }
    return $fa_types;
}

function ghq_personality_to_fa($types)
{
    $fa_types = [];
    foreach($types as $type){
        if($type == 'physical'){
            array_push($fa_types,'جسمانی');
        }
        if($type == 'anxiety'){
            array_push($fa_types,'اضطرابی');
        }
        if($type == 'social'){
            array_push($fa_types,'اجتماعی');
        }
        if($type == 'depression'){
            array_push($fa_types,'افسردگی');
        }
        if($type == 'sum_of_all'){
            array_push($fa_types,'کل');
        }
        
    }
    return $fa_types;
}

function get_role_names($roles)
{
    $rand = ['success','dark','warning','primary', 'secondary' , 'info'];
    foreach($roles as $role){
        echo '<div class="badge mx-1 bg-'.$rand[rand(0,5)].'">'.$role.'</div>';
    }
}

function get_permission_fa_name_via_role($permissions)
{
    $fa_permissions = [];
    foreach($permissions as $per){
        switch($per){
            case 'createUser':
                array_push($fa_permissions , 'افزودن کاربر');
                break;
            case 'unconfirmedUser':
                array_push($fa_permissions , 'کاربران تایید نشده');
                break;
            case 'confirmedUser':
                array_push($fa_permissions , 'کاربران تایید شده');
                break;
            case 'importedUser':
                array_push($fa_permissions , 'کاربران ایمپورت شده');
                break;
            case 'testedUser':
                array_push($fa_permissions , 'کاربران تست داده');
                break;
            case 'exportAllUser':
                array_push($fa_permissions , 'خروجی تمام کاربران');
                break;
            case 'unconfirmedJobs':
                array_push($fa_permissions , 'موقعیت های شغلی تایید نشده');
                break;
            case 'pooyeshRequest':
                array_push($fa_permissions , 'درخواست های پویش');
                break;
            case 'hireRequest':
                array_push($fa_permissions , 'درخواست های استخدام');
                break;
            case 'manageRegulations':
                array_push($fa_permissions , 'مدیریت آیین نامه ها');
                break;
            case 'manageNotifications':
                array_push($fa_permissions , 'مدیریت اطلاعیه ها');
                break;
            case 'signature':
                array_push($fa_permissions , 'افزودن امضاء');
                break;
            case 'manageEvents':
                array_push($fa_permissions , 'مدیریت رویداد ها');
                break;
            case 'support':
                array_push($fa_permissions , 'پشتیبانی');
                break;
            case 'readLog':
                array_push($fa_permissions , 'مشاهده گزارشات');
                break;
            case 'managePermission':
                array_push($fa_permissions , 'مدیریت دسترسی ها');
                break;
            case 'secretariat':
                array_push($fa_permissions , 'دبیرخانه');
                break;
            case 'manageArticles':
                array_push($fa_permissions , 'مدیریت مجله');
                break;
        }
    }
    return $fa_permissions;
}

function change_filters_name_to_fa($key , $value)
{
    switch($key){
        case 'type_intern':
            if($value == "on"){
                return 'موقعیت های کارآموزی';
            }
            break;
        case 'type_hire':
            if($value == "on"){
                return 'موقعیت های استخدامی';
            }
            break;
        case 'grade':
            return 'تحصیلات : '.$value;
            break;
        case 'workExperience':
            return 'سابقه کاری : '.$value;
            break;
        case 'salary': 
            return 'حقوق : '.$value;
            break;
        case 'age':
            return 'سن : '.$value;
            break;
        case 'city':
            return 'شهر : '.$value;
            break;
        
    }
}

function authUserCanRegisterInEvent($event)
{
    if($event->company_acceptance == true && Auth::user()->groupable_type == CompanyInfo::class){

        return true;

    }elseif($event->student_acceptance == true && Auth::user()->groupable_type == StudentInfo::class){

        return true;

    }elseif($event->applicant_acceptance == true && Auth::user()->groupable_type == ApplicantInfo::class){

        return true;

    }

    return false;
}

function authUserCanUseDiscount($discount)
{
    if($discount->company_acceptance == true && Auth::user()->groupable_type == CompanyInfo::class){

        return true;

    }elseif($discount->student_acceptance == true && Auth::user()->groupable_type == StudentInfo::class){

        return true;

    }elseif($discount->applicant_acceptance == true && Auth::user()->groupable_type == ApplicantInfo::class){

        return true;

    }

    return false;
}

function getPercent(int $pecent, int $totla)
{
    $num = 100 / $pecent;

    return $totla / $num;
}

function getOnetLevel($level)
{
    $round = round($level);

    if (!is_float($round) || $round < 0) {
        return 'ضعیف'; 
    }

    if($round > 5){
        $round = 5;
    }
    
    switch ($round){
        case 0 :
            return 'ضعیف';
        case 1 :
            return 'ضعیف';
        case 2 :
            return 'مبتدی';
        case 3 :
            return 'متوسط';
        case 4 :
            return 'خوب';
        case 5 :
            return 'عالی';

    }
}

function changeFaLevelToOnetLevel($level)
{
    switch($level){
        case 'ضعیف':
            return 1;
        case 'مبتدی':
            return 2;
        case 'متوسط':
            return 3;
        case 'خوب':
            return 4;
        case 'عالی':
            return 5;
    }
}

function changeLevelToFaLevel($level)
{
    switch($level){
        case 1:
            return 'ضعیف';
        case 2:
            return 'مبتدی';
        case 3:
            return 'متوسط';
        case 4:
            return 'خوب';
        case 5:
            return 'عالی';
    }
}

function getPersionSalaryForOnetJob($salary)
{
    switch ($salary) {
        case 'agreement' :
            return 'توافقی';
            break;
        case 'work_office':
            return 'اداره کار';
            break;
        case '4':
            return 'زیر ۴ میلیون تومان';
            break;
        case '8':
            return 'بین ۴ تا ۸ میلیون تومان';
            break;
        case '16':
            return 'بین ۸ تا ۱۶ میلیون تومان';
            break;
        case '25':
            return 'بین ۱۶ تا ۲۵ میلیون تومان';
            break;
        case '30':
            return 'بالای ۲۵ میلیون تومان';
            break;
    }
}
function getTypePackage($type)
{
    switch($type){
        case 'company':
            $type = 'شرکت';
            break;
        case 'student':
            $type = 'دانشجو';
            break;
        case 'applicant':
            $type = 'کارجو';
            break;
    }
    return $type;
}

function getPurchasedType($type)
{

    switch($type){
        case 'career':
            return ['career'];
            break;
        case 'major':
            return ['major'];
            break;
        case 'full':
            return ['major', 'career'];
            break;
        default:
            return null;
    }

}

function getTypeOfSuggestedTestToFa($type)
{

    switch($type){
        case 'career':
            return 'شغل';
            break;
        case 'major':
            return 'رشته';
            break;
        case 'full':
            return 'کامل';
            break;
    }

}

function unmatterToPersion($data)
{
    if($data == 'unmatter'){
        return 'فرفی ندارد';
    }

    return $data;
}

function valueToPercent($value, $total)
{
    if($value > $total){
        return 0;
    }
    return 100 - round(100*$value/$total);
}