@extends('admin.layouts.master')
@section('title' , $title)
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="d-flex card-header justify-content-between">
                    <h3 class="card-title">{{$title}}</h3>
                    <a href="{{ route('index.user') }}" class="btn btn-primary">صفحه اصلی</a>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            
                            <form action="{{ route('applicant.search.user') }}" method="get" class="input-group">
                                @csrf
                                <input type="hidden" name="title" value="{{ $title }}">
                                <input type="hidden" name="type" value="app">

                                <input type="text" name="name" class="form-control"
                                    @isset($oldData['name'])
                                        value="{{ $oldData['name'] }}"
                                    @endisset
                                placeholder="نام :">

                                <input type="text" name="family" class="form-control" 
                                    @isset($oldData['family'])
                                        value="{{ $oldData['family'] }}"
                                    @endisset
                                placeholder="نام خانوادگی :">

                                <input type="text" name="n_code" class="form-control"
                                    @isset($oldData['n_code'])
                                        value="{{ $oldData['n_code'] }}"
                                    @endisset
                                placeholder="کد ملی :">

                                <input type="text" name="mobile" class="form-control" 
                                    @isset($oldData['mobile'])
                                        value="{{ $oldData['mobile'] }}"
                                    @endisset
                                placeholder="تلفن همراه :">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-info form-control div-border-user-company-style">جستجو</button>
                                </div>
                            </form>

                        </div>
                        <div class="col-2 me-2">
                            <a href="{{ route('applicant.accepted.user') }}" class="btn btn-warning w-100">رفع فیلتر</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route('export.applicant.accepted.user') }}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="row w-100 mb-2">
                        <span>تعداد کل : {{$applicants->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($applicants)>0)
                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-info text-white fs-12">ردیف</th>
                                        <th class="text-center bg-info text-white">نام و نام خانوادگی</th>
                                        <th class="text-center bg-info text-white">تلفن همراه</th>
                                        <th class="text-center bg-info text-white">کد ملی</th>
                                        <th class="text-center bg-info text-white">استان | شهر</th>
                                        <th class="text-center bg-info text-white">سن</th>
                                        <th class="text-center bg-info text-white">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="co_table">
                                    @php
                                        $counter = (($applicants->currentPage() -1) * $applicants->perPage()) + 1;
                                    @endphp
                                    @foreach ($applicants as $applicant)
                                        <tr>
                                            <td class="text-nowrap text-center align-middle">{{$counter++}}</td>
                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route('candidate.single' , $applicant->id) }}" target="_blank">
                                                    <div class="col-5 col-md-9 p-md-0">{{$applicant->name}} {{$applicant->family}}</div>
                                                </a>
                                            </td>

                                            <td class="text-nowrap align-middle">{{$applicant->mobile}}</td>

                                            @if ($applicant->groupable->n_code)
                                                <td class="text-nowrap align-middle">{{$applicant->groupable->n_code}}</td>
                                            @elseif($applicant->n_code)
                                                <td class="text-nowrap align-middle">{{$applicant->n_code}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif

                                            <td class="text-nowrap align-middle">
                                                @if ($applicant->groupable->province || $applicant->groupable->city)
                                                    @if ($applicant->groupable->province)
                                                        <small class="badge bg-info">استان</small> {{$applicant->groupable->province->name}}
                                                    @endif
                                                    @if ($applicant->groupable->city)
                                                        <small class="badge bg-success">شهر</small> {{$applicant->groupable->city->name}}
                                                    @endif
                                                @else
                                                    ثبت نشده
                                                @endif
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                @if ($applicant->age)
                                                    {{$applicant->age}} 
                                                @else
                                                    --
                                                @endif
                                            </td>

                                            <td class="text-center align-middle">
                                                <div class="btn-group align-top">
                                                    <a href="javascript:void(0)" onclick="ConfirmUser({{$applicant->id}})" class="btn btn-sm btn-danger badge">رد </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit-modal-{{$applicant->id}}">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content modal-content-demo">
                                                    <div class="modal-header">
                                                        <h6 class="modal-title">ارسال تیکت</h6>
                                                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{ route('candidate.rejecte.user') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <input type="hidden" name="receiver_id" value="{{$applicant->id}}">
                                                            <div class="mb-3">
                                                                <label for="recipient-name" class="col-form-label">موضوع :</label>
                                                                <input name="subject" value="رد کاربر" type="text" class="form-control" id="recipient-name">
                                                            </div>
                                                            @error('subject') <small id="subjectErr" class="text-danger">{{$message}}</small> @enderror
                                                            <div class="mb-3">
                                                                <label for="message-text" class="col-form-label">متن :</label>
                                                                <textarea name="content" class="form-control" id="message-text"></textarea>
                                                            </div>
                                                            @error('content') <small id="contentErr" class="text-danger">{{$message}}</small> @enderror
                                                            <div class="mb-3">
                                                                <label for="attachment-file" class="col-form-label">فایل ضمیمه :</label>
                                                                <input name="attachment" type="file" class="form-control" id="attachment-file">
                                                            </div>
                                                            @error('attachment') <small id="attachmentErr" class="text-danger">{{$message}}</small> @enderror
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn ripple btn-primary" type="submit">ارسال و رد کاربر</button>
                                                            <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$applicants->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="confirmUserUrl" value="{{ route('confirm.user') }}">
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/panel/js/user/user-management.js') }}"></script>
@endsection