@extends('admin.layouts.master')

@section('title' , $title)

@section('main')
    <div class="mt-4 row row-cards">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="mt-2 text-center alert alert-success">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="mt-2 text-center alert alert-danger">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="d-flex card-header justify-content-between">
                    <h3 class="card-title">{{ $title }}</h3>
                    <a href="{{ route('index.user') }}" class="btn btn-primary">صفحه اصلی</a>
                </div>
                <div class="card-body">
                    <div class="mb-5 row w-100">
                        <div class="col-9">
                            <form action="{{ route('search.all.user') }}" method="get" class="input-group">
                                @csrf
                                <input type="hidden" name="title" value="{{ $title }}">

                                <input type="text" name="name" class="form-control"
                                    @isset($oldData['name'])
                                        value="{{ $oldData['name'] }}"
                                    @endisset
                                placeholder="نام :">

                                <input type="text" name="family" class="form-control"
                                    @isset($oldData['family'])
                                        value="{{ $oldData['family'] }}"
                                    @endisset
                                placeholder="نام خانوادگی :">

                                <input type="text" name="n_code" class="form-control"
                                    @isset($oldData['n_code'])
                                        value="{{ $oldData['n_code'] }}"
                                    @endisset
                                placeholder="کد ملی :">

                                <input type="text" name="mobile" class="form-control"
                                    @isset($oldData['mobile'])
                                        value="{{ $oldData['mobile'] }}"
                                    @endisset
                                placeholder="تلفن همراه :">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-info form-control div-border-user-company-style">جستجو</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-2 me-2">
                            <a href="{{ route('all.user') }}" class="btn btn-warning w-100">رفع فیلتر</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route('export.student.accepted.user') }}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="mb-2 row w-100">
                        <span>تعداد : {{$users->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($users)>0)
                            <table class="table mb-0 border-top table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-white bg-info fs-12">ردیف</th>
                                        <th class="text-center text-white bg-info">نام و نام خانوادگی</th>
                                        <th class="text-center text-white bg-info">تلفن همراه</th>
                                        <th class="text-center text-white bg-info">کد ملی</th>
                                        <th class="text-center text-white bg-info">نوع کاربری</th>
                                        <th class="text-center text-white bg-info">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="co_table">
                                    @php
                                        $counter = (($users->currentPage() -1) * $users->perPage()) + 1;
                                    @endphp
                                    @foreach ($users as $user)
                                        <tr>
                                            <td class="text-center align-middle text-nowrap">{{$counter++}}</td>
                                            
                                            <td class="align-middle text-nowrap">
                                                @if ($user->groupable)
                                                    @if ($user->groupable_type == "App\Models\CompanyInfo")
                                                        <a href="{{ route('company.single' , $user->id) }}" target="_blank" class="text-description">
                                                            <div class="col-5 col-md-9 p-md-0">{{$user->name}} {{$user->family}}</div>
                                                        </a>
                                                    @elseif($user->groupable_type == "App\Models\StudentInfo" || $user->groupable_type == "App\Models\ApplicantInfo")
                                                        <a href="{{ route('candidate.single' , $user->id) }}" target="_blank" class="text-description">
                                                            <div class="col-5 col-md-9 p-md-0">{{$user->name}} {{$user->family}}</div>
                                                        </a>
                                                    @endif
                                                @else
                                                    <div class="col-5 col-md-9 p-md-0">{{$user->name}} {{$user->family}}</div>
                                                @endif
                                                
                                            </td>

                                            <td class="align-middle text-nowrap">{{$user->mobile}}</td>

                                            @if ($user->n_code)
                                                <td class="align-middle text-nowrap">{{$user->n_code}}</td>
                                            @else
                                                <td class="align-middle text-nowrap">ثبت نشده</td>
                                            @endif

                                            <td class="align-middle text-nowrap">
                                                {{ user_has_role_fa($user) }}
                                            </td>

                                            <td class="text-center align-middle">
                                                <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <span class="visually-hidden">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" onclick="editToCompany({{$user->id}})" href="javascript:void(0)">تغییر به نقش شرکت</a></li>
                                                    <li><a class="dropdown-item" onclick="editToApplicant({{$user->id}})" href="javascript:void(0)">تغییر به نقش کارجو</a></li>
                                                    <li><a class="dropdown-item" onclick="editToStudent({{$user->id}})" href="javascript:void(0)">تغییر به نقش دانشجو</a></li>
                                                </ul>
                                            </td> 
                                            
                                        </tr>
                                        {{-- <div class="modal fade" id="edit-modal-{{$user->id}}">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content modal-content-demo">
                                                    <div class="modal-header">
                                                        <h6 class="modal-title">ارسال تیکت</h6>
                                                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{ route('candidate.rejecte.user') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <input type="hidden" name="receiver_id" value="{{$user->id}}">
                                                            <div class="mb-3">
                                                                <label for="recipient-name" class="col-form-label">موضوع :</label>
                                                                <input name="subject" value="رد کاربر" type="text" class="form-control" id="recipient-name">
                                                            </div>
                                                            @error('subject') <small id="subjectErr" class="text-danger">{{$message}}</small> @enderror
                                                            <div class="mb-3">
                                                                <label for="message-text" class="col-form-label">متن :</label>
                                                                <textarea name="content" class="form-control" id="message-text"></textarea>
                                                            </div>
                                                            @error('content') <small id="contentErr" class="text-danger">{{$message}}</small> @enderror
                                                            <div class="mb-3">
                                                                <label for="attachment-file" class="col-form-label">فایل ضمیمه :</label>
                                                                <input name="attachment" type="file" class="form-control" id="attachment-file">
                                                            </div>
                                                            @error('attachment') <small id="attachmentErr" class="text-danger">{{$message}}</small> @enderror
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn ripple btn-primary" type="submit">ارسال و رد کاربر</button>
                                                            <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div> --}}
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center alert alert-warning">
                                اطلاعاتی جهت نمایش وجود ندارد
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$users->links('pagination.panel')}}
            </div>
        </div>
    </div>
    <input type="hidden" id="editCompanyRoleUrl" value="{{ route('edit.company.role.user') }}">
    <input type="hidden" id="editApplicantRoleUrl" value="{{ route('edit.applicant.role.user') }}">
    <input type="hidden" id="editStudentRoleUrl" value="{{ route('edit.student.role.user') }}">
@endsection
@section('script')
    <script src="{{ asset('assets/panel/js/user/user-management.js') }}"></script>
@endsection