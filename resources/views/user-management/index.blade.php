@extends('admin.layouts.master')
@section('title' , $title)
@section('main')

    <div class="page-header">
        <h1 class="page-title"> {{ $title }} </h1>
        @if(Session::has('success'))
            <div class="row">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد اصلی</a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            
            <div class="row">

                <div class="col-12 col-md-6 col-xl-3">

                    <div class="overflow-hidden card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6> تعداد شرکت ها </h6>
                                    <h2 class="mb-0 number-font">{{ $companies }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="50px" src="{{ asset('assets/panel/images/icon/um-dashboard/company.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        <a
                        @can('manageCompany')
                            href="{{ route('company.un.user') }}"
                        @else
                            title="دسترسی ندارید"
                        @endcan
                        class="p-2 text-description text-dark w-100">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/waiting-primary.svg') }}" alt="">
                            <span class="h5 ms-3"> شرکت های در انتظار بررسی </span>
                        </a>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        <a
                        @can('manageCompany')
                            href="{{ route('company.accepted.user') }}"
                        @else
                            title="دسترسی ندارید"
                        @endcan
                         class="p-2 text-description text-dark w-100">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/accept-primary.svg') }}" alt="">
                            <span class="h5 ms-3"> شرکت های تایید شده </span>
                        </a>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        <a
                        @can('manageCompany')
                            href="{{ route('company.rejected.user') }}"
                        @else
                            title="دسترسی ندارید"
                        @endcan
                        class="p-2 text-description text-dark w-100">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/reject-primary.svg') }}" alt="">
                            <span class="h5 ms-3"> شرکت های رد شده </span>
                        </a>
                    </div>

                </div>

                <div class="col-12 col-md-6 col-xl-3">
                    <div class="overflow-hidden card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6> تعداد کارجویان </h6>
                                    <h2 class="mb-0 number-font">{{ $applicants }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/um-dashboard/hire.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        <a
                        @can('manageApplicant')
                            href="{{ route('applicant.un.user') }}"
                        @else
                            title="دسترسی ندارید"
                        @endcan
                        class="p-2 text-description text-dark w-100">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/waiting-info.svg') }}" alt="">
                            <span class="h5 ms-3"> کارجویان در انتظار بررسی </span>
                        </a>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        <a
                        @can('manageApplicant')
                            href="{{ route('applicant.accepted.user') }}"
                        @else
                            title="دسترسی ندارید"
                        @endcan
                        class="p-2 text-description text-dark w-100">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/accept-info.svg') }}" alt="">
                            <span class="h5 ms-3"> کارجویان تایید شده </span>
                        </a>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        <a
                        @can('manageApplicant')
                            href="{{ route('applicant.rejected.user') }}"
                        @else
                            title="دسترسی ندارید"
                        @endcan
                        class="p-2 text-description text-dark w-100">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/reject-info.svg') }}" alt="">
                            <span class="h5 ms-3"> کارجویان رد شده </span>
                        </a>
                    </div>

                </div>

                <div class="col-12 col-md-6 col-xl-3">
                    <div class="overflow-hidden card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class=""> تعداد دانشجویان </h6>
                                    <h2 class="mb-0 number-font">{{ $students }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/um-dashboard/intern.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        <a
                        @can('manageStudent')
                            href="{{ route('student.un.user') }}"
                        @else
                            title="دسترسی ندارید"
                        @endcan
                        class="p-2 text-description text-dark w-100">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/waiting-success.svg') }}" alt="">
                            <span class="h5 ms-3"> دانشجویان در انتظار بررسی </span>
                        </a>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        <a
                        @can('manageStudent')
                            href="{{ route('student.accepted.user') }}"
                        @else
                            title="دسترسی ندارید"
                        @endcan
                        class="p-2 text-description text-dark w-100">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/accept-success.svg') }}" alt="">
                            <span class="h5 ms-3"> دانشجویان تایید شده </span>
                        </a>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        <a
                        @can('manageStudent')
                            href="{{ route('student.rejected.user') }}"
                        @else
                            title="دسترسی ندارید"
                        @endcan
                        class="p-2 text-description text-dark w-100">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/reject-success.svg') }}" alt="">
                            <span class="h5 ms-3"> دانشجویان رد شده </span>
                        </a>
                    </div>

                </div>

                <div class="col-12 col-md-6 col-xl-3">
                    <div class="overflow-hidden card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6> کلیه کاربران </h6>
                                    <h2 class="mb-0 number-font">{{ $all }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="50px" src="{{ asset('assets/panel/images/icon/um-dashboard/user.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        @can('createUser')
                        <a href="{{ route('admin.create.user') }}" class="p-2 text-description text-dark w-100 {{Route::is('admin.create.new.user')?'active':''}}">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/waiting-green.svg') }}" alt="">
                            <span class="h5 ms-3"> افزودن کاربر </span>
                        </a>
                        @endcan
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        @can('importedUser')
                        <a href="{{route('employee.imported-list')}}" class="p-2 text-description text-dark w-100 {{Route::is('employee.imported-list')?'active':''}}">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/accept-green.svg') }}" alt="">
                            <span class="h5 ms-3">کاربران ایمپورت شده</span>
                        </a>
                        @endcan
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        @can('testedUser')
                        <a href="{{route('employee.testers-list')}}" class="p-2 text-description text-dark w-100 {{Route::is('employee.testers-list')?'active':''}}">
                            <img class="text-start ms-3" width="30px" src="{{ asset('assets/panel/images/icon/um-dashboard/reject-green.svg') }}" alt="">
                            <span class="h5 ms-3">کاربران تست داده</span>
                        </a>
                        @endcan
                    </div>

                    <div class="mb-3 card align-items-center hover">
                        @can('testedUser')
                        <a href="{{route('all.user')}}" class="p-2 text-description text-dark w-100 {{Route::is('all.user')?'active':''}}">
                            <img class="text-start ms-3" width="30px" style="height: 30px !important;" src="{{ asset('assets/panel/images/icon/um-dashboard/user.svg') }}" alt="">
                            <span class="h5 ms-3">همه کاربران</span>
                        </a>
                        @endcan
                    </div>

                </div>

            </div>

      
        </div>
    </div>
@endsection
