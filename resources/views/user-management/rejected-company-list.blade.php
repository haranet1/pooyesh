@extends('admin.layouts.master')
@section('title' , $title)
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="row card-header justify-content-between">
                    <div class="col-12 col-md-3 mb-2 mb-md-0">
                        <h3 class="card-title">{{ $title }}</h3>
                    </div>
                    <div class="col-12 col-md-3 text-end">
                        <a href="{{ route('index.user') }}" class="btn btn-primary">صفحه اصلی</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            <form action="{{ route('company.search.user') }}" method="get" class="input-group">
                                @csrf
                                <input type="hidden" name="title" value="{{ $title }}">
                                <input type="hidden" name="type" value="re_co">

                                <input type="text" name="coname" class="form-control"
                                    @isset($oldData['coname'])
                                        value="{{ $oldData['coname'] }}"
                                    @endisset
                                placeholder="نام شرکت :">

                                <input type="text" name="n_code" class="form-control"
                                    @isset($oldData['n_code'])
                                        value="{{ $oldData['n_code'] }}"
                                    @endisset
                                placeholder="کد ملی :">

                                <input type="text" name="name" class="form-control" 
                                    @isset($oldData['name'])
                                        value="{{ $oldData['name'] }}"
                                    @endisset
                                placeholder="نام نماینده :">

                                <input type="text" name="family" class="form-control" 
                                    @isset($oldData['family'])
                                        value="{{ $oldData['family'] }}"
                                    @endisset
                                placeholder="فامیل نماینده :">

                                <input type="text" name="mobile" class="form-control" 
                                    @isset($oldData['mobile'])
                                        value="{{ $oldData['mobile'] }}"
                                    @endisset
                                placeholder="تلفن همراه :">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-info form-control div-border-user-company-style">جستجو</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-2 me-2">
                            <a href="{{ route('company.rejected.user') }}" class="btn btn-warning w-100">رفع فیلتر</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route('export.company.rejected.user') }}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="row w-100 mb-2">
                        <span>تعداد کل : {{$companies->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        
                        @if (count($companies)>0)
                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-info text-white fs-12">ردیف</th>
                                        <th class="text-center bg-info text-white">نام شرکت</th>
                                        <th class="text-center bg-info text-white">شناسه ملی</th>
                                        {{-- <th class="text-center bg-info text-white">نام مدیرعامل</th> --}}
                                        <th class="text-center bg-info text-white">نماینده شرکت</th>
                                        <th class="text-center bg-info text-white">موبایل (نماینده)</th>
                                        <th class="text-center bg-info text-white">آپلود فایل</th>
                                        <th class="text-center bg-info text-white">وضعیت</th>
                                        <th class="text-center bg-info text-white">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="co_table">
                                    @php
                                        $counter = (($companies->currentPage() -1) * $companies->perPage()) + 1;
                                    @endphp
                                    @foreach ($companies as $company)
                                    @error('newsletter'. $company->id )
                                        <input type="hidden" id="newsletterErrStatus" value="true">
                                        <input type="hidden" id="newsletterErrMessage" value="{{ $message }}">
                                    @else
                                        <input type="hidden" id="newsletterErrStatus" value="false">
                                    @enderror
                                        <tr>
                                            <td class="text-center align-middle">{{$counter++}}</td>
                                            @if ($company->groupable)
                                                <td class="align-middle">
                                                    <a href="{{ route('company.single' , $company->id) }}" target="_blank" class="d-flex align-items-center text-description text-dark">
                                                        <div class="d-none d-md-inline p-0 ps-2 ">
                                                            @if($company->groupable->logo)
                                                                <img alt="image" class="avatar avatar-md br-7" src="{{asset($company->groupable->logo->path)}}">
                                                            @else
                                                                <img alt="image" class="avatar avatar-md br-7" src="{{asset('assets/front/images/company-logo-1.png')}}">
                                                            @endif
                                                        </div>
                                                        <div class="d-md-inline p-md-0 ms-0 ms-md-2">{{$company->groupable->name}}</div>
                                                    </a>
                                                </td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif

                                            @if ($company->groupable->n_code)
                                                <td class="text-nowrap align-middle">{{$company->groupable->n_code}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif

                                            {{-- @if ($company->groupable)
                                                <td class="text-nowrap align-middle">{{$company->groupable->ceo}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">مدیر عامل ثبت نشده</td>
                                            @endif --}}

                                            
                                            <td class="text-nowrap align-middle">{{$company->name}} {{$company->family}}</td>
                                            <td class="text-nowrap align-middle">{{$company->mobile}}</td>
                                            
                                            

                                            <td class="text-nowrap align-middle text-center">
                                                <form action="{{ route('co.newsletter.upload.user') }}" method="post" class="row w-100 align-items-center justify-content-center" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="col-8 text-center">
                                                        <input type="hidden"  name="company" value="{{ $company->id }}">

                                                        <input type="file" name="newsletter{{ $company->id }}" class="form-control"
                                                        @if (!is_null($company->groupable->newsletter_id))
                                                            disabled
                                                        @endif >

                                                        @error('newsletter'. $company->id )<small class="text-danger">{{ $message }}</small>@enderror
                                                    </div>
                                                    <div class="col-3 text-center">
                                                        @if (is_null($company->groupable->newsletter_id))
                                                            <button type="submit" class="btn btn-primary">ثبت</button>
                                                        @else
                                                            <button type="submit" class="btn btn-dark" disabled>ثبت شده</button>
                                                        @endif
                                                    </div>
                                                </form>
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                @if ($company->status == 0 && $company->groupable->status == 0)
                                                    <span class="badge bg-danger"> در مرحله اول توسط کارشناس رد شده است </span>
                                                @elseif ($company->status == 2 && $company->groupable->status == 1)
                                                    <span class="badge bg-danger"> در مرحله دوم توسط حراست رد شده است </span>
                                                @elseif ($company->status == 2 && $company->groupable->status == 0)
                                                    <span class="badge bg-danger"> پس از تایید، توسط کارشناس رد شده است </span>
                                                @endif
                                            </td>
                                            
                                            <td class="text-center align-middle">
                                                <div class="btn-group align-top">
                                                    <a href="javascript:void(0)" onclick="ConfirmCoInfo({{$company->id}})" class="btn btn-sm btn-success badge">تایید </a>
                                                    
                                                    {{-- <button type="button" class="btn btn-sm btn-danger badge" data-bs-toggle="modal" data-bs-target="#edit-modal-{{$company->id}}">رد</button> --}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                @lang('public.no_info')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$companies->links('pagination.panel')}}
            </div>
        </div>
    </div>
    <input type="hidden" id="coInfoConfirmUserUrl" value="{{ route('co.info.confirm.user') }}">
@endsection
@section('script')
    <script src="{{ asset('assets/panel/js/user/user-management.js') }}"></script>
@endsection