@extends($panel)

@section('title','رسید خرید')

@section('main')
    <style>
        .item-span{
            font-size: 1rem;
        }
    </style>

    <div class="page-header">
        <h1 class="page-title">داشبورد</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">رسید</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-lg-12 col-xl-6">
            <div class="input-group mb-5">
            </div>


            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            
            <div class="card">
                @if ($payment->receipt && $payment->tracking_code)
                    <div class="card-header d-flex justify-content-center">
                        <h3 class="text-success mb-0">{{$payment->status}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-8">

                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="text-center">
                                        <span>رسید خرید شما</span>
                                    </div>
                                </div>

                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">هزینه :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->price}}</span>
                                        <span class="item-span">تومان</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">تاریخ / زمان :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span" dir="ltr">{{str_replace('-', '/', $payment->payment_date_time) }}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">شماره پیگیری :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->tracking_code}}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">رسید پرداخت :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->receipt}}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">بانک صادر کننده :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->payer_bank}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                
            </div>
        </div>
    </div>
    
@endsection
@section('script')
    <script src="{{asset('assets/panel/js/company.js')}}"></script>
    

@endsection
