@extends('admin.layouts.master')
@section('title' , $title)
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="d-flex card-header justify-content-center">
                    <h3 class="card-title">{{$title}}</h3>

                </div>
                <div class="card-body">
                    <div class="row w-100 mb-2">
                        <span>تعداد کل : {{$requests->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($requests)>0)
                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-info text-white fs-12">ردیف</th>
                                        <th class="text-center bg-info text-white">{{$head1}}</th>
                                        <th class="text-center bg-info text-white">{{$head2}}</th>
                                        <th class="text-center bg-info text-white">{{$head3}}</th>
                                        <th class="text-center bg-info text-white">{{$head4}}</th>
                                        <th class="text-center bg-info text-white">{{$head5}}</th>
                                    </tr>
                                </thead>
                                <tbody id="co_table">
                                    @php
                                        $counter = (($requests->currentPage() -1) * $requests->perPage()) + 1;
                                    @endphp
                                    @foreach ($requests as $req)
                                        <tr>
                                            <td class="text-nowrap text-center align-middle">{{$counter++}}</td>
                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route('candidate.single' , $student->id) }}">
                                                    <div class="col-5 col-md-9 p-md-0">{{$student->name}} {{$student->family}}</div>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
