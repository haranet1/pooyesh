@extends('admin.layouts.master')

@section('title' , 'درخواست های استخدام')

@section('main')
    <div class="page-header">
        <h1 class="page-title">لیست درخواست های استخدام</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست درخواست های استخدام</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="page-options ms-auto">
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="row w-100 mb-2">
                        <span>تعداد کل : {{$requests->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        @if(count($requests)>0)
                        
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>ردیف</th>
                                <th>فرستنده</th>
                                <th>گیرنده</th>
                                <th>موقعیت شغلی</th>
                                <th>تاریخ</th>

                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $counter = (($requests->currentPage() -1) * $requests->perPage()) + 1;
                            @endphp
                            @foreach($requests as $request)
                            <tr>
                                <td class="text-nowrap align-middle">{{$counter++}}</td>
                                <td class="text-nowrap align-middle">
                                    @if($request->sender_type == 'co')
                                        <span class="badge bg-warning">شرکت</span>
                                        <a class="text-black" href="{{ route('company.single' , $request->sender->id) }}">
                                            {{$request->sender->groupable->name}}
                                        </a>
                                    @elseif($request->sender_type == 'std')
                                        <span class="badge bg-success">دانشجو</span>
                                        <a class="text-black" href="{{ route('candidate.single' , $request->sender->id) }}">
                                            {{$request->sender->name." ".$request->sender->family}}
                                        </a>
                                    @elseif($request->sender_type == 'app')
                                        <span class="badge bg-info">کارجو</span>
                                        <a class="text-black" href="{{ route('candidate.single' , $request->sender->id) }}">
                                            {{$request->sender->name." ".$request->sender->family}}
                                        </a>
                                    @endif
                                </td>
                                <td class="text-nowrap align-middle">
                                    @if($request->receiver_type == 'co')
                                        <span class="badge bg-warning">شرکت</span>
                                        <a class="text-black" href="{{ route('company.single' , $request->receiver->id) }}">
                                            {{$request->receiver->groupable->name}}
                                        </a>
                                    @elseif($request->receiver_type == 'std')
                                        <span class="badge bg-success">دانشجو</span>
                                        <a class="text-black" href="{{ route('candidate.single' , $request->receiver->id) }}">
                                            {{$request->receiver->name." ".$request->receiver->family}}
                                        </a>
                                        
                                    @elseif($request->sender_type == 'app')
                                        <span class="badge bg-info">کارجو</span>
                                        <a class="text-black" href="{{ route('candidate.single' , $request->receiver->id) }}">
                                            {{$request->receiver->name." ".$request->receiver->family}}
                                        </a>
                                    @endif
                                </td>
                                <td class="text-nowrap align-middle"> 
                                    @if ($request->job)
                                        <a class="text-black" href="{{ route('job', $request->job->id) }}">
                                            {{ $request->job->title }}
                                        </a>
                                    @endif
                                </td>
                                <td class="text-nowrap align-middle"> {{verta($request->created_at)->formatDate()}}</td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="#" class="btn btn-sm btn-primary badge" type="button">تایید و ارسال قرارداد </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>

@endsection