@extends('admin.layouts.master')
@section('title' , $title)
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ $title }}</h3>
                </div>
                <div class="card-body">
                    {{-- <div class="row w-100 mb-5">
                        <div class="col-9">
                            <div class="input-group">
                                <input type="text" id="co_name" class="form-control" placeholder="نام شرکت :" class="input-user-company-style">
                                <input type="text" id="name" class="form-control" placeholder="نام :">
                                <input type="text" id="family" class="form-control" placeholder="نام خانوادگی :"  class="input-user-company-style">
                                <input type="text" id="mobile" class="form-control" placeholder="تلفن همراه :"  class="input-user-company-style">
                                <div class="input-group-prepend">
                                    <div onclick="coSearch()" class="btn btn-info form-control div-border-user-company-style">جستجو</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 me-2">
                            <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedUsers()">تایید شرکت‌های انتخاب شده</button>
                        </div>
                        <div class="col-1">
                            <a href="{{route('companies.all.export')}}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div> --}}
                    <div class="row w-100 mb-2">
                        <span>تعداد کل : {{$companies->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($companies)>0)
                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-info text-white fs-12">ردیف</th>
                                        <th class="text-center bg-info text-white">نام شرکت</th>
                                        <th class="text-center bg-info text-white">شناسه ملی</th>
                                        {{-- <th class="text-center bg-info text-white">نام مدیرعامل</th> --}}
                                        <th class="text-center bg-info text-white">نماینده شرکت</th>
                                        <th class="text-center bg-info text-white">موبایل (نماینده)</th>
                                        <th class="text-center bg-info text-white">فایل روزنامه</th>
                                        <th class="text-center bg-info text-white">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="co_table">
                                    @php
                                        $counter = (($companies->currentPage() -1) * $companies->perPage()) + 1;
                                    @endphp
                                    @foreach ($companies as $company)
                                        <tr>
                                            <td class="text-nowrap text-center align-middle">{{$counter++}}</td>
                                            @if ($company->groupable)
                                                <td class="text-nowrap align-middle">
                                                    <a href="{{ route('company.single' , $company->id) }}" class="row align-items-center">
                                                        <div class="col-3 d-none d-md-block p-0 ps-2 ">
                                                            @if($company->groupable->logo)
                                                                <img alt="image" class="avatar avatar-md br-7" src="{{asset($company->groupable->logo->path)}}">
                                                            @else
                                                                <img alt="image" class="avatar avatar-md br-7" src="{{asset('assets/front/images/company-logo-1.png')}}">
                                                            @endif
                                                        </div>
                                                        <div class="col-5 col-md-9 p-md-0">{{$company->groupable->name}}</div>
                                                    </a>
                                                </td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif

                                            @if ($company->groupable->n_code)
                                                <td class="text-nowrap align-middle">{{$company->groupable->n_code}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">ثبت نشده</td>
                                            @endif

                                            {{-- @if ($company->groupable)
                                                <td class="text-nowrap align-middle">{{$company->groupable->ceo}}</td>
                                            @else
                                                <td class="text-nowrap align-middle">مدیر عامل ثبت نشده</td>
                                            @endif --}}

                                            <td class="text-nowrap align-middle">{{$company->name}} {{$company->family}}</td>
                                            <td class="text-nowrap align-middle">{{$company->mobile}}</td>

                                            <td class="text-nowrap align-middle text-center">
                                                @if ($company->groupable->newsletter)
                                                    <a href="{{ asset($company->groupable->newsletter->path) }}" target="blank">
                                                        <img src="{{ asset($company->groupable->newsletter->path) }}" class="avatar avatar-md br-7" alt="فایل روزنامه">
                                                    </a>
                                                @else
                                                    ثبت نشده است
                                                @endif
                                            </td>

                                            <td class="text-center align-middle">
                                                <div class="btn-group align-top">
                                                    <a href="javascript:void(0)" onclick="ConfirmUser({{$company->id}})" class="btn btn-sm btn-success badge">تایید </a>
                                                    <button type="button" class="btn btn-sm btn-danger badge" data-bs-toggle="modal" data-bs-target="#edit-modal-{{$company->id}}">رد</button>
                                                </div>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit-modal-{{$company->id}}">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content modal-content-demo">
                                                    <div class="modal-header">
                                                        <h6 class="modal-title">ارسال تیکت</h6>
                                                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{ route('co.reject.protection') }}" method="POST">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <input type="hidden" name="receiver_id" value="{{$company->id}}">
                                                            <div class="mb-3">
                                                                <label for="recipient-name" class="col-form-label">موضوع :</label>
                                                                <input name="subject" value="رد کاربر" type="text" class="form-control" id="recipient-name">
                                                            </div>
                                                            @error('subject') <small id="subjectErr" class="text-danger">{{$message}}</small> @enderror
                                                            <div class="mb-3">
                                                                <label for="message-text" class="col-form-label">متن :</label>
                                                                <textarea name="content" class="form-control" id="message-text"></textarea>
                                                            </div>
                                                            @error('content') <small id="contentErr" class="text-danger">{{$message}}</small> @enderror
                                                            <div class="mb-3">
                                                                <label for="attachment-file" class="col-form-label">فایل ضمیمه :</label>
                                                                <input name="attachment" type="file" class="form-control" id="attachment-file">
                                                            </div>
                                                            @error('attachment') <small id="attachmentErr" class="text-danger">{{$message}}</small> @enderror
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn ripple btn-primary" type="submit">ارسال و رد کاربر</button>
                                                            <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                @lang('public.no_info')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$companies->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="confirmUserUrl" value="{{ route('confirm.user') }}">
        {{-- <input type="hidden" id="showCompanySingleUrl" value="{{ route('company.single' , ':id' ) }}"> --}}
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/panel/js/user/user-management.js') }}"></script>
@endsection