@extends('admin.layouts.master')
@section('title' , $title)
@section('css')
    <link rel="stylesheet" href="{{asset('assets/blog/style.css')}}">
@endsection
@section('main')

<div class="row row-cards mt-4">
    <div class="col-12">
        @if ($errors->any())
            <div class="alert alert-danger mt-2 text-center">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success mt-2 text-center">
                <h5>{{Session::pull('success')}}</h5>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger mt-2 text-center">
                <h5>{{Session::pull('error')}}</h5>
            </div>
        @endif

        <div class="card">
            <div class="row card-header justify-content-between">
                <div class="col-12 col-md-3 mb-2 mb-md-0">
                    <h3 class="card-title">{{ $title }}</h3>
                </div>
            </div>

            <div class="card-body">
                <ul class="nav nav-pills mb-3" id="comments" role="tablist">
                    <li class="nav-item" role="presentation">
                      <button class="nav-link active" id="in-progress-tab" data-bs-toggle="pill" data-bs-target="#in-progress" type="button" role="tab" aria-controls="in-progress" aria-selected="true">در انتظار تایید</button>
                    </li>
                    <li class="nav-item" role="presentation">
                      <button class="nav-link" id="rejected-tab" data-bs-toggle="pill" data-bs-target="#rejected" type="button" role="tab" aria-controls="rejected" aria-selected="false">رد شده</button>
                    </li>
                    <li class="nav-item" role="presentation">
                      <button class="nav-link" id="confirmed-tab" data-bs-toggle="pill" data-bs-target="#confirmed" type="button" role="tab" aria-controls="confirmed" aria-selected="false">تایید شده</button>
                    </li>
                  </ul>
                  <div class="tab-content" id="commentsContent">
                    <div class="tab-pane fade show active" id="in-progress" role="tabpanel" aria-labelledby="in-progress-tab">

                        <div class="table-responsive table-lg">
                            @if ($inProgress->count() > 0)
        
                                <table class="table border-top table-bordered mb-0 table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center bg-info text-white">کاربر</th>
                                            <th class="text-center bg-info text-white width-fixed">متن دیدگاه</th>
                                            <th class="text-center bg-info text-white">نوشته شده در صفحه</th>
                                            <th class="text-center bg-info text-white">امکانات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($inProgress as $comment)
                                            <tr>
                                                <td class="text-nowrap align-middle">{{ $comment->user->name .' '. $comment->user->family}}</td>
        
                                                <td class="align-middle width-fixed">
                                                    <div>{{ $comment->body }}</div>
                                                </td>
        
                                                <td class="text-nowrap align-middle">
                                                    <a href="{{route('single.article.blog', $comment->commentable->slug)}}">
                                                        {{ Str::limit($comment->commentable->title,30,'...') }}
                                                    </a>
                                                </td>
        
        
                                                <td class="text-center align-middle">
                                                    <div class="btn-group align-top">
                                                        <a href="javascript:void(0)" onclick="ConfirmOrRejectComment({{$comment->id }}, 1)" class="btn btn-sm btn-success badge">تایید </a>
                                                        <a href="javascript:void(0)" type="button" onclick="ConfirmOrRejectComment({{$comment->id}}, 2)" class="btn btn-sm btn-danger badge" >رد</a>
                                                    </div>

                                                </td>
        
                                            </tr>
        
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="mb-5">
                                    {{$inProgress->links('pagination.panel')}}
                                </div>
                            @else
                                <div class="alert alert-warning text-center">
                                    {{__('public.no_info')}}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade" id="rejected" role="tabpanel" aria-labelledby="rejected-tab">
                        <div class="table-responsive table-lg">
                            @if ($rejected->count() > 0)
        
                                <table class="table border-top table-bordered mb-0 table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center bg-info text-white">کاربر</th>
                                            <th class="text-center bg-info text-white width-fixed">متن دیدگاه</th>
                                            <th class="text-center bg-info text-white">نوشته شده در صفحه</th>
                                            <th class="text-center bg-info text-white">امکانات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($rejected as $comment)
                                            <tr>
                                                <td class="text-nowrap align-middle">{{ $comment->user->name .' '. $comment->user->family}}</td>
        
                                                <td class="align-middle width-fixed">
                                                    <div>{{ $comment->body }}</div>
                                                </td>
        
                                                <td class="text-nowrap align-middle">
                                                    <a href="{{route('single.article.blog', $comment->commentable->slug)}}">
                                                        {{ Str::limit($comment->commentable->title,30,'...') }}
                                                    </a>
                                                </td>
        
        
                                                <td class="text-center align-middle">
                                                    <div class="btn-group align-top">
                                                        <a href="javascript:void(0)" onclick="ConfirmOrRejectComment({{$comment->id }}, 1)" class="btn btn-sm btn-success badge">تایید </a>
                                                    </div>
                                                </td>
        
                                            </tr>
        
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="mb-5">
                                    {{$rejected->links('pagination.panel')}}
                                </div>
        
        
                            @else
                                <div class="alert alert-warning text-center">
                                    {{__('public.no_info')}}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade" id="confirmed" role="tabpanel" aria-labelledby="confirmed-tab">
                        <div class="table-responsive table-lg">
                            @if ($confirmed->count() > 0)
        
                                <table class="table border-top table-bordered mb-0 table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center bg-info text-white">کاربر</th>
                                            <th class="text-center bg-info text-white width-fixed">متن دیدگاه</th>
                                            <th class="text-center bg-info text-white">نوشته شده در صفحه</th>
                                            <th class="text-center bg-info text-white">امکانات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($confirmed as $comment)
                                            <tr>
                                                <td class="text-nowrap align-middle">{{ $comment->user->name .' '. $comment->user->family}}</td>
        
                                                <td class="align-middle width-fixed">
                                                    <div>{{ $comment->body }}</div>
                                                </td>
        
                                                <td class="text-nowrap align-middle">
                                                    <a href="{{route('single.article.blog', $comment->commentable->slug)}}">
                                                        {{ Str::limit($comment->commentable->title,30,'...') }}
                                                    </a>
                                                </td>
        
        
                                                <td class="text-center align-middle">
                                                    <div class="btn-group align-top">
                                                        <a href="javascript:void(0)" type="button" onclick="ConfirmOrRejectComment({{$comment->id}}, 2)" class="btn btn-sm btn-danger badge" >رد</a>
                                                    </div>
                                                </td>
        
                                            </tr>
        
                                        @endforeach
                                    </tbody>
                                </table>

        
                            <div class="mb-5">
                                {{$confirmed->links('pagination.panel')}}
                            </div>
                            @else
                                <div class="alert alert-warning text-center">
                                    {{__('public.no_info')}}
                                </div>
                            @endif
                        </div>

                    </div>
                  </div>

            </div>
        </div>
    </div>

</div>
<input type="hidden" value="{{ route('reject.comment') }}" id="rejectUrl">
<input type="hidden" value="{{ route('confirm.comment') }}" id="confirmUrl">

@endsection

@section('script')
    <script>
    const rejectUrl = document.getElementById('rejectUrl').value;
    const confirmUrl = document.getElementById('confirmUrl').value;

        
    function ConfirmOrRejectComment(commentId, option) {
        var text;
        var successText;
        let commentUrl;
        if(option == 1){
            text = 'این کامنت تایید خواهد شد';
            successText = 'با موفقیت تایید شد';
            commentUrl = confirmUrl;
        }
        else if(option == 2){
            text = 'این کامنت رد خواهد شد';
            successText = 'با موفقیت رد شد';
            commentUrl = rejectUrl;
        }

        Swal.fire({
            title: 'آیا مطمئن هستید؟',
            text: text,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#198f20',
            cancelButtonColor: '#d33',
            cancelButtonText: 'انصراف',
            confirmButtonText: 'بله'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': myCsrf
                    },
                    url: commentUrl,
                    data: {
                        commentId: commentId,
                    },
                    dataType: 'json',
                    success: function (response) {
                        Swal.fire(
                            'موفق!',
                            successText,
                            'success'
                        )
                        location.reload()
                    },
                    error: function (response) {
                        Swal.fire(
                            'ناموفق!',
                            'خطایی رخ داد',
                            'error',
                        )
                    }
                });


            }
        })
    }

    </script>
@endsection
