@extends('admin.layouts.master')
@section('title' , $title)
@section('css')
    <link rel="stylesheet" href="{{asset('assets/blog/style.css')}}">
    <link href="{{asset('assets/vendor/filepond/filepond.min.css') }}" rel="stylesheet" />
    <link href="{{asset('assets/vendor/filepond/filepond-plugin-image-preview.css') }}" rel="stylesheet" />
    <link href="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css" rel="stylesheet"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="page-header">
        <h1 class="page-title"> {{ $title }} </h1>
        @if(Session::has('success'))
            <div class="row">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="row">
                <div class="alert alert-danger">
                    {{Session::pull('error')}}
                </div>
            </div>
        @endif
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">مجله</a></li>
            </ol>
        </div>
    </div>
    <div class="row row-cards justify-content-center">
        <div class="col-xl-11 col-lg-12">
            <div class="card">
                <form action="{{ route('update.article.blog')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ route('upload.article.blog') }}" id="uploadUrl">
                <div class="card-body">
                    <input type="hidden" name="article_id" value="{{ $article->id }}">
                    <div class="row mb-4 px-md-5 justify-content-center">
                        <label class="col-md-12 form-label text-center">تصویر پیش نمایش مقاله را اپلود کنید</label>
                        <div class="col-md-5">
                            <input type="file" id="image-upload" name="image" class="my-pond">
                        </div>
                        @error('image') <span class="text-danger text-center">{{$message}}</span> @enderror
                    </div>

                    <div class="row mb-4 px-md-4">
                        <div class="col-md-10">
                            <input name="title" type="text" class="form-control" value="{{ $article->title }}" id="title" placeholder="عنوان مقاله را وارد کنید...">
                            @error('title') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>

                    <div class="row mb-4 px-md-4">
                        <div class="col-md-10">
                            <select name="category" class="form-select">
                                <option  value="">دسته بندی مقاله</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->title }}" {{ $article->category->title == $category->title ? 'selected' : '' }}>{{ $category->title }}</option>
                                    @endforeach
                            </select>
                            @error('category') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>

                    <div class=" row mb-4 main-container">
                        @error('editorData') <span class="text-danger">{{$message}}</span> @enderror
                        <div class="editor-container editor-container_classic-editor editor-container_include-block-toolbar" id="editor-container">
                            <div class="editor-container__editor">
                                <textarea name="editorData" id="editor">{{ $article->content }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class=" row mb-0 justify-content-center">
                        <div class="col justify-content-center text-center">
                            <button type="submit" class="btn btn-success btn-lg px-5" id="submit">ثبت</button>
                        </div>
                    </div>
                </form>

                </div>

            </div>
        </div>

    </div>

@endsection

@section('script')
    <script type="module" src="{{ asset('assets/blog/main.js') }}"></script>
    <script src="{{ asset('assets/vendor/filepond/filepond-plugin-image-preview.js') }}"></script>
    <script src="{{ asset('assets/vendor/filepond/filepond-plugin-file-validate-type.js') }}"></script>
    <script src="{{ asset('assets/vendor/filepond/filepond-plugin-file-validate-size.js') }}"></script>
    <script src="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.js"></script>
    <script src="{{ asset('assets/vendor/filepond/filepond.js') }}"></script>

    <script type="module">
        var upload = "{{ route('storeImage.article.blog') }}";
        var revert = "{{ route('revert.article.blog') }}";
        var path = "{{ asset($article->photo->path) }}";
        let Name = "{{ $article->photo->name }}";
        let path2 = path.replace(Name , '');

        FilePond.registerPlugin(FilePondPluginImagePreview);
        FilePond.registerPlugin(FilePondPluginFilePoster);
        FilePond.registerPlugin(FilePondPluginFileValidateType);
        FilePond.registerPlugin(FilePondPluginFileValidateSize);
        // file input
        var input = document.querySelector('#image-upload');

        const pond = FilePond.create(input);

            pond.server = {
                process: upload,
                revert: revert,
                load: path2+'/' ,
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            };
            pond.setOptions({
                labelInvalidField: 'فایل نامعتبر است.',
                labelFileTypeNotAllowed: 'نوع فایل نامعتبر است',
                storeAsFile : true,
                acceptedFileTypes: ['image/*'],
                maxFileSize: '1024KB',
                labelMaxFileSizeExceeded: 'حجم فایل زیاد است'
            })
            pond.files = [
                {
                    source: Name,
                    options: {
                        type: 'local',
                    }
                }
            ];

      </script>
@endsection
       



