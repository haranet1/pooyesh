@extends('admin.layouts.master')
@section('title' , $title)
@section('css')
    <link rel="stylesheet" href="{{asset('assets/blog/style.css')}}">
@endsection
@section('main')

<div class="row row-cards mt-4">
    <div class="col-12">
        @if ($errors->any())
            <div class="alert alert-danger mt-2 text-center">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success mt-2 text-center">
                <h5>{{Session::pull('success')}}</h5>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger mt-2 text-center">
                <h5>{{Session::pull('error')}}</h5>
            </div>
        @endif

        <div class="card">
            <div class="row card-header justify-content-between">
                <div class="col-12 col-md-3 mb-2 mb-md-0">
                    <h3 class="card-title">{{ $title }}</h3>
                </div>
                <div class="col-12 col-md-3 text-end">
                    <a data-bs-toggle="modal" data-bs-target="#create-category-modal" class="btn btn-primary">اضافه کردن</a>
                </div>
            </div>

            <div class="modal fade" id="create-category-modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content modal-content-demo">
                        <div class="modal-header">
                            <h6 class="modal-title">افزودن دسته بندی جدید</h6>
                            <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form action="{{ route('store.category.mag') }}" method="POST" class="form-horizontal">
                            @csrf
                            <div class="modal-body fieldset">

                                <div class="row mb-4">
                                    <label class="col-md-4 form-label">عنوان دسته بندی</label>
                                    <div class="col-md-8">
                                        <input name="title" type="text" class="form-control" value="">
                                        @error('title') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <label class="col-md-4 form-label">نوع دسته بندی</label>
                                    <div class="col-md-8">
                                        <select name="rank" class="form-select">
                                            <option  value="">انتخاب</option>
                                            <option value="ordinary">عادی</option>
                                            <option value="special">ویژه</option>
                                        </select>
                                        @error('rank') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                            </div>     
                            <div class="modal-footer">
                                <button class="btn ripple btn-primary" type="submit">ثبت</button>
                                <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="card-body">
                <div class="table-responsive table-lg">
                    @if ($categories->count() > 0)

                        <table class="table border-top table-bordered mb-0 table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center bg-info text-white">عنوان</th>
                                    <th class="text-center bg-info text-white">تعداد مقاله</th>
                                    <th class="text-center bg-info text-white">نوع دسته بندی</th>
                                    <th class="text-center bg-info text-white">امکانات</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td class="text-nowrap align-middle">{{ $category->title }}</td>

                                        <td class="text-nowrap align-middle">
                                            <span>{{ $category->articles->count() }}</span>
                                        </td>

                                        <td class="text-nowrap align-middle">
                                            <span>
                                                @if($category->rank == 0)
                                                    عادی
                                                @elseif ($category->rank == 1)
                                                    ویژه
                                                @endif
                                            </span>
                                        </td>


                                        <td class="text-center align-middle">
                                            <div class="align-top d-flex gap-2 justify-content-center">
                                                <a class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#edit-category-modal-{{ $category->id }}">ویرایش</a>
                                                <a class="btn btn-sm btn-danger delete" onclick="deleteCategory({{ $category->id }})" href="javascript:void(0)">حذف</a>
                                                <a class="" onclick="Raising_rank_category({{ $category->id}},{{ $category->rank}})" href="javascript:void(0)">
                                                    <span class="star 
                                                        @if($category->rank == 1)
                                                            starred
                                                        @endif
                                                    ">
                                                        <i class="star-icon fa fa-star"></i>
                                                      </span>
                                                </a>



                                                <input type="hidden" value="{{ route('delete.category.mag') }}" id="deleteurl">
                                                <input type="hidden" value="{{ route('rank.category.mag') }}" id="rankurl">
                                            </div>
                                        </td>

                                    </tr>

                                    <div class="modal fade" id="edit-category-modal-{{ $category->id }}">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content modal-content-demo">
                                                <div class="modal-header">
                                                    <h6 class="modal-title">ویرایش دسته بندی</h6>
                                                    <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form action="{{ route('update.category.mag') }}" method="POST" class="form-horizontal">
                                                    @csrf
                                                    <div class="modal-body fieldset">
                    
                                                        <input type="hidden" name="category_id" value="{{ $category->id }}">
                                                        <div class=" row mb-4">
                                                            <label class="col-md-4 form-label">عنوان دسته بندی</label>
                                                            <div class="col-md-8">
                                                                <input name="title" type="text" class="form-control" value="{{ $category->title }}">
                                                                @error('title') <span class="text-danger">{{$message}}</span> @enderror
                                                            </div>
                                                        </div>
                                                        <div class=" row mb-4">
                                                            <label class="col-md-4 form-label">نوع دسته بندی</label>
                                                            <div class="col-md-8">
                                                                <select name="rank" class="form-select">
                                                                    <option  value="">انتخاب</option>
                                                                    <option value="ordinary" {{ $category->rank == 0 ? 'selected' : '' }}>عادی</option>
                                                                    <option value="special" {{ $category->rank == 1 ? 'selected' : '' }}>ویژه</option>
                                                                </select>
                                                                @error('rank') <span class="text-danger">{{$message}}</span> @enderror
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    <div class="modal-footer">
                                                        <button class="btn ripple btn-primary" type="submit">ثبت</button>
                                                        <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>



                    @else
                        <div class="alert alert-warning text-center">
                            {{__('public.no_info')}}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>


@endsection

@section('script')
    <script src="{{asset('assets/panel/js/article.js')}}"></script>
@endsection
