@extends('admin.layouts.master')
@section('title' , $title)
@section('css')
    <link rel="stylesheet" href="{{asset('assets/blog/style.css')}}">
@endsection
@section('main')

<div class="row row-cards mt-4">
    <div class="col-12">

    @if(Session::has('success'))
        <div class="alert alert-success mt-2 text-center">
            <h5>{{Session::pull('success')}}</h5>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger mt-2 text-center">
            <h5>{{Session::pull('error')}}</h5>
        </div>
    @endif

    <div class="card">
        <div class="row card-header justify-content-between">
            <div class="col-12 col-md-3 mb-2 mb-md-0">
                <h3 class="card-title">{{ $title }}</h3>
            </div>
            <div class="col-12 col-md-3 text-end">
                <a href="{{ route('create.article.blog') }}" class="btn btn-primary">مقاله جدید</a>
            </div>
        </div>
        
        <div class="row row-cards">
            <div class="col-lg-12 col-xl-12">
                @if ($articles->count() > 0)
                    <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4 mt-6 px-3">
                        @foreach ($articles as $article)
                            <div class="col-12 col-md-4  mb-3">
                                <div class="custom-card radius-10 border-0 border-3 border-info shadow-lg bg-white">
                                    <div class="card-body">
                                        <!-- article info -->
                                        <div class="d-flex align-items-center position-relative">
                                            <div class="flex-shrink-0">
                                                <img class="img-md" width="70px" height="70px" src="{{asset($article->photo->path)}}" alt="article-image" loading="lazy">
                                            </div>
                                            <div class="flex-grow-1 ms-3 text-center">
                                                <a class="fw-bold mt-2" href="{{route('single.article.blog', $article->slug ) }}">{{$article->title}}</a>

                                                <div>
                                                    <h5 class="fw-bold mt-3"> {{ $article->category->title }}</h5>
                                                </div>

                                                <div class="d-flex justify-content-between align-items-center p-sm-2 p-1 border-top border-secondery-subtle mt-1">
                                                    <div class="d-flex">
                                                        <div class="d-flex fs-7 ms-1 fw-semibold blue view-blog">
                                                            <div class=" align-self-center">
                                                                <span>بازدید</span>
                                                            </div>
                                                            <div class="ms-1 align-self-center">{{$article->views}}</div>
                                                        </div>
            
                                                    </div>
        
        
                                                    <div class="d-flex fs-7 me-2 fw-semibold blue">
                                                        <div class=" align-self-center">
                                                            <i class="fa-solid fa-calendar-days ms-1"></i>
                                                        </div>
                                                        <div class="ms-1 align-self-center">{{ verta($article->created_at)->format('%d %B %Y') }}</div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        
                                        <!-- END : article info -->
                    
                                        <!-- Options buttons -->
                                        <div class="d-flex align-items-center justify-content-center gap-2 mt-1">
                                            <div class="justify-content-center text-center">
                                                <a href="{{route('edit.article.blog',$article->id )}}" type="button" class="btn btn-success btn-sm px-5">ویرایش</a>
                                             </div>
                                            <div class="justify-content-center text-center">
                                                <a href="{{route('delete.article.blog', $article->id )}}" type="button" class="btn btn-info btn-sm px-5">حذف</a>
                                             </div>
                                            <div class="justify-content-center text-center">
                                                <a class="" onclick="Raising_rank_article({{ $article->id}},{{ $article->rank}})" href="javascript:void(0)">
                                                    <span class="star 
                                                        @if($article->rank == 1)
                                                            starred
                                                        @endif
                                                    ">
                                                        <i class="star-icon fa fa-star"></i>
                                                      </span>
                                                </a>
                                             </div>
                                             <input type="hidden" value="{{ route('rank.article.blog') }}" id="rankArticleurl">
                                        </div>
                                        <!-- END : Options buttons -->
                    
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="mb-5">
                            {{$articles->links('pagination.panel')}}
                        </div>
                    </div>

                @else
                <div class="alert alert-warning text-center">
                    مقاله ای جهت نمایش وجود ندارد. 
                </div>
                @endif

            </div>

        </div>
    </div>

</div>

</div>


@endsection

@section('script')
    <script>
        var ArticleUrl = document.getElementById('rankArticleurl').value;
    </script>
    <script src="{{asset('assets/panel/js/article.js')}}"></script>
@endsection
