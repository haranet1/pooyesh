@extends('admin.layouts.master')
@section('title' , $config['title'])

@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="d-flex card-header justify-content-between">
                    <h3 class="d-inline card-title">{{ $config['title'] }}</h3>
                    <div class="d-inline">
                        <a href="{{ asset('assets/guide/intern-contract-pattern.jpg') }}" target="_blank" class="btn btn-info">راهنما چاپ</a>
                        @if (Route::is('show.notprinted.secretariat'))
                            <a href="{{ route('show.printed.secretariat') }}" class="btn btn-primary">لیست قراردادهای چاپ شده</a>
                        @else
                            <a href="{{ route('show.notprinted.secretariat') }}" class="btn btn-primary">لیست قراردادهای قابل چاپ </a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-2">
                        <span>تعداد کل : {{$requests->total()}}</span>
                    </div>
                    <div class="table-responsive table-lg">
                        @if ($requests->count() > 0)
                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-info text-white fs-12">ردیف</th>
                                        <th class="text-center bg-info text-white">فرستنده</th>
                                        <th class="text-center bg-info text-white">گیرنده</th>
                                        <th class="text-center bg-info text-white">تاریخ</th>
                                        <th class="text-center bg-info text-white">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="contractTable">
                                    @php
                                        $counter = (($requests->currentPage() -1) * $requests->perPage()) + 1;
                                    @endphp
                                    @foreach ($requests as $request)
                                        <tr>
                                            <td class="text-nowrap text-center align-middle">{{ $counter }}</td>
                                            <td class="text-nowrap align-middle">
                                                @if ($request->sender->groupable_type == \App\Models\StudentInfo::class)
                                                    <a href="{{ route('candidate.single' , $request->sender_id) }}" class="text-description text-dark">
                                                        <div class="col-5 col-md-9 p-md-0">{{$request->sender->name}} {{$request->sender->family}}</div>
                                                    </a>
                                                @else
                                                    <a href="{{ route('company.single' , $request->sender_id) }}" class="text-description text-dark">
                                                        <div class="col-5 col-md-9 p-md-0">{{$request->sender->groupable->name}}</div>
                                                    </a>
                                                @endif
                                            </td>
                                            <td class="text-nowrap align-middle">
                                                @if ($request->receiver->groupable_type == \App\Models\StudentInfo::class)
                                                    <a href="{{ route('candidate.single' , $request->receiver_id) }}" class="text-description text-dark">
                                                        <div class="col-5 col-md-9 p-md-0">{{$request->receiver->name}} {{$request->receiver->family}}</div>
                                                    </a>
                                                @else
                                                    <a href="{{ route('company.single' , $request->receiver_id) }}" class="text-description text-dark">
                                                        <div class="col-5 col-md-9 p-md-0">{{$request->receiver->groupable->name}}</div>
                                                    </a>
                                                @endif
                                            </td>
                                            <td class="text-nowrap align-middle"> {{verta($request->created_at)->formatDate()}}</td>
                                            <td class="text-center align-middle">
                                                <a href="{{ route('show.intern.contract' , $request->id) }}" target="_blank"
                                                    class="btn btn-primary text-white" type="button">دانلود قرارداد</a>
                                                @if (Route::is('show.notprinted.secretariat'))
                                                    <a href="javascript:void(0)" class="btn btn-success" onclick="printContract({{$request->id}})">چاپ</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center mt-3 py-2 ">
                                <h5>{{__('public.no_info')}}</h5>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
             </div>
        </div>
        <input type="hidden" id="printContractUrl" value="{{ route('print.secertariat') }}">
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/pooyesh/js/secretariat.js') }}"></script>    
@endsection