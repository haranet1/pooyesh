@extends('admin.layouts.master')
@section('title' , $title)
@section('main')

    <div class="page-header">
        <h1 class="page-title"> {{ $title }} </h1>
        @if(Session::has('success'))
            <div class="row">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد اصلی</a></li>
            </ol>
        </div>
    </div>

    <div class="card-body">

        <form class="form-horizontal" action="{{route('update.setting.user')}}" method="POST">
            @csrf
            <h4 class="mb-4">محدودیت شرکت ها</h4>
            <div class="row mb-4">
                <label class="form-label col-md-3">تعداد موقعیت کارآموزی مجاز شرکت ها</label>
                <div class="col-md-6">
                    <input name="create_internship_limit" type="number" class="form-control" value="{{ $create_internship_limit }}" placeholder="عدد">
                </div>
            </div>
            <div class="row mb-4">
                <label class="form-label col-md-3">تعداد درخواست کارآموزی مجاز شرکت ها</label>
                <div class="col-md-6">
                    <input name="request_internship_limit" type="number" class="form-control" value="{{ $request_internship_limit }}" placeholder="عدد">
                </div>
            </div>
            <div class="row mb-4">
                <label class="form-label col-md-3">تعداد قبول درخواست کارآموزی مجاز شرکت ها</label>
                <div class="col-md-6">
                    <input name="confirm_internship_limit" type="number" class="form-control" value="{{ $confirm_internship_limit }}" placeholder="عدد">
                </div>
            </div>
            <hr>
            <div class="row mb-4">
                <label class="form-label col-md-3">تعداد موقعیت استخدامی مجاز شرکت ها</label>
                <div class="col-md-6">
                    <input name="create_hire_limit" type="number" class="form-control" value="{{ $create_hire_limit }}" placeholder="عدد">
                </div>
            </div>
            <div class="row mb-4">
                <label class="form-label col-md-3">تعداد درخواست استخدامی مجاز شرکت ها</label>
                <div class="col-md-6">
                    <input name="request_hire_limit" type="number" class="form-control" value="{{ $request_hire_limit }}" placeholder="عدد">
                </div>
            </div>
            <div class="row mb-4">
                <label class="form-label col-md-3">تعداد قبول درخواست استخدامی مجاز شرکت ها</label>
                <div class="col-md-6">
                    <input name="confirm_hire_limit" type="number" class="form-control" value="{{ $confirm_hire_limit }}" placeholder="عدد">
                </div>
            </div>


            <h4 class="mb-4 mt-8">قیمت تست ها mbti</h4>
            <div class="row mb-4">
                <label class="form-label col-md-3">قیمت مشاهده مشاغل متناسب با تیپ شخصیتی</label>
                <div class="col-md-6">
                    <input name="career_matches_price" type="text" class="form-control" value="{{ $career_matches_price }}" placeholder="تومان">
                </div>
            </div>
            <div class="row mb-4">
                <label class="form-label col-md-3">قیمت مشاهده رشته های تحصیلی متناسب با تیپ شخصیتی</label>
                <div class="col-md-6">
                    <input name="major_matches_price" type="text" class="form-control" value="{{ $major_matches_price }}" placeholder="تومان">
                </div>
            </div>

            <div class=" row mb-0 justify-content-center ">
                <div class="col justify-content-center text-center ">
                   <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                </div>
            </div>
        </form>

    </div>


@endsection
