<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="فن‌یار">
    <meta name="author" content="haranet.ir">
    <meta name="_token" content="{{csrf_token()}}">

    <link href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}" rel="shortcut icon"/>

    <link id="style"  href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.rtl.min.css')}}" rel="stylesheet"/>

    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/panel/css/dark-style.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/panel/css/transparent-style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/panel/css/skin-modes.css')}}" rel="stylesheet"/>

    <link href="{{asset('assets/panel/css/icons.css')}}" rel="stylesheet"/>

    <link  rel="stylesheet" type="text/css" media="all" href="{{asset('assets/panel/colors/color1.css')}}"/>

    <link rel="stylesheet"  href="{{asset('assets/panel/fonts/styles-fa-num/iran-yekan.css')}}">
    <link href="{{asset('assets/panel/css/rtl.css')}}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('assets/datepicker/dist/css/persian-datepicker.css')}}"/>

    <link rel="stylesheet" href="{{asset('assets/vendor/ckeditor5/ckeditor5.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/ckeditor5/ckeditor5-content.css')}}">

    <title>{{ env('APP_NAME') }} | @yield('title')</title>

    @yield('css')
</head>
<body class="app sidebar-mini rtl light-mode">
    
    <div id="global-loader">
        <img src="{{asset('assets/panel/images/loader.svg')}}" class="loader-img" alt="Loader">
    </div>

    <div class="page">
        <div class="page-main">

            @include('admin.layouts.header')

            <div class="sticky">
                <div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
                <div class="app-sidebar">
                    <div class="side-header">
                        <a class="header-brand1" href="#">
                            <img src="{{asset('assets/panel/images/brand/jobist/white-text-logo.svg')}}" style="width: 140px" class="header-brand-img desktop-logo" alt="logo">
                            <img src="{{asset('assets/panel/images/brand/jobist/white-notext-logo.svg')}}" style="width: 140px" class="header-brand-img toggle-logo" alt="logo">
                            <img src="{{asset('assets/panel/images/brand/jobist/logo.svg')}}" style="width: 140px" class="header-brand-img light-logo" alt="logo">
                            <img src="{{asset('assets/panel/images/brand/jobist/text-logo.svg')}}" style="width: 140px" class="header-brand-img light-logo1" alt="logo">
                        </a>
                    </div>
                    <div class="main-sidemenu">
                        <div class="slide-left disabled" id="slide-left">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24"
                                 viewBox="0 0 24 24">
                                <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z"/>
                            </svg>
                        </div>
                        @include('admin.layouts.sidebar')
                        <div class="slide-right" id="slide-right">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24"
                                 viewBox="0 0 24 24">
                                <path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-content app-content mt-0">
                <div class="side-app">
    
                    <div class="main-container container-fluid">
    
                        @yield('main')
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-md-12 col-sm-12 text-center">
                        <a href="https://www.haranet.ir">حرانت ©</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>


    <script src="{{asset('assets/panel/js/jquery.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{asset('assets/panel/js/jquery.sparkline.min.js')}}"></script>

<script src="{{asset('assets/panel/js/sticky.js')}}"></script>

<script src="{{asset('assets/panel/js/circle-progress.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/peitychart/jquery.peity.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/peitychart/peitychart.init.js')}}"></script>

<script src="{{asset('assets/panel/plugins/sidebar/sidebar.js')}}"></script>

<script src="{{asset('assets/panel/plugins/p-scroll/perfect-scrollbar.js')}}"></script>
<script src="{{asset('assets/panel/plugins/p-scroll/pscroll.js')}}"></script>
<script src="{{asset('assets/panel/plugins/p-scroll/pscroll-1.js')}}"></script>

<script src="{{asset('assets/panel/plugins/chart/Chart.bundle.js')}}"></script>
<script src="{{asset('assets/panel/plugins/chart/rounded-barchart.js')}}"></script>
<script src="{{asset('assets/panel/plugins/chart/utils.js')}}"></script>

<script src="{{asset('assets/panel/plugins/select2/select2.full.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
<script src="{{asset('assets/panel/plugins/datatable/dataTables.responsive.min.js')}}"></script>

<script src="{{asset('assets/panel/js/apexcharts.js')}}"></script>
<script src="{{asset('assets/panel/plugins/apexchart/irregular-data-series.js')}}"></script>

<script src="{{asset('assets/panel/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{asset('assets/panel/plugins/flot/jquery.flot.fillbetween.js')}}"></script>
<script src="{{asset('assets/panel/plugins/flot/chart.flot.sampledata.js')}}"></script>
<script src="{{asset('assets/panel/plugins/flot/dashboard.sampledata.js')}}"></script>

<script src="{{asset('assets/panel/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

<script src="{{asset('assets/panel/plugins/sidemenu/sidemenu.js')}}"></script>

<script src="{{asset('assets/panel/plugins/bootstrap5-typehead/autocomplete.js')}}"></script>
<script src="{{asset('assets/panel/js/typehead.js')}}"></script>

<script src="{{asset('assets/panel/js/index1.js')}}"></script>

<script src="{{asset('assets/panel/js/themeColors.js')}}"></script>

<script src="{{asset('assets/panel/js/custom.js')}}"></script>
<script src="{{asset('assets/panel/js/custom1.js')}}"></script>

<script src="{{asset('assets/swal2/swal2.all.min.js')}}"></script>
<script src="{{asset('assets/datepicker/dist/js/persian-datepicker.js')}}"></script>
<script src="{{asset('assets/datepicker/assets/persian-date.min.js')}}"></script>
<script src="{{asset('assets/swal2/swal2.all.min.js')}}"></script>
{{-- ck editor --}}
<script type="importmap">
    {
        "imports": {
            "ckeditor5": "{{ asset('assets/vendor/ckeditor5/ckeditor5.js') }}",
            "fa": "{{ asset('assets/vendor/ckeditor5/translations/fa.js') }}",
            "ckeditor5/": "{{ asset('assets/vendor/ckeditor5').'/' }}"
        }
    }
</script>
{{-- <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('assets/ckeditor/adapters/jquery.js')}}"></script> --}}

<script>
    const imageUrl = "{{asset('assets/front/images/company-logo-1.png')}}";
    var users =[];
    var config = {
        routes: {
            ajax:"{{route('employee.ajax')}}",
            delete_sadra:"{{route('delete.sadra')}}",
            delete_discount:"{{route('employee.discount.destroy')}}",
            search_ajax:"{{route('employee.ajax.search')}}",
            assign_to_company: "{{route('employee.ajax.assign-to-company')}}",
            assign_to_student: "{{route('employee.ajax.assign-to-student')}}",
            assign_to_applicant: "{{route('employee.ajax.assign-to-applicant')}}",
            un_co_search: "{{route('employee.ajax.unconfirmed-company-search')}}",
            un_app_search: "{{route('employee.ajax.unconfirmed-applicant-search')}}",
            un_std_search: "{{route('employee.ajax.unconfirmed-student-search')}}",
            co_search: "{{route('employee.ajax.confirmed-company-search')}}",
            app_search: "{{route('employee.ajax.confirmed-applicant-search')}}",
            std_search: "{{route('employee.ajax.confirmed-student-search')}}",
            im_co_search: "{{route('employee.ajax.imported-company-search')}}",
            im_app_search: "{{route('employee.ajax.imported-applicant-search')}}",
            im_std_search: "{{route('employee.ajax.imported-student-search')}}",
            test_mbti_search: "{{route('employee.ajax.tester-mbti-search')}}",
            test_enneagram_search: "{{route('employee.ajax.tester-enneagram-search')}}",
            delete_package:"{{route('delete.package.user')}}",
            
        },
    };
</script>
<script src="{{asset('assets/panel/js/employee.js')}}"></script>
<script src="{{asset('assets/panel/js/purchase-packages.js')}}"></script>
<script>
    var currentRoute = "{{ Route::currentRouteName() }}";
    var publicMenuRoutes = ['employee.unconfirmed-job-positions','files.index','emp.announce.list','emp.announce.create','emp.signature.create','emp.graders.list','employee.create.user', 'employee.unconfirmed-stds', 'employee.unconfirmed-companies', 'employee.co.list', 'employee.std.list', 'employee.pooyesh-reqs', 'employee.hire-reqs'];
    var sadraMenuRoutes = ['employee.sadra.edit', 'employee.sadra.create','index.sadra','employee.discount.index','employee.discount.create','employee.sadra.report','employee.buy-plan.index','employee.buy-plan.create','employee.buy-plan.edit'];
    var suppMenuRoutes = ['employee.tickets.list'];
    var otherMenuRoutes = ['employee.co-log-activity.list','employee.std-log-activity.list','employee.emp-log-activity.list'];

    if (publicMenuRoutes.includes(currentRoute)) {
        $('.slide.public').addClass('is-expanded');
    }
    if (sadraMenuRoutes.includes(currentRoute)) {
        $('.slide.sadra').addClass('is-expanded');
    }
    if (suppMenuRoutes.includes(currentRoute)) {
        $('.slide.supp').addClass('is-expanded');
    }
    if (otherMenuRoutes.includes(currentRoute)) {
        $('.slide.other').addClass('is-expanded');
    }
</script>
<script>
    function MarkAsRead(clicked_element,notification){

        let item = clicked_element.parentNode.parentNode
        $.ajax({
            headers:{
                'X-CSRF-TOKEN':'{{csrf_token()}}'
            },
            url: '{{ route("notifications.ajax") }}',
            type: 'POST',
            data: {
                do:'mark-as-read',
                notification:notification
            },
            success: function(response) {
                item.remove()
            },
            error: function(xhr, status, error) {
                // Handle any errors that occur during the request
            }
        });
    }
</script>

@yield('script')

</body>
</html>