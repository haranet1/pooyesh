<div class="app-header header sticky">
    <div class="container-fluid main-container">
        <div class="d-flex">
            <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-bs-toggle="sidebar"
               href="javascript:void(0)"></a>

            <a class="logo-horizontal " href="#">
                <img src="{{asset('assets/panel/images/brand/jobist/white-text-logo.svg')}}" style="width: 120px" class="header-brand-img desktop-logo" alt="logo">
                <img src="{{asset('assets/panel/images/brand/jobist/text-logo.svg')}}" style="width: 120px" class="header-brand-img light-logo1" alt="logo">
            </a>

            <div class="main-header-center ms-3 d-none d-lg-block">

            </div>
            <div class="d-flex order-lg-2 ms-auto header-right-icons">
                <div class="dropdown d-none">
                    <a href="javascript:void(0)" class="nav-link icon" data-bs-toggle="dropdown">
                        <i class="fe fe-search"></i>
                    </a>
                    <div class="dropdown-menu header-search dropdown-menu-start">
                        <div class="input-group w-100 p-2">

                        </div>
                    </div>
                </div>

                <button class="navbar-toggler navresponsive-toggler d-lg-none ms-auto" type="button"
                        data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent-4"
                        aria-controls="navbarSupportedContent-4" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon fe fe-more-vertical"></span>
                </button>
                <div class="navbar navbar-collapse responsive-navbar p-0">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                        <div class="d-flex order-lg-2">

                            <div class="d-flex country">
                                <a class="nav-link icon theme-layout nav-link-bg layout-setting">
                                    <span class="dark-layout"><i class="fe fe-moon"></i></span>
                                    <span class="light-layout"><i class="fe fe-sun"></i></span>
                                </a>
                            </div>


                            <div class="dropdown d-flex">
                                <a class="nav-link icon full-screen-link nav-link-bg">
                                    <i class="fe fe-minimize fullscreen-button"></i>
                                </a>
                            </div>


                            <div class="dropdown  d-flex notifications">
                                <a class="nav-link icon" data-bs-toggle="dropdown">
                                    <i class="fe fe-bell"></i>
                                    @if(\Illuminate\Support\Facades\Auth::user()->unreadNotifications()->count() > 0 )
                                        @php($hasNotify = true)
                                        <span class=" pulse"></span>
                                    @endif
                                </a>
                                <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                    <div class="drop-heading border-bottom">
                                        <div class="d-flex">
                                            <h6 class="mt-1 mb-0 fs-16 fw-semibold text-dark">اعلانات
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="notifications-menu" style="overflow-y: scroll">
                                        @if(isset($hasNotify))
                                            @foreach(\Illuminate\Support\Facades\Auth::user()->unreadNotifications as $notification)

                                                @isset($notification->data['link'])
                                                    <a href="{{ $notification->data['link'] }}" class="p-0">
                                                        <div class="dropdown-item d-flex" >
                                                            <div class="me-3 notifyimg  bg-primary brround box-shadow-primary">
                                                                <i class="fe fe-mail"></i>
                                                            </div>
                                                            <div class="mt-1 wd-80p">

                                                                <h5 class="notification-label mb-1">
                                                                    {{$notification->data['message']}}
                                                                </h5>

                                                                <span class="notification-subtext">{{verta($notification->created_at)->formatDifference()}}</span>
                                                                <a href="javascript:void(0)" onclick="MarkAsRead(this,'{{$notification->id}}')">
                                                                    <span class="btn btn-outline-success notification-subtext p-1">متوجه شدم</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @else
                                                    <div class="dropdown-item d-flex" >
                                                        <div class="me-3 notifyimg  bg-primary brround box-shadow-primary">
                                                            <i class="fe fe-mail"></i>
                                                        </div>
                                                        <div class="mt-1 wd-80p">

                                                            <h5 class="notification-label mb-1">
                                                                {{$notification->data['message']}}
                                                            </h5>

                                                            <span class="notification-subtext">{{verta($notification->created_at)->formatDifference()}}</span>
                                                            <a href="javascript:void(0)" onclick="MarkAsRead(this,'{{$notification->id}}')">
                                                                <span class="btn btn-outline-success notification-subtext p-1">متوجه شدم</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endisset
                                            @endforeach
                                        @else
                                            <h5 class="notification-label text-danger m-4">
                                                اعلان جدیدی وجود ندارد
                                            </h5>
                                        @endif

                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    {{-- <a href="notify-list.html" class="dropdown-item text-center p-3 text-muted">مشاهده همه
                                         اعلان</a>--}}
                                </div>
                            </div>

                            <div class="dropdown  d-flex message">
                                <a class="nav-link icon text-center" data-bs-toggle="dropdown">
                                    <i class="fe fe-message-square"></i>
                                    @if(\Illuminate\Support\Facades\Auth::user()->getUnreadTickets()->count() > 0)
                                        <span class="pulse-danger"></span>
                                    @endif
                                </a>
                                <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                    <div class="drop-heading border-bottom">
                                        <div class="d-flex">
                                            <h6 class="mt-1 mb-0 fs-16 fw-semibold text-dark">شما {{\Illuminate\Support\Facades\Auth::user()->getUnreadTickets()->count()}} پیام خوانده نشده دارید</h6>
                                        </div>
                                    </div>
                                    <div class="message-menu message-menu-scroll" style="overflow-y: scroll">
                                        @if(\Illuminate\Support\Facades\Auth::user()->getUnreadTickets()->count() > 0)
                                            @foreach(\Illuminate\Support\Facades\Auth::user()->getUnreadTickets() as $ticket)
                                                <a class="dropdown-item d-flex" href="{{route('index.ticket' , $ticket->id)}}">
                                                    <div class="wd-90p">
                                                        <div class="d-flex">
                                                            <h5 class="mb-1">{{$ticket->sender->name." ".$ticket->sender->family}}</h5>
                                                            <small class="text-muted ms-auto text-end">
                                                                {{verta($ticket->created_at)->formatDifference()}}  
                                                            </small>
                                                        </div>
                                                        <span>{{$ticket->subject}}</span>
                                                    </div>
                                                </a>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <a href="{{route('inbox.ticket')}}" class="dropdown-item text-center p-3 text-muted">نمایش همه پیام ها</a>
                                </div>
                            </div>
                            {{-- <div class="demo-icon nav-link icon">
                                <i class="fe fe-settings fa-spin  text_primary"></i>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="dropdown d-flex profile-1">
                    <a href="javascript:void(0)" data-bs-toggle="dropdown"
                       class="nav-link leading-none d-flex">
                        <img src="{{asset('assets/panel/images/users/admin-user.svg')}}" alt="profile-user"
                             class="avatar  profile-user brround cover-image">
                    </a>
                    <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow transform-dropdown">
                        <div class="drop-heading">
                            <div class="text-center">
                                <h5 class="text-dark mb-0 fs-14 fw-semibold">{{\Illuminate\Support\Facades\Auth::user()->name." ".\Illuminate\Support\Facades\Auth::user()->family}}</h5>
                                <small class="text-muted"> </small>
                            </div>
                        </div>
                        <div class="dropdown-divider m-0"></div>

                        <a class="dropdown-item" href="{{route('employee.profile.edit')}}">
                            <i class="dropdown-icon fe fe-lock"></i>ویرایش پروفایل
                        </a>
                        <a onclick="document.getElementById('logout').submit()"
                           class="dropdown-item" href="javascript:void(0)">
                            <i class="dropdown-icon fe fe-lock"></i> خروج
                        </a>
                        <form id="logout" action="{{route('logout')}}" method="POST">
                            @csrf
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
