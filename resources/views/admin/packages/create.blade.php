@extends('admin.layouts.master')
@section('title' , $title)
@section('main')

    <div class="page-header">
        <h1 class="page-title"> {{ $title }} </h1>
        @if(Session::has('success'))
            <div class="row">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">مدیریت کاربران</a></li>
            </ol>
        </div>
    </div>
    <div class="row row-cards justify-content-center">
        <div class="col-xl-8 col-lg-12">
            <div class="card">

                <div class="card-body">
                    <form class="form-horizontal" action="{{route('store.package.user')}}" method="post">
                        @csrf
                        <div class=" row mb-4">
                            <label class="col-md-4 form-label">عنوان بسته</label>
                            <div class="col-md-8">
                                <input name="name" type="text" class="form-control" >
                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <h4 class="card-title text-primary text-center">تعداد کارآموزی</h4>
                        <div class="row mb-4">
                            <label class="col-md-4 form-label" for="">ایجاد موقعیت کارآموزی</label>
                            <div class="col-md-8">
                                <input name="internship_positions" type="number" class="form-control" value="" placeholder="عدد">
                                @error('internship_positions') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-4 form-label" for="">درخواست کارآموزی به دانشجو</label>
                            <div class="col-md-8">
                                <input name="internship_requests" type="number" class="form-control" value="" placeholder="عدد">
                                @error('internship_requests') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-4 form-label" for="">تایید درخواست کارآموزی دانشجو</label>
                            <div class="col-md-8">
                                <input name="internship_confirms" type="number" class="form-control" value="" placeholder="عدد">
                                @error('internship_confirms') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <h4 class="card-title text-primary text-center">تعداد استخدامی</h4>
                        <div class="row mb-4">
                            <label class="col-md-4 form-label" for="">ایجاد موقعیت استخدامی</label>
                            <div class="col-md-8">
                                <input name="hire_positions" type="number" class="form-control" value="" placeholder="عدد">
                                @error('hire_positions') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-4 form-label" for="">درخواست استخدامی به کارجو</label>
                            <div class="col-md-8">
                                <input name="hire_requests" type="number" class="form-control" value="" placeholder="عدد">
                                @error('hire_requests') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-4 form-label" for="">تایید درخواست استخدامی</label>
                            <div class="col-md-8">
                                <input name="hire_confirms" type="number" class="form-control" value="" placeholder="عدد">
                                @error('hire_confirms') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-4 form-label" for="">نوع کاربر</label>
                            <div class="col-md-8">
                                <select name="type" class="form-select">
                                    <option value="">انتخاب نوع کاربر</option>
                                    <option selected value="company">شرکت</option>
                                    <option value="student">دانشجو</option>
                                    <option value="applicant">کارجو</option>
                                </select>
                                @error('type') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-4 form-label" for="">قیمت</label>
                            <div class="col-md-8">
                                <input name="price" type="text" class="form-control" value="" placeholder="تومان">
                                @error('price') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>


                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>


@endsection
