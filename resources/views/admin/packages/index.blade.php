@extends('admin.layouts.master')
@section('title' , $title)
@section('main')

<div class="row row-cards mt-4">
    <div class="col-12">

    @if(Session::has('success'))
        <div class="alert alert-success mt-2 text-center">
            <h5>{{Session::pull('success')}}</h5>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger mt-2 text-center">
            <h5>{{Session::pull('error')}}</h5>
        </div>
    @endif

    <div class="card">
        <div class="row card-header justify-content-between">
            <div class="col-12 col-md-3 mb-2 mb-md-0">
                <h3 class="card-title">{{ $title }}</h3>
            </div>
            <div class="col-12 col-md-3 text-end">
                <a href="{{ route('create.package.user') }}" class="btn btn-primary">افزودن بسته</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive table-lg">
                @if ($packages->count() > 0)

                    <table class="table border-top table-bordered mb-0 table-striped">
                        <thead>
                            <tr>
                                <th class="text-center bg-info text-white align-middle">عنوان</th>
                                <th class="text-center bg-info text-white align-middle">تعداد موقعیت های کارآموزی</th>
                                <th class="text-center bg-info text-white align-middle">تعداد درخواست های کاراموزی</th>
                                <th class="text-center bg-info text-white align-middle">تعداد تایید درخواست های کارآموزی</th>
                                <th class="text-center bg-info text-white align-middle">تعداد موقعیت های استخدامی</th>
                                <th class="text-center bg-info text-white align-middle">تعداد درخواست های استخدامی</th>
                                <th class="text-center bg-info text-white align-middle">تعداد تایید درخواست های استخدامی</th>
                                <th class="text-center bg-info text-white align-middle">نوع کاربر</th>
                                <th class="text-center bg-info text-white align-middle">قیمت</th>
                                <th class="text-center bg-info text-white align-middle">امکانات</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($packages as $package)
                                <tr>
                                    <td class="text-nowrap align-middle">{{ Str::limit($package->name,'20','...') }}</td>

                                    <td class="text-nowrap align-middle">
                                        <span>{{ $package->additional_internship_positions }}</span>
                                    </td>

                                    <td class="text-nowrap align-middle" >
                                        <span>{{ $package->additional_internship_requests }}</span>
                                    </td>

                                    <td class="text-nowrap align-middle">
                                         <span>{{ $package->additional_internship_confirms }}</span>
                                    </td>

                                    <td class="text-nowrap align-middle">
                                        <span>{{ $package->additional_hire_positions }}</span>
                                    </td>

                                    <td class="text-nowrap align-middle" >
                                        <span>{{ $package->additional_hire_requests }}</span>
                                    </td>

                                    <td class="text-nowrap align-middle">
                                         <span>{{ $package->additional_hire_confirms }}</span>
                                    </td>

                                    <td class="align-middle text-center">
                                        <span>{{ getTypePackage($package->type) }}</span>

                                    </td>

                                    <td class="align-middle text-center">
                                        <span>{{ number_format($package->price) }}</span>
                                    </td>

                                    <td class="text-center align-middle">
                                        <div class="btn-group align-top">
                                            <a class="btn btn-sm btn-primary" href="{{route('edit.package.user', $package->id)}}">ویرایش</a>
                                            <a class="btn btn-sm btn-danger delete" onclick="deletePackage({{ $package->id }})" href="javascript:void(0)">حذف</a>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                @else
                    <div class="alert alert-warning text-center">
                        {{__('public.no_info')}}
                    </div>
                @endif
            </div>
        </div>
    </div>
    {{-- <div class="mb-5">
        {{$package->links('pagination.panel')}}
    </div> --}}
</div>

</div>


@endsection
