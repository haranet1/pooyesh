@extends('panel.company.master')

@section('title' , 'لیست رویدادها')

@section('main')

    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست رویدادها</a></li>
            </ol>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
            <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
            <div class="flex-grow-1">
                <p class="f-16" >
                    <strong>در این صفحه لیست نمایشگاه های کار نمایش داده میشود.</strong>
                </p>
            </div>
        </div>
    </div>

    @if(Session::has('error'))
        <div class="alert alert-danger  text-center">
            {{Session::pull('error')}}
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success  text-center">
            {{Session::pull('success')}}
        </div>
    @endif

    <div class="row row-cards">
        <div class="col-12">

            @if ($events->count() > 0)
                <div class="row">
                    @foreach ($events as $event)
                        @php
                            $eventExist = false;
                        @endphp
                        @foreach ($company_sadra as $sadra)
                            @if ($event->id == $sadra->sadra_id && ($sadra->status == 1 || is_null($sadra->status)))
                                @php
                                    $eventExist = true;
                                @endphp
                                @break
                            @endif
                        @endforeach

                        <div class="col-4 mb-5">
                            <div class="card h-100">
                                @if($event->photo)
                                    <a target="_blank" href="{{url($event->photo->path)}}">
                                        <img src="{{ asset($event->photo->path) }}" class="card-img-top" alt="Skyscrapers"/>
                                    </a>

                                @else
                                    <div class="p-5">
                                        <img src="{{ asset('assets/images/default-image.svg') }}" class="card-img-top p-5" alt="Skyscrapers"/>
                                    </div>
                                @endif

                                <div class="card-body">
                                  <h5 class="card-title">{{ $event->title }}</h5>
                                  <p class="card-text" dir="ltr">از
                                        تاریخ: {{verta($event->start_at)->format('Y/m/d')}}
                                        تا {{verta($event->end_at)->format('Y/m/d')}}</p>


                                    @if($event->start_register_at)
                                    <p class="card-text" dir="ltr">شروع ثبت نام از
                                        تاریخ: {{verta($event->start_register_at)->format('Y/m/d')}}
                                        تا {{verta($event->end_register_at)->format('Y/m/d')}}</p>
                                    @endif

                                    <p class="card-text">{{ Str::limit($event->description , '170' , '...') }}</p>
                                </div>
                                <div class="card-footer text-center">
                                    @if ($eventExist == true)
                                        <span class="btn btn-dark disabled ">ثبت نام شده</span>
                                    @elseif (verta(now())->lessThan(verta($event->end_register_at)))
                                        @if($event->map)
                                            <div class="d-flex justify-content-between">
                                                <a class="btn btn-info" target="_blank" href="{{url($event->map->path)}}"> نقشه رویداد</a>
                                                <a class="btn btn-success" href="{{ route('show.register.sadra' , $event->id) }}"> ثبت نام </a>
                                            </div>
                                        @else
                                            <a class="btn btn-success " href="{{ route('show.register.sadra' , $event->id) }}"> ثبت نام </a>
                                        @endif
                                    @else
                                        <a class="btn btn-warning disabled "href="javascript:void(0)">
                                            اتمام مهلت ثبت نام
                                        </a>
                                    @endif
                                </div>
                              </div>
                        </div>

                    @endforeach
                </div>
            @else
                <div class="alert alert-warning text-center">
                    {{__('public.no_info')}}
                </div>
            @endif

            <div class="mb-5">
                {{$events->links('pagination.panel')}}
            </div>

        </div>
    </div>

@endsection
