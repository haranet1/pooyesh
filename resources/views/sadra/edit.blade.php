@extends('admin.layouts.master')

@section('title' , 'ویرایش رویداد صدرا')

@section('main')
    
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ویرایش رویداد </a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ویرایش رویداد: {{$sadra->title}}</h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{ route('update.sadra') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="sadra_id" value="{{ $sadra->id }}">
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">عنوان</label>
                            <div class="col-md-9">
                                <input name="title" value="{{$sadra->title}}" type="text" class="form-control" >
                                @error('title') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تصویر</label>
                            <div class="col-md-9">
                                <input name="photo" type="file" class="form-control" >
                                @error('photo') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">نقشه غرفه ها</label>
                            <div class="col-md-9">
                                <input name="map" type="file" class="form-control" >
                                @error('map') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">بنر(نمایش در صفحه اصلی)</label>
                            <div class="col-md-9">
                                <input name="banner" type="file" class="form-control" >
                                @error('banner') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ شروع رویداد</label>
                            <div class="col-md-9">
                                <input name="start" value="{{str_replace('-','/',verta($sadra->start_at)->formatDate())}}" type="text" id="start" class="form-control" >
                                @error('start') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ خاتمه رویداد</label>
                            <div class="col-md-9">
                                <input name="end" value="{{str_replace('-','/',verta($sadra->end_at)->formatDate())}}" type="text" id="end" class="form-control" >
                                @error('end') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ شروع ثبت نام</label>
                            <div class="col-md-9">
                                <input name="start_register" value="{{str_replace('-','/',verta($sadra->start_register_at)->formatDate())}}" type="text" id="start_register" class="form-control" >
                                @error('start_register') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ پایان ثبت نام</label>
                            <div class="col-md-9">
                                <input name="end_register" value="{{str_replace('-','/',verta($sadra->end_register_at)->formatDate())}}" type="text" id="end_register" class="form-control" >
                                @error('end_register') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">توضیحات</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="" cols="30" rows="10">{{$sadra->description}}</textarea>
                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                            <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

@endsection

@section('script')
    <script>
        $('#start').persianDatepicker({
            altField: '#start',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
        $('#end').persianDatepicker({
            altField: '#end',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });

        $('#end_register').persianDatepicker({
            altField: '#end_register',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
        $('#start_register').persianDatepicker({
            altField: '#start_register',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
    </script>
@endsection