@extends('panel.company.master')

@section('title' , 'ثبت نام در رویداد')

@section('main')

    <style>
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 2px solid rgba(0, 0, 0, 0.553);
        }
    </style>

    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">ثبت نام در رویداد</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-7 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">اطلاعات تکمیلی ثبت نام</h4>
                </div>

                <div class="card-body">
                    <form class="form-horizontal" action="{{route('register.sadra')}}" id="form" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="sadra" value="{{$event->id}}">
                        {{-- plan --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="plan">انتخاب پلن:</label>
                            <div class="col-md-6">
                                <select class="form-select justify-content-center form-control" name="plan" id="plan">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($event->buy_plans as $plan)
                                        @if($plan->deleted==0)
                                            <option class="form-control"
                                            @if (old('plan') == $plan->id)
                                                selected
                                            @endif
                                            value="{{$plan->id}}">{{$plan->name}}</option>
                                            @if($plan->map)
                                                <a href="{{asset($plan->map->path)}}">نقشه</a>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                                @error('plan')<span class="text-danger">{{ $message }}</span>@enderror
                                
                            </div>
                        </div>

                        {{-- booth --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="booth">انتخاب
                                غرفه:</label>
                            <div class="col-md-6">
                                <select class="form-select form-control" name="booth" id="booth">
                                    <option class="form-control" value="">ابتدا پلن را انتخاب کنید</option>
                                </select>
                                <span id="verifyBooth" class="text-danger"></span>
                            </div>
                        </div>

                        {{-- logo --}}
                        <script>let logoCheck = true ;</script>
                        @if (! Auth::user()->groupable->logo_id)
                            <script>logoCheck = false ;</script>
                            <div class="row mb-4 justify-content-center">
                                <label class="col-md-4 form-label" for="logo">لوگو شرکت: </label>
                                <div class="col-md-6">
                                    <div class="wrap-input validate-input input-group">
                                        <input name="logo" id="logo" class="input border-start-0 ms-0 form-control py-3" type="file"
                                        @if (old('logo'))
                                            value="{{ old('logo') }}"
                                        @endif
                                        >
                                    </div>
                                    <span id="verifyLogo" class="text-danger"></span>
                                </div>
                            </div>
                        @endif

                        {{-- coordinator --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="coordinator">نام مسئول هماهنگی شرکت : </label>
                            <div class="col-md-6">
                                <div class="wrap-input validate-input input-group">
                                    <input id="coordinator" name="coordinator" class="form-control" type="text">
                                </div>
                                @error('coordinator')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>

                        {{-- coordinator mobile --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="coordinatorMobile">تلفن همراه مسئول هماهنگی : </label>
                            <div class="col-md-6">
                                <div class="wrap-input validate-input input-group">
                                    <input id="coordinatorMobile" name="coordinatorMobile" class="form-control" numeric type="text">
                                </div>
                                @error('coordinatorMobile')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>

                        {{-- contract --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="contract">وضعیت تفاهم‌نامه همکاری شرکت با دانشگاه :</label>
                            <div class="col-md-6">
                                <select class="form-select justify-content-center form-control" name="contract" id="contract">
                                    <option value="">انتخاب کنید</option>
                                    <option value="تمایل به انعقاد تفاهم‌نامه رایگان با دانشگاه">تمایل به انعقاد تفاهم‌نامه رایگان با دانشگاه</option>
                                    <option value="شرکت قبلا با دانشگاه تفاهم‌نامه همکاری دارد">شرکت قبلا با دانشگاه تفاهم‌نامه همکاری دارد</option>
                                    <option value="عدم تمایل به تفاهم‌نامه همکاری با دانشگاه">عدم تمایل به تفاهم‌نامه همکاری با دانشگاه</option>
                                </select>
                                @error('contract')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>

                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="inviter">معرف :</label>
                            <div class="col-md-6">
                                <select class="form-select justify-content-center form-control" name="inviter" id="inviter">
                                    <option value="">انتخاب کنید</option>
                                    {{-- <option class="form-control" value="دانشکده حقوق">دانشکده حقوق</option> --}}
                                    {{-- <option class="form-control" value="دانشکده مهارت و کارآفرینی">دانشکده مهارت و کارآفرینی</option> --}}
                                    {{-- <option class="form-control" value="دانشکده علوم انسانی">دانشکده علوم انسانی</option> --}}
                                    {{-- <option class="form-control" value="دانشکده هنر، معماری و شهرسازی">دانشکده هنر، معماری و شهرسازی</option> --}}
                                    <option class="form-control" value="دکتر علیرضا نقش">دکتر علیرضا نقش</option>
                                    <option class="form-control" value="دکتر بهنام صغیرزاده">دکتر بهنام صغیرزاده</option>
                                    <option class="form-control" value="دکتر احسان روحانی">دکتر احسان روحانی</option>
                                    <option class="form-control" value="دکتر محمد شاهقلی">دکتر محمد شاهقلی</option>
                                    <option class="form-control" value="دکتر محمدرضا یوسفی">دکتر محمدرضا یوسفی</option>
                                    <option class="form-control" value="دکتر مهدی ترابی">دکتر مهدی ترابی</option>
                                    <option class="form-control" value="دکتر سیدمحمدعلی زنجانی">دکتر سیدمحمدعلی زنجانی</option>
                                    <option class="form-control" value="دکتر خوشنام شجاعی">دکتر خوشنام شجاعی</option>
                                    <option class="form-control" value="دکتر مجید هاشم زاده">دکتر مجید هاشم زاده</option>
                                    <option class="form-control" value="دکتر شهره آجودانیان">دکتر شهره آجودانیان</option>
                                    <option class="form-control" value="دکتر محمد حججی">دکتر محمد حججی</option>
                                    <option class="form-control" value="دکتر حمید صابری">دکتر حمید صابری</option>
                                    <option class="form-control" value="دکتر اعرابی">دکتر اعرابی</option>
                                    <option class="form-control" value="دکتر ساسان زندی">دکتر ساسان زندی</option>
                                    <option class="form-control" value="دکتر حامد قمی">دکتر حامد قمی</option>
                                    <option class="form-control" value="دکتر مهناز مروی">دکتر مهناز مروی</option>
                                    <option class="form-control" value="دکتر الهام ناظمی">دکتر الهام ناظمی</option>
                                    <option class="form-control" value="دکتر اکبری">دکتر اکبری</option>
                                    <option class="form-control" value="دکتر سلمانی">دکتر سلمانی</option>
                                    <option class="form-control" value="دکتر بکتاش">دکتر بکتاش</option>
                                    <option class="form-control" value="دکتر طباطبایی">دکتر طباطبایی</option>
                                    <option class="form-control" value="سامانه جابیست">سامانه جابیست</option>
                                    <option class="form-control" value="سایر موارد">سایر موارد</option>
                                </select>
                                @error('inviter')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>

                        {{-- <hr> --}}

                        {{-- <div class="row bb-4 justify-content-center">
                            <p>
                                در سوالات زیر رشته و شاخه مورد نیاز استخدامی شرکت خود را مشخص و تعداد حدودی نیروی کار مورد نیاز را در هر گرایش در کادر زیر آن مشخص فرمایید.
                            </p>
                        </div> --}}

                        {{-- <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">

                                    <div class="col-12 col-md-3 ">
                                        <label class="form-label mb-3">دانشکده علوم انسانی : </label>
                                    </div>

                                    <div class="col-12 col-md-5">
                                        <select id="facultyOfHumanities" class="form-control">
                                            <option value="">انتخاب کنید</option>
                                            <option value="زبان انگلیسی"> زبان انگلیسی </option>
                                            <option value="جغرافیا و گردشگری"> جغرافیا و گردشگری </option>
                                            <option value="روانشناسی"> روانشناسی </option>
                                            <option value="تاریخ"> تاریخ </option>
                                            <option value="زبان و ادبیات فارسی"> زبان و ادبیات فارسی </option>
                                            <option value="زبان و ادبیات عربی"> زبان و ادبیات عربی </option>
                                            <option value="مدیریت"> مدیریت </option>
                                            <option value="حسابداری"> حسابداری </option>
                                            <option value="آموزش و پرورش ابتدایی"> آموزش و پرورش ابتدایی </option>
                                            <option value="تربیت بدنی"> تربیت بدنی </option>
                                        </select>
                                        <span id="facultyOfHumanitiesErr" class="text-danger mt-2 mt-md-0"></span>
                                    </div>

                                    <div class="col-12 mt-2 mt-md-0 col-md-2">
                                        <input type="text" id="facultyOfHumanitiesNumber" class="form-control" placeholder="تعداد">
                                        <span id="facultyOfHumanitiesNumberErr" class="text-danger mt-2 mt-md-0"></span>
                                    </div>

                                    <div class="col-12 mt-2 mt-md-0 col-md-2">
                                        <button onclick="addFacultyOfHumanities()" type="button" id="registerFacultyOfHumanities" class="btn btn-sm btn-success">ثبت</button>
                                    </div>

                                </div>
                            </div>
                        </div> --}}

                        {{-- <div class="row mb-4 justify-content-center">
                            <div class="selectgroup selectgroup-pills w-100" id="facultyOfHumanities-list">
                                
                            </div>
                        </div> --}}

                        {{-- <hr> --}}

                        {{-- <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">

                                    <div class="col-12 col-md-3 ">
                                        <label class="form-label mb-3">دانشکده حقوق : </label>
                                    </div>

                                    <div class="col-12 col-md-5">
                                        <select id="schoolOfLaw" class="form-control">
                                            <option value="">انتخاب کنید</option>
                                            <option value="حقوق"> حقوق </option>
                                            <option value="معارف اسلامی"> معارف اسلامی </option>
                                            <option value="علوم قضایی"> علوم قضایی </option>
                                            <option value="الهیات"> الهیات </option>
                                        </select>
                                        <span id="schoolOfLawErr" class="text-danger mt-2 mt-md-0"></span>
                                    </div>

                                    <div class="col-12 mt-2 mt-md-0 col-md-2">
                                        <input type="text" id="schoolOfLawNumber" class="form-control" placeholder="تعداد">
                                        <span id="schoolOfLawNumberErr" class="text-danger mt-2 mt-md-0"></span>
                                    </div>

                                    <div class="col-12 mt-2 mt-md-0 col-md-2">
                                        <button type="button" onclick="addSchoolOfLaw()" class="btn btn-sm btn-success">ثبت</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row mb-4 justify-content-center">
                            <div class="selectgroup selectgroup-pills w-100" id="schoolOfLaw-list">
                                
                            </div>
                        </div>

                        <hr>

                        <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">

                                    <div class="col-12 col-md-3 ">
                                        <label class="form-label mb-3">دانشکده هنر، معماری و شهرسازی : </label>
                                    </div>

                                    <div class="col-12 col-md-5">
                                        <select id="art" class="form-control">
                                            <option value="">انتخاب کنید</option>
                                            <option value="کارشناسی مهندسي معماري"> کارشناسی مهندسي معماري </option>
                                            <option value="کارشناسی مهندسي تكنولوژي معماري"> کارشناسی مهندسي تكنولوژي معماري </option>
                                            <option value="کارشناسی معماری داخلی"> کارشناسی معماری داخلی </option>
                                            <option value="کارشناسی طراحي صنعتي"> کارشناسی طراحي صنعتي </option>
                                            <option value="کارشناسی ارتباط تصويري"> کارشناسی ارتباط تصويري </option>
                                            <option value="کارشناسی طراحی لباس"> کارشناسی طراحی لباس </option>
                                            <option value="کارشناسی طراحی پارچه و لباس"> کارشناسی طراحی پارچه و لباس </option>
                                            <option value="کارشناسی نقاشي"> کارشناسی نقاشي </option>
                                            <option value="کارشناسی مهندسی شهرسازی"> کارشناسی مهندسی شهرسازی </option>
                                            <option value="كارشناسي ارشد طراحي پارچه و لباس"> كارشناسي ارشد طراحي پارچه و لباس </option>
                                            <option value="كارشناسي ارشد پژوهش هنر"> كارشناسي ارشد پژوهش هنر </option>
                                            <option value="كارشناسي ارشد معماري- معماري"> كارشناسي ارشد معماري- معماري </option>
                                            <option value="كارشناسي ارشد معماري- انرژي"> كارشناسي ارشد معماري- انرژي </option>
                                            <option value="كارشناسي ارشد معماري داخلي"> كارشناسي ارشد معماري داخلي </option>
                                            <option value="کارشناسی ارشد مديريت پروژه و ساخت"> کارشناسی ارشد مديريت پروژه و ساخت </option>
                                            <option value="كارشناسي ارشد طراحي شهري"> كارشناسي ارشد طراحي شهري </option>
                                            <option value="كارشناسي ارشد برنامه ريزي شهري"> كارشناسي ارشد برنامه ريزي شهري </option>
                                            <option value="کارشناسی ارشد طراحی صنعتی"> کارشناسی ارشد طراحی صنعتی </option>
                                        </select>
                                        <span id="artErr" class="text-danger mt-2 mt-md-0"></span>
                                    </div>

                                    <div class="col-12 mt-2 mt-md-0 col-md-2">
                                        <input type="text" id="artNumber" class="form-control mt-2 mt-md-0" placeholder="تعداد">
                                        <span id="artNumberErr" class="text-danger mt-2 mt-md-0"></span>
                                    </div>

                                    <div class="col-12 mt-2 mt-md-0 col-md-2">
                                        <button type="button" onclick="addArt()" class="btn btn-sm btn-success">ثبت</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row mb-4 justify-content-center">
                            <div class="selectgroup selectgroup-pills w-100" id="art-list">
                                
                            </div>
                        </div>
                        
                        <hr>

                        <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">

                                    <div class="col-12 col-md-3 ">
                                        <label class="form-label mb-3">دانشکده مهارت و کارآفرینی  : </label>
                                    </div>

                                    <div class="col-12 col-md-5">
                                        <select id="skillCollege" class="form-control">
                                            <option value="">انتخاب کنید</option>
                                            <option value="کاردانی و کارشناسی ناپیوسته معماری"> کاردانی و کارشناسی ناپیوسته معماری </option>
                                            <option value="کاردانی فتوگرافیک"> کاردانی فتوگرافیک </option>
                                            <option value="کاردانی و کارشناسی ناپیوسته طراحی و دوخت پارچه و لباس"> کاردانی و کارشناسی ناپیوسته طراحی و دوخت پارچه و لباس </option>
                                            <option value="کاردانی انیمیشن"> کاردانی انیمیشن </option>
                                            <option value="کاردانی طراحی و ساخت طلا و جواهر"> کاردانی طراحی و ساخت طلا و جواهر </option>
                                            <option value="کاردانی آموزش و پرورش ابتدایی"> کاردانی آموزش و پرورش ابتدایی </option>
                                            <option value="کاردانی تربیت کودک"> کاردانی تربیت کودک </option>
                                            <option value="کاردانی تربیت بدنی"> کاردانی تربیت بدنی </option>
                                        </select>
                                        <span id="skillCollegeErr" class="text-danger mt-2 mt-md-0"></span>
                                    </div>

                                    <div class="col-12 mt-2 mt-md-0 col-md-2">
                                        <input type="text" id="skillCollegeNumber" class="form-control" placeholder="تعداد">
                                        <span id="skillCollegeNumberErr" class="text-danger mt-2 mt-md-0"></span>
                                    </div>

                                    <div class="col-12 mt-2 mt-md-0 col-md-2">
                                        <button type="button" onclick="addSkillCollege()" class="btn btn-sm btn-success">ثبت</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row mb-4 justify-content-center">
                            <div class="selectgroup selectgroup-pills w-100" id="skillCollege-list">
                                
                            </div>
                        </div> --}}
                        

                        <div class=" row mt-3 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                                <button type="button" onclick="submitForm()" class="btn btn-success btn-lg px-5"> ثبت نام در رویداد </button>
                                {{-- <a href="javascript:void(0)" onclick="Check()" class="btn btn-success btn-lg px-5"> ثبت نام
                                </a> --}}
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-lg-12" >
            <div class="card" style="position: sticky !important; top: 100px">
                <div class="card-header"></div>
                <div class="card-body">
                    {{-- company --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">نام شرکت : </label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span>{{Auth::user()->groupable->name}}</span>
                        </div>
                    </div>
                    {{-- company --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">زمینه فعالیت : </label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span>{{Auth::user()->groupable->activity_field}}</span>
                        </div>
                    </div>
                    {{-- ceo --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">نام مدیر عامل شرکت : </label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span>{{Auth::user()->groupable->ceo}}</span>
                        </div>
                    </div>

                    {{-- map --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">نقشه:</label>
                        <div class="col-6">
                            <a name="map" href="javascript:void(0)" target="_blank">
                                <img style="max-width: 100px;max-height: 200px" name="map" id="map" class="img-fluid border border-danger"  src="">
                            </a>
                            <span class="d-block text-info">روی نقشه کلیک کنید</span>
                        </div>
                    </div>
                    {{-- price --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">هزینه ثبت نام
                            (تومان) :</label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span id="price"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script src="{{ asset('assets/panel/js/co-register-to-sadra.js') }}"></script>

    <script>
        let plan = document.getElementById('plan');
        plan.addEventListener('change', function(){
            let planValue = document.getElementById('plan').value;
            let booth = document.getElementById('booth');
            booth.innerHTML = "";

            if(planValue){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    url: "{{route('company.ajax.sadra.get.booth')}}",
                    data: {
                        id : planValue
                    },
                    dataType: "json",
                    success: function (response) {
                        let price = document.getElementById('price');
                        price.innerHTML = "";
                        let img = document.querySelector('img[name="map"]');
                        let link = document.querySelector('a[name="map"]');
                        img.src = "";
                        link.href = "";

                        if(response){
                            let data = ""
                            response['booths'].forEach(function(resp){
                                data += '<option class="form-control" value="' + resp['id'] + '">' + resp['name'] + '</option>'
                            });
                            booth.innerHTML = data;

                            let imgsrc = '{{asset(':url')}}';
                            imgsrc = imgsrc.replace(':url', response['map']);
                            img.src = imgsrc
                            link.href = imgsrc;

                            price.innerHTML = response['price'];
                        }
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            }
        });
    </script>

    {{-- <script>

        let facultyOfHumanities = document.getElementById('facultyOfHumanities').value;

        let registerFacultyOfHumanities = document.getElementById('registerFacultyOfHumanities');

        let facultyOfHumanitiesNumber = document.getElementById('facultyOfHumanitiesNumber').value;

        let facultyOfHumanitiesList = document.getElementById('facultyOfHumanities-list');

        registerFacultyOfHumanities.addEventListener('click' , function () {

            facultyOfHumanitiesList.innerHTML = 
                                `<label class="selectgroup-item">` +
                                    `<input type="hidden" name="`+ facultyOfHumanities +`" value="`+ facultyOfHumanitiesNumber +`" class="array-skills selectgroup-input">`+
                                    `<span class="selectgroup-button">`+ facultyOfHumanities +`<span class="text-info"> `+ facultyOfHumanitiesNumber +` عدد</span></span>`+
                                `</label>`

        });

    </script> --}}

    {{-- <script>

        const idArray = []
        function addSkill(){
            var selectedText = document.getElementById('skills').value;
            if (selectedText != ""){
                checkSkillExists(selectedText,function(skill){
                    if (skill !== null){
                        var collection_skill = document.getElementsByClassName('array-skills');
                        var elementsArray=[].slice.call(collection_skill)
                            var list = document.getElementById('skill-list')
                            var skill_id = skill
                            var element = document.createElement('label');
                            var skill_level = document.getElementById('skill_level').value;
                            var newInput = `<input type="hidden" name="required_skills[${skill_id}]" value="${skill_level}" class="array-skills selectgroup-input">`
                        if(elementsArray.length < 1){
                            element.classList.add('selectgroup-item');
                            element.innerHTML = newInput + '<span class="selectgroup-button">'+ selectedText +'</span>';
                            list.appendChild(element);
                            idArray.push(skill_id)
                        }else if(!idArray.includes(skill_id)){
                                    element.classList.add('selectgroup-item');
                                    element.innerHTML = newInput + `<span class="selectgroup-button">${selectedText}</span>`;
                                    list.appendChild(element);
                                    idArray.push(skill_id)
                        }
                        document.getElementById('skills').value = ""
                    }else{
                            console.log('ERROR')
                    }
                }) 
            }
        }

    </script> --}}
    
@endsection
