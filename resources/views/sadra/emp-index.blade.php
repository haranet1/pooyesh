@extends('admin.layouts.master')

@section('title' , 'لیست رویداد ها')

@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">

            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="card">
                <div class="row card-header justify-content-between">
                    <div class="col-12 col-md-3 mb-2 mb-md-0">
                        <h3 class="card-title">لیست رویداد ها</h3>
                    </div>
                    <div class="col-12 col-md-3 text-end">
                        <a href="{{ route('create.sadra') }}" class="btn btn-primary">افزودن رویداد</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive table-lg">
                        @if ($events->count() > 0)

                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center bg-info text-white">عنوان</th>
                                        <th class="text-center bg-info text-white">شروع و پایان ثبت نام</th>
                                        <th class="text-center bg-info text-white">شروع و پایان رویداد</th>
                                        <th class="text-center bg-info text-white">توضیحات</th>
                                        <th class="text-center bg-info text-white">عکس</th>
                                        <th class="text-center bg-info text-white">تقشه رویداد</th>
                                        <th class="text-center bg-info text-white">بنر</th>
                                        <th class="text-center bg-info text-white">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($events as $event)
                                        <tr>
                                            <td class="text-nowrap align-middle">{{ Str::limit($event->title,'20','...') }}</td>

                                            <td class="text-nowrap align-middle" >
                                                @if(!is_null($event->start_register_at))
                                                    <span>{{verta($event->start_register_at)->formatDate()}}</span>
                                                    <span>لغایت</span>
                                                    <span dir="ltr">{{verta($event->end_register_at)->formatDate()}}</span>
                                                @endif
                                            </td>
    
                                            <td class="text-nowrap align-middle" >
                                                <span>{{verta($event->start_at)->formatDate()}}</span>
                                                <span>لغایت</span>
                                                <span dir="ltr">{{verta($event->end_at)->formatDate()}}</span>
                                            </td>
    
                                            <td class="text-nowrap align-middle"> {{Str::limit($event->description,'20','...')}}</td>
    
                                            <td class="align-middle text-center">
                                                @if($event->photo)
                                                    <img alt="image" class="avatar avatar-md br-7" src="{{asset($event->photo->path)}}">
                                                @else
                                                    <img alt="image" class="avatar avatar-md br-7" src="{{asset('assets/images/default-image.svg')}}">
                                                @endif
                                            </td>
    
                                            <td class="align-middle text-center">
                                                @if($event->map)
                                                    <a href="{{url($event->map->path)}}"> <img alt="image" class="avatar avatar-md br-7" src="{{asset($event->map->path)}}"></a>
    
                                                @endif
                                            </td>
    
                                            <td class="align-middle text-center">
                                                @if($event->banner)
                                                    <a href="{{url($event->banner->path)}}"> <img alt="image" class="avatar avatar-md br-7" src="{{asset($event->banner->path)}}"></a>
    
                                                @endif
                                            </td>
    
                                            <td class="text-center align-middle">
                                                <div class="btn-group align-top">
                                                    <a class="btn btn-sm btn-primary" href="{{route('edit.sadra', $event->id)}}">ویرایش</a>
                                                    <a class="btn btn-sm btn-danger delete" onclick="deleteSadraEvent({{$event->id}})" href="javascript:void(0)">حذف</a>
                                                    <a class="btn btn-sm btn-success"  href="{{route('employee.buy-plan.create',$event->id)}}">افزودن پلن ثبت نام</a>
                                                </div>
                                                <div class="btn-group align-top mt-1">
                                                    <a class="btn btn-sm btn-info" href="{{route('employee.sadra.requests',$event->id)}}">درخواست های ثبت نام</a>
                                                    <a class="btn btn-sm btn-secondary" href="{{route('employee.sadra.done-requests',$event->id)}}">ثبت نام های نهایی</a>
                                                </div>
                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$events->links('pagination.panel')}}
            </div>
        </div>

    </div>


@endsection