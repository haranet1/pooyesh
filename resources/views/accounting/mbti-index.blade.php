@extends('admin.layouts.master')

@section('title' , 'حسابداری تست mbti')

@section('main')

    <style>
        .dropdown.dropdown-lg .dropdown-menu {
            margin-top: -1px;
            padding: 6px 20px;
        }
        .input-group-btn .btn-group {
            display: flex !important;
        }
        .btn-group .btn {
            border-radius: 0;
            margin-left: -1px;
        }
        .btn-group .btn:last-child {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }
        .btn-group .form-horizontal .btn[type="submit"] {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
        }
        .form-horizontal .form-group {
            margin-left: 0;
            margin-right: 0;
        }
        .form-group .form-control:last-child {
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        div.title {
            display: none;
        }

        @media screen and (min-width: 768px) {
            
            .dropdown.dropdown-lg {
                position: static !important;
            }
            .dropdown.dropdown-lg .dropdown-menu {
                min-width: 500px;
            }
        }
    </style>
    
    <div class="page-header">
        <h1 class="page-title">بخش حسابداری</h1>
        @if(Session::has('success'))
            <div class="row">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="row">
                <div class="alert alert-danger">
                    {{Session::pull('error')}}
                </div>
            </div>
        @endif
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">تست mbti</a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="row mb-4">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-4">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">خریدار</h6>
                                    <h1 class="mb-0 number-font">
                                        {{ $students }}
                                    </h1>
                                    <span>نفر</span>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img class="img-fluid" src="{{asset('assets/images/icons8-company-100.png')}}"alt="company">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-4">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">فاکتور</h6>
                                    <h1 class="mb-0 number-font">
                                        {{ $suggestedTestJobCount }}
                                    </h1>
                                    <span>عدد</span>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img class="img-fluid" src="{{asset('assets/images/icons8-company-100.png')}}"alt="company">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-4">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">کل دریافتی</h6>
                                    <h1 class="mb-0 number-font">
                                        {{ number_format($totalReceived) }}
                                    </h1>
                                    <span>تومان</span>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img class="img-fluid" src="{{asset('assets/images/icons8-company-100.png')}}"alt="company">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-12 col-md-10">
                    <form action="{{ route('search.mbti.accounting') }}" class="row justify-content-center" method="get">
                        <div class="col-12 col-md-8">
                            <div class="input-group" id="adv-search">
                                {{-- <div class="form-group"> --}}
                                    <input class="form-control" type="text" 
                                        @isset($oldData['start_date'])
                                            value="{{ $oldData['start_date'] }}"
                                        @endisset
                                    id="start_date" name="start_date" placeholder="از تاریخ"/>
                                    <input class="form-control" type="text" 
                                        @isset($oldData['end_date'])
                                            value="{{ $oldData['end_date'] }}"
                                        @endisset
                                    id="end_date" name="end_date" placeholder="تا تاریخ"/>
                                {{-- </div> --}}
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                            </div>
                        </div>

                        <div class="col-4 col-md-2 mt-2 mt-md-0">
                            <div class="dropdown dropdown-lg">
                                
                                <button type="button" type="submit" class="btn btn-info dropdown-toggle w-100" data-bs-toggle="dropdown" aria-expanded="false">
                                    <span class="caret">فیلتر</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" role="menu">
                                        <div class="form-group">
                                            <label for="filter">نوع خرید</label>
                                            <select name="option" class="form-control form-select">
                                                @isset($oldData['option'])
                                                    <option 
                                                        @if ($oldData['option'] == '')
                                                            selected
                                                        @endif
                                                    value="">همه</option>
                                                    <option 
                                                        @if ($oldData['option'] == 'full')
                                                            selected
                                                        @endif
                                                    value="full">کامل</option>
                                                    <option 
                                                        @if ($oldData['option'] == 'major')
                                                            selected
                                                        @endif
                                                    value="major">رشته</option>
                                                    <option 
                                                        @if ($oldData['option'] == 'career')
                                                            selected
                                                        @endif
                                                    value="career">شغل</option>
                                                @else
                                                    <option value="">همه</option>
                                                    <option value="full">کامل</option>
                                                    <option value="major">رشته</option>
                                                    <option value="career">شغل</option>
                                                @endisset
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="filter">وضعیت پرداخت</label>
                                            <select name="status" class="form-control form-select">
                                                @isset($oldData['status'])
                                                    <option 
                                                        @if ($oldData['status'] == '')
                                                            
                                                        @endif
                                                    value="">همه</option>
                                                    <option 
                                                        @if ($oldData['status'] == '1')
                                                            
                                                        @endif
                                                    value="1">پرداخت شده</option>
                                                    <option 
                                                        @if ($oldData['status'] == '0')
                                                            
                                                        @endif
                                                    value="0">پرداخت نشده</option>
                                                @else
                                                    <option value="">همه</option>
                                                    <option value="1">پرداخت شده</option>
                                                    <option value="0">پرداخت نشده</option>
                                                @endisset
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="contain">موبایل کاربر</label>
                                            <input type="text" name="mobile"
                                                @isset($oldData['mobile'])
                                                    value="{{ $oldData['mobile'] }}"
                                                @endisset
                                            class="form-control">
                                            
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-2">
                                            <span class="glyphicon glyphicon-search" aria-hidden="true">
                                            </span>
                                        </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-4 col-md-2 mt-2 mt-md-0">
                            <a href="{{ route('mbti.accounting') }}" class="btn btn-warning w-100">رفع فیلتر</a>
                        </div>
                        
                    </form>
                </div>

                

                <div class="col-6 col-md-2 mt-2 mt-md-0 mx-auto">
                    <form action="{{ route('export.mbti.accounting') }}" method="post">
                        @csrf
                        <input type="hidden" name="mobile" value="{{ isset($oldData['mobile']) ? $oldData['mobile'] : ''}}">
                        <input type="hidden" name="option" value="{{ isset($oldData['option']) ? $oldData['option'] : ''}}">
                        <input type="hidden" name="status" value="{{ isset($oldData['status']) ? $oldData['status'] : ''}}">
                        <input type="hidden" name="start_date" value="{{ isset($oldData['start_date']) ? $oldData['start_date'] : ''}}">
                        <input type="hidden" name="end_date" value="{{ isset($oldData['end_date']) ? $oldData['end_date'] : ''}}">

                        <button type="sumbit" class="btn btn-secondary w-100">خروجی اکسل</button>
                    </form>
                </div>
            </div>

            

            <div class="row mb-2">
                <span>تعداد : {{$suggestedTestJobs->total()}}</span>
            </div>

            <div class="row mb-4">
                <div class="table-responsive table-lg">
                    @if ($suggestedTestJobs->count() > 0)
                        <table class="table border-top table-bordered mb-0 table-striped">
                            <thead>
                                <tr>
                                    <th class="bg-info text-white fs-12">ردیف</th>
                                    <th class="text-center bg-info text-white">خریدار</th>
                                    <th class="text-center bg-info text-white">نوع خرید</th>
                                    <th class="text-center bg-info text-white">مبلغ</th>
                                    <th class="text-center bg-info text-white">تخفیف</th>
                                    <th class="text-center bg-info text-white">وضعیت پرداخت</th>
                                    <th class="text-center bg-info text-white">تاریخ پرداخت</th>
                                    <th class="text-center bg-info text-white">تراکنش ها</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $counter = (($suggestedTestJobs->currentPage() -1) * $suggestedTestJobs->perPage()) + 1;
                                @endphp
                                @foreach ($suggestedTestJobs as $factor)
                                    <tr>
                                        <td class="text-nowrap text-center align-middle">{{$counter++}}</td>
                                        {{-- user --}}
                                        <td class="text-nowrap align-middle">
                                            @if ($factor->buyer->groupable_type == App\Models\StudentInfo::class || $factor->buyer->groupable_type == App\Models\ApplicantInfo::class)
                                                <a href="{{ route('candidate.single' , $factor->buyer->id) }}" target="_blank" class="text-description">
                                                    <div class="col-5 col-md-9 p-md-0">{{ $factor->buyer->name }} {{ $factor->buyer->family }}</div>
                                                </a>
                                            @elseif ($factor->buyer->groupable_type == App\Models\CompanyInfo::class)
                                                <a href="{{ route('company.single' , $factor->buyer->id) }}" target="_blank" class="text-description">
                                                    <div class="col-5 col-md-9 p-md-0">{{ $factor->buyer->groupable->name }}</div>
                                                </a>
                                            @else
                                                <div class="col-5 col-md-9 p-md-0">{{ $factor->buyer->name }} {{ $factor->buyer->family }}</div>
                                            @endif
                                            <div class="col-5 col-md-9 p-md-0">{{ $factor->buyer->mobile }}</div>
                                        </td>
                                        
                                        <td class="text-nowrap align-middle text-center">
                                            {{ getTypeOfSuggestedTestToFa($factor->option) }}
                                        </td>
                                        
                                        <td class="text-nowrap align-middle text-center">
                                            @if ($factor->payments)
                                                @php
                                                    $price = 0;
                                                    foreach($factor->payments as $payment){
                                                        
                                                        $price = $payment->price;
                                                        
                                                    }
                                                @endphp
                                                {{ number_format($price) }} تومان
                                            @endif
                                        </td>

                                        <td class="text-nowrap align-middle text-center">
                                            @if ($factor->discount)
                                                {{ number_format($discount_price) }} تومان
                                            @else
                                                بدون تخفیف
                                            @endif
                                        </td>

                                        <td class="text-nowrap align-middle text-center">
                                            @if ($factor->status == 1)
                                                <span class="text-success">پرداخت شده</span>
                                            @else
                                                <span class="text-danger">پرداخت نشده</span>
                                            @endif
                                        </td>

                                        <td class="text-nowrap align-middle text-center">
                                            @if ($factor->payments)
                                                @php
                                                    $date = now();
                                                    foreach($factor->payments as $payment){
                                                        if(! is_null($payment->receipt)){
                                                            $date = $payment->updated_at;
                                                        }
                                                    }
                                                @endphp
                                                {{ verta($date)->formatJalaliDate() }}
                                            @endif
                                        </td>

                                        <td class="text-nowrap align-middle text-center">
                                            <button type="button" class="btn btn-info" data-bs-target="#modal-{{ $factor->id }}" data-bs-toggle="modal">:</button>
                                            <div class="modal fade" id="modal-{{ $factor->id }}" tabindex="-1"  aria-hidden="false">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h6 class="modal-title">لیست تراکنش ها</h6>
                                                            <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="">
                                                                <table class="table border-top table-bordered mb-0 table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="fs-12">ردیف</th>
                                                                            <th class="text-center">مبلغ</th>
                                                                            <th class="text-center">رسید</th>
                                                                            <th class="text-center">وضعیت</th>
                                                                            <th class="text-center">تاریخ و ساعت پرداخت</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @php
                                                                            $counter2 = 1;
                                                                        @endphp
                                                                        @foreach ($factor->payments as $payment)
                                                                            <tr>
                                                                                <td class="text-nowrap text-center align-middle">{{$counter2++}}</td>
                                                                                <td class="text-nowrap align-middle text-center">
                                                                                    {{ number_format($payment->price) }} تومان
                                                                                </td>
                                                                                <td class="text-nowrap align-middle text-center">
                                                                                    {{ $payment->receipt ?? '- - -' }}
                                                                                </td>
                                                                                <td class="text-nowrap align-middle text-center">
                                                                                    {{ $payment->status ?? '- - -' }}
                                                                                </td>
                                                                                <td class="text-nowrap align-middle text-center">
                                                                                    {{ $payment->payment_date_time ?? '- - -' }}
                                                                                </td>
                                                                                
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-warning text-center">
                            اطلاعاتی جهت نمایش وجود ندارد
                        </div>
                    @endif
                </div>
            </div>

            <div class="mb-5">
                {{$suggestedTestJobs->links('pagination.panel')}}
            </div>

        </div>
        
    </div>


@endsection

@section('script')
    <script>
        $('#start_date').persianDatepicker({
            altField: '#start_date',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
        $('#end_date').persianDatepicker({
            altField: '#end_date',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
    </script>
@endsection