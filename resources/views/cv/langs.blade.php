@extends('panel.student.master')

@section('title' , 'مهارت ها')

@section('css')
    <link href="{{ asset('assets/panel/css/style-css/create-job-position.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js@9.0.1/public/assets/styles/choices.min.css" />

@endsection

@section('main')

    <style>
        .choices__item--selectable {
            
            color: #000; /* رنگ متن سفارشی */
        }
        .choices__inner {
            border-radius: 1.3rem !important;
        }

        .choices__list--dropdown .choices__item {
            padding: 10px; /* سفارشی کردن فاصله داخلی گزینه‌ها */
        }

        .choices__list--dropdown {
            max-height: 300px; /* حداکثر ارتفاع */
            overflow-y: auto;  /* فعال کردن اسکرول عمودی */
        }
    </style>

    @if(Session::has('success'))
        <div class="row mt-5">
            <div class="alert alert-success">
                {{Session::pull('success')}}
            </div>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="row">
            <div class="alert alert-danger">
                {{Session::pull('error')}}
            </div>
        </div>
    @endif
    
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ساخت رزومه</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-12">
            <div class="card statiy-md-top">
                <div class="card-header justify-content-between">
                    <div class="alert bg-info text-white w-100 text-center">
                        <h3 class="card-title">
                            ! از لیست زیر مهارت های فنی فعلی خود را انتخاب کرده و سطح هر مهارت را تایین کنید.
                        </h3>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('store.lang.cv') }}" method="POST" id="savedForm">
                        @csrf
                        @if($langs && $langs->count() > 0)
                            <div id="technologySkillBox" class="rounded-4 py-5 px-md-6 px-3 box-body duties-box position-relative">
                                <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3">
                                    <h3 class="fw-bold text-white-c fs-5 mb-0 task-title">زبان های خارجی</h3>
                                </div>

                                <div class="row my-5 px-5">
                                    <div class="col-12 col-md-6">
                                        <label for="" class="form-label">انتخاب زبان خارجی</label>
                                        <select class="form-select" dir="rtl" id="js-choice">
                                            <option selected value="">انتخاب کنید</option>
                                            @foreach ($langs as $lang)
                                                <option class="ms-5" value="{{ $lang->id }}">{{ $lang->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row" id="langsContainer" style="min-height: 280px;">
                                    @if ($myLangs->count() > 0)
                                        @foreach ($myLangs as $mylang)
                                            <div class="col-md-6 col-12">
                                                
                                                <div class="bg-body-2 mt-4 rounded-4 p-3 lang-active">
                                                    <div class="flex-column">
                                                        <div class="d-flex align-self-start align-self-md-center mb-2 align-items-center w-100 justify-content-between">
                                                            <input class="form-check-input d-none" type="checkbox" id="langInput-{{ $mylang->id }}" value="{{ $mylang->id }}" />
                                                            <label for="langInput-{{ $mylang->id }}" class="text-dark-c mb-0 fs-16 fw-normal">{{ $mylang->name }}</label>

                                                            <a href="{{ route('delete.lang.cv' , $mylang->id) }}" class="text-danger px-2 fs-20" data-bs-toggle="tooltip" data-bs-placement="top" title="حذف">X</a>
                                                        </div>
                                                        <div class="d-flex width-range gap-3 align-items-center me-md-5 w-100">
                                                            <input oninput="changeDataRange(this , 'lang')" id="langRange-{{ $mylang->id }}" type="range" 
                                                            class="form-range" min="1" max="5" step="1" value="{{ changeFaLevelToOnetLevel($mylang->pivot->level) }}"/>
                                                            <div class="text-dark-c mb-0 fs-16 fw-normal fit-content w-25">{{ $mylang->pivot->level }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
            
                            </div>
                            <p id="technologySkillErr" class="red form-feedback fs-18 mt-2"></p>
                        @endif
                        <div class="row justify-content-center mt-5">
                            <div class="col-12 col-md-4">
                                <button type="submit" 
                                    {{-- onclick="submitForm()"  --}}
                                    class="text-white-c bg-success btn-add w-100">
                                    ثبت
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/choices.js@9.0.1/public/assets/scripts/choices.min.js"></script>
    <script src="{{ asset('assets/panel/js/cv/langs.js') }}"></script>
@endsection