@extends('panel.student.master')

@section('title' , 'اطلاعات آکادمیک')

@section('main')
        <style>
            .month-grid-box > .header > .title {
                display: none !important;
            }
        </style>
        
        @if(Session::has('success'))
            <div class="row mt-5">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="row">
                <div class="alert alert-danger">
                    {{Session::pull('error')}}
                </div>
            </div>
        @endif
    
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ساخت رزومه</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card statiy-md-top" style="position: sticky !important; top: 100px;">
                <div class="card-header justify-content-between">
                    <h3 class="card-title">اضافه کردن اطلاعات آکادمیک</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('store.academic.info.cv') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-md-6 mb-2">
                                <label for="grade" class="form-lable">انتخاب مقطع</label>
                                <select  class="form-control form-select mb-2" name="grade" id="grade">
                                    <option value="">انتخاب ...</option>
                                    <option value="دکترا">دکترا</option>
                                    <option value="کارشناسی ارشد">کارشناسی ارشد</option>
                                    <option value="کارشناسی">کارشناسی</option>
                                    <option value="کاردانی">کاردانی</option>
                                    <option value="دیپلم">دیپلم</option>
                                </select>
                                @error('grade')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6 mb-2">
                                <label for="major" class="form-lable">رشته</label>
                                <input type="text"  name="major" id="major" class="form-control mb-2">
                                @error('major')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6 mb-2">
                                <label for="university" class="form-lable">دانشگاه | موسسه آموزشی</label>
                                <input type="text"  name="university" id="university" class="form-control mb-2">
                                @error('university')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6 mb-2">
                                <label for="end" class="form-lable">سال اخذ مدرک</label>
                                <input type="text" name="end_date" id="end" class="form-control mb-2">
                                @error('end_date')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6 mb-2">
                                <label for="avg" class="form-lable">معدل</label>
                                <input type="text" name="avg" id="avg" class="form-control mb-2">
                                @error('avg')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="AcademicBtn" class="form-lable"></label>
                                <button type="submit" class="btn btn-success w-100 mt-2" id="AcademicBtn">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @if ($academinInfos->count() > 0)
            <div class="col-12 col-md-4">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h3 class="card-title">اطلاعات ثبت شده</h3>
                    </div>
                    <div class="card-body">
                        @foreach ($academinInfos as $academicInfo)
                            <ul class="list-group list-group-light mb-5">
                                <li class="list-group-item">مقطع تحصیلی : {{ $academicInfo->grade }}</li>
                                <li class="list-group-item">رشته : {{ $academicInfo->major }}</li>
                                <li class="list-group-item">دانشگاه | موسسه آموزشی : {{ $academicInfo->university }}</li>
                                <li class="list-group-item">سال اخذ مدرک : {{ $academicInfo->end_date }}</li>
                                <li class="list-group-item">معدل {{ $academicInfo->avg }}</li>
                                <li class="list-group-item">
                                    <form action="{{ route('delete.AcademicInfo.cv') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="academicInfoId" value="{{ $academicInfo->id }}">
                                        <button type="submit" class="btn btn-danger text-white">
                                            <i class="fa fa-trash"></i> حذف
                                        </button>
                                    </form>
                                </li>
                            </ul>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection
@section('script')
    <script>
        $('#end').persianDatepicker({
            altField: '#end',
            altFormat: "YYYY",
            observer: true,
            format: 'YYYY',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
            "dayPicker": {
                "enabled": false,
                "titleFormat": "YYYY MMMM"
            },
            "monthPicker": {
                "enabled": false,
                "titleFormat": "YYYY"
            },
            "yearPicker": {
                "enabled": true,
                "titleFormat": "YYYY"
            }
        }
    );
    </script>
@endsection