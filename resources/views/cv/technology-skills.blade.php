@extends('panel.student.master')

@section('title' , 'مهارت ها')

@section('css')
    <link href="{{ asset('assets/panel/css/style-css/create-job-position.css') }}" rel="stylesheet" />
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js@9.0.1/public/assets/styles/base.min.css" /> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js@9.0.1/public/assets/styles/choices.min.css" />

@endsection

@section('main')

    <style>
        .choices__item--selectable {
            
            color: #000; /* رنگ متن سفارشی */
        }

        .choices__inner {
            border-radius: 1.3rem !important;
        }

        .choices__list--dropdown .choices__item {
            padding: 10px; /* سفارشی کردن فاصله داخلی گزینه‌ها */
        }

        .choices__list--dropdown {
            max-height: 300px; /* حداکثر ارتفاع */
            overflow-y: auto;  /* فعال کردن اسکرول عمودی */
        }
    </style>

        @if(Session::has('success'))
            <div class="mt-5 row">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="row">
                <div class="alert alert-danger">
                    {{Session::pull('error')}}
                </div>
            </div>
        @endif
    
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ساخت رزومه</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-12">
            <div class="card statiy-md-top">
                <div class="card-header justify-content-between">
                    <div class="text-center text-white alert bg-info w-100">
                        <h3 class="card-title">
                            ! از لیست زیر مهارت های فنی فعلی خود را انتخاب کرده و سطح هر مهارت را تایین کنید.
                        </h3>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('store.technology.skills.cv') }}" method="POST" id="savedForm">
                        @csrf
                        @if($techExamples && $techExamples->count() > 0)
                            <div id="technologySkillBox" class="px-3 py-5 rounded-4 px-md-6 box-body duties-box position-relative">
                                <div class="py-3 heading-box-2 rounded-bottom-3 heading-top-2 px-7">
                                    <h3 class="mb-0 fw-bold text-white-c fs-5 task-title">مهارت های فنی</h3>
                                </div>

                                <div class="px-5 my-5 row">
                                    <div class="col-12 col-md-6">
                                        <label for="" class="form-label">انتخاب مهارت فنی (نرم افزار تخصصی)</label>
                                        <select name="technologySkillId" class="form-select" dir="rtl" id="js-choice">
                                            @foreach ($techExamples as $techExample)
                                                <option class="ms-5" value="{{ $techExample->id }}">{{ $techExample->example }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-12 col-md-6">
                                        <label for="" class="form-label">بعد از انتخاب مهارت فنی تکنولوژی مورد نظر را انتخاب کنید</label>
                                        <select class="form-select" dir="rtl" id="js-choice2">
                                            
                                        </select>
                                    </div> --}}
                                </div>

                                <div class="row" id="technologySkillsContainer" style="min-height: 280px;">
                                    @if ($myTechnologySkills->count() > 0)
                                        @foreach ($myTechnologySkills as $myTechnologySkill)
                                            <div class="col-md-6 col-12">
                                                <div class="p-3 mt-4 bg-body-2 rounded-4 technologySkill-active">
                                                    <div class="flex-column">
                                                        <div class="mb-2 d-flex align-self-start align-self-md-center align-items-center w-100 justify-content-between">

                                                            <input 
                                                                class="form-check-input d-none" type="checkbox" id="technologySkillInput-{{ $myTechnologySkill->exampleName->id }}" value="{{ $myTechnologySkill->exampleName->id }}" />
                                                            
                                                            <label for="technologySkillInput-{{ $myTechnologySkill->id }}" class="px-2 mb-0 text-dark-c fs-16 fw-normal">{{ $myTechnologySkill->exampleName->example }}</label>

                                                            <a href="{{ route('delete.technology.skills.cv' , $myTechnologySkill->id) }}" 
                                                                class="px-2 text-danger fs-20" data-bs-toggle="tooltip" data-bs-placement="top" title="حذف">X</a>
                                                        </div>
                                                        <div class="gap-3 d-flex width-range align-items-center me-md-5 w-100">
                                                            <input oninput="changeDataRange(this , 'technologySkill')" id="technologySkillRange-{{ $myTechnologySkill->exampleName->id }}" type="range" 
                                                            class="form-range" min="1" max="5" step="1" value="{{ $myTechnologySkill->level }}"/>
                                                            <div class="mb-0 text-dark-c fs-16 fw-normal fit-content w-25">{{ changeLevelToFaLevel($myTechnologySkill->level) }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
            
                            </div>
                            <p id="technologySkillErr" class="mt-2 red form-feedback fs-18"></p>
                        @endif
                        <div class="mt-5 row justify-content-center">
                            <div class="col-12 col-md-4">
                                <button type="button" 
                                    onclick="submitForm()" 
                                    class="text-white-c bg-success btn-add w-100">
                                    ثبت
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<input type="hidden" id="getExamplesCvUrl" value="{{ route('get.examples.cv') }}">
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/choices.js@9.0.1/public/assets/scripts/choices.min.js"></script>
    <script src="{{ asset('assets/panel/js/cv/technology-skills.js') }}"></script>
@endsection