@extends('panel.student.master')

@section('title' , 'سابقه کاری')

@section('main')
    <style>
        .month-grid-box > .header > .title {
            display: none !important;
        }
    </style>

        @if(Session::has('success'))
            <div class="row mt-5">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="row">
                <div class="alert alert-danger">
                    {{Session::pull('error')}}
                </div>
            </div>
        @endif
    
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ساخت رزومه</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card statiy-md-top" style="position: sticky !important; top: 100px;">
                <div class="card-header justify-content-between">
                    <h3 class="card-title">اضافه کردن سابقه کاری</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('store.work.experiences.cv') }}" method="POST">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-6 mb-2">
                                <label for="title" class="form-lable">عنوان</label>
                                <input type="text"  name="title" id="title" class="form-control mb-2">
                                @error('title')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6 mb-2">
                                <label for="company" class="form-lable">نام سازمان</label>
                                <input type="text"  name="company" id="company" class="form-control mb-2">
                                @error('company')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6 mb-2">
                                <label for="start_date" class="form-lable">تاریخ شروع</label>
                                <input type="text" name="start_date" id="start_date" class="form-control mb-2">
                                @error('start_date')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6 mb-2">
                                <label for="end_date" class="form-lable">تاریخ پایان</label>
                                <input type="text" name="end_date" id="end_date" class="form-control mb-2">
                                @error('end_date')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6">
                                <button type="submit" class="btn btn-success w-100 mt-2" id="AcademicBtn">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @if ($myExperiences->count() > 0)
            <div class="col-12 col-md-4">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h3 class="card-title">اطلاعات ثبت شده</h3>
                    </div>
                    <div class="card-body">
                    @foreach ($myExperiences as $expersience)
                        <ul class="list-group list-group-light mb-5">
                            <li class="list-group-item">عنوان : {{ $expersience->title }}</li>
                            <li class="list-group-item">نام سازمان : {{ $expersience->company }}</li>
                            <li class="list-group-item">تاریخ شروع : {{ verta($expersience->start_date)->formatJalaliDate() }}</li>
                            <li class="list-group-item">تاریخ پایان : {{ verta($expersience->end_date)->formatJalaliDate() }}</li>
                            <li class="list-group-item">
                                <form action="{{ route('delete.work.experiences.cv') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="workId" value="{{ $expersience->id }}">
                                    <button type="submit" class="btn btn-danger text-white">
                                        <i class="fa fa-trash"></i> حذف
                                    </button>
                                </form>
                            </li>
                        </ul>
                    @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection
@section('script')
    <script>
        $('#start_date').persianDatepicker({
            altField: '#start_date',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
        $('#end_date').persianDatepicker({
            altField: '#end_date',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
    </script>
@endsection