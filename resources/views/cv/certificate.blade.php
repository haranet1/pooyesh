@extends('panel.student.master')

@section('title' , 'دوره ها و گواهینامه ها')

@section('main')

    @if(Session::has('success'))
        <div class="row mt-5">
            <div class="alert alert-success">
                {{Session::pull('success')}}
            </div>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="row">
            <div class="alert alert-danger">
                {{Session::pull('error')}}
            </div>
        </div>
    @endif

    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ساخت رزومه</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-12 col-md-4">
            <div class="card" style="position: sticky !important; top: 100px;">
                <div class="card-header">
                    <h3 class="card-title">اضافه کردن دوره ها و گواهینامه ها</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('store.certificates.cv') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-12 mb-2">
                                <label for="title" class="form-lable">عنوان</label>
                                <input type="text"  name="title" id="title" class="form-control mb-2">
                                @error('title')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 mb-2">
                                <label for="image_file" class="form-lable">بارگزاری تصویر</label>
                                <input type="file"  name="image_file" id="image_file" class="form-control mb-2">
                                @error('image_file')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6">
                                <button type="submit" class="btn btn-success w-100 mt-2" id="AcademicBtn">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if ($myCertificates->count() > 0)
            <div class="col-12 col-md-8">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h3 class="card-title">اطلاعات ثبت شده</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($myCertificates as $certificate)
                                    <div class="col-12 col-md-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="d-flex justify-content-center align-items-center" style="height:200px">
                                                    <img style="" src="{{asset($certificate->photo->path)}}" class="img-fluid mb-4"></a>
                                                </div>
                                            </div>
                                            <div class="d-flex card-footer justify-content-between align-items-center">
                                                <h4 class="card-title mb-0">
                                                    {{ $certificate->title }}
                                                </h4>
                                                <form class="" action="{{ route('delete.certificates.cv') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="certificateId" value="{{ $certificate->id }}">
                                                    <button type="submit" class="btn btn-link p-0">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </button>  
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection