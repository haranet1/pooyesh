@extends('panel.student.master')

@section('title' , 'شبکه های اجتماعی')

@section('main')

    @if(Session::has('success'))
        <div class="row mt-5">
            <div class="alert alert-success">
                {{Session::pull('success')}}
            </div>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="row mt-5">
            <div class="alert alert-danger">
                {{Session::pull('error')}}
            </div>
        </div>
    @endif

    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ساخت رزومه</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">اضافه کردن شبکه اجتماعی</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('store.socials.cv') }}" method="POST">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-6 mb-2">
                                <label for="social" class="form-lable">انتخاب شبکه اجتماعی</label>
                                <select class="form-control form-select" name="social" id="social">
                                    <option value="instagram">اینستاگرام</option>
                                    <option value="telegram">تلگرام</option>
                                    <option value="linkedin">لینکدین</option>
                                    <option value="twitter">توییتر</option>
                                    <option value="github">گیت هاب</option>
                                    <option value="eitaa">ایتا</option>
                                </select>
                                @error('social')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6 mb-2">
                                <label for="link" class="form-lable">شناسه کاربری (ID) :</label>
                                <input type="text"  name="link" id="link" class="form-control mb-2">
                                @error('link')<span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 col-md-6">
                                <button type="submit" class="btn btn-success w-100 mt-2" id="AcademicBtn">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if ($mySocials->count() > 0)
            <div class="col-12 col-md-4">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h3 class="card-title">اطلاعات ثبت شده</h3>
                    </div>
                    <div class="card-body">
                        @foreach ($mySocials as $social)
                            <form action="{{ route('delete.socials.cv') }}" method="post">
                                @csrf
                                <label class="selectgroup-item">
                                    <span class="selectgroup-button"> 
                                        <input type="hidden" name="soId" value="{{ $social->id }}">
                                        <button type="submit" class="btn btn-link p-0">
                                            <i class="fa fa-trash text-danger"></i>
                                        </button>  
                                        {{ __('socialNetworks.' . $social->name) }} | {{ $social->link }}
                                    </span>
                                </label>
                            </form>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection