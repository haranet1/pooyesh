<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>قرار داد پویش</title>

    <style>
        @font-face {
            font-family: "IranNastaliq";
            /* src: url("https://db.onlinewebfonts.com/t/3b019b0df3b0d6ea4d1b2f051132febb.eot");
            src: url("https://db.onlinewebfonts.com/t/3b019b0df3b0d6ea4d1b2f051132febb.eot?#iefix")format("embedded-opentype"), */
            url({{ asset('fonts/woff2/IranNastaliq.woff2') }})format("woff2"),
            url({{ asset('fonts/woff/IranNastaliq.woff') }})format("woff");
        }
        @font-face {
            font-family: 'Sans';
            font-style: normal;
            font-weight: 400;
            src:  url({{asset('fonts/woff/IRANSansWeb.woff')}}) format('woff');
        }
        .body {
            background: white;
            margin: 0 auto;
            /* margin-bottom: 0.5cm; */
            /* box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5); */
        }

        .A4 {
            width: 21cm;
            height: 29.5cm;
        }
        header {
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            height: 100px;
        }
        footer {
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            height: 100px;
            font-family: Sans;
            font-size: 13px;
        }
        .flex-33 {
            flex: 33%;
            text-align: center;
            margin-top: 0.5cm;
        }

        .first-title {
            display: flex;
            align-items: center;
            justify-content: center;
            margin: 0cm 2cm 0cm 2cm;
        }
        .title {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-left: 2cm;   
            margin-right: 2cm;
        }
        section{
            margin-left: 2cm;   
            margin-right: 2cm;
            text-align: center;
            text-align: justify;
            font-size: 14px;
            font-family: Sans;
        }
        .right-header{
            font-family: IranNastaliq;
            font-size: 20px;
            line-height: 20px;
        }
        .ul1 > li {
            margin-bottom: 10px;
        }

    </style>
</head>
<body size="A4" dir="rtl">
    <div class="A4 body">

        <header>
            <div class="flex-33 right-header">دانشگاه آزاد اسلامی <br> واحد نجف آباد</div>
            <div class="flex-33">
                <img class="" src="{{ asset('assets/images/img.png') }}" alt="" style="width: 20%">
            </div>
            <div class="flex-33" style="font-family: Sans; font-size: 12px">
                شماره : {{ $contractName }} <br>
                تاریخ : {{ verta()->now()->formatJalaliDate() }}
            </div>
        </header>

        <div class="first-title right-header">
            <h3 style="margin-top: 10px; margin-bottom: 10px;">بسمه تعالی</h3>
        </div>

        <div class="title right-header">
            <h2 style="margin-top: 10px; margin-bottom: 20px;">قرار داد کارآموزی طرح پویش</h2>
        </div>
    
        <section class="sec1" style="font-size: 12px">
            <span> این قرارداد مابین شرکت </span>
            <span>{{ $company->groupable->name }}</span>
    
            <span> به شماره ثبت </span>
            <span>{{ $company->groupable->registration_number }}</span>
    
            @if ($company->groupable->address)
                <span> به آدرس </span>
                <span>{{ $company->groupable->address }}</span>
            @endif
    
            @if ($company->sex == 'male')
                <span> به نمایندگی جناب آقای </span>
                <span>{{ $company->name }} {{ $company->family }}</span>
            @else
                <span> به نمایندگی سرکار خانم </span>
                <span>{{ $company->name }} {{ $company->family }}</span>
            @endif
    
            @if ($company->n_code)
                <span> با کد ملی </span>
                <span>{{ $company->n_code }}</span>
            @endif
    
            @if ($company->groupable->phone)
                <span> به شماره تلفن </span>
                <span>{{ $company->groupable->phone }}</span>
                <span> و </span>
            @endif
    
            <span> با شماره همراه </span>
            <span>{{ $company->mobile }}</span>
    
            <span> که مِن بعد حامی نامیده می شود </span>
            
            @if ($student->sex == 'male')
                <span> و آقای </span>
                <span>{{ $student->name }} {{ $student->family }}</span>
            @else
                <span> و خانم </span>
                <span>{{ $student->name }} {{ $student->family }}</span>
            @endif
    
            @if ($student->groupable->n_code)
                <span> داری کد ملی </span>
                <span>{{ $student->groupable->n_code }}</span>
            @endif
    
            <span> دانشجوی </span>
            
            @if ($student->groupable->major)
                <span> رشته </span>
                <span>{{ $student->groupable->major }}</span>
            @endif
    
            @if ($student->groupable->grade)
                <span> مقطع </span>
                <span>{{ $student->groupable->grade }}</span>
            @endif
    
            <span> دانشگاه آزاد اسلامی واحد نجف آباد </span>
    
            @if ($student->groupable->std_number)
                <span> به شماره دانشجویی </span>
                <span>{{ $student->groupable->std_number }}</span>
            @endif
    
            <span> با شماره موبایل </span>
            <span>{{ $student->mobile }}</span>
    
            @if (!$student->groupable->address)
                <span> در </span>
                @if ($student->groupable->province)
                    <span> استان </span>
                    <span>{{ $student->groupable->province->name }}</span>
                @endif
                @if ($student->groupable->city)
                    <span> شهر </span>
                    <span>{{ $student->groupable->city->name }}</span>
                @endif
                <span> به آدرس </span>
                <span> {{ $student->groupable->address }} </span>
                @if ($student->groupable->Plaque)
                    <span> پلاک </span>
                    <span>{{ $student->groupable->Plaque }}</span>
                @endif
            @endif
    
            <span> که مِن بعد کارآموز نامیده می شود </span>
    
            <span> و جناب آقای دكتر مهدی شانه معاون آموزشی و مهارتی </span>
    
            <span> دانشگاه آزاد اسلامی واحد نجف آباد به عنوان ناظر قرارداد به شرح ذیل می گردد. </span>
           
        </section>
    
        <section class="sec2" style="font-size: 12px">
            <h5 style="margin-bottom: 10px;"> ماده1) موضوع قرارداد </h5>
    
            <span> اجرای طرح پویش (اشتغال با تحصیل) با هدف تواًم کردن آموزش مهارت </span>
    
            <h4 style="margin-bottom: 10px;"> ماده2) مدت قرارداد </h4>
    
            <span> مدت این قرارداد از تاریخ </span>
            <span>{{ verta()->now()->formatJalaliDate() }}</span>
    
            <span> لغایت </span>
            <span>{{ verta()->now()->addMonths(3)->formatJalaliDate() }}</span>
    
            <span> می باشد و کارآموز موظف است در طول نیمسال 240 ساعت در قالب کارآموزی و با معرفی نامه از واحد دانشگاهی در محل تعیین شده توسط حامی حضور داشته باشند. </span>
    
            <h5 style="margin-bottom: 10px;"> ماده3) مبلغ قرارداد </h5>
    
            @isset($salary)
                <span> در صورت رضایت حامی از عملکرد کارآموز، ماهانه مبلغ </span>
                <span>{{ $salary['num'] }} ریال</span>
                <span> ( {{ $salary['word'] }} ریال) </span>
                <span> حامی به شماره حساب </span>
                <span> 0106047297001 </span>
                <span>( IR27010000000106047297001 )</span>
            @endisset
            
            <span> به نام دانشگاه آزاد اسلامی واحد نجف آباد نزد بانک ملی ایران واریز گردد که این مبلغ در شهریه نیمسال دانشجو لحاظ می گردد. </span>
            <span>لازم به ذکر است که حامی ملزم است تاریخ واریز را به دانشگاه اعلام نماید.</span>
            <span> بدیهی است این وجه هیچ گونه تعهدی برای حامی نزد دانشجو ایجاد نخواهد کرد. </span>
            <br>
            <span><Strong> تبصره یک: </Strong></span>
            <span> مبلغ فوق بابت 2 روز حضور و فعالیت کارآموز در هفته می باشد و چنانچه بر حسب نیاز و توافق طرفین مدت حضور کارآموز افزایش یابد، این مبلغ افزایش می یابد. </span>
    
            <h4 style="margin-bottom: 10px;"> ماده4) تعهدات حامی </h4>
            <ul>
                <li>
                    <span> تامین ایمنی محیط و شرایط و امکانات مناسب و مورد نیاز جهت حضور و انجام فعالیت کارآموز به عهده حامی می باشد و باید به تایید دانشگاه برسد. </span>
                </li>
            </ul>
            <ul class="ul1">
                <li>
                    <span> به کارگیری دانشجو در قالب کارآموزی پویش است و از تحمیل کارهای خارج از عرف کارآموزی و دانشجویی باید اجتناب شود. </span>
                </li>
                <li>
                    <span> حامی می تواند پس از آشنایی با توانایی های دانشجو، در صورت توافق دو طرف خارج از طرح پویش با دانشجو به شرط آنکه به تحصیل تمام وقت دانشجو لطمه ای نزد به صورت موردی قرارداد ببندد و این توافق ارتباطی با دانشگاه ندارد و فقط لازم است واحد دانشگاهی مطلع شود. </span>
                </li>
                <li>
                    <span> دانشجویان پویشي در چارچوب قرارداد پویش، دانشجوي كارآموزي تلقي ميشوند و حامي باید قوانين حاكم بر كارآموزي دانشگاه را رعایت كند تا دچار محدودیت هاي ناشي از قوانين نيروي كار نشود. در ضمن حامي مي توانداز كمك واحد دانشگاهي در جهت تربيت نيروي مورد نياز خود در حين تحصيل دانشجو برخوردار گردد و دانشجویان پویشي را در اولویت استخدام قرار دهد. </span>
                </li>
            </ul>

            <h5 style="margin-bottom: 10px;"> ماده5) تعهدات کارآموز </h5>

            <ul class="ul1">
                <li>
                    <span> دانشجو موظف است به لحاظ تكميل ساعات كارآموزي مطابق باتقویم دانشگاه در محل تعيين شده توسط حامي حضور یابد. بدیهي است هرگونه تغيير باید با هماهنگي و تایيد دانشگاه باشد. </span>
                </li>
                
                
            </ul>
        </section>
    
        <footer>
            <div class="flex-33" style="margin-top: 0">
                حامی <br> {{ $company->name }} {{ $company->family }}
            </div>
            <div class="flex-33" style="position: relative;margin-top: 0">
                <img src="{{ asset('assets/signatures/shane.png') }}" style="position: absolute;width: 100%;top:-55px;left:5px" alt="">
                <span style="z-index: 10">ناظر <br> دكتر مهدی شانه <br> معاون آموزشی و مهارتی</span>
            </div>
            <div class="flex-33" style="margin-top: 0">
                کارآموز <br> {{ $student->name }} {{ $student->family }}
            </div>
        </footer>
    </div>
    
    <div class="A4 body">
        <header>
            <div class="flex-33 right-header" style="margin-top: 1cm">دانشگاه آزاد اسلامی <br> واحد نجف آباد</div>
            <div class="flex-33" style="margin-top: 1cm">
                <img class="" src="{{ asset('assets/images/img.png') }}" alt="" style="width: 20%">
            </div>
            <div class="flex-33" style="font-family: Sans; font-size: 12px; margin-top: 1cm">
                شماره : 1401/ص/21129 <br>
                تاریخ : {{ verta()->now()->formatJalaliDate() }}
            </div>
        </header>

        <section style="margin-top: 1cm ;font-size: 12px">
            
            
            <ul class="ul1">
                <li>
                    <span> دانشجو مسئوليت كامل حسن انجام كار در طول مدت اجراي قرارداد را دارد و موظف است طبق برنامه اعلامی در محل تعيين شده توسط حامي حضور یابد. </span>
                </li>
                <li>
                    <span> دانشجوي پویشي موظف به رعایت مفاد قرارداد خود با حامي است و لازم است به لحاظ حضور به موقع، انجام مسئوليت هایي كه پذیرفته است و امانتداري در زمينه حفظ دارایي هاي سخت افزاري، نرم افزاري و اطلاعات محرمانه حامي مقيد باشد و مسئوليت حقوقي هر گونه بي توجهي به عهده خود او است و دانشگاه در این رابطه هيچگونه مسئوليتي ندارد. </span>
                </li>
                <li>
                    <span> دانشجوي پویشي ملزم به رعایت تمامي مقررات، بخشنامه ها و دستور العمل هاي دانشگاه و حامي مي باشد. </span>
                </li>
                <li>
                    <span> قوانين حقوقي حاكم بر دانشجوي پویشي همانند قوانين حاكم بر دانشجویان كارآموزي و كارورزي است و در صورت عدم رعایت مقررات دانشگاه و حامي،مسئوليت هر حادثه ناشي از بي توجهي به عهده دانشجوي پویشي است. </span>
                </li>
                <li>
                    <span> در صورت گزارش حامي مبني برعدم رعایت انضباط لازم با وجود تذكرات، واحد دانشگاهي مي تواند دانشجو را از طرح خارج و وي را از مزایاي این طرح محروم كند. </span>
                </li>
                <li>
                    <span> گواهینامه طرح پویش تنها به دانشجویی اعطا می شود که دوره را به طور کامل با موفقیت بگذراند و حامی از عملکرد ایشان رضایت داشته باشد. </span>
                </li>
                <li>
                    <span> در صورت بروز هرگونه مشکلی در محیط کار و یا تحمیل خواسته های خارج از عرف و چهارچوب در محیط کارآموزی، لازم است موضوع در اسرع وقت به واحد دانشگاهی اطلاع داده شود تا نسبت به اصلاح وضعیت و یا برخورد لازم از سوی واحد دانشگاهی اقدام شود. </span>
                </li>
                <li>
                    <span> دانشجو متعهد به رعایت موازین اخلاقی و شئونات اسلامی، ظوابط آموزشی و قوانین ایمنی و اجرایی حاکم بر محل فعالیت تحت نظر حامی می باشد. </span>
                </li>
                <li>
                    <span> با توجه به اینکه مطابق با زمان مقرر در این قرارداد دانشجو به منظور طی نموندن مدت کارآموزی مطابق با نظر دانشگاه در محل تعیین شده توسط حامی حضور دارد، با استناد به نامه شماره 29094 مورخ 1399/07/20 اداره کل تعاون، کار و رفاه اجتماعی استان اصفهان و نامه شماره 290/99/71420 مورخ 1399/12/23 اداره کل تامین اجتماعی استان اصفهان، روابط فیمابین دانشجو و حامی مشمول قوانین کار و تامین اجتماعی نمی باشد. بر همین اساس دانشجو و یا خانواده وی حق هرگونه شکایت در خصوص ادعای دریافت حقوق و مزایای مرتب در قانون کار را از خود سلب می نماید و چنانچه به موجب شکایت دانشجو و یا خانواده وی، حامی متحمل پرداخت جریمه و یا محکومیت ریالی گردد، دانشجو شخصا متعهد به جبران خسارت وارده خواهد بود. </span>
                </li>
                <li>
                    <span> شایان ذکر است دانشجو در طی زمان گذراندن کارآموزی پویش مشمول بیمه حوادث دانشجویی از طرف دانشگاه می باشد. </span>
                </li>
            </ul>

            <h5 style="margin-bottom: 10px;"> ماده6) قانون حاکم بر قرارداد </h5>

            <span> این قرارداد بر اساس ماده 10 قانون مدنی منعقد گردیده است و طرفین قرار داد متعهد و ملزم به اجاری کلیه مفاد قرارداد می باشند و شروط ضمن العقد برای طرفین لازم الاجرا می باشدو درکلیه موارد که این قرارداد پیش بینی و لحاظ نگردیده است، اصول کلی حقوقی حاکم برعقد، معتبر و لازم الاجرا خواهد بود. </span>

            <h5 style="margin-bottom: 10px;"> ماده7) فسخ قرارداد </h5>

            <span> چنانچه هرکدام از طرفین قرارداد به تعهدات خود عمل نکنند، طرف مقابل با اطلاع ناظر قرارداد امکان فسخ قرارداد را دارد. </span>

            <h5 style="margin-bottom: 10px;"> ماده8) نسخ قرارداد </h5>

            <span> این قرارداد در 8 ماده و یک تبصره در 3 نسخه تنظیم و به امضای طرفین رسیده است و تمام نسخ آن حکم واحد دارند. </span>
        </section>

        <footer>
            <div class="flex-33">
                حامی <br> {{ $company->name }} {{ $company->family }}
            </div>
            <div class="flex-33" style="position: relative">
                <img src="{{ asset('assets/signatures/shane.png') }}" style="position: absolute;width: 100%;top:-55px;left:5px" alt="">
                <span style="z-index: 10">ناظر <br> دكتر مهدی شانه <br> معاون آموزشی و مهارتی</span>
            </div>
            <div class="flex-33">
                کارآموز <br> {{ $student->name }} {{ $student->family }}
            </div>
        </footer>

    </div>
</body>
</html>

<script src="{{ asset('assets/swal2/swal2.all.min.js') }}"></script>

<script>
     window.print();
</script>