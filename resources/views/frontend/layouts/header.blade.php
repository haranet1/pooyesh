
<header class="pxp-header fixed-top d-print-none">
    <div class="pxp-container">
        <div class="pxp-header-container ">
            <div class="pxp-logo d-flex align-items-center">
                <a href="{{route('home')}}" style="font-size: 15px; width:195px" class="pxp-animate d-md-block d-none my-2">
                    <img src="{{asset('assets/panel/images/brand/logo.svg')}}" 
                         alt="logo">
                </a>
                <div class="pxp-nav-trigger navbar d-xl-none d-block flex-fill">
                    <a role="button" data-bs-toggle="offcanvas" data-bs-target="#pxpMobileNav"
                       aria-controls="pxpMobileNav">
                        <div class="pxp-line-1"></div>
                        <div class="pxp-line-2"></div>
                        <div class="pxp-line-3"></div>
                    </a>
                    <div class="offcanvas offcanvas-start pxp-nav-mobile-container" tabindex="-1" id="pxpMobileNav">
                        <div class="offcanvas-header">
                            <div class="pxp-logo ">
                                <a href="{{route('home')}}" class="pxp-animate" style="width:70px !important;">
                                    <img src="{{asset('assets/panel/images/brand/logo.svg')}}" style=" height:70px !important"
                                         alt="logo">
                                </a>
                            </div>
                            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                                    aria-label="Close"></button>
                        </div>

                        <div class="offcanvas-body">
                            <nav class="pxp-nav-mobile">
                                <ul class="navbar-nav justify-content-end flex-grow-1">
                                    
                                    <!--<li class="nav-item res-nav dropdown">
                                        <a role="button" class="nav-link dropdown-toggle"
                                           data-bs-toggle="dropdown">جستجوی شغل</a>
                                        <ul class="dropdown-menu">
                                            <li class="pxp-dropdown-header">لیست شغل</li>
                                            <li class="nav-item"><a href="{{route('jobs.list')}}">جستجوی برتر با کارت</a>
                                            </li>
                                            <li class="pxp-dropdown-header">تک شغل</li>
                                        </ul>
                                    </li>
                                    <li class="nav-item res-nav dropdown">
                                        <a role="button" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">کارفرما / کارجو</a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item dropdown">
                                                <a role="button" class="nav-link dropdown-toggle"
                                                   data-bs-toggle="dropdown">کارفرمایان</a>
                                                <ul class="dropdown-menu">
                                                    <li class="nav-item"><a href="{{route('companies.list')}}">لیست</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a role="button" class="nav-link dropdown-toggle"
                                                   data-bs-toggle="dropdown">کارجویان </a>
                                                <ul class="dropdown-menu">
                                                    <li class="nav-item">
                                                        <a href="{{route('candidates.list')}}">لیست</a>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>
                                    </li>-->
                                    
                                    {{-- <li class="nav-item res-nav dropdown">
                                        <a role="button" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">پروژه
                                            ها</a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item dropdown">
                                                <a role="button" class="nav-link dropdown-toggle"
                                                   data-bs-toggle="dropdown">پروژه ها</a>
                                                <ul class="dropdown-menu">
                                                    <li class="nav-item"><a href="{{route('projects.list')}}"> لیست پروژه
                                                            ها</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a role="button" class="nav-link dropdown-toggle"
                                                   data-bs-toggle="dropdown">پروژه</a>
                                                <ul class="dropdown-menu">
                                                    <li class="nav-item"><a href="#">نمایش پروژه
                                                        </a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li> --}}
                                    {{-- <li class="nav-item res-nav"><a href="{{route('academic-guidance.form')}}">هدایت تحصیلی ویژه داوطلبان کنکور</a></li> --}}
                                    
                                    <li class="nav-item res-nav dropdown">
                                        <a role="button" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">آزمون های شخصیت شناسی</a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item dropdown">
                                                <a href="{{route('mbti.index')}}" role="button" class="nav-link">تست شخصیت شناسی MBTI</a>
                                                <a href="{{route('haaland.index')}}" role="button" class="nav-link">تست رغبت شناسی هالند</a>
                                                <a href="{{route('enneagram.index')}}" role="button" class="nav-link">تست شخصیت شناسی انیاگرام</a>
                                                <a href="{{route('neo.index')}}" role="button" class="nav-link">تست شخصیت شناسی NEO</a>
                                                <a href="{{route('ghq.index')}}" role="button" class="nav-link">تست سلامت GHQ</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-item res-nav"><a href="{{route('companies.list')}}">شرکت ها</a></li>
                                    <li class="nav-item res-nav"><a href="{{route('jobs.list')}}">موقعیت های شغلی</a></li>
                                    <li class="nav-item res-nav"><a href="{{route('about')}}">درباره ما</a></li>
                                    <li class="nav-item res-nav"><a href="{{route('faqs')}}">سوالات متداول</a></li>
                                    <li class="nav-item res-nav"><a href="{{route('contact')}}">تماس با ما</a></li>

                                    @guest
                                        <li class="nav-item res-nav"><a href="{{route('login')}}">ورود</a></li>
                                        <li class="nav-item res-nav"><a href="{{route('register')}}">ثبت نام</a></li>
                                    @endguest
                                    @auth
                                        @if(\Illuminate\Support\Facades\Auth::user()->hasRole('student') || \Illuminate\Support\Facades\Auth::user()->hasRole('applicant'))
                                            <li class="nav-item res-nav"><a  href="{{route('std.panel')}}">داشبورد</a></li>
                                        @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('company'))
                                            <li class="nav-item res-nav"><a  href="{{route('company.panel')}}">داشبورد</a></li>
                                        @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('employee'))
                                            <li class="nav-item res-nav"><a  href="{{route('employee.panel')}}">داشبورد</a></li>
                                        @endif
                                        <li>
                                            <form action="{{route('logout')}}" method="POST">
                                                @csrf
                                                <button class="btn btn-sm btn-warning mt-3" type="submit">خروج</button>
                                            </form>
                                        </li>
                                    @endauth

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <a href="{{route('home')}}" style="font-size: 15px; width:70px" class="pxp-animate d-md-none d-block">
                    <img height="50" src="{{asset('assets/panel/images/brand/logo-2.svg')}}" style="max-height: 70px" class="img-fluid"
                         alt="logo">
                </a>
            </div>
            <nav class="pxp-nav dropdown-hover-all d-none d-xl-block">
                <ul>
                    {{-- <li class="dropdown">
                        <a href="{{route('projects.list')}}" class="dropdown-toggle">پروژه ها</a>
                    </li> --}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown">آزمون های شخصیت شناسی</a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{route('mbti.index')}}">تست MBTI</a></li>
                            <li><a class="dropdown-item" href="{{route('haaland.index')}}">رغبت سنج هالند</a></li>
                            <li><a class="dropdown-item" href="{{route('enneagram.index')}}">تست انیاگرام</a></li>
                            <li><a class="dropdown-item" href="{{route('neo.index')}}">تست نئو</a></li>
                            <li><a class="dropdown-item" href="{{route('ghq.index')}}">تست سلامت GHQ</a></li>
                        </ul>
                    </li>
                    {{-- <li><a class="dropdown-toggle" href="{{route('test.salamat')}}">آزمون سلامت</a></li> --}}
                    <li><a class="dropdown-toggle" href="{{route('companies.list')}}">شرکت ها</a></li>
                    <li><a class="dropdown-toggle" href="{{route('jobs.list')}}">موقعیت‌ها</a></li>
                    <li><a class="dropdown-toggle" href="{{route('about')}}">درباره ما</a></li>
                    <li><a class="dropdown-toggle" href="{{route('faqs')}}">سوالات متداول</a></li>
                    <li><a class="dropdown-toggle" href="{{route('contact')}}">تماس با ما</a></li>
                    {{-- <li><a class="dropdown-toggle" href="{{route('blog')}}">مجله</a></li> --}}
                    {{-- <li class="bg-light rounded">
                        <a class="dropdown-toggle" href="{{route('academic-guidance.form')}}">هدایت تحصیلی ویژه داوطلبان کنکور</a>
                    </li> --}}
                </ul>
            </nav>
            <nav class="pxp-user-nav responsive  d-none d-sm-flex">
                @guest
                <a class="btn rounded-pill pxp-nav-btn mx-2"
                   href="{{route('login')}}" role="button">ورود</a>
                <a class="btn rounded-pill pxp-nav-btn mx-2"
                   href="{{route('register')}}" role="button">ثبت نام</a>
                @else
                <div class="pxp-dashboard-side-user-nav">
                    <div class="dropdown pxp-dashboard-side-user-nav-dropdown dropup">
                        <a role="button" class="dropdown-toggle" data-bs-toggle="dropdown">
                            <div class="pxp-dashboard-side-user-nav-name btn rounded-pill pxp-nav-btn">
                                <span>
                                    {{\Illuminate\Support\Facades\Auth::user()->name." ".\Illuminate\Support\Facades\Auth::user()->family}}
                                </span>
                            </div>
                        </a>
                        <ul class="dropdown-menu ">
                            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('student') || \Illuminate\Support\Facades\Auth::user()->hasRole('applicant'))
                                <li><a class="dropdown-item" href="{{route('std.panel')}}">داشبورد</a></li>
                            @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('company'))
                                <li><a class="dropdown-item" href="{{route('company.panel')}}">داشبورد</a></li>
                            @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('employee'))
                                <li><a class="dropdown-item" href="{{route('employee.panel')}}">داشبورد</a></li>
                            @endif
                            <li>
                                <form action="{{route('logout')}}" method="POST">
                                    @csrf
                                    <button class="dropdown-item" type="submit">خروج</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
                @endguest
            </nav>
        </div>
    </div>
</header>
