@extends('frontend.layout.master')
@section('meta')
    <meta property="og:url" content="https://jobist.ir/jobs"/>
    <meta name="description" content="مرجع دسته‌بندی شغلی جابیست با ۲۲ گروه شغلی متنوع و استانداردهای شغلی o-net. اطلاعات کامل درباره مسیر شغلی و انتخاب بهترین شغل متناسب با توانایی‌ها و علایق شما." />
    <meta property="og:title" content="دسته‌بندی‌های شغلی در جابیست | راهنمای مشاغل و استانداردهای" />
    <meta property="og:description" content="کشف کنید که در کدام دسته‌بندی شغلی جای می‌گیرید. با استفاده از استانداردهای شغلی o-net، شغل متناسب با مهارت‌ها و علاقه‌های خود را بیابید و مسیر شغلی موفقی را آغاز کنید." />
    
    <link rel="canonical" href="https://jobist.ir/jobs"/>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/frontend/css/jobs.css')}}"/>
@endsection
@section('title' , 'استاندارد شغلی')
@section('page' , 'جابیست')
@section('main')
<div class="container-fluid ">
        <div class="row align-items-center justify-content-center py-5 bg-jobs">

            <div class="col-md-7 ">
                <div class="p-4 rounded-3 text-white bg-blue shadow-f">
                    <h1 class="h1 mb-3 ">استاندارد شغلی جابیست</h1>
                    <p class="fw-normal my-4 h6 " align="justify">
                        استاندارد شغلی ONET (Occupational Information Network) یک پایگاه داده جامع و معتبر است که اطلاعات مربوط به مشاغل مختلف را ارائه می‌دهد. این استاندارد توسط وزارت کار ایالات متحده ایجاد شده و شامل توصیف مهارت‌ها، توانایی‌ها، وظایف، و دانش مورد نیاز برای هر شغل است. ONET ابزاری مفید برای کارجویان، کارفرمایان و مشاوران شغلی است که به آن‌ها کمک می‌کند تا مسیرهای شغلی مناسب را شناسایی کنند، نیازهای بازار کار را درک کنند و انتخاب‌های آگاهانه‌تری داشته باشند. این استاندارد همچنین برای تحلیل شغل و توسعه منابع انسانی کاربرد گسترده‌ای دارد.
                    </p>
                    <div class="d-flex align-items-center justify-content-end gap-3">
                        <a href="{{route('jobs.list')}}" class="btn bg-yellow button2 fw-bold mt-2">جستجوی آگهی های شغلی</a>
                        <a href="{{route('job.category', ['category' => ''])}}" class="btn bg-green text-white button3 fw-bold mt-2">لیست مشاغل</a>
                    </div>
                    
                </div>

            </div>



        </div>
</div>
<div class="container mt-5">
    <div class="mb-5 p-4 border-dash-purple2 lh-p bg-white">
        <h3 class="title z-0">استاندارد شغلی ONET (Occupational Information Network)</h3>
        <div class="text-container">
            <p class="fs-18">
                اطلاعات مربوط به بیش از هزار شغل مختلف را پوشش می‌دهد. این اطلاعات شامل توصیف دقیق وظایف شغلی، مهارت‌ها و توانایی‌های مورد نیاز، دانش مرتبط، ابزارها و فناوری‌های مورد استفاده در هر حرفه، شرایط محیط کاری، ویژگی‌های شخصیتی و الزامات آموزشی است. این پایگاه داده، با دسته‌بندی مشاغل بر اساس معیارهای علمی و عملی، به کارجویان، دانشجویان، کارفرمایان و مشاوران شغلی امکان می‌دهد تا به‌راحتی اطلاعات مفیدی درباره مشاغل مورد نظر خود به دست آورند.
            </p>

            <p class="fs-18">
                یکی از ویژگی‌های برجسته ONET، استفاده از یک چارچوب استاندارد به نام ONET Content Model است. این مدل، ساختاری برای تحلیل شغل ایجاد می‌کند که شامل شش بخش اصلی می‌باشد: الزامات کاری، ویژگی‌های کارگر، تجربه‌های شغلی، الزامات آموزشی، فعالیت‌های شغلی و عوامل محیطی. این چارچوب، دیدگاه جامعی از هر شغل ارائه می‌دهد و باعث می‌شود کاربران بتوانند مهارت‌ها و نیازهای خود را با الزامات شغل مقایسه کنند.
            </p>                            
            <div class="row align-items-center">
                <div class="col-md-7 col-12">
                    <h3 class="title mt-4 z-0">کاربردهای استاندارد شغلی</h3>
                    <p class="fs-18">
                        کاربردهای استاندارد شغلی ONET بسیار گسترده است. این پایگاه داده به کارجویان کمک می‌کند تا مسیر شغلی مناسب خود را شناسایی کنند و فرصت‌های شغلی را بر اساس علاقه و توانایی‌هایشان ارزیابی کنند. همچنین، کارفرمایان می‌توانند از اطلاعات ONET برای طراحی شغل، تدوین شرح وظایف و جذب نیروی انسانی استفاده کنند. مشاوران شغلی نیز می‌توانند از این منبع برای ارائه مشاوره دقیق‌تر به مراجعان خود بهره بگیرند.  
                    </p>
                    <p class="fs-18">
                        در نهایت، استاندارد شغلی O-NET به‌عنوان یکی از منابع پیشرو در تحلیل شغل و بازار کار، نقش مهمی در تسهیل ارتباط میان نیروی کار و کارفرمایان ایفا می‌کند و به بهبود بهره‌وری و توسعه نیروی انسانی کمک می‌کند.
                    </p>
                </div>
                <div class="col-md-5 col-12">
                    <img src="{{asset('assets/frontend/img/jobs/Resume.svg')}}" alt="رزومه کارجو">
                </div>
            </div>

        

        </div>
        
    </div>


    <div class="mt-4">
        <h3 class="main-title text-center h2 mx-auto">دسته بندی شغل ها</h3>
        <div class="row row-cols-2 row-cols-lg-4 g-md-5 g-2 mt-3">
            @foreach ($categories as $category)
            <div class="col">
                <a href="{{route('job.category', ['category' => $category->id])}}">
                    <div class="bg-white box-shadow rounded-4 p-sm-3 p-2 h-100">
                        <div class="d-flex flex-column align-items-center">
                            <img src="{{ asset($category->photo->path) }}" class="card-img text-center h-100 py-3" loading="lazy" width="153" height="153" alt="دسته بندی شغل {{$category->title}}">
                            <h3 class="blue text-center fs-5">{{ $category->title }}</h3>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>







</div>
@endsection