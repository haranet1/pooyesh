@extends('frontend.layout.master')
@section('meta')
    <meta name="description" content="اطلاعات کامل درباره موقعیت شغلی، شامل شرح وظایف، شرایط احراز، مزایا و فرصت‌های شغلی مرتبط. با آگاهی از جزئیات این شغل، تصمیم‌گیری بهتری برای آینده شغلی خود داشته باشید." />
    <meta property="og:url" content="https://jobist.ir/jobs/{{$job->id}}"/>
    <meta property="og:title" content=" اطلاعات شغل | جابیست " />
    <meta property="og:description" content="اطلاعات کامل درباره موقعیت شغلی، شامل شرح وظایف، شرایط احراز، مزایا و فرصت‌های شغلی مرتبط. با آگاهی از جزئیات این شغل، تصمیم‌گیری بهتری برای آینده شغلی خود داشته باشید." /> 
    <link rel="canonical" href="https://jobist.ir/jobs/{{$job->id}}"/>
@endsection
@section('title' , $job->title)
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/job-single.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/single-candidate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/jobs.css') }}">
@endsection

@section('main')

    <div class="container">
        <div class="Rectangl-2 position-relative rounded-4">
            <div class="mt-5 position-relative Rectangl-1 z-3 bg-white rounded-4">
                <div class="bg-image-top position-relative z-3 box-shadow-c rounded-4 p-4 description-box d-flex align-items-center">
                    <div class="d-flex justify-content-between align-items-start z-3 flex-md-row flex-column-reverse">
                        <div class="">
                            <h2 class="mb-2 blue fs-3 fw-bold">{{ $job->title }}</h2>
                            <p class="mb-3 fs-14 badge badge-custom bg-info"> گروه شغلی 
                                <strong class="blue2">{{ $job->category->title }}</strong>

                            </p>
                            
                            
                                <p class="text fs-18">
                                    {{$job->description}}
                                </p>
  
                        </div>

                    </div>

                </div>
            </div>
        </div>



        <div class="border-dash-purple mt-10 p-4">
            <h3 class="title-job fs-4">شرح شغل (استاندارد)</h3>
            <div class="Rectangl-3">
                <div class="p-md-5 p-2 box-shadow-c rounded-4 position-relative z-3 bg-white">
                        <h3 class=" fw-bold text-primary fs-5">استاندارد :</h3>
                        <p class="text fs-5 lh-lg">
                            @php
                                echo $job->description;
                            @endphp
                        </p>
                </div>
            </div>
        </div>


            @if ($job->tasks->count() > 0)
                <div class="border-dash-purple2 mt-10 p-3">
                    <div class="d-flex title-box2">
                        <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                        <h3 class="title-job2 fs-4">وظایف مورد انتظار</h3>
                    </div>
                    <div class="Rectangl-4 position-relative">
                        <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                            <div class="row align-items-center row-gap-1 fix-width-wrraper">
                                @foreach ($job->tasks as $task)
                                    @if ($loop->index < 5)
                                        <div class="row justify-content-center align-items-md-center">
                                            <p justify class="text fs-5 progress-title fanumber"> {{ $loop->index + 1 }} . {{ $task->title }} </p>
                                            
                                        </div>
                                    @endif
                                        
                                @endforeach
                                <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapsetasks" aria-expanded="false">موارد بیشتر</span>
                                <div class="collapse" id="collapsetasks">
                                    @foreach ($job->tasks as $task)
                                        @if ($loop->index >= 5)
                                            <div class="row justify-content-center align-items-md-center">
                                                <p justify class="text fs-5 progress-title fanumber"> {{ $loop->index + 1 }} . {{ $task->title }} </p>
                                                
                                            </div>
                                        @endif
                                        
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        

        <div class="row">


                @if ($skills->count() > 0)
                    <div class="col-12 col-md-6">
                        <div class="border-dash-purple2 mt-10 p-3">
                            <div class="d-flex title-box2">
                                <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                                <h3 class="title-job2 fs-4">مهارت های مورد نیاز</h3>
                            </div>
                            <div class="Rectangl-4 position-relative">
                                <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                                    <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap mb-4">
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-excellent ms-1"></div>
                                            <span class="fs-5 fw-medium excellent">عالی</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-good ms-1"></div>
                                            <span class="fs-5 fw-medium good">خوب</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-fair ms-1"></div>
                                            <span class="fs-5 fw-medium fair">متوسط</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <div class="dot-poor ms-1"></div>
                                            <span class="fs-5 fw-medium poor">ضعیف</span>
                                        </div>
                                    </div>
                                    <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                                        
                                        <div class="d-flex flex-column w-100">
                                            @foreach ($skills as $skill)
                                                @if ($loop->index < 5)
                                                <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                    <h4 class="text fs-5 progress-title">{{ $skill->skillname->title }}</h4>
                                                    <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح مهارت مورد نیاز">
                                                        <div class="progress-bar @if($skill->level->data_value >= 4) bg-excellent
                                                            @elseif($skill->level->data_value >= 3)
                                                                bg-good
                                                            @elseif($skill->level->data_value >= 2)
                                                                bg-fair
                                                            @elseif($skill->level->data_value >= 0)
                                                                bg-poor
                                                            @endif
                                                        " style="width: @if($skill->level->data_value >= 4) 100%
                                                            @elseif($skill->level->data_value >= 3)
                                                                75%
                                                            @elseif($skill->level->data_value >= 2)
                                                                50%
                                                            @elseif($skill->level->data_value >= 0)
                                                                25%
                                                            @endif
                                                        "></div>
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach
                                            <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseskill" aria-expanded="false">موارد بیشتر</span>
                                            <div class="collapse" id="collapseskill">
                                                @foreach ($skills as $skill)
                                                    @if ($loop->index >= 5)
                                                        <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                            <h4 class="text fs-5 progress-title">{{ $skill->skillname->title }}</h4>
                                                            <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح مهارت مورد نیاز">
                                                                <div class="progress-bar @if($skill->level->data_value >= 4) bg-excellent
                                                                    @elseif($skill->level->data_value >= 3)
                                                                        bg-good
                                                                    @elseif($skill->level->data_value >= 2)
                                                                        bg-fair
                                                                    @elseif($skill->level->data_value >= 0)
                                                                        bg-poor
                                                                    @endif
                                                                " style="width: @if($skill->level->data_value >= 4) 100%
                                                                    @elseif($skill->level->data_value >= 3)
                                                                        75%
                                                                    @elseif($skill->level->data_value >= 2)
                                                                        50%
                                                                    @elseif($skill->level->data_value >= 0)
                                                                        25%
                                                                    @endif
                                                                "></div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($knowledges->count() > 0)
                    <div class="col-12 col-md-6">
                        <div class="border-dash-purple2 mt-10 p-3">
                            <div class="d-flex title-box2">
                                <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                                <h3 class="title-job2 fs-4">دانش مورد نیاز</h3>
                            </div>
                            <div class="Rectangl-4 position-relative">
                                <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                                    <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap mb-4">
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-excellent ms-1"></div>
                                            <span class="fs-5 fw-medium excellent">عالی</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-good ms-1"></div>
                                            <span class="fs-5 fw-medium good">خوب</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-fair ms-1"></div>
                                            <span class="fs-5 fw-medium fair">متوسط</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <div class="dot-poor ms-1"></div>
                                            <span class="fs-5 fw-medium poor">ضعیف</span>
                                        </div>
                                    </div>
                                    <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                                        <div class="d-flex flex-column w-100">
                                            @foreach ($knowledges as $knowledge)
                                                @if ($loop->index < 5)
                                                    <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                        <h4 class="text fs-5 progress-title ">{{ $knowledge->knowledgeName->title }}</h4>
                                                        <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح دانش مورد نیاز">
                                                            <div class="progress-bar
                                                                @if($knowledge->level->data_value >= 4)
                                                                    bg-excellent
                                                                @elseif($knowledge->level->data_value >= 3)
                                                                    bg-good
                                                                @elseif($knowledge->level->data_value >= 2)
                                                                    bg-fair
                                                                @elseif($knowledge->level->data_value >= 0 )
                                                                    bg-poor
                                                                @endif
                                                            " style="width:
                                                                @if($knowledge->level->data_value >= 4)
                                                                    100%
                                                                @elseif($knowledge->level->data_value >= 3)
                                                                    75%
                                                                @elseif($knowledge->level->data_value >= 2)
                                                                    50%
                                                                @elseif($knowledge->level->data_value >= 0 )
                                                                    25%
                                                                @endif
                                                            "></div>
                                                        </div>
                                                    </div>
                                                @endif
                                               
                                            @endforeach
                                            <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseknowledge" aria-expanded="false">موارد بیشتر</span>
                                            <div class="collapse" id="collapseknowledge">
                                                @foreach ($knowledges as $knowledge)
                                                @if ($loop->index >= 5)
                                                    <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                        <h4 class="text fs-5 progress-title ">{{ $knowledge->knowledgeName->title }}</h4>
                                                        <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح دانش مورد نیاز">
                                                            <div class="progress-bar
                                                                @if($knowledge->level->data_value >= 4)
                                                                    bg-excellent
                                                                @elseif($knowledge->level->data_value >= 3)
                                                                    bg-good
                                                                @elseif($knowledge->level->data_value >= 2)
                                                                    bg-fair
                                                                @elseif($knowledge->level->data_value >= 0 )
                                                                    bg-poor
                                                                @endif
                                                            " style="width:
                                                                @if($knowledge->level->data_value >= 4)
                                                                    100%
                                                                @elseif($knowledge->level->data_value >= 3)
                                                                    75%
                                                                @elseif($knowledge->level->data_value >= 2)
                                                                    50%
                                                                @elseif($knowledge->level->data_value >= 0 )
                                                                    25%
                                                                @endif
                                                            "></div>
                                                        </div>
                                                    </div>
                                                @endif
                                               
                                            @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                @if ($abilities->count() > 0)
                    <div class="col-12 col-md-6">
                        <div class="border-dash-purple2 mt-10 p-3">
                            <div class="d-flex title-box2">
                                <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                                <h3 class="title-job2 fs-4">توانایی ها مورد نیاز</h3>
                            </div>
                            <div class="Rectangl-4 position-relative">
                                <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                                    <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap mb-4">
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-excellent ms-1"></div>
                                            <span class="fs-5 fw-medium excellent">عالی</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-good ms-1"></div>
                                            <span class="fs-5 fw-medium good">خوب</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-fair ms-1"></div>
                                            <span class="fs-5 fw-medium fair">متوسط</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <div class="dot-poor ms-1"></div>
                                            <span class="fs-5 fw-medium poor">ضعیف</span>
                                        </div>
                                    </div>
                                    <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                                        <div class="d-flex flex-column w-100">
                                            @foreach ($abilities as $ability)
                                                @if ($loop->index < 5)
                                                    <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                        <h4 class="text fs-5 progress-title">{{ $ability->abilityName->title }}</h4>
                                                        <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح توانایی مورد نیاز">
                                                            <div class="progress-bar
                                                                @if($ability->level->data_value >= 4)
                                                                    bg-excellent
                                                                @elseif($ability->level->data_value >= 3)
                                                                    bg-good
                                                                @elseif($ability->level->data_value >= 2)
                                                                    bg-fair
                                                                @elseif($ability->level->data_value >= 0 )
                                                                    bg-poor
                                                                @endif
                                                            " style="width:
                                                                @if($ability->level->data_value >= 4)
                                                                    100%
                                                                @elseif($ability->level->data_value >= 3)
                                                                    75%
                                                                @elseif($ability->level->data_value >= 2)
                                                                    50%
                                                                @elseif($ability->level->data_value >= 0 )
                                                                    25%
                                                                @endif
                                                            "></div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseability" aria-expanded="false">موارد بیشتر</span>
                                            <div class="collapse" id="collapseability">
                                                @foreach ($abilities as $ability)
                                                @if ($loop->index >= 5)
                                                    <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                        <h4 class="text fs-5 progress-title">{{ $ability->abilityName->title }}</h4>
                                                        <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح توانایی مورد نیاز">
                                                            <div class="progress-bar
                                                                @if($ability->level->data_value >= 4)
                                                                    bg-excellent
                                                                @elseif($ability->level->data_value >= 3)
                                                                    bg-good
                                                                @elseif($ability->level->data_value >= 2)
                                                                    bg-fair
                                                                @elseif($ability->level->data_value >= 0 )
                                                                    bg-poor
                                                                @endif
                                                            " style="width:
                                                                @if($ability->level->data_value >= 4)
                                                                    100%
                                                                @elseif($ability->level->data_value >= 3)
                                                                    75%
                                                                @elseif($ability->level->data_value >= 2)
                                                                    50%
                                                                @elseif($ability->level->data_value >= 0 )
                                                                    25%
                                                                @endif
                                                            "></div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                @if ($job->workStyles->count() > 0)
                    <div class="col-12 col-md-6">
                        <div class="border-dash-purple2 mt-10 p-3">
                            <div class="d-flex title-box2">
                                <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                                <h3 class="title-job2 fs-5">ویژگی های فردی در محیط کاری</h3>
                            </div>
                            <div class="Rectangl-4 position-relative">
                                <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">

                                    <div class="row align-items-center row-gap-3 fix-width-wrraper">
                                        @foreach ($workStyles as $workStyle)
                                                @if ($loop->index < 6)
                                                <div class="col-6">
                                                    <div class="row justify-content-center  mb-3 align-items-md-center">
                                                        <h4 class="text fs-5 progress-title"> {{ $workStyle->workStyleName->title }} </h4>
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach
                                    </div>
                                       
                                        <div class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseworkStyle" aria-expanded="false">موارد بیشتر</div>
                                        <div class="collapse" id="collapseworkStyle">
                                            <div class="row align-items-center row-gap-3 fix-width-wrraper">
                                                @foreach ($workStyles as $workStyle)
                                                    @if ($loop->index >= 6)
                                                    
                                                        <div class="col-6">
                                                            <div class="row justify-content-center  mb-3 align-items-md-center">
                                                                <h4 class="text fs-5 progress-title"> {{ $workStyle->workStyleName->title }} </h4>
                                                            </div>
                                                        </div>
                                                    
                                                    @endif
                                                
                                                @endforeach
                                            </div>
                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                @endif



                @if ($technologySkills->count() > 0)
            
                    <div class="border-dash-purple2 mt-10 p-3">
                        <div class="d-flex title-box2">
                            <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                            <h3 class="title-job2 fs-4">مهارت های فنی مورد نیاز</h3>
                        </div>
                        <div class="Rectangl-4 position-relative">
                            <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                                <div class="row align-items-center row-gap-3 fix-width-wrraper">
                                    @foreach ($technologySkills as $technologySkill)
                                        @if ($loop->index < 6)
                                        <div class="col-6">
                                            <div class="row justify-content-center  mb-3 align-items-md-center">
                                                <h4 class="text fs-5 progress-title"> {{ $technologySkill->technologySkillName->title }} </h4>
                                                @if (!is_null($technologySkill))
                                                    <p class=""> مثال : {{ $technologySkill->example->example ?? '' }} </p>
                                                @endif
                                            </div>
                                        </div>
                                        @endif
                                
                                      
                                    @endforeach
                                    <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapsetechnologySkill" aria-expanded="false">موارد بیشتر</span>
                                    <div class="collapse" id="collapsetechnologySkill">
                                        <div class="row align-items-center row-gap-3 fix-width-wrraper">
                                        @foreach ($technologySkills as $technologySkill)
                                            @if ($loop->index >= 6)
                                                <div class="col-6">
                                                    <div class="row justify-content-center  mb-3 align-items-md-center">
                                                        <h4 class="text fs-5 progress-title"> {{ $technologySkill->technologySkillName->title }} </h4>
                                                        @if (!is_null($technologySkill))
                                                            <p class=""> مثال : {{ $technologySkill->example->example ?? '' }} </p>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

        </div>

        
        
    </div>

@endsection


