@extends('frontend.layout.master')
@section('meta')
    <meta name="description" content="با جابیست، به جدیدترین آگهی‌های استخدام و کارآموزی در سال ۱۴۰۳ دسترسی پیدا کنید. اینجا جایی است که فرصت شغلی، استخدام، کارآموزی، فرصت کار، و آگهی استخدام روزانه به شما ارائه می‌شود. شروع کنید و راهی بهترین شغل برای خودتان را پیدا کنید." />
    <meta property="og:url" content="http://127.0.0.1:8000/category/job/"/>
    <meta property="og:title" content=" فرصت‌های شغلی جابیست|جدیدترین آگهی‌های استخدام و کارآموزی 1403" />
    <meta property="og:description" content=" با جابیست، به جدیدترین آگهی‌های استخدام و کارآموزی در سال ۱۴۰۳ دسترسی پیدا کنید. اینجا جایی است که فرصت شغلی، استخدام، کارآموزی، فرصت کار، و آگهی استخدام روزانه به شما ارائه می‌شود. شروع کنید و راهی بهترین شغل برای خودتان را پیدا کنید. " /> 
    <link rel="canonical" href="http://127.0.0.1:8000/category/job/"/>
@endsection
@section('title' , $title )
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/jobs-search.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/jobs.css') }}">
@endsection

@section('main')

    <div class="container-fluid dot-bg">
        <div class="container border-dash-s pb-2 px-2">
            <div class="bg-blue p-4 rounded-bottom-5">
                <div class="row row-gap-2 mt-3 py-1 column-gap-4 flex-wrap justify-content-center">

                        <div class="col-12 col-lg-4 input-g rounded-pill bg-white">
                            <div class="align-self-center">
                                <img src="{{ asset('assets/frontend/img/search-jobs/layer-group-solid.svg') }}" 
                                    class="img-fluid w-90" alt="گروه شغلی" height="22" width="22">
                            </div>
                            <div class="position-relative select-icon px-1 rounded-pill">
                                <form action="{{ route('job.category') }}" method="get" id="searchForm">
                                <select id="category" name="category" 
                                        class="select-custom greys bg-white form-select">
                                    <option selected value="">گروه شغلی</option>
                                    @foreach ($categories as $category)
                                        @if(isset($oldData) && !is_null($oldData['category']))
                                            @if ($oldData['category'] == $category->id)
                                                <option selected value="{{ $category->id }}">{{ $category->title }}</option>
                                            @else
                                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                            @endif
                                            
                                        @else
                                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                                        @endif
                                    
                                    @endforeach
                                </select>
                                </form>
                            </div>
                        </div>
                    

                    <div class="col-12 col-lg-4 input-g rounded-pill bg-white">
                        <div class="align-self-center">
                            <img src="{{ asset('assets/frontend/img/search-jobs/search.svg') }}" 
                                 class="img-fluid w-90" alt="عنوان شغل" height="24" width="24">
                        </div>
                        <input type="search" id="search-input" name="jobTitle" 
                            placeholder="عنوان شغل" 
                            class="form-select select-custom greys bg-white input rounded-pill">
                    </div>
                </div>
            </div>
        </div> 
    </div>


<div class="mt-4">
    <div class="container border-dash2 p-4 mt-5">
        <div class="row align-items-stretch" id="searchResults">
            @foreach ($jobs as $job)
            <div class="col-lg-6 col-12">
                <a href="{{route('jobs.single',$job->id)}}" class="pointer">
                    <div class="box-shadow-c pb-md-0 bg-white box-hover rounded-4 mb-4 border-blue position-relative">
                        <div class="bg-halfc-2 py-md-4 py-3 pb-4 px-md-3 px-2">
                            <div class="d-flex align-items-start">
                                <div class="mb-3 mb-md-0">
                                    <div class="bg-gr rounded-4 cat-img">
                                        <img src="{{ asset($job->category->photo->path) }}" class="w-100 img-fluid" alt="{{ $job->title }}" height="47" width="60">
                                    </div>
                                </div>
                                <div class="px-2">
                                    <h5 class="h5 fw-bold blue fs-sm-18">{{ Str::limit($job->title,50 ,'...') }}</h5>
                                    <p class="fs-6 text-grey fs-sm-14 mb-2">{{ Str::limit($job->description,100 ,'...') }}</p>
                                </div>
    
                            </div>
                        </div>
                        <div class="position-absolute job-btn bg-blue text-white text-center rounded-4">
                            مشاهده
                        </div>
                    </div>
                </a>

            </div>
            @endforeach

            {{-- {{$jobs->links('pagination.frontend')}} --}}
        </div>
        <div id="pagination" class="d-flex justify-content-center align-items-center py-4 gap-3 flex-nowrap overflow-auto">
           
        </div>
    </div>

</div>

@endsection

@section('script')

<script>
    const allJobs = {!! json_encode($allJobs->map(function ($job) {
        return [
            'id' => $job->id,
            'title' => $job->title,
            'description' => $job->description,
            'categoryPhotoPath' => asset($job->category->photo->path),
            'url' => route('jobs.single', $job->id),
        ];
    })->toArray()) !!};
    console.log(allJobs);
    
    
</script>

    <script src="{{ asset('assets/frontend/js/jobs.js') }}"></script>
@endsection