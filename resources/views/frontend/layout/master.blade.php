<!DOCTYPE html>
<html lang="fa-IR" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#585859">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta property="og:site_name" content="جابیست"/>

    @yield('meta')

    <link rel="preload" href="{{asset('assets/frontend/fonts/webfonts/Vazirmatn-Regular.woff2')}}" as="font" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" href="{{asset('assets/frontend/fonts/webfonts/Vazirmatn-Medium.woff2')}}" as="font" type="font/woff2" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/Vazirmatn-font-face.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/all.min.css') }}">

    @yield('css')
    
    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}"/>
    <link rel="apple-touch-icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}" type="image/x-icon"/>

    


    <title>@yield('page') | @yield('title')</title>
</head>
<body class="bg-full">
    <div class="container-fluid px-0">
        <!-- header -->

        @include('frontend.layout.header')

        <main>
            @yield('main')
        </main>
        

       

            <!-- footer -->

        {{-- @include('frontend.layout.footer') --}}

    </div>

    <input type="hidden" id="CSRF" value="{{ csrf_token() }}">
    <script src="{{ asset('assets/frontend/js/jquery-3.6.0.min.js') }}" ></script>
    <script src="{{ asset('assets/frontend/js/bootstrap.bundle.min.js') }}" ></script>
    <script src="{{ asset('assets/frontend/js/persianumber.min.js') }}" defer></script>
    <script src="{{ asset('assets/frontend/js/main.js') }}" defer ></script>
    @yield('script')
</body>
</html>