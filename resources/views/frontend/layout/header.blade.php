<div class="sticky-top">
    <a href="{{ route('mbti.index') }}" class="top-header d-flex p-sm-1 justify-content-around align-items-center" aria-label="انجام تست ام بی تی ای">
        <div class="w-50 z-3">
            <div class="d-flex z-3 justify-content-between align-items-center px-lg-5 py-1 flex-lg-row flex-column">
                <button class="btn bg-yellow rounded-pill button2 first_button blue fw-bold d-lg-block d-none">الان تست بده</button>
                <div class="fw-bold z-3 fs-18 text-white text-center px-1">
                    شغل مخصوص خودتو کشف کن
               </div>
            </div>
        </div>
        <div class="w-50 z-3">
            <div class="d-flex z-3 justify-content-between align-items-center px-lg-5 py-1 flex-lg-row flex-column">
                <div class="fw-bold z-3 fs-18 text-center blue px-1">
                    رشته تحصیلی مخصوص خودتو پیدا کن
               </div>
               <button class="btn bg-blue text-white button rounded-pill second_button fw-bold d-lg-block d-none">الان تست بده</button>
            </div>
        </div>

    </a>
    <header class="text-white d-flex justify-content-center">
        <nav class="navbar navbar-expand-md header col navbar-dark height-nav align-items-center">
            <div class="me-xl-5 me-lg-4 me-3 ms-1 ms-md-3 my-sm-1 mt-1 mb-sm-3 mb-1">
                <a href="{{ route('home') }}" aria-label="home page">
                    <img class="logo-main" src="{{ asset('assets/frontend/img/home/jobist-wo.svg') }}" alt="logo" width="150" height="44">
                </a>
            </div>

            <div class="d-flex d-md-none">
                <div class="d-flex ms-xl-5 ms-lg-4 ps-0">
                    @auth
                    <a href="{{ route('dashboard') }}" class="profile d-flex flex-row align-items-center padding rounded-pill text-white mx-1">
                        <p class="mb-0 h6 ms-sm-2 head-btn user-name">{{ Auth::user()->name }} {{ Auth::user()->family }}</p>
                        <i class="fa-solid fa-user align-self-center h4 my-0 p-1"></i>
                    </a>
                    <form action="{{ route('logout') }}" class="d-md-flex align-items-center d-none" method="POST">
                        @csrf
                        <button type="submit" class="profile d-flex flex-row align-items-center padding rounded-pill text-white mx-1 border border-0">
                            <p class="mb-0 h6 ms-sm-2 head-btn">خروج</p>
                            <i class="fa-solid fa-right-from-bracket align-self-center h5 my-0 p-1"></i>
                        </button>
                    </form>
                    
                    @else
                    <a href="{{ route('login') }}" class="profile d-flex flex-row align-items-center padding rounded-pill text-white mx-1">
                        <p class="mb-0 h6 ms-sm-2 head-btn">ورود</p>
                        <i class="fa-solid fa-user align-self-center h4 my-0 p-1"></i>
                    </a>
                    <a href="{{ route('register') }}" class="profile d-flex flex-row align-items-center padding rounded-pill text-white mx-1">
                        <p class="mb-0 h6 ms-sm-2 head-btn">ثبت نام</p>
                        <i class="fa-solid fa-pen-to-square align-self-center h4 my-0 p-1"></i>
                    </a>
                    @endauth
                </div>
                <button class="navbar-toggler p-0 border-unset ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-label="toggle button">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse margin-menu" id="navbar">
                <ul class="navbar-nav col d-flex justify-content-between pe-0 pb-0">
                    <li class="nav-item dropdown d-md-block d-none">
                        <a href="{{ route('std.exams') }}" class="nav-link h3 mb-0 dropdown-toggle pointer d-md-block d-none {{ Route::is('std.exams') ? 'active-link' : '' }}" 
                            data-bs-hover="dropdown" aria-expanded="false" aria-label="menu dropdown">تست شخصیت شناسی</a>
                        <ul class="dropdown-menu header-dm">
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('mbti.index')}}">شخصیت شناسی MBTI</a></li>
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('neo.index')}}">شخصیت شناسی NEO</a></li>
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('haaland.index')}}">رغبت شناسی haaland (هالند)</a></li>
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('enneagram.index')}}">شخصیت شناسی Enneagram (ایناگرام)</a></li>
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('ghq.index')}}">سلامت عمومی GHQ</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown d-md-none d-block">
                        <a href="{{ route('std.exams') }}" class="nav-link h3 mb-0 dropdown-toggle pointer d-md-none d-block {{ Route::is('std.exams') ? 'active-link' : '' }}" 
                            data-bs-toggle="dropdown" aria-expanded="false" aria-label="menu dropdown">تست شخصیت شناسی</a>
                        <ul class="dropdown-menu header-dm">
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('std.exams')}}">لیست تست ها</a></li>
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('mbti.index')}}">شخصیت شناسی MBTI</a></li>
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('neo.index')}}">شخصیت شناسی NEO</a></li>
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('haaland.index')}}">رغبت شناسی haaland (هالند)</a></li>
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('enneagram.index')}}">شخصیت شناسی Enneagram (ایناگرام)</a></li>
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{route('ghq.index')}}">سلامت عمومی GHQ</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link h3 mb-0 {{ Route::is('standard.index') ? 'active-link' : '' }}" href="{{ route('standard.index') }}">کار شناسی</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link h3 mb-0 {{ Route::is('companies.list') ? 'active-link' : '' }}" href="{{ route('companies.list') }}">شرکت‌ها</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link h3 mb-0 {{ Route::is('jobs.list') ? 'active-link' : '' }}" href="{{ route('jobs.list') }}">آگهی‌های شغلی</a>
                    </li>

                    <li class="nav-item dropdown d-md-block d-none">
                        <a class="nav-link h3 mb-0 dropdown-toggle d-md-block d-none {{ Route::is(['articles.blog','article.blog.search','category.blog.search']) ? 'active-link' : '' }}" 
                            href="{{ route('articles.blog') }}" data-bs-hover="dropdown" aria-expanded="false" aria-label="menu dropdown">مجله</a>
                        <ul class="dropdown-menu header-dm">
                            @foreach ($categories as $category)
                                <li><a class="dropdown-item header-item text-end mb-1" href="{{ route('category.blog.search', $category->slug) }}">{{$category->title}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="nav-item dropdown d-md-none d-block">
                        <a href="{{ route('articles.blog') }}" class="nav-link h3 mb-0 dropdown-toggle d-md-none d-block {{ Route::is(['articles.blog','article.blog.search','category.blog.search']) ? 'active-link' : '' }}" 
                        data-bs-toggle="dropdown" aria-expanded="false" aria-label="menu dropdown">مجله</a>
                        <ul class="dropdown-menu header-dm">
                            <li><a class="dropdown-item header-item text-end mb-1" href="{{ route('articles.blog') }}">همه مقاله‌ها</a></li>
                            @foreach ($categories as $category)
                                <li><a class="dropdown-item header-item text-end mb-1" href="{{ route('category.blog.search', $category->slug) }}">{{$category->title}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    @auth
                    <li class="nav-item">
                        <form action="{{ route('logout') }}" class="d-flex d-md-none align-items-center" method="POST">
                            @csrf
                            <button type="submit" class="profile d-flex flex-row align-items-center padding rounded-pill mb-2 text-white border border-0">
                                <p class="mb-0 h6 ms-sm-2 head-btn">خروج</p>
                                <i class="fa-solid fa-right-from-bracket align-self-center h4 my-0 p-1"></i>
                            </button>
                        </form>
                    </li>
                    @endauth
                </ul>
            </div>

            <div class="d-none d-md-flex">
                <div class="d-flex ms-xl-5 ms-lg-4 ps-0">
                    @auth
                    <div class="dropdown">
                        <a href="{{ route('dashboard') }}" class="profile d-flex flex-row align-items-center padding rounded-pill text-white mx-1 dropdown-toggle" data-bs-hover="dropdown" aria-expanded="false">
                            <p class="mb-0 h6 ms-sm-2 head-btn user-name">{{ Auth::user()->name }} {{ Auth::user()->family }}</p>
                            <i class="fa-solid fa-user align-self-center h5 my-0 p-1"></i>
                        </a>
                        <ul class="dropdown-menu header-dm">
                            <li>
                                <a class="dropdown-item header-item text-end mb-1 d-flex align-items-center" href="{{ route('dashboard') }}">
                                    <i class="fa-solid fa-table-cells-large align-self-center h6 my-0 p-1"></i>
                                    <p class="mb-0 h6 ms-sm-2 head-btn">داشبورد</p>
                                </a>
                            </li>
                            <li>
                                <form action="{{ route('logout') }}" class="d-md-flex align-items-center d-none" method="POST">
                                    @csrf
                                    <button type="submit" class="dropdown-item header-item text-end mb-1 d-flex align-items-center">
                                        <i class="fa-solid fa-right-from-bracket align-self-center h6 my-0 p-1"></i>
                                        <p class="mb-0 h6 ms-sm-2 head-btn">خروج</p>
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </div>

                    @else
                    <a href="{{ route('login') }}" class="profile d-flex flex-row align-items-center padding rounded-pill text-white mx-1">
                        <p class="mb-0 h6 ms-sm-2 head-btn">ورود</p>
                        <i class="fa-solid fa-user align-self-center h5 my-0 p-1"></i>
                    </a>
                    <a href="{{ route('register') }}" class="profile d-lg-flex flex-row align-items-center padding rounded-pill text-white mx-1 d-none">
                        <p class="mb-0 h6 ms-sm-2 head-btn">ثبت نام</p>
                        <i class="fa-solid fa-pen-to-square align-self-center h5 my-0 p-1"></i>
                    </a>
                    @endauth
                </div>
                <button class="navbar-toggler p-0 border-unset ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
    </header>
</div>
