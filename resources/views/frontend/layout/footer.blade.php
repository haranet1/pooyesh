<div class="container-fluid p-0 footer">
    <div class="text-center"><a href="#" aria-label="back to top">
        <img src="{{ asset('assets/frontend/img/home/icons8-back-26.png') }}" loading="lazy" class="up-icon" alt="برو به اول صفحه"></a>
    </div>
    <footer class="bg-pattern mt-auto">
        
        <div class="text-center">
            <img src="{{ asset('assets/frontend/img/home/hg.png') }}" loading="lazy" class="footer-svg position-absolute top-0" alt="footer curve">
        </div>

        <div class="container">
            <div class="pt-5 text-sm-end text-center">
                <h2 class="text-white">{{ env('APP_NAME') }}</h2>
            </div>
            <div class="row row-cols-lg-4 row-cols-sm-2 row-cols-1 mt-2 g-4">
                <div class="col">
                    <p class="light-grey fs-7 text-justify footer-text">
                        جابیست به عنوان آکادمیک‌ترین پلتفرم استخدام، کاریابی و کارآموزی با ارائه فرصت های شغلی مبتنی بر ویژگی های شخصیتی شروع به فعالیت کرد.  
                        جابیست با اصول حرفه ای، فرد را در مسیر رسیدن به شغل ایده آل خود یاری می‌دهد.  
                        پیشنهاد تخصصی شغل در جابیست با استاندارد جهانی o-net و امکان ایجاد فرصت کارآموزی در شرکت های مطرح، رویای شغلی شما را به واقعیت تبدیل می‌کند.                    </p>
                </div>
                <div class="col">
                    <div class="pe-lg-4">
                       <p class="text-white fw-normal mb-2 text-sm-end text-center h4">دسترسی های سریع</p>
                        <ul class="list-unstyled p-0 light-grey text-sm-end text-center">
                            <li class="mb-2"><a href="{{ route('jobs.list') }}">جست و جوی شغل</a></li>
                            <li class="mb-2"><a href="{{ route('companies.list') }}">لیست شرکت ها</a></li>
                            <li class="mb-2"><a href="{{ route('faq.h') }}">سوالات متداول</a></li>
                            <li class="mb-2"><a href="{{ route('aboutus') }}">درباره ما</a></li>
                            <li class="mb-2"><a href="{{ route('contactus.h') }}">تماس با ما</a></li>
                            <li class="mb-2"><a href="{{ route('articles.blog') }}">بلاگ</a></li>
                        </ul> 
                    </div>
                </div>
                <div class="col">
                    <p class="text-white fw-normal mb-2 text-sm-end text-center h4">اطلاعات تماس</p>
                    <ul class="list-unstyled p-0 light-grey text-sm-end text-center">
                        <li class="mb-2">
                            <i class="fa-solid fa-phone-volume ms-1 light-grey fs-6"></i>
                            <span>۰۳۱-۴۲۲۹۳۳۰۵</span>
                        </li>
                        <li class="mb-2">
                            <i class="fa-regular fa-envelope ms-1 light-grey fs-6"></i>
                            <span>info@jobist.ir</span>
                        </li>
                        <li class="mb-2">
                            <i class="fa-solid fa-location-dot ms-1 light-grey fs-6"></i>
                            <span class="text-justify">اصفهان، نجف آباد، بلوار دانشگاه،<br> دانشگاه آزاد اسلامی واحد نجف آباد،<br> پارک علم و فناوری، پلاک ۲۰۵</span>
                        </li>
                    </ul>
                </div>
                <div class="col my-auto">
                
                    <div class="d-flex p-2 rounded-3 gap-3 justify-content-center">
                        <div class="rounded-3">
                            <img class="rounded-3 bg-white" style="width: 110px;height: 110px;cursor:pointer;" referrerpolicy='origin' id ='rgvjjzpewlaojxlzesgtapfu' onclick="window.open('https://logo.samandehi.ir/Verify.aspx?id=374105&p=xlaojyoeaodsrfthobpddshw', 'Popup', 'toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30')" alt ='logo-samandehi' src ='https://logo.samandehi.ir/logo.aspx?id=374105&p=qftiyndtshwlnbpdlymaujyn' />
                        </div>
                        <div class="rounded-3">
                            <a class="d-inline-block rounded-3 bg-white" referrerpolicy='origin' target='_blank' style="width: 110px;height: 110px;" href='https://trustseal.enamad.ir/?id=518419&Code=qTqmURGRjCej5gLa3DLaC9XkPrHrq9o8'><img class="d-inline-block object-fit-contain rounded-3" referrerpolicy='origin' src='https://trustseal.enamad.ir/logo.aspx?id=518419&Code=qTqmURGRjCej5gLa3DLaC9XkPrHrq9o8' alt='' style='cursor:pointer;max-width:110px;width: 110px;height: 110px;' code='qTqmURGRjCej5gLa3DLaC9XkPrHrq9o8'></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-white mt-5 rounded-4 py-2">
                <div class="d-flex justify-content-between align-items-center flex-lg-row flex-column">
                    <p class="text-muted card-text mb-0 me-3">تمامی حقوق مادی و معنوی برای موسسه کاریابی اینترنتی جابیست محفوظ است.</p>
                    <div class="ms-lg-5 mt-lg-0 mt-3">
                        <a aria-label="linkdin socialmedia" href="https://www.linkedin.com/company/haranet/" target="_blank"><img src="{{ asset('assets/frontend/img/home/linkdin.png') }}" loading="lazy" class="footer-logo" id="linkdin" alt="linkdin icon" height="38" width="38"></a>
                        {{-- <img src="{{ asset('assets/frontend/img/home/twitter.png') }}" class="footer-logo" id="twitter"> --}}
                        <a aria-label="instagram socialmedia" href="https://www.instagram.com/haranet.ir?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw==" target="_blank"><img src="{{ asset('assets/frontend/img/home/instagram.png') }}" loading="lazy" class="footer-logo" id="instagram" alt="instagram icon" height="38" width="38"></a>
                        {{-- <img src="{{ asset('assets/frontend/img/home/facebook.png') }}" class="footer-logo" id="facebook"> --}}
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mt-3 flex-sm-row flex-column align-items-center">
                <p class="text-primary fs-6 ms-2 h5">طراحی و اجرای سامانه: </p>
                <a href="https://haranet.ir" target="_blank" class="light-grey h5 fs-6">شرکت آرتین نوآفرین حرا (حرانت)</a>
            </div>
        </div>
    </footer>
</div>