@extends('frontend.layout.master')

@section('meta')
    <meta name="description"  content=" صفحه سوالات متداول جابیست، به پرسش‌های شایع در مورد استخدام، کاریابی و مسایل مرتبط پاسخ داده می‌شود. اینجا مکانی است که می‌توانید راهنمایی‌هایی برای پیشروی در مسیر حرفه‌ای خود پیدا کنید." />
    <meta property="og:url" content="https://jobist.ir/FAQ"/>
    <meta property="og:title" content="سوالات متداول" />
    <meta property="og:description" content=" صفحه سوالات متداول جابیست، به پرسش‌های شایع در مورد استخدام، کاریابی و مسایل مرتبط پاسخ داده می‌شود. اینجا مکانی است که می‌توانید راهنمایی‌هایی برای پیشروی در مسیر حرفه‌ای خود پیدا کنید." /> 
    <link rel="canonical" href="https://jobist.ir/FAQ"/>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/faq.css') }}">
@endsection


@section('title' , 'راهنمایی برای مسیر حرفه‌ای شما')
@section('page' , 'پاسخ به سوالات متداول در جابیست' )

@section('main')
<div class="container">
    <div class="d-flex justify-content-between heading mb-2">
        <h1 class="mt-5 h2">سوالات متداول</h1>
        <img src="{{ asset('assets/frontend/img/faq/faq.webp') }}" class="mt-5 image" alt="سوالات متداول" height="140" width="180">
    </div>
    <div class="mb-3">
        <button class="accordion-custom py-2 rounded-3 acc-shadow">
            <p class="mb-0 me-4 header-acc h4">
                جابیست، چه مزیتی نسبت به دیگر سایتهای کاریابی و استخدامی دارد؟
            </p>
            <div class="plus-icon">
                <i class="fa-solid fa-plus plus"></i>
            </div>
        </button>
        <div class="panel rounded-bottom-4 acc-shadow">
            <p class="mt-3 fs-5 fw-medium acc-text">
                جابیست برتریهای زیادی نسبت ب دیگر سایت های کاریابی دارد؛ از جمله، پایگاه داده گسترده از فرصت های
                شغلی و ابزارهای پیشرفته برای ارتباط بین کارفرما و کارجو
            </p>
        </div>
    </div>
    <div class="mb-3">
        <button class="accordion-custom py-2 rounded-3 acc-shadow">
            <p class="mb-0 me-4 header-acc h4">
               چگونه می‌توانم جستجوی موقعیت شغلی مرتبط با تخصص خود را در این سایت انجام دهم؟
            </p>
            <div class="plus-icon">
                <i class="fa-solid fa-plus plus"></i>
            </div>
        </button>
        <div class="panel rounded-bottom-4 acc-shadow">
            <p class="mt-3 fs-5 fw-medium acc-text">
                جستجوی موقعیت شغلی مرتبط با تخصص شما در جابیست بسیار آسان است؛ با استفاده از فیلترهای متنوع می‌توانید به سرعت به موقعیت‌های دقیقاً مورد نیاز خود دست یابید.
            </p>
        </div>
    </div>
    <div class="mb-3">
        <button class="accordion-custom py-2 rounded-3 acc-shadow">
            <p class="mb-0 me-4 header-acc h4">
                چگونه می‌توانم پیگیری وضعیت درخواست شغلی خود را انجام دهم؟
            </p>
            <div class="plus-icon">
                <i class="fa-solid fa-plus plus"></i>
            </div>
        </button>
        <div class="panel rounded-bottom-4 acc-shadow">
            <p class="mt-3 fs-5 fw-medium acc-text">
                پیگیری وضعیت درخواست شغلی خود در جابیست بسیار ساده است؛ از طریق پنل کاربری خود، قسمت استخدام درخواست های همکاری می‌توانید به راحتی وضعیت درخواست خود را پیگیری کرده و به روزرسانی‌ها را مشاهده کنید.
            </p>
        </div>
    </div>
    <div class="mb-3">
        <button class="accordion-custom py-2 rounded-3 acc-shadow">
            <p class="mb-0 me-4 header-acc h4">
             آیا سایت شما ابزارهایی برای بررسی و ارزیابی کارجوها را دارد؟
            </p>
            <div class="plus-icon">
                <i class="fa-solid fa-plus plus"></i>
            </div>
        </button>
        <div class="panel rounded-bottom-4 acc-shadow">
            <p class="mt-3 fs-5 fw-medium acc-text">
                بله، جابیست ابزارهای متعددی برای بررسی و ارزیابی کارجو ها از جمله تست های شخصیت شناسی دارد.
            </p>
        </div>
    </div>
    <div class="mb-3">
        <button class="accordion-custom py-2 rounded-3 acc-shadow">
            <p class="mb-0 me-4 header-acc h4">
            چگونه می‌توان یک موقعیت شغلی جدید را به سایت شما اضافه کرد؟
            </p>
            <div class="plus-icon">
                <i class="fa-solid fa-plus plus"></i>
            </div>
        </button>
        <div class="panel rounded-bottom-4 acc-shadow">
            <p class="mt-3 fs-5 fw-medium acc-text">
                برای اضافه کردن یک موقعیت شغلی جدید به جابیست، کافیست به پنل مدیریتی خود وارد شوید و از قسمت  استخدام ، لیست موقعیت های شغلی جدید را انتخاب کرده ؛ روی گزینه ثبت موقعیت جدید کلیک و موقعیت خود را ثبت کنید.
            </p>
        </div>
    </div>
    

</div>

@endsection

@section('script')
    <script src="{{ asset('assets/frontend/js/faq.js') }}"></script>
@endsection