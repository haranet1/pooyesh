<div class="container">
    <section class="mt-5">
        <div class="row">
            <div class="col-12 d-flex flex-sm-row flex-column justify-content-between">
                <h2 class="h3 title">دسته بندی های شغلی</h2>
                <div class="mt-md-5 mt-2">
                    <a href="{{ route('jobs.list') }}"><span class="term">مشاهده همه</span></a>
                    <i class="fa-solid fa-chevron-left grey"></i>
                </div>
            </div>
        </div>
        <div class="row row-cols-2 row-cols-lg-4 g-md-5 g-2 mt-3">
            @foreach ($categories as $category)
                <div class="col">
                    <div class="bg-white box-shadow rounded-4 p-3 h-100">
                        <div class="d-flex flex-column align-items-center">
                            <img src="{{ asset($category->photo->path) }}" class="card-img text-center h-100 py-3" loading="lazy" width="153" height="153" alt="دسته بندی شغل {{$category->title}}">
                            <h3 class="blue text-center h4">{{ $category->title }}</h3>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
</div>