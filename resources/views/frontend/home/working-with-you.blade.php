<div class="container">
    <section class="section-3">

        <div class="row">
            <div class="col-12 d-flex flex-sm-row flex-column justify-content-between">
                <h2 class="h3 title">افتخار ما همکاری با شماست</h2>
                <div class="mt-md-5 mt-2">
                    <a href="{{ route('companies.list') }}"><span class="term">مشاهده همه</span></a>
                    <i class="fa-solid fa-chevron-left grey"></i>
                </div>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-lg-3 row-cols-sm-2 mt-5 row-gap-5">

            @foreach ($companies as $company)
                <div class="col px-md-5 px-3">
                    <a href="{{ route('company.single' , $company->user->id) }}" class="bg-circles position-relative">
                            <div class="d-flex py-1 align-items-center box-shadow-company bg-white company-boxes box-hover gap-3 c-logo-img position-relative">
                                @isset($company->logo)
                                    <div class="logo-width-h rounded-circle overflow-hidden position-relative bg-white z-3">
                                        <img src="{{ asset($company->logo->path) }}" class="img-wh position-absolute p-1" loading="lazy" alt="لوگوی شرکت {{$company->name}}">
                                    </div>
                                @else
                                    <div class="logo-width-h rounded-circle overflow-hidden position-relative bg-white c-logo-img">
                                        <img src="{{ asset('assets/front/images/company-logo-1.png') }}" class="img-wh position-absolute" loading="lazy" alt="لوگوی شرکت {{$company->name}}">
                                    </div>
                                @endisset
                                <p class="fs-18 fw-semibold blue mb-0 fs-sm-15 h5">{{ $company->name }}</p>
                            </div>
                    </a>
                </div>


            @endforeach




            
        </div>
    </section>
</div>