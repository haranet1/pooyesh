
<div class="bg-pattern2 container-fluid mt-5">
    <div class="container">

        <div class="mt-3">
            <div class="d-flex justify-content-center">
                <h3 class="main-title h4 fw-bold">مسیر خودتو پیدا کن</h3>
            </div>
            <div class="row justify-content-between mt-5">
                <div class="col-md-4 padding-box home">
                    <div class="bg-white box-shadow-c d-flex align-items-center justify-content-center flex-column p-3 rounded-5">
                        <img src="{{ asset('assets/frontend/img/home/jobs.svg') }}" loading="lazy" class="image-size" width="203" height="203" alt="مسیر شغلی شما">
                        <h4 class="blue h5 mt-3 fw-bold">مسیر شغلی</h4>
                        <p class="text-grey-a text-center fs-5 mb-0">
                            با انجام تست و یافتن تیپ شخصیتی خود میتوانید بهترین 
                           <span class="blue fw-medium"> گزینه های شغلی</span>
                             برای خود را مشاهده کنید
                        </p>
                        <a href="{{route('mbti.index')}}" class="btn bg-blue text-white button fw-bold mt-2">انجام تست</a>
                    </div>
                </div>
                <div class="col-md-4 padding-box ptop path-box home my-auto" id="home">
                    <div class="d-flex align-items-center justify-content-center flex-column p-2 rounded-4 ">
                        <img src="{{ asset('assets/frontend/img/home/main-1.webp') }}" loading="lazy" class="img-fluid" width="305" height="305" alt="انتخاب مسیر زندگی">
                    </div>
                </div>
                <div class="col-md-4 padding-box home">
                    <div class="bg-white box-shadow-c d-flex align-items-center justify-content-center flex-column p-3 rounded-5">
                        <img src="{{ asset('assets/frontend/img/home/majors.svg') }}" loading="lazy" class="image-size" width="203" height="203" alt="مسیر تحصیلی شما">
                        <h4 class="blue h5 mt-3 fw-bold text-center">مسیر تحصیلی</h4>
                        <p class="text-grey-a text-center fs-5 mb-0">
                            با انجام تست و یافتن تیپ شخصیتی خود میتوانید بهترین 
                            <span class="blue fw-medium">رشته های تحصیلی</span>
                             برای خود را مشاهده کنید
                        </p>
                        <a href="{{route('mbti.index')}}" class="btn bg-blue text-white button fw-bold mt-2">انجام تست</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
