@extends('frontend.layout.master')

@section('meta')
    <meta name="description" content="جابیست به عنوان آکادمیک‌ترین پلتفرم استخدام، کاریابی و کارآموزی با ارائه فرصت های شغلی مبتنی بر ویژگی های شخصیتی شروع به فعالیت کرد." />
    <meta property="og:url" content="https://jobist.ir"/>
    <meta property="og:title" content=" جابیست |آکادمیک‌ترین پلتفرم استخدام، کاریابی و کارآموزی" />
    <meta property="og:description" content=" جابیست به عنوان آکادمیک‌ترین پلتفرم استخدام، کاریابی و کارآموزی با ارائه فرصت های شغلی مبتنی بر ویژگی های شخصیتی شروع به فعالیت کرد. " /> 
    <link rel="canonical" href="https://jobist.ir"/>
    <link rel="preload" href="{{asset('assets/frontend/img/home/sale-Overlay-min2.webp')}}" as="image" fetchpriority='high'>
    <link rel="preload" href="{{ asset('assets/frontend/img/home/image (3).webp') }}" as="image" fetchpriority='high'>
@endsection

@section('title' , 'استخدام، کاریابی و کارآموزی')
@section('page' , 'جابیست' )
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/about-us.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/owl.carousel.min.css') }}">
@endsection
@section('main')

    {{-- top section --}}
    @include('frontend.home.first-section')

    {{-- counter --}}
    @include('frontend.home.counter')

    {{-- last job --}}
    @include('frontend.home.last-job')

    {{-- mbti --}}
    @include('frontend.home.mbti-test')

    {{-- starter --}}
    @include('frontend.home.starter')

    {{-- working with you --}}
    @include('frontend.home.working-with-you')

    {{-- why jobist --}}
    @include('frontend.home.why-jobist')

    {{-- category --}}
    @include('frontend.home.category')

@endsection

@section('script')
    <script src="{{ asset('assets/frontend/js/owl.carousel.min.js') }}" defer></script>
    <script src="{{ asset('assets/frontend/js/home.js') }}" defer></script>
@endsection