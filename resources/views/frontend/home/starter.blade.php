       
<div class="bg-blue-pt container-fluid mb-6 height-start px-0 py-4">
    <div class="container mt-md-5 mt-2">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h2 class="h3 text-white mb-4 starter-title">از کجا شروع کنم؟</h2>
            </div>
        </div>
    </div> 
    <div class="container">
        <div class="row row-cols-2 row-cols-lg-4 g-lg-2 g-2 move-down align-items-stretch">
            <div class="col">
                <div class="bg-white box-shadow rounded-4 p-md-3 p-2 h-100">
                    <div class="d-flex flex-column align-items-center h-100 justify-content-between">
                        <img src="{{ asset('assets/frontend/img/home/Untitled-2.webp') }}" class="card-img text-center" loading="lazy" width="170" height="170" alt="logo section">
                        <h3 class="blue text-center h4">ثبت نام در {{ env('APP_NAME') }}</h3>
                        <p class="green mt-4 align-self-end mb-0 ms-2 h5"><a class="green" href="{{ route('register') }}">ثبت نام</a></p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="bg-white box-shadow rounded-4 p-md-3 p-2 h-100">
                    <div class="d-flex flex-column align-items-center h-100 justify-content-between">
                        <img src="{{ asset('assets/frontend/img/home/Untitled-3.webp') }}" class="card-img text-center" loading="lazy" width="170" height="170" alt="logo section">
                        <h3 class="blue text-center h4">انجام تست و ساخت رزومه</h3>
                        <p class="green mt-4 align-self-end mb-0 ms-2 h5"><a class="green" href="{{ route('std.exams') }}">شروع تست</a></p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="bg-white box-shadow rounded-4 p-md-3 p-2 h-100">
                    <div class="d-flex flex-column align-items-center h-100 justify-content-between">
                        <img src="{{ asset('assets/frontend/img/home/Untitled-4.webp') }}" class="card-img text-center" loading="lazy" width="170" height="170" alt="logo section">
                        <h3 class="blue text-center h4">جست و جوی مشاغل</h3>
                        <p class="green mt-4 align-self-end mb-0 ms-2 h5"><a class="green" href="{{ route('jobs.list') }}">جست و جو</a></p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="bg-white box-shadow rounded-4 p-md-3 p-2 h-100">
                    <div class="d-flex flex-column align-items-center h-100 justify-content-between">
                        <img src="{{ asset('assets/frontend/img/home/Untitled-5.webp') }}" class="card-img text-center" loading="lazy" width="170" height="170" alt="logo section">
                        <h3 class="blue text-center h4">ارسال درخواست شغلی</h3>
                        <p class="green mt-4 align-self-end mb-0 ms-2 h5"><a class="green" href="{{ route('faq.h') }}">سوالات متداول</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
