<div class="container">
    <section class="mt-4">

        <div class="row">
            <div class="col-12 d-flex flex-sm-row flex-column justify-content-between">
                <h2 class="h3 title">آخرین پیشنهادات شغلی</h2>
                <div class="mt-md-5 mt-2">
                    <a href="{{ route('jobs.list') }}"><span class="term">مشاهده همه</span></a>
                    <i class="fa-solid fa-chevron-left grey"></i>
                </div>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-lg-3 row-gap-3 mt-3">

            @foreach ($lastJobPositions as $job)
                {{-- {{dd($job)}} --}}
                <div class="col p-3">
                    <div class="box-shadow-c pb-md-3 bg-darkgrey box-hover
                        @if($job->type == 'intern')
                            border-piece-2
                        @else
                            border-piece
                        @endif
                        ">
                        <div class=" p-3
                            @if($job->type == 'intern')
                                bg-halfc-2
                            @else
                                bg-halfc
                            @endif
                        ">
                            <div class="d-flex">
                                <img src="{{ asset('assets/frontend/img/company/heart-regular.png') }}" class="w-small me-auto" alt="like button" height="18" width="18">
                            </div>
                            <div class="d-flex align-items-center mb-4">
                                <div class="ms-3 px-2 py-3 rounded-4
                                    @if($job->type == 'intern')
                                        bg-gr-2
                                    @else
                                        bg-gr
                                    @endif
                                ">
                                @if($job->type == 'intern')
                                    <img src="{{ asset('assets/frontend/img/company/user-pen-solid.png') }}" class="job-img" alt="موقعیت کارآموزی" height="47" width="60">
                                @else
                                    <img src="{{ asset('assets/frontend/img/company/computer-solid.png') }}" class="job-img" alt="موقعیت شغلی" height="42" width="60">
                                @endif
                                   
                                </div>
                                <div class="d-flex flex-column">
                                    <span class="h5 fw-bold
                                        @if($job->type == 'intern')
                                            green-job
                                        @else
                                            blue
                                        @endif
                                    ">{{ $job->title }}</span>
                                    <span class="fs-6 text-grey">{{ $job->job->category->title }}</span>
                                </div>
                            </div>
                            <div class="">
                                @if ($job->province || $job->city)
                                    <div class="d-flex align-items-center">
                                        <img src="{{ asset('assets/frontend/img/company/earth-asia-solid.png') }}" class="w-small ms-1" alt="آیکون آدرس" height="18" width="18">
                                        <span class="grey ps-2 card-text">

                                            @if ($job->province)
                                                {{ $job->province->name }}
                                            @endif

                                            @if ($job->province && $job->city)
                                                ,
                                            @endif

                                            @if ($job->city)
                                                {{ $job->city->name }}
                                            @endif

                                        </span>
                                    </div>
                                @endif
                                <div class="">
                                    @if ($job->type)
                                    <span class="text-muted">{{ job_type_persian($job->type) }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="mt-md-3 mt-1 d-flex justify-content-between pt-1 align-items-sm-center flex-sm-row flex-column">
                                <div class="mb-sm-0 mb-1 d-flex flex-xl-row flex-md-column flex-row flex-wrap">
                                    <span class="text-grey fs-7 ms-1 fanumber">{{  verta($job->updated_at)->formatDifference() }} توسط</span>
                                    <strong class="text-dark">{{ $job->company->groupable->name }}</strong>
                                </div>
                                <a href="{{ route('job' , $job->id) }}" class="btn rounded-4 align-self-end text-dark fs-7 fw-medium
                                    @if($job->type == 'intern')
                                        cv-btn-2
                                    @else
                                        cv-btn
                                    @endif
                                    ">مشاهده اطلاعات</a>
                            </div>
                        </div>
                    </div>
                </div>








{{--                 
                <div class="col">
                    <div class="box-shadow p-4 pb-3 rounded-4 bg-white box-hover">
                        <div> --}}
                            {{-- <div class="red h6 me-3 mb-2">فوری</div> urgent --}}
                            {{-- <a href="{{ route('job' , $job->id) }}" class="d-flex align-items-center mb-3 text-description text-dark">
                                <img src="{{ asset('assets/frontend/img/home/submit (1).png') }}" class="job-img ms-3">
                                <div class="d-flex flex-column">
                                    <span class="h5 fw-semibold">{{ $job->title }}</span>
                                    <span class="fs-6">{{ $job->company->groupable->name }}</span>
                                </div>
                            </a>
                            <div class="me-5">
                                <div class="me-4">

                                    @if ($job->province || $job->city)
                                        @if ($job->province)
                                            <span class="grey ps-2 separate card-text">{{ $job->province->name }}</span>
                                        @endif
                                        @if ($job->city)
                                            <span class="grey ps-2 separate card-text">{{ $job->city->name }}</span>
                                        @endif
                                    @endif

                                    @if ($job->salary && ! is_null($job->salary))
                                        <span class="green pe-2 card-text fanumber">{{ $job->salary }}</span>
                                    @endif

                                </div>
                                <div class="me-4">
                                    @if ($job->type)
                                        <span class="text-muted">{{ job_type_persian($job->type) }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="mt-4 d-flex justify-content-between border-top border-secondary-subtle pt-3">
                            <span class="grey fanumber">{{ verta($job->updated_at)->formatDifference() }}</span>
                            <a href="{{ route('job' , $job->id) }}" class="btn cv-btn">اطلاعات بیشتر</a> --}}

                            {{-- @auth
                                @hasanyrole('student|applicant')
                                    <input type="hidden" id="sendCooperationRequestUrl" value="{{ route('front.ajax.cooperation-request') }}">
                                    <button class="btn cv-btn" id="sendRequest-{{$job->id}}" onclick="SendCooperationRequest({{ $job->id }})">ارسال رزومه</button>
                                @endhasallroles
                            @else
                                <input type="hidden" id="loginUrl" value="{{ route('login') }}">
                                <button class="btn cv-btn" id="loginSwal" onclick="guestUserToLoging()">اطلاعات بیشتر</button>
                            @endauth --}}

                        {{-- </div>
                    </div>
                </div> --}}



            @endforeach

        </div>

    </section>
</div>