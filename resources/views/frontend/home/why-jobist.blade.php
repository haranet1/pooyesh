<div class="container">
    <section class="section-4">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h2 class="h3 title">چرا {{ env('APP_NAME') }} ؟</h2>
            </div>
        </div>
        <div class="bg-blue-pt mt-5 rounded-5 py-5 pe-5 ps-2">
            <div class="row row-cols-1 row-cols-md-2 g-md-5 row-gap-4">
                <div class="col">
                    <div>
                        <div class="bg-yellow box-shadow square-box rounded-3 fs-2 blue fw-bold justify-content-center align-items-center">
                            <span>۱</span>
                        </div>
                        <h3 class="h3 text-white mt-3">استخدام تخصصی</h3>
                        <p class="light-grey card-text">شناسایی نیروی متخصص و با مهارت بر اساس نیاز شما</p>
                    </div>
                </div>
                <div class="col">
                    <div>
                        <div class="bg-yellow box-shadow square-box rounded-3 fs-2 blue fw-bold justify-content-center align-items-center">
                            <span>۲</span>
                        </div>
                        <h3 class="h3 text-white mt-3">آموزش مهارت محور</h3>
                        <p class="light-grey card-text">آموزش تخصصی مهارت های مورد نیاز به متقاضیان</p>
                    </div>
                </div>
                <div class="col">
                    <div>
                        <div class="bg-yellow box-shadow square-box rounded-3 fs-2 blue fw-bold justify-content-center align-items-center">
                            <span>۳</span>
                        </div>
                        <h3 class="h3 text-white mt-3">طرح پویش (کارآموزی)</h3>
                        <p class="light-grey card-text">اشتغال، همزمان با تحصیل</p>
                    </div>
                </div>
                <div class="col">
                    <div>
                        <div class="bg-yellow box-shadow square-box rounded-3 fs-2 blue fw-bold justify-content-center align-items-center">
                            <span>۴</span>
                        </div>
                        <h3 class="h3 text-white mt-3">رویداد ها و نمایشگاه ها</h3>
                        <p class="light-grey card-text">برگزاری رویداد های تخصصی جهت شناسایی بهترین ها برای شما</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>