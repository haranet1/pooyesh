<div class="container-fluid top-section">
    <section>
        <div class="row align-items-center">
            <div class="col-12 p-md-1 p-3 col-md-7 section-1">
                <div class="d-flex flex-row justify-content-center mt-md-5 mt-3 flex-wrap">
                    <p class="h1 ms-sm-3 blue fw-medium display-4">مهارت،</p>
                    <p class="h1 ms-sm-3 green fw-medium display-4">کارآموزی،</p>
                    <p class="h1 blue fw-medium display-4">اشتغال</p>
                </div>

                {{-- search --}}

                <form action="{{ route('job.home.search') }}" method="POST" class="d-flex flex-row justify-content-center mt-lg-5 mt-3" role="search">
                    @csrf
                        <input name="jobTitle" class="form-control ms-2 form-shadow rounded-pill py-2 border-input" type="search" placeholder="جست و جو کنید..." aria-label="Search">
                        <button class="button btn bg-blue rounded-circle btn-shadow" type="submit" aria-label="search button" style="width: 40px; height:40px">
                            <i class="fa-solid fa-magnifying-glass text-white align-self-center fs-6"></i>
                        </button>
                </form>

                <div class="d-flex flex-row flex-wrap gap-0 column-gap-3 border-brand bg-white rounded-5 py-3 px-md-5 px-3 mt-5 mx-md-5 mx-1 margin-brand justify-content-between">
                    <div id="carousel-home" class="owl-carousel">
                        <div>
                            <img src="{{ asset('assets/front/images/jan-snak-logo.webp') }}" class="logo-width align-self-center" width="82" height="24" alt="logo jan-snak">
                        </div>
                        <div>
                            <img src="{{ asset('assets/front/images/hamgam.webp') }}" class="logo-width align-self-center" width="82" height="20" alt="logo hamgam">
                        </div>
                        <div>
                            <img src="{{ asset('assets/front/images/arshak-logo.webp') }}" class="logo-width align-self-center" width="82" height="32" alt="logo arshak">
                        </div>
                        <div>
                            <img src="{{ asset('assets/front/images/haranet.webp') }}" class="logo-width align-self-center" width="82" height="21" alt="logo haranet">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5 p-0 main-img text-center">
                <img src="{{ asset('assets/frontend/img/home/image (3).webp') }}" class="img-fluid" fetchpriority="high" width="434" height="478" alt="شغل کاریابی">
            </div>
        </div>
    </section>
</div>