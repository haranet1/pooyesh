<div class="container-fluid bg-pattern">
    <section class="row row-cols-1 row-cols-sm-3 row-cols-md-5 row-gap-3 justify-content-center">
        <div class="col">    
            <div class="counter flex-fill devider pb-sm-0 pb-3">
                <span class="num" data-val="{{ $counts['companies'] }}">۰+</span>
                <span class="text-c">کارفرما</span>
            </div>
        </div>   
        <div class="col">
            <div class="counter flex-fill devider pb-sm-0 pb-3">
                <span class="num" data-val="{{ $counts['jobs'] }}">۰+</span>
                <span class="text-c">موقعیت شغلی</span>
            </div>
        </div> 
        <div class="col">
            <div class="counter flex-fill devider pb-sm-0 pb-3">
                <span class="num" data-val="{{ $counts['students'] }}">۰+</span>
                <span class="text-c">کارجو</span>
            </div>
        </div>
        <div class="col">
            <div class="counter flex-fill devider pb-sm-0 pb-3">
                <span class="num" data-val="{{ $counts['mbtis'] }}">۰+</span>
                <span class="text-c">تست MBTI</span>
            </div>
        </div>
        <div class="col">
            <div class="counter flex-fill">
                <span class="num" data-val="{{ $counts['enneagram'] }}">۰+</span>
                <span class="text-c">تست انیاگرام</span>
            </div>
        </div>    
    </section>
</div>