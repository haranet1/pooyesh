@extends('frontend.layout.master')

@section('meta')
    <meta name="description"  content=" با تست‌های شخصیتی در جابیست، ویژگی‌ها و نقاط قوت خود را شناسایی کنید. این تست‌ها به شما کمک می‌کنند تا در مسیر حرفه‌ای و شخصی خود پیشرفت کنید." />
    <meta property="og:title" content=" تست‌های شخصیتی در جابیست | ارزیابی و شناسایی ویژگی‌های شخصیتی"/>
    <meta property="og:url" content="https://jobist.ir/exams"/>
    <meta property="og:description" content=" با تست‌های شخصیتی در جابیست، ویژگی‌ها و نقاط قوت خود را شناسایی کنید. این تست‌ها به شما کمک می‌کنند تا در مسیر حرفه‌ای و شخصی خود پیشرفت کنید. " /> 
    <link rel="canonical" href="https://jobist.ir/exams"/>
@endsection


@section('title','ارزیابی و شناسایی ویژگی‌های شخصیتی')
@section('page' , 'جابیست')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')

<div class="bg-pattern container-fluid py-3">
    <div class="container">
        <div class="row align-items-center">

            <div class="col-md-8">
                <div class="p-3 rounded-4 text-white">
                    <h1 class="h1 mb-3 text-center">آزمون های شخصیت شناسی</h1>
                    <p class="fw-normal my-4 h6 text-center" align="justify">برای اینکه بتوانیم در مسیر رشد فردی حرکت کنیم، اولین قدم شناخت خود است. شناخت شخصیت، نقاط قوت و ضعف، علایق و توانایی ها می‌تواند به ما کمک کند تا مسیر زندگی و حرفه‌ای‌مان را بهتر درک کنیم. یکی از ابزارهای موثر برای این خودشناسی، تست‌های شخصیت‌شناسی است که بر اساس مطالعات روانشناختی طراحی شده‌اند.</p>
                </div>

            </div>

            <div class="col-md-4">
                <img src="{{asset('assets/frontend/img/tests/In-progress.webp')}}" alt="تست های شخصیت شناسی" class=" img-fluid" style="transform: scaleX(-1);">
            </div>

        </div>
    </div>
</div>
<div class="container-fliud section-test">
    <div class="container">
        <div class="row row-cards">
            <div class="col-lg-12 col-xl-12">
                <div class="p-lg-5 p-2">
                    <div class="row justify-content-center">
                        <div class="col-12">
    
                            <div class="mb-5 mt-3 p-4 border-dash-purple2 lh-p bg-white">
                                <h3 class="title z-0">راهی برای کشف دنیای درونی شما</h3>
                                <div class="text-container">
                                    <p class="fs-18">شخصیت هر فرد مجموعه‌ای پیچیده و منحصربه‌فرد از ویژگی‌ها، علاقه‌ها، و تمایلات است که او را از دیگران متمایز می‌کند. تست‌های شخصیت‌شناسی به شما کمک می‌کنند تا این ویژگی‌های پنهان و گاه ناشناخته را بشناسید. این تست‌ها بر اساس پژوهش‌های علمی طراحی شده‌اند و با تحلیل دقیق پاسخ‌های شما، تصویری روشن‌تر از شخصیتتان ارائه می‌دهند. از طریق این خودشناسی، می‌توانید با آگاهی بیشتری مسیر زندگی، شغل، و حتی روابط خود را انتخاب کنید.</p>
                                    <div class="hidden-content">
                                        <p class="fs-18">جابیست دارای چندین تست معتبر و پرطرفدار است که به‌طور گسترده در حوزه روان‌شناسی و توسعه فردی به کار گرفته می‌شوند مانند تست MBTI که نوع شخصیت و رفتارهای شما را تحلیل می‌کند. هر یک از این تست‌ها می‌توانند کمک کنند تا نقاط قوت و ضعف خود را بهتر بشناسید و به رشد و پیشرفت خود دست یابید. با انتخاب هر تست، شما یک قدم به شناخت عمیق‌تر از خود نزدیک‌تر خواهید شد.</p>                            
                                
                                        <h3 class="title mt-4 z-0">ارتباط خودشناسی و موفقیت</h3>
                                        <p class="fs-18">تست‌های شخصیت‌شناسی تنها یک ابزار سرگرم‌کننده نیستند؛ بلکه راهی عمیق و کاربردی برای خودشناسی و شناخت ویژگی‌های پنهان شخصیت شما هستند. فواید تست شخصیت فراتر از تشخیص نوع شخصیت است و به شما در کشف استعدادها، نقاط قوت و ضعف کمک می‌کند. با این شناخت می‌توانید تصمیمات بهتری برای آینده بگیرید، مسیر شغلی مناسب‌تری را انتخاب کنید و حتی در زندگی شخصی و روابط خود، آگاهانه‌تر عمل کنید. اهمیت خودشناسی در موفقیت، امروزه بیشتر از همیشه حس می‌شود و تست‌های شخصیت‌شناسی یکی از ابزارهای مؤثر برای رسیدن به این هدف هستند.</p>
                                        <p class="fs-18">استفاده از آزمون شخصیت‌شناسی مزایای زیادی به همراه دارد که از تقویت مهارت‌های ارتباطی تا بهبود کیفیت زندگی شما را شامل می‌شود. این آزمون‌ها به شما کمک می‌کنند تا درک بهتری از تمایلات رفتاری و احساسی خود داشته باشید و روابط اجتماعی خود را بهبود بخشید. با درک عمیق‌تر از خود، می‌توانید در موقعیت‌های مختلف بهتر واکنش نشان دهید و مسیر پیشرفت شخصی و حرفه‌ای خود را با آگاهی و اعتماد به نفس بیشتری طی کنید. تست‌های شخصیت‌شناسی با فراهم آوردن فرصتی برای خودشناسی عمیق، به شما امکان می‌دهند که به نسخه‌ای بهتر و کامل‌تر از خود تبدیل شوید.</p>
                                    
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <button id="toggle-button" class="btn bg-blue text-white button fw-bold mt-3 mx-auto fs-6 d-flex align-items-center gap-1">
                                        <span>نمایش بیشتر</span> <i class="fa fa-angle-down fs-6"></i>
                                    </button>
                                </div>
                                
                            </div>
    
    
                            {{-- MBTI  --}}
                            <div class="row test-card-shadow rounded-4 mb-5 bg-white px-0" style="border:1px solid gray !important;">
                                <div class="col-md-4 col-12 px-0">
                                    <a href="{{route('mbti.index')}}">
                                        <div class="image-btl mbti-bg p-3">
                                            <img src="{{asset('assets/images/mbti.svg')}}" class="card-img-top image-btl bg-white" alt="MBTI">
                                        </div>
                                    </a>

                                    
                                </div>
                                <div class="col-md-8 col-12 pe-0 py-4 ">
                                    <a href="{{route('mbti.index')}}" class="text-dark">
                                        <div class="pe-0">
                                            <div class="fs-5 text-warp mbti-bg text-white p-2 px-4 test-title">
                                                تست شخصیت شناسی MBTI
                                            </div>
                                            <p class="fs-6 pt-4 px-4" align="justify">
                                                تستMBTI، یا تست مایرز-بریگز، یکی از معتبرترین ابزارهای شخصیت‌شناسی است که برای درک بهتر خود و دیگران به کار می‌رود. این آزمون با ترکیب ویژگی‌های شخصیتی و تمایلات فردی، افراد را در 16 تیپ شخصیتی دسته‌بندی می‌کند و به شما کمک می‌کند تا نقاط قوت و ضعف خود را بهتر بشناسید. در دنیای پیچیده امروز که انتخاب مسیر شغلی و تصمیم‌گیری‌های بزرگ چالش‌برانگیز هستند، شناخت عمیق‌تر از خود می‌تواند موفقیت‌های حرفه‌ای و شخصی بیشتری به ارمغان بیاورد.
                                            <br>
                                            یکی از جذاب‌ترین ویژگی‌های تست MBTI، طبقه‌بندی افراد در 16 تیپ شخصیتی است که هر کدام دارای ویژگی‌های منحصر به فرد خود هستند و شناخت آن‌ها می‌تواند به خودشناسی و بهبود تعاملات فردی کمک کند.
                                            </p>
               
                                            
                                        </div>
                                        <div class="d-flex flex-sm-row flex-column gap-4 px-4">
                                            <div class="p-2 px-3 option-test">
                                                طول آزمون: ۱۵ دقیقه
                                            </div>
                                            <div class="p-2 px-3 option-test">
                                                ۶۰ سوال
                                            </div>
                                        </div>
                                    </a>

                                </div>
    
                                <div class="col-md-2 col-sm-6 col-9 me-auto px-0">
                                    <a href="{{route('mbti.takeTest')}}" class="btn btn-tests mbti-bg text-white fw-bold mt-2" data-modal="mbti">شروع آزمون</a>
                                </div>

                                
                                {{-- <div class="col-12 mt-4 py-3">
                                    <div class="row">
                                        <div class="col-9">
                                            <p class="fs-6 px-3" align="justify">
                                                تست MBTI، یک پرسش‌نامه روان‌سنجی فردی است که برای شناسایی نوع شخصیت، نقاط قوت و اولویت‌های افراد طراحی شده است. این تست توسط ایزابل مایرز (Isabel Myers) و مادرش کاترین بریگز (Katherine Briggs) و بر اساس کارشان بر روی تئوری کارل یونگ (Carl Jung) درمورد انواع شخصیت تهیه شده است. امروزه، تست MBTI به‌عنوان یکی از از پرکاربردترین ابزارهای روان‌شناسی در دنیا شناخته می‌شود.
                                            </p>
                                        </div>
                                        <div class="col-3">
                                            <div class="bg-white rounded-circle">
                                                <img src="{{asset('assets/images/mbti.svg')}}" class="card-img-top rounded-circle" alt="MBTI">
                                            </div>
        
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                            @guest
                            <div class="modal fade" id="mbtiModal" tabindex="-1" aria-labelledby="mbtiModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="mbtiModalLabel">کاربر مهمان</h5>
                                            <button type="button" class="btn-close share-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                        برای شرکت در آزمون شخصیت شناسی باید ابتدا وارد حساب کاربری خود شوید.
                                        </div>
                                        <div class="modal-footer">
                                            <a href="{{route('mbti.takeTest')}}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                <p class="mb-0">ورود</p>
                                                <i class="fa-solid fa-user align-self-center my-0"></i>
                                            </a>
                                            <a href="{{ route('register') }}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                <p class="mb-0">ثبت نام</p>
                                                <i class="fa-solid fa-pen-to-square align-self-center my-0"></i>
                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endguest
    


                            {{-- haaland --}}
                            <div class="row test-card-shadow rounded-4 mb-5 bg-white px-0" style="border:1px solid gray !important;">
                                <div class="col-md-4 col-12 px-0">
                                    <a href="{{route('haaland.index')}}">
                                        <div class="image-btl haaland-bg p-3">
                                            <img src="{{asset('assets/images/haaland.svg')}}" class="card-img-top image-btl bg-white" alt="MBTI">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-8 col-12 pe-0 py-4">
                                    <a href="{{route('haaland.index')}}" class="text-dark">
                                        <div class="pe-0">
                                            <div class="fs-5 text-warp haaland-bg text-white p-2 px-4 test-title">
                                                رغبت شناسی haaland (هالند)
                                            </div>
                                            <p class="fs-6 pt-4 px-4" align="justify">
                                                وقتی کارهایی را انجام می‌دهید که دوست دارید، از کار خود لذت می‌برید. کدهای تست شخصیت شناسی هالند مجموعه‌ای از انواع شخصیت است که توسط روانشناس جان ل. هالند در دهه ۱۹۷۰ ساخته‌شده است. دکتر هالند معتقد است که مردم بهترین عملکرد خود را در محیط‌های کاری‌ای نشان می‌دهند که با ترجیحات و علاقه‌مندی‌هایشان مطابقت دارد. هماهنگ بودن فرد و محیط کار می‌تواند بهترین شرایط را ایجاد کند. اکثر مردم ترکیبی از دو یا سه زمینه موردتوجه هالند هستند. این دو یا سه زمینه تبدیل به “کد هالند” شما می‌شوند.
                                            </p>
                                        </div>
                                        <div class="d-flex gap-4 flex-sm-row flex-column px-4">
                                            <div class="p-2 px-3 option-test">
                                                طول آزمون: ۵ دقیقه
                                            </div>
                                            <div class="p-2 px-3 option-test">
                                                ۱۸ سوال
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div class="col-md-2 col-sm-6 col-9 me-auto ps-0">
                                    <a href="{{route('haaland.take.test')}}" class="btn btn-tests haaland-bg text-white fw-bold mt-2" data-modal="haaland">شروع آزمون</a>
                                </div>
                            </div>
                            @guest
                            <div class="modal fade" id="haalandModal" tabindex="-1" aria-labelledby="haalandModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="haalandModalLabel">کاربر مهمان</h5>
                                            <button type="button" class="btn-close share-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                        برای شرکت در آزمون شخصیت شناسی باید ابتدا وارد حساب کاربری خود شوید.
                                        </div>
                                        <div class="modal-footer">
                                            <a href="{{route('haaland.take.test')}}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                <p class="mb-0">ورود</p>
                                                <i class="fa-solid fa-user align-self-center my-0"></i>
                                            </a>
                                            <a href="{{ route('register') }}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                <p class="mb-0">ثبت نام</p>
                                                <i class="fa-solid fa-pen-to-square align-self-center my-0"></i>
                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endguest
    
    
                            {{-- enneagram --}}
                            <div class="row test-card-shadow rounded-4 mb-5 bg-white px-0" style="border:1px solid gray !important;">
                                <div class="col-md-4 col-12 px-0">
                                    <a href="{{route('enneagram.index')}}">
                                        <div class="image-btl bg-primary p-3">
                                            <img src="{{asset('assets/images/enneagram.svg')}}" class="card-img-top image-btl bg-white" alt="MBTI">
                                        </div>
                                    </a>

                                    
                                </div>
                                <div class="col-md-8 col-12 pe-0 py-4">
                                    <a href="{{route('enneagram.index')}}" class="text-dark">
                                        <div class="pe-0">
                                            <div class="fs-5 text-warp bg-primary text-white p-2 px-4 test-title">
                                                تست شخصیت شناسی Enneagram (انیاگرام)
                                            </div>
                                            <p class="fs-6 pt-4 px-4" align="justify">
                                                
                                                تست انیاگرام یک معیار شخصیت شناسی افراد هست که با استفاده از این میتوانند شخصیت قالب خود را پیدا نموده و برای بهبود شخصیتی خود اقدام کنند. انیاگرام یکی از سیستم های شخصیت شناسی است که انسانها را براساس خصوصیات افراد به 9 گروه یا نه تیپ شخصیتی تقسیم بندی می کند. انیاگرام enneagram کلمه‌ای یونانی‌ است و از دو بخش ennea به معنای 9 و gram به معنای وجه تشکیل شده است. enneagram در زبان فارسی اناگرام ، انیاگرام ، ایناگرام ، اینیاگرام و حتی آناگرام نیز ترجمه شده است 

                                            </p>
                                        </div>
                                        <div class="d-flex gap-4 flex-sm-row flex-column px-4">
                                            <div class="p-2 px-3 option-test">
                                                طول آزمون: ۲۵ دقیقه
                                            </div>
                                            <div class="p-2 px-3 option-test">
                                                ۱۲۰ سوال
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div class="col-md-2 col-sm-6 col-9 me-auto ps-0">
                                    <a href="{{route('enneagram.takeTest')}}" class="btn btn-tests bg-primary text-white fw-bold mt-2" data-modal="enneagram">شروع آزمون</a>
                                </div>
                            </div>
                            @guest
                            <div class="modal fade" id="enneagramModal" tabindex="-1" aria-labelledby="enneagramModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="enneagramModalLabel">کاربر مهمان</h5>
                                            <button type="button" class="btn-close share-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                        برای شرکت در آزمون شخصیت شناسی باید ابتدا وارد حساب کاربری خود شوید.
                                        </div>
                                        <div class="modal-footer">
                                            <a href="{{route('enneagram.takeTest')}}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                <p class="mb-0">ورود</p>
                                                <i class="fa-solid fa-user align-self-center my-0"></i>
                                            </a>
                                            <a href="{{ route('register') }}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                <p class="mb-0">ثبت نام</p>
                                                <i class="fa-solid fa-pen-to-square align-self-center my-0"></i>
                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endguest


    
    
                            {{-- neo --}}
                            <div class="row test-card-shadow rounded-4 mb-5 bg-white px-0" style="border:1px solid gray !important;">
                                <div class="col-md-4 col-12 px-0">
                                    <a href="{{route('neo.index')}}">
                                        <div class="image-btl neo-bg p-3">
                                            <img src="{{asset('assets/images/neo.svg')}}" class="card-img-top image-btl bg-white" alt="NEO">
                                        </div>
                                    </a>

                                </div>
                                <div class="col-md-8 col-12 pe-0 py-4">
                                    <a href="{{route('neo.index')}}" class="text-dark">
                                        <div class="pe-0">
                                            <div class="fs-5 text-warp neo-bg text-white p-2 px-4 test-title">
                                                تست شخصیت شناسی NEO
                                            </div>
                                            <p class="fs-6 pt-4 px-4" align="justify">
                                                تست نئو آزمون مشهور و جامع برای سنجش 5 عامل بزرگ شخصیت می باشد. هریک از این 5 صفت بزرگ شخصیتی شامل 6 جنبه دیگر هستند که باعث می شود این تست 30 ویژگی جزئی را نیز ارزیابی نماید. تست شخصیت شناسی نئو دارای فرم های کوتاه و بلند است.

                                            </p>
                                        </div>
                                        <div class="d-flex gap-4 flex-sm-row flex-column px-4">
                                            <div class="p-2 px-3 option-test">
                                                طول آزمون: ۱۵ دقیقه
                                            </div>
                                            <div class="p-2 px-3 option-test">
                                                ۶۰ سوال
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div class="col-md-2 col-sm-6 col-9 me-auto ps-0">
                                    <a href="{{route('neo.take.test')}}" class="btn btn-tests neo-bg text-white fw-bold mt-2" data-modal="neo">شروع آزمون</a>
                                </div>
                            </div>
                            @guest
                            <div class="modal fade" id="neoModal" tabindex="-1" aria-labelledby="neoModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="neoModalLabel">کاربر مهمان</h5>
                                            <button type="button" class="btn-close share-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                        برای شرکت در آزمون شخصیت شناسی باید ابتدا وارد حساب کاربری خود شوید.
                                        </div>
                                        <div class="modal-footer">
                                            <a href="{{route('neo.take.test')}}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                <p class="mb-0">ورود</p>
                                                <i class="fa-solid fa-user align-self-center my-0"></i>
                                            </a>
                                            <a href="{{ route('register') }}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                <p class="mb-0">ثبت نام</p>
                                                <i class="fa-solid fa-pen-to-square align-self-center my-0"></i>
                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endguest
    
    
                            {{-- ghq --}}
                            <div class="row test-card-shadow rounded-4 mb-5 bg-white px-0" style="border:1px solid gray !important;">
                                <div class="col-md-4 col-12 px-0">
                                    <a href="{{route('ghq.index')}}">
                                        <div class="image-btl ghq-bg p-3">
                                            <img src="{{asset('assets/images/ghq.svg')}}" class="card-img-top image-btl bg-white" alt="ghq">
                                        </div>
                                    </a>
  
                                    
                                </div>
                                <div class="col-md-8 col-12 pe-0 py-4">
                                    <a href="{{route('ghq.index')}}">
                                        <div class="pe-0">
                                            <a href="{{route('ghq.index')}}" class="fs-5 text-warp ghq-bg text-white p-2 px-4 test-title">
                                                تست سلامت عمومی GHQ
                                            </a>
                                            <p class="fs-6 pt-4 px-4" align="justify">
                                                تست سلامت عمومی (GHQ) بـه بررسـی وضـعیت روانی و جسمانی فرد در یک ماهه اخیر می پردازد و شامل نشانه هـایی مانند افکار و احساسات نابهنجار و جنبه هایی از رفتار قابل مشاهده است که بر موقعیت اینجـا و اکنـون تأکیـد دارد. تست سلامت عمومی دو نگرانی اصلی را شناسایی می کند: 1) ناتوانی در انجام عملکردهای عادی و 2) ظهور پدیده های جدید و ناراحت کننده. این آزمون به شما نشان می دهد که وضع عمومی سلامت شما در چند هفته گذشته، چگونه بوده است. تست سلامت عمومی GHQ چهار اصلی را می سنجد: علائم جسمانی، علائم افسردگی، علائم اضطرابی و اختلال خواب و علائم کارکرد اجتماعی.

                                            </p>
                                        </div>
                                        <div class="d-flex gap-4 flex-sm-row flex-column px-4">
                                            <div class="p-2 px-3 option-test">
                                                طول آزمون: ۵ دقیقه
                                            </div>
                                            <div class="p-2 px-3 option-test">
                                                ۲۸ سوال
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div class="col-md-2 col-sm-6 col-9 me-auto ps-0">
                                    <a href="{{route('ghq.take.test')}}" class="btn btn-tests ghq-bg text-white fw-bold mt-2" data-modal="ghq">شروع آزمون</a>
                                </div>
                            </div>


                            @guest
                                <div class="modal fade" id="ghqModal" tabindex="-1" aria-labelledby="ghqModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="ghqModalLabel">کاربر مهمان</h5>
                                                <button type="button" class="btn-close share-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                            برای شرکت در آزمون شخصیت شناسی باید ابتدا وارد حساب کاربری خود شوید.
                                            </div>
                                            <div class="modal-footer">
                                                <a href="{{route('ghq.take.test')}}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                    <p class="mb-0">ورود</p>
                                                    <i class="fa-solid fa-user align-self-center my-0"></i>
                                                </a>
                                                <a href="{{ route('register') }}" class="btn bg-blue text-white button fw-bold fs-6 d-flex align-items-center gap-1">
                                                    <p class="mb-0">ثبت نام</p>
                                                    <i class="fa-solid fa-pen-to-square align-self-center my-0"></i>
                                                </a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endguest
    
                        </div>
    
                    </div>
    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById("toggle-button").addEventListener("click", function () {
        const textContainer = document.querySelector(".text-container");
        const hiddenContent = document.querySelector(".hidden-content");
        const button = this;

        if (textContainer.classList.contains("expanded")) {
            textContainer.classList.remove("expanded");
            hiddenContent.style.display = "none";
            button.innerHTML = '<span>نمایش بیشتر</span> <i class="fa fa-angle-down fs-6"></i>';
        } else {
            textContainer.classList.add("expanded");
            hiddenContent.style.display = "block";
            button.innerHTML = '<span>نمایش کمتر</span> <i class="fa fa-angle-up fs-6"></i>';
        }
    });


    const testButtons = document.querySelectorAll('.btn-tests'); 

    testButtons.forEach(button => {
        button.addEventListener('click', function (event) {

            const loginModalElement = document.getElementById(this.dataset.modal + 'Modal'); 
            if (loginModalElement) {
                event.preventDefault(); 
                const loginModal = new bootstrap.Modal(loginModalElement);
                loginModal.show(); 
            }
        });
    });


});
</script>

@endsection