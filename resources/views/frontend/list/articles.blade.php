@extends('frontend.layout.master')
@section('meta')
    <meta name="description" content="مطالب و مقالاتی در زمینه رشد فردی، بهبود مهارت‌های شخصی و حرفه‌ای، و راهنمایی برای پیدا کردن شغل مناسب و فرصت‌های شغلی جدید. از راهنمایی‌های ما برای بهبود مسیر حرفه‌ای خود استفاده کنید." />
    <meta property="og:url" content="https://jobist.ir/mag"/>
    <meta property="og:title" content=" مجله جابیست | مقالات مفید و کاربردی در جهت زندگی شغلی بهتر" />
    <meta property="og:description" content="دنبال فرصت‌های شغلی جدید و رشد فردی هستید؟ مقالات ما به شما در پیدا کردن شغل مناسب و بهبود مهارت‌های حرفه‌ای کمک می‌کند. همین حالا بخوانید و قدمی به سمت موفقیت بردارید." />
    <link rel="canonical" href="https://jobist.ir/mag"/>
    <link rel="preload" href="{{asset('assets/frontend/img/home/sale-Overlay-min2.webp')}}" as="image"/>
@endsection
@section('title' , 'مجله')
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/article-style.css') }}"> 
@endsection


@section('main')

<div class="container-fluid top-section">
    <section>
        <div class="row align-items-center">
            <div class="col-12 col-md-12 mt-3">
                <div class="d-flex flex-row justify-content-center mt-md-5 mt-3 flex-wrap">
                    <h3 class="text-center blue fw-medium display-4">مجله جابیست</h3>
                </div>

                {{-- search --}}

                <form action="{{ route('article.blog.search') }}" method="POST" class="d-flex flex-row justify-content-center mt-lg-4 mt-3" role="search">
                    @csrf
                        <input name="queryArticle" class="form-control ms-2 form-shadow rounded-pill py-2 border-input" type="search" placeholder="جست و جو در مقاله ها..." aria-label="Search" 
                            @isset($queryArticle)
                                value="{{ $queryArticle }}"
                            @endisset
                        >
                        <button class="button btn bg-blue rounded-circle btn-shadow" type="submit" aria-label="Search">
                            <i class="fa-solid fa-magnifying-glass text-white align-self-center fs-6"></i>
                        </button>
                </form>

                <div class="d-flex flex-row flex-wrap gap-0 column-gap-2 justify-content-center mt-3 mb-5">
                    @foreach ($categories as $category)
                        @if($category->rank == 1)
                            <a href="{{route('category.blog.search', $category->slug)}}" class="border-brand bg-white rounded-5 py-1 px-md-2 px-2 mt-2 mx-md-3 mx-1 margin-brand text-dark">{{ $category->title }}</a>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>
    </section>
</div>


<div class="container-fluid bg-art">
    <div class="container mt-0">
        <div class="row">
            {{-- pc  --}}
            <div class="col-12 col-md-3 d-md-block d-none mt-5">

                    <div class="side-blog">
                        <div class="mb-1">
                            <a  role="button" aria-label="toggle category" class="d-flex gap-2 border-bottom border-secondery-subtle p-3 toggle-btn" data-bs-toggle="collapse" data-bs-target="#categories" aria-expanded="true">
                                <div class="align-self-center">
                                    <i class="fa-solid fa-angle-down fs-5 blue"></i>
                                </div>
                                <h3 class="fs-5 mb-0 text-grey-s2 fw-medium blue">دسته بندی</h3>
                            </a>
                            <div class="collapse show" id="categories">
                                <ul class="list-unstyled fw-normal pb-2 fs-6 px-2 mb-0">
                                    @foreach ($categories as $category)
                                        <li class="p-2 border-bottom border-secondery-subtle category-filter d-flex align-items-center justify-content-between
                                            @if(isset($categoryId) && $categoryId == $category->id)
                                                active-category
                                            @endif
                                        ">
                                            <a href="{{ route('category.blog.search', $category->slug) }}" class="d-flex align-items-center w-100 blue" id="category_filter">
                                                <i class="fa-solid fa-table ms-2"></i>
                                                <div>{{ $category->title }}</div>
                                            </a>
                                            <a class="plus" href="{{route('articles.blog')}}"><img src="{{asset('assets/frontend/img/search-jobs/plus-solid.svg')}}" alt=""></a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

            </div>

            {{-- mobile --}}
            <div class="col-12 d-block d-md-none mt-3 mb-2">
                <div class="side-blog-mo mx-auto">
                    <div class="mb-1">
                        <a role="button" aria-label="toggle category" class="d-flex gap-2 border-bottom border-secondery-subtle p-3 toggle-btn" data-bs-toggle="collapse" data-bs-target="#categories" aria-expanded="false">
                            <div class="align-self-center">
                                <i class="fa-solid fa-angle-up fs-5"></i>
                            </div>
                            <h3 id="gradeBtn" class="fs-5 mb-0 text-grey-s2 fw-medium">دسته بندی</h3>
                        </a>
                        <div class="collapse" id="categories">
                            <ul class="list-unstyled fw-normal pb-1 fs-6 px-2 mb-0">
                                @foreach ($categories as $category)
                                <li role="button" class="p-2 border-bottom border-secondery-subtle category-filter d-flex align-items-center justify-content-between
                                    @if(isset($categoryId) && $categoryId == $category->id)
                                        active-category
                                    @endif
                                ">
                                <a href="{{ route('category.blog.search', ['category' => $category->slug]) }}" class="d-flex align-items-center w-100" id="category_filter">
                                    <i class="fa-solid fa-table ms-2"></i>
                                    <div>{{ $category->title }}</div>
                                </a>
                                <a class="d-none 
                                    @if(isset($categoryId) && $categoryId == $category->id)
                                        d-block
                                    @endif
                                " href="{{route('articles.blog')}}"><img src="{{asset('assets/frontend/img/search-jobs/plus-solid.svg')}}" alt=""></a>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-9 mt-5">
                @if ($articles->count() > 0)
                    <div class="row row-cols-xl-2 row-cols-1 row-gap-5">
                        @foreach ($articles as $article)
                            <div class="col py-3">
                                <a href="{{route('single.article.blog', $article->slug ) }}">
                                    <div class="d-flex flex-column blog-box box-hover px-2">
                                        <div class="d-flex flex-sm-row flex-column px-2">

                                            <div class="position-relative me-2">
                                                <div class="img-blog-wrapper">
                                                    <img src="{{asset($article->photo->path)}}" alt="{{ $article->category->title }}" class="blog-img" height="140" width="200">
                                                </div>
                                                <div class="category-blog">{{ $article->category->title }}</div>
                                            </div>
    
                                            <div class="d-flex flex-column column-gap-2 justify-content-between w-100">
                                                    <h4 class="fs-6 my-3 fw-semibold text-dark me-2">{{$article->title}}</h4>
                                            </div>
    
                                        </div>

                                        <div class="d-flex justify-content-between align-items-center p-sm-2 p-1 border-top border-secondery-subtle mt-1">
                                            <div class="d-flex">
                                                <div class="d-flex fs-7 ms-1 fw-semibold blue view-blog">
                                                    <div class=" align-self-center">
                                                        <i class="fa-regular fa-eye ms-1"></i>
                                                    </div>
                                                    <div class="ms-1 align-self-center">{{$article->views}}</div>
                                                </div>
    
                                                <div class="d-flex fs-7 ms-1 fw-semibold blue view-blog">
                                                    <div class=" align-self-center">
                                                        <i class="fa-regular fa-heart ms-1"></i>
                                                    </div>
                                                    <div class="ms-1 align-self-center">{{ $article->likes->count() }}</div>
                                                </div>
                                            </div>

                                            <div class="d-flex fs-7 me-2 fw-semibold blue">
                                                <div class=" align-self-center">
                                                    <i class="fa-solid fa-calendar-days ms-1"></i>
                                                </div>
                                                <div class="ms-1 align-self-center">{{ verta($article->created_at)->format('%d %B %Y') }}</div>
                                            </div>
                                        </div>
                                    </div>

                                </a>
                            </div>
                        @endforeach
                    </div>
                    {{$articles->links('pagination.frontend')}}
                @else
                    <div class="no-info box-shadow-c w-md-50 mx-auto">
                        مقاله ای یافت نشد!
                    </div>
                @endif
                
            </div>
        </div>
    
    </div>
</div>

@endsection

