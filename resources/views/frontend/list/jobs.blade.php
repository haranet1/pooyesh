@extends('frontend.layout.master')

@section('meta')
    <meta name="description" content="با جابیست، به جدیدترین آگهی‌های استخدام و کارآموزی در سال ۱۴۰۳ دسترسی پیدا کنید. اینجا جایی است که فرصت شغلی، استخدام، کارآموزی، فرصت کار، و آگهی استخدام روزانه به شما ارائه می‌شود. شروع کنید و راهی بهترین شغل برای خودتان را پیدا کنید." />
    <meta property="og:url" content="https://jobist.ir/list/jobs"/>
    <meta property="og:title" content=" فرصت‌های شغلی جابیست|جدیدترین آگهی‌های استخدام و کارآموزی 1403" />
    <meta property="og:description" content=" با جابیست، به جدیدترین آگهی‌های استخدام و کارآموزی در سال ۱۴۰۳ دسترسی پیدا کنید. اینجا جایی است که فرصت شغلی، استخدام، کارآموزی، فرصت کار، و آگهی استخدام روزانه به شما ارائه می‌شود. شروع کنید و راهی بهترین شغل برای خودتان را پیدا کنید. " /> 
    <link rel="canonical" href="https://jobist.ir/list/jobs"/>
@endsection

@section('title' , 'جدیدترین آگهی‌های استخدام و کارآموزی 1403')
@section('page' , 'فرصت‌های شغلی جابیست' )

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/jobs-search.css') }}">
@endsection

@section('main')
    {{-- search --}}

    <form action="{{ route('jobs.search') }}" method="get" id="searchForm">
        <div class="container-fluid dot-bg">
            <div class="container border-dash-s pb-2 px-2">
                <div class="bg-blue p-md-5 p-4 rounded-bottom-5">
                    
                        <div class="row row-gap-2 mt-3 py-1 column-gap-2 flex-wrap">

                            <div class="col-12 col-lg-3 input-g rounded-pill bg-white">
                                <div class="align-self-center">
                                    <img src="{{ asset('assets/frontend/img/search-jobs/layer-group-solid.svg') }}" class="img-fluid w-90" alt="گروه شغلی" height="22" width="22">
                                </div>
                                <div class="position-relative select-icon px-1 rounded-pill">
                                    <select id="category" name="category" class="select-custom greys bg-white form-select" aria-label="Default select example">
                                        <option selected value="" class="option">گروه شغلی</option>
                                        @foreach ($categories as $category)
                                            @isset($oldData['category'])
                                                @if ($oldData['category'] == $category->id)
                                                    <option selected value="{{ $category->id }}">{{ $category->title }}</option>
                                                @else
                                                    <option value="{{ $category->id }}" class="option">{{ $category->title }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $category->id }}" class="option">{{ $category->title }}</option>
                                            @endisset
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-lg-4 input-g rounded-pill bg-white">
                                <div class="align-self-center">
                                    <img src="{{ asset('assets/frontend/img/search-jobs/search.svg') }}" class="img-fluid w-90" alt="عنوان شغل" height="24" width="24">
                                </div>
                                <input type="search" name="jobTitle" placeholder="عنوان شغل" class="form-select select-custom greys bg-white input rounded-pill"
                                
                                    @if(isset($oldData['jobTitle']) && ! is_null($oldData['jobTitle']) && $oldData['jobTitle'] != '')
                                        value="{{ $oldData['jobTitle'] }}"
                                    @endif
                                >
                            </div>

                            <div class="col-12 col-lg-3 input-g rounded-pill bg-white">
                                <div class="align-self-center">
                                    <img src="{{ asset('assets/frontend/img/search-jobs/map-location-dot-solid.svg') }}" class="img-fluid w-90" alt="استان موقعیت شغلی" height="18" width="23">
                                </div>
                                <div class="position-relative select-icon">
                                    <input type="hidden" id="getCitiesUrl" value="{{ route('get.cities.ajax.front') }}">
                                    <select name="province" id="province" onchange="getCities()" class="rounded-pill select-custom greys bg-white form-select" aria-label="Default select example">
                                        <option selected value="" class="option">انتخاب استان</option>
                                        @foreach ($provinces as $province)
                                            @isset($oldData['province'])
                                                @if ($oldData['province'] == $province->id)
                                                    <option selected value="{{ $province->id }}">{{ $province->name }}</option>
                                                @else
                                                    <option value="{{ $province->id }}" class="option">{{ $province->name }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $province->id }}" class="option">{{ $province->name }}</option>
                                            @endisset
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-lg-1 rounded-pill search-btn-wrapper">
                                <button class="search-btn rounded-pill" onclick="submitFilterForm()">
                                    جستجو
                                </button>
                            </div>
                        </div>

                    
                </div>
            </div> 
        </div>


            {{-- pc  --}}

        <div class="container border-dash2 p-4 mt-5">
            <div class="row">

                <div class="col-12 col-md-4 d-md-block d-none">
                    
                        <div class="bg-white d-flex align-items-center justify-content-between p-3 rounded-3 flex-lg-row flex-column">
                            <h3 class="fs-5 mb-0 text-grey-s fw-medium fs-md-18">فقط موقعیت های کارآموزی</h3>
                            <label class="switch align-self-end">
                                
                                @isset($oldData['type_intern'])
                                    @if ($oldData['type_intern'] == "on")
                                        <input type="checkbox" id="intern_input" checked name="type_intern" onchange="justOneType(this)" aria-label="switch intern">
                                    @else
                                        <input type="checkbox" id="intern_input" name="type_intern" onchange="justOneType(this)" aria-label="switch intern">
                                    @endif
                                @else
                                    <input type="checkbox" id="intern_input" name="type_intern" onchange="justOneType(this)" aria-label="switch intern">
                                @endisset
                                
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="bg-white d-flex align-items-center justify-content-between p-3 rounded-3 mt-4 flex-lg-row flex-column">
                            <h3 class="fs-5 mb-0 blue fw-medium fs-md-18">فقط موقعیت های استخدامی</h3>
                            <label class="switch align-self-end">

                                @isset($oldData['type_hire'])
                                    @if ($oldData['type_hire'] == "on")
                                        <input type="checkbox" id="hire_input" checked name="type_hire" onchange="justOneType(this)" aria-label="switch hire">
                                    @else
                                        <input type="checkbox" id="hire_input" name="type_hire" onchange="justOneType(this)" aria-label="switch hire">
                                    @endif
                                @else
                                    <input type="checkbox" id="hire_input" name="type_hire" onchange="justOneType(this)" aria-label="switch hire">
                                @endisset

                                

                                <span class="slider round"></span>
                            </label>
                        </div>


                        <div class="bg-white px-3 rounded-3 mt-4">

                            <div class="mb-1">
                                <a role="button" aria-label="toggle filter" class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#education-collapse" aria-expanded="false">
                                    <img src="{{ asset('assets/frontend/img/search-jobs/graduation-cap-solid.svg') }}" class="img-fluid w-logo-menu" alt="icon graduation-cap">

                                    <h3 id="gradeBtn" class="fs-5 mb-0 text-grey-s2 fw-medium">تحصیلات</h3>

                                </a>
                                <div class="collapse" id="education-collapse">
                                    
                                    @isset($oldData['grade'])
                                        @if ($oldData['grade'] != '' || ! is_null($oldData['grade']))
                                            <input type="hidden" id="grade_input" name="grade" value="{{ $oldData['grade'] }}">
                                        @else
                                            <input type="hidden" id="grade_input" name="grade">
                                        @endif
                                    @else
                                        <input type="hidden" id="grade_input" name="grade">
                                    @endisset

                                    <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0">
                                        <li role="button" onclick="setValueToInput('grade_input' , 'gradeBtn' , 'دکترا')" class="p-2 item-border">دکترا</li>
                                        <li role="button" onclick="setValueToInput('grade_input' , 'gradeBtn' , 'کارشناسی ارشد')" class="p-2 item-border">کارشناسی ارشد</li>
                                        <li role="button" onclick="setValueToInput('grade_input' , 'gradeBtn' , 'کارشناسی')" class="p-2 item-border">کارشناسی</li>
                                        <li role="button" onclick="setValueToInput('grade_input' , 'gradeBtn' , 'کاردانی')" class="p-2 item-border">کاردانی</li>
                                        <li role="button" onclick="setValueToInput('grade_input' , 'gradeBtn' , 'دیپلم')" class="p-2">دیپلم</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="mb-1">
                                <a role="button" aria-label="toggle filter" class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#workExperience-collapse" aria-expanded="false">
                                    <img src="{{ asset('assets/frontend/img/search-jobs/Group 2517.svg') }}" class="img-fluid w-logo-menu" alt="icon Group">

                                    <h3 id="workExperienceBtn" class="fs-5 mb-0 text-grey-s2 fw-medium">سابقه کاری</h3>

                                </a>
                                <div class="collapse" id="workExperience-collapse">

                                    @isset($oldData['workExperience'])
                                        @if ($oldData['workExperience'] != '' || ! is_null($oldData['workExperience']))
                                            <input type="hidden" value="{{ $oldData['workExperience'] }}" id="work_experience_input" name="workExperience">
                                        @else
                                            <input type="hidden" id="work_experience_input" name="workExperience">
                                        @endif
                                    @else
                                        <input type="hidden" id="work_experience_input" name="workExperience">
                                    @endisset

                                    <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0">
                                        <li role="button" onclick="setValueToInput('work_experience_input' , 'workExperienceBtn' , 'تا ۲ سال')" class="p-2 item-border">تا ۲ سال</li>
                                        <li role="button" onclick="setValueToInput('work_experience_input' , 'workExperienceBtn' , '۳ تا ۵ سال')" class="p-2 item-border">۳ تا ۵ سال</li>
                                        <li role="button" onclick="setValueToInput('work_experience_input' , 'workExperienceBtn' , '۶ تا ۱۰ سال')" class="p-2 item-border">۶ تا ۱۰ سال</li>
                                        <li role="button" onclick="setValueToInput('work_experience_input' , 'workExperienceBtn' , 'بالای ۱۰ سال')" class="p-2">بالای ۱۰ سال</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="mb-1">
                                <a role="button" aria-label="toggle filter" class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#salary-collapse" aria-expanded="false">
                                    <img src="{{ asset('assets/frontend/img/search-jobs/Money.svg') }}" class="img-fluid w-logo-menu" alt="icon Money">

                                    <h3 id="salaryBtn" class="fs-5 mb-0 text-grey-s2 fw-medium">حقوق</h3>

                                </a>
                                <div class="collapse" id="salary-collapse">

                                    @isset($oldData['salary'])
                                        @if ($oldData['salary'] != '' || ! is_null($oldData['salary']))
                                            <input type="hidden" value="{{ $oldData['salary']}}" id="salary_input" name="salary">
                                        @else
                                            <input type="hidden" id="salary_input" name="salary">
                                        @endif
                                    @else
                                        <input type="hidden" id="salary_input" name="salary">
                                    @endisset
                                    
                                    <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0">
                                        <li role="button" onclick="setValueToInput('salary_input' , 'salaryBtn' , 'زیر ۴ میلیون')" class="p-2 item-border">زیر ۴ میلیون</li>
                                        <li role="button" onclick="setValueToInput('salary_input' , 'salaryBtn' , 'بین ۴ تا ۸ میلیون')" class="p-2 item-border">بین ۴ تا ۸ میلیون</li>
                                        <li role="button" onclick="setValueToInput('salary_input' , 'salaryBtn' , 'بین ۸ تا ۱۶ میلیون')" class="p-2 item-border">بین ۸ تا ۱۶ میلیون</li>
                                        <li role="button" onclick="setValueToInput('salary_input' , 'salaryBtn' , 'بین ۱۶ تا ۲۵ میلیون')" class="p-2 item-border">بین ۱۶ تا ۲۵ میلیون</li>
                                        <li role="button" onclick="setValueToInput('salary_input' , 'salaryBtn' , 'بالای ۲۵ میلیون')" class="p-2">بالای ۲۵ میلیون</li>
                                    </ul>
                                </div>
                            </div>


                            <div class="mb-1">
                                <a role="button" aria-label="toggle filter" class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#age-collapse" aria-expanded="false">
                                    <img src="{{ asset('assets/frontend/img/search-jobs/user-clock-solid.svg') }}" class="img-fluid w-logo-menu" alt="user-clock icon">

                                    <h3 id="ageBtn" class="fs-5 mb-0 text-grey-s2 fw-medium">رنج سنی</h3>

                                </a>
                                <div class="collapse" id="age-collapse">

                                    @isset($oldData['age'])
                                        @if ($oldData['age'] != '' || ! is_null($oldData['age']))
                                            <input type="hidden" value="{{ $oldData['age'] }}" id="age_input" name="age">
                                        @else
                                            <input type="hidden" id="age_input" name="age">
                                        @endif
                                    @else
                                        <input type="hidden" id="age_input" name="age">
                                    @endisset

                                    <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0">
                                        <li role="button" onclick="setValueToInput('age_input' , 'ageBtn' , 'زیر ۲۰ سال')" class="p-2 item-border">زیر ۲۰ سال</li>
                                        <li role="button" onclick="setValueToInput('age_input' , 'ageBtn' , '۲۱ تا ۳۰ سال')" class="p-2 item-border">۲۱ تا ۳۰ سال</li>
                                        <li role="button" onclick="setValueToInput('age_input' , 'ageBtn' , '۳۱ تا ۴۰ سال')" class="p-2 item-border">۳۱ تا ۴۰ سال</li>
                                        <li role="button" onclick="setValueToInput('age_input' , 'ageBtn' , 'بالای ۴۱ سال')" class="p-2">بالای ۴۱ سال</li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="mb-1">
                                <a role="button" aria-label="toggle filter" class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#ulCity-collapse" aria-expanded="false">
                                    <img src="{{ asset('assets/frontend/img/search-jobs/location-dot-solid (1).svg') }}" class="img-fluid w-logo-menu" alt="location icon">

                                    <h3 id="cityBtn" class="fs-5 mb-0 text-grey-s2 fw-medium">شهر</h3>

                                </a>
                                <div class="collapse" id="ulCity-collapse">

                                    @isset($oldData['city'])
                                        @if ($oldData['city'] != '' || ! is_null($oldData['city']))
                                            <input type="hidden" value="{{ $oldData['city'] }}" id="city_input" name="city">
                                        @else
                                            <input type="hidden" id="city_input" name="city">
                                        @endif
                                    @else
                                        <input type="hidden" id="city_input" name="city">
                                    @endisset

                                    <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0 ulCity">
                                        <li class="p-2 item-border">استان مورد نظر را انتخاب کنید</li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    

                </div>



                <div class="col-12 col-md-8">

                    <div class="bg-white p-3 rounded-3 box-shadow-c d-md-block d-none">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="fs-5 mb-0 blue fw-bold">{{ $jobs->total() }} فرصت شغلی فعال یافت شد :</h3>
                            {{-- <button class="sort-btn bg-blue position-relative rounded-3 text-white">
                                مرتب سازی بر اساس
                            </button> --}}
                        </div>

                        @if (count($oldData) > 0)
                            @php
                                $hasFilter = false;
                            @endphp
                            <div class="row row-cols-auto mt-4 row-gap-2 column-gap-2 px-3">
                                @foreach ($oldData as $key => $value)
                                    @if ($key != 'category' && $key != 'jobTitle' && $key != 'province')
                                        @if ($value != '' && ! is_null($value))
                                        <div role="button" class="filter-btn rounded-4 col" data-key="{{ $key }}" data-value="{{ $value }}" onclick="removeValueInInput(this)">{{ change_filters_name_to_fa($key, $value) }}
                                                </div>
                                            @php
                                                $hasFilter = true;
                                            @endphp
                                        @endif
                                        
                                    @endif
                                @endforeach
                            </div>
                            @if ($hasFilter == true)
                                <div class="d-flex justify-content-start">
                                    <div role="button" class="remove-filter-btn mt-4 me-auto align-self-start" onclick="removeAllValueInInputs()">
                                        حذف فیلتر ها
                                    </div>
                                </div>
                            @endif
                            
                        @endif

                    </div>

                {{-- mobile  --}}

                    <div class="bg-white p-3 rounded-3 box-shadow-c d-md-none d-block">
                        <h3 class="fs-6 mb-0 blue fw-bold">{{ $jobs->total() }} فرصت شغلی فعال یافت شد :</h3>
                        <div class="d-flex justify-content-center gap-2 align-items-center flex-row mt-3">
                            <div role="button" class="filter-btn-mobile bg-blue position-relative rounded-4 text-white" data-bs-toggle="collapse" data-bs-target="#filter-collapse" aria-expanded="false">
                                فیلتر ها
                            </div>
                            {{-- <button class="sort-btn-mobile bg-blue position-relative rounded-4 text-white"> 
                                مرتب سازی
                            </button> --}}
                        </div>
                        <div class="collapse" id="filter-collapse">
                            @if (count($oldData) > 0)
                                @php
                                    $hasFilter = false;
                                @endphp
                                @foreach ($oldData as $key => $value)
                                    @if ($key != 'category' && $key != 'jobTitle' && $key != 'province')
                                        @if ($value != '' && ! is_null($value))
                                            <div role="button" class="filter-btn rounded-4 col mt-3" data-key="{{ $key }}" data-value="{{ $value }}" onclick="removeValueInInput(this)">{{ change_filters_name_to_fa($key, $value) }}</div>
                                            @php
                                                $hasFilter = true;
                                            @endphp
                                        @endif
                                    @endif
                                @endforeach
                                @if ($hasFilter == true)
                                    <button class="remove-filter-btn mt-3" onclick="removeAllValueInInputs()">
                                        حذف فیلتر ها
                                    </button>
                                @endif
                                
                            @endif
                            <div class="bg-white d-flex align-items-center p-3 rounded-3 box-shadow-c mt-3">
                                @isset($oldData['type_intern'])
                                    @if ($oldData['type_intern'] == "on")
                                        <input type="checkbox" checked id="intern_mo" name="type_intern" class="checkbox-mobile ms-2" onchange="justOneType(this)"  aria-label="switch intern">
                                    @else
                                        <input type="checkbox" id="intern_mo" name="type_intern" class="checkbox-mobile ms-2" onchange="justOneType(this)" aria-label="switch intern">
                                    @endif
                                @else
                                    <input type="checkbox" id="intern_mo" name="type_intern" class="checkbox-mobile ms-2" onchange="justOneType(this)" aria-label="switch intern">
                                @endisset
                                <lable class="fs-5 mb-0 text-grey-s fw-medium fs-md-18">فقط موقعیت های کارآموزی</lable>
                            </div>

                            <div class="bg-white d-flex align-items-center p-3 rounded-3 box-shadow-c mt-3">
                                @isset($oldData['type_hire'])
                                    @if ($oldData['type_hire'] == "on")
                                        <input type="checkbox" id="hire_mo" checked name="type_hire" class="checkbox-mobile ms-2" onchange="justOneType(this)" aria-label="switch hire">
                                    @else
                                        <input type="checkbox" id="hire_mo" name="type_hire" class="checkbox-mobile ms-2" onchange="justOneType(this)" aria-label="switch hire">
                                    @endif
                                @else
                                    <input type="checkbox" id="hire_mo" name="type_hire" class="checkbox-mobile ms-2" onchange="justOneType(this)" aria-label="switch hire">
                                @endisset
                                
                                <lable class="fs-5 mb-0 blue fw-medium fs-md-18">فقط موقعیت های استخدامی</lable>
                            </div>



                            <div class="bg-white px-3 rounded-3 mt-4">

                                <div class="mb-1">
                                    <div class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#education-collapse" aria-expanded="false">
                                        <img src="{{ asset('assets/frontend/img/search-jobs/graduation-cap-solid.svg') }}" class="img-fluid w-logo-menu">
                                        <h3 class="fs-5 mb-0 text-grey-s2 fw-medium">تحصیلات</h3>
                                    </div>
                                    <div class="collapse" id="education-collapse">

                                        @isset($oldData['grade'])
                                            @if ($oldData['grade'] != '' || ! is_null($oldData['grade']))
                                                <input type="hidden" id="grade_mo" name="grade" value="{{ $oldData['grade'] }}">
                                            @else
                                                <input type="hidden" id="grade_mo" name="grade">
                                            @endif
                                        @else
                                            <input type="hidden" id="grade_mo" name="grade">
                                        @endisset

                                        <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0">
                                            <li role="button" onclick="setValueToInput('grade' , 'gradeBtn' , 'دکترا')" class="p-2 item-border">دکترا</li>
                                            <li role="button" onclick="setValueToInput('grade' , 'gradeBtn' , 'کارشناسی ارشد')" class="p-2 item-border">کارشناسی ارشد</li>
                                            <li role="button" onclick="setValueToInput('grade' , 'gradeBtn' , 'کارشناسی')" class="p-2 item-border">کارشناسی</li>
                                            <li role="button" onclick="setValueToInput('grade' , 'gradeBtn' , 'کاردانی')" class="p-2 item-border">کاردانی</li>
                                            <li role="button" onclick="setValueToInput('grade' , 'gradeBtn' , 'دیپلم')" class="p-2">دیپلم</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="mb-1">
                                    <div class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#workExperience-collapse" aria-expanded="false">
                                        <img src="{{ asset('assets/frontend/img/search-jobs/Group 2517.svg') }}" class="img-fluid w-logo-menu">
                                        <h3 class="fs-5 mb-0 text-grey-s2 fw-medium">سابقه کاری</h3>
                                    </div>
                                    <div class="collapse" id="workExperience-collapse">

                                        @isset($oldData['workExperience'])
                                            @if ($oldData['workExperience'] != '' || ! is_null($oldData['workExperience']))
                                                <input type="hidden" value="{{ $oldData['workExperience'] }}" id="work_experience_mo" name="workExperience">
                                            @else
                                                <input type="hidden" id="work_experience_mo" name="workExperience">
                                            @endif
                                        @else
                                            <input type="hidden" id="work_experience_mo" name="workExperience">
                                        @endisset

                                        <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0">
                                            <li role="button" onclick="setValueToInput('workExperience' , 'workExperienceBtn' , 'تا ۲ سال')" class="p-2 item-border">تا ۲ سال</li>
                                            <li role="button" onclick="setValueToInput('workExperience' , 'workExperienceBtn' , '۳ تا ۵ سال')" class="p-2 item-border">۳ تا ۵ سال</li>
                                            <li role="button" onclick="setValueToInput('workExperience' , 'workExperienceBtn' , '۶ تا ۱۰ سال')" class="p-2 item-border">۶ تا ۱۰ سال</li>
                                            <li role="button" onclick="setValueToInput('workExperience' , 'workExperienceBtn' , 'بالای ۱۰ سال')" class="p-2">بالای ۱۰ سال</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="mb-1">
                                    <div class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#salary-collapse" aria-expanded="false">
                                        <img src="{{ asset('assets/frontend/img/search-jobs/Money.svg') }}" class="img-fluid w-logo-menu">
                                        <h3 class="fs-5 mb-0 text-grey-s2 fw-medium">حقوق</h3>
                                    </div>
                                    <div class="collapse" id="salary-collapse">

                                        @isset($oldData['salary'])
                                            @if ($oldData['salary'] != '' || ! is_null($oldData['salary']))
                                                <input type="hidden" value="{{ $oldData['salary']}}" id="salary_mo" name="salary">
                                            @else
                                                <input type="hidden" id="salary_mo" name="salary">
                                            @endif
                                        @else
                                            <input type="hidden" id="salary_mo" name="salary">
                                        @endisset

                                        <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0">
                                            <li role="button" onclick="setValueToInput('salary' , 'salaryBtn' , 'زیر ۴ میلیون')" class="p-2 item-border">زیر ۴ میلیون</li>
                                            <li role="button" onclick="setValueToInput('salary' , 'salaryBtn' , 'بین ۴ تا ۸ میلیون')" class="p-2 item-border">بین ۴ تا ۸ میلیون</li>
                                            <li role="button" onclick="setValueToInput('salary' , 'salaryBtn' , 'بین ۸ تا ۱۶ میلیون')" class="p-2 item-border">بین ۸ تا ۱۶ میلیون</li>
                                            <li role="button" onclick="setValueToInput('salary' , 'salaryBtn' , 'بین ۱۶ تا ۲۵ میلیون')" class="p-2 item-border">بین ۱۶ تا ۲۵ میلیون</li>
                                            <li role="button" onclick="setValueToInput('salary' , 'salaryBtn' , 'بالای ۲۵ میلیون')" class="p-2">بالای ۲۵ میلیون</li>
                                        </ul>
                                    </div>
                                </div>


                                <div class="mb-1">
                                    <div class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#age-collapse" aria-expanded="false">
                                        <img src="{{ asset('assets/frontend/img/search-jobs/user-clock-solid.svg') }}" class="img-fluid w-logo-menu">
                                        <h3 class="fs-5 mb-0 text-grey-s2 fw-medium">رنج سنی</h3>
                                    </div>
                                    <div class="collapse" id="age-collapse">

                                        @isset($oldData['age'])
                                            @if ($oldData['age'] != '' || ! is_null($oldData['age']))
                                                <input type="hidden" value="{{ $oldData['age'] }}" id="age_mo" name="age">
                                            @else
                                                <input type="hidden" id="age_mo" name="age">
                                            @endif
                                        @else
                                            <input type="hidden" id="age_mo" name="age">
                                        @endisset

                                        <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0">
                                            <li role="button" onclick="setValueToInput('age' , 'ageBtn' , 'زیر ۲۰ سال')" class="p-2 item-border">زیر ۲۰ سال</li>
                                            <li role="button" onclick="setValueToInput('age' , 'ageBtn' , '۲۱ تا ۳۰ سال')" class="p-2 item-border">۲۱ تا ۳۰ سال</li>
                                            <li role="button" onclick="setValueToInput('age' , 'ageBtn' , '۳۱ تا ۴۰ سال')" class="p-2 item-border">۳۱ تا ۴۰ سال</li>
                                            <li role="button" onclick="setValueToInput('age' , 'ageBtn' , 'بالای ۴۱ سال')" class="p-2">بالای ۴۱ سال</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="mb-1">
                                    <div class="d-flex gap-2 border-bottom border-secondery-subtle py-3 toggle-btn position-relative" data-bs-toggle="collapse" data-bs-target="#ulCity-collapse" aria-expanded="false">
                                        <img src="{{ asset('assets/frontend/img/search-jobs/location-dot-solid (1).svg') }}" class="img-fluid w-logo-menu">
                                        <h3 class="fs-5 mb-0 text-grey-s2 fw-medium">شهر</h3>
                                    </div>
                                    <div class="collapse" id="ulCity-collapse">

                                        @isset($oldData['city'])
                                            @if ($oldData['city'] != '' || ! is_null($oldData['city']))
                                                <input type="hidden" value="{{ $oldData['city'] }}" id="city_mo" name="city">
                                            @else
                                                <input type="hidden" id="city_mo" name="city">
                                            @endif
                                        @else
                                            <input type="hidden" id="city_mo" name="city">
                                        @endisset

                                        <ul class="list-unstyled fw-normal pb-1 fs-18 pe-2 mb-0 ulCity">
                                            <li class="p-2 item-border">استان مورد نظر را انتخاب کنید</li>
                                        </ul>
                                    </div>
                                </div>
                                
                            </div>

                        </div>

                    </div>




                {{-- job boxes  --}}

                    @if ($jobs->count() > 0)
                        <div class="mt-5">
                            @foreach ($jobs as $job)
                                @if ($job->type == 'intern')
                                    <div class="box-shadow-c pb-md-0 bg-white box-hover border-dash-green-job mb-5">
                                        <div class="bg-halfc-2 py-4 px-lg-5 px-3">
                                            <div class="d-flex justify-content-between align-items-start flex-md-row flex-column-reverse">
                                                <div class="d-flex align-items-center mb-4">
                                                    <div class="bg-gr-2 ms-3 px-2 py-3 rounded-4">
                                                        <img src="{{ asset('assets/frontend/img/company/user-pen-solid.png') }}" class="job-img" alt="موقعیت کارآموزی" height="47" width="60">
                                                    </div>
                                                    <div class="d-flex flex-column">
                                                        <span class="h5 fw-bold green-job fs-sm-18">{{ $job->title }}</span>
                                                        <span class="fs-6 text-grey fs-sm-14">{{ $job->job->category->title }}</span>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end align-self-end align-self-md-start">
                                                    <div class="icon-bg-2 d-flex align-items-center justify-content-center">
                    
                                                        <img src="{{ asset('assets/frontend/img/search-jobs/arrow-up-from-bracket-solid.svg') }}" class="w-small" alt="arrow up from bracket" height="20" width="18">
                                                    </div>
                                                    <div class="icon-bg-2 d-flex align-items-center justify-content-center">
                    
                                                        <img src="{{ asset('assets/frontend/img/search-jobs/heart-regular.svg') }}" class="w-small" alt="favorite icon" height="18" width="18">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                            
                                            @if ($job->province || $job->city)
                                                <div>
                                                    <div class="d-flex align-items-center">
                                                        <img src="{{ asset('assets/frontend/img/company/earth-asia-solid.png') }}" class="w-small ms-1" alt="آیکون آدرس" height="18" width="18">
                                                        <span class="grey ps-2 card-text fw-medium fs-sm-16">
                                                            @if ($job->province)
                                                                {{ $job->province->name }}
                                                            @endif
                                                            @if ($job->city)
                                                                {{ $job->city->name }}
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            <div class="d-flex flex-lg-row flex-column justify-content-between align-items-lg-center">
                                                <div>
                                                    <span class="grey card-text fw-medium fs-sm-16">{{ job_type_persian($job->type) }}</span>
                                                </div>
                                                <div class="mt-0 d-flex justify-content-end pt-1 align-items-lg-center flex-lg-row flex-column">
                                                    <div class="mb-sm-0 mb-2 d-flex flex-sm-row flex-column align-items-sm-center mt-sm-0 mt-2">
                                                        <span class="text-grey fs-7 ms-1">{{ verta($job->created_at)->formatDifference() }} توسط</span>
                                                        <strong class="text-dark">{{ $job->company->groupable->name }}</strong>
                                                    </div>
                                                    <a href="{{ route('job' , $job->id) }}" class="btn cv-btn-2 rounded-4 align-self-end text-dark me-2 mt-lg-0 mt-1 fs-sm-14">مشاهده اطلاعات</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                @else
                                    <div class="box-shadow-c pb-md-0 bg-white box-hover border-dash-blue-job mb-5">
                                        <div class="bg-halfc py-4 px-lg-5 px-3">
                                            <div class="d-flex justify-content-between align-items-start flex-md-row flex-column-reverse">
                                                <div class="d-flex align-items-center mb-4">
                                                    <div class="bg-gr ms-3 px-2 py-3 rounded-4">
                                                        <img src="{{ asset('assets/frontend/img/company/computer-solid.png') }}" class="job-img" alt="موقعیت شغلی" height="42" width="60">
                                                    </div>
                                                    <div class="d-flex flex-column">
                                                        <span class="h5 fw-bold blue fs-sm-18">{{ $job->title }}</span>
                                                        <span class="fs-6 text-grey fs-sm-14">{{ $job->job->category->title }}</span>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end align-self-end align-self-md-start">
                                                    <div class="icon-bg d-flex align-items-center justify-content-center">
                    
                                                        <img src="{{ asset('assets/frontend/img/search-jobs/arrow-up-from-bracket-solid.svg') }}" class="w-small" alt="arrow up from bracket" height="20" width="18">
                                                    </div>
                                                    <div class="icon-bg d-flex align-items-center justify-content-center">
                    
                                                        <img src="{{ asset('assets/frontend/img/search-jobs/heart-regular.svg') }}" class="w-small" alt="favorite icon" height="18" width="18">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                            
                                            @if ($job->province || $job->city)
                                                <div>
                                                    <div class="d-flex align-items-center">
                                                        <img src="{{ asset('assets/frontend/img/company/earth-asia-solid.png') }}" class="w-small ms-1" alt="آیکون آدرس" height="18" width="18">
                                                        <span class="grey ps-2 card-text fw-medium fs-sm-16">
                                                            @if ($job->province)
                                                                {{ $job->province->name }}
                                                            @endif
                                                            @if ($job->city)
                                                                {{ $job->city->name }}
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            <div class="d-flex flex-lg-row flex-column justify-content-between align-items-lg-center">
                                                <div>
                                                    <span class="grey card-text fw-medium fs-sm-16">{{ job_type_persian($job->type) }}</span>
                                                </div>
                                                <div class="mt-0 d-flex justify-content-end pt-1 align-items-lg-center flex-lg-row flex-column">
                                                    <div class="mb-sm-0 mb-2 d-flex flex-sm-row flex-column align-items-sm-center mt-sm-0 mt-2">
                                                        <span class="text-grey fs-7 ms-1">{{ verta($job->created_at)->formatDifference() }} توسط</span>
                                                        <strong class="text-dark">{{ $job->company->groupable->name }}</strong>
                                                    </div>
                                                    <a href="{{ route('job' , $job->id) }}" class="btn cv-btn rounded-4 align-self-end text-dark me-2 mt-lg-0 mt-1 fs-sm-14">مشاهده اطلاعات</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            
                        </div>
                        @else
                        
                        @endif
                        
                    </div>
                    {{$jobs->links('pagination.frontend')}}
            </div>
        </div>
        
    </form>

    

<input type="hidden" id="getCitiesUrl" value="{{ route('get.cities.ajax.front') }}">
@endsection

@section('script')
    <script src="{{ asset('assets/frontend/js/jobs-search.js') }}"></script>
@endsection