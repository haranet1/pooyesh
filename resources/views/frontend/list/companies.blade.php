@extends('frontend.layout.master')

@section('meta')
    <meta name="description"  content="با جابیست، اگر به دنبال جستجوی شرکت‌های برتر و معتبر در هر صنعت هستید؟ اینجا جایی است که می‌توانید به سرعت شرکت‌هایی را که به دنبال کارآموزی یا استخدام هستند پیدا کنید. با ما، به دنبال شرکت‌هایی با محیط کاری مناسب و فرصت‌های شغلی جذاب باشید." />
    <meta property="og:url" content="https://jobist.ir/list/companies"/>
    <meta property="og:title" content=" جستجوی شرکت‌ها با جابیست | پیدا کردن بهترین محیط‌های کاری و فرصت‌های شغلی"/>
    <meta property="og:description" content=" با جابیست، اگر به دنبال جستجوی شرکت‌های برتر و معتبر در هر صنعت هستید؟ اینجا جایی است که می‌توانید به سرعت شرکت‌هایی را که به دنبال کارآموزی یا استخدام هستند پیدا کنید. با ما، به دنبال شرکت‌هایی با محیط کاری مناسب و فرصت‌های شغلی جذاب باشید." /> 
    <link rel="canonical" href="https://jobist.ir/list/companies"/>
    <link rel="preload" href="{{asset('assets/frontend/img/home/sale-Overlay-min2.webp')}}" as="image"/>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/companies-style.css') }}">    
@endsection

@section('title' , 'پیدا کردن بهترین محیط‌های کاری و فرصت‌های شغلی')
@section('page' , 'جستجوی شرکت‌ها با جابیست ' )

@section('main')
    
    <div class="bg-pattern container-fluid py-5">
        <div class="container">
            <h1 class="h2 text-white">جست و جوی شرکت ها</h1>
            <p class="text-white fw-normal mb-4 h6">برای بهترین شرکت های جهان کار کنید.</p>
            <div class="row rounded-3 bg-white py-2 input-shadow mb-3">
                <form action="{{ route('companies.search') }}" method="GET" class="d-flex">
                    {{-- @csrf --}}
                    <div class="align-self-center ms-2">
                        <i class="fa-solid fa-building-user light-grey2 fs-4"></i>
                    </div>

                    <input type="search" name="name" placeholder="نام شرکت" class="select-custom bg-white me-2 input"
                    @isset($name)
                        value="{{ $name }}"
                    @endisset
                    >

                    <div class="align-self-center">
                        <button type="submit" class="btn" aria-label="search button">
                            <i class="fa-solid fa-magnifying-glass light-grey2 fs-4"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="mt-5">
            <div class="row row-cols-lg-3 row-gap-5 row-cols-sm-2 row-cols-1 pb-4 px-3 pt-5">
            @if ($companies-> count() > 0)
                    @foreach ($companies as $company)
                        <div class="col px-4">
                            <a href="{{ route('company.single' , $company->id) }}">
                                <div class="companies-box d-flex flex-column align-items-center p-3 heigh-c-fixed justify-content-between">
                                    
                                    @isset($company->groupable->logo)
                                        <div class="logo-width-h rounded-circle overflow-hidden logo-shadow position-relative">

                                            <img src="{{ asset($company->groupable->logo->path) }}" class="img-wh position-absolute p-1" alt="لوگوی شرکت {{$company->name}}">
                                        </div>
                                    @else
                                        <div class="logo-width-h rounded-circle overflow-hidden logo-shadow position-relative">

                                            <img src="{{ asset('assets/front/images/company-logo-1.png') }}" class="img-wh position-absolute" alt="لوگوی شرکت {{$company->name}}">
                                        </div>
                                    @endisset

                                            <div class="d-flex align-items-center flex-column w-100">
                                                <h2 class="fs-4 mt-3 fw-semibold mb-3 blue text-center">{{ $company->groupable->name }}</h2>

                                                <p class="mb-3 grey text-center fs-16 px-2 fw-semibold">{{ Str::limit(strip_tags($company->groupable->about), 50, '...') }}</p>
                                            </div>
                                            
                                                
                                            
                                            <div class="d-flex align-items-center job-counts bg-white gap-2 py-2 px-4 justify-content-between">
                                                <span class="grey fw-medium fanumber text-center">
                                                    @isset($company->hire_job)
                                                        @if($company->hire_job > 0)
                                                            موقعیت شغلی: {{ $company->hire_job }}
                                                        @else
                                                            بدون موقعیت شغلی
                                                        @endif
                                                    @else
                                                        بدون موقعیت شغلی
                                                    @endisset
                                                    
                                                </span>
                                                <span class="grey fw-medium fanumber text-center">
                                                    @isset($company->intern_job)
                                                        @if($company->intern_job > 0)
                                                            موقعیت کارآموزی: {{ $company->intern_job }}
                                                        @else
                                                            بدون موقعیت کارآموزی
                                                        @endif
                                                    @else
                                                        بدون موقعیت کارآموزی
                                                    @endisset
                                                </span>
                                            </div>
                                            {{-- <div class="d-flex align-items-center job-counts bg-white">
                                                <div class="">
                                                    <span class="grey">بدون شغل</span> 
                                                </div>
                                            </div> --}}
                                                    
                                        
                                         
                                </div>
                            </a>
                        </div>

                    @endforeach
            @else
                                            
            @endif
            </div>
        </div> 
        {{$companies->links('pagination.frontend')}}
    </div>




@endsection