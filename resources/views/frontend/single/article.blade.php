@extends('frontend.layout.master')
@section('meta')
    <meta name="description" content="{{e(Str::limit(strip_tags($html), 155))}}"/>
    <meta name="keywords" content="{{$article->keywords ?? 'جابیست'}}">
    <meta property="og:title" content="مقاله | {{$article->title}} - [جابیست]"/>
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{{e(Str::limit(strip_tags($html), 155))}}"/>
    <meta property="og:url" content="https://jobist.ir/mag/{{$article->slug}}"/>
    <meta property="og:image" content="{{asset($article->photo->path)}}"/>
    <meta property="og:site_name" content="جابیست">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="{{$article->title}} - [جابیست]">
    <meta name="twitter:description" content="{{e(Str::limit(strip_tags($html), 155))}}">
    <meta name="twitter:image" content="{{asset($article->photo->path)}}">
    <meta name="robots" content="index, follow" />
    <link rel="canonical" href="{{url()->current()}}"/>

@endsection
@section('title' , $article->title)
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/article-style.css') }}"> 
@endsection
@section('main')
@if ($article)


<div class="container-fluid top-section-article">
    <div class="mt-0">
        <div class="row flex-md-row flex-column-reverse py-1 align-items-center h-fix-top">
            <div class="col-md-7 col-12 mt-md-5 mt-0 py-4 pe-3">


                <div class="mt-md-2 mt-3 px-md-5 px-1">

                    <div class="mt-3">
                        <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 mt-2 margin-brand d-flex align-items-center">
                            <i class="fa-solid fa-layer-group ms-2"></i>
                            <div data="{{ $article->id }}" id="article">
                                گروه : {{$article->category->title}}
                            </div>
                        </div>
                    </div>
                
                        
                    <h2 class="blue fw-bold h4 mt-3">{{$article->title}}</h2>


                    <div class="d-flex justify-content-end align-items-center p-sm-2 p-1  mt-1">
                        <div class="d-flex fs-7 ms-1 fw-semibold blue view-blog border-start border-secondery-subtle">
                            <div class=" align-self-center">
                                <i class="fa-regular fa-eye ms-1"></i>
                            </div>
                            <div class="ms-1 align-self-center">{{$article->views}}</div>
                        </div>

                        <div class="d-flex fs-7 ms-1 fw-semibold blue view-blog border-start border-secondery-subtle">
                            <div class="align-self-center">
                                <i class="fa-regular fa-heart ms-1"></i>
                            </div>
                            <div class="ms-1 align-self-center" id="likes-count">{{ $article->likes->count() }}</div>
                        </div>

                        <div class="d-flex fs-7 me-2 fw-semibold blue">
                            <div class=" align-self-center">
                                <i class="fa-solid fa-calendar-days ms-1"></i>
                            </div>
                            <div class="ms-1 align-self-center">{{ verta($article->created_at)->format('%d %B %Y') }}</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-5 col-12 p-0">
                <div class="img-article-wrapper mx-auto">
                    <img src="{{asset($article->photo->path)}}" alt="article-image" loading="lazy" class="article-img">
                </div>
            </div>
        </div>
    </div>

</div>


<div class="bg-art-article">
    <div class="container mt-0">
        <div class="pt-4">
            {{ Breadcrumbs::render('article', $article) }}
        </div>


        <div class="row">

            <div class="col-12 col-md-9 mt-3 px-0 px-sm-3">
                @if ($article)

                <div class="article-content bg-white p-md-5 p-3 rounded-4">
                    {!! $html !!}
                </div>
                @endif

                <div class="d-md-none d-block sidebar-sticky-md pt-4">
                    <div class="side-blog mb-0 collapse" id="collapseMenuGuide">
                        <div class="mb-1">
                            <div class="" id="sidebar">
                                <ul class="list-unstyled fw-normal pb-2 fs-6 px-3 pb-2 mb-0 toc">
                
                                    @foreach ($index as $item)
                                        <li role="button" class="p-2 border-bottom border-secondery-subtle
                                            @switch($item['h'])
                                                @case(3)
                                                    pe-toc-3
                                                    @break
                                            
                                                @case(4)
                                                    pe-toc-4
                                                    @break
                                            
                                                @case(5)
                                                    pe-toc-5
                                                    @break
                                            @endswitch
                                        ">
                                            <a data-target="{{ $item['slug'] }}" class="d-flex">
                                                <i class="fa-solid fa-caret-left ms-2"></i>
                                                <div class="text-dark">{{ $item['text'] }}</div>
                                            </a>
                                        </li>
                                    @endforeach
                
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{-- mobile --}}
                    <div class="d-flex flex-row justify-content-between align-items-center p-1 bg-darkgrey">
                        <div class="pointer" data-bs-toggle="collapse" data-bs-target="#collapseMenuGuide">
                            <i class="fa-solid fa-circle-question fs-5 blue me-2"></i>
                        </div>
                        <div class="wrap">
                            @if (!Auth::check())
                            <svg data-bs-toggle="popover" data-bs-title="string" data-bs-custom-class="custom-popover" data-bs-placement="left" data-bs-content="لطفا ابتدا به حساب کاربری خود وارد شوید" width="50" height="50" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 680 680" xml:space="preserve">
        
                                <g class="heartdisable">
                                    <path d="m493.9 212.7c-35.7-35.7-93.5-35.7-129.1 0-12.4 12.3-20.4 27.4-24.2 43.2-3.8-15.8-11.8-30.8-24.2-43.2-35.7-35.7-93.5-35.7-129.1 0-35.7 35.7-35.7 93.5 0 129.1l153.3 153.4 153.4-153.4c35.6-35.6 35.6-93.4-.1-129.1z"/>
                                </g>
                            </svg>
                            @else
                              <svg class="likeButton {{ $article->isLikedByUser(auth()->id()) ? 'like' : '' }}"
                                  data-article-id="{{ $article->id }}" width="50" height="50" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 680 680" xml:space="preserve">
        
                                    <g id="heart">
                                    <path d="m493.9 212.7c-35.7-35.7-93.5-35.7-129.1 0-12.4 12.3-20.4 27.4-24.2 43.2-3.8-15.8-11.8-30.8-24.2-43.2-35.7-35.7-93.5-35.7-129.1 0-35.7 35.7-35.7 93.5 0 129.1l153.3 153.4 153.4-153.4c35.6-35.6 35.6-93.4-.1-129.1z"/>
                                    </g>
                                    <g id="circle">
                                    <circle class="st0" cx="340" cy="340" r="220"/>
                                    </g>
                                    <g id="particle">
                                    <path d="m159.4 190.6-47.1-51.4 26.1-15.1z"/>
                                    <path d="m135.9 217.5-66.5-20.9 15.1-26.2z"/>
                                    <path d="m521.7 190.2 47.1-51.4-26.1-15.1z"/>
                                    <path d="m545.2 217.2 66.5-21-15-26.1z"/>
                                    <path d="m521.7 490.5 47.1 51.5-26.1 15z"/>
                                    <path d="m545.2 463.6 66.5 20.9-15 26.2z"/>
                                    <path d="m159.4 490.9-47.1 51.4 26.1 15.1z"/>
                                    <path d="m135.9 463.9-66.5 21 15.1 26.1z"/>
                                    </g>
                              </svg>
        
                          @endif
                        </div>
                        <div class="pointer ms-2" type="button" data-bs-toggle="modal" data-bs-target="#shareModal2">
                            <i class="fa-solid fa-share-nodes fs-4 grey2"></i>
                        </div>
                    </div>

                </div>
                <div class="modal fade" id="shareModal2" tabindex="-1" aria-labelledby="shareModal2" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                      <div class="modal-content">
                        <div class="modal-header text-center">
                          <h1 class="modal-title fs-5 fw-bold" id="exampleModalLabel">اشتراک گذاری</h1>
                          <button type="button" class="btn-close share-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                          <div class="d-flex align-items-center justify-content-center fs-3">
                            <a href="https://telegram.me/share/url?url={{ urlencode(url()->current()) }}&text={{ urlencode($article->title) }}" target="_blank" class="social-madia-icon">
                                <img src="{{asset('assets/frontend/img/blog/telegram-app.svg')}}" alt="">
                            </a>
                            <a href="https://twitter.com/intent/tweet?url={{ urlencode(url()->current()) }}&text={{ urlencode($article->title) }}" class="social-madia-icon" target="_blank">
                                <img src="{{asset('assets/frontend/img/blog/twitterx.svg')}}" alt="">
                            </a>
                            <a href="https://api.whatsapp.com/send?text={{ urlencode($article->title . ' ' . url()->current()) }}" class="social-madia-icon" target="_blank">
                                <img src="{{asset('assets/frontend/img/blog/whatsapp.svg')}}" alt="">
                            </a>
                            <a href="https://www.linkedin.com/sharing/share-offsite/?url={{ urlencode(url()->current()) }}" class="social-madia-icon" target="_blank">
                                <img src="{{asset('assets/frontend/img/blog/linkedin.svg')}}" alt="">
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>

            {{-- pc  --}}

            <div class="col-12 col-md-3 d-md-block d-none mt-3">
                <div class="sidebar-sticky">
                    <div class="side-blog mb-3">
                        <div class="mb-1">
                            <div class="d-flex gap-2 border-bottom border-secondery-subtle p-3 toggle-btn" id="sidebar-title" data-bs-toggle="collapse" data-bs-target="#sidebar" aria-expanded="true">
                                <div class="align-self-center">
                                    <i class="fa-solid fa-angle-down fs-5"></i>
                                </div>
                                <h3  class="fs-5 mb-0 text-grey-s2 fw-medium">راهنمای مقاله</h3>
                            </div>
                            <div class="collapse show" id="sidebar">
                                <ul class="list-unstyled fw-normal pb-2 fs-6 px-3 pb-5 mb-0 toc">

                                    @foreach ($index as $item)
                                        <li role="button" class="p-2 border-bottom border-secondery-subtle
                                            @switch($item['h'])
                                                @case(3)
                                                    pe-toc-3
                                                    @break
                                            
                                                @case(4)
                                                    pe-toc-4
                                                    @break
                                            
                                                @case(5)
                                                    pe-toc-5
                                                    @break
                                            @endswitch
                                        ">
                                            <a data-target="{{ $item['slug'] }}" class="d-flex align-items-center">
                                                <i class="fa-solid fa-caret-left ms-2"></i>
                                                <div>{{ $item['text'] }}</div>
                                            </a>
                                        </li>
                                    @endforeach
    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="side-blog-2">
                        <div class="mb-1">
                            <div class="d-flex p-2 align-items-center">
                                <div class="wrap">
                                    @if (!Auth::check())
                                    <svg data-bs-toggle="popover" data-bs-title="string" data-bs-custom-class="custom-popover" data-bs-placement="left" data-bs-content="لطفا ابتدا به حساب کاربری خود وارد شوید" width="50" height="50" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 680 680" xml:space="preserve">

                                        <g class="heartdisable">
                                            <path d="m493.9 212.7c-35.7-35.7-93.5-35.7-129.1 0-12.4 12.3-20.4 27.4-24.2 43.2-3.8-15.8-11.8-30.8-24.2-43.2-35.7-35.7-93.5-35.7-129.1 0-35.7 35.7-35.7 93.5 0 129.1l153.3 153.4 153.4-153.4c35.6-35.6 35.6-93.4-.1-129.1z"/>
                                        </g>
                                    </svg>
                                    @else
                                      <svg class="likeButton {{ $article->isLikedByUser(auth()->id()) ? 'like' : '' }}"
                                          data-article-id="{{ $article->id }}" width="50" height="50" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 680 680" xml:space="preserve">

                                            <g id="heart">
                                            <path d="m493.9 212.7c-35.7-35.7-93.5-35.7-129.1 0-12.4 12.3-20.4 27.4-24.2 43.2-3.8-15.8-11.8-30.8-24.2-43.2-35.7-35.7-93.5-35.7-129.1 0-35.7 35.7-35.7 93.5 0 129.1l153.3 153.4 153.4-153.4c35.6-35.6 35.6-93.4-.1-129.1z"/>
                                            </g>
                                            <g id="circle">
                                            <circle class="st0" cx="340" cy="340" r="220"/>
                                            </g>
                                            <g id="particle">
                                            <path d="m159.4 190.6-47.1-51.4 26.1-15.1z"/>
                                            <path d="m135.9 217.5-66.5-20.9 15.1-26.2z"/>
                                            <path d="m521.7 190.2 47.1-51.4-26.1-15.1z"/>
                                            <path d="m545.2 217.2 66.5-21-15-26.1z"/>
                                            <path d="m521.7 490.5 47.1 51.5-26.1 15z"/>
                                            <path d="m545.2 463.6 66.5 20.9-15 26.2z"/>
                                            <path d="m159.4 490.9-47.1 51.4 26.1 15.1z"/>
                                            <path d="m135.9 463.9-66.5 21 15.1 26.1z"/>
                                            </g>
                                      </svg>

                                  @endif
                                </div>
                                <div class="pointer me-2" type="button" data-bs-toggle="modal" data-bs-target="#shareModal">
                                    <i class="fa-solid fa-share-nodes fs-4 grey2"></i>
                                </div>

                                  
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal fade" id="shareModal" tabindex="-1" aria-labelledby="shareModal" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header text-center">
                      <h1 class="modal-title fs-5 fw-bold" id="exampleModalLabel">اشتراک گذاری</h1>
                      <button type="button" class="btn-close share-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <div class="d-flex align-items-center justify-content-center fs-3">
                        <a href="https://telegram.me/share/url?url={{ urlencode(url()->current()) }}&text={{ urlencode($article->title) }}" target="_blank" class="social-madia-icon">
                            <img src="{{asset('assets/frontend/img/blog/telegram-app.svg')}}" alt="">
                        </a>
                        <a href="https://twitter.com/intent/tweet?url={{ urlencode(url()->current()) }}&text={{ urlencode($article->title) }}" class="social-madia-icon" target="_blank">
                            <img src="{{asset('assets/frontend/img/blog/twitterx.svg')}}" alt="">
                        </a>
                        <a href="https://api.whatsapp.com/send?text={{ urlencode($article->title . ' ' . url()->current()) }}" class="social-madia-icon" target="_blank">
                            <img src="{{asset('assets/frontend/img/blog/whatsapp.svg')}}" alt="">
                        </a>
                        <a href="https://www.linkedin.com/sharing/share-offsite/?url={{ urlencode(url()->current()) }}" class="social-madia-icon" target="_blank">
                            <img src="{{asset('assets/frontend/img/blog/linkedin.svg')}}" alt="">
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="container mt-0">
        <div class="row">
            @if ($suggestion->count() > 0)
            <div class="col-md-12 col-12">
                <div class="border-dash-purple mt-7 px-sm-4 px-2 py-4">
                    <h3 class="title-next fs-4">از همین دسته بندی</h3>
                        <div class="p-md-3 p-2 rounded-4 position-relative z-3">

                                <div class="row row-cols-lg-3 row-cols-md-2 row-cols-1  row-gap-5">
                                    @foreach ($suggestion as $suggested)
                                        <div class="col py-3">
                                            <a href="{{route('single.article.blog', $suggested->slug ) }}">
                                                <div class="d-flex flex-column blog-box2 box-hover px-2 justify-content-between">
                                                    <div class="d-flex flex-column px-2 justify-content-around h-100">
            
                                                        <div class="position-relative d-flex justify-content-center h-fix-card">
                                                            <div class="img-blog-wrapper2">
                                                                <img src="{{asset($suggested->photo->path)}}" alt="article-image" loading="lazy" class="blog-img2">
                                                            </div>
                                                            <div class="category-blog2">{{$suggested->category->title}}</div>
                                                        </div>
                
                                                        <div class="d-flex flex-column column-gap-2 justify-content-between w-100">
                                                                <h4 class="fs-6 my-3 fw-semibold text-dark mx-2">{{$suggested->title}}</h4>
                                                        </div>
                
                                                    </div>
            
                                                    <div class="d-flex justify-content-between align-items-center p-sm-2 p-1 border-top border-secondery-subtle mt-1">
                                                        <div class="d-flex">
                                                            <div class="d-flex fs-7 ms-1 fw-semibold blue view-blog">
                                                                <div class=" align-self-center">
                                                                    <i class="fa-regular fa-eye ms-1"></i>
                                                                </div>
                                                                <div class="ms-1 align-self-center">{{ $suggested->views }}</div>
                                                            </div>
                
                                                            <div class="d-flex fs-7 ms-1 fw-semibold blue view-blog">
                                                                <div class=" align-self-center">
                                                                    <i class="fa-regular fa-heart ms-1"></i>
                                                                </div>
                                                                <div class="ms-1 align-self-center">{{ $suggested->likes->count() }}</div>
                                                            </div>
                                                        </div>

            
                                                        <div class="d-flex fs-7 me-2 fw-semibold blue">
                                                            <div class=" align-self-center">
                                                                <i class="fa-solid fa-calendar-days ms-1"></i>
                                                            </div>
                                                            <div class="ms-1 align-self-center">{{ verta($suggested->created_at)->format('%d %B %Y') }}</div>
                                                        </div>
                                                    </div>
                                                </div>
            
                                            </a>
                                        </div>
    
                                    @endforeach
                                </div>
        
                    </div>
                </div>
            </div>
            @endif

            <div class="col-md-9 col-12">
                <div class="box-shadow-c mt-7 p-4 rounded-4">
                    <div class="d-flex align-items-center">
                        <i class="fa-regular fa-message ms-2"></i>
                        <div class="blue fw-bold h5 mb-0">دیدگاه شما</div>
                    </div>
                    @if(Auth::check())
                        @if(Auth::user()->IsConfirmed())
                            <form method="POST" action="{{ route('store.comment.article') }}">
                                @csrf
                                <div class="mt-3">
                                    <input type="hidden" name="commentable_id" value="{{ $article->id }}">
                                        <textarea class="form-control comment-input" name="body" id="text" placeholder="نظر شما..." rows="5"></textarea>
                                </div>
                                <div class="text-start mt-3">
                                    <button type="submit" class="btn btn-send rounded-3 bg-blue text-white fs-18 px-5">ارسال</button>
                                </div>
                            </form>
                        @else
                            <div class="mt-3">
                                <textarea class="form-control comment-input" placeholder="برای ارسال دیدگاه باید حساب کاربری شما تایید شده باشد." rows="1" disabled></textarea>
                            </div>
                        @endif
                    @else
                        <div class="mt-3">
                            <textarea class="form-control comment-input" placeholder="لطفا برای ارسال دیدگاه وارد حساب کاربری خود شوید" rows="1" disabled></textarea>
                        </div>
                        <div class="text-start mt-3">
                            <a type="button" href="{{route('login')}}"  class="btn btn-send rounded-3 bg-blue text-white fs-18 px-5">ورود</a>
                        </div>
                    @endif
                </div>
                @auth
                    @if($comments->count() > 0)
                        <div class="mt-5">
                            @foreach ($comments as $comment)

                                <div style="margin-right: {{ $comment->parent_id ? '20px' : '0px' }};" class="box-shadow-c mt-3 p-md-3 p-1 rounded-4 bg-lightgrey">
                                    <div class="comment-body">
                                        <div class="p-md-3 p-2 bg-white rounded-4 position-relative {{ $comment->confirmedReplies->count() > 0 ? 'mb-5' : '' }}">
                                            <div class="d-flex align-items-center p-1 flex-column flex-md-row">
                                                <div class="col-right user-comment mx-sm-2 px-0">
                                                    @if($comment->user->sex == 'female')
                                                        <img src="{{ asset('assets/frontend/img/candidate/famale.png') }}" alt="" class="img-fluid">
                                                    @else
                                                        <img src="{{ asset('assets/frontend/img/candidate/male.svg') }}" alt="" class="img-fluid">
                                                    @endif
                                                    <div class="mt-1 text-center">
                                                        <span class="fs-14">{{ $comment->user->name. ' '. $comment->user->family }}</span>
                                                    </div>
                                                </div>

                                                <div class="col-left p-2 fs-6 fw-medium">
                                                    {{ $comment->body }} 
                                                </div>
                                            </div>
                                            <div class="text-start">
                                                <div class="btn btn-send rounded-3 bg-blue pointer fs-14 px-2 mt-1" data-bs-toggle="collapse" data-bs-target="#comment-{{ $comment->id }}" aria-expanded="true">
                                                    <span class="text-white">پاسخ</span>
                                                    <span class="text-white me-1">
                                                        <i class="fa-solid fa-reply fs-14"></i>
                                                    </span>
                                                </div>
                                            </div>

                                            @if ($comment->confirmedReplies->count() > 0)
                                                <div class="text-start open-comments">
                                                    <div class="position-relative">
                                                        <img src="{{ asset('assets/frontend/img/blog/svg-path.svg') }}" alt="" class="img-fluid">
                                                        <span class="pointer blue p-2 fs-4 angleDown" onclick="angleDownRotate(this)" data-bs-toggle="collapse" data-bs-target="#reply-{{ $comment->id }}" aria-expanded="true">
                                                            <i class="fa-solid fa-angle-down"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>



                                        <div class="text-start">
                                            <div class="collapse p-3" id="comment-{{ $comment->id }}">
                                                <form method="POST" action="{{ route('store.comment.article') }}">
                                                    @csrf
                                                    <input type="hidden" name="commentable_id" value="{{ $article->id }}">
                                                    <input type="hidden" name="parent_id" value="{{ $comment->id }}">
                                                    <textarea class="form-control comment-input" name="body" rows="5" required></textarea>
                                                    <button type="submit" class="btn btn-send rounded-3 bg-blue text-white fs-6 px-5 mt-3">ارسال</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    @if ($comment->confirmedReplies->count() > 0)
                                    
                                        <div class="mt-3 collapse" id="reply-{{ $comment->id }}">
                                            @include('partials.comment', ['comments' => $comment->confirmedReplies, 'depth' => 1])
                                        </div>
                                    @endif



                                </div>

                            @endforeach
                        </div>

                    @endif
                @endauth
            </div>
        </div>







    </div>


    
    <input type="hidden" value="{{ route('additionView.article') }}" id="additionViewUrl">
    <input type="hidden" value="{{ route('like.article') }}" id="likeUrl">
</div>

@endif
@endsection

@section('script')
<script>
    document.querySelectorAll('.toc li a').forEach(anchor => {
        
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            const targetId = this.getAttribute('data-target');
            const targetElement = document.getElementById(targetId);
            const rect = targetElement.getBoundingClientRect();

            window.scrollTo({
                top: rect.y + globalThis.scrollY - 150,
                left: 0,
                behavior: "smooth",
            });

        });
    });

    
</script>
    <script src="{{ asset('assets/frontend/js/article.js') }}"></script>
@endsection

