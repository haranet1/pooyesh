@extends('frontend.layout.master')
@section('meta')
    <meta name="description" content="مشاهده رزومه و توانمندی‌های کارجو، سابقه کاری، تحصیلات و مهارت‌ها. این صفحه به شما کمک می‌کند تا با استعدادهای کارجو بیشتر آشنا شوید و فرصت‌های شغلی مناسب را بیابید." />
    <meta property="og:url" content="https://jobist.ir/single/candidate/{{$user->id}}"/>
    <meta property="og:title" content=" صفحه اختصاصی کارجو | جابیست " />
    <meta property="og:description" content="مشاهده رزومه و توانمندی‌های کارجو، سابقه کاری، تحصیلات و مهارت‌ها. این صفحه به شما کمک می‌کند تا با استعدادهای کارجو بیشتر آشنا شوید و فرصت‌های شغلی مناسب را بیابید." /> 
    <link rel="canonical" href="https://jobist.ir/single/candidate/{{$user->id}}"/>
@endsection
@section('title', 'پروفایل کارجو')
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/single-candidate.css') }}">
@endsection

@section('main')


<div class="container mt-md-4 mt-0">

    <img src="{{ asset('assets/frontend/img/candidate/top-image.png') }}" class="img-fluid bottom-n d-none d-md-inline">

    <div class="row px-xl-5 px-2">
        <div class="col-lg-3 px-4" >
        <div class="" style="position:sticky; top:90px;">
            <div class="bg-darkgrey rounded-4 box-shadow-c py-4 px-xl-4 px-lg-3 px-3 mb-5" >
                <div class="d-flex flex-column flex-sm-row flex-lg-column border-bottom border-dark-subtle">

                    <div class="img-student text-center mx-auto mx-sm-2 mx-lg-auto p-3 student-pic-wrraper">
                        @if($user->sex == 'female')

                            <img src="{{ asset('assets/frontend/img/candidate/famale.png') }}" alt="آواتار کارجو" class="img-fluid student-pic">

                        @else

                            <img src="{{ asset('assets/frontend/img/candidate/male.svg') }}" alt="آواتار کارجو" class="img-fluid student-pic">

                        @endif
                    </div>

                    <div class="d-flex align-items-center justify-content-center mt-3 gap-1 pb-3">
                        <div class="dot-lightgreen"></div>
                        <span class="blue fs-5 fw-medium">{{ $user->name . ' ' . $user->family }}</span>
                    </div>
                </div>

                <div class="mt-4">
                    {{-- <div>
                        <h5 class="fs-6 mb-2 text-grey">حوزه کاری:</h5>
                        <p class="fs-6 text">فناوری اطلاعات و ارتباطات</p>
                    </div> --}}
                    @hasanyrole('company|employee')
                        <div class="mt-md-3 mt-2">
                            <h5 class="fs-6 mb-2 grey">تلفن:</h5>
                            <p class="fs-6 text">{{ $user->mobile }}</p>
                        </div>
                    @endhasallroles

                    @if ($user->groupable->city)
                        <div>
                            <h5 class="fs-6 mb-2 grey">شهر :</h5>
                            <p class="fs-6 text">{{$user->groupable->city->name}}</p>
                        </div>
                    @endif

                    <div class="mt-md-3 mt-2">
                        <h5 class="fs-6 mb-2 grey">ایمیل:</h5>
                        <p class="fs-6 text">{{$user->email}}</p>
                    </div>
                </div>
                @if (count($user->social_networks) > 0)
                    <div>
                        <h5 class="fs-6 mb-2 grey mb-3">شبکه های اجتماعی:</h5>
                        @foreach ($user->social_networks as $socialNetwork)
                            <div class="bg-light-blue d-flex w-100 rounded-4 justify-content-end align-items-center p-1 mb-2">
                                <span class="ms-2 fs-6 text text-end">{{ $socialNetwork->link }}</span>
                                <div class="bg-blue rounded-circle circle-icon d-flex align-items-center justify-content-center">
                                    @if($socialNetwork->name == 'instagram')
                                        <img src="{{ asset('assets/frontend/img/candidate/insta.png') }}" class="social-icon" alt="instagram icon" height="23" width="23">
                                    @elseif($socialNetwork->name == 'linkedin')
                                        <img src="{{ asset('assets/frontend/img/candidate/linkdin.png') }}" class="social-icon" alt="linkedin icon" height="23" width="23">
                                    @elseif($socialNetwork->name == 'telegram')
                                        <img src="{{ asset('assets/frontend/img/candidate/tell.png') }}" class="social-icon" alt="telegram icon" height="23" width="23">
                                    @elseif($socialNetwork->name == 'eitaa')
                                        <img src="{{ asset('assets/frontend/img/candidate/eitaa.svg') }}" class="social-icon" alt="eitaa icon" height="23" width="23">
                                    @elseif($socialNetwork->name == 'twitter')
                                        <img src="{{ asset('assets/frontend/img/candidate/twitter.svg') }}" class="social-icon" alt="twitter icon" height="23" width="23">
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>

            @auth
                @if (Auth::user()->groupable_type == \App\Models\CompanyInfo::class)
                    @if ($user->hasRole('student'))
                        <div class="bg-darkgrey rounded-4 box-shadow-c py-4 px-xl-4 px-lg-3 px-3 mb-5" >
                            <h3 class="fs-5 text">ارسال درخواست همکاری</h3>
                            <p class="fs-18 text-grey fw-medium">موقعیت کارآموزی / شغلی</p>
                            <select name="job-position" id="job" class="form-select text-grey">
                                <option selected value="">انتخاب کنید</option>
                                @foreach ($job_positions as $job)
                                    <option value="{{ $job->id }}" jobType="{{ $job->type == 'intern' ? 'true' : 'false' }}">
                                        {{job_type_persian($job->type)}} -
                                        {{$job->title}}
                                    </option>
                                @endforeach
                            </select>
                            @if ($companyCanSendRequest)
                                <button class="bg-blue btn text-white rounded-4 mt-3 w-100 fw-bold py-2 " id="work-request">
                                    ارسال درخواست
                                </button>
                            @else
                                <button class="bg-dark btn text-white rounded-4 mt-3 w-100 fw-bold py-2 pe-none">
                                    شما قبلا برای این کارجو درخواست فرستاده اید
                                </button>
                            @endif
                        </div>
                    @else
                        <div class="bg-darkgrey rounded-4 box-shadow-c py-4 px-xl-4 px-lg-3 px-3 mb-5">
                            <h3 class="fs-5 text">ارسال درخواست همکاری</h3>
                            <p class="fs-18 text-grey fw-medium">موقعیت شغلی</p>
                            <select name="job-position" id="job" class="form-select text-grey">
                                <option selected value="">انتخاب کنید</option>
                                @foreach ($job_positions as $job)
                                    @if ($job->type != 'intern')
                                        <option value="{{ $job->id }}" jobType="{{ $job->type == 'intern' ? 'true' : 'false' }}">
                                            {{job_type_persian($job->type)}} -
                                            {{$job->title}}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($companyCanSendRequest)
                                <button class="bg-blue btn text-white rounded-4 mt-3 w-100 fw-bold py-2 " id="work-request">
                                    ارسال درخواست
                                </button>
                            
                            @else
                                <button class="bg-dark btn text-white rounded-4 mt-3 w-100 fw-bold py-2 pe-none">
                                    شما قبلا برای این کارجو درخواست فرستاده اید
                                </button>
                            @endif

                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                        </div>
                    @endif
                @endif
            @else

            @endauth
        </div>
            

        </div>
        <div class="col-lg-9 px-4">

            @if ($user->academicInfos && count($user->academicInfos) > 0)
                <div class="box-shadow-c bg-darkgrey p-4 rounded-4 mb-5">
                    <div class="bg-blue w-fit rounded-top-only move-right position-relative">
                        <div class="d-flex flex-row align-items-center">
                            <div class="bg-yellow title-icon d-flex align-items-center justify-content-center">
                                <img src="{{ asset('assets/frontend/img/candidate/graduation-cap-solid.png') }}" alt="graduation cap icon" class="title-img">
                            </div>
                            <h4 class="text-white h5 mb-0 mx-3">اطلاعات آکادمیک</h4>
                        </div>
                    </div>
                    <div class="row row-cols-lg-3 row-cols-md-2 row-cols-1 mt-5 row-gap-3">
                        @foreach ($user->academicInfos as $info)
                        <div class="col px-2">
                            <div class="rounded-4 box-shadow-c bg-white">
                                <div class="academic-info p-2 pe-3 position-relative">
                                    <div class="br-blue pe-3">
                                        <p class="text fs-18 mb-1">{{ $info->grade }} : {{ $info->major }}</p>
                                        <p class="text fs-18 mb-1">{{ $info->university }}</p>
                                        <p class="text fs-18 mb-1">{{ $info->end_date }}</p>
                                        <p class="text fs-18 mb-1">معدل : {{ $info->avg }}</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            @endif

            @if ($user->skills && count($user->skills) > 0)
                <div class="box-shadow-c bg-darkgrey p-4 rounded-4 mb-5">
                    <div class="d-flex justify-content-between flex-md-row flex-column">
                        <div class="bg-blue w-fit rounded-top-only move-right position-relative">
                            <div class="d-flex flex-row align-items-center">
                                <div class="bg-yellow title-icon d-flex align-items-center justify-content-center">
                                    <img src="{{ asset('assets/frontend/img/candidate/sliders-solid.png') }}" alt="sliders icon" class="title-img">
                                </div>
                                <h4 class="text-white h5 mb-0 mx-3">مهارت ها</h4>
                            </div>
                        </div>
                        <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap">
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-excellent ms-1"></div>
                                <span class="fs-5 fw-medium excellent">عالی</span>
                            </div>
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-good ms-1"></div>
                                <span class="fs-5 fw-medium good">خوب</span>
                            </div>
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-fair ms-1"></div>
                                <span class="fs-5 fw-medium fair">متوسط</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <div class="dot-poor ms-1"></div>
                                <span class="fs-5 fw-medium poor">ضعیف</span>
                            </div>
                        </div>
                    </div>

                    <div class="mt-4 d-flex">
                        <div class="d-flex flex-column w-100">
                            @foreach ($user->skills as $skill)
                                <div class="d-flex mb-3 flex-md-row flex-column align-items-md-center">
                                    <h4 class="text fs-5 progress-title">{{ $skill->skillName->title }}</h4>
                                    <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح مهارت ها">
                                        <div class="progress-bar
                                            @if($skill->level == 5)
                                                bg-excellent
                                            @elseif($skill->level == 4)
                                                bg-good
                                            @elseif($skill->level == 3)
                                                bg-fair
                                            @elseif($skill->level <= 2)
                                                bg-poor
                                            @endif
                                        " style="width:
                                            @if($skill->level == 5)
                                                100%
                                            @elseif($skill->level == 4)
                                                75%
                                            @elseif($skill->level == 3)
                                                50%
                                            @elseif($skill->level <= 2)
                                                25%
                                            @endif
                                        "></div>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                    </div>

                </div>
            @endif

            @if ($user->knowledges && count($user->knowledges) > 0)
                <div class="box-shadow-c bg-darkgrey p-4 rounded-4 mb-5">
                    <div class="bg-blue w-fit rounded-top-only move-right position-relative">
                        <div class="d-flex flex-row align-items-center">
                            <div class="bg-yellow title-icon d-flex align-items-center justify-content-center">
                                <img src="{{ asset('assets/frontend/img/candidate/knowledge.png') }}" alt="knowledge icon" class="title-img">
                            </div>
                            <h4 class="text-white h5 mb-0 mx-3">دانش</h4>
                        </div>
                    </div>
                    <div class="row row-cols-lg-4 row-cols-sm-2 row-cols-auto mt-5 align-items-center row-gap-3 fix-width-wrraper">
                        @foreach ($user->knowledges as $knowledge)
                        <div class="col fix-width-skill">
                            <div class="d-flex ujstify-content-center flex-column align-items-center p-3
                                @if(changeLevelToFaLevel($knowledge->level) == 'عالی')
                                    excellent-box-border
                                @elseif(changeLevelToFaLevel($knowledge->level) == 'خوب')
                                    good-box-border
                                @elseif(changeLevelToFaLevel($knowledge->level) == 'متوسط' || changeLevelToFaLevel($knowledge->level) == 'مبتدی')
                                    fair-box-border
                                @elseif(changeLevelToFaLevel($knowledge->level) == 'ضعیف')
                                    poor-box-border
                                @endif
                            ">
                            <h4 class="text h5">{{ $knowledge->knowledgeName->title }}</h4>
                            <div class="d-flex justify-content-center
                                @if(changeLevelToFaLevel($knowledge->level) == 'عالی')
                                    excellent-tag
                                @elseif(changeLevelToFaLevel($knowledge->level) == 'خوب')
                                    good-tag
                                @elseif(changeLevelToFaLevel($knowledge->level) == 'متوسط' || changeLevelToFaLevel($knowledge->level) == 'مبتدی')
                                    fair-tag
                                @elseif(changeLevelToFaLevel($knowledge->level) == 'ضعیف')
                                    poor-tag
                                @endif
                                ">
                                    <span class="fs-14 fw-medium text-white align-self-center">{{ changeLevelToFaLevel($knowledge->level) }}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            @endif

            @if ($user->abilities && count($user->abilities) > 0)
                <div class="box-shadow-c bg-darkgrey p-4 rounded-4 mb-5">
                    <div class="d-flex justify-content-between flex-md-row flex-column">
                        <div class="bg-blue w-fit rounded-top-only move-right position-relative">
                            <div class="d-flex flex-row align-items-center">
                                <div class="bg-yellow title-icon d-flex align-items-center justify-content-center">
                                    <img src="{{ asset('assets/frontend/img/candidate/selfconfident.png') }}" alt="selfconfident icon" class="title-img">
                                </div>
                                <h4 class="text-white h5 mb-0 mx-3">توانایی ها</h4>
                            </div>
                        </div>
                        <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap">
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-excellent ms-1"></div>
                                <span class="fs-5 fw-medium excellent">عالی</span>
                            </div>
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-good ms-1"></div>
                                <span class="fs-5 fw-medium good">خوب</span>
                            </div>
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-fair ms-1"></div>
                                <span class="fs-5 fw-medium fair">متوسط</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <div class="dot-poor ms-1"></div>
                                <span class="fs-5 fw-medium poor">ضعیف</span>
                            </div>
                        </div>
                    </div>

                    <div class="mt-4 d-flex">
                        <div class="d-flex flex-column w-100">
                            @foreach ($user->abilities as $abilitie)
                                <div class="d-flex mb-3 flex-md-row flex-column align-items-md-center">
                                    <h4 class="text fs-5 progress-title">{{ $abilitie->abilityName->title }}</h4>
                                    <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح توانایی ها">
                                        <div class="progress-bar
                                            @if($abilitie->level == 5)
                                                bg-excellent
                                            @elseif($abilitie->level == 4)
                                                bg-good
                                            @elseif($abilitie->level == 3)
                                                bg-fair
                                            @elseif($abilitie->level <= 2)
                                                bg-poor
                                            @endif
                                        " style="width:
                                            @if($abilitie->level == 5)
                                                100%
                                            @elseif($abilitie->level == 4)
                                                75%
                                            @elseif($abilitie->level == 3)
                                                50%
                                            @elseif($abilitie->level <= 2)
                                                25%
                                            @endif
                                        "></div>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                    </div>

                </div>
            @endif
            
            @if ($user->technologySkills && count($user->technologySkills) > 0)
                <div class="box-shadow-c bg-darkgrey p-4 rounded-4 mb-5">
                    <div class="bg-blue w-fit rounded-top-only move-right position-relative">
                        <div class="d-flex flex-row align-items-center">
                            <div class="bg-yellow title-icon d-flex align-items-center justify-content-center">
                                <img src="{{ asset('assets/frontend/img/candidate/connection.png') }}" alt="connection icon" class="title-img">
                            </div>
                            <h4 class="text-white h5 mb-0 mx-3">مهارت های فنی</h4>
                        </div>
                    </div>
                    <div class="row row-cols-lg-4 row-cols-sm-2 row-cols-auto mt-5 align-items-center row-gap-3 fix-width-wrraper">
                        @foreach ($user->technologySkills as $technologySkill)
                        <div class="col fix-width-skill">
                            <div class="d-flex ujstify-content-center flex-column align-items-center p-3
                                @if(changeLevelToFaLevel($technologySkill->level) == 'عالی')
                                    excellent-box-border
                                @elseif(changeLevelToFaLevel($technologySkill->level) == 'خوب')
                                    good-box-border
                                @elseif(changeLevelToFaLevel($technologySkill->level) == 'متوسط' || changeLevelToFaLevel($technologySkill->level) == 'مبتدی')
                                    fair-box-border
                                @elseif(changeLevelToFaLevel($technologySkill->level) == 'ضعیف')
                                    poor-box-border
                                @endif
                            ">
                            <h4 class="text h6">{{ $technologySkill->exampleName->example }}</h4>
                            <div class="d-flex justify-content-center
                                @if(changeLevelToFaLevel($technologySkill->level) == 'عالی')
                                    excellent-tag
                                @elseif(changeLevelToFaLevel($technologySkill->level) == 'خوب')
                                    good-tag
                                @elseif(changeLevelToFaLevel($technologySkill->level) == 'متوسط' || changeLevelToFaLevel($technologySkill->level) == 'مبتدی')
                                    fair-tag
                                @elseif(changeLevelToFaLevel($technologySkill->level) == 'ضعیف')
                                    poor-tag
                                @endif
                                ">
                                    <span class="fs-14 fw-medium text-white align-self-center">{{ changeLevelToFaLevel($technologySkill->level) }}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            @endif

            @if ($user->workStyles && count($user->workStyles) > 0)
                <div class="box-shadow-c bg-darkgrey p-4 rounded-4 mb-5">
                    <div class="d-flex justify-content-between flex-md-row flex-column">
                        <div class="bg-blue w-fit rounded-top-only move-right position-relative">
                            <div class="d-flex flex-row align-items-center">
                                <div class="bg-yellow title-icon d-flex align-items-center justify-content-center">
                                    <img src="{{ asset('assets/frontend/img/candidate/suitcase.png') }}" alt="suitcase icon" class="title-img">
                                </div>
                                <h4 class="text-white h5 mb-0 mx-3">ویژگی های کاری</h4>
                            </div>
                        </div>
                        <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap">
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-excellent ms-1"></div>
                                <span class="fs-5 fw-medium excellent">عالی</span>
                            </div>
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-good ms-1"></div>
                                <span class="fs-5 fw-medium good">خوب</span>
                            </div>
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-fair ms-1"></div>
                                <span class="fs-5 fw-medium fair">متوسط</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <div class="dot-poor ms-1"></div>
                                <span class="fs-5 fw-medium poor">ضعیف</span>
                            </div>
                        </div>
                    </div>

                    <div class="mt-4 d-flex">
                        <div class="d-flex flex-column w-100">
                            @foreach ($user->workStyles as $workStyle)
                                <div class="d-flex mb-3 flex-md-row flex-column align-items-md-center">
                                    <h4 class="text fs-5 progress-title">{{ $workStyle->workStyleName->title }}</h4>
                                    <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح ویژگی های کاری">
                                        <div class="progress-bar
                                            @if($workStyle->level == 5)
                                                bg-excellent
                                            @elseif($workStyle->level == 4)
                                                bg-good
                                            @elseif($workStyle->level == 3)
                                                bg-fair
                                            @elseif($workStyle->level <= 2)
                                                bg-poor
                                            @endif
                                        " style="width:
                                            @if($workStyle->level == 5)
                                                100%
                                            @elseif($workStyle->level == 4)
                                                75%
                                            @elseif($workStyle->level == 3)
                                                50%
                                            @elseif($workStyle->level <= 2)
                                                25%
                                            @endif
                                        "></div>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                    </div>

                </div>
            @endif

            @if ($user->languages && count($user->languages) > 0)
                <div class="box-shadow-c bg-darkgrey p-4 rounded-4 mb-5">
                    <div class="bg-blue w-fit rounded-top-only move-right position-relative">
                        <div class="d-flex flex-row align-items-center">
                            <div class="bg-yellow title-icon d-flex align-items-center justify-content-center">
                                <img src="{{ asset('assets/frontend/img/candidate/globe-solid.png') }}" alt="globe icon" class="title-img">
                            </div>
                            <h4 class="text-white h5 mb-0 mx-3">زبان های خارجی</h4>
                        </div>
                    </div>
                    <div class="row row-cols-lg-4 row-cols-sm-2 row-cols-auto mt-5 align-items-center row-gap-3 fix-width-wrraper">
                        @foreach ($user->languages as $language)
                        <div class="col fix-width-skill">
                            <div class="d-flex ujstify-content-center flex-column align-items-center p-3
                                @if($language->pivot->level == 'عالی')
                                    excellent-box-border
                                @elseif($language->pivot->level == 'خوب')
                                    good-box-border
                                @elseif($language->pivot->level == 'متوسط' || $language->pivot->level == 'مبتدی')
                                    fair-box-border
                                @elseif($language->pivot->level == 'ضعیف')
                                    poor-box-border
                                @endif
                            ">
                            <h4 class="text h5">{{ $language->name }}</h4>
                            <div class="d-flex justify-content-center
                                @if($language->pivot->level == 'عالی')
                                    excellent-tag
                                @elseif($language->pivot->level == 'خوب')
                                    good-tag
                                @elseif($language->pivot->level == 'متوسط' || $language->pivot->level == 'مبتدی')
                                    fair-tag
                                @elseif($language->pivot->level == 'ضعیف')
                                    poor-tag
                                @endif
                                ">
                                    <span class="fs-14 fw-medium text-white align-self-center">{{ $language->pivot->level }}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            @endif

            @if ($user->work_experiences && count($user->work_experiences) > 0)
                <div class="box-shadow-c bg-darkgrey p-4 rounded-4 mb-5">
                    <div class="bg-blue w-fit rounded-top-only move-right position-relative">
                        <div class="d-flex flex-row align-items-center">
                            <div class="bg-yellow title-icon d-flex align-items-center justify-content-center">
                                <img src="{{ asset('assets/frontend/img/candidate/work.png') }}" alt="work icon" class="title-img">
                            </div>
                            <h4 class="text-white h5 mb-0 mx-3">سابقه کاری</h4>
                        </div>
                    </div>
                    <div class="row row-cols-lg-3 row-cols-md-2 row-cols-1 mt-5 row-gap-3">
                        @foreach ($user->work_experiences as $work)
                        <div class="col px-2">
                            <div class="rounded-4 box-shadow-c bg-white">
                                <div class="academic-info p-2 pe-3 position-relative">
                                    <div class="br-blue pe-3">
                                        <p class="text fs-18 mb-1">{{ $work->title }}</p>
                                        <p class="text fs-18 mb-1">در {{ $work->company }}</p>
                                        @if ($work->start_date)
                                            <p class="text fs-18 mb-1">تاریخ شروع : {{ verta($work->start_date)->formatJalaliDate() }}</p>
                                        @endif
                                        @if ($work->end_date)
                                            <p class="text fs-18 mb-1">تاریخ پایان : {{ verta($work->end_date)->formatJalaliDate() }}</p>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            @endif

            @if($user->certificates && count($user->certificates) > 0)
                <div class="box-shadow-c bg-darkgrey p-4 rounded-4 mb-5">
                    <div class="bg-blue w-fit rounded-top-only move-right position-relative">
                        <div class="d-flex flex-row align-items-center">
                            <div class="bg-yellow title-icon d-flex align-items-center justify-content-center">
                                <img src="{{ asset('assets/frontend/img/candidate/newspaper-regular.png') }}" alt="newspaper regular icon" class="title-img">
                            </div>
                            <h4 class="text-white h5 mb-0 mx-3">دوره ها و گواهینامه ها</h4>
                        </div>
                    </div>
                    <div class="row row-cols-lg-4 row-cols-sm-2 row-cols-1 mt-4 row-gap-3">
                        @foreach ($user->certificates as $certificate)
                            <div class="col px-xl-3 px-2">
                                <div class="rounded-4 border-dash-blue">
                                    <a href="{{ asset($certificate->photo->path) }}" target="_blank">
                                        <img src="{{ asset($certificate->photo->path) }}" class="img-fluid w-100" alt="گواهینامه دوره">
                                    </a>
                                    <div class="py-3 px-2">

                                        <div class="d-flex justify-content-center flex-row flex-lg-column flex-xxl-row">
                                            <span class="fs-14 text">نام دوره: </span>
                                            <span class="me-2 fs-14 text">{{ $certificate->title }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif

        </div>
    </div>

</div>
@endsection
@section('script')
    <script src="{{ asset('assets/swal2/swal2.all.min.js') }}" defer></script>
    <script defer>
        const packageUrl = "{{ route('index.package.company') }}";
        const requestUrl = "{{ route('front.ajax.co-cooperation-request') }}";
        $('#work-request').on('click', function() {


            let job = $('#job').val();

            let jobType = $('#job').find(':selected').attr('jobType');

            if(job == ''){
                Swal.fire({
                    icon : 'error',
                    title : 'خطا !',
                    text : 'موقعیت کارآموزی - شغلی را انتخاب کنید. در صورت نبود موقعیت، آن را ایجاد کنید',
                    showCancelButton: true,
                    confirmButtonText : 'ایجاد موقعیت کارآموزی',
                    cancelButtonText : 'ایجاد موقعیت شغلی',
                }).then((result)=>{
                    if(result.isConfirmed){
                        window.location.href = "{{ route('create.pos.job') }}";
                    }else if(result.dismiss == "cancel"){
                        window.location.href = "{{ route('create.pos.job') }}";
                    }
                })
            }else{
                function getProposedSalary(){
                    return new Promise((resolve, reject) => {
                        if(jobType == 'true'){
                            Swal.fire({
                                icon : 'info',
                                title : 'توجه !',
                                text : 'حقوق پیشنهادی خود را برای درخواست کارآموزی به صورت عددی و به تومان وارد کنید.',
                                input: "text",
                                inputValidator: (value) => {
                                    if (!value || isNaN(value)) {
                                        return 'لطفاً یک عدد معتبر وارد کنید';
                                    }
                                },
                                showCancelButton: true,
                                confirmButtonText : 'ثبت حقوق و ارسال درخواست',
                                cancelButtonText : 'لغو',
                            }).then((result)=>{
                                if(result.isConfirmed){
                                    resolve(result.value);
                                } else {
                                    reject('canceled');
                                }
                            });
                        } else {
                            resolve(null); 
                        }
                    });
                }

                getProposedSalary()
                    .then((proposedSalary) => {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            }
                        })

                        let user = '{{ $user->id }}'
                        $.ajax({
                            type: 'POST',
                            url: requestUrl,
                            data: {
                                job: job,
                                receiver: user,
                                proposed_salary: proposedSalary
                            },
                            success: function(data) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'درخواست شما با موفقیت ارسال شد',
                                    showConfirmButton: false,
                                    timer: 3500
                                })
                                $('#work-request').removeClass('bg-blue')
                                $('#work-request').addClass('bg-black pe-none')
                                $('#work-request').html('شما قبلا برای این کارجو درخواست فرستاده اید')
                            },
                            error: function(data) {
                                errorMessage = data.responseJSON ? data.responseJSON.errorText : 'خطای ناشناخته';
                                // console.log(errorMessage);
                                
                                Swal.fire({
                                    icon: 'error',
                                    title: 'خطا',
                                    text: errorMessage,
                                    showConfirmButton: true,
                                    confirmButtonText : 'خرید بسته خدماتی',
                                    // timer: 4500
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        window.location.href = packageUrl;
                                    }
                                });
                            }

                        })
                    }).catch((error) => {
                        // console.log(error); 
                    });


   
            }
        })
    </script>
@endsection
