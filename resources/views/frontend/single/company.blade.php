@extends('frontend.layout.master')

@section('title' , 'مشخصات شرکت')
@section('page' , 'جابیست')
@section('meta')
    <meta name="description" content="با جابیست، اگر به دنبال جستجوی شرکت‌های برتر و معتبر در هر صنعت هستید؟ اینجا جایی است که می‌توانید به سرعت شرکت‌هایی را که به دنبال کارآموزی یا استخدام هستند پیدا کنید. با ما، به دنبال شرکت‌هایی با محیط کاری مناسب و فرصت‌های شغلی جذاب باشید." />
    <meta property="og:url" content="https://jobist.ir/single/company/{{$company->id}}"/>
    <meta property="og:title" content=" صفحه اختصاصی شرکت‌ در جابیست | پیدا کردن بهترین محیط‌های کاری و فرصت‌های شغلی"/>
    <meta property="og:description" content=" با جابیست، اگر به دنبال جستجوی شرکت‌های برتر و معتبر در هر صنعت هستید؟ اینجا جایی است که می‌توانید به سرعت شرکت‌هایی را که به دنبال کارآموزی یا استخدام هستند پیدا کنید. با ما، به دنبال شرکت‌هایی با محیط کاری مناسب و فرصت‌های شغلی جذاب باشید." /> 
    <link rel="canonical" href="https://jobist.ir/single/company/{{$company->id}}"/>
    <link rel="preload" href="{{asset('assets/frontend/img/home/sale-Overlay-min2.webp')}}" as="image"/>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/company-style.css') }}"> 
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/owl.carousel.min.css') }}">
@endsection


@section('main')
    <div class="container mt-md-4 mt-0">

        <!-------- pc  -------->

        @if ($company->groupable->header)
        <div class="image-top overflow-hidden rounded-5">
            <img src="{{ asset($company->groupable->header->path) }}" class="img-fluid d-md-inline d-none w-100 rounded-5" width="1296" height="340">
        </div>
        @else
            <img src="{{ asset('assets/frontend/img/company/group-diverse-people-having-business-meeting.jpg') }}" class="rounded-5 img-fluid image-top d-md-inline d-none" width="1296" height="340">
        @endif
        <div class="bg-darkgrey rounded-5 move-up box-shadow-c w-85 z-1 position-relative mx-auto d-md-block d-none">
            <div class="d-flex justify-content-start">
                <div class="p-4 logo-border logo-box-c">
                    <img src="{{ asset($company->logo) }}" class="img-fluid logo-c-width" width="176" height="230">
                </div>
                <div class="mt-4 w-100 py-2">
                    <h3 class="blue fw-semibold px-4">{{ $company->groupable->name }}</h3>
                    <div class="row row-cols-lg-3 row-cols-md-2 row-cols-1 mt-3 row-gap-4 px-4 justify-content-center">
                        <div class="col">
                            <div class="px-lg-2 px-md-4">
                                <div>
                                    <h5 class="fs-6 mb-2 text-grey">حوزه کاری:</h5>
                                    <p class="fs-18">{{ $company->groupable->activity_field }}</p>
                                </div>
                                <div class="mt-md-3 mt-2">
                                    <h5 class="fs-6 mb-2 text-grey">تلفن:</h5>
                                    <p class="fs-18 fanumber">{{ $company->groupable->phone }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="px-lg-2 px-md-4">
                                <div>
                                    <h5 class="fs-6 mb-2 text-grey">اندازه شرکت:</h5>
                                    <p class="fs-18 fanumber">{{ $company->groupable->size }}</p>
                                </div>
                                <div class="mt-md-3 mt-2">
                                    <h5 class="fs-6 mb-2 text-grey">ایمیل:</h5>
                                    <p class="fs-17">{{ $company->email }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="px-2">
                                <div>
                                    <h5 class="fs-6 mb-2 text-grey">تاسیس شده در:</h5>
                                    <p class="fs-18 fanumber">{{ $company->groupable->year }}</p>
                                </div>
                                <div class="mt-md-3 mt-2">
                                    <h5 class="fs-6 mb-2 text-grey fanumber">مکان:</h5>
                                    
                                    @if ($company->groupable->province)
                                        <p class="fs-18">{{ $company->groupable->province->name }} ،

                                            @if ($company->groupable->city)

                                                {{ $company->groupable->city->name }}

                                            @elseif ($company->groupable->city)

                                                {{ $company->groupable->city }}
                                                
                                            @endif
                                        </p>
                                    @else
                                        <p class="fs-18">ایران،

                                            @if ($company->groupable->city)

                                                {{ $company->groupable->city }}
                                                
                                            @endif

                                        </p>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-------- mobile  -------->
        <div class="d-inline d-md-none">
            @if ($company->groupable->header)
                <div class="image-top rounded-bottom-3 overflow-hidden">
                    <img src="{{ asset($company->groupable->header->path) }}" class="img-fluid w-100">                    
                </div>
            @else
                <img src="{{ asset('assets/frontend/img/company/group-diverse-people-having-business-meeting.jpg') }}" class="img-fluid image-top rounded-bottom-3">
            @endif
            <div class="px-3">
                <div class="bg-darkgrey mx-auto rounded-3 move-up box-shadow-c">
                    <div class="boder-dash-logo bg-white position-relative mx-auto">
                        <img src="{{ asset($company->logo) }}" class="position-absolute company-logo">
                    </div>
                    <div class="mt-4 w-100 py-2">
                        <h3 class="blue fw-semibold px-4 text-center">{{ $company->groupable->name }}</h3>
                        <div class="row row-cols-1 mt-3 px-3">
                            <div class="col">
                                <div>
                                    <h5 class="fs-6 mb-2 text-grey">حوزه کاری:</h5>
                                    <p class="fs-18">{{ $company->groupable->activity_field }}</p>
                                </div>
                                <div class="mt-md-3 mt-2">
                                    <h5 class="fs-6 mb-2 text-grey">تلفن:</h5>
                                    <p class="fs-18 fanumber">{{ $company->groupable->phone }}</p>
                                </div>
                            </div>
                            <div class="col">
                                <div>
                                    <h5 class="fs-6 mb-2 text-grey">اندازه شرکت:</h5>
                                    <p class="fs-18 fanumber">{{ $company->groupable->size }}</p>
                                </div>
                                <div class="mt-md-3 mt-2">
                                    <h5 class="fs-6 mb-2 text-grey">ایمیل:</h5>
                                    <p class="fs-18">{{ $company->email }}</p>
                                </div>
                            </div>
                            <div class="col">
                                <div>
                                    <h5 class="fs-6 mb-2 text-grey">تاسیس شده در:</h5>
                                    <p class="fs-18 fanumber">{{ $company->groupable->year }}</p>
                                </div>
                                <div class="mt-md-3 mt-2">
                                    <h5 class="fs-6 mb-2 text-grey">مکان:</h5>
                                    @if ($company->groupable->province)
                                        <p class="fs-18">{{ $company->groupable->province->name }} ،

                                            @if ($company->groupable->city)

                                                {{ $company->groupable->city->name }}

                                            @elseif ($company->groupable->city)

                                                {{ $company->groupable->city }}
                                                
                                            @endif
                                        </p>
                                    @else
                                        <p class="fs-18">ایران،

                                            @if ($company->groupable->city)

                                                {{ $company->groupable->city }}
                                                
                                            @endif

                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div class="mt-5">
            <ul class="nav nav-underline bg-blue pt-3 pe-4 custom-rounded" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                <button class="nav-link active text-black nav-btn fs-18 pb-2 text-white tab-title" id="job-details-d" data-bs-toggle="tab" data-bs-target="#job-details-d-pane" type="button" role="tab" aria-controls="job-details-d-pane" aria-selected="true">درباره شرکت</button>
                </li>
                {{-- <li class="nav-item" role="presentation">
                <button class="nav-link text-black nav-btn fs-18 pb-2 text-white tab-title" id="about-company-d" data-bs-toggle="tab" data-bs-target="#about-company-d-pane" type="button" role="tab" aria-controls="about-company-d-pane" aria-selected="false">خدمات و محصولات</button>
                </li> --}}
            </ul>
            <div class="tab-style position-relative"></div>
            <div class="tab-content pe-4 m-top" id="myTabContent">
                <div class="tab-pane fade show active px-4 pt-3 pb-4 rounded-bottom-4 box-shadow-c bg-darkgrey" id="job-details-d-pane" role="tabpanel" aria-labelledby="job-details-d" tabindex="0">
                    @if (! is_null($company->groupable->about))
                        <p class="text-justify fanumber word-wrap">
                            @php
                                echo $company->groupable->about;
                            @endphp
                        </p>
                    @endif
                </div>
                {{-- <div class="tab-pane fade pe-md-4 pe-2 pt-3 box-shadow-c rounded-bottom-4 bg-darkgrey" id="about-company-d-pane" role="tabpanel" aria-labelledby="about-company-d" tabindex="0">
                    <div class="row row-cols-lg-3 row-cols-md-2 row-cols-1 row-gap-3 py-md-3 pb-3 ps-md-4 ps-2 justify-content-center">
                        <div class="col">
                            <div class="bg-white p-4 rounded-4 border-dash">
                                <h4 class="blue h5 fw-semibold mb-3">طراحی و تخصص وب سایت</h4>
                                <p class="text-justify fs-18">للورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت  ایجاد</p>
                                <div class="d-flex align-items-center justify-content-end">
                                    <h5 class="green ms-2 mb-0 fw-semibold">مشاوره و مشاهده پلن</h5>
                                    <img src="{{ asset('assets/frontend/img/company/angle-left-solid@2x.png') }}" class="left-angle-width">
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="bg-white p-4 rounded-4 border-dash">
                                <h4 class="blue h5 fw-semibold mb-3">طراحی و تخصص وب سایت</h4>
                                <p class="text-justify fs-18">للورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت  ایجاد</p>
                                <div class="d-flex align-items-center justify-content-end">
                                    <h5 class="green ms-2 mb-0 fw-semibold">مشاوره و مشاهده پلن</h5>
                                    <img src="{{ asset('assets/frontend/img/company/angle-left-solid@2x.png') }}" class="left-angle-width">
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="bg-white p-4 rounded-4 border-dash">
                                <h4 class="blue h5 fw-semibold mb-3">طراحی و تخصص وب سایت</h4>
                                <p class="text-justify fs-18">لولورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت ایجاد</p>
                                <div class="d-flex align-items-center justify-content-end">
                                    <h5 class="green ms-2 mb-0 fw-semibold">مشاوره و مشاهده پلن</h5>
                                    <img src="{{ asset('assets/frontend/img/company/angle-left-solid@2x.png') }}" class="left-angle-width">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>




        @if ($hire_positions->count() > 0)
            <div class="mt-5">
                <div class="row mb-3">
                    <div class="d-flex flex-column">
                        <h2 class="h3 title">موقعیت های استخدامی</h2>
                        <p class="fs-6 fw-medium text-grey">لیست موقعیت های استخدامی ثبت شده توسط شرکت {{ $company->groupable->name }}</p>
                    </div>
                </div>
                <div id="carousel-company-1" class="owl-carousel">
                    @foreach ($hire_positions as $hire)
                        <div>
                            <div class="col p-3">
                                <div class="box-shadow-c pb-md-3 bg-darkgrey box-hover border-piece">
                                    <div class="bg-halfc p-3">
                                        <div class="d-flex">
                                            <img src="{{ asset('assets/frontend/img/company/heart-regular.png') }}" class="w-small me-auto">
                                        </div>
                                        <div class="d-flex align-items-center mb-4">
                                            <div class="bg-gr ms-3 px-2 py-3 rounded-4">
                                                <img src="{{ asset('assets/frontend/img/company/computer-solid.png') }}" class="job-img">
                                            </div>
                                            <div class="d-flex flex-column">
                                                <span class="h5 fw-bold blue">{{ $hire->title }}</span>
                                                <span class="fs-6 text-grey">{{ $hire->job->category->title }}</span>
                                            </div>
                                        </div>
                                        <div class="">
                                            @if ($hire->province || $hire->city)
                                                <div class="d-flex align-items-center">
                                                    <img src="{{ asset('assets/frontend/img/company/earth-asia-solid.png') }}" class="w-small ms-1">
                                                    <span class="grey ps-2 card-text">

                                                        @if ($hire->province)
                                                            {{ $hire->province->name }}
                                                        @endif

                                                        @if ($hire->province && $hire->city)
                                                            ,
                                                        @endif

                                                        @if ($hire->city)
                                                            {{ $hire->city->name }}
                                                        @endif

                                                    </span>
                                                </div>
                                            @endif
                                            <div class="">
                                                <span class="text-muted">{{ job_type_persian($hire->type) }}</span>
                                            </div>
                                        </div>
                                        <div class="mt-3 d-flex justify-content-between pt-1 align-items-sm-center flex-sm-row flex-column">
                                            <div class="mb-sm-0 mb-2 d-flex flex-sm-row flex-column">
                                                <span class="text-grey fs-7 ms-1 fanumber">{{ verta($hire->created_at)->formatDifference() }} توسط</span>
                                                <strong class="text-dark">{{ $company->groupable->name }}</strong>
                                            </div>
                                            <a href="{{ route('job' , $hire->id) }}" class="btn cv-btn rounded-4 align-self-end text-dark">مشاهده اطلاعات</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>  
            </div>
        @endif


        @if ($intern_positions->count() > 0)
            <div class="mt-5 block-intern">
                <div class="row mb-3">
                    <div class="d-flex flex-column">
                        <h2 class="h3 title">موقعیت های کارآموزی</h2>
                        <p class="fs-6 fw-medium text-grey">لیست موقعیت های کارآموزی ارسال شده توسط شرکت {{ $company->groupable->name }}</p>
                    </div>
                </div>
                <div id="carousel-company-2" class="owl-carousel">
                    @foreach ($intern_positions as $intern)
                        <div>
                            <div class="col p-3">
                                <div class="box-shadow-c pb-md-3 bg-darkgrey box-hover border-piece-2">
                                    <div class="bg-halfc-2 p-3">
                                        <div class="d-flex">
                                            <img src="{{ asset('assets/frontend/img/company/heart-regular.png') }}" class="w-small me-auto">
                                        </div>
                                        <div class="d-flex align-items-center mb-4">
                                            <div class="bg-gr-2 ms-3 px-2 py-3 rounded-4">
                                                <img src="{{ asset('assets/frontend/img/company/user-pen-solid.png') }}" class="job-img">
                                            </div>
                                            <div class="d-flex flex-column">
                                                <span class="h5 fw-bold txt-green">{{ $intern->title }}</span>
                                                <span class="fs-6 text-grey">{{ $intern->job->category->title }}</span>
                                            </div>
                                        </div>
                                        <div class="">
                                            @if ($intern->province || $intern->city)
                                                <div class="d-flex align-items-center">
                                                    <img src="{{ asset('assets/frontend/img/company/earth-asia-solid.png') }}" class="w-small ms-1">
                                                    <span class="grey ps-2 card-text">

                                                        @if ($intern->province)
                                                            {{ $intern->province->name }}
                                                        @endif

                                                        @if ($intern->province && $intern->city)
                                                            ,
                                                        @endif

                                                        @if ($intern->city)
                                                            {{ $intern->city->name }}
                                                        @endif

                                                    </span>
                                                </div>
                                            @endif
                                            <div class="">
                                                <span class="text-muted">{{ job_type_persian($intern->type) }}</span>
                                            </div>
                                        </div>
                                        <div class="mt-3 d-flex justify-content-between pt-1 align-items-sm-center flex-sm-row flex-column">
                                            <div class="mb-sm-0 mb-2 d-flex flex-sm-row flex-column">
                                                <span class="text-grey fs-7 ms-1 fanumber">{{ verta($intern->created_at)->formatDifference() }} توسط</span>
                                                <strong class="text-dark">{{ $company->groupable->name }}</strong>
                                            </div>
                                            <a href="{{ route('job', $intern->id) }}" class="btn cv-btn-2 rounded-4 align-self-end">مشاهده اطلاعات</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>  
            </div>
        @endif



        <div class="mt-5">
            <div class="row mb-3">
                <div class="d-flex flex-column">
                    <h2 class="h3 title">با ما در ارتباط باشید</h2>
                    <p class="fs-6 fw-medium text-grey">راه های ارتباطی و شبکه های اجتماعی شرکت {{ $company->groupable->name }} </p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="bg-darkgrey p-4 rounded-4 box-shadow-c mb-2">
                        <h2 class="h4 title mb-3">راه های ارتباطی</h2>
                        <div class="d-flex mb-2 justify-content-between">
                            <div class="d-flex w-100">
                                <div class="logo-bg rounded-circle">
                                    <i class="fa-solid fa-qrcode text-white contact-logo"></i>
                                </div>
                                <div class="text-muted fs-18 fw-medium mb-0 me-2">کد qr</div>
                            </div>
                            <div class="">
                                <div class="blue fs-18 fw-medium">
                                    {!! QrCode::size(90)->generate(route('company.single', $company->id)) !!}
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="d-flex align-items-center mb-2">
                                <div class="logo-bg rounded-circle">
                                    <i class="fa-solid fa-location-dot text-white contact-logo"></i>
                                </div>
                                <div class="text-muted fs-18 fw-medium mb-0 me-2">نشانی</div>
                            </div>
                            <p class="blue fs-18 fw-medium">
                                @if (! is_null($company->groupable->address))

                                    {{ $company->groupable->address }}
                                @else

                                    @if ($company->groupable->province)
                                        <p class="fs-18">{{ $company->groupable->province->name }} ،

                                            @if ($company->groupable->city)

                                                {{ $company->groupable->city->name }}

                                            @elseif ($company->groupable->city)

                                                {{ $company->groupable->city }}
                                                
                                            @endif
                                        </p>
                                    @else
                                        <p class="fs-18">ایران،

                                            @if ($company->groupable->city)

                                                {{ $company->groupable->city }}
                                                
                                            @endif

                                        </p>
                                    @endif

                                @endif
                            </p>
                        </div>
                        <div>
                            <div class="d-flex align-items-center mb-2">
                                <div class="logo-bg rounded-circle">
                                    <i class="fa-solid fa-phone-volume text-white contact-logo"></i>
                                </div>
                                <div class="text-muted fs-18 fw-medium mb-0 me-2">با ما تماس بگیرید</div>
                            </div>
                            <p class="blue fs-18 fw-medium fanumber">
                                @if (! is_null($company->groupable->phone))

                                    {{ $company->groupable->phone }}

                                @endif
                            </p>
                        </div>
                        <div>
                            <div class="d-flex align-items-center mb-2">
                                <div class="logo-bg rounded-circle">
                                    <i class="fa-solid fa-at text-white contact-logo"></i>
                                </div>
                                <div class="text-muted fs-18 fw-medium mb-0 me-2">به ما ایمیل بزنید</div>
                            </div>
                            <p class="blue fs-18 fw-medium">
                                @if (! is_null($company->email))

                                    {{ $company->email }}
                                    
                                @endif
                            </p>
                        </div>
                    </div>

                    {{-- <div class="bg-darkgrey p-4 rounded-4 box-shadow-c mb-2 mb-md-0">
                        <h2 class="h4 title mb-3">ما را دنبال کنید</h2>
                        <div class="d-flex gap-3">
                            <div class="logo-bg rounded-circle">
                                <img src="{{ asset('assets/frontend/img/company/Path 1956.png') }}" class="contact-logo">
                            </div>
                            <div class="logo-bg rounded-circle">
                                <img src="{{ asset('assets/frontend/img/company/Path 1955.png') }}" class="contact-logo">
                            </div>
                            <div class="logo-bg rounded-circle">
                                <img src="{{ asset('assets/frontend/img/company/Group 92.png') }}" class="contact-logo">
                            </div>
                            <div class="logo-bg rounded-circle">
                                <img src="{{ asset('assets/frontend/img/company/Group 91.png') }}" class="contact-logo">
                            </div>
                        </div>
                    </div> --}}

                </div>
                <div class="col-12 col-md-8">
                    <div class="bg-darkgrey p-4 rounded-4 box-shadow-c">
                        <h2 class="h4 title mb-4 w-50">تماس با شرکت</h2>
                        <div class="row gy-4">
                            <input type="hidden" id="owner" value="{{ $company->id }}">
                            {{-- <div class="col-md-6">
                                <input type="text" class="form-control customc-input" id="name" placeholder="نام">
                                <span class="text-danger me-2" id="nameErr"></span>
                            </div> --}}
                            <div class="col-md-6">
                                <input type="email" class="form-control customc-input" id="email" placeholder="ایمیل">
                                <span class="text-danger me-2" id="emailErr"></span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control customc-input" id="subject" placeholder="موضوع">
                                <span class="text-danger me-2" id="subjectErr"></span>
                            </div>
                            <div class="col-12">
                                <textarea class="form-control customc-input" id="text" placeholder="پیام..." rows="5"></textarea>
                                <span class="text-danger me-2" id="textErr"></span>
                            </div>
                        </div>
                        <div class="text-start mt-3">
                            @auth
                                <button type="button" id="send-ticket" onclick="sendTicket()" class="btn btn-send rounded-3 bg-blue text-white fs-18 px-5">ارسال پیام</button>
                            @else
                                <a href="{{ route('login') }}" class="btn btn-send rounded-3 bg-blue text-white fs-18 px-5">ارسال پیام</a>
                            @endauth
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <input type="hidden" id="sendTicketUrl" value="{{route('front.ajax.ticket.store-company-ticket')}}">
@endsection

@section('script')
    <script src="{{ asset('assets/frontend/js/owl.carousel.min.js') }}" defer></script>
    <script src="{{ asset('assets/swal2/swal2.all.min.js') }}" defer></script>
    <script src="{{ asset('assets/frontend/js/js-company.js') }}" defer></script>

@endsection

