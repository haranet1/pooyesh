@extends('frontend.layout.master')

@section('meta')
    <meta name="description"  content=" جابیست، به عنوان یکی از پرقدرت‌ترین و برجسته‌ترین پلتفرم‌های استخدام، کاریابی و کارآموزی، با تمرکز بر ویژگی‌های شخصیتی افراد، به آن‌ها امکان می‌دهد تا به سرعت و با موفقیت به شغلی که با توانایی‌ها و خواسته‌هایشان هماهنگ است، دست پیدا کنند." />
    <meta property="og:url" content="https://jobist.ir/about-us"/>
    <meta property="og:title" content=" درباره ما | جابیست چیست " />
    <meta property="og:description" content=" جابیست، به عنوان یکی از پرقدرت‌ترین و برجسته‌ترین پلتفرم‌های استخدام، کاریابی و کارآموزی، با تمرکز بر ویژگی‌های شخصیتی افراد، به آن‌ها امکان می‌دهد تا به سرعت و با موفقیت به شغلی که با توانایی‌ها و خواسته‌هایشان هماهنگ است، دست پیدا کنند." /> 
    <link rel="canonical" href="https://jobist.ir/about-us"/>
@endsection

@section('title' , 'درباره تیم ما')
@section('page' , 'درباره جابیست' )

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/about-us.css') }}">
@endsection


@section('main')
    
<div class="container">


    <div class="pb-4 blue-shadow bg-darkgrey">
        <img src="{{ asset('assets/frontend/img/about-us/Group 291.png') }}" class="img-fluid mask-img">
        <div class="d-flex justify-content-center mt-3">
            <h1 class="text-center main-title h2 fw-bold">درباره جابیست</h1>
        </div>
        <p class="text-center grey mt-3 px-3">فرصتی جدید برای تحصیل مهارت محور و اشتغال همزمان با تحصیل</p>
        <p class="text-justify px-sm-5 px-4 fs-19 fw-medium mx-auto">
            جابیست، به عنوان یکی از پرقدرت‌ترین و برجسته‌ترین پلتفرم‌های استخدام، کاریابی و کارآموزی، با تمرکز بر ویژگی‌های شخصیتی افراد، به آن‌ها امکان می‌دهد تا به سرعت و با موفقیت به شغلی که با توانایی‌ها و خواسته‌هایشان هماهنگ است، دست پیدا کنند. این پلتفرم حرفه‌ای و با استانداردهای جهانی o-net، ارائه دهنده پیشنهادهای تخصصی و فرصت‌های کارآموزی در شرکت‌های معتبر است، که باعث تحول رویای شغلی افراد می‌شود و آن را به واقعیت تبدیل می‌کند.
        </p>
    </div>

    <div class="mt-5">
        <div class="d-flex">
            <h3 class="sub-title h4 fw-bold">۳ قدم برای پیدا کردن مورد مناسب شما</h3>
        </div>
        <div class="row justify-content-between mt-5">
            <div class="col-md-4 padding-box">
                <div class="bg-darkgrey box-shadow-c d-flex align-items-center justify-content-center flex-column p-3 rounded-4">
                    <img src="{{ asset('assets/frontend/img/about-us/register.png') }}" class="image-size">
                    <h4 class="blue h5 mt-3 fw-bold">ثبت نام</h4>
                    <p class="text-grey-a text-center fs-5 mb-0">با ثبت نام در جابیست مسیر شغلی حرفه ای خود را آغاز کنید</p>
                </div>
            </div>
            <div class="col-md-4 padding-box ptop path-box">
                <div class="bg-darkgrey box-shadow-c d-flex align-items-center justify-content-center flex-column p-3 rounded-4">
                    <img src="{{ asset('assets/frontend/img/about-us/resume.png') }}" class="image-size">
                    <h4 class="blue h5 mt-3 fw-bold text-center">ساخت رزومه</h4>
                    <p class="text-grey-a text-center fs-5 mb-0">اولین قدم برای استخدام در شغل رویایی تان ایجاد یک رزومه حرفه ای است</p>
                </div>
            </div>
            <div class="col-md-4 padding-box pt-4">
                <div class="bg-darkgrey box-shadow-c d-flex align-items-center justify-content-center flex-column p-3 rounded-4">
                    <img src="{{ asset('assets/frontend/img/about-us/send-resume.png') }}" class="image-size">
                    <h4 class="blue h5 mt-3 fw-bold text-center">ارسال رزومه</h4>
                    <p class="text-grey-a text-center fs-5 mb-0">رزومه خود را برای کارفرمایان ارسال و آمادگی خود برای همکاری را اعلام نمایید</p>
                </div>
            </div>
        </div>
    </div>

    @auth
    @else
        <div class="mt-5 d-flex flex-column justify-content-center">
            <div class="d-flex justify-content-center mb-3">
                <a href="{{ route('register') }}" class="btn register-btn align-self-center px-4">ثبت نام کنید</a>
            </div>
            
            <div class="d-flex justify-content-center">
                <h4 class="blue h5 ms-2">حساب کاربری دارید؟</h4>
                <a href="{{ route('login') }}" class="h5 mid-blue">ورود</a>
            </div>
        </div>
    @endauth

    <div class="mt-5">
        <div class="d-flex">
            <h3 class="sub-title h4 fw-bold">محصولات و خدمات</h3>
        </div>
        <div class="row mt-5 px-sm-0 pe-1">
            <div class="col-md-6 px-4">
                <div class="bg-darkgrey box-shadow-c pt-4 position-relative">
                    <div class="bg-blue w-25 rounded-top-3 py-3 move-right position-relative">
                        <div class="d-flex flex-row align-items-center me-4">
                            <div class="fa-circle fa circle-1 ms-2">.</div>
                            <h4 class="text-white h5 mb-0">برای کارفرما</h4>
                        </div>
                    </div>
                    <div class="timeline">
                        <ul>
                            <li class="first-l">
                                <div>
                                </div>
                            </li>
                            <li class="li-2">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        ثبت آگهی استخدام: به سادگی و در عرض چند دقیقه، می‌توانید آگهی‌های استخدامی خود را درج کنید و جزئیات مربوط به شغل، شرایط کاری، موقعیت مکانی و غیره را وارد نمایید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-3">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        دسترسی به رزومه: شما می‌توانید به رزومه‌های کارجویان دسترسی داشته باشید و به دنبال کاندیدهای مناسب برای شغل‌های خود بگردید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-4">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        ابزارهای مدیریتی: با استفاده از فیلترهای جستجو و ابزارهای مدیریتی، می‌توانید فرآیند جستجو و انتخاب را بهینه کنید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-5">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        سیستم اطلاع رسانی: از آخرین اطلاعات مربوط به استخدام از طریق اعلان‌ها و اطلاعیه‌ها مطلع شوید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-6">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        ارتباط مستقیم با کارجویان: امکان برقراری ارتباط مستقیم با کارجویان از طریق پیام‌های آنلاین را دارید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-7">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        حفاظت از داده‌ها و امنیت: ما اطلاعات شما را با استفاده از استانداردهای امنیتی بالا حفاظت می‌کنیم.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-8">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        پشتیبانی و خدمات مشتریان: تیم پشتیبانی ما همیشه آماده است تا به شما در هر گونه مشکل یا سوال کمک کند.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-9">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        برگزاری نمایشگاه کار: برگزاری نمایشگاه ویژه کارفرمایان، به منظور معرفی برند، محصولات، فرصت‌های شغلی و ارتباط مستقیم با کارجویان.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-10">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        استفاده از استانداردهای O*NET : به شرکت‌ها امکان می‌دهد تا اطلاعات کاملی از موقعیت های شغلی پیدا کنند، اطلاعاتی همچون شرایط فیزیکی، محیط کاری و فرصت‌های پیشرفت شغلی را نیز ارائه دهند.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-11">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        انتخاب بهترین استخدام: با توجه به نتایج آزمون‌های شخصیتی و استعدادیابی، می‌توانید بهترین کاندیدا را برای هر شغل انتخاب کنید و تصمیمات بهتری درباره استخدام بگیرید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-12">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        تجزیه و تحلیل داده‌های کارجویان: با استفاده از داده‌های آزمون‌های شخصیتی، رغبت‌سنجی و استعدادیابی کارجویان، می‌توانید با دقت بیشتر، موقعیت شغلی را به کارجو پیشنهاد دهید.
                                    </h4>
                                </div>
                            </li>
                            <li class="last-l">
                                <div>
                                </div>
                            </li>
                    
                        </ul>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-6 px-4">
                <div class="bg-darkgrey box-shadow-c pt-4 position-relative">
                    <div class="bg-yellow w-25 rounded-top-3 py-3 move-right position-relative">
                        <div class="d-flex flex-row align-items-center me-4">
                            <div class="fa-circle fa circle-2 ms-2">.</div>
                            <h4 class="blue h5 mb-0">برای کارجو</h4>
                        </div>
                    </div>
                    <div class="timeline-2">
                        <ul>
                            <li class="first-l">
                                <div>
                                </div>
                            </li>
                            <li class="li-2">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        جستجوی شغل: با استفاده از ابزارهای جستجوی پیشرفته، می‌توانید به سرعت به شغل‌های مورد نظر خود دسترسی پیدا کنید، بر اساس موقعیت جغرافیایی، صنعت، حقوق و شرایط دیگر.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-3">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        سفارشی‌سازی رزومه: با استفاده از ویرایشگر رزومه، می‌توانید رزومه‌تان را به بهترین شکل ممکن طراحی و سفارشی کنید تا برجستگی و برتری خود را نشان دهید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-4">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        اطلاعیه‌های استخدامی: از آخرین فرصت‌های شغلی از طریق اطلاعیه‌های استخدامی باخبر شوید و همواره در جریان باشید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-5">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        پیام‌رسانی با کارفرماها: با استفاده از ابزار پیام‌رسانی، می‌توانید مستقیماً با کارفرماها ارتباط برقرار کنید و سوالات خود را مطرح کنید یا اطلاعات بیشتری دریافت کنید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-6">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        ارسال درخواست استخدام: به سادگی می‌توانید رزومه‌تان را برای شغل‌های مورد نظر ارسال کنید و به فرصت‌های شغلی دسترسی پیدا کنید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-7">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        امکانات امنیتی: حریم خصوصی و امنیت اطلاعات شما برای ما بسیار مهم است و ما با استفاده از فناوری‌های مدرن و استانداردهای امنیتی بالا، اطلاعات شما را محافظت می‌کنیم.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-8">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        راهنمایی و پشتیبانی: تیم پشتیبانی ما همیشه آماده است تا به شما در هر گونه سوال یا مشکل کمک کند و راهنمایی‌های لازم را ارائه دهد.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-9">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        آزمون‌های شخصیتی: با آزمون‌های شخصیتی ما، می‌توانید به تشخیص و درک عمیق‌تری از خودتان و مهارت‌هایتان بپردازید تا بهترین تصمیمات حرفه‌ای را بگیرید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-10">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        آزمون های رغبت‌سنجی: با رغبت‌سنجی، می‌توانید علایق، ارزش‌ها و اهداف خود را بهتر شناسایی کنید و شغلی را پیدا کنید که با آن بهترین همخوانی را دارید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-11">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        آزمون‌های استعدادیابی: با آزمون‌های استعدادیابی، می‌توانید مهارت‌ها، استعدادها و قابلیت‌های خود را بیابید و به سمت شغلی بروید که با آنان هماهنگی بیشتری دارید.
                                    </h4>
                                </div>
                            </li>
                            <li class="li-12">
                                <div>
                                    <h4 class="fs-6 mb-0 product-list">
                                        مشاوره حرفه‌ای: تیم مشاوران ما همواره آماده است تا به شما در انتخاب شغلی مناسب و توسعه حرفه‌ای کمک کند و سوالات شما را پاسخ دهد.
                                    </h4>
                                </div>
                            </li>
                            <li class="last-l">
                                <div>
                                </div>
                            </li>
                    
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="mt-5">
        <div class="d-flex">
            <h3 class="sub-title h4 fw-bold">معرفی برخی از امکانات جابیست</h3>
        </div>
        <div class="row row-cols-md-2 row-cols-1 mt-5 px-md-4">
            <div class="col px-4 mb-4">
                <div class="d-flex rounded-3r bg-light-blue align-items-center ps-3">
                    <div class="bg-yellow py-2 px-2 radius-feature d-flex align-items-center justify-content-center">
                        <img src="{{ asset('assets/frontend/img/about-us/file-lines-regular.png') }}" class="feature-icon">
                    </div>
                    <div class="me-3">
                        <p class="mb-0 fs-18 fw-medium text-f">انجام تست شخصیت شناسی</p>
                    </div>
                </div>
            </div>
            <div class="col px-4 mb-4">
                <div class="d-flex rounded-3r bg-light-blue align-items-center ps-3">
                    <div class="bg-yellow py-2 px-2 radius-feature d-flex align-items-center justify-content-center">
                        <img src="{{ asset('assets/frontend/img/about-us/display-solid.png') }}" class="feature-icon">
                    </div>
                    <div class="me-3">
                        <p class="mb-0 fs-18 fw-medium text-f">تجربه کاربری و رابط کاربری جدید</p>
                    </div>
                </div>
            </div>
            <div class="col px-4 mb-4">
                <div class="d-flex rounded-3r bg-light-blue align-items-center ps-3">
                    <div class="bg-yellow py-2 px-2 radius-feature d-flex align-items-center justify-content-center">
                        <img src="{{ asset('assets/frontend/img/about-us/building-user-solid.png') }}" class="feature-icon">
                    </div>
                    <div class="me-3">
                        <p class="mb-0 fs-18 fw-medium text-f">بخش آشنایی با محیط های کاری و سازمان ها</p>
                    </div>
                </div>
            </div>
            <div class="col px-4 mb-4">
                <div class="d-flex rounded-3r bg-light-blue align-items-center ps-3">
                    <div class="bg-yellow py-2 px-2 radius-feature d-flex align-items-center justify-content-center">
                        <img src="{{ asset('assets/frontend/img/about-us/Group 295.png') }}" class="feature-icon">
                    </div>
                    <div class="me-3">
                        <p class="mb-0 fs-18 fw-medium text-f">امکان ساخت چندین رزومه در یک حساب کاربری</p>
                    </div>
                </div>
            </div>
            <div class="col px-4 mb-4">
                <div class="d-flex rounded-3r bg-light-blue align-items-center ps-3">
                    <div class="bg-yellow py-2 px-2 radius-feature d-flex align-items-center justify-content-center">
                        <img src="{{ asset('assets/frontend/img/about-us/free-code-camp.png') }}" class="feature-icon">
                    </div>
                    <div class="me-3">
                        <p class="mb-0 fs-18 fw-medium text-f">رایگان شدن انتشار فرصت های شغلی</p>
                    </div>
                </div>
            </div>
            <div class="col px-4 mb-4">
                <div class="d-flex rounded-3r bg-light-blue align-items-center ps-3">
                    <div class="bg-yellow py-2 px-2 radius-feature d-flex align-items-center justify-content-center">
                        <img src="{{ asset('assets/frontend/img/about-us/user-plus-solid.png') }}" class="feature-icon">
                    </div>
                    <div class="me-3">
                        <p class="mb-0 fs-18 fw-medium text-f">ارتقای برند کارفرمایی</p>
                    </div>
                </div>
            </div>
        </div>
    </div>






</div>


@endsection