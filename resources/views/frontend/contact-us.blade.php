@extends('frontend.layout.master')

@section('meta')
    <meta name="description"  content=" در صورت داشتن هرگونه سوال، پیشنهاد یا انتقاد، با تیم جابیست تماس بگیرید. ما در خدمت شما هستیم تا بهترین پاسخ را ارائه دهیم." />
    <meta property="og:url" content="https://jobist.ir/contact-us"/>
    <meta property="og:title" content=" تماس با جابیست | ارتباط با تیم ما " />
    <meta property="og:description" content=" در صورت داشتن هرگونه سوال، پیشنهاد یا انتقاد، با تیم جابیست تماس بگیرید. ما در خدمت شما هستیم تا بهترین پاسخ را ارائه دهیم." /> 
    <link rel="canonical" href="https://jobist.ir/contact-us"/>
@endsection

@section('title' , 'ارتباط با تیم ما')
@section('page' , 'تماس با جابیست' )

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/contact-us.css') }}">
@endsection


@section('main')
    <div class="container width-section">
        <div class="row bg-pattern mt-5 rounded-4 shadow">
            <div class="col-12 col-md-6">
                <div class="px-4 mt-4">
                    <h1 class="text-white h2">تماس با ما</h1>
                    <p class="light-grey text-justify mt-3 mb-0 context">در اینجا شما دعوتید به اشتراگ گذاری نظرات، پیشنهادات و هرگونه سوال یا ابهاماتی  که دارید، با ما. ما از تمامی بازخورد های شما استقبال  میکنیم و تلاش خواهیم کرد تا به هر سوال و نیازی که داریرد، با سرعت و با دقت پاسخ دهیم. </p>
                    <div class="text-center mt-5">
                        <img src="{{ asset('assets/frontend/img/contact-us/Group 1279.png') }}" class="img-fluid image">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <form class="mt-5">
                    <div class="border-form pt-4 pt-md-0">
                        <div class="d-flex justify-content-center mb-4">
                            <input type="text" class="form-input" id="name" placeholder="نام و نام خانوادگی (اختیاری)">
                            <img src="{{ asset('assets/frontend/img/contact-us/user.png') }}" class="form-icon">
                        </div>
                        <div class="d-flex justify-content-center mb-4">
                            <input type="email" class="form-input" id="email" placeholder="آدرس ایمیل (اختیاری)">
                            <img src="{{ asset('assets/frontend/img/contact-us/mail(1).png') }}" class="form-icon">
                        </div>
                        <div class="d-flex justify-content-center mb-4">
                            <input type="text" class="form-input" id="comment" placeholder="نظر شما">
                            <img src="{{ asset('assets/frontend/img/contact-us/edit.png') }}" class="form-icon">
                        </div>
                        <div class="height-btn">
                            <button class="text-white bg-green btn rounded-pill float-start mt-5 ms-5 px-3 mb-4">ثبت نظر</button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>

    <div class="container width-section">
        <div class="row mt-5 rounded-4 border border-dark-subtle p-2 shadow-map">
            <div class="col-12 col-md-6 px-0 my-auto">
                <div class="ratio ratio-4x3">
                <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d107483.43271657874!2d51.31247455906513!3d32.663242078066844!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3fbdd56fca792395%3A0xa1dae1a44c4fe176!2sM97V%2B7XC%2C%20Najafabad%2C%20Isfahan%20Province!3m2!1d32.6632387!2d51.3952646!5e0!3m2!1sen!2s!4v1710570177619!5m2!1sen!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>                
                </div>
            </div>
            <div class="col-12 col-md-6 p-3 pb-2">
                <div class="border-box py-2 me-md-5 me-2">
                    <h4 class="my-3">شماره تماس</h4>
                    <p class="grey">۰۳۱-۴۲۲۹۳۳۰۵</p>
                </div>
                <div class="border-box py-2 me-md-5 me-2">
                    <h4 class="my-3">ایمیل</h4>
                    <p class="grey">info@jobist.ir</p>
                </div>
                <div class="border-box py-2 me-md-5 me-2">
                    <h4 class="my-3">آدرس</h4>
                    <p class="grey">پارک علم و فناوری دانشگاه آزاد اسلامی<br> نجف آباد</p>
                </div>
            </div>
        </div>
    </div>
@endsection