@extends('frontend.layout.master')

@section('meta')
    <meta name="description" content="تست شخصیت‌شناسی NEO در جابیست به بررسی پنج عامل اصلی شخصیت شما می‌پردازد و به شما در شناخت بهتر خود برای رشد فردی و حرفه‌ای کمک می‌کند." />
    <meta property="og:title" content="تست شخصیت‌شناسی NEO | ارزیابی پنج عامل اصلی شخصیت در جابیست" />
    <meta property="og:url" content="https://jobist.ir/neo" />
    <meta property="og:description" content="با تست شخصیت‌شناسی NEO در جابیست، ویژگی‌های شخصیتی خود را عمیق‌تر بشناسید و مهارت‌های خود را در زندگی و کار تقویت کنید." />
    <link rel="canonical" href="https://jobist.ir/neo" />
@endsection
@section('title','تست شخصیت شناسی نئو')
@section('page' , 'جابیست')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')


<div class="container-fluid section-test">
    <div class="mt-0 container">
        <div class="row flex-lg-row flex-column-reverse align-items-center h-fix-top">
            <div class="col-lg-8 col-12 mt-md-3 mt-0 py-4 pe-3">

                <div class="mt-md-4 mt-3 px-md-5 px-1">

                    <h2 class="blue fw-bold h1 mt-3">آزمون شخصیت شناسی NEO</h2>

                    <p class="fs-5 mt-3">
                        آزمون شخصیت‌شناسی نئو، ابزاری علمی و معتبر است که پنج بُعد اصلی شخصیت شامل برون‌گرایی، سازگاری، وظیفه‌شناسی، پایداری هیجانی و گشودگی به تجربه را ارزیابی می‌کند.
                    </p>


                    <div class="mt-3 d-flex flex-sm-row flex-column align-items-center gap-4">

                        <div class="d-flex align-items-center gap-4">
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-stopwatch ms-2 blue"></i>
                                <div>
                                    ۱۵ دقیقه
                                </div>
                            </div>
    
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-circle-question ms-2 blue"></i>
                                <div>
                                    ۶۰ سوال
                                </div>
                            </div>
                        </div>
                        <a href="{{route('neo.take.test')}}" class="btn2 text-white fw-bold">شروع آزمون</a>
                    </div>

      
                </div>

            </div>
            <div class="col-lg-4 mx-auto col-12 py-4">
                <div class="img-article-wrapper mx-auto">
                    <img src="{{asset('assets/images/neo-image.svg')}}" alt="آزمون شخصیت شناسی نئو" loading="lazy" class="article-img">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="p-md-5 p-3 shadow-section rounded-4 mt-2 bg-white">
            <h3 class="mb-3 blue fw-bold title z-0">دریچه‌ای به شناخت پنج بُعد اصلی شخصیت</h3>
            <p class="lh-p text-justify">
                آزمون شخصیت‌شناسی نئو (NEO Personality Inventory) یکی از معتبرترین ابزارهای روان‌شناسی برای ارزیابی شخصیت است که بر اساس مدل پنج عاملی شخصیت (Big Five) طراحی شده است. این آزمون، شخصیت را در قالب پنج بُعد اصلی تحلیل می‌کند و اطلاعات دقیقی درباره ویژگی‌های فردی، توانایی‌ها و نقاط ضعف ارائه می‌دهد. با توجه به علمی بودن این ابزار، آزمون نئو در حوزه‌های مختلفی از جمله مشاوره شغلی، روان‌درمانی و تحقیقات روان‌شناسی کاربرد دارد.
            </p>

            <h3 class="title mb-3 blue fw-bold z-0">کاربردهای آزمون نئو</h3>

            <p class="lh-p text-justify">
                آزمون نئو در زمینه‌های مختلفی کاربرد دارد:
                <br>
                <span class=" fw-semibold fs-18"> مشاوره شغلی:</span>
                این آزمون به افراد کمک می‌کند تا با شناخت بهتر شخصیت خود، شغل‌هایی را انتخاب کنند که با ویژگی‌های آن‌ها همخوانی داشته باشد.
                <br>

                <span class=" fw-semibold fs-18"> روان‌درمانی:</span>
                درک دقیق شخصیت فرد به روان‌درمان‌گر کمک می‌کند تا برنامه درمانی مناسبی طراحی کند و روی نقاط قوت و ضعف تمرکز کند.
                <br>
                <span class=" fw-semibold fs-18"> رشد فردی:</span>
                آزمون نئو به افراد کمک می‌کند تا الگوهای رفتاری و احساسی خود را بشناسند و برای رشد و بهبود تلاش کنند.
                <br>
                <span class=" fw-semibold fs-18"> تیم‌سازی و مدیریت منابع انسانی:</span>
                شناخت شخصیت کارکنان با استفاده از آزمون نئو، به مدیران کمک می‌کند تا تیم‌های متعادل و کارآمدی ایجاد کنند و محیط کاری مناسبی فراهم آورند.
            </p>


            <h3 class="title mb-3 blue fw-bold z-0">پایه‌های نظری آزمون نئو</h3>
            <p>
                آزمون نئو بر اساس مدل پنج عاملی شخصیت بنا شده است، که معتقد است شخصیت انسان را می‌توان در پنج بُعد کلیدی خلاصه کرد:
            </p>
            <p class="lh-p text-justify">

                <span class=" fw-semibold fs-18"> برون‌گرایی (Extraversion):</span>
                میزان تمایل فرد به برقراری ارتباط اجتماعی، تجربه هیجانات مثبت و کسب انرژی از تعاملات اجتماعی را می‌سنجد. افراد با نمره بالا در این بُعد معمولاً اجتماعی، پرانرژی و خوش‌بین هستند، در حالی که افراد با نمره پایین‌تر تمایل به انزوا و درون‌گرایی دارند.
                <br>

                <span class=" fw-semibold fs-18"> سازگاری (Agreeableness):</span>
                این بُعد نشان‌دهنده تمایل فرد به همکاری، همدلی و رفتارهای نوع‌دوستانه است. افراد با نمره بالا معمولاً مهربان، دلسوز و قابل‌اعتماد هستند، در حالی که افراد با نمره پایین‌تر ممکن است رقابت‌جو و منتقد باشند.
                <br>
                <span class=" fw-semibold fs-18"> وظیفه‌شناسی (Conscientiousness):</span>
                این بُعد به سطح سازمان‌دهی، پشتکار و مسئولیت‌پذیری فرد اشاره دارد. افراد وظیفه‌شناس معمولاً منظم، دقیق و هدف‌گرا هستند، در حالی که افراد با نمره پایین‌تر ممکن است بی‌برنامه و بی‌توجه به جزئیات باشند.
                <br>
                <span class=" fw-semibold fs-18"> پایداری هیجانی (Neuroticism):</span>
                این بُعد میزان استرس، اضطراب و ناپایداری عاطفی فرد را ارزیابی می‌کند. افراد با نمره پایین در این بُعد معمولاً آرام و مقاوم در برابر استرس هستند، در حالی که افراد با نمره بالا بیشتر مستعد تجربه نگرانی و احساسات منفی‌اند.
                <br>
                <span class=" fw-semibold fs-18"> گشودگی به تجربه (Openness to Experience):</span>
                این بُعد تمایل فرد به کنجکاوی، خلاقیت و تجربه‌های جدید را نشان می‌دهد. افراد با نمره بالا در این بُعد معمولاً خلاق، ماجراجو و علاقه‌مند به یادگیری‌اند، در حالی که افراد با نمره پایین‌تر ممکن است محافظه‌کار و سنت‌گرا باشند.
            </p>


        </div>
    </div>

</div>


@endsection
