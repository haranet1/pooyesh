@extends('frontend.layout.master')

@section('meta')
    <meta name="description" content="آزمون سلامت عمومی GHQ در جابیست ابزاری موثر برای ارزیابی سلامت روان شما است. این تست به شناسایی مشکلات روانی و بهبود کیفیت زندگی کمک می‌کند." />
    <meta property="og:title" content="آزمون سلامت عمومی GHQ | ارزیابی سلامت روان در جابیست" />
    <meta property="og:url" content="https://jobist.ir/ghq" />
    <meta property="og:description" content="با آزمون سلامت عمومی GHQ در جابیست، سلامت روان خود را بررسی کنید و گام‌های موثری برای بهبود زندگی فردی و حرفه‌ای خود بردارید." />
    <link rel="canonical" href="https://jobist.ir/ghq" />
@endsection
@section('title','آزمون سلامت عمومی GHQ')
@section('page' , 'جابیست')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')


<div class="container-fluid section-test">
    <div class="mt-0 container">
        <div class="row flex-lg-row flex-column-reverse align-items-center h-fix-top">
            <div class="col-lg-8 col-12 mt-md-3 mt-0 py-4 pe-3">

                <div class="mt-md-4 mt-3 px-md-5 px-1">

                    <h2 class="blue fw-bold h1 mt-3">آزمون سلامت عمومی GHQ</h2>

                    <p class="fs-5 mt-3">
                        آزمون سلامت عمومی GHQ یک ابزار معتبر برای ارزیابی وضعیت روانی و شناسایی مشکلات روان‌شناختی در افراد است.
                    </p>


                    <div class="mt-3 d-flex flex-sm-row flex-column align-items-center gap-4">

                        <div class="d-flex align-items-center gap-4">
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-stopwatch ms-2 blue"></i>
                                <div>
                                    ۵ دقیقه
                                </div>
                            </div>
    
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-circle-question ms-2 blue"></i>
                                <div>
                                    ۲۸ سوال
                                </div>
                            </div>
                        </div>
                        <a href="{{route('ghq.take.test')}}" class="btn2 text-white fw-bold">شروع آزمون</a>
                    </div>

      
                </div>

            </div>
            <div class="col-lg-4 mx-auto col-12 py-4">
                <div class="img-article-wrapper mx-auto">
                    <img src="{{asset('assets/images/ghq-image.svg')}}" alt="آزمون سلامت عمومی GHQ" loading="lazy" class="article-img">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="p-md-5 p-3 shadow-section rounded-4 mt-2 bg-white">
            <h3 class="mb-3 blue fw-bold title z-0">ابزاری برای سنجش روان‌شناختی</h3>
            <p class="lh-p text-justify">
                تست سلامت عمومی (GHQ) بـه بررسـی وضـعیت روانی و جسمانی فرد در یک ماهه اخیر می پردازد و شامل نشانه هـایی مانند افکار و احساسات نابهنجار و جنبه هایی از رفتار قابل مشاهده است که بر موقعیت اینجـا و اکنـون تأکیـد دارد. تست سلامت عمومی دو نگرانی اصلی را شناسایی می کند: 1) ناتوانی در انجام عملکردهای عادی و 2) ظهور پدیده های جدید و ناراحت کننده. این آزمون به شما نشان می دهد که وضع عمومی سلامت شما در چند هفته گذشته، چگونه بوده است. تست سلامت عمومی GHQ چهار اصلی را می سنجد: علائم جسمانی، علائم افسردگی، علائم اضطرابی و اختلال خواب و علائم کارکرد اجتماعی.

            </p>
            <p class="lh-p text-justify">
                آزمون سلامت عمومی (General Health Questionnaire یا GHQ) یکی از ابزارهای معتبر و پرکاربرد در روان‌شناسی است که به منظور شناسایی و ارزیابی مشکلات روانی و روان‌شناختی طراحی شده است. این آزمون توسط گلدبرگ در سال 1972 توسعه یافت و از آن زمان تاکنون در مطالعات روان‌شناسی و پزشکی در سراسر جهان استفاده شده است.
            </p>

            <h3 class="title mb-3 blue fw-bold z-0">هدف از آزمون GHQ</h3>

            <p class="lh-p text-justify">
                هدف اصلی آزمون GHQ شناسایی افرادی است که ممکن است با مشکلات روان‌شناختی مانند استرس، اضطراب، افسردگی یا دیگر اختلالات عاطفی مواجه باشند. این آزمون نه‌تنها در تشخیص مشکلات روانی کمک می‌کند، بلکه برای ارزیابی سطح سلامت روان در جمعیت‌های عمومی یا گروه‌های خاص (مانند دانشجویان، کارکنان و بیماران) نیز مفید است.
                <br>
                آزمون سلامت عمومی در نسخه‌های مختلفی ارائه می‌شود، از جمله نسخه‌های 12، 28، 30 و 60 سؤالی. رایج‌ترین نسخه، GHQ-28 است که شامل چهار مقیاس است:
                <br>
                <span class=" fw-semibold fs-18"> علائم جسمانی: </span>
                بررسی مشکلات بدنی که ممکن است با وضعیت روانی مرتبط باشند.
                <br>

                <span class=" fw-semibold fs-18"> اضطراب و بی‌خوابی:</span>
                ارزیابی نگرانی‌ها و اختلالات خواب
                <br>
                <span class=" fw-semibold fs-18"> اختلال در عملکرد اجتماعی:</span>
                سنجش توانایی فرد در انجام وظایف روزمره.
                <br>
                <span class=" fw-semibold fs-18"> افسردگی شدید: </span>
                شناسایی علائم افسردگی و افکار مرتبط با ناامیدی.
                <br>
                پاسخ‌دهندگان به سؤالات این آزمون معمولاً بر اساس یک مقیاس چهارگزینه‌ای پاسخ می‌دهند که نشان‌دهنده شدت یا فراوانی مشکلات است.
            </p>


            <h3 class="title mb-3 blue fw-bold z-0">ویژگی‌های منحصر‌به‌فرد GHQ</h3>
            <p class="lh-p text-justify">
                یکی از دلایل محبوبیت GHQ، سادگی و سرعت اجرا است. این آزمون به راحتی قابل تکمیل است و نیازی به زمان طولانی ندارد. علاوه بر این، GHQ ابزاری غربالگری است، به این معنا که برای شناسایی اولیه مشکلات روانی طراحی شده و جایگزین تشخیص‌های دقیق بالینی نمی‌شود.
            </p>
            <p class="lh-p text-justify">
                در دنیای پرشتاب امروز، حفظ سلامت روان به اندازه سلامت جسمانی اهمیت دارد. آزمون GHQ به عنوان یک ابزار غربالگری ساده، به افراد و متخصصان کمک می‌کند تا مشکلات روانی را در مراحل اولیه شناسایی کنند و اقدامات لازم برای درمان یا پیشگیری را انجام دهند.
            </p>


        </div>
    </div>

</div>


@endsection
