@extends('frontend.layout.master')

@section('meta')
    <meta name="description" content="تست شخصیت‌شناسی انیاگرام در جابیست به شما کمک می‌کند ویژگی‌های شخصیتی، نقاط قوت و ضعف خود را شناسایی کنید. این تست راهنمایی برای رشد فردی و حرفه‌ای شماست." />
    <meta property="og:title" content="تست شخصیت‌شناسی انیاگرام | کشف ویژگی‌های شخصیتی در جابیست" />
    <meta property="og:url" content="https://jobist.ir/enneagram" />
    <meta property="og:description" content="با تست شخصیت‌شناسی انیاگرام در جابیست، ابعاد مختلف شخصیت خود را کشف کنید و مسیر رشد شخصی و شغلی خود را هموار کنید." />
    <link rel="canonical" href="https://jobist.ir/enneagram" />
@endsection

@section('title','تست شخصیت شناسی انیاگرام')
@section('page' , 'جابیست')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')


<div class="container-fluid section-test">
    <div class="mt-0 container">
        <div class="row flex-lg-row flex-column-reverse align-items-center h-fix-top">
            <div class="col-lg-8 col-12 mt-md-3 mt-0 py-4 pe-3">

                <div class="mt-md-4 mt-3 px-md-5 px-1">

                    <h2 class="blue fw-bold h1 mt-3">آزمون شخصیت شناسی انیاگرام</h2>

                    <p class="fs-5 mt-3">
                        آزمون شخصیت‌شناسی انیاگرام با تحلیل انگیزه‌ها، الگوهای رفتاری و واکنش‌های احساسی، به شناخت عمیق‌تر از شخصیت شما می‌پردازد.
                    </p>


                    <div class="mt-3 d-flex flex-sm-row flex-column align-items-center gap-4">

                        <div class="d-flex align-items-center gap-4">
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-stopwatch ms-2 blue"></i>
                                <div>
                                    ۱۵ دقیقه
                                </div>
                            </div>
    
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-circle-question ms-2 blue"></i>
                                <div>
                                    ۶۰ سوال
                                </div>
                            </div>
                        </div>
                        <a href="{{route('enneagram.takeTest')}}" class="btn2 text-white fw-bold">شروع آزمون</a>
                    </div>

      
                </div>

            </div>
            <div class="col-lg-4 mx-auto col-12 py-4">
                <div class="img-article-wrapper mx-auto">
                    <img src="{{asset('assets/images/enneagram-image.svg')}}" alt="آزمون شخصیت شناسی انیاگرام" loading="lazy" class="article-img">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="p-md-5 p-3 shadow-section rounded-4 mt-2 bg-white">
            <h3 class="mb-3 blue fw-bold title z-0">آزمون شخصیت‌شناسی انیاگرام: سفری به درون شخصیت و انگیزه‌های شما</h3>
            <p class="lh-p text-justify">
                آزمون شخصیت‌شناسی انیاگرام یکی از ابزارهای قدرتمند خودشناسی است که به تحلیل عمیق شخصیت افراد می‌پردازد. این آزمون بر اساس نظریه‌ای شکل گرفته که شخصیت را به عنوان مجموعه‌ای از الگوهای رفتاری، انگیزشی و احساسی بررسی می‌کند. برخلاف برخی از آزمون‌های شخصیت‌شناسی که به ویژگی‌های سطحی‌تر توجه دارند، انیاگرام به ریشه‌های عمیق‌تری از رفتارها و تصمیم‌گیری‌های فردی می‌پردازد و به شما کمک می‌کند تا خود را درک کنید و روابط خود را بهبود بخشید.
            </p>
            <p class="lh-p text-justify">
                انیاگرام (Enneagram) یک مدل روان‌شناسی و خودشناسی است که تاریخچه آن به ترکیبی از روان‌شناسی مدرن، فلسفه و عرفان بازمی‌گردد. کلمه انیاگرام از دو واژه یونانی "Ennea" به معنی عدد 9 و "Gram" به معنی نمودار گرفته شده است. این نظریه بر این باور است که هر فرد دارای یک الگوی رفتاری و انگیزشی اصلی است که تعیین‌کننده نوع شخصیت اوست.
                <br>
                هدف انیاگرام این است که به افراد کمک کند تا انگیزه‌های پنهان پشت رفتارهای خود را بشناسند و بهتر بتوانند از چرخه‌های منفی و محدودکننده‌ای که بر زندگی‌شان تأثیر می‌گذارند، خارج شوند.
            </p>
            <h3 class="title mb-3 blue fw-bold z-0">چگونه انیاگرام شخصیت شما را بررسی می‌کند؟</h3>
            <p class="lh-p text-justify">آزمون انیاگرام با طرح مجموعه‌ای از سوالات عمیق، تلاش می‌کند تا به انگیزه‌ها، ارزش‌ها و الگوهای رفتاری شما پی ببرد. این آزمون به جای تمرکز بر رفتارهای بیرونی، سعی دارد بفهمد چرا شما به شیوه‌ای خاص عمل می‌کنید و چه عواملی شما را به سوی این رفتارها سوق می‌دهند.</p>
                <p class="lh-p text-justify">
                    این آزمون سه جنبه اصلی شخصیت را بررسی می‌کند:
                    <br>
                    <span class=" fw-semibold fs-18"> الگوهای فکری: چگونه مسائل را تحلیل و پردازش می‌کنید؟</span>
                   
                    <br>

                    <span class=" fw-semibold fs-18"> واکنش‌های احساسی: چطور با احساسات خود و دیگران برخورد می‌کنید؟</span>
            
                    <br>
                <span class=" fw-semibold fs-18"> شیوه‌های رفتاری: در شرایط مختلف چگونه عمل می‌کنید؟</span>
                </p>


            <h3 class="title mb-3 blue fw-bold z-0">کاربردهای آزمون انیاگرام</h3>
            <p>
                آزمون انیاگرام در حوزه‌های مختلف زندگی فردی و حرفه‌ای کاربرد دارد:
            </p>
            <p class="lh-p text-justify">

                <span class=" fw-semibold fs-18"> خودشناسی و رشد فردی:</span>
                انیاگرام به شما کمک می‌کند نقاط قوت و ضعف خود را بشناسید، انگیزه‌های رفتاری خود را درک کنید و با آگاهی بیشتری تصمیم بگیرید.
                <br>

                <span class=" fw-semibold fs-18"> روابط بین‌فردی:</span>
                این آزمون می‌تواند به شما نشان دهد چگونه با دیگران تعامل می‌کنید و چگونه می‌توانید روابط بهتری بسازید. درک انگیزه‌های افراد دیگر نیز به بهبود ارتباطات و کاهش سوءتفاهم‌ها کمک می‌کند.
                <br>
                <span class=" fw-semibold fs-18"> محیط کاری و تیم‌سازی:</span>
                شناخت انگیزه‌ها و سبک‌های شخصیتی کارکنان و همکاران می‌تواند به بهبود عملکرد تیمی و افزایش بهره‌وری کمک کند.
                <br>
                <span class=" fw-semibold fs-18"> مدیریت استرس و تعارض:</span>
                با شناخت الگوهای رفتاری خود، می‌توانید استرس‌های روزمره را بهتر مدیریت کنید و در مواجهه با تعارضات رفتاری مناسب‌تر داشته باشید.
            </p>


    
                <h3 class="title mb-3 blue fw-bold z-0">۹ تیپ شخصیتی در تست انیاگرام</h3>
                <p>
                    آزمون شخصیت‌شناسی انیاگرام افراد را بر اساس انگیزه‌ها، واکنش‌ها و الگوهای رفتاری به 9 تیپ شخصیتی دسته‌بندی می‌کند. هر تیپ شخصیتی در این مدل نمایانگر مجموعه‌ای از ویژگی‌ها، تمایلات و چالش‌های منحصر به فرد است که افراد را به سوی رفتارها و تصمیم‌گیری‌های خاص هدایت می‌کند. این دسته‌بندی‌ها به شما کمک می‌کنند تا بهتر بفهمید چرا به شیوه‌ای خاص رفتار می‌کنید و چگونه می‌توانید رشد و پیشرفت شخصی خود را تقویت کنید. در ادامه هر یک از این 9 تیپ شخصیتی به اختصار معرفی می‌شود:
                </p>
    
                <div class="row row-cols-1 row-cols-lg-3 gap-4 justify-content-center mb-5 p-3">
                    <div class="col p-3 realistic-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">تیپ 1 (کمال‌گرا):</h5>
                        <p class="mb-2 fw-semibold">افرادی اصول‌گرا، منظم و به دنبال عدالت که تمایل دارند همه چیز را به بهترین شکل ممکن انجام دهند.</p>

                    </div>
                    
                    <div class="col p-3 investigative-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">تیپ 2 (یاری‌گر):</h5>
                        <p class="mb-2 fw-semibold">افرادی مهربان، دلسوز و حمایت‌گر که اغلب تمرکز زیادی بر نیازهای دیگران دارند.</p>

                    </div>

                    <div class="col p-3 Artistic-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">تیپ 3 (موفقیت‌جو):</h5>
                        <p class="mb-2 fw-semibold">افرادی جاه‌طلب، هدف‌گرا و پرتلاش که به دنبال پیشرفت و دستیابی به موفقیت هستند.</p>

                    </div>

                    <div class="col p-3 Social-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">تیپ 4 (فردگرا):</h5>
                        <p class="mb-2 fw-semibold">افرادی خلاق، احساسی و منحصر به فرد که به دنبال هویت و معنای عمیق در زندگی هستند.</p>

                    </div>

                    <div class="col p-3 Enterprising-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">تیپ 5 (پژوهش‌گر):</h5>
                        <p class="mb-2 fw-semibold">افرادی تحلیل‌گر، مستقل و کنجکاو که به دنبال درک و جمع‌آوری دانش‌اند.</p>

                    </div>

                    <div class="col p-3 Conventional-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">تیپ 6 (وفادار):</h5>
                        <p class="mb-2 fw-semibold"> افرادی مسئولیت‌پذیر، وفادار و محتاط که به دنبال امنیت و ثبات هستند.</p>

                    </div>

                    <div class="col p-3 realistic-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">تیپ 7 (ماجراجو)</h5>
                        <p class="mb-2 fw-semibold">افرادی خوش‌بین، پرانرژی و جستجوگر که به دنبال تجربه‌های جدید و لذت‌بخش هستند.</p>

                    </div>

                    <div class="col p-3 investigative-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">تیپ 8 (چالش‌گر):</h5>
                        <p class="mb-2 fw-semibold">افرادی قوی، قاطع و مقتدر که به دنبال کنترل و دفاع از خود و دیگران هستند.</p>

                    </div>

                    <div class="col p-3 Social-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">تیپ 9 (صلح‌طلب): </h5>
                        <p class="mb-2 fw-semibold">افرادی آرام، هماهنگ و سازگار که به دنبال ایجاد تعادل و آرامش در زندگی هستند.</p>

                    </div>

                </div>

        </div>
    </div>

</div>


@endsection
