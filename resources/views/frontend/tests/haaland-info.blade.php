@extends('frontend.layout.master')

@section('meta')
    <meta name="description" content="تست رغبت‌سنجی هالند در جابیست به شما کمک می‌کند علاقه‌ها و استعدادهای شغلی خود را کشف کرده و شغلی متناسب با شخصیت خود انتخاب کنید." />
    <meta property="og:title" content="تست رغبت‌سنجی هالند | کشف علاقه‌ها و استعدادهای شغلی در جابیست" />
    <meta property="og:url" content="https://jobist.ir/haaland" />
    <meta property="og:description" content="با تست رغبت‌سنجی هالند در جابیست، مسیر شغلی متناسب با شخصیت خود را بیابید و تصمیمی آگاهانه برای آینده شغلی خود بگیرید." />
    <link rel="canonical" href="https://jobist.ir/haaland" />
@endsection
@section('title','آزمون رغبت شناسی هالند')
@section('page' , 'جابیست')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')


<div class="container-fluid section-test">
    <div class="mt-0 container">
        <div class="row flex-lg-row flex-column-reverse align-items-center h-fix-top">
            <div class="col-lg-8 col-12 mt-md-3 mt-0 py-4 pe-3">

                <div class="mt-md-4 mt-3 px-md-5 px-1">

                    <h2 class="blue fw-bold h1 mt-3">آزمون رغبت شناسی هالند</h2>

                    <p class="fs-5 mt-3">تست رغبت‌شناسی هالند، ابزاری کاربردی برای کشف علایق شغلی و انتخاب مسیر تحصیلی متناسب با شخصیت و توانمندی‌های شما است.</p>


                    <div class="mt-3 d-flex flex-sm-row flex-column align-items-center gap-4">

                        <div class="d-flex align-items-center gap-4">
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-stopwatch ms-2 blue"></i>
                                <div>
                                    ۵ دقیقه
                                </div>
                            </div>
    
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-circle-question ms-2 blue"></i>
                                <div>
                                    ۱۸ سوال
                                </div>
                            </div>
                        </div>
                        <a href="{{route('haaland.take.test')}}" class="btn2 text-white fw-bold">شروع آزمون</a>
                    </div>

      
                </div>

            </div>
            <div class="col-lg-4 mx-auto col-12 py-4">
                <div class="img-article-wrapper mx-auto">
                    <img src="{{asset('assets/images/haaland-image.svg')}}" alt="آزمون رغبت شناسی هالند" loading="lazy" class="article-img">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="p-md-5 p-3 shadow-section rounded-4 mt-2 bg-white">
            <h3 class="mb-3 blue fw-bold title z-0">رغبت شناسی هالند چگونه کار میکند</h3>
            <p class="lh-p text-justify">
                رغبت‌شناسی هالند یکی از معروف‌ترین و کاربردی‌ترین آزمون‌های روان‌شناسی در زمینه انتخاب شغل و مسیر تحصیلی است. این آزمون بر اساس نظریه "جان هالند"، روان‌شناس برجسته، طراحی شده و هدف آن شناسایی علایق و تمایلات فردی و تطبیق آن‌ها با مشاغل و محیط‌های کاری مناسب است. نظریه هالند بر این باور است که رضایت شغلی و موفقیت حرفه‌ای زمانی حاصل می‌شود که بین شخصیت فرد و محیط شغلی تناسب وجود داشته باشد.
            </p>
            <p class="lh-p text-justify">
                رغبت‌شناسی هالند نه تنها برای انتخاب مسیر شغلی، بلکه برای برنامه‌ریزی تحصیلی نیز ابزاری ارزشمند محسوب می‌شود. این آزمون به افراد کمک می‌کند تا با شناخت بهتر علایق خود، رشته تحصیلی مناسبی را انتخاب کنند که با شخصیت و تمایلاتشان همخوانی داشته باشد. از آنجایی که انتخاب نادرست می‌تواند منجر به کاهش انگیزه و رضایت در آینده شود، تست هالند با ارائه پیشنهادهای دقیق و علمی، به تصمیم‌گیری آگاهانه‌تر و موفقیت‌آمیزتر کمک می‌کند.
            </p>
            <h3 class="title mb-3 blue fw-bold z-0">کاربردهای تست هالند</h3>
            <p class="lh-p text-justify">تست هالند برای افراد در هر مرحله از زندگی، از دانش‌آموزان دبیرستانی گرفته تا افرادی که به دنبال تغییر شغل هستند، مفید است. این تست می‌تواند به شما کمک کند:</p>
                <p class="lh-p text-justify">
                    <span class=" fw-semibold fs-18"> علایق و تمایلات خود را بهتر بشناسید</span>
                   
                    <br>

                    <span class=" fw-semibold fs-18"> مشاغلی را انتخاب کنید که با شخصیت شما سازگار باشند.</span>
            
                    <br>
                <span class=" fw-semibold fs-18"> مسیر تحصیلی مناسبی را برای رسیدن به اهداف شغلی خود انتخاب کنید.</span>
            
                <br>
                <span class=" fw-semibold fs-18"> از تغییر مسیرهای ناموفق جلوگیری کنید و زمان و انرژی خود را بهینه‌تر مدیریت کنید.</span>
            
            <br>
            آنچه تست هالند را از دیگر ابزارهای خودشناسی متمایز می‌کند، تمرکز آن بر رابطه میان شخصیت و محیط شغلی است. این تست به جای ارزیابی مهارت‌های فنی، به علایق عمیق فردی توجه می‌کند و نشان می‌دهد که چگونه این علایق می‌توانند شما را در شغل موردنظر راضی و موفق نگه دارند.
                </p>
    
                <h3 class="title mb-3 blue fw-bold z-0">شش تیپ شخصیتی در مدل هالند</h3>
                <p>
                    نظریه هالند افراد را به شش تیپ شخصیتی تقسیم می‌کند. این تیپ‌ها بر اساس ویژگی‌ها، علایق و تمایلات فردی تعریف شده‌اند:
                </p>
    
                <div class="row row-cols-1 row-cols-lg-3 gap-4 justify-content-center mb-5 p-3">
                    <div class="col p-3 realistic-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">واقع‌گرا (Realistic):</h5>
                        <p class="mb-2 fw-semibold">افراد واقع‌گرا تمایل به فعالیت‌های عملی و فیزیکی دارند. این افراد معمولاً در شغل‌هایی مانند مهندسی، کشاورزی، یا مکانیک موفق هستند.</p>

                    </div>
                    <div class="col p-3 investigative-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">جستجوگر (Investigative):</h5>
                        <p class="mb-2 fw-semibold">این تیپ شخصیت به مسائل فکری، تحقیق و حل مشکلات علاقه‌مند است. علوم طبیعی، ریاضیات، و پژوهش‌های علمی از جمله زمینه‌های مناسب برای این افراد است.</p>

                    </div>
                    <div class="col p-3 Artistic-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">هنری (Artistic):</h5>
                        <p class="mb-2 fw-semibold">افراد هنری خلاق، حساس و مبتکر هستند و در زمینه‌هایی مانند هنر، نویسندگی، طراحی و موسیقی پیشرفت می‌کنند.</p>
                    </div>
                    <div class="col p-3 Social-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">اجتماعی (Social):</h5>
                        <p class="mb-2 fw-semibold">این تیپ شخصیت به کمک کردن به دیگران و ارتباطات انسانی علاقه‌مند است. شغل‌هایی مانند معلمی، مشاوره، یا پرستاری برای این افراد مناسب است.</p>
                    </div>
                    <div class="col p-3 Enterprising-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center mb-0">متهور (Enterprising):</h5>
                        <p class="mb-2 fw-semibold">
                            افراد متهور به فعالیت‌های مدیریتی و رهبری علاقه دارند. این تیپ معمولاً در تجارت، بازاریابی، یا سیاست موفق عمل می‌کند.
                        </p>
                    </div>
                    <div class="col p-3 Conventional-color rounded-4 pointer text-center">
                        <h5 class="h4 text-center">قراردادی (Conventional):</h5>
                        <p class="mb-2 fw-semibold">این افراد به فعالیت‌های سازمان‌یافته و ساختارمند علاقه دارند. شغل‌هایی مانند حسابداری، مدیریت داده‌ها، یا دفترداری برای آن‌ها مناسب است.</p>
                    </div>
                </div>

        </div>
    </div>

</div>


@endsection
