@extends('frontend.layout.master')

@section('meta')
    <meta name="description" content="تست شخصیت‌شناسی MBTI در جابیست به شما کمک می‌کند تیپ شخصیتی خود را شناسایی کنید و بهترین تصمیم‌ها را برای مسیر شغلی و زندگی شخصی‌تان بگیرید." />
    <meta property="og:title" content="تست شخصیت‌شناسی MBTI | شناخت تیپ شخصیتی در جابیست" />
    <meta property="og:url" content="https://jobist.ir/mbti" />
    <meta property="og:description" content="با تست شخصیت‌شناسی MBTI در جابیست، تیپ شخصیتی خود را بشناسید و توانایی‌ها و ویژگی‌های منحصر‌به‌فردتان را برای پیشرفت بهتر کشف کنید." />
    <link rel="canonical" href="https://jobist.ir/mbti" />
@endsection
@section('title','تست شخصیت شناسی MBTI')
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')


<div class="container-fluid section-test">
    <div class="mt-0 container">
        <div class="row flex-lg-row flex-column-reverse align-items-center h-fix-top">
            <div class="col-lg-8 col-12 mt-md-3 mt-0 py-4 pe-3">

                <div class="mt-md-4 mt-3 px-md-5 px-1">

                    <h2 class="blue fw-bold h1 mt-3">تست شخصیت شناسی MBTI</h2>

                    <p class="fs-5 mt-3">یک ابزار شخصیت‌شناسی است که با تحلیل تمایلات فردی در چهار بُعد مختلف، به شما کمک می‌کند تا نوع شخصیت و سبک تعاملات خود را بهتر بشناسید</p>


                    <div class="mt-3 d-flex flex-sm-row flex-column align-items-center gap-4">

                        <div class="d-flex align-items-center gap-4">
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-stopwatch ms-2 blue"></i>
                                <div>
                                    ۱۵ دقیقه
                                </div>
                            </div>
    
                            <div class="border-brand fit-content bg-white rounded-5 py-1 px-md-2 px-2 margin-brand d-flex align-items-center">
                                <i class="fa-solid fa-circle-question ms-2 blue"></i>
                                <div>
                                    ۶۰ سوال
                                </div>
                            </div>
                        </div>
                        <a href="{{route('mbti.takeTest')}}" class="btn2 text-white fw-bold">شروع آزمون</a>
                    </div>

      
                </div>

            </div>
            <div class="col-lg-4 mx-auto col-12 py-4">
                <div class="img-article-wrapper mx-auto">
                    <img src="{{asset('assets/images/mbti-image.svg')}}" alt="article-image" loading="lazy" class="article-img">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="p-md-5 p-3 shadow-section rounded-4 mt-2 bg-white">
            <h3 class="mb-3 blue fw-bold title z-0">تست MBTI چیست؟</h3>
            <p class="lh-p text-justify">تست MBTI (Myers-Briggs Type Indicator) یکی از مشهورترین ابزارهای شخصیت‌شناسی است که برای تحلیل ویژگی‌ها و تمایلات افراد در موقعیت‌های مختلف به کار می‌رود. این تست بر پایه نظریات روانشناس سوئیسی کارل گوستاو یونگ بنا شده و ویژگی‌های شخصیت افراد را بر اساس چهار بُعد مختلف بررسی می‌کند:</p>
            <p class="lh-p text-justify">تست MBTI با تحلیل چهار بُعد اصلی شخصیت شما، به شما کمک می‌کند تا نوع شخصیت خود را بشناسید. این تست شامل 16 تیپ شخصیتی مختلف است و به شما نشان می‌دهد که چگونه با دیگران تعامل می‌کنید، اطلاعات را پردازش می‌کنید و تصمیم‌گیری می‌کنید. با نتایج تست MBTI می‌توانید نه‌تنها شخصیت خود را بهتر درک کنید، بلکه در انتخاب‌های شغلی و روابط نیز آگاهانه‌تر عمل کنید.</p>
                <p class="lh-p text-justify">
                    <span class=" fw-semibold fs-18"> درون‌گرایی (I) یا برون‌گرایی (E):</span>
                   
                    <br>
            این بُعد به چگونگی تعامل افراد با دنیای بیرونی و نحوه دریافت انرژی آن‌ها اشاره دارد. افراد درون‌گرا بیشتر درونی و تأمل‌گرا هستند، در حالی که افراد برون‌گرا از تعاملات اجتماعی انرژی می‌گیرند.
                    <br>
                    <span class=" fw-semibold fs-18"> حسی (S) یا شهودی (N):</span>
            
            <br>
            این بُعد به نحوه دریافت اطلاعات افراد اشاره دارد. افراد حسی به حقایق و جزئیات توجه می‌کنند و به تجربیات ملموس اهمیت می‌دهند، در حالی که افراد شهودی تمایل دارند به ایده‌ها، امکانات، و الگوهای کلی فکر کنند.
            <br>
            <span class=" fw-semibold fs-18"> فکری (T) یا احساسی (F):</span>
            
            <br>
            این بُعد نشان می‌دهد افراد چگونه تصمیم می‌گیرند. افراد فکری براساس منطق و تحلیل تصمیم‌گیری می‌کنند، در حالی که افراد احساسی تمایل دارند با توجه به ارزش‌های شخصی و احساسات خود انتخاب کنند.
            <br>
            <span class=" fw-semibold fs-18"> قضاوت‌گرا (J) یا ملاحظه‌گر (P):</span>
            
            <br>
            این بُعد به نحوه سازمان‌دهی زندگی افراد مربوط است. افراد قضاوت‌گرا به نظم و برنامه‌ریزی اهمیت می‌دهند و دوست دارند همه چیز را تحت کنترل داشته باشند، در حالی که افراد ملاحظه‌گر انعطاف‌پذیرتر هستند و ترجیح می‌دهند گزینه‌ها را باز نگه دارند.
                </p>
    
                <h3 class="title mb-3 blue fw-bold z-0">۴ گروه اصلی در MBTI</h3>
                <p>در MBTI، تیپ‌های شخصیتی به چهار گروه اصلی تقسیم می‌شوند که هر گروه ویژگی‌های مشخصی دارد:</p>
    
                <h4 class="text-center mb-3">گروه تحلیل‌گرها (Analysts):</h4>
                <div class="row row-cols-2 row-cols-md-4 gap-3 justify-content-center mb-5">
                    <div class="col p-3 test-category analysts-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">معمار</p>
                        <h5 class="h4 text-center mb-0">INTJ</h5>
                    </div>
                    <div class="col p-3 test-category analysts-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">متفکر</p>
                        <h5 class="h4 text-center mb-0">INTP</h5>
                    </div>
                    <div class="col p-3 test-category analysts-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">فرمانده</p>
                        <h5 class="h4 text-center mb-0">ENTJ</h5>
                    </div>
                    <div class="col p-3 test-category analysts-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">مجادله‌گر</p>
                        <h5 class="h4 text-center mb-0">ENTP</h5>
                    </div>
                </div>


                <h4 class="text-center mb-3">گروه دیپلمات‌ها (Diplomats):</h4>
                <div class="row row-cols-2 row-cols-md-4 gap-3 justify-content-center mb-5">
                    <div class="col p-3 test-category diplomats-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">مبارز</p>
                        <h5 class="h4 text-center">ENFP</h5>
                    </div>
                    <div class="col p-3 test-category diplomats-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">قهرمان</p>
                        <h5 class="h4 text-center">ENFJ</h5>
                    </div>
                    <div class="col p-3 test-category diplomats-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">همدل</p>
                        <h5 class="h4 text-center">INFP</h5>
                    </div>
                    <div class="col p-3 test-category diplomats-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">مشاور</p>
                        <h5 class="h4 text-center">INFJ</h5>
                    </div>
                </div>


                <h4 class="text-center mb-3">گروه نگهبانان (Sentinels):</h4>
                <div class="row row-cols-2 row-cols-md-4 gap-3 justify-content-center mb-5">
                    <div class="col p-3 test-category sentinels-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">سفیر</p>
                        <h5 class="h4 text-center">ESFJ</h5>
                    </div>
                    <div class="col p-3 test-category sentinels-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">مجری</p>
                        <h5 class="h4 text-center">ESTJ</h5>
                    </div>
                    <div class="col p-3 test-category sentinels-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">مدافع</p>
                        <h5 class="h4 text-center">ISFJ</h5>
                    </div>
                    <div class="col p-3 test-category sentinels-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">تدارکچی</p>
                        <h5 class="h4 text-center">ISTJ</h5>
                    </div>
                </div>


                <h4 class="text-center mb-3">گروه کاوشگران (Explorers):</h4>
                <div class="row row-cols-2 row-cols-md-4 gap-3 justify-content-center mb-5">
                    <div class="col p-3 test-category explorers-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">سرگرم کننده</p>
                        <h5 class="h4 text-center">ESFP</h5>
                    </div>
                    <div class="col p-3 test-category explorers-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">کارآفرین</p>
                        <h5 class="h4 text-center">ESTP</h5>
                    </div>
                    <div class="col p-3 test-category explorers-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">ماجراجو</p>
                        <h5 class="h4 text-center">ISFP</h5>
                    </div>
                    <div class="col p-3 test-category explorers-border rounded-4 pointer text-center">
                        <p class="mb-2 fw-semibold">چیره‌دست</p>
                        <h5 class="h4 text-center">ISTP</h5>
                    </div>
                </div>
        </div>
    </div>

</div>


@endsection
