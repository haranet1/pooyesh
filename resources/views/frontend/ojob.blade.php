@extends('frontend.layout.master')
@section('meta')
    <meta name="description" content="اطلاعات کامل درباره موقعیت شغلی، شامل شرح وظایف، شرایط احراز، مزایا و فرصت‌های شغلی مرتبط. با آگاهی از جزئیات این شغل، تصمیم‌گیری بهتری برای آینده شغلی خود داشته باشید." />
    <meta property="og:url" content="https://jobist.ir/job/{{$job->id}}"/>
    <meta property="og:title" content=" اطلاعات موقعیت شغلی | جابیست " />
    <meta property="og:description" content="اطلاعات کامل درباره موقعیت شغلی، شامل شرح وظایف، شرایط احراز، مزایا و فرصت‌های شغلی مرتبط. با آگاهی از جزئیات این شغل، تصمیم‌گیری بهتری برای آینده شغلی خود داشته باشید." /> 
    <link rel="canonical" href="https://jobist.ir/job/{{$job->id}}"/>
@endsection
@section('title' , 'موقعیت های شغلی')
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/job-single.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/single-candidate.css') }}">
@endsection

@section('main')
    <style>
        #star {
                fill: #CDCDCD;
                transition: fill 200ms;
                z-index: 1;
            }

            #star.active {
                fill: #FFA600;
                animation: fav 600ms ease;
            }

            @keyframes fav {
            70% {
                transform: scale(1.5);
            }
            100% {
                transform: scale(1);
            }
        }
    </style>

    <script>
        let applicantCantSendInternRequest = false;
    </script>
    @if(Session::has('error'))
        <script>
            applicantCantSendInternRequest = true;
            let msg = '{{Session::pull('error')}}'
        </script>
    @endif
    <div class="container">
        <div class="Rectangl-2 position-relative rounded-4">
            <div class="mt-5 position-relative Rectangl-1 z-3 bg-white rounded-4">
                <div class="bg-image-top position-relative z-3 box-shadow-c rounded-4 p-4">
                    <div class="d-flex justify-content-between align-items-start z-3 flex-md-row flex-column-reverse">
                        <div>
                            <h2 class="mb-2 blue fs-3 fw-bold">{{ $job->title }}</h2>
                            <p class="mb-3 fs-14 badge badge-custom bg-info"> استاندارد شغلی 
                                <strong class="blue2">{{ $job->job->title }}</strong>
                                 از گروه شغلی 
                                <strong class="blue2">{{ $job->job->category->title }}</strong>
                            </p>
                            <h3 class="mb-3 fs-5">شرکت <a href="{{ route('company.single',$job->company->id) }}" class="blue2 fw-bold" target="_blank">{{ $job->company->groupable->name }}</a></h3>
                            @if ($job->province || $job->city)
                                <p class="text fs-18">
                                    @if ($job->province)
                                        {{ $job->province->name }}
                                    @endif
                                    @if ($job->province && $job->city)
                                        |
                                    @endif
                                    @if ($job->city)
                                        {{ $job->city->name }}
                                    @endif
                                    @if ($job->province && $job->city && $job->address && $job->address != 'unmatter')
                                        |
                                    @endif
                                    @if ($job->address && $job->address != 'unmatter')
                                        {{ $job->address}}
                                    @endif
                                </p>
                            @endif
                            <div class="border-dash-blue py-1 px-2 purple-light fw-bold fs-6">{{ job_type_persian($job->type) }}</div>
                        </div>
                        @auth
                            <div id="star" role="button" class="align-self-end align-self-md-start mb-sm-0 mb-2
                            @if (!is_null($favoriteJob))
                                active
                            @endif
                            ">
                                {{-- <img onclick="setFavorite(this,{{ $job->id }})" role="button" src="{{ asset('assets/frontend/img/job-single/bookmark-regular.svg') }}" class="w-75 h-auto"> --}}
                                
                                    <svg width="42px" height="40px" viewBox="0 0 42 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                          <path d="M21,34 L10.4346982,39.5545079 C8.47875732,40.5828068 7.19697214,39.6450119 7.56952871,37.4728404 L9.5873218,25.7082039 L1.03981311,17.3764421 C-0.542576313,15.8339937 -0.0467737017,14.3251489 2.13421047,14.0082334 L13.946577,12.2917961 L19.2292279,1.58797623 C20.2071983,-0.393608322 21.7954064,-0.388330682 22.7707721,1.58797623 L28.053423,12.2917961 L39.8657895,14.0082334 C42.0525979,14.3259953 42.5383619,15.8381017 40.9601869,17.3764421 L32.4126782,25.7082039 L34.4304713,37.4728404 C34.8040228,39.6508126 33.5160333,40.5800681 31.5653018,39.5545079 L21,34 Z" ></path>
                                    </svg>
                                
                            </div>
                            
                        @endauth
                    </div>
                    <div class="d-flex justify-content-between align-items-md-center mt-3 flex-md-row flex-column">
                        <p class="text fs-18 mb-0">{{ verta($job->updated_at)->formatDifference() }}
                        @if ($job->salary || $job->salary == 0)
                            . حقوق : {{ getPersionSalaryForOnetJob($job->salary) }}
                        @else
                        . حقوق : توافقی
                        @endif
                        </p>
                        <div class=" mt-md-0 mt-3 d-flex">
                            {{-- <button class="btn share-btn px-2 rounded-3">
                                <img src="{{ asset('assets/frontend/img/job-single/arrow-up-from-bracket-solid.svg') }}" class="w-75 h-auto">
                            </button> --}}
                            @if ((Auth::check() && Auth::user()->status == 2) || (Auth::check() && Auth::user()->groupable_type == App\Models\CompanyInfo::class))
                                <button class="btn cv-btn disabled me-3">شما مجاز به ارسال رزومه نیستید</button>
                            @else
                                @if ($hasCooperationRequest == true)
                                    <button class="btn cv-btn disabled me-3">رزومه ارسال شده است</button>
                            {{-- @elseif(Auth::user()->groupable_type == \App\Models\ApplicantInfo::class)
                                    <button class="btn cv-btn disabled me-3">شما نمیتوانید رزومه ایی ارسال کنید</button>--}}
                                
                                @elseif (Auth::check() && Auth::user()->status == 1 && Auth::user()->hasVerifiedEmail())
                                    {{--<input type="hidden" id="sendCooperationRequestUrl" value="{{ route('front.ajax.cooperation-request') }}">--}}
                                    <form action="{{ route('front.ajax.cooperation-request') }}" method="POST" class=" me-3">
                                        @csrf
                                        <input type="hidden" value="{{ $job->id }}" name="job_id">
                                        <button class="btn send-cv-btn text-white fs-6 fw-bold rounded-3 px-3" id="sendRequest-{{$job->id}}" type="submit" >ارسال رزومه</button>
                                    </form>
                                @else
                                    <button class="btn cv-btn disabled me-3">اکانت شما توسط دانشگاه تایید نشده است</button>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if ($job->about)
            <div class="border-dash-purple mt-10 p-4">
                <h3 class="title-job fs-4">شرح شغل</h3>
                <div class="Rectangl-3">
                    <div class="row p-2 box-shadow-c rounded-4 position-relative z-3 align-items-center">

                        <div class="col-12 col-md-6 mt-4 mt-md-2">
                            <div class="bg-white p-4 rounded-5 shadow-lg">
                                <h3 class=" fw-bold text-primary fs-5">توضیحات شرکت : </h3>
                                <div class="text fs-5 lh-lg">
                                    @php
                                        echo $job->about;
                                    @endphp
                                </div>
                            </div>
                            
                        </div>

                        <div class="col-12 col-md-6 mt-4 mt-md-2">
                            <div class="bg-white p-4 rounded-5 shadow-lg">
                                <h3 class=" fw-bold text-primary fs-5">استاندارد :</h3>
                                <p class="text fs-5 lh-lg">
                                    @php
                                        echo $job->description;
                                    @endphp
                                </p>
                            </div>
                        </div>
                        
                    </div>
                </div>

            </div>
        @endif

        {{-- <div class="border-dash-purple mt-10 p-4">
            <h3 class="title-job fs-4">شرح شغل (استاندارد)</h3>
            <div class="Rectangl-3">
                <div class="p-md-5 p-2 box-shadow-c rounded-4 position-relative z-3 bg-white">
                    
                </div>
            </div>

        </div> --}}

        @isset($tasks)
            @if ($tasks->count() > 0)
                <div class="border-dash-purple2 mt-10 p-3">
                    <div class="d-flex title-box2">
                        <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                        <h3 class="title-job2 fs-4">وظایف مورد انتظار</h3>
                    </div>
                    <div class="Rectangl-4 position-relative">
                        <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                            <div class="row align-items-center row-gap-1 fix-width-wrraper">
                                @foreach ($tasks as $task)
                                    {{-- <div class="col-6"> --}}
                                        <div class="row justify-content-center align-items-md-center">
                                            <p justify class="text fs-5 progress-title"> {{ $loop->index + 1 }} . {{ $task->task->title }} </p>
                                            {{-- <p class=""> مثال : {{ $task->example }} </p> --}}
                                        </div>
                                    {{-- </div> --}}
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endisset

        



        <div class="mt-10">
            <ul class="nav nav-underline pt-3 pe-4 custom-rounded" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                <button class="nav-link active text-black nav-btn fs-5 pb-2 text-white tab-title" id="job-details-d" data-bs-toggle="tab" data-bs-target="#job-details-d-pane" type="button" role="tab" aria-controls="job-details-d-pane" aria-selected="true">جزئیات موقعیت شغلی</button>
                </li>
                {{-- @if ($job->company->about) --}}
                    <li class="nav-item" role="presentation">
                        <button class="nav-link text-black nav-btn fs-5 pb-2 text-white tab-title" id="about-company-d" data-bs-toggle="tab" data-bs-target="#about-company-d-pane" type="button" role="tab" aria-controls="about-company-d-pane" aria-selected="false">درباره شرکت</button>
                    </li>
                {{-- @endif --}}
            </ul>
            <div class="tab-style position-relative"></div>
            <div class="tab-content pe-4 m-top" id="myTabContent">
                <div class="tab-pane fade show active px-md-4 px-1 pt-3 pb-sm-4 pb-2 rounded-bottom-4 box-shadow-c" id="job-details-d-pane" role="tabpanel" aria-labelledby="job-details-d" tabindex="0">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <table class="table table-borderless mt-sm-4 mt-1 job-table mb-0">
                                <tbody>
                                    <tr>
                                        <th class="w-50" scope="row">نوع همکاری:</th>
                                        <td class="w-50">{{ job_type_persian($job->type) }}</td>
                                    </tr>
                                    <tr>
                                        <th class="w-50" scope="row">سطح:</th>
                                        <td class="w-50">{{ job_level_persian($job->level) }}</td>
                                    </tr>
                                    <tr>
                                        <th class="w-50" scope="row">مقطع تحصیلی:</th>
                                        <td class="w-50">
                                            {{ $job->grade == 'unmatter' ? "فرقی ندارد" : $job->grade }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="w-50" scope="row">رشته تحصیلی:</th>
                                        <td class="w-50">
                                            {{ $job->major_id == null ? "فرقی ندارد" : $job->major->title }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="w-50" scope="row">جنسیت:</th>
                                        <td class="w-50">
                                            {{ $job->sex == 'unmatter' ? "فرقی ندارد" : change_sex_to_fa($job->sex) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-12 col-md-6">
                            <table class="table table-borderless mt-sm-4 mt-1 job-table mb-0">
                                <tbody>
                                    
                                    <tr>
                                        <th class="w-50" scope="row">بازه سنی:</th>
                                        <td class="w-50">
                                            {{$job->age == 0 || $job->age == 'unmatter' ? "فرقی ندارد" : ''}}
                                            {{$job->age == 20 ? "تا 20 سال" : ''}}
                                            {{$job->age == 30 ? "21 تا 30 سال" : ''}}
                                            {{$job->age == 40 ? "31 تا 40 سال" : ''}}
                                        </td>
                                    </tr>
                                    @if ($job->working_hours)
                                        <tr>
                                            <th class="w-50" scope="row">ساعت کاری:</th>
                                            <td class="w-50">
                                                {{ $job->working_hours }}
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($job->experience)
                                        <tr>
                                            <th class="w-50" scope="row">سابقه کاری:</th>
                                            <td class="w-50">
                                                {{ job_experience_to_fa($job->experience) }}
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($job->marige_type)
                                        <tr>
                                            <th class="w-50" scope="row">وضعیت تاهل:</th>
                                            <td class="w-50">
                                                {{ change_marige_type_to_fa($job->marige_type) }}
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($job->military_service_status)
                                        <tr>
                                            <th class="w-50" scope="row">وضعیت نظام وظیفه:</th>
                                            <td class="w-50">
                                                {{ $job->military_service_status == 'unmatter' ? 'فرقی ندارد' : $job->military_service_status }}
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- @if ($job->company->about) --}}
                    <div class="tab-pane fade pe-md-4 pe-2 pt-3 box-shadow-c rounded-bottom-4" id="about-company-d-pane" role="tabpanel" aria-labelledby="about-company-d" tabindex="0">
                        

                        <div class="d-flex flex-column justify-content-start">
                            <div class="d-flex flex-md-row align-items-center flex-column">
                                @if ($job->company->groupable->logo)
                                    <div class="p-4 logo-company-wrapper">
                                        <img src="{{ asset($job->company->groupable->logo->path) }}" class="img-fluid logo-company-img" alt="لوگوی شرکت {{ $job->company->groupable->name }}">
                                    </div>
                                @endif
                                <h3 class="blue fw-semibold px-4 mt-md-0 mt-4">{{ $job->company->groupable->name }}</h3>
                            </div>

                            <div class="mt-1 w-100 py-2">

                                <div class="row row-cols-lg-3 row-cols-md-2 row-cols-1 mt-3 row-gap-4 px-4 justify-content-center">
                                    <div class="col">
                                        <div class="px-lg-2 px-md-4">
                                            <div>
                                                <h5 class="fs-6 mb-2 text-grey">حوزه کاری:</h5>
                                                <p class="fs-18">{{ $job->company->groupable->activity_field }}</p>
                                            </div>
                                            <div class="mt-md-3 mt-2">
                                                <h5 class="fs-6 mb-2 text-grey">تلفن:</h5>
                                                <p class="fs-18 fanumber">{{ $job->company->groupable->phone }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="px-lg-2 px-md-4">
                                            <div>
                                                <h5 class="fs-6 mb-2 text-grey">اندازه شرکت:</h5>
                                                <p class="fs-18 fanumber">{{ $job->company->groupable->size }}</p>
                                            </div>
                                            <div class="mt-md-3 mt-2">
                                                <h5 class="fs-6 mb-2 text-grey">ایمیل:</h5>
                                                <p class="fs-18">{{ $job->company->email }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="px-2">
                                            <div>
                                                <h5 class="fs-6 mb-2 text-grey">تاسیس شده در:</h5>
                                                <p class="fs-18 fanumber">{{ $job->company->groupable->year }}</p>
                                            </div>
                                            <div class="mt-md-3 mt-2">
                                                <h5 class="fs-6 mb-2 text-grey fanumber">مکان:</h5>
                                                
                                                @if ($job->company->groupable->province)
                                                    <p class="fs-18">{{ $job->company->groupable->province->name }} ،
            
                                                        @if ($job->company->groupable->city)
            
                                                            {{ $job->company->groupable->city->name }}
            
                                                        @elseif ($job->company->groupable->city)
            
                                                            {{ $job->company->groupable->city }}
                                                            
                                                        @endif
                                                    </p>
                                                @else
                                                    <p class="fs-18">ایران،
            
                                                        @if ($job->company->groupable->city)
            
                                                            {{ $job->company->groupable->city }}
                                                            
                                                        @endif
            
                                                    </p>
                                                @endif
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="text fs-5 lh-lg p-3">
                                    @php
                                        echo $job->company->about
                                    @endphp
                                </p>
                            </div>
                        </div>
                        
                        <p class="text fs-5 lh-lg p-3">
                            @php
                                echo $job->company->about
                            @endphp
                        </p>
                    </div>
                {{-- @endif --}}
            </div>
        </div>

        <div class="row">
            @if ($job->oldSkills->count() > 0)
                <div class="border-dash-purple2 mt-10 p-3 col-12 col-md-6">
                    <div class="d-flex title-box2">
                        <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                        <h3 class="title-job2 fs-4">مهارت های مورد نیاز</h3>
                    </div>
                    <div class="Rectangl-4 position-relative">
                        <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                            <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap mb-4">
                                <div class="d-flex align-items-center ms-md-3 ms-2">
                                    <div class="dot-excellent ms-1"></div>
                                    <span class="fs-5 fw-medium excellent">عالی</span>
                                </div>
                                <div class="d-flex align-items-center ms-md-3 ms-2">
                                    <div class="dot-good ms-1"></div>
                                    <span class="fs-5 fw-medium good">خوب</span>
                                </div>
                                <div class="d-flex align-items-center ms-md-3 ms-2">
                                    <div class="dot-fair ms-1"></div>
                                    <span class="fs-5 fw-medium fair">متوسط</span>
                                </div>
                                <div class="d-flex align-items-center">
                                    <div class="dot-poor ms-1"></div>
                                    <span class="fs-5 fw-medium poor">ضعیف</span>
                                </div>
                            </div>
                            <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                                <div class="d-flex flex-column w-100">
                                    @foreach ($job->oldSkills as $skill)
                                        <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                            <h4 class="text fs-5 progress-title">{{ $skill->name }}</h4>
                                            <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح مهارت مورد نیاز">
                                                <div class="progress-bar
                                                    @if(changeFaLevelToOnetLevel($skill->pivot->skill_level) == 4)
                                                        bg-excellent
                                                    @elseif(changeFaLevelToOnetLevel($skill->pivot->skill_level) == 3)
                                                        bg-good
                                                    @elseif(changeFaLevelToOnetLevel($skill->pivot->skill_level) == 2)
                                                        bg-fair
                                                    @elseif(changeFaLevelToOnetLevel($skill->pivot->skill_level) == 1)
                                                        bg-poor
                                                    @endif
                                                " style="width:
                                                    @if(changeFaLevelToOnetLevel($skill->pivot->skill_level) == 4)
                                                        100%
                                                    @elseif(changeFaLevelToOnetLevel($skill->pivot->skill_level) == 3)
                                                        75%
                                                    @elseif(changeFaLevelToOnetLevel($skill->pivot->skill_level) == 2)
                                                        50%
                                                    @elseif(changeFaLevelToOnetLevel($skill->pivot->skill_level) == 1)
                                                        25%
                                                    @endif
                                                "></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @isset($skills)
                @if ($skills->count() > 0)
                    <div class="col-12 col-md-6">
                        <div class="border-dash-purple2 mt-10 p-3">
                            <div class="d-flex title-box2">
                                <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                                <h3 class="title-job2 fs-4">مهارت ها مورد نیاز</h3>
                            </div>
                            <div class="Rectangl-4 position-relative">
                                <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                                    <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap mb-4">
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-excellent ms-1"></div>
                                            <span class="fs-5 fw-medium excellent">عالی</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-good ms-1"></div>
                                            <span class="fs-5 fw-medium good">خوب</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-fair ms-1"></div>
                                            <span class="fs-5 fw-medium fair">متوسط</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <div class="dot-poor ms-1"></div>
                                            <span class="fs-5 fw-medium poor">ضعیف</span>
                                        </div>
                                    </div>
                                    <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                                        <div class="d-flex flex-column w-100">
                                            @foreach ($skills as $skill)
                                                <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                    <h4 class="text fs-5 progress-title">{{ $skill->skill->title }}</h4>
                                                    <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح مهارت مورد نیاز">
                                                        <div class="progress-bar
                                                            @if($skill->importance == 5)
                                                                bg-excellent
                                                            @elseif($skill->importance == 4)
                                                                bg-good
                                                            @elseif($skill->importance == 3)
                                                                bg-fair
                                                            @elseif($skill->importance <= 2 )
                                                                bg-poor
                                                            @endif
                                                        " style="width:
                                                            @if($skill->importance == 5)
                                                                100%
                                                            @elseif($skill->importance == 4)
                                                                75%
                                                            @elseif($skill->importance == 3)
                                                                50%
                                                            @elseif($skill->importance <= 2 )
                                                                25%
                                                            @endif
                                                        "></div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endisset

            @isset($knowledges)
                @if ($knowledges->count() > 0)
                    <div class="col-12 col-md-6">
                        <div class="border-dash-purple2 mt-10 p-3">
                            <div class="d-flex title-box2">
                                <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                                <h3 class="title-job2 fs-4">دانش مورد نیاز</h3>
                            </div>
                            <div class="Rectangl-4 position-relative">
                                <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                                    <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap mb-4">
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-excellent ms-1"></div>
                                            <span class="fs-5 fw-medium excellent">عالی</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-good ms-1"></div>
                                            <span class="fs-5 fw-medium good">خوب</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-fair ms-1"></div>
                                            <span class="fs-5 fw-medium fair">متوسط</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <div class="dot-poor ms-1"></div>
                                            <span class="fs-5 fw-medium poor">ضعیف</span>
                                        </div>
                                    </div>
                                    <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                                        <div class="d-flex flex-column w-100">
                                            @foreach ($knowledges as $knowledge)
                                                <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                    <h4 class="text fs-5 progress-title ">{{ $knowledge->knowledge->title }}</h4>
                                                    <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح دانش مورد نیاز">
                                                        <div class="progress-bar
                                                            @if($knowledge->importance == 5)
                                                                bg-excellent
                                                            @elseif($knowledge->importance == 4)
                                                                bg-good
                                                            @elseif($knowledge->importance == 3)
                                                                bg-fair
                                                            @elseif($knowledge->importance <= 2 )
                                                                bg-poor
                                                            @endif
                                                        " style="width:
                                                            @if($knowledge->importance == 5)
                                                                100%
                                                            @elseif($knowledge->importance == 4)
                                                                75%
                                                            @elseif($knowledge->importance == 3)
                                                                50%
                                                            @elseif($knowledge->importance <= 2 )
                                                                25%
                                                            @endif
                                                        "></div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endisset

            @isset($abilities)
                @if ($abilities->count() > 0)
                    <div class="col-12 col-md-6">
                        <div class="border-dash-purple2 mt-10 p-3">
                            <div class="d-flex title-box2">
                                <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                                <h3 class="title-job2 fs-4">توانایی ها مورد نیاز</h3>
                            </div>
                            <div class="Rectangl-4 position-relative">
                                <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                                    <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap mb-4">
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-excellent ms-1"></div>
                                            <span class="fs-5 fw-medium excellent">عالی</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-good ms-1"></div>
                                            <span class="fs-5 fw-medium good">خوب</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-fair ms-1"></div>
                                            <span class="fs-5 fw-medium fair">متوسط</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <div class="dot-poor ms-1"></div>
                                            <span class="fs-5 fw-medium poor">ضعیف</span>
                                        </div>
                                    </div>
                                    <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                                        <div class="d-flex flex-column w-100">
                                            @foreach ($abilities as $ability)
                                                <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                    <h4 class="text fs-5 progress-title">{{ $ability->ability->title }}</h4>
                                                    <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح توانایی مورد نیاز">
                                                        <div class="progress-bar
                                                            @if($ability->importance == 5)
                                                                bg-excellent
                                                            @elseif($ability->importance == 4)
                                                                bg-good
                                                            @elseif($ability->importance == 3)
                                                                bg-fair
                                                            @elseif($ability->importance <= 2 )
                                                                bg-poor
                                                            @endif
                                                        " style="width:
                                                            @if($ability->importance == 5)
                                                                100%
                                                            @elseif($ability->importance == 4)
                                                                75%
                                                            @elseif($ability->importance == 3)
                                                                50%
                                                            @elseif($ability->importance <= 2 )
                                                                25%
                                                            @endif
                                                        "></div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endisset

            @isset($workStyles)
                @if ($workStyles->count() > 0)
                    <div class="col-12 col-md-6">
                        <div class="border-dash-purple2 mt-10 p-3">
                            <div class="d-flex title-box2">
                                <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                                <h3 class="title-job2 fs-5">ویژگی های فردی در محیط کاری</h3>
                            </div>
                            <div class="Rectangl-4 position-relative">
                                <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                                    <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap mb-4">
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-excellent ms-1"></div>
                                            <span class="fs-5 fw-medium excellent">عالی</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-good ms-1"></div>
                                            <span class="fs-5 fw-medium good">خوب</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-md-3 ms-2">
                                            <div class="dot-fair ms-1"></div>
                                            <span class="fs-5 fw-medium fair">متوسط</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <div class="dot-poor ms-1"></div>
                                            <span class="fs-5 fw-medium poor">ضعیف</span>
                                        </div>
                                    </div>
                                    <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                                        <div class="d-flex flex-column w-100">
                                            @foreach ($workStyles as $workStyle)
                                                <div class="d-flex mb-3 flex-md-row flex-column flex-wrap align-items-md-center">
                                                    <h4 class="text fs-5 progress-title">{{ $workStyle->workStyle->title }}</h4>
                                                    <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" aria-label="سطح ویژگی های فردی مورد نیاز">
                                                        <div class="progress-bar
                                                            @if($workStyle->importance == 5)
                                                                bg-excellent
                                                            @elseif($workStyle->importance == 4)
                                                                bg-good
                                                            @elseif($workStyle->importance == 3)
                                                                bg-fair
                                                            @elseif($workStyle->importance <= 2 )
                                                                bg-poor
                                                            @endif
                                                        " style="width:
                                                            @if($workStyle->importance == 5)
                                                                100%
                                                            @elseif($workStyle->importance == 4)
                                                                75%
                                                            @elseif($workStyle->importance == 3)
                                                                50%
                                                            @elseif($workStyle->importance <= 2 )
                                                                25%
                                                            @endif
                                                        "></div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endisset

            @isset($technologySkills)
                @if ($technologySkills->count() > 0)
                    <div class="border-dash-purple2 mt-10 p-3">
                        <div class="d-flex title-box2">
                            <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2" alt="Rhombus icon">
                            <h3 class="title-job2 fs-4">مهارت های فنی مورد نیاز</h3>
                        </div>
                        <div class="Rectangl-4 position-relative">
                            <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                                <div class="row align-items-center row-gap-3 fix-width-wrraper">
                                    @foreach ($technologySkills as $technologySkill)
                                        <div class="col-6">
                                            <div class="row justify-content-center  mb-3 align-items-md-center">
                                                <h4 class="text fs-5 progress-title"> {{ $technologySkill->technologySkill->title }} </h4>
                                                @if (!is_null($technologySkill->jobTechnologySkill))
                                                    <p class=""> مثال : {{ $technologySkill->jobTechnologySkill->example->example ?? '' }} </p>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endisset
        </div>

        
        
    </div>

    <input type="hidden" id="job_id" value="{{ $job->id }}">
    <input type="hidden" id="addToFavoriteUrl" value="{{ route('add.ojob.to.favorite.ajax') }}">
@endsection

@section('script')
    <script src="{{ asset('assets/frontend/js/ojob.js') }}"></script>
@endsection

