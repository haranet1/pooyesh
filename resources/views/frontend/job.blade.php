@extends('frontend.layout.master')

@section('title' , 'موقعیت های شغلی')
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/job-single.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/single-candidate.css') }}">
@endsection

@section('main')
    <script>
        let applicantCantSendInternRequest = false;
    </script>
    @if(Session::has('error'))
        <script>
            applicantCantSendInternRequest = true;
            let msg = '{{Session::pull('error')}}'
        </script>
    @endif
    <div class="container">
        <div class="Rectangl-2 position-relative rounded-4">
            <div class="mt-5 position-relative Rectangl-1 z-3 bg-white rounded-4">
                <div class="bg-image-top position-relative z-3 box-shadow-c rounded-4 p-4">
                    <div class="d-flex justify-content-between align-items-start z-3 flex-md-row flex-column-reverse">
                        <div>
                            <h2 class="mb-3 blue fs-3 fw-bold">{{ $job->title }}</h2>
                            <h3 class="mb-3 blue2 fs-5 fw-bold">شرکت : {{ $job->company->groupable->name }}</h3>
                            @if ($job->province || $job->city)
                                <p class="text fs-18">
                                    @if ($job->province)
                                        {{ $job->province->name }}
                                    @endif
                                    @if ($job->province && $job->city)
                                        |
                                    @endif
                                    @if ($job->city)
                                        {{ $job->city->name }}
                                    @endif
                                </p>
                            @endif
                            <div class="border-dash-blue py-1 px-2 purple-light fw-bold fs-6">{{ job_type_persian($job->type) }}</div>
                        </div>
                        <div class="align-self-end align-self-md-start mb-sm-0 mb-2">
                            <img src="{{ asset('assets/frontend/img/job-single/bookmark-regular.svg') }}" class="w-75 h-auto">
                        </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-md-center mt-3 flex-md-row flex-column">
                        <p class="text fs-18 mb-0">{{ verta($job->updated_at)->formatDifference() }}
                        @if ($job->salary || $job->salary == 0)
                            . حقوق : {{ $job->salary }} تومان
                        @else
                        . حقوق : توافقی
                        @endif
                        </p>
                        <div class=" mt-md-0 mt-3 d-flex">
                            <button class="btn share-btn px-2 rounded-3">
                                <img src="{{ asset('assets/frontend/img/job-single/arrow-up-from-bracket-solid.svg') }}" class="w-75 h-auto">
                            </button>
                            @if ((Auth::check() && Auth::user()->status == 2) || Auth::user()->groupable_type == App\Models\CompanyInfo::class)
                                <button class="btn cv-btn disabled me-3">شما مجاز به ارسال رزومه نیستید</button>
                            @else
                                @if ($hasCooperationRequest == true)
                                    <button class="btn cv-btn disabled me-3">رزومه ارسال شده است</button>
                            {{-- @elseif(Auth::user()->groupable_type == \App\Models\ApplicantInfo::class)
                                    <button class="btn cv-btn disabled me-3">شما نمیتوانید رزومه ایی ارسال کنید</button>--}}
                                
                                @elseif (Auth::user()->status == 1 && Auth::user()->hasVerifiedEmail())
                                    {{--<input type="hidden" id="sendCooperationRequestUrl" value="{{ route('front.ajax.cooperation-request') }}">--}}
                                    <form action="{{ route('front.ajax.cooperation-request') }}" method="POST" class=" me-3">
                                        @csrf
                                        <input type="hidden" value="{{ $job->id }}" name="job_id">
                                        <button class="btn send-cv-btn text-white fs-6 fw-bold rounded-3 px-3" id="sendRequest-{{$job->id}}" type="submit" >ارسال رزومه</button>
                                    </form>
                                @else
                                    <button class="btn cv-btn disabled me-3">اکانت شما توسط دانشگاه تایید نشده است</button>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="border-dash-purple mt-10 p-4">
            <h3 class="title-job fs-4">شرح شغل و وظیفه</h3>
            <div class="Rectangl-3">
                <div class="p-md-5 p-2 box-shadow-c rounded-4 position-relative z-3 bg-white">
                    <p class="text fs-5 lh-lg">
                        @php
                            echo $job->description;
                        @endphp
                    </p>
                </div>
            </div>

        </div>



        <div class="mt-10">
            <ul class="nav nav-underline pt-3 pe-4 custom-rounded" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                <button class="nav-link active text-black nav-btn fs-5 pb-2 text-white tab-title" id="job-details-d" data-bs-toggle="tab" data-bs-target="#job-details-d-pane" type="button" role="tab" aria-controls="job-details-d-pane" aria-selected="true">جزئیات مورد شغلی</button>
                </li>
                {{-- @if ($job->company->about) --}}
                    <li class="nav-item" role="presentation">
                        <button class="nav-link text-black nav-btn fs-5 pb-2 text-white tab-title" id="about-company-d" data-bs-toggle="tab" data-bs-target="#about-company-d-pane" type="button" role="tab" aria-controls="about-company-d-pane" aria-selected="false">درباره شرکت</button>
                    </li>
                {{-- @endif --}}
            </ul>
            <div class="tab-style position-relative"></div>
            <div class="tab-content pe-4 m-top" id="myTabContent">
                <div class="tab-pane fade show active px-md-4 px-1 pt-3 pb-sm-4 pb-2 rounded-bottom-4 box-shadow-c" id="job-details-d-pane" role="tabpanel" aria-labelledby="job-details-d" tabindex="0">
                    <table class="table table-borderless mt-sm-4 mt-1 job-table">
                        <tbody>
                            <tr>
                                <th scope="row">نوع همکاری:</th>
                                <td>{{ job_type_persian($job->type) }}</td>
                            </tr>
                            <tr>
                                <th scope="row">گروه شغلی:</th>
                                <td>{{ $job->category->name }}</td>
                            </tr>
                            <tr>
                                <th scope="row">سطح:</th>
                                <td>{{ job_level_persian($job->level) }}</td>
                            </tr>
                            <tr>
                                <th scope="row">مقطع تحصیلی:</th>
                                <td>
                                    {{ $job->academic_level == 0 ? "فرقی ندارد" : $job->academic_level }}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">جنسیت:</th>
                                <td>
                                    {{ $job->sex == 0 ? "فرقی ندارد" : $job->sex }}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">بازه سنی:</th>
                                <td>
                                    {{$job->age == 0 ? "فرقی ندارد" : ''}}
                                    {{$job->age == 20 ? "تا 20 سال" : ''}}
                                    {{$job->age == 30 ? "21 تا 30 سال" : ''}}
                                    {{$job->age == 40 ? "31 تا 40 سال" : ''}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {{-- @if ($job->company->about) --}}
                    <div class="tab-pane fade pe-md-4 pe-2 pt-3 box-shadow-c rounded-bottom-4" id="about-company-d-pane" role="tabpanel" aria-labelledby="about-company-d" tabindex="0">
                        

                        <div class="d-flex flex-column justify-content-start">
                            <div class="d-flex flex-md-row align-items-center flex-column">
                                @if ($job->company->groupable->logo)
                                    <div class="p-4 logo-company-wrapper">
                                        <img src="{{ asset($job->company->groupable->logo->path) }}" class="img-fluid logo-company-img">
                                    </div>
                                @endif
                                <h3 class="blue fw-semibold px-4 mt-md-0 mt-4">{{ $job->company->groupable->name }}</h3>
                            </div>

                            <div class="mt-1 w-100 py-2">

                                <div class="row row-cols-lg-3 row-cols-md-2 row-cols-1 mt-3 row-gap-4 px-4 justify-content-center">
                                    <div class="col">
                                        <div class="px-lg-2 px-md-4">
                                            <div>
                                                <h5 class="fs-6 mb-2 text-grey">حوزه کاری:</h5>
                                                <p class="fs-18">{{ $job->company->groupable->activity_field }}</p>
                                            </div>
                                            <div class="mt-md-3 mt-2">
                                                <h5 class="fs-6 mb-2 text-grey">تلفن:</h5>
                                                <p class="fs-18 fanumber">{{ $job->company->groupable->phone }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="px-lg-2 px-md-4">
                                            <div>
                                                <h5 class="fs-6 mb-2 text-grey">اندازه شرکت:</h5>
                                                <p class="fs-18 fanumber">{{ $job->company->groupable->size }}</p>
                                            </div>
                                            <div class="mt-md-3 mt-2">
                                                <h5 class="fs-6 mb-2 text-grey">ایمیل:</h5>
                                                <p class="fs-18">{{ $job->company->email }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="px-2">
                                            <div>
                                                <h5 class="fs-6 mb-2 text-grey">تاسیس شده در:</h5>
                                                <p class="fs-18 fanumber">{{ $job->company->groupable->year }}</p>
                                            </div>
                                            <div class="mt-md-3 mt-2">
                                                <h5 class="fs-6 mb-2 text-grey fanumber">مکان:</h5>
                                                
                                                @if ($job->company->groupable->province)
                                                    <p class="fs-18">{{ $job->company->groupable->province->name }} ،
            
                                                        @if ($job->company->groupable->city)
            
                                                            {{ $job->company->groupable->city->name }}
            
                                                        @elseif ($job->company->groupable->city)
            
                                                            {{ $job->company->groupable->city }}
                                                            
                                                        @endif
                                                    </p>
                                                @else
                                                    <p class="fs-18">ایران،
            
                                                        @if ($job->company->groupable->city)
            
                                                            {{ $job->company->groupable->city }}
                                                            
                                                        @endif
            
                                                    </p>
                                                @endif
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="text fs-5 lh-lg p-3">
                                    @php
                                        echo $job->company->about
                                    @endphp
                                </p>
                            </div>
                        </div>
                        
                        <p class="text fs-5 lh-lg p-3">
                            @php
                                echo $job->company->about
                            @endphp
                        </p>
                    </div>
                {{-- @endif --}}
            </div>
        </div>

        @if ($job->skills && count($job->skills) > 0)
            <div class="border-dash-purple2 mt-10 p-3">
                <div class="d-flex title-box2">
                    <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2">
                    <h3 class="title-job2 fs-4">مهارت ها</h3>
                </div>
                <div class="Rectangl-4 position-relative">
                    <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                        <div class="d-flex align-items-center justify-content-end mt-md-0 mt-3 flex-wrap mb-4">
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-excellent ms-1"></div>
                                <span class="fs-5 fw-medium excellent">عالی</span>
                            </div>
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-good ms-1"></div>
                                <span class="fs-5 fw-medium good">خوب</span>
                            </div>
                            <div class="d-flex align-items-center ms-md-3 ms-2">
                                <div class="dot-fair ms-1"></div>
                                <span class="fs-5 fw-medium fair">متوسط</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <div class="dot-poor ms-1"></div>
                                <span class="fs-5 fw-medium poor">ضعیف</span>
                            </div>
                        </div>
                        <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                            <div class="d-flex flex-column w-100">
                                @foreach ($job->skills as $skill)
                                    <div class="d-flex mb-3 flex-md-row flex-column align-items-md-center">
                                        <h4 class="text fs-5 progress-title">{{ $skill->name }}</h4>
                                        <div class="progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                            <div class="progress-bar
                                                @if($skill->pivot->skill_level == 'عالی')
                                                    bg-excellent
                                                @elseif($skill->pivot->skill_level == 'خوب')
                                                    bg-good
                                                @elseif($skill->pivot->skill_level == 'متوسط')
                                                    bg-fair
                                                @elseif($skill->pivot->skill_level == 'ضعیف')
                                                    bg-poor
                                                @endif
                                            " style="width:
                                                @if($skill->pivot->skill_level == 'عالی')
                                                    100%
                                                @elseif($skill->pivot->skill_level == 'خوب')
                                                    75%
                                                @elseif($skill->pivot->skill_level == 'متوسط')
                                                    50%
                                                @elseif($skill->pivot->skill_level == 'ضعیف')
                                                    25%
                                                @endif
                                            "></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endif



        @if ($job->languages && count($job->languages) > 0)
            <div class="border-dash-purple2 mt-10 p-3">
                <div class="d-flex title-box2">
                    <img src="{{ asset('assets/frontend/img/job-single/Group 2444.svg') }}" class="w-small-j h-auto ms-2">
                    <h3 class="title-job2 fs-4">زبان های خارجی</h3>
                </div>

                <div class="Rectangl-4 position-relative">
                    <div class="p-md-5 p-3 box-shadow-c rounded-4 position-relative z-3 bg-white">
                        <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-auto align-items-center row-gap-3 fix-width-wrraper">
                            @foreach ($job->languages as $lang)
                                <div class="col fix-width-skill">
                                    <div class="d-flex ujstify-content-center flex-column align-items-center p-3
                                        @if($lang->pivot->level == 'عالی')
                                            excellent-box-border
                                        @elseif($lang->pivot->level == 'خوب')
                                            good-box-border
                                        @elseif($lang->pivot->level == 'متوسط')
                                            fair-box-border
                                        @elseif($lang->pivot->level == 'ضعیف')
                                            poor-box-border
                                        @endif
                                    ">
                                    <h4 class="text h5">{{ $lang->name }}</h4>
                                    <div class="d-flex justify-content-center
                                        @if($lang->pivot->level == 'عالی')
                                            excellent-tag
                                        @elseif($lang->pivot->level == 'خوب')
                                            good-tag
                                        @elseif($lang->pivot->level == 'متوسط')
                                            fair-tag
                                        @elseif($lang->pivot->level == 'ضعیف')
                                            poor-tag
                                        @endif
                                    ">
                                            <span class="fs-14 fw-medium text-white align-self-center">{{ $lang->pivot->level }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>
@endsection


