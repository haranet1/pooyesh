@extends('admin.layouts.master')

@section('title' , 'تعریف رویداد صدرا')

@section('main')
    <style>
        .product-variants {
            margin: 0px 0 10px;
            color: #6f6f6f;
            font-size: 13px;
            line-height: 1.692;
        }
        ul.js-product-variants {

            display: inline-block;
            vertical-align: middle;
        }
        ul.js-product-variants li {
            margin: 0 8px 0 0;
            display: inline-block;
        }

        .product-variant>span {
            font-size: inherit;
            color: inherit;
            padding-left: 15px;
            margin-top: 3px;
            float: right;
        }

        .product-variants {
            margin-right: -8px;
            list-style: none;
            padding: 0;
            display: inline-block;
            margin-bottom: 0;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }



        .ui-variant input[type=checkbox] {
            visibility: hidden;
            position: absolute;
        }

        .ui-variant--check {
            cursor: pointer;
            border: 1px solid transparent;
            border-radius: 10px;
            color: #6f6f6f;
            padding: 3px 10px;
            font-size: 13px;
            font-size: .929rem;
            line-height: 1.692;
            display: block;
            -webkit-box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
            box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
        }

        .ui-variant--color .ui-variant--check {
            padding-right: 37px;
        }

        input[type=checkbox]:checked+.ui-variant--check {
            border-color: #2c90f6;
            background-color: #2c91f8;
            color: white;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .ui-variant--color .ui-variant-shape {
            width: 18px;
            height: 18px;
            position: absolute;
            right: 8px;
            top: 8px;
            border-radius: 50%;
            content: "";
            cursor: pointer;
        }
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 2px solid rgba(0, 0, 0, 0.553);
        }


    </style>


    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> تعریف رویداد جدید</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">تعریف رویداد جدید</h4>
                </div>

                <div class="card-body">
                    <form class="form-horizontal" id="formElement" action="{{route('store.event')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">
                                <span>عنوان</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="title" type="text" class="form-control" >
                                @error('title') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>پوستر</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="poster" type="file" class="form-control" >
                                @error('poster') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>بنر</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="banner" type="file" class="form-control" >
                                @error('banner') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">نقشه</label>
                            <div class="col-md-9">
                                <input name="map" type="file" class="form-control" >
                                @error('map') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>توضیحات</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="" cols="30" rows="5"></textarea>
                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <hr>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>برای شرکت در رویداد : </span>
                            </label>
                            <label class="col-md-9 form-label" >
                                <input  name="is_free" type="checkbox" id="is_free" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span id="free-span" class="custom-switch-description">از شرکت کنندگان هزینه گرفته شود</span>
                            </label>
                            @error('is_free') <span class="text-danger">{{$message}}</span> @enderror
                        </div>

                        <div class="row mb-4">

                            <label class="col-md-3 form-label" for="example-email">
                                <span>امکان خرید چند بلیط : </span>
                            </label>
                            <label class="col-md-9 form-label" >
                                <input type="checkbox" id="can_multy_buy" name="can_multy_buy" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span id="can_multy_buy_span" class="custom-switch-description">وجود ندارد</span>
                            </label>
                            @error('can_multy_buy') <small class="text-danger">{{$message}}</small> @enderror
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>نوع کاربری شرکت کنندگان : </span>
                            </label>
                            <div class="col-md-9">
                                <div class="product-variants">
                                    <ul class="js-product-variants">
                                        <li class="ui-variant">
                                            <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-building"></i>
                                                            </span>
                                                <input type="checkbox" id="company_acceptance" name="company_acceptance" class="variant-selector">
                                                <span class="ui-variant--check">شرکت</span>
                                            </label>
                                        </li>
                                        <li class="ui-variant">
                                            <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                <input type="checkbox" id="student_acceptance" name="student_acceptance" class="variant-selector">
                                                <span class="ui-variant--check">دانشجو</span>
                                            </label>
                                        </li>
                                        <li class="ui-variant">
                                            <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                <input type="checkbox" id="applicant_acceptance" name="applicant_acceptance" class="variant-selector">
                                                <span class="ui-variant--check">کارجو</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <small class="text-danger " id="acceptanceErr"></small>
                        </div>


                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                                <button type="button" id="submitBtn" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $('#is_free').on('change' , function () {
            
            if($('#is_free').is(':checked')){
                $('#free-span').text('از شرکت کنندگان هزینه گرفته نشود')
                $('#free-span').removeClass('text-success')
                $('#free-span').addClass('text-danger')
            }else{
                $('#free-span').text('از شرکت کنندگان هزینه گرفته شود')
                $('#free-span').removeClass('text-danger')
                $('#free-span').addClass('text-success')
            }

        })

        $('#can_multy_buy').on('change' , function () {

            if($('#can_multy_buy').is(':checked')){
                $('#can_multy_buy_span').text('وجود دارد')
            }else{
                $('#can_multy_buy_span').text('وجود ندارد')
            }

        });

        $('#submitBtn').on('click' , function () {

            if($('#company_acceptance').is(':checked') || $('#student_acceptance').is(':checked') || $('#applicant_acceptance').is(':checked')){
                $('#formElement').submit();
            }else{
                $('#acceptanceErr').text('یکی از نوع های کاربری شرکت کنندگان را انتخاب کنید')
            }

        })
    </script>
@endsection
