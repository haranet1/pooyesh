@extends($panel)

@section('title' , 'لیست رویدادها')

@section('main')

    
    <div class="row row-cards mt-5">
        <div class="col-12">

            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="row">
                @if ($events->count() > 0)
                    @foreach ($events as $event)
                        @php
                            $hasCapacity = true;
                            if ($event->infos->count() == 0) {
                                $hasCapacity = false;
                            }
                        @endphp
                        <div class="col-12 col-md-4">
                            <div class="card ">
                                <div class="d-flex card-header justify-content-center overflow-hidden" style="min-height: 300px;max-height: 380px">
                                    <img class="" src="{{ asset($event->poster->path) }}" style="min-height: 300px;max-height: 380px"  alt="">
                                </div>
                                <div class="card-body pb-2">
                                    <div class="d-flex justify-content-between">
                                        <p class="h4 m-0">
                                            رویداد {{ $event->title }}
                                        </p>
                                        @if ($event->is_free == true)
                                            <small class="bg-success text-white px-2 rounded-1 m-0">رایگان</small>
                                        @endif
                                    </div>
                                    <div class="d-flex mt-3">
                                        <p>
                                            {{ Str::limit($event->description, 100, '...') }}
                                        </p>
                                    </div>
                                    <div class="d-flex mt-3">
                                        <span class="bg-info text-white px-2 rounded-1 m-0">
                                            ویژه
                                            @if ($event->company_acceptance == true)
                                                شرکت ها
                                            @endif

                                            @if ($event->company_acceptance == true && $event->student_acceptance == true && $event->applicant_acceptance == false)
                                                و
                                            @endif

                                            @if ($event->company_acceptance == true && $event->student_acceptance == true && $event->applicant_acceptance == true)
                                                ،
                                            @endif

                                            @if ($event->student_acceptance == true)
                                                دانشجویان
                                            @endif

                                            @if ($event->student_acceptance == true && $event->applicant_acceptance == true)
                                                و
                                            @endif

                                            @if ($event->applicant_acceptance == true)
                                                کارجویان
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    @if ($event->is_active == false)
                                        <div class="btn btn-gray pe-none">رویداد غیر فعال است</div>
                                    @elseif (authUserCanRegisterInEvent($event) == false)
                                        <div class="btn btn-gray pe-none">امکان ثبت نام شما وجود ندارد</div>
                                    @elseif ($hasCapacity == false)  
                                        <div class="btn btn-dark">تکمیل ظرفیت</div>
                                    @else
                                        {{-- <form action="{{ route('lets.register.event') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="event_id" value="{{ $event->id }}">
                                            <button type="submit" class="btn btn-primary">ثبت نام</button>
                                        </form> --}}
                                        <a href="{{ route('lets.register.event' , $event->id) }}" class="btn btn-primary">ثبت نام</a>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="alert alert-warning text-center">
                        {{__('public.no_info')}}
                    </div>
                @endif
            </div>

        </div>
    </div>

@endsection