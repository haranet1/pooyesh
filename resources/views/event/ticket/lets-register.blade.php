@extends($panel)

@section('title' , 'ثبت نام در رویداد')

@section('css')
    <link href="{{ asset('assets/panel/css/style-css/register-event.css') }}" rel="stylesheet" />
@endsection

@section('main')

    
    <div class="row row-cards mt-5">
        
        <div class="col-12 col-md-4 ">
            <div class="card "style="position: sticky !important; top: 100px">
                <div class="d-flex card-header justify-content-center overflow-hidden" style="min-height: 260px;max-height: 280px">
                    <img class="" src="{{ asset($event->poster->path) }}" style="min-height: 300px;max-height: 380px"  alt="">
                </div>
                <div class="card-body d-none" id="priceDiv">
                    <form action="{{ route('register.event') }}" method="post" id="registerEventForm">
                        @csrf
                        <input type="hidden" name="event_id" value="{{ $event->id }}">
                        <input type="hidden" name="info_id" id="info_id">
                        <input type="hidden" name="discount_id" id="discount_id">
                        <div class="row mb-3">
                            <input type="hidden" id="priceInput">
                            <h4 class="">
                                هزینه بلیط : <span id="priceElement"></span> تومان
                            </h4>
                        </div>
                        <div class="row align-items-center mb-3">
                            @if ($event->can_multy_buy == true)
                                <div class="col-6 col-md-3 p-0">
                                    <label for="multyBuy" class="form-lable">تعداد :</label>
                                </div>
                                <select name="multyBuy" id="multyBuy" onchange="getSubTotal(this)" class="form-select form-control col-6 col-md-9 ">
                                    <option value="1">1</option>
                                </select>
                            @endif
                        </div>
                        <div class="row align-items-center mb-4">
                            <div class="col-4 col-md-3 p-0">
                                <label for="discount" class="form-lable">کد تخفیف :</label>
                            </div>
                            <div class="col-4 col-md-7 pe-2">
                                <input type="text" id="discount_code" class="form-control">
                                <span class="text-sm text-danger" id="discount_code_err"></span>
                            </div>
                            <div class="col-4 col-md-2 p-0">
                                <button type="button" class="btn btn-success" onclick="applyDiscount()">ثبت</button>
                            </div>
                        </div>
                        <div class="row mb-4 align-items-center">
                            <div class="col-4 col-md-3 p-0">
                                <label for="discount" class="form-lable">درگاه پرداخت :</label>
                            </div>
                            <select name="gateway" id="gateway" class="col-4 col-md-9 form-select form-control">
                                <option value="iaun"> درگاه دانشگاه آزاد </option>
                            </select>
                        </div>
                        <div class="d-none mb-3" id="discount_price_div">
                            <h4 class="">
                                تخفیف : <span id="discount_price"></span> تومان
                            </h4>
                        </div>
                        <div class="row mb-3">
                            <input type="hidden" id="totalPriceInput">
                            <h4 class="">
                                هزینه قابل پرداخت : <span id="totalPrice"></span> تومان
                            </h4>
                        </div>
                        <div class="row justify-content-center">
                            <button type="button" id="pay-btn" class="btn btn-success w-25">خرید</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-8 px-0">

            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            
            <div class="card mb-0">
                <div class="card-body">
                    <div class="d-flex w-100 justify-content-between">
                        <div class="card-title">
                            {{ $event->title }}
                        </div>
                        <div class="">
                            <span class="bg-info text-white px-2 rounded-1 m-0">
                                ویژه
                                @if ($event->company_acceptance == true)
                                    شرکت ها
                                @endif
    
                                @if ($event->company_acceptance == true && $event->student_acceptance == true && $event->applicant_acceptance == false)
                                    و
                                @endif
    
                                @if ($event->company_acceptance == true && $event->student_acceptance == true && $event->applicant_acceptance == true)
                                    ،
                                @endif
    
                                @if ($event->student_acceptance == true)
                                    دانشجویان
                                @endif
    
                                @if ($event->student_acceptance == true && $event->applicant_acceptance == true)
                                    و
                                @endif
    
                                @if ($event->applicant_acceptance == true)
                                    کارجویان
                                @endif
                            </span>
                            @if ($event->is_free == true)
                                <span class="bg-success text-white px-2 rounded-1 m-0">رایگان</span>
                            @endif
                        </div>
                    </div>
                    <p>
                        {{ $event->description }}
                    </p>
                </div>
            </div>

            {{-- <div class="row justify-content-center p-4">
                <div class="alert alert-info alert-dismissible fade-show">
                    <div class="d-flex align-items-center">
                        <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
                        <div class="flex-grow-1">
                            <p class="f-16" >
                                فرم زیر را با دقت کامل کنید تا ثبت نام شما تکمیل گردد.
                            </p>
                        </div>
                    </div>
                </div>
            </div> --}}

            <div class="row mb-4 justify-content-center px-4">

                <div class="col-12" id="div1">
                    <div class="d-flex flex-column px-0">

                        <input type="hidden" id="event" value="{{ $event->id }}">

                        <label class="form-label" for="plan">
                            <span class="fs-5 text-color-dark">انتخاب سانس و پنل:</span>
                            <span class="text-danger">
                                <small>(اجباری)</small>
                            </span>
                        </label>

                        @foreach($event->infos as $info)
                            <div class="mt-4 ticket-box d-flex 
                                @if (isset($info->cantRegisterThisInfo) || $info->sold_out == true)
                                    disabled-ticket
                                @endif
                                "
                                @if (! isset($info->cantRegisterThisInfo) && $info->sold_out != true)
                                    onclick="activeTicket(this , {{ $info->id }})"
                                @endif
                            >
                                <input type="hidden" name="postId" value="{{ $info->id }}"/>

                                <div class="d-flex flex-column align-items-center justify-content-center date">
                                    <span class="date-d text-color-dark">{{ verta($info->sans->date)->format('%d') }}</span>
                                    <span class="date-m text-color-dark">{{ verta($info->sans->date)->format('%B') }}</span>
                                </div>

                                <div class="ticket-info">
                                    <h3 class="fs-18 text-color-dark">سانس {{ $info->sans->title }} | پلن {{$info->plan->title}}</h3>
                                    <div class="mt-2">
                                        <span class="text-color-dark">{{ verta($info->sans->date)->formatWord('l') }} | ساعت {{ $info->sans->time }} </span>
                                    </div>

                                    <div class="mt-2">
                                        {{-- <span>توضیحات:</span> --}}
                                        <span class="text-color-dark">{{ $info->plan->description }} </span>
                                    </div>

                                    <div class="mt-3">
                                        @if ($info->sold_out != true)
                                            <span class="text-danger fs-7">ظرفیت باقی مانده:</span>
                                            <span class="text-color-dark">{{$info->plan->count - $info->plan_counter }} نفر</span>
                                        @else
                                            <span class="text-danger fs-7"> تکمیل ظرفیت </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                
            </div>
        </div>
    </div>

<input type="hidden" id="getEventInfoUrl" value="{{ route('get.info.event') }}">
<input type="hidden" id="applyDiscountUrl" value="{{ route('apply.discount.event') }}">
@endsection

@section('script')
    <script src="{{ asset('assets/panel/js/emp-event.js') }}"></script>

    <script>
        $('#pay-btn').on('click' , function () {


            Swal.fire({
                icon : 'warning',
                title : 'توجه !',
                text : 'پس از انجام عملیات پرداخت حتما منتظر بمانید تا به صفحه رسید هدایت شوید و به هیچ عنوان صفحات را refresh نکنید یا از صفحات پیشِ رو خارج نشوید.' ,
                confirmButtonText : 'پرداخت',
            }).then((result) => {
                if(result.isConfirmed){
                    $('#registerEventForm').submit()
                }
            });

        });
    </script>
@endsection