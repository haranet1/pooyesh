@extends($panel)

@section('title','رویدادها')

@section('main')
    <style>
        .item-span{
            font-size: 1rem;
        }
    </style>

    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست رویدادها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-lg-12 col-xl-6">
            <div class="input-group mb-5">
            </div>

            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            
            <div class="card">
                @if ($payment->receipt && $payment->tracking_code)
                    <div class="card-header d-flex justify-content-center">
                        <h3 class="text-success mb-0">{{$payment->status}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-8">

                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="text-center">
                                        <span> خرید بلیط رویداد {{ $payment->order->info->event->title }} </span>
                                    </div>
                                </div>

                                {{-- <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="text-center">
                                        <span> {{ $payment->order->count }} بلیط به قیمت {{ $payment->order->info->plan->price }} تومان و با تخفیف {{ $payment->order->discount_price }} تومان</span>
                                    </div>
                                </div> --}}

                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">هزینه :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->price}}</span>
                                        <span class="item-span">تومان</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">تاریخ / زمان :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span" dir="ltr">{{ verta($payment->payment_date_time)->formatJalaliDatetime() }}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">شماره پیگیری :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->tracking_code}}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">رسید پرداخت :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->receipt}}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center border border-info mb-3" style="border-radius: 10px !important; box-shadow: 2px 2px lightskyblue">
                                    <div class="col-6 text-center">
                                        <span class="item-span">بانک صادر کننده :</span>
                                    </div>
                                    <div class="col-6 text-center">
                                        <span class="item-span">{{$payment->payer_bank}}</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <a href="{{route('print.ticket.event', $payment->id)}}" target="_blank" class="btn btn-success item-span"> چاپ بلیط </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                
            </div>
            <div class="mb-5">
                {{-- {{$events->links('pagination.panel')}} --}}
            </div>
        </div>
    </div>
    
@endsection
@section('script')
    <script src="{{asset('assets/panel/js/company.js')}}"></script>
    

@endsection
