@extends($panel)

@section('title' , 'بلیط های ثبت شده')

@section('main')

    <div class="row row-cards mt-5">
        <div class="col-12">
            
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="card">
                <div class="row card-header justify-content-between">
                    <div class="col-12 col-md-3 mb-2 mb-md-0">
                        <h3 class="card-title">لیست بلیط های ثبت شده</h3>
                    </div>
                    <div class="col-12 col-md-3 text-end">
                        <a href="{{ route('list.event') }}" class="btn btn-primary">شرکت در رویداد</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive table-lg">

                        @if($eventTickets->count() > 0)

                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center bg-info text-white">رویداد</th>
                                        <th class="text-center bg-info text-white">سانس</th>
                                        <th class="text-center bg-info text-white">پلن</th>
                                        <th class="text-center bg-info text-white">قیمت و تعداد</th>
                                        <th class="text-center bg-info text-white">تخفیف</th>
                                        <th class="text-center bg-info text-white">هزینه قابل پرداخت</th>
                                        <th class="text-center bg-info text-white">وضعیت</th>
                                        <th class="text-center bg-info text-white">امکانات</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    @foreach ($eventTickets as $eTicket)
                                        
                                        <tr>

                                            <td class="text-nowrap align-middle">{{ Str::limit($eTicket->event->title,'20','...') }}</td>

                                            <td class="text-nowrap align-middle">
                                                <span> تاریخ : {{ verta($eTicket->info->sans->date)->formatJalaliDate() }}</span>
                                                <span> ساعت : {{ $eTicket->info->sans->time }}</span>
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                {{ $eTicket->info->plan->title }}
                                            </td>

                                            <td class="text-nowrap align-middle text-center">
                                                <span> قیمت هر بلیط : {{ $eTicket->info->plan->price }} تومان</span>
                                                <br>
                                                <span> تعداد : {{ $eTicket->count }}</span>
                                            </td>

                                            <td class="text-nowrap align-middle text-center">
                                                @if ($eTicket->has('discount'))
                                                    {{ $eTicket->discount_price }} تومان
                                                @else
                                                    ---
                                                @endif
                                            </td>

                                            <td class="text-nowrap align-middle text-center">
                                                {{ $eTicket->payable_price }} تومان
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                
                                            </td>

                                            <td class="text-nowrap align-middle text-center">
                                                @if (! is_null($eTicket->receipt))

                                                @else
                                                    <form class="row" action="{{ route('pay.after.register.event') }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="event_ticket_id" value="{{ $eTicket->id }}">
                                                        <select name="gateway" id="gateway" class="form-select form-control col-12 col-md-8 mx-1">
                                                            <option value="iaun"> درگاه دانشگاه آزاد </option>
                                                        </select>
                                                        <button type="submit" class="btn btn-success col-12 col-md-3 mx-1"> پرداخت </button>
                                                    </form>
                                                @endif

                                            </td>

                                        </tr>

                                    @endforeach

                                </tbody>
                            </table>

                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection