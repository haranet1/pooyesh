<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>چاپ بلیط</title>

    <style>
        @font-face {
            font-family: "IranNastaliq";
            /* src: url("https://db.onlinewebfonts.com/t/3b019b0df3b0d6ea4d1b2f051132febb.eot");
            src: url("https://db.onlinewebfonts.com/t/3b019b0df3b0d6ea4d1b2f051132febb.eot?#iefix")format("embedded-opentype"), */
            url({{ asset('fonts/woff2/IranNastaliq.woff2') }})format("woff2"),
            url({{ asset('fonts/woff/IranNastaliq.woff') }})format("woff");
        }
        @font-face {
            font-family: 'Sans';
            font-style: normal;
            font-weight: 400;
            src:  url({{asset('fonts/woff/IRANSansWeb.woff')}}) format('woff');
        }
        @font-face {
            font-family: 'source';
            font-style: normal;
            font-weight: 400;
            src: url({{ asset('assets/panel/fonts/SourceCodePro-Regular.ttf') }});
        }
        .ticket-box{
            /* border: 2px solid #cecbff!important; */
            background-color: #fff;
            position: relative;
            /* cursor: pointer; */
            border-radius: 10px;
            /* background-image: url('../../../public/assets/panel/images/img/register-event/Group\ 1173.svg'); */
            background-repeat: no-repeat;
            background-position: 5% 90%;
            background-size: 15%;
            display: flex;
            margin-top: 20px;
        }

        .date{
            width: 180px;
            border-left: 2px dashed #83c5be;
            background-color: #d8deff;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }
        .text-color-dark{
            color: #080808;
            font-weight: 600;
        }
        .date-d{
            font-size: 20px;
            font-weight: bold;
            color: #001219;
        }
        .date-m{
            font-size: 20px;
            font-weight: 600;
            color: #001219;
        }
        .ticket-info{
            width: 100%;
            padding-left: 15px;
            padding-right: 30px;
            padding-top: 10px;
            padding-bottom: 15px;
            position: relative;
            background-color: #e9ecef;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
        }
        /* .ticket-info:before {
            content: "";
            display: block;
            width: 30px;
            height: 30px;
            background-color: #fff;
            position: absolute;
            top: -15px;
            right: -15px;
            z-index: 1;
            border-radius: 50%;
        }
        .ticket-info:after {
            content: "";
            display: block;
            width: 30px;
            height: 30px;
            background-color: #fff;
            position: absolute;
            bottom: -15px;
            right: -15px;
            z-index: 1;
            border-radius: 50%;
        } */
        .fs-18{
            font-size: 18px;
        }
        .mt-3{
            margin-top: 15px; 
        }
        .date-time{
            margin-top: 15px;
            display: flex;
            flex-direction: column;
            gap: 10px; 
        }
        .fs-7{
            font-size: 16px;
        }
        .ticket-wrapper{
            /* padding: 20px; */
            font-family: 'Sans';

        }
        .body {
            background: white;
            margin: 0 auto;
            /* margin-bottom: 0.5cm; */
            
        }
        .text-danger{
            color: rgb(187, 2, 2);
            font-weight: 600;
        }
        .hidden-print {
            box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
            width: 100%;
            height: 100%;
        }
        .A4 {
            width: 21cm;
            height: 29.5cm;
        }
        @media print {
            .hidden-print {
                box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0) !important;
            }
        }
        .image-ticket{
            position: absolute;
            z-index: 1;
            width: 188px;
            height: 203px;
            bottom: 0px;
            left: 0px;
            /* opacity: .6; */
            mix-blend-mode: multiply;
        }
    </style>
</head>
<body size="A4" dir="rtl">
    <div class="A4 body hidden-print">
        @foreach ($payment->order->codes as $code)
            <div class="ticket-wrapper">
                <div class="ticket-box">
                    {{-- <img src="{{ asset('assets/panel/images/img/register-event/Group 1173.svg') }}" class="image-ticket"> --}}
                    
                    <input type="hidden" name="postId" value=""/>
                    
                    <div class="date">
                        <div class="">
                            <span class="date-d text-color-dark">{{ verta($payment->order->info->sans->date)->format('%d') }}</span>
                            <span class="date-m text-color-dark">{{ verta($payment->order->info->sans->date)->format('%B') }}</span>
                        </div>
                        <div class="" style="mix-blend-mode: multiply">
                            {!! $code->qr !!}
                        </div>
                        <span style="font-family: source">{{ $code->code }}</span>
                    </div>

                    <div class="ticket-info" style="display: flex;justify-content: space-between">
                        <div class="" style="">
                            <h3 class="fs-18 text-color-dark" style="">
                                {{ $payment->order->event->title }}
                                
                            </h3>
    
                            @if ($payment->order->info->sans->title)
                                <span class="text-color-dark"> سانس {{ $payment->order->info->sans->title }}</span>
                                
                            @endif
                            <span class="text-color-dark"> پلن {{ $payment->order->info->plan->title }}</span>
    
                            <div class="date-time">
                                <div class="text-color-dark"> زمان برگزاری : {{ verta($payment->order->info->sans->date)->formatJalaliDate() }} ساعت : {{ $payment->order->info->sans->time }} </div>
                                <div class="text-color-dark">قیمت: {{ $payment->order->info->plan->price }} تومان</div>
                            </div>
                        </div>

                        <div class="" style="display: flex; align-items: center; margin-left: 2rem">
                            <div class="" style="text-align: center">
                                <div class="" style="mix-blend-mode: multiply">
                                    {!! $code->qr !!}
                                </div>
                                <div style="font-family: source ">{{ $code->code }}</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <script>
        window.print();
   </script>
</body>
</html>