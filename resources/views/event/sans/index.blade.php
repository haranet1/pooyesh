@extends('admin.layouts.master')

@section('title' , 'لیست سانس ها')

@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">

            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="card">
                <div class="row card-header justify-content-between">
                    <div class="col-12 col-md-3 mb-2 mb-md-0">
                        <h3 class="card-title">لیست سانس های رویداد {{ $event->title }}</h3>
                    </div>
                    <div class="col-12 col-md-3 text-end">
                        <a href="{{ route('index.event') }}" class="btn btn-info">لیست رویداد ها</a>
                        <a href="{{ route('create.sans.event' , $event->id) }}" class="btn btn-primary">افزودن سانس</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive table-lg">
                        @if ($eventSans->count() > 0)

                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center bg-info text-white">عنوان</th>
                                    <th class="text-center bg-info text-white">توضیحات</th>
                                    <th class="text-center bg-info text-white">تاریخ ثبت نام</th>
                                    <th class="text-center bg-info text-white">روز</th>
                                    <th class="text-center bg-info text-white">زمان</th>
                                    <th class="text-center bg-info text-white">عملکرد ها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($eventSans as $sans)
                                    <tr>

                                        @if ($sans->title)
                                            <td class="align-middle text-center">{{ $sans->title }}</td>
                                        @else
                                            <td class="align-middle text-center"> --- </td>
                                        @endif

                                        @if ($sans->description)
                                            <td class="text-nowrap align-middle">{{ Str::limit($sans->description,'20','...') }}</td>
                                        @else
                                            <td class="text-nowrap align-middle"> --- </td>
                                        @endif

                                        @if ($sans->start_register_at && $sans->end_register_at)
                                            <td class="text-nowrap align-middle">
                                                {{ verta($sans->start_register_at)->formatJalaliDate() }} الی {{ verta($sans->end_register_at)->formatJalaliDate() }}
                                            </td>   
                                        @else
                                            <td class="text-nowrap align-middle"> --- </td>
                                        @endif
                                        

                                        <td class="text-nowrap align-middle">{{ verta($sans->date)->formatJalaliDate() }}</td>

                                        <td class="text-nowrap align-middle">{{ $sans->time }}</td>

                                        

                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a class="btn btn-sm btn-primary" href="{{route('edit.sans.event', $sans->id)}}">ویرایش</a>
                                                <a class="btn btn-sm btn-danger delete" onclick="deleteEventSans({{$sans->id}})" href="javascript:void(0)">حذف</a>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$eventSans->links('pagination.panel')}}
            </div>
        </div>

    </div>

<input type="hidden" id="deleteSansUrl" value="{{ route('delete.sans.event') }}">
@endsection

@section('script')
    <script src="{{ asset('assets/panel/js/emp-event.js') }}"></script>
@endsection