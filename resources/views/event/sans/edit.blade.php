@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('title' , 'ویرایش سانس')

@section('main')

    <style>
        .mdtp__wrapper {
            direction: rtl;
        }
        .mdtp__time_holder {
            direction: rtl;
            text-align: right;
        }
        div.title {
            display: none;
        }
    </style>


    <div class="row row-cards justify-content-center mt-5">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ویرایش سانس رویداد {{ $sans->event->title }}</h4>
                </div>

                <div class="card-body">
                    <form class="form-horizontal" action="{{route('update.sans.event')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="event_id" value="{{ $sans->event->id }}">
                        <input type="hidden" name="sans_id" value="{{ $sans->id }}">
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">
                                <span>عنوان</span>
                            </label>
                            <div class="col-md-9">
                                <input name="title" type="text" class="form-control" 
                                @if ($sans->title)
                                    value="{{ $sans->title }}"
                                @elseif (old('title'))
                                    value="{{ old('title') }}"
                                @endif
                                >
                                @error('title') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>شروع ثبت نام</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="start_register_at" type="text" id="start_register_at" class="form-control" 
                                @if($sans->start_register_at)
                                    value="{{ verta($sans->start_register_at)->formatJalaliDate() }}"
                                @elseif (old('start_register_at'))
                                    value="{{ old('start_register_at') }}"
                                @endif
                                >
                                @error('start_register_at') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>پایان ثبت نام</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="end_register_at" type="text" id="end_register_at" class="form-control" 
                                @if($sans->end_register_at)
                                    value="{{ verta($sans->end_register_at)->formatJalaliDate() }}"
                                @elseif (old('end_register_at'))
                                    value="{{ old('end_register_at') }}"
                                @endif
                                >
                                @error('end_register_at') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>تاریخ | روز</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="date" type="text" id="date" class="form-control"
                                @if($sans->date)
                                    value="{{ verta($sans->date)->formatJalaliDate() }}"
                                @elseif (old('date'))
                                    value="{{ old('date') }}"
                                @endif
                                >
                            </div>
                        </div>

                        <div class="row mb-4 align-items-center">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>زمان | ساعت</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="time" type="text" id="time" class="form-control"
                                @if ($sans->time)
                                    value="{{ $sans->time }}"
                                @elseif (old('time'))
                                    value="{{ old('time') }}"
                                @endif
                                >
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>توضیحات</span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="description" cols="30" rows="5">
                                    @if ($sans->description)
                                    {{$sans->description}}
                                    @elseif(old('description'))
                                    {{old('description')}}
                                    @endif
                                </textarea>
                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                                <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('script')+
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>

        $('#start_register_at').persianDatepicker({
            altField: '#start_register_at',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });

        $('#end_register_at').persianDatepicker({
            altField: '#end_register_at',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });

        $('#date').persianDatepicker({
            altField: '#date',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
        
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            flatpickr("#time", {
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                time_24hr: true,
                locale: {
                    firstDayOfWeek: 1,
                    weekdays: {
                        shorthand: ["ی", "د", "س", "چ", "پ", "ج", "ش"],
                        longhand: [
                            "یک‌شنبه",
                            "دوشنبه",
                            "سه‌شنبه",
                            "چهارشنبه",
                            "پنج‌شنبه",
                            "جمعه",
                            "شنبه"
                        ]
                    },
                    months: {
                        shorthand: [
                            "ژانویه",
                            "فوریه",
                            "مارس",
                            "آوریل",
                            "می",
                            "ژوئن",
                            "ژوئیه",
                            "اوت",
                            "سپتامبر",
                            "اکتبر",
                            "نوامبر",
                            "دسامبر"
                        ],
                        longhand: [
                            "ژانویه",
                            "فوریه",
                            "مارس",
                            "آوریل",
                            "می",
                            "ژوئن",
                            "ژوئیه",
                            "اوت",
                            "سپتامبر",
                            "اکتبر",
                            "نوامبر",
                            "دسامبر"
                        ]
                    }
                }
            });
        });
    </script>
@endsection


