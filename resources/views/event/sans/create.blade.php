@extends('admin.layouts.master')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('title' , 'تعریف سانس')

@section('main')

    <style>
        .mdtp__wrapper {
            direction: rtl;
        }
        .mdtp__time_holder {
            direction: rtl;
            text-align: right;
        }
        div.title {
            display: none;
        }
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 2px solid rgba(0, 0, 0, 0.553);
        }
    </style>


    <div class="row row-cards justify-content-center mt-5">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ثبت سانس رویداد {{ $event->title }}</h4>
                </div>

                <div class="card-body">
                    <form class="form-horizontal" id="sansForm" action="{{route('store.sans.event')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="event_id" value="{{ $event->id }}">
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">
                                <span>عنوان</span>
                            </label>
                            <div class="col-md-9">
                                <input name="title" type="text" class="form-control" 
                                @if (old('title'))
                                    value="{{ old('title') }}"
                                @endif
                                >
                                @error('title') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>شروع ثبت نام</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="start_register_at" type="text" id="start_register_at" class="form-control" 
                                @if (old('start_register_at'))
                                    value="{{ old('start_register_at') }}"
                                @endif
                                >
                                @error('start_register_at') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>پایان ثبت نام</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="end_register_at" type="text" id="end_register_at" class="form-control" 
                                @if (old('end_register_at'))
                                    value="{{ old('end_register_at') }}"
                                @endif
                                >
                                @error('end_register_at') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>تاریخ | روز</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="date" type="text" id="date" class="form-control" 
                                @if (old('date'))
                                    value="{{ old('date') }}"
                                @endif
                                >
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>زمان | ساعت</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="time" type="text" id="time" class="form-control" 
                                @if (old('time'))
                                    value="{{ old('time') }}"
                                @endif
                                >
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>توضیحات</span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="description" cols="30" rows="5">@if(old('description')){{old('description')}}@endif</textarea>
                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <hr>

                        <div class="row mb-4" id="selectPlanForm">
                            <label class="col-md-2 form-label" for="example-email">
                                <span>انتخاب پلن</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            
                            @if ($plans->count() > 0)
                                <div class="col-md-6 mb-2">
                                    <select id="plan" class="form-select form-control">
                                        @foreach ($plans as $plan)
                                            <option value="{{ $plan->id }}">{{ $plan->title }} | ظرفیت {{ $plan->count }} نفر</option>
                                        @endforeach
                                    </select>
                                    @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            @endif
                            <div class="col-md-4 ">
                                <div class="d-flex w-100">
                                    @if ($plans->count() > 0)
                                        <button type="button" id="selectThePlan" class="btn btn-success mx-1">انتخاب</button>
                                    @endif
                                    <button type="button" id="createNewPlanBtn" class="btn btn-info mx-1">ساخت پلن جدید</button>
                                </div>
                            </div>
                        </div>

                        

                        <div class="d-none" id="planFormDiv">
                                <input type="hidden" id="plan_id" value="">
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label">
                                        <span>ساخت پلن جدید</span>
                                    </label>
                                </div>
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label">
                                        <span>عنوان</span>
                                        <span class="text-danger">
                                            <small>(اجباری)</small>
                                        </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input id="planTitle" type="text" class="form-control" >
                                        <span id="planTitleErr" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">
                                        <span>پوستر</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input id="planPhoto" type="file" class="form-control" >
                                        <span id="planPhotoErr" class="text-danger"></span>
                                    </div>
                                </div>
        
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">
                                        <span>قیمت</span>
                                        <span class="text-danger">
                                            <small>(اجباری)</small>
                                        </span>
                                    </label>
                                    <div class="col-md-9">
                                        @if ($event->is_free == true)
                                            <input type="hidden" value="0" id="planPrice">
                                            <input disabled type="text" value="0" class="form-control">
                                        @else
                                            <input id="planPrice" type="text" class="form-control">
                                        @endif
                                        
                                        
                                        @error('price') 
                                            <span id="planPriceErr" class="text-danger"></span>
                                        @else
                                            @if ($event->is_free == true)
                                                <span id="planPriceErr" class="text-info">به دلیل رایگان بودن رویداد، امکان وارد کردن قیمت وجود ندارد.</span>
                                            @else
                                                <span id="planPriceErr" class="text-info">به صورت عددی و به تومان وارد کنید .</span>
                                            @endif
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">
                                        <span>ظرفیت</span>
                                        <span class="text-danger">
                                            <small>(اجباری)</small>
                                        </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input id="planCount" type="text" min="0" class="form-control">
                                        <span id="planCountErr" class="text-danger"></span>
                                    </div>
                                </div>
        
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">
                                        <span>توضیحات</span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" id="planDescription" cols="30" rows="5"></textarea>
                                        <span id="planDescriptionErr" class="text-danger"></span>
                                    </div>
                                </div>

                                <div class=" row mb-3 justify-content-center ">
                                    <div class="col justify-content-center text-center ">
                                        <button type="button" id="registerPlan" class="btn btn-success btn-lg px-5">ثبت پنل</button>
                                    </div>
                                </div>
                        </div>

                        {{-- <div class="d-none" id="planFormDiv">
                            <form class="form-horizontal d-none" id="planForm" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="event_id" value="{{ $event->id }}">
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label">
                                        <span>ساخت پلن جدید</span>
                                    </label>
                                </div>
                                <div class=" row mb-4">
                                    <label class="col-md-3 form-label">
                                        <span>عنوان</span>
                                        <span class="text-danger">
                                            <small>(اجباری)</small>
                                        </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input name="title" type="text" class="form-control" 
                                        @if (old('title'))
                                            value="{{ old('title') }}"
                                        @endif
                                        >
                                        @error('title') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">
                                        <span>پوستر</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input name="photo" type="file" class="form-control" >
                                        @error('photo') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
        
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">
                                        <span>قیمت</span>
                                        <span class="text-danger">
                                            <small>(اجباری)</small>
                                        </span>
                                    </label>
                                    <div class="col-md-9">
                                        @if ($event->is_free == true)
                                            <input type="hidden" value="0" name="price">
                                            <input disabled type="text" value="0" class="form-control">
                                        @else
                                            <input name="price" type="text" class="form-control"
                                            @if (old('price'))
                                                value="{{ old('price') }}"
                                            @endif
                                            >
                                        @endif
                                        
                                        
                                        @error('price') 
                                            <span class="text-danger">{{$message}}</span>
                                        @else
                                            @if ($event->is_free == true)
                                                <span class="text-info">به دلیل رایگان بودن رویداد، امکان وارد کردن قیمت وجود ندارد.</span>
                                            @else
                                                <span class="text-info">به صورت عددی و به تومان وارد کنید .</span>
                                            @endif
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">
                                        <span>ظرفیت</span>
                                        <span class="text-danger">
                                            <small>(اجباری)</small>
                                        </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input name="count" type="text" min="0" class="form-control" 
                                        @if (old('count'))
                                            value="{{ old('count') }}"
                                        @endif
                                        >
                                        @error('count') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
        
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">
                                        <span>توضیحات</span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="description" id="description" cols="30" rows="5">@if(old('description')){{old('description')}}@endif</textarea>
                                        @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
        
                                <div class=" row mb-0 justify-content-center ">
                                    <div class="col justify-content-center text-center ">
                                        <button type="button" id="registerPlan" class="btn btn-success btn-lg px-5">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div> --}}

                        <div class="d-none mb-4 " >
                            <label class="col-md-3 form-label ps-0" for="example-email">
                                <span>پلن های انتخاب شده :</span>
                            </label>
                            <div class="" id="selectedPlan">
                            </div>
                        </div>

                        <input type="hidden" name="planCounter" id="planCounter" >
                    </form>
                </div>
                

            </div>
            <div class=" row justify-content-center mb-3">
                <div class="col justify-content-center text-center ">
                    <button type="button" id="registerSansBtn" class="btn btn-success btn-lg px-5">ثبت سانس</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')+
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>

        $('#start_register_at').persianDatepicker({
            altField: '#start_register_at',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });

        $('#end_register_at').persianDatepicker({
            altField: '#end_register_at',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });

        $('#date').persianDatepicker({
            altField: '#date',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
        
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            flatpickr("#time", {
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                time_24hr: true,
                locale: {
                    firstDayOfWeek: 1,
                    weekdays: {
                        shorthand: ["ی", "د", "س", "چ", "پ", "ج", "ش"],
                        longhand: [
                            "یک‌شنبه",
                            "دوشنبه",
                            "سه‌شنبه",
                            "چهارشنبه",
                            "پنج‌شنبه",
                            "جمعه",
                            "شنبه"
                        ]
                    },
                    months: {
                        shorthand: [
                            "ژانویه",
                            "فوریه",
                            "مارس",
                            "آوریل",
                            "می",
                            "ژوئن",
                            "ژوئیه",
                            "اوت",
                            "سپتامبر",
                            "اکتبر",
                            "نوامبر",
                            "دسامبر"
                        ],
                        longhand: [
                            "ژانویه",
                            "فوریه",
                            "مارس",
                            "آوریل",
                            "می",
                            "ژوئن",
                            "ژوئیه",
                            "اوت",
                            "سپتامبر",
                            "اکتبر",
                            "نوامبر",
                            "دسامبر"
                        ]
                    }
                }
            });
        });
    </script>

    <script>
        
        let planCounter = 1;

        $('#selectThePlan').on('click' , function () {
            
            if($('#plan').val()){
                $('#selectedPlan').parent().removeClass('d-none').addClass('d-flex');
                $('#selectedPlan').append(
                    ` <label for="" class="selectgroup-item">
                            <input type="hidden" name="planId`+ planCounter +`" value="`+ $('#plan').val() +`">
                            <span class="selectgroup-button">`+ $('#plan option:selected').text() +`</span>
                    </label>`
                )
                planCounter++;
            }

        })
        $('#createNewPlanBtn').on('click' , function () {
            $('#createNewPlanBtn').addClass('d-none')
            $('#planFormDiv').removeClass('d-none')
        })
        

        $('#registerPlan').on('click', function () {
            var planTitle = $('#planTitle').val();
            var planCount = $('#planCount').val();
            var planPrice = $('#planPrice').val();
            var planDescription = $('#planDescription').val();
            var file = $('#planPhoto')[0].files[0];

            if (planTitle.trim() === '' || planCount.trim() === '' || planPrice.trim() === '') {
                alert('لطفا تمامی فیلدهای اجباری را پر کنید.');
                return;
            }

            console.log(planTitle);

            if (file) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var fileContent = e.target.result;

                    $('#selectedPlan').parent().removeClass('d-none').addClass('d-flex');
                    $('#selectedPlan').append(
                        `<label class="selectgroup-item">
                            <input type="hidden" name="planId${planCounter}" value="">
                            <input type="hidden" name="planTitle${planCounter}" value="${planTitle}">
                            <input type="hidden" name="planPrice${planCounter}" value="${planPrice}">
                            <input type="hidden" name="planCount${planCounter}" value="${planCount}">
                            <input type="hidden" name="planDescription${planCounter}" value="${planDescription}">
                            <input type="hidden" id="planPhoto${planCounter}" name="planPhoto${planCounter}" value="${fileContent}">
                            <span class="selectgroup-button">${planTitle} | ظرفیت ${planCount} نفر</span>
                        </label>`
                    );

                    planCounter++;
                }
                reader.readAsDataURL(file);
            } else {
                $('#selectedPlan').parent().removeClass('d-none').addClass('d-flex');
                $('#selectedPlan').append(
                    `<label class="selectgroup-item">
                        <input type="hidden" name="planId${planCounter}" value="">
                        <input type="hidden" name="planTitle${planCounter}" value="${planTitle}">
                        <input type="hidden" name="planPrice${planCounter}" value="${planPrice}">
                        <input type="hidden" name="planCount${planCounter}" value="${planCount}">
                        <input type="hidden" name="planDescription${planCounter}" value="${planDescription}">
                        <input type="hidden" id="planPhoto${planCounter}" name="planPhoto${planCounter}" value="">
                        <span class="selectgroup-button">${planTitle} | ظرفیت ${planCount} نفر</span>
                    </label>`
                );

                planCounter++;
            }

            $('#selectedPlan').removeClass('d-none').addClass('d-flex');
        });

        $('#registerSansBtn').on('click' , function () {
            $('#sansForm').submit();
        })

        $('#finalSubmit').on('click', function () {
            if ($('#selectedPlan').children().length === 0) {
                alert('حداقل یک پلن باید اضافه کنید.');
                return false;
            }
        });

        
    </script>
    <script>
        
    </script>
@endsection


