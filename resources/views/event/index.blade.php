@extends('admin.layouts.master')

@section('title' , 'لیست رویداد ها')

@section('main')
    <div class="row row-cards mt-5">
        <div class="col-12">

            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="card">
                <div class="row card-header justify-content-between">
                    <div class="col-12 col-md-3 mb-2 mb-md-0">
                        <h3 class="card-title">لیست رویداد ها</h3>
                    </div>
                    <div class="col-12 col-md-3 text-end">
                        <a href="{{ route('verify.page.event') }}" class="btn btn-success ">احراز کد بلیط</a>
                        <a href="{{ route('create.event') }}" class="btn btn-primary ">افزودن رویداد</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive table-lg">
                        @if ($events->count() > 0)

                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center bg-info text-white">عنوان</th>
                                    <th class="text-center bg-info text-white">توضیحات</th>
                                    <th class="text-center bg-info text-white">پوستر</th>
                                    <th class="text-center bg-info text-white">بنر</th>
                                    <th class="text-center bg-info text-white">نقشه رویداد</th>
                                    <th class="text-center bg-info text-white">وضعیت</th>
                                    <th class="text-center bg-info text-white">امکانات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($events as $event)
                                    <tr>
                                        <td class="text-nowrap align-middle">{{ Str::limit($event->title,'20','...') }}</td>

                                        <td class="text-nowrap align-middle"> {{Str::limit($event->description,'20','...')}}</td>

                                        <td class="align-middle text-center">
                                            @if($event->poster)
                                                <a href="{{url($event->poster->path)}}"><img alt="image" class="avatar avatar-md br-7" src="{{asset($event->poster->path)}}"></a>
                                            @else
                                                <img alt="image" class="avatar avatar-md br-7" src="{{asset('assets/images/default-image.svg')}}">
                                            @endif
                                        </td>

                                        <td class="align-middle text-center">
                                            @if($event->banner)
                                                <a href="{{url($event->banner->path)}}"> <img alt="image" class="avatar avatar-md br-7" src="{{asset($event->banner->path)}}"></a>
                                            @else
                                                - - -
                                            @endif
                                        </td>

                                        <td class="align-middle text-center">
                                            @if($event->map)
                                                <a href="{{url($event->map->path)}}"> <img alt="image" class="avatar avatar-md br-7" src="{{asset($event->map->path)}}"></a>
                                            @else
                                                - - -
                                            @endif
                                        </td>
                                        <td class="align-middle text-center">
                                            <div class="">
                                                @if($event->is_free == true)
                                                    <span class="bg-info badge">رایگان</span>
                                                @else
                                                    <span class="bg-info badge">غیر رایگان</span>
                                                @endif

                                                @if($event->can_multy_buy == false)
                                                    <span class="bg-primary badge">امکان خرید چند بلیط</span>
                                                @endif

                                                @if($event->is_active == true)
                                                    <span class="bg-success badge">فعال</span>
                                                @else
                                                    <span class="bg-dark badge">غیر فعال</span>
                                                @endif
                                            </div>
                                            <div class="">
                                                @if ($event->company_acceptance == true)
                                                    <span class="bg-primary badge">شرکت ها</span>
                                                @endif

                                                @if($event->student_acceptance == true)
                                                    <span class="bg-primary badge">دانشجو ها</span>
                                                @endif

                                                @if ($event->applicant_acceptance == true)
                                                    <span class="bg-primary badge">کارجو ها</span>
                                                @endif
                                            </div>
                                        </td>

                                        <td class="text-center align-middle">
                                            <div class="row">
                                                <div class="btn-group align-top">
                                                    <a class="btn btn-sm btn-primary" href="{{route('edit.event', $event->id)}}">ویرایش</a>
                                                    <a class="btn btn-sm btn-danger" onclick="deleteEvent({{$event->id}})" href="javascript:void(0)">حذف</a>
                                                    @if($event->is_active == true)
                                                        <a class="btn btn-sm btn-dark" onclick="activeEvent({{ $event->id }})" href="javascript:void(0)">غیر فعال کردن</a>
                                                    @else
                                                        <a class="btn btn-sm btn-success" onclick="activeEvent({{ $event->id }})" href="javascript:void(0)">فعال کردن</a>
                                                    @endif
    
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="btn-group align-top mt-1">
                                                    <a class="btn btn-sm btn-success"  href="{{ route('plans.event' , $event->id) }}">لیست پلن ها</a>
                                                    <a class="btn btn-sm btn-info" href="{{ route('sans.event',$event->id) }}">لیست سانس ها</a>
                                                    <a class="btn btn-sm btn-secondary" href="{{ route('tickets.event', $event->id) }}">بلیط ها</a>
                                                </div>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$events->links('pagination.panel')}}
            </div>
        </div>

    </div>

<input type="hidden" id="deleteEventUrl" value="{{ route('delete.event') }}">
<input type="hidden" id="activeEventUrl" value="{{ route('active.event') }}">
@endsection

@section('script')
    <script src="{{ asset('assets/panel/js/emp-event.js') }}"></script>
@endsection