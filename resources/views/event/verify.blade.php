@extends('admin.layouts.master')

@section('title' , 'احراز کد بلیط')

@section('main')
    
    <div class="row row-cards mt-5">
        <div class="col-12">

            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="row justify-content-center">
                <div class="col-12 col-md-6 order-last order-md-first">
                    <div class="card">
                        <div class="card-header text-center bg-info text-white">
                            <div class="">
                                احراز کد بلیط
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('verify.code.event') }}" method="POST" class="">
                                @csrf
                                <div class=" row mb-4">
                                    <label class="col-md-3 form-label">
                                        <span> کد را وارد کنید : </span>
                                        {{-- <span class="text-danger">
                                            <small>(اجباری)</small>
                                        </span> --}}
                                    </label>
                                    <div class="col-md-9">
                                        <input name="code" type="text" class="form-control" >
                                        @error('code') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    <div class="col-2">
                                        <button class="btn btn-success" type="submit">جستجو</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 order-first order-md-last">
                    @isset($ticket)
                        <div class="card">
                            <div class="card-header bg-success justify-content-center">
                                @if (is_null($code->status))
                                    <h4 class="text-white mb-0">
                                        بلیط مورد نظر قابل شرکت در رویداد است
                                    </h4>
                                @endif
                            </div>
                            <div class="card-body">
                                <div class="row mb-3">
                                    <h5 class="col-12">
                                        بلیط رویداد {{ $ticket->event->title }} 
                                    </h5>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-12">
                                        خریدار : {{ $ticket->user->name }} {{ $ticket->user->family }}
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-12">
                                        در تاریخ <strong>{{ verta($ticket->info->sans->date)->format('%d، %B  %Y') }}</strong> ساعت <strong>{{ $ticket->info->sans->time }}</strong>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-12">
                                        نام پلن : {{ $ticket->info->plan->title }}
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    @if (is_null($code->status))
                                        <div class="text-center">
                                            <a href="{{ route('login.event' , $code->id) }}" class="btn btn-success mx-2">ثبت ورود</a>
                                            <a href="{{ route('void.code.event' , $code->id) }}" class="btn btn-danger mx-2">باطل کردن بلیط</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endisset
                    
                </div>
            </div>
        </div>
    </div>

@endsection