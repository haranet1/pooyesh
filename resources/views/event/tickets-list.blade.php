@extends('admin.layouts.master')

@section('title' , 'لیست بلیط های خریداری شده')

@section('main')
    
    <div class="row row-cards mt-5">
        <div class="col-12">

            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="card">
                <div class="row card-header justify-content-between">
                    <div class="col-12 col-md-4 mb-2 mb-md-0">
                        <h3 class="card-title">لیست بلیط های خریداری شده رویداد {{ $event->title }}</h3>
                    </div>
                    <div class="col-12 col-md-3 text-end">
                        <a href="{{ route('index.event') }}" class="btn btn-info">لیست رویداد ها</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive table-lg">
                        @if($tickets->count() > 0)
    
                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center bg-info text-white">خریدار</th>
                                        <th class="text-center bg-info text-white">سانس</th>
                                        <th class="text-center bg-info text-white">پلن</th>
                                        <th class="text-center bg-info text-white">تعداد و قیمت</th>
                                        <th class="text-center bg-info text-white">تخفیف</th>
                                        <th class="text-center bg-info text-white">هزینه نهایی</th>
                                        <th class="text-center bg-info text-white">وضعیت</th>
                                    </tr>
                                </thead>
    
                                <tbody>
    
                                    @foreach ($tickets as $ticket)
                                        <tr>
                                            <td class="align-middle text-center">{{ $ticket->user->name }} {{ $ticket->user->family }}</td>

                                            <td class="text-nowrap align-middle">
                                                <span> تاریخ : {{ verta($ticket->info->sans->date)->formatJalaliDate() }}</span>
                                                <span> ساعت : {{ $ticket->info->sans->time }}</span>
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                {{ $ticket->info->plan->title }}
                                            </td>

                                            <td class="text-nowrap align-middle text-center">
                                                <span> قیمت هر بلیط : {{ $ticket->info->plan->price }} تومان</span>
                                                <br>
                                                <span> تعداد : {{ $ticket->count }}</span>
                                            </td>

                                            <td class="text-nowrap align-middle text-center">
                                                @if ($ticket->has('discount'))
                                                    {{ $ticket->discount_price }} تومان
                                                @else
                                                    ---
                                                @endif
                                            </td>

                                            <td class="text-nowrap align-middle text-center">
                                                {{ $ticket->payable_price }} تومان
                                            </td>


                                            <td class="align-middle text-center">
                                                @if ($ticket->codes->count() > 0)
                                                    @php
                                                        $codeIsHeld = true;
                                                        foreach ($ticket->codes as $code) {
                                                            if(is_null($code->status) || $code->status == 0){
                                                                $codeIsHeld = false;
                                                            }
                                                        }
                                                    @endphp
                                                    @if ($codeIsHeld == true)
                                                        <span class="bg-success badge">شرکت کرده</span>
                                                    @else
                                                        <span class="bg-warning badge">شرکت نکرده</span>
                                                    @endif
                                                @else
                                                    
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
    
                                </tbody>
                            </table>
    
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$tickets->links('pagination.panel')}}
            </div>
        </div>
    </div>

@endsection