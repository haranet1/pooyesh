@extends('admin.layouts.master')

@section('title' , 'لیست پلن ها')

@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">

            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="card">
                <div class="row card-header justify-content-between">
                    <div class="col-12 col-md-3 mb-2 mb-md-0">
                        <h3 class="card-title">لیست پلن های رویداد {{ $event->title }}</h3>
                    </div>
                    <div class="col-12 col-md-3 text-end">
                        <a href="{{ route('index.event') }}" class="btn btn-info">لیست رویداد ها</a>
                        <a href="{{ route('create.plan.event' , $event->id) }}" class="btn btn-primary">افزودن پلن</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive table-lg">
                        @if ($plans->count() > 0)

                            <table class="table border-top table-bordered mb-0 table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center bg-info text-white">عنوان</th>
                                    <th class="text-center bg-info text-white">توضیحات</th>
                                    <th class="text-center bg-info text-white">قیمت</th>
                                    <th class="text-center bg-info text-white">ظرفیت</th>
                                    <th class="text-center bg-info text-white">تصویر</th>
                                    <th class="text-center bg-info text-white">عملکرد ها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($plans as $plan)
                                    <tr>

                                        <td class="align-middle text-center">{{ $plan->title }}</td>

                                        @if ($plan->description)
                                            <td class="text-nowrap align-middle">{{ Str::limit($plan->description,'20','...') }}</td>
                                        @else
                                            <td class="text-nowrap align-middle"> --- </td>
                                        @endif

                                        @if ($plan->price != "0")
                                            <td class="text-nowrap align-middle">{{ $plan->price }}</td>
                                        @else
                                            <td class="text-nowrap align-middle">رایگان</td>
                                        @endif

                                        <td class="text-nowrap align-middle">{{ $plan->count }}</td>

                                        <td class="align-middle text-center">
                                            @if($plan->photo)
                                                <a href="{{url($plan->photo->path)}}">
                                                    <img alt="image" class="avatar avatar-md br-7" src="{{asset($plan->photo->path)}}">
                                                </a>
                                            @endif
                                        </td>

                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a class="btn btn-sm btn-primary" href="{{route('edit.plan.event', $plan->id)}}">ویرایش</a>
                                                <a class="btn btn-sm btn-danger delete" onclick="deleteEventPlan({{$plan->id}})" href="javascript:void(0)">حذف</a>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$plans->links('pagination.panel')}}
            </div>
        </div>

    </div>

<input type="hidden" id="deletePlanUrl" value="{{ route('delete.plan.event') }}">
@endsection

@section('script')
    <script src="{{ asset('assets/panel/js/emp-event.js') }}"></script>
@endsection