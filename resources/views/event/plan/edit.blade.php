@extends('admin.layouts.master')

@section('title' , 'ویرایش پلن')

@section('main')

    <div class="row row-cards justify-content-center mt-5">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ویرایش پلن رویداد {{ $plan->event->title }}</h4>
                </div>

                <div class="card-body">
                    <form class="form-horizontal" action="{{route('update.plan.event')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="event_id" value="{{ $plan->event->id }}">
                        <input type="hidden" name="plan_id" value="{{ $plan->id }}">
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">
                                <span>عنوان</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="title" type="text" class="form-control" 
                                @if ($plan->title)
                                    value="{{ $plan->title }}"
                                @elseif (old('title'))
                                    value="{{ old('title') }}"
                                @endif
                                >
                                @error('title') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>پوستر</span>
                            </label>
                            <div class="col-md-1">
                                @if ($plan->photo)
                                    <a href="{{ url($plan->photo->path) }}" target="_blank"><img alt="image" class="avatar avatar-md br-7" src="{{ asset($plan->photo->path) }}"></a>
                                @endif
                            </div>
                            <div class="col-md-8">
                                <input name="photo" type="file" class="form-control" >
                                @error('photo') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>قیمت</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                @if ($plan->event->is_free == true)
                                    <input type="hidden" value="0" name="price">
                                    <input disabled type="text" value="0" class="form-control">
                                @else
                                    <input name="price" type="text" class="form-control"
                                    @if($plan->price)
                                        value="{{ $plan->price }}"
                                    @elseif (old('price'))
                                        value="{{ old('price') }}"
                                    @endif
                                    >
                                @endif
                                
                                
                                @error('price') 
                                    <span class="text-danger">{{$message}}</span>
                                @else
                                    @if ($plan->event->is_free == true)
                                        <span class="text-info">به دلیل رایگان بودن رویداد، امکان وارد کردن قیمت وجود ندارد.</span>
                                    @else
                                        <span class="text-info">به صورت عددی و به تومان وارد کنید .</span>
                                    @endif
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>ظرفیت</span>
                                <span class="text-danger">
                                    <small>(اجباری)</small>
                                </span>
                            </label>
                            <div class="col-md-9">
                                <input name="count" type="text" min="0" class="form-control" 
                                @if($plan->count)
                                    value="{{ $plan->count }}"
                                @elseif (old('count'))
                                    value="{{ old('count') }}"
                                @endif
                                >
                                @error('count') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>توضیحات</span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="description" cols="30" rows="5">
                                    @if ($plan->description)
                                    {{$plan->description}}
                                    @elseif(old('description'))
                                    {{old('description')}}
                                    @endif
                                </textarea>
                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                                <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection


