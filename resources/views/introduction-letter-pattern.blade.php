<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <title></title>
    <link href="{{public_path('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
    <style>
        body {

            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }
        body[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }
    </style>
</head>
<body style="font-family: fa" size="A4"  >
<div class="container ">
    <div class="row mt-5" dir="rtl">
        <div class="col-8 mt-3">
            <img src="{{public_path('assets/images/uni-logo.PNG')}}" class="img-fluid" alt="iau-logo">
        </div>
        <div class="col-4 mt-5">
            <p>شماره: <span>134242</span></p>
            <p>تاریخ: <span>{{verta(now())->formatDate()}}</span></p>
            <p>پیوست: <span>ندارد</span></p>
        </div>
        <hr class="bg-dark">
        <div class="row px-5">
            <div class="col-12 text-center justify-content-center">
                <p>بسمه تعالی</p>
            </div>
            <div class="col-12">
                <strong>مدیریت محترم شرکت/مرکز: <span class="text-bold">{{$data['company']}}</span></strong>
                <p>با سلام و احترام</p>
                <p>پس از حمد خدا و صلوات بر محمد و آل محمد(ص)، بدینوسیله سرکار خانم/ جناب آقای: {{$data['student']['name']." ".$data['student']['family']}} با کد ملی
                    {{$data['student']['groupable']['n_code']}} دانشجوی رشته {{$data['student']['groupable']['major']}} مقطع {{$data['student']['groupable']['grade']}} به شماره دانشجویی {{$data['student']['groupable']['std_number']}} جهت گذراندن دوره کارآموزی طرح پویش به مدت
                    {{$data['student']['groupable']['duration']}} ساعت حضورتان معرفی میگردد. </p>

                <p>لازم به ذکر است دانشجو موظف به رعایت مقررات انضباطی و ایمنی آن واحد صنعتی بوده و مطابق بخشنامه سازمان مرکزی دانشگاه، در طول دوره کارآموزی تحت پوشش بیمه حوادث قرار دارد.</p>

            </div>
            <div class="row justify-content-end text-left" >
                <div class="col-5 text-left justify-content-end">
                    <p>دکتر شهلا روزبهانی</p>
                    <p>معاون آموزش های عمومی و مهارتی دانشگاه آزاد اسلامی</p>
                    <p> واحد نجف آباد</p>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
