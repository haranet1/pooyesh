<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{{ env('APP_NAME') }}">
    <meta name="author" content="haranet.ir">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/panel/images/brand/favicon.ico') }}" />

    <title>{{ env('APP_NAME') }} | تکمیل اطلاعات</title>

    <link id="style" href="{{ asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/panel/css/style.css') }}" rel="stylesheet" />
    <link href="{{  asset('assets/panel/css/dark-style.css')  }}" rel="stylesheet" />
    <link href="{{ asset('assets/panel/css/transparent-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/panel/css/skin-modes.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/panel/css/icons.css') }}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{ asset('assets/panel/colors/color1.css') }}" />
    <link href="{{ asset('assets/panel/switcher/css/switcher.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/panel/switcher/demo.css') }}" rel="stylesheet" />
    <link rel="stylesheet" id="fonts" href="{{ asset('assets/panel/fonts/styles-fa-num/iran-yekan.css') }}">
    <link href="{{ asset('assets/panel/css/rtl.css') }}" rel="stylesheet" />
</head>
<body class="app sidebar-mini rtl login-img">

<div class="">

    <div id="global-loader">
        <img src="{{ asset('assets/panel/images/loader.svg') }}" class="loader-img" alt="Loader">
    </div>


    <div class="page">
        <div class="">
            <div class="col col-login mx-auto mt-7">
                <div class="text-center">
                    <img src="{{ asset('assets/panel/images/brand/jobist/white-text-logo.svg') }}" style="max-height: 70px; width:160px" class="header-brand-img" alt="">
                </div>
            </div>
            <div class="container mt-3">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-8">
                        <div class="wrap-login100 p-6">
                            <span class="login100-form-title pb-3">
                                تکمیل اطلاعات کاربری
                            </span>
                            <div class="row justify-content-center">
                                <span class="text-danger text-center pb-3" style="font-size: 1rem">(تمام فیلد ها الزامی است)</span>
                            </div>
                            <div class="panel panel-primary">
                                <form action="{{route('store.std.info')}}" method="POST" class="login100-form validate-form">
                                    @csrf

                                    <div class="row">
                                        <div class="col-12 col-lg-5">
                                            <div class="wrap-input100 mb-0 validate-input input-group">
                                                <span  class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account-card-details" aria-hidden="true"></i>
                                                </span>
                                                <input name="std_number" inputmode="numeric" value="{{old('std_number')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                       placeholder="شماره دانشجویی">
                                            </div>
                                            @error('std_number') <small class="text-danger ">{{$message}}</small> @enderror


                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span  class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account-card-details" aria-hidden="true"></i>
                                                </span>
                                                <input name="n_code" inputmode="numeric" value="{{old('n_code')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                       placeholder="شماره ملی">
                                            </div>
                                            @error('n_code') <small class="text-danger ">{{$message}}</small> @enderror

                                            {{-- <div class="wrap-input100 mb-0 mt-2  validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-email-outline" aria-hidden="true"></i>
                                                </span>
                                                <input name="email" value="{{old('email')}}" class="input100 border-start-0 ms-0 form-control" type="email"
                                                       placeholder="ایمیل">
                                            </div>
                                            @error('email') <small class="text-danger ">{{$message}}</small> @enderror --}}


                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-calendar-range" aria-hidden="true"></i>
                                                </span>
                                                <input name="age" inputmode="numeric" value="{{old('age')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                       placeholder="سن">
                                            </div>
                                            @error('age') <small class="text-danger ">{{$message}}</small> @enderror

                                            @if(is_null(Auth::user()->sex))
                                                <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account-multiple" aria-hidden="true"></i>
                                                </span>
                                                    <select name="sex" style="height: 44px" class="form-control form-select select2">
                                                        <option value="">جنسیت</option>
                                                        <option value="male">مرد</option>
                                                        <option value="female">زن</option>
                                                    </select>
                                                </div>
                                                @error('sex') <small class="text-danger ">{{$message}}</small> @enderror
                                            @endif

                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account" aria-hidden="true"></i>
                                                </span>
                                                <input name="major" value="{{old('major')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                       placeholder="رشته تحصیلی">
                                            </div>
                                            @error('major') <small class="text-danger ">{{$message}}</small> @enderror

                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account" aria-hidden="true"></i>
                                                </span>
                                                <input name="post_code" value="{{old('post_code')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                       placeholder="کد پستی">
                                            </div>
                                            @error('post_code') <small class="text-danger ">{{$message}}</small> @enderror



                                        </div>
                                        <div class="col-12 col-lg-7">
                                            <div class="wrap-input100 mb-0 mt-2 mt-md-0 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-map-marker-radius" aria-hidden="true"></i>
                                                </span>
                                                <select id="province" style="height: 44px" onchange="getCities()" name="province" class="form-control form-select select2" >
                                                    <option value="" label="انتخاب استان">انتخاب استان</option>
                                                    @foreach($provinces as $province)
                                                        <option value="{{$province->id}}"
                                                        @if (old('province'))
                                                            {{old('province') == $province->id ? 'selected' : ''}}
                                                        @endif
                                                        >{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @error('province') <small class="text-danger ">{{$message}}</small> @enderror


                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-city" aria-hidden="true"></i>
                                                </span>
                                                <select style="height: 44px" id="city" name="city" class="form-control form-select select2" >
                                                    <option value="">انتخاب شهر</option>

                                                </select>
                                            </div>
                                            @error('city') <small class="text-danger ">{{$message}}</small> @enderror

                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-map-marker-radius" aria-hidden="true"></i>
                                                </span>
                                                <input name="address" value="{{old('address')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                       placeholder="آدرس کامل">
                                            </div>
                                            @error('address') <small class="text-danger ">{{$message}}</small> @enderror

                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account" aria-hidden="true"></i>
                                                </span>
                                                <input name="plaque" value="{{old('plaque')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                       placeholder="پلاک">
                                            </div>
                                            @error('plaque') <small class="text-danger ">{{$message}}</small> @enderror

                                           @if(is_null(Auth::user()->email))
                                                <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account" aria-hidden="true"></i>
                                                </span>
                                                    <input name="email" value="{{old('email')}}" class="input100 border-start-0 ms-0 form-control" type="email"
                                                           placeholder="پست الکترونیکی">
                                                </div>
                                                @error('email') <small class="text-danger ">{{$message}}</small> @enderror
                                           @endif

                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-city-alt" aria-hidden="true"></i>
                                                </span>
                                                <select style="height: 44px" id="university" name="university_id" class="form-control form-select select2" >
                                                    <option value="" label="انتخاب واحد دانشگاهی">انتخاب واحد دانشگاهی</option>
                                                    @foreach($universities as $uni)
                                                        <option value="{{$uni->id}}"
                                                            @if(old('university_id'))
                                                                {{old('university_id') == $uni->id ? 'selected' : ''}}
                                                            @endif
                                                            >{{$uni->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @error('university_id') <small class="text-danger ">{{$message}}</small> @enderror

                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account" aria-hidden="true"></i>
                                                </span>
                                                <select style="height: 44px" name="grade" id="grade" class="form-control form-select select2">
                                                    <option
                                                    @if (old('grade'))
                                                        {{old('grade') == '' ? 'selected' : ''}}
                                                    @endif
                                                    value="">انتخاب مقطع تحصیلی</option>
                                                    <option
                                                    @if (old('grade'))
                                                        {{old('grade') == 'دکترا' ? 'selected' : ''}}
                                                    @endif
                                                    value="دکترا">دکترا</option>
                                                    <option
                                                    @if (old('grade'))
                                                        {{old('grade') == 'کارشناسی ارشد' ? 'selected' : ''}}
                                                    @endif
                                                    value="کارشناسی ارشد">کارشناسی ارشد</option>
                                                    <option
                                                    @if (old('grade'))
                                                        {{old('grade') == 'کارشناسی' ? 'selected' : ''}}
                                                    @endif
                                                    value="کارشناسی">کارشناسی</option>
                                                    <option
                                                    @if (old('grade'))
                                                        {{old('grade') == 'کاردانی' ? 'selected' : ''}}
                                                    @endif
                                                    value="کاردانی">کاردانی</option>
                                                    <option
                                                    @if (old('grade'))
                                                        {{old('grade') == 'دیپلم' ? 'selected' : ''}}
                                                    @endif
                                                    value="دیپلم">دیپلم</option>
                                                </select>
                                            </div>
                                            @error('grade') <small class="text-danger ">{{$message}}</small> @enderror


                                            {{-- <div class="wrap-input100 mt-2 mb-0 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-hourglass-outline" aria-hidden="true"></i>
                                                </span>
                                                <input name="duration" inputmode="numeric" value="{{old('duration')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                                       placeholder="تعداد ساعت دوره کارآموزی">
                                            </div>
                                            @error('duration') <small class="text-danger ">{{$message}}</small> @enderror --}}
                                        </div>
                                    </div>

                                    <div class="container-login100-form-btn">
                                        <button type="submit" class="login100-form-btn btn-primary">ثبت اطلاعات</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>


<script src="{{ asset('assets/panel/js/jquery.min.js')}}"></script>

<script src="{{ asset('assets/panel/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{ asset('assets/panel/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{ asset('assets/panel/js/show-password.min.js')}}"></script>

<script src="{{ asset('assets/panel/js/generate-otp.js')}}"></script>

<script src="{{ asset('assets/panel/js/themeColors.js')}}"></script>

<script src="{{ asset('assets/panel/js/custom.js')}}"></script>
<script src="{{ asset('assets/panel/js/custom1.js')}}"></script>

<script>
    function getCities(){
        var provinceValue = document.getElementById('province').value;
        if(provinceValue != 0){
            $(document).ready(function(){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    url: '{{route('users.registerAjax')}}',
                    data: {
                        do: 'get-cities',
                        province:$('#province').val() ,
                    },
                    dataType: 'json',
                    success: function (response) {
                        var city = document.getElementById('city')
                        city.innerHTML = '<option value="">انتخاب شهر</option>'
                        response.forEach(function (res){
                            city.innerHTML += '<option value="'+res['id']+'" label="'+res['name']+'">'+res['name']+'</option>'
                        })
                    },
                    error: function (response) {
                        console.log(response)
                        console.log('error')
                    }
                });
            });
        }
    }
</script>
</body>
</html>
