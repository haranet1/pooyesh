<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="پویش">
    <meta name="author" content="haranet.ir">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/frontend/img/home/Asset 1.svg') }}" />

    <title>{{ env('APP_NAME') }} | تکمیل اطلاعات</title>

    <link id="style" href="{{ asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/panel/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/panel/css/dark-style.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/panel/css/transparent-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/panel/css/skin-modes.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/panel/css/icons.css') }}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{ asset('assets/panel/colors/color1.css') }}" />
    <link rel="stylesheet" id="fonts" href="{{ asset('assets/panel/fonts/styles-fa-num/iran-yekan.css') }}">
    <link href="{{ asset('assets/panel/css/rtl.css') }}" rel="stylesheet" />
</head>
<body class="app sidebar-mini rtl login-img">

<div class="">
    <div id="global-loader">
        <img src="{{ asset('assets/panel/images/loader.svg') }}" class="loader-img" alt="Loader">
    </div>
    <div class="page">
        <div class="">
            <div class="col col-login mx-auto mt-7">
                <div class="text-center">
                    <img src="{{ asset('assets/panel/images/brand/jobist/white-text-logo.svg') }}" style="max-height: 70px; width:160px" class="header-brand-img" alt="">
                </div>
            </div>
            <div class="container-login100">
                <div class="wrap-login100 p-6 login-container">
                    <span class="login100-form-title pb-5">
                    تکمیل اطلاعات شرکت
                    </span>
                    <div class="row justify-content-center">
                        <span class="text-danger text-center pb-3" style="font-size: 1rem">(تمام فیلد ها الزامی است)</span>
                    </div>
                        <div class="panel panel-primary">
                            <form action="{{route('store.co.info')}}" method="POST" class="login100-form validate-form" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{ asset('assets/front/images/co-icon/company.svg') }}" alt="company-name">
                                            </span>
                                            <input value="{{ old('name') }}" name="name" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="نام شرکت ">
                                        </div>
                                        @error('name') <small class="text-danger ">{{ $message }}</small> @enderror

                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{asset('assets/front/images/co-icon/date.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('year')}}" name="year" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="سال تاسیس ">
                                        </div>
                                        @error('year') <small class="text-danger ">{{ $message }}</small> @enderror

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{ asset('assets/front/images/co-icon/phone.svg') }}" alt="company-name">
                                            </span>
                                            <input value="{{ old('phone') }}" name="phone" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="تلفن ">
                                        </div>
                                        @error('phone') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{asset('assets/front/images/co-icon/n_code.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('n_code')}}"  name="n_code" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="کد ملی شرکت ">
                                        </div>
                                        @error('n_code') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px; padding:2px" src="{{asset('assets/front/images/co-icon/r_number.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('r_number')}}" name="r_number" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="شماره ثبت ">
                                        </div>
                                        @error('r_number') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                    <div class="col-12 col-lg-6">

                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{asset('assets/front/images/co-icon/field.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('field')}}" name="field" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="حوزه فعالیت ">
                                        </div>
                                        @error('field') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px; padding:1px;" src="{{asset('assets/front/images/co-icon/admin.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('ceo')}}" name="ceo" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="نام مدیر عامل">
                                        </div>
                                        @error('ceo') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{ asset('assets/front/images/co-icon/size.svg') }}" alt="company-name">
                                            </span>
                                            <select name="size" class="input100 border-start-0 ms-0 form-control">
                                                <option value="">تعداد کارکنان </option>
                                                <option value="1 تا 5 نفر" {{ old('size') === '1 تا 5 نفر' ? 'selected' : '' }}>1 تا 5 نفر</option>
                                                <option value="6 تا 10 نفر" {{ old('size') === '6 تا 10 نفر' ? 'selected' : '' }}>6 تا 10 نفر</option>
                                                <option value="11 تا 20 نفر" {{ old('size') === '11 تا 20 نفر' ? 'selected' : '' }}>11 تا 20 نفر</option>
                                                <option value="21 تا 50 نفر" {{ old('size') === '21 تا 50 نفر' ? 'selected' : '' }}>21 تا 50 نفر</option>
                                                <option value="50 نفر به بالا" {{ old('size') === '50 نفر به بالا' ? 'selected' : '' }}>50 نفر به بالا</option>
                                            </select>
                                        </div>
                                        @error('size') <small class="text-danger">{{ $message }}</small> @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    {{-- <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{asset('assets/front/images/co-icon/email.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('email')}}" name="email" class="input100 border-start-0 ms-0 form-control" type="email"
                                                   placeholder="ایمیل ">
                                        </div>
                                        @error('email') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{asset('assets/front/images/co-icon/inviter.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('inviter')}}" name="inviter" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder=" معرف ">
                                        </div>
                                        @error('inviter') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{asset('assets/front/images/co-icon/province.svg')}}" alt="company-name">
                                            </span>
                                            <select class="form-control" onchange="getCities()" name="province" id="province">
                                                <option value="">انتخاب استان</option>
                                                @foreach($provinces as $province)
                                                    <option value="{{$province->id}}"
                                                    @if (old('province'))
                                                        {{old('province') == $province->id ? 'selected' : ''}}
                                                    @endif
                                                    >{{$province->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('province') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{asset('assets/front/images/co-icon/city.svg')}}" alt="company-name">
                                            </span>
                                            <select id="city" name="city" class="form-control" >
                                                <option value="">انتخاب شهر</option>
                                            </select>
                                        </div>
                                        @error('city') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 18px" src="{{asset('assets/front/images/co-icon/location.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('address')}}" name="address" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="آدرس ">
                                        </div>
                                        @error('address') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{asset('assets/front/images/co-icon/inviter.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('Plaque')}}" name="Plaque" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder=" پلاک ">
                                        </div>
                                        @error('Plaque') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="wrap-input100 validate-input input-group">
                                            <span class="input-group-text bg-white text-muted">
                                                <img style="width: 20px" src="{{asset('assets/front/images/co-icon/email.svg')}}" alt="company-name">
                                            </span>
                                            <input value="{{old('post_code')}}" name="post_code" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder=" کد پستی ">
                                        </div>
                                        @error('post_code') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-12 col-lg-6">
                                        <label for="header">تصویر پس زمینه: </label>
                                        <div class="wrap-input100 validate-input input-group ">
                                            <input name="header_img" class="input100 border-start-0 ms-0 form-control py-3" type="file">
                                        </div>
                                        @error('header_img') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <label for="logo">لوگو شرکت: </label>
                                        <div class="wrap-input100 validate-input input-group">
                                            <input name="logo" class="input100 border-start-0 ms-0 form-control py-3" type="file">
                                        </div>
                                        @error('logo') <small class="text-danger ">{{$message}}</small> @enderror

                                    </div>
                                </div>

                                <div class="row justify-content-center">
                                    <div class="col-12 ">
                                        <label for="desc">درباره شرکت: </label>
                                        <div class="wrap-input100 validate-input input-group justify-content-center">

                                            <textarea class="form-control" name="about" id="editor" cols="30" rows="5">{{ old('about') }}</textarea>
                                        </div>
                                        @error('about') <small class="text-danger ">{{$message}}</small> @enderror
                                    </div>
                                </div>

                                <div class="container-login100-form-btn">
                                    <button type="submit" class="login100-form-btn btn-primary">ثبت اطلاعات</button>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('assets/panel/js/jquery.min.js') }}"></script>

<script src="{{ asset('assets/panel/plugins/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/panel/plugins/bootstrap/js/bootstrap.min.js') }}"></script>

{{--<script src="{{'assets/panel/js/show-password.min.js'}}"></script>--}}

{{--<script src="{{'assets/panel/js/generate-otp.js'}}"></script>--}}

<script src="{{ asset('assets/panel/js/themeColors.js') }}"></script>

<script src="{{ asset('assets/panel/js/custom.js') }}"></script>
<script src="{{ asset('assets/panel/js/custom1.js') }}"></script>

<script src="{{ asset('assets/panel/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/panel/plugins/ckeditor/fa.js') }}"></script>

<script>
    ClassicEditor.create( document.querySelector( '#editor' ),{language: 'fa'} ).catch( error => {console.error( error );} );
</script>

<script>
    function getCities(){
        var provinceValue = document.getElementById('province').value;
        if(provinceValue != 0){
            $(document).ready(function(){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    url: "{{route('users.registerAjax')}}",
                    data: {
                        do: 'get-cities',
                        province:$('#province').val() ,
                    },
                    dataType: 'json',
                    success: function (response) {
                        var city = document.getElementById('city')
                        city.innerHTML = '<option value="">انتخاب شهر</option>'
                        response.forEach(function (res){
                            city.innerHTML += '<option value="'+res['id']+'" label="'+res['name']+'">'+res['name']+'</option>'
                        })
                    },
                    error: function (response) {
                        console.log(response)
                        console.log('error')
                    }
                });
            });
        }
    }
</script>
</body>
</html>
