@extends('panel.employee.master')

@section('title','لیست پیام ها')

@section('main')
    <div class="page-header">
        <h1 class="page-title">فهرست پیام ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">برنامه‌ها</a></li>
                <li class="breadcrumb-item active" aria-current="page">فهرست پیام ها</li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="row justify-content-start">
                <div class="col-12 col-md-4 input-group mb-5">
                    <input type="text" id="search" onkeyup="search()" class="form-control" placeholder="جستجو">
                    <div class="input-group-text btn btn-primary">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="page-options ms-auto">
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($tickets)>0)
                        <table class="table border-top table-bordered mb-0" id="table">
                            <thead>
                            <tr>
                                <th>موضوع </th>
                                <th>فرستنده </th>
                                <th>متن </th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tickets as $ticket)
                            <tr>
                                <td class="text-nowrap align-middle">{{$ticket->subject}}</td>
                                <td class="text-nowrap align-middle">{{$ticket->sender->name." ".$ticket->sender->family}}</td>
                                <td class="text-nowrap align-middle" ><p style="width: 50ch;overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{{$ticket->content}}</p></td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="javascript:void(0)" onclick="deleteTicket({{$ticket->id}})"  class="btn btn-sm btn-danger badge" type="button">حذف </a>
                                        @if(is_null($ticket->read_at))
                                            <a href="{{route('employee.ticket.read',$ticket)}})" class="btn btn-sm btn-success badge" type="button">خوانده شد </a>
                                        @endif
                                            <a class="btn btn-primary " data-bs-target="#modal-ticket-{{$ticket->id}}" data-bs-toggle="modal" href="javascript:void(0)">
                                            مشاهده پیام </a>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade" id="modal-ticket-{{$ticket->id}}">
                                <div
                                    class="modal-dialog" role="document">
                                    <div class="modal-content modal-content-demo">
                                        <div class="modal-header">
                                            <div class="row">
                                                <h6 class="modal-title text-primary">متن پیام: </h6>
                                                <p class="modal-title" >
                                                    {{ $ticket->content}}
                                                </p>
                                            </div>
                                            
                                        </div>
                                        <div class="modal-body" style="max-height: 450px !important; overflow-y: scroll !important">
                                            
                                            @if($ticket->replies)
                                               <h6>پاسخ دریافتی:</h6>
                                                @foreach($ticket->replies as $reply)
                                                    <div class="row justify-content-start mb-2">
                                                        <div class="col-10 border bg-info text-white" style="border-radius: 20px">
                                                            <div class="row justify-content-start">
                                                                <p class="text-dark">{{$ticket->sender->name." ".$ticket->sender->family}}</p>
                                                            </div>
                                                            <div class="row mt-2">
                                                                <div class="col-10">
                                                                    <p>{{$reply->content}} </p>
                                                                </div>
                                                                
                                                                <span class="text-end">{{verta($reply->created_at)->formatDate()}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="modal-footer justify-contant-start">
                                            <h6>ارسال پاسخ:</h6>
                                            <form class="py-1" action="{{route('employee.ticket.send-reply')}}" method="POST">
                                                @csrf
                                                <input type="hidden" name="parent_id" value="{{$ticket->id}}">
                                                <input type="text" class="form-control" name="content">
                                                <button class="btn mt-2 btn-success" type="submit">ارسال</button>
                                            </form>
                                            <button
                                                class="btn btn-light"
                                                data-bs-dismiss="modal">
                                                بستن
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$tickets->links('pagination.panel')}}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function search() {
            var table = document.getElementById('table')
            var key = $('#search').val()
            $('#table tr').remove()
            table.innerHTML = '<tr>\n' +
                '                            <th>موضوع</th>\n' +
                '                            <th>فرستنده</th>\n' +
                '                            <th>متن </th>\n' +
                '                            <th>عملکردها</th>\n' +
                '                        </tr>'


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: 'POST',
                url: '{{route('employee.ajax.search')}}',
                data: {
                    do: 'search-tickets',
                    key: key
                },
                success: function (response) {
                    console.log(response)
                    response['data'].forEach(function (re) {
                        var row = table.insertRow(-1);
                        var td0 = row.insertCell(0)
                        var td1 = row.insertCell(1)
                        var td2 = row.insertCell(2)
                        var td3 = row.insertCell(3)
                        td0.innerHTML = re['subject']
                        td1.innerHTML = re['sender_name']
                        td2.innerHTML = re['content']
                        td3.innerHTML = ''
                    })
                },
                error: function (data) {
                    console.log(data)
                }
            })
        }
    </script>
    <script>
        function deleteTicket(id){
            Swal.fire({
                title: 'آیا مطمئن هستید؟',
                text: "پیام حذف خواهد شد!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#198f20',
                cancelButtonColor: '#d33',
                cancelButtonText: 'انصراف',
                confirmButtonText: 'بله'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        headers:{
                            'X-CSRF-TOKEN':'{{csrf_token()}}'
                        },
                        url:'{{route('company.ajax')}}',
                        type:'post',
                        data:{
                            do:'delete-ticket',
                            id:id,
                        },
                        success:function (response){
                            location.reload()
                        },
                        error:function (response){
                            console.log(response)
                        }
                    })
                }
            })
        }
    </script>
@endsection
