@extends('panel.employee.master')
@section('title','لیست دانشجویان')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست دانشجویان</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
                <input type="text" id="search" class="form-control" onkeyup="search()" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                        <a href="{{route('employee.std.export-excel')}}" class="btn btn-success">خروجی اکسل</a>

                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($students) > 0)
                            <table class="table border-top table-bordered mb-0" id="table">
                                <thead>
                                <tr>
                                    <th>نام و نام خانوادگی </th>
                                    <th>تلفن</th>
                                    <th>سن</th>
                                    <th>دانشگاه</th>
                                    <th>رشته</th>
                                    <th>شهر</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $std)
                                    <tr>
                                        <td class="text-nowrap align-middle">{{$std->name." ".$std->family}}</td>
                                        <td class="text-nowrap align-middle">{{$std->mobile}}</td>
                                        <td class="text-nowrap align-middle">{{$std->groupable->age}}</td>
                                        <td class="text-nowrap align-middle">{{$std->groupable->university}}</td>
                                        <td class="text-nowrap align-middle">{{$std->groupable->major}}</td>
                                        <td class="text-nowrap align-middle">{{$std->groupable->city->name}}</td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a href="{{route('candidate.single',$std->id)}}" class="btn btn-sm btn-primary badge" type="button">پروفایل </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
               {{$students->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        function search() {
            var table = document.getElementById('table')
            var key = $('#search').val()
            $('#table tr').remove()
            table.innerHTML = '<tr>\n' +
                '                            <th>نام و نام خانوادگی</th>\n' +
                '                            <th>تلفن</th>\n' +
                '                            <th>سن </th>\n' +
                '                            <th>دانشگاه</th>\n' +
                '                            <th>رشته</th>\n' +
                '                            <th>شهر</th>\n' +
                '                            <th>عملکردها</th>\n' +
                '                        </tr>'


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: 'POST',
                url: '{{route('employee.ajax.search')}}',
                data: {
                    do: 'search-students',
                    key: key
                },
                success: function (response) {
                    response['data'].forEach(function (re) {
                        var route = "{{route('candidate.single',['id' => ':id'])}}"
                        route = route.replace(':id', re['id'])
                        var row = table.insertRow(-1);
                        var td0 = row.insertCell(0)
                        var td1 = row.insertCell(1)
                        var td2 = row.insertCell(2)
                        var td3 = row.insertCell(3)
                        var td4 = row.insertCell(4)
                        var td5 = row.insertCell(5)
                        var td6 = row.insertCell(6)
                        td0.innerHTML = re['name']+" "+re['family']
                        td1.innerHTML = re['mobile']
                        td2.innerHTML = re['groupable']['age']
                        td3.innerHTML = re['groupable']['university']
                        td4.innerHTML = re['groupable']['major']
                        td5.innerHTML = re['groupable']['city']['name']
                        td6.innerHTML = '<div class="btn-group align-top">' +
                            ' <a href="' + route + '" class="btn btn-sm btn-primary badge" type="button"> پروفایل </a> </div>'
                    })
                },
                error: function (data) {
                    console.log(data)
                }
            })
        }
    </script>
@endsection
