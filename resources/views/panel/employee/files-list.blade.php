@extends('panel.employee.master')
@section('title','لیست فایل ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست فایل ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">

            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                       <a href="{{route('files.create')}}" class="btn btn-success">افزودن فایل جدید</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($files)>0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>عنوان</th>
                                    <th>لینک</th>
                                    <th>قابل دانلود برای دانشجو</th>
                                    <th>قابل دانلود برای شرکت</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($files as $file)
                                    <tr>

                                        <td class="text-nowrap align-middle"> {{$file->name}}</td>
                                        <td class="text-nowrap align-middle"><a href="{{asset($file->path)}}">لینک فایل</a></td>
                                        <td>
                                            @if($file->std_can_dl)
                                                <span class="badge bg-success"><i class="fa fa-check"></i></span>
                                            @else
                                                <span class="badge bg-danger"><i class="fa fa-times"></i></span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($file->co_can_dl)
                                                <span class="badge bg-success"><i class="fa fa-check"></i></span>
                                            @else
                                                <span class="badge bg-danger"><i class="fa fa-times"></i></span>
                                            @endif
                                        </td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a href="javascript:void(0)" onclick="deleteFile({{$file->id}})" class="btn btn-danger">حذف</a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$files->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
