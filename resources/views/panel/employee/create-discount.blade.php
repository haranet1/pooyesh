@extends('panel.employee.master')
@section('title','تعریف کد تخفیف')
@section('main')

    <style>
        div.title {
            display: none;
        }
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 2px solid rgba(0, 0, 0, 0.553);
        }
        .product-variants {
            margin: 0px 0 10px;
            color: #6f6f6f;
            font-size: 13px;
            line-height: 1.692;
        }
        ul.js-product-variants {

            display: inline-block;
            vertical-align: middle;
        }
        ul.js-product-variants li {
            margin: 0 8px 0 0;
            display: inline-block;
        }

        .product-variant>span {
            font-size: inherit;
            color: inherit;
            padding-left: 15px;
            margin-top: 3px;
            float: right;
        }

        .product-variants {
            margin-right: -8px;
            list-style: none;
            padding: 0;
            display: inline-block;
            margin-bottom: 0;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }



        .ui-variant input[type=checkbox] {
            visibility: hidden;
            position: absolute;
        }

        .ui-variant--check {
            cursor: pointer;
            border: 1px solid transparent;
            border-radius: 10px;
            color: #6f6f6f;
            padding: 3px 10px;
            font-size: 13px;
            font-size: .929rem;
            line-height: 1.692;
            display: block;
            -webkit-box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
            box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
        }

        .ui-variant--color .ui-variant--check {
            padding-right: 37px;
        }

        input[type=checkbox]:checked+.ui-variant--check {
            border-color: #2c90f6;
            background-color: #2c91f8;
            color: white;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .ui-variant--color .ui-variant-shape {
            width: 18px;
            height: 18px;
            position: absolute;
            right: 8px;
            top: 8px;
            border-radius: 50%;
            content: "";
            cursor: pointer;
        }
    </style>

    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> تعریف کد تخفیف</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ثبت کد تخفیف</h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('store.discount')}}" method="post" >
                        @csrf
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label" for="name">عنوان کد</label>
                            <div class="col-md-9">
                                <input name="name" id="name" type="text" class="form-control" >
                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="is_percent">نوع</label>
                            <div class="col-md-9">
                                <select class="form-select form-control" name="is_percent" id="is_percent">
                                    <option class="form-control" value="1">درصد</option>
                                    <option class="form-control" value="0">ریال</option>
                                </select>
                                @error('is_percent') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label" for="value">مقدار</label>
                            <div class="col-md-9">
                                <input name="value" id="value" type="text" class="form-control" >
                                @error('value') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="start">تاریخ شروع </label>
                            <div class="col-md-9">
                                <input name="begin" type="text" id="start" class="form-control" >
                                @error('begin') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="end">تاریخ پایان</label>
                            <div class="col-md-9">
                                <input name="end" type="text" id="end" class="form-control" >
                                @error('end') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="max">تعداد</label>
                            <div class="col-md-9">
                                <input name="max" type="text" id="max" class="form-control" >
                                @error('max') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="description">توضیحات</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="description" cols="30" rows="5"></textarea>
                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <hr>

                        <div class="row mb-4 align-items-end">

                            <div class="col-12 col-md-5">
                                <label for="event" class="form-lable">رویداد :</label>
                                <input type="hidden" id="event_input" name="isAllEvent" value="0">
                                <select id="event" onchange="getEventInfos()" class="form-select form-control">
                                    @foreach ($events as $event)
                                        <option value="all"> همه موارد </option>
                                        <option value="{{ $event->id }}">{{ $event->title }}</option>
                                    @endforeach
                                </select>
                                <span class="text-sm text-danger" id="eventErr"></span>
                            </div>

                            <div class="col-12 col-md-5">
                                <label for="event" class="form-lable">سانس و پلن :</label>
                                <select id="eventInfo" class="form-select form-control">
                                    <option value=""> رویداد را انتخاب کنید </option>
                                </select>
                                <span class="text-sm text-danger" id="eventInfoErr"></span>
                            </div>
                            
                            <div class="col-12 col-md-2">
                                <button type="button" class="btn btn-success" onclick="addEventInfo()">افزودن</button>
                            </div>

                        </div>

                        <hr>

                        <div class="row mb-4 align-items-end">

                            <div class="col-12 col-md-5">
                                <label for="sadra" class="form-lable">صدرا :</label>
                                <input type="hidden" id="sadra_input" name="isAllSadra" value="0">
                                <select id="sadra" onchange="getSadraPlans()" class="form-select form-control">
                                    <option value="all"> همه موارد </option>
                                    @foreach ($sadras as $sadra)
                                        <option value="{{ $sadra->id }}">{{ $sadra->title }}</option>
                                    @endforeach
                                </select>
                                <span class="text-sm text-danger" id="sadraErr"></span>
                            </div>

                            <div class="col-12 col-md-5">
                                <label for="sadraPlan" class="form-lable">سانس و پلن :</label>
                                <select id="sadraPlan" class="form-select form-control">
                                    <option value=""> رویداد را انتخاب کنید </option>
                                </select>
                                <span class="text-sm text-danger" id="sadraPlanErr"></span>
                            </div>
                            <div class="col-12 col-md-2">
                                <button type="button" class="btn btn-success" onclick="addSadraPlan()">افزودن</button>
                            </div>

                        </div>

                        
                        <hr>

                        <div class="d-flex mb-4" id="">
                            <div class="" id="selectedEventInfo">

                            </div>
                        </div>

                        <div class="d-flex mb-4" id="">
                            <div class="" id="selectedSadraPlans">

                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">
                                <span>نوع کاربری شرکت کنندگان : </span>
                            </label>
                            <div class="col-md-9">
                                <div class="product-variants">
                                    <ul class="js-product-variants">
                                        <li class="ui-variant">
                                            <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-building"></i>
                                                            </span>
                                                <input type="checkbox" id="company_acceptance" name="company_acceptance" class="variant-selector">
                                                <span class="ui-variant--check">شرکت</span>
                                            </label>
                                        </li>
                                        <li class="ui-variant">
                                            <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                <input type="checkbox" id="student_acceptance" name="student_acceptance" class="variant-selector">
                                                <span class="ui-variant--check">دانشجو</span>
                                            </label>
                                        </li>
                                        <li class="ui-variant">
                                            <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                <input type="checkbox" id="applicant_acceptance" name="applicant_acceptance" class="variant-selector">
                                                <span class="ui-variant--check">کارجو</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <small class="text-danger " id="acceptanceErr"></small>
                        </div>



                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
    <input type="hidden" id="getEventInfos" value="{{ route('get.info.event2') }}">
    <input type="hidden" id="getSadraPlans" value="{{ route('get.sadra.plans') }}">
@endsection
@section('script')
    <script>
        $('#start').persianDatepicker({
            altField: '#start',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
        $('#end').persianDatepicker({
            altField: '#end',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
    </script>
    <script src="{{ asset('assets/panel/js/discount.js') }}"></script>
@endsection
