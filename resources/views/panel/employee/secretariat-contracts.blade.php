@extends('panel.employee.master')
@section('title','لیست شرکت ها')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/timeline-style.css')}}"/>
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست درخواست های تایید شده توسط کارشناس پویش</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست درخواست های تایید شده توسط کارشناس پویش</h3>
                </div>
                <div class="card-body">
                    <div class="card-pay">
                        <ul class="tabs-menu nav">
                            <li class=""><a href="#tab20" class="active" data-bs-toggle="tab">قرارداد های جدید</a></li>
                            <li><a href="#tab21" data-bs-toggle="tab" class="">قرارداد های چاپ شده</a></li>
                        </ul>
                        <div class="tab-content">
                            {{-- mbti list --}}
                            <div class="tab-pane active show" id="tab20">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        {{-- <div class="col-10">
                                            <div class="input-group">
                                                <input type="text" id="test_mbti_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="test_mbti_family_search" class="form-control" placeholder="نام خانوادگی :" style="border-radius: 0px;">
                                                <input type="text" id="test_mbti_mobile_search" class="form-control" placeholder="تلفن همراه :" style="border-radius: 0px;">
                                                <input type="text" id="test_mbti_province_search" class="form-control" placeholder="استان :" style="border-radius: 0px;">
                                                <select id="test_mbti_sex_search" class="form-control" style="border-radius: 0px;">
                                                    <option value="0">جنسیت :</option>
                                                    <option value="male">مرد</option>
                                                    <option value="female">زن</option>
                                                </select>
                                                <select id="test_mbti_role_search" class="form-control" style="border-radius: 0px;">
                                                    <option value="0">نقش :</option>
                                                    <option value="company">شرکت</option>
                                                    <option value="applicant">کارجو</option>
                                                    <option value="student">دانشجو</option>
                                                </select>
                                                <div class="input-group-prepend">
                                                    <div onclick="testerMbtiSearch()" id="test_mbti_btn_search" class="btn btn-info form-control" style="border-top-right-radius: 0px;border-bottom-right-radius: 0px;">جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <a href="" class="btn btn-secondary">خروجی اکسل</a>
                                        </div> --}}
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$requests->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg justify-content-center text-center">
                                        @if(count($requests)>0)
                                            <table class="table border-top table-bordered mb-0" id="table">
                                                <thead>
                                                <tr>
                                                    {{-- <th>ردیف</th> --}}
                                                    <th>فرستنده</th>
                                                    <th>گیرنده</th>
                                                    <th>تاریخ</th>
                
                                                    <th class="text-center">عملکردها</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {{-- @php
                                                    $counter = (($requests->currentPage() -1) * $requests->perPage()) + 1;
                                                @endphp --}}
                                                @foreach($requests as $request)
                                                    <tr>
                                                        {{-- <td class="text-nowrap align-middle"> {{$request++}}</td> --}}
                                                        <td class="text-nowrap align-middle">
                                                            @if($request->sender_type == 'co')
                                                                <span class="badge bg-warning">شرکت</span>
                                                                {{$request->sender->groupable->name}}
                                                            @elseif($request->sender_type == 'std')
                                                                <span class="badge bg-info">دانشجو</span>
                                                                {{$request->sender->name." ".$request->sender->family}}
                                                            @elseif ($request->sender_type == 'apl')
                                                                <span class="badge bg-info">کارجو</span>
                                                                {{$request->sender->name." ".$request->sender->family}}
                                                            @endif
                                                        </td>
                                                        <td class="text-nowrap align-middle">
                                                            @if($request->receiver_type == 'co')
                                                                <span class="badge bg-warning">شرکت</span>
                                                                {{$request->receiver->groupable->name}}
                                                            @elseif($request->receiver_type == 'std')
                                                                <span class="badge bg-info">دانشجو</span>
                                                                {{$request->receiver->name." ".$request->receiver->family}}
                                                            @elseif ($request->receiver_type == 'apl')
                                                                <span class="badge bg-info">کارجو</span>
                                                                {{$request->receiver->name." ".$request->receiver->family}}
                                                            @endif
                                                        </td>
                                                        <td class="text-nowrap align-middle"> {{verta($request->created_at)->formatDate()}}</td>
                                                        <td class="text-center align-middle">
                                                            <div class="btn-group align-top">
                                                                @if($request->process)
                                                                    @if(!str_contains(getPooyeshProcessStatus($request->process->id),'لغو'))
                                                                        @if (!is_null($request->process->accepted_by_uni_at) && is_null($request->process->contract_printed_at))
                                                                            <a onclick="printContract({{$request->id}})" 
                                                                                href="{{asset($request->contract->path)}}" download
                                                                                class="btn btn-sm btn-success text-white" type="button">دانلود
                                                                                قرارداد</a>
                                                                        @endif
                                                                    @endif
                                                                @endif
                                                            </div>
                
                                                            <div class="btn-group align-top">
                                                                <a @if($request->status == 1) class="btn btn-sm btn-primary"
                                                                   data-bs-target="#modal-{{$request->id}}"
                                                                   data-bs-toggle="modal"
                                                                   href="javascript:void(0)"
                                                                   @else class="disabled btn btn-sm btn-primary" @endif>
                                                                    روند درخواست</a>
                
                                                            </div>
                                                            <div class="modal fade "
                                                                id="modal-{{$request->id}}">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content modal-content-demo">
                                                                        <div class="modal-header">
                                                                            <h6 class="modal-title">
                                                                                روند پویش
                                                                                کارجو {{$request->sender->name." ".$request->sender->family}}</h6>
                                                                            <button
                                                                                aria-label="Close"
                                                                                class="btn-close"
                                                                                data-bs-dismiss="modal">
                                                                                <span
                                                                                    aria-hidden="true ">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div
                                                                                class="card-body">
                                                                                <div class="vtimeline">
                                                                                    <div class="row gutters">
                                                                                        <div
                                                                                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                                                            <div class="card">
                                                                                                <div class="card-body">
                                                                                                @if($request->process)
                                                                                                    <!-- Timeline start -->
                                                                                                        <div class="timeline">
                                                                                                            <div class="timeline-row">
                                                                                                                <div
                                                                                                                    class="timeline-time">
                                                                                                                    {{verta($request->created_at)->formatDate()}}
                                                                                                                    <small>{{verta($request->created_at)->formatTime()}}</small>
                                                                                                                </div>
                                                                                                                <div
                                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                                <div
                                                                                                                    class="timeline-content ">
                                                                                                                    <i class="fa fa-check"></i>
                                                                                                                    <h4>ارسال درخواست
                                                                                                                        توسط {{$request->sender_type == 'co'?'شرکت' :'کارجو'}} </h4>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            @if($request->process->rejected_by_co_at)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->rejected_by_co_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->rejected_by_co_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-warning ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4>رد شده توسط
                                                                                                                            شرکت</h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                                                                                                            @if($request->process->rejected_by_std_at)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->rejected_by_std_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->rejected_by_std_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-warning ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4>رد شده توسط
                                                                                                                            کارجو</h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if($request->sender_type == 'std' && $request->process->accepted_by_co_at == null)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        <small>...</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-muted">
                                                                                                                        <i class="fa fa-hourglass"></i>
                                                                                                                        <h4>در انتظار
                                                                                                                            تایید توسط
                                                                                                                            شرکت </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                                                                                                            @if($request->sender_type == 'std' && $request->process->accepted_by_co_at)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->accepted_by_co_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->accepted_by_co_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4> تایید توسط
                                                                                                                            شرکت </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif


                                                                                                            @if($request->sender_type == 'co' && $request->process->accepted_by_std_at == null)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        <small>...</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-muted">
                                                                                                                        <i class="fa fa-hourglass"></i>
                                                                                                                        <h4>در انتظار
                                                                                                                            تایید توسط
                                                                                                                            کارجو </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                                                                                                            @if($request->sender_type == 'co' && $request->process->accepted_by_std_at)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->accepted_by_std_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->accepted_by_std_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4> تایید توسط
                                                                                                                            کارجو </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if(is_null($request->process->rejected_by_uni_at)  && is_null($request->process->accepted_by_uni_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        <small>...</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-muted">
                                                                                                                        <i class="fa fa-hourglass"></i>
                                                                                                                        <h4>در انتظار
                                                                                                                            تایید
                                                                                                                            دانشگاه</h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if(!is_null($request->process->accepted_by_uni_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->accepted_by_uni_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->accepted_by_uni_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4>تایید توسط
                                                                                                                            دانشگاه </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if(!is_null($request->process->rejected_by_uni_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->rejected_by_uni_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->rejected_by_uni_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-danger ">
                                                                                                                        <i class="fa fa-times"></i>
                                                                                                                        <h4>رد شده توسط
                                                                                                                            دانشگاه </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if (!is_null($request->process->accepted_by_uni_at) && is_null($request->process->rejected_by_uni_at) && is_null($request->process->contract_printed_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        <small>...</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-muted">
                                                                                                                        <i class="fa fa-hourglass"></i>
                                                                                                                        <h4>مراجعه دانشجو به دبیرخانه</h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if (!is_null($request->process->contract_printed_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        <small>...</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-muted">
                                                                                                                        <i class="fa fa-hourglass"></i>
                                                                                                                        <h4>تحویل قرارداد به دانشجو</h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if(!is_null($request->process->canceled_by_std_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->canceled_by_std_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->canceled_by_std_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-danger ">
                                                                                                                        <i class="fa fa-times"></i>
                                                                                                                        <h4>لغو توسط
                                                                                                                            کارجو </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if(!is_null($request->process->canceled_by_co_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->canceled_by_co_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->canceled_by_co_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-danger ">
                                                                                                                        <i class="fa fa-times"></i>
                                                                                                                        <h4>لغو توسط
                                                                                                                            شرکت </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if(!is_null($request->process->done_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->done_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->done_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4>اتمام </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                        </div>
                                                                                                        <!-- Timeline end -->
                                                                                                    @endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="modal-footer">
                                                                            <button
                                                                                class="btn btn-light"
                                                                                data-bs-dismiss="modal">
                                                                                بستن
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-warning text-center mt-3 py-2 ">
                                                <h5>{{__('public.no_info')}}</h5>
                                            </div>
                                        @endif
                                    </div>

                                </div>
                                <div class="mb-5">
                                   {{$requests->links('pagination.panel')}}
                                </div>
                            </div>
                            <div class="tab-pane" id="tab21">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        {{-- <div class="col-10">
                                            <div class="input-group">
                                                <input type="text" id="test_enne_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="test_enne_family_search" class="form-control" placeholder="نام خانوادگی :" style="border-radius: 0px;">
                                                <input type="text" id="test_enne_mobile_search" class="form-control" placeholder="تلفن همراه :" style="border-radius: 0px;">
                                                <input type="text" id="test_mbti_province_search" class="form-control" placeholder="استان :" style="border-radius: 0px;">
                                                <select id="test_enne_sex_search" class="form-control" style="border-radius: 0px;">
                                                    <option value="0">جنسیت :</option>
                                                    <option value="male">مرد</option>
                                                    <option value="female">زن</option>
                                                </select>
                                                <select id="test_enne_role_search" class="form-control" style="border-radius: 0px;">
                                                    <option value="0">نقش :</option>
                                                    <option value="company">شرکت</option>
                                                    <option value="applicant">کارجو</option>
                                                    <option value="student">دانشجو</option>
                                                </select>
                                                <div class="input-group-prepend">
                                                    <div onclick="testerEnnagramSearch()" id="test_enne_btn_search" class="btn btn-info form-control" style="border-top-right-radius: 0px;border-bottom-right-radius: 0px;">جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <a href="" class="btn btn-secondary">خروجی اکسل</a>
                                        </div> --}}
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$printedRequests->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg">
                                        @if(count($printedRequests) > 0)
                                            <table class="table border-top table-bordered mb-0 table table-striped" id="table">
                                                <thead>
                                                <tr>
                                                    {{-- <th>ردیف</th> --}}
                                                    <th>فرستنده</th>
                                                    <th>گیرنده</th>
                                                    {{-- <th>وضعیت اولیه</th> --}}
                                                    <th>تاریخ</th>
                                                    <th class="text-center">عملکردها</th>
                                                </tr>
                                                </thead>
                                                <tbody id="test-enneagram-table-body">
                                                {{-- @php
                                                    $counter = (($printedRequests->currentPage() -1) * $printedRequests->perPage()) + 1;
                                                @endphp --}}
                                                @foreach($printedRequests as $request)
                                                    <tr>
                                                        {{-- <td class="text-nowrap align-middle"> {{$request++}}</td> --}}
                                                        <td class="text-nowrap align-middle">
                                                            @if($request->sender_type == 'co')
                                                                <span class="badge bg-warning">شرکت</span>
                                                                {{$request->sender->groupable->name}}
                                                            @elseif($request->sender_type == 'std')
                                                                <span class="badge bg-info">دانشجو</span>
                                                                {{$request->sender->name." ".$request->sender->family}}
                                                            @elseif ($request->sender_type == 'apl')
                                                                <span class="badge bg-info">کارجو</span>
                                                                {{$request->sender->name." ".$request->sender->family}}
                                                            @endif
                                                        </td>
                                                        <td class="text-nowrap align-middle">
                                                            @if($request->receiver_type == 'co')
                                                                <span class="badge bg-warning">شرکت</span>
                                                                {{$request->receiver->groupable->name}}
                                                            @elseif($request->receiver_type == 'std')
                                                                <span class="badge bg-info">دانشجو</span>
                                                                {{$request->receiver->name." ".$request->receiver->family}}
                                                            @elseif ($request->receiver_type == 'apl')
                                                                <span class="badge bg-info">کارجو</span>
                                                                {{$request->receiver->name." ".$request->receiver->family}}
                                                            @endif
                                                        </td>
                                                        <td class="text-nowrap align-middle"> {{verta($request->created_at)->formatDate()}}</td>
                                                        <td class="text-center align-middle">
                                                            <div class="btn-group align-top">
                                                                @if($request->process)
                                                                    @if(!str_contains(getPooyeshProcessStatus($request->process->id),'لغو'))
                                                                        
                                                                    @endif
                                                                @endif
                                                            </div>
                
                                                            <div class="btn-group align-top">
                                                                <a @if($request->status == 1) class="btn btn-sm btn-primary"
                                                                   data-bs-target="#modal-{{$request->id}}"
                                                                   data-bs-toggle="modal"
                                                                   href="javascript:void(0)"
                                                                   @else class="disabled btn btn-sm btn-primary" @endif>
                                                                    روند درخواست</a>
                
                                                            </div>
                                                            <div class="modal fade "
                                                                 id="modal-{{$request->id}}">
                                                                <div
                                                                    class="modal-dialog modal-lg"
                                                                    role="document">
                                                                    <div
                                                                        class="modal-content modal-content-demo">
                                                                        <div
                                                                            class="modal-header">
                                                                            <h6 class="modal-title">
                                                                                روند پویش
                                                                                کارجو {{$request->sender->name." ".$request->sender->family}}</h6>
                                                                            <button
                                                                                aria-label="Close"
                                                                                class="btn-close"
                                                                                data-bs-dismiss="modal">
                                                                                <span
                                                                                    aria-hidden="true ">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div
                                                                                class="card-body">
                                                                                <div class="vtimeline">
                                                                                    <div class="row gutters">
                                                                                        <div
                                                                                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                                                            <div class="card">
                                                                                                <div class="card-body">
                                                                                                @if($request->process)
                                                                                                    <!-- Timeline start -->
                                                                                                        <div class="timeline">
                                                                                                            <div class="timeline-row">
                                                                                                                <div
                                                                                                                    class="timeline-time">
                                                                                                                    {{verta($request->created_at)->formatDate()}}
                                                                                                                    <small>{{verta($request->created_at)->formatTime()}}</small>
                                                                                                                </div>
                                                                                                                <div
                                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                                <div
                                                                                                                    class="timeline-content ">
                                                                                                                    <i class="fa fa-check"></i>
                                                                                                                    <h4>ارسال درخواست
                                                                                                                        توسط {{$request->sender_type == 'co'?'شرکت' :'کارجو'}} </h4>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            @if($request->process->rejected_by_co_at)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->rejected_by_co_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->rejected_by_co_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-warning ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4>رد شده توسط
                                                                                                                            شرکت</h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                                                                                                            @if($request->process->rejected_by_std_at)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->rejected_by_std_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->rejected_by_std_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-warning ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4>رد شده توسط
                                                                                                                            کارجو</h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                
                                                                                                            @if($request->sender_type == 'std' && $request->process->accepted_by_co_at == null)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        <small>...</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-muted">
                                                                                                                        <i class="fa fa-hourglass"></i>
                                                                                                                        <h4>در انتظار
                                                                                                                            تایید توسط
                                                                                                                            شرکت </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                                                                                                            @if($request->sender_type == 'std' && $request->process->accepted_by_co_at)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->accepted_by_co_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->accepted_by_co_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4> تایید توسط
                                                                                                                            شرکت </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                
                
                                                                                                            @if($request->sender_type == 'co' && $request->process->accepted_by_std_at == null)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        <small>...</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-muted">
                                                                                                                        <i class="fa fa-hourglass"></i>
                                                                                                                        <h4>در انتظار
                                                                                                                            تایید توسط
                                                                                                                            کارجو </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                                                                                                            @if($request->sender_type == 'co' && $request->process->accepted_by_std_at)
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->accepted_by_std_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->accepted_by_std_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4> تایید توسط
                                                                                                                            کارجو </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                
                                                                                                            @if(is_null($request->process->rejected_by_uni_at)  && is_null($request->process->accepted_by_uni_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        <small>...</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-muted">
                                                                                                                        <i class="fa fa-hourglass"></i>
                                                                                                                        <h4>در انتظار
                                                                                                                            تایید
                                                                                                                            دانشگاه</h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                
                                                                                                            @if(!is_null($request->process->accepted_by_uni_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->accepted_by_uni_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->accepted_by_uni_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4>تایید توسط
                                                                                                                            دانشگاه </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                
                                                                                                            @if(!is_null($request->process->rejected_by_uni_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->rejected_by_uni_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->rejected_by_uni_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-danger ">
                                                                                                                        <i class="fa fa-times"></i>
                                                                                                                        <h4>رد شده توسط
                                                                                                                            دانشگاه </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif

                                                                                                            @if (!is_null($request->process->contract_printed_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->contract_printed_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->contract_printed_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4>تحویل قرارداد به دانشجو</h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                
                                                                                                            @if(!is_null($request->process->canceled_by_std_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->canceled_by_std_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->canceled_by_std_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-danger ">
                                                                                                                        <i class="fa fa-times"></i>
                                                                                                                        <h4>لغو توسط
                                                                                                                            کارجو </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                
                                                                                                            @if(!is_null($request->process->canceled_by_co_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->canceled_by_co_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->canceled_by_co_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content bg-danger ">
                                                                                                                        <i class="fa fa-times"></i>
                                                                                                                        <h4>لغو توسط
                                                                                                                            شرکت </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                
                                                                                                            @if(!is_null($request->process->done_at))
                                                                                                                <div
                                                                                                                    class="timeline-row">
                                                                                                                    <div
                                                                                                                        class="timeline-time">
                                                                                                                        {{verta($request->process->done_at)->formatDate()}}
                                                                                                                        <small>{{verta($request->process->done_at)->formatTime()}}</small>
                                                                                                                    </div>
                                                                                                                    <div
                                                                                                                        class="timeline-dot fb-bg"></div>
                                                                                                                    <div
                                                                                                                        class="timeline-content ">
                                                                                                                        <i class="fa fa-check"></i>
                                                                                                                        <h4>اتمام </h4>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                
                                                                                                        </div>
                                                                                                        <!-- Timeline end -->
                                                                                                    @endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="modal-footer">
                                                                            <button
                                                                                class="btn btn-light"
                                                                                data-bs-dismiss="modal">
                                                                                بستن
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-warning text-center">
                                                {{__('public.no_info')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-5">
                                   {{$printedRequests->links('pagination.panel')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
