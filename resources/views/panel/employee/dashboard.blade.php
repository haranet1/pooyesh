@extends('panel.employee.master')
@section('title','پنل کارکنان')
@section('main')
@php
    $managmentPermissions = ['createUser','unconfirmedUser','confirmedUser','importedUser','testedUser','exportAllUser'];
    $public = ['unconfirmedJobs','pooyeshRequest','hireRequest','manageRegulations','manageNotifications','signature'];
@endphp
    <div class="page-header">
        <h1 class="page-title"> داشبورد کارکنان</h1>
        @if(Session::has('success'))
            <div class="row">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد اصلی</a></li>
            </ol>
        </div>
    </div>
    @include('alert.email-alert')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">تعداد شرکت ها</h6>
                                    <h2 class="mb-0 number-font">{{$companies}}</h2>
                                    <h6>شرکت</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img class="img-fluid" src="{{asset('assets/images/icons8-company-100.png')}}"alt="company">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">تعداد کارجویان</h6>
                                    <h2 class="mb-0 number-font">{{$students}}</h2>
                                    <h6>کارجو</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img class="img-fluid" src="{{asset('assets/images/icons8-std-96.png')}}"
                                             alt="company">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">تعداد موقعیت های استخدام</h6>
                                    <h2 class="mb-0 number-font fs-5">{{$hire_positions}}</h2>
                                    <h6>موقعیت</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img class="img-fluid" src="{{asset('assets/images/icons8-hire-100.png')}}"
                                             alt="company">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">تعداد موقعیت های کارآموزی</h6>
                                    <h2 class="mb-0 number-font fs-5">{{$intern_positions}}</h2>
                                    <h6>موقعیت</h6>

                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img class="img-fluid"
                                             src="{{asset('assets/images/icons8-internship-100.png')}}" alt="company">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- personality test chart --}}
    <section>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت MBTI
                            <span class="mx-2">تعداد تست : {{$mbti_test_user}}</span>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn btn-success">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="mbtiChart"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-6">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت Enneagram
                            <span class="mx-2">تعداد تست : {{$enneagram_test_user}}</span>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn btn-primary">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="enneagramChart" ></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-6">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت GHQ
                            <span class="mx-2">تعداد تست : {{$ghq_test_user}}</span>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn text-white" style="background-color: #af46af">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="ghqChart" ></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-6">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت Neo
                            <span class="mx-2">تعداد تست : {{$neo_test_user}}</span>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn text-white" style="background-color:#eb8117; !important">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="neoChart"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-6">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت Haaland
                            <span class="mx-2">تعداد تست : {{$haaland_test_user}}</span>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn text-white" style="background-color:#d8a516; !important">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="haalandChart" ></div>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card text-white bg-info overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        <i class="fa fa-comment"></i>
                        فرم ها - آیین نامه ها
                    </div>
                    <div class="card-toolbar">
                        {{--<a class="btn btn-light" href="#"><i class="fa fa-eye"></i> مشاهده</a>--}}
                        <a  class="btn btn-light "
                           data-bs-target="#modal-files"
                           data-bs-toggle="modal"
                           href="javascript:void(0)">
                            <i class="fa fa-eye"></i> مشاهده</a>
                    </div>
                </div>
                <div class="card-body">
                   <p class="mb-5"> فرم ها و آیین نامه های مورد نیاز خود را از این قسمت مشاهده و دریافت نمایید.</p>

                    @if(count($files)> 0)
                        @foreach($files as $file)
                            <a href="{{asset($file->path)}}">
                                <h4 class="text-light border">
                                    <i class="fa fa-caret-left"></i>
                                    {{$file->name}}
                                </h4>
                            </a>
                        @endforeach
                    @endif
                </div>
                <div class="modal fade " id="modal-files">
                    <div
                        class="modal-dialog" role="document">
                        <div class="modal-content modal-content-demo">
                            <div class="modal-header">
                                <h6 class="modal-title text-primary">لیست آیین نامه ها و فرم ها</h6>
                            </div>
                            <div class="modal-body">
                                    @if(count($files)> 0)
                                        <ol class="list-group">
                                        @foreach($files as $file)
                                            <li class="list-group-item my1">
                                                <a href="{{asset($file->path)}}"><h6 class="m-0">{{$file->name}}</h6></a>
                                            </li>
                                        @endforeach
                                        </ol>
                                    @endif
                            </div>
                            <div
                                class="modal-footer">
                                <button
                                    class="btn btn-light"
                                    data-bs-dismiss="modal">
                                    بستن
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card text-white bg-danger overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        <i class="fe fe-headphones"></i>
                        درخواست های پشتیبانی
                    </div>
                    <div class="card-toolbar">
                        <a class="btn btn-light" href="{{route('employee.tickets.list')}}"><i class="fe fe-headphones"></i> درخواست های پشتیبانی</a>
                    </div>
                </div>
                <div class="card-body">
                    <p class="mb-5">آخرین درخواست پشتیبانی دریافتی را از این قسمت مشاهده نمایید.</p>

                    @if(count($tickets)> 0)
                        @foreach($tickets as $ticket)
                                <div class="d-flex border-bottom pb-2 mt-2" >
                                    <div class="me-3 notifyimg   ">
                                        <i class="fe fe-mail"></i>
                                    </div>
                                    <div class="mt-1 wd-80p">
                                        <h5 class=" text-light mb-1">
                                            موضوع:
                                            {{$ticket->subject}}
                                        </h5>
                                        <span class="notification-subtext text-light">{{verta($ticket->created_at)->formatDifference()}}</span>
                                        <span>فرستنده:</span>
                                        <span class="notification-subtext text-light"> {{$ticket->sender->name." ".$ticket->sender->family}}  </span>
                                    </div>
                                </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card   overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                       اطلاعیه ها
                    </div>
                    <div class="card-toolbar">
                        <a class="btn btn-primary " href="{{route('emp.announce.list')}}"> مشاهده همه</a>
                    </div>
                </div>
                <div class="card-body d-flex flex-column p-2" style="height: 400px">
                    <div class="card-scroll scroll-auto p-5 pr-6 overflow-auto">
                        @if(count($announcements) > 0)
                            <ol class="list-group">
                            @foreach($announcements as $announce)

                                <li class="list-group-item my-1">
                                    <i class="fa fa-caret-left"></i>
                                    <a data-bs-target="#modal-announce-{{$announce->id}}"
                                       data-bs-toggle="modal"
                                       href="javascript:void(0)">
                                        {{$announce->title}}
                                    </a>

                                </li>
                                    <div class="modal fade" id="modal-announce-{{$announce->id}}">
                                        <div
                                            class="modal-dialog" role="document">
                                            <div class="modal-content modal-content-demo">
                                                <div class="modal-header">
                                                    <h6 class="modal-title text-primary">متن اطلاعیه: {{$announce->title}}</h6>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        {{$announce->content}}
                                                    </p>
                                                    @if($announce->file)
                                                        <a download href="{{asset($announce->file->path)}}">دانلود فایل</a>
                                                    @endif
                                                </div>
                                                <div
                                                    class="modal-footer">
                                                    <button
                                                        class="btn btn-light"
                                                        data-bs-dismiss="modal">
                                                        بستن
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                            </ol>
                            @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card   overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        اخبار
                    </div>
                    <div class="card-toolbar">
                        <a class="btn btn-primary " href="#"> مشاهده همه</a>
                    </div>
                </div>
                <div class="card-body d-flex flex-column p-2" style="height: 400px">
                    <div class="card-scroll scroll-auto p-5 pr-6 overflow-auto">
                        @if(count($news) > 0)
                            <ol class="list-group">
                                @foreach($news as $news_item)
                                    <li class="list-group-item my-1">
                                        <i class="fa fa-caret-left"></i>
                                            {{$news_item->title}}
                                    </li>
                                @endforeach
                            </ol>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (Auth::user()->canAny($managmentPermissions))
        <h3>مدیریت کاربران</h3>
        <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">
            @can('createUser')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('employee.create.user')}}">
                        <div class="card text-white bg-info">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    افزودن کاربر
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan
            
            @can('unconfirmedUser')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('employee.unconfirmed-list')}}">
                        <div class="card text-white bg-info">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    کاربران تایید نشده
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

            @can('confirmedUser')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('employee.confirmed-list')}}">
                        <div class="card text-white bg-info">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    کاربران تایید شده
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

            @can('importedUser')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('employee.imported-list')}}">
                        <div class="card text-white bg-info">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    کاربران ایمپورت شده
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

            @can('testedUser')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('employee.testers-list')}}">
                        <div class="card text-white bg-info">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    کاربران تست داده
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

            @can('exportAllUser')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('employee.export.all.user.show')}}">
                        <div class="card text-white bg-info">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    خروجی تمام کاربران
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan
        </div>
    @endif
    
    @if (Auth::user()->canAny($public))
        <h3>عمومی</h3>
        <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">

            @can('unconfirmedJobs')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('employee.unconfirmed-job-positions')}}">
                        <div class="card text-white bg-primary">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    موقعیت های شغلی تایید نشده
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

            @can('pooyeshRequest')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('employee.pooyesh-reqs')}}">
                        <div class="card text-white bg-primary">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    درخواست های پویش
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

            @can('hireRequest')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('employee.hire-reqs')}}">
                        <div class="card text-white bg-primary">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    درخواست های استخدام
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

            @can('manageRegulations')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('files.index')}}">
                        <div class="card text-white bg-primary">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    فرم ها و آیین نامه ها  
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

            @can('manageNotifications')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('emp.announce.list')}}">
                        <div class="card text-white bg-primary">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    اطلاعیه ها
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

            @can('signature')
                <div class="col-md-3 col-xl-3">
                    <a href="{{route('emp.signature.create')}}">
                        <div class="card text-white bg-primary">
                            <div class="card-body text-center">
                                <h4 class="card-title border-bottom py-3">
                                    افزودن امضا
                                </h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endcan

        </div>
    @endif
    
    @can('manageEvents')
        <h3>صدرا</h3>
        <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">

            <div class="col-md-3 col-xl-3">
                <a href="{{route('employee.sadra.create')}}">
                    <div class="card text-white bg-success">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3">
                                ثبت رویداد
                            </h4>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-xl-3">
                <a href="{{route('index.sadra')}}">
                    <div class="card text-white bg-success">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3">لیست رویدادها</h4>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-xl-3">
                <a href="{{route('employee.buy-plan.index')}}">
                    <div class="card text-white bg-success">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3"> لیست پلن های ثبت نام </h4>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-xl-3">
                <a href="{{route('employee.discount.index')}}">
                    <div class="card text-white bg-success">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3">کد تخفیف</h4>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-xl-3">
                <a href="{{route('employee.sadra.report')}}">
                    <div class="card text-white bg-success">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3">گزارش</h4>
                        </div>
                    </div>
                </a>
            </div>

            {{-- <div class="col-md-3 col-xl-3">
                <a href="{{route('employee.sadra.requests')}}">
                    <div class="card text-white bg-success">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3">بررسی درخواست های ثبت نام</h4>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-xl-3">
                <a href="{{route('employee.sadra.done-requests')}}">
                    <div class="card text-white bg-success">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3">ثبت نام های نهایی شده</h4>
                        </div>
                    </div>
                </a>
            </div> --}}


        </div>
    @endcan
    
@endsection

@section('script')
    {{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> --}}
    <script src="{{ asset('assets/panel/js/verify-email.js') }}"></script>
    
    <script>
        let x = {{$mbtiCountOfType}};
        let y = JSON.parse({!!json_encode($mbtiType)!!});
        console.log(y);
            var options = {
          series: [{
          data: x,
        }],
        chart: {
            offsetX: -20,
            offsetY: 20,
            height: 350,
            type: 'bar',
            events: {
                click: function(chart, w, e) {
                // console.log(chart, w, e)
                }
            },
        },
        series: [
            {
                name: 'تعداد',
                data: x
            }
        ],
        // colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true,
            // horizontal: true,
            dataLabels:{
                position: 'center',
            }
          }
        },
        dataLabels: {
          enabled: true
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: y,
          labels: {
            style: {
            //   colors: colors,
              fontSize: '16px'
            }
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
                show: false,
            }
          }
        }],
        };

        var chart = new ApexCharts(document.querySelector("#mbtiChart"), options);
        chart.render();
    </script>

    <script>
        let enneagramCount = {{$enneagramCountOfType}};
        let enneagramType = JSON.parse({!!json_encode($enneagramType)!!});
        var options = {
            series: enneagramCount,
            chart: {
                offsetX: -30,
                offsetY: 40,
                width: 420,
                type: 'pie',
            },
            labels: enneagramType,
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        offsetX:20,
                        width: 350
                    },
                    legend: {
                    position: 'bottom'
                    }
                }
            }]
        };

        var chart = new ApexCharts(document.querySelector("#enneagramChart"), options);
        chart.render(); 
    </script>

    <script>
        let neoAvg = JSON.parse({!!json_encode($neoAvg)!!});
        let neoType = JSON.parse({!!json_encode($neoType)!!});
        var options = {
          series: [{
            name: "میانگین",
            data: neoAvg
        }],
          chart: {
          height: 350,
          type: 'line',
          
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: true
        },
        stroke: {
            colors:['#B466F2'],
          curve: 'straight'
        },
        grid: {
          row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
          },
        },
        xaxis: {
          categories: neoType,
        }
        };

        var chart = new ApexCharts(document.querySelector("#neoChart"), options);
        chart.render();
    </script>

    <script>
        
        let haalandAvg = JSON.parse({!!json_encode($haalandAvg)!!});
        let haalandType = JSON.parse({!!json_encode($haalandType)!!});
        var options = {
          series: [{
          data: haalandAvg
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: true,
          }
        },
        dataLabels: {
          enabled: true
        },
        xaxis: {
          categories: haalandType,
        }
        };

        var chart = new ApexCharts(document.querySelector("#haalandChart"), options);
        chart.render();
    </script>

    <script>
        let ghqAvg = JSON.parse({!!json_encode($ghqAvg)!!});
        let ghqType = JSON.parse({!!json_encode($ghqType)!!});
                var options = {
          series: [{
          name: 'میانگین',
          data: ghqAvg
        }],
          chart: {
          height: 350,
          type: 'bar',
        },
        plotOptions: {
          bar: {
            borderRadius: 10,
            dataLabels: {
              position: 'top', // top, center, bottom
            },
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val + "%";
          },
          offsetY: -20,
          style: {
            fontSize: '12px',
            colors: ["#304758"]
          }
        },
        
        xaxis: {
          categories: ghqType,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
        yaxis: {
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false,
          },
          labels: {
            show: false,
            formatter: function (val) {
              return val + "%";
            }
          }
        
        },
        };

        var chart = new ApexCharts(document.querySelector("#ghqChart"), options);
        chart.render();
    </script>

    

@endsection