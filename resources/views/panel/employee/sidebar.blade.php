@php
    $managmentPermissions = ['createUser','unconfirmedUser','confirmedUser','importedUser','testedUser','exportAllUser'];
    $public = ['unconfirmedJobs','pooyeshRequest','hireRequest','manageRegulations','manageNotifications','signature'];
    $userManagementRoutes = [
        Route::is('employee.create.user'),
        Route::is('index.user'),
        Route::is('index.package.user'),
        Route::is('employee.imported-list'),
        Route::is('employee.testers-list'),
        Route::is('employee.export.all.user.show')
    ];
    $pooyeshRoutes = [
        Route::is('emp.intern.pos.job'),
        Route::is('emp.intern.req.job'),
        Route::is('index.resume')
    ];
    $hireRoutes = [
        Route::is('emp.hire.pos.job'),
        Route::is('emp.hire.req.job'),
    ];
    $publicRoutes = [
        Route::is('employee.personality-tests.result-show'),
        Route::is('index.article.blog'),
        Route::is('files.index'),
        Route::is('emp.announce.list'),
        Route::is('emp.signature.create'),
    ];
    $supRoutes = [
        Route::is('inbox.ticket'),
        Route::is('co-user.rejected.sent.ticket')
    ];
    $readLogRoutes = [
        Route::is('employee.co-log-activity.list'),
        Route::is('employee.std-log-activity.list'),
        Route::is('employee.emp-log-activity.list')
    ];
    $protectionRoutes = [
        Route::is('co.list.protection'),
        Route::is('list.rejected.protection')
    ];
    $amozeshyarExpert = [
        Route::is('wait.for.review.amozeshyar.expert'),
        Route::is('course.unit.registered.amozeshyar.expert'),
    ];
    $managePermissionRoutes = [
        Route::is('employee.role-create'),
        Route::is('employee.role.edit'),
        Route::is('employee.show.role.assign'),
    ];
    $magRoutes = [
        Route::is('index.article.blog'),
        Route::is('create.article.blog'),
        Route::is('all.category.mag'),
        Route::is('admin.comments'),
    ];
    $accountingRoutes = [
        Route::is('mbti.accounting'),
        Route::is('package.accounting'),
    ];
@endphp
<ul class="side-menu">
    <li class="sub-category">
        <h3>اصلی</h3>
    </li>
    <li class="slide">
        <a class="side-menu__item has-link" data-bs-toggle="slide" href="{{route('employee.panel')}}">
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/dashboard.svg')}}" alt="داشبورد">
            <span class="side-menu__label">داشبورد</span>
        </a>
        <a class="side-menu__item has-link" target="_blank" data-bs-toggle="slide" href="{{route('home')}}">
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/home.svg')}}" alt="صفحه اصلی سایت">
            <span class="side-menu__label">صفحه اصلی سایت</span>
        </a>
        @can('manageOnet')
            <a class="side-menu__item has-link" target="_blank" data-bs-toggle="slide" href="{{ url('admin') }}">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/onet.svg')}}" alt="پنل استاندارد Onet">
                <span class="side-menu__label">پنل استاندارد Onet</span>
            </a>
        @endcan
        <a class="side-menu__item has-link" target="_blank" data-bs-toggle="slide" href="{{ route('edit.setting.user') }}">
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/setting.svg')}}" alt="تنظیمات">
            <span class="side-menu__label">تنظیمات</span>
        </a>
    </li>

    <li class="sub-category">
        <h3> امکانات</h3>
    </li>

    @if (Auth::user()->canAny($managmentPermissions))
        <li class="slide user
        @foreach ($userManagementRoutes as $userManagementRoute)
            @if (Route::currentRouteName() == $userManagementRoute)
                is-expanded
            @endif
        @endforeach
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/user-managment.svg')}}" alt="مدیریت کاربران">
                <span class="side-menu__label">مدیریت کاربران</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">

                <li class="side-menu-label1"><a href="javascript:void(0)">مدیریت کاربران</a></li>
{{--                @can('createUser')<li><a href="{{route('employee.create.user')}}"  class="slide-item {{Route::is('employee.create.user')?'active':''}}">افزودن کاربر</a></li>@endcan--}}
                <li><a href="{{route('index.user')}}"  class="slide-item {{Route::is('index.user')?'active':''}}">صفحه اصلی</a></li>
                <li><a href="{{route('index.package.user')}}"  class="slide-item {{Route::is('index.package.user')?'active':''}}">بسته های خدماتی</a></li>
                {{-- @can('unconfirmedUser')<li><a href="{{route('company.un.user')}}" class="slide-item {{Route::is('company.un.user')?'active':''}}">شرکت های تایید نشده</a></li>@endcan --}}
                {{-- @can('unconfirmedUser')<li><a href="{{route('student.un.user')}}" class="slide-item {{Route::is('student.un.user')?'active':''}}">دانشجویان تایید نشده</a></li>@endcan --}}
                {{-- @can('unconfirmedUser')<li><a href="{{route('applicant.un.user')}}" class="slide-item {{Route::is('applicant.un.user')?'active':''}}">کارجویان تایید نشده</a></li>@endcan --}}
                {{-- @can('confirmedUser')<li><a href="{{route('employee.confirmed-list')}}" class="slide-item {{Route::is('employee.confirmed-list')?'active':''}}">کاربران تایید شده</a></li>@endcan --}}
{{--                @can('importedUser')<li><a href="{{route('employee.imported-list')}}" class="slide-item {{Route::is('employee.imported-list')?'active':''}}">کاربران ایمپورت شده</a></li>@endcan--}}
{{--                @can('testedUser')<li><a href="{{route('employee.testers-list')}}" class="slide-item {{Route::is('employee.testers-list')?'active':''}}">کاربران تست داده</a></li>@endcan--}}
                @can('exportAllUser')<li><a href="{{route('employee.export.all.user.show')}}" class="slide-item {{Route::is('employee.export.all.user.show')?'active':''}}">خروجی تمام کاربران</a></li>@endcan
            </ul>
        </li>
    @endif

    @can('pooyeshRequest')
        <li class="slide
        @foreach ($pooyeshRoutes as $pooyeshRoute)
            @if (Route::currentRouteName() == $pooyeshRoute)
                is-expanded
            @endif
        @endforeach">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/intern.svg')}}" alt="پویش">
                <span class="side-menu__label">طرح پویش</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">طرح پویش</a></li>
                <li><a href="{{ route('emp.intern.pos.job') }}" class="slide-item {{ Route::is('emp.intern.pos.job')?'active':'' }}"> موقعیت‌ ها </a></li>
                <li><a href="{{ route('emp.intern.req.job') }}" class="slide-item {{ Route::is('emp.intern.req.job')?'active':'' }}"> درخواست ها </a></li>
                {{-- <li><a href="{{ route('intern.request.pooyesh') }}" class="slide-item {{ Route::is('intern.request.pooyesh')?'active':'' }}">درخواست های پویش</a></li> --}}
                <li><a href="{{ route('index.resume') }}" class="slide-item {{ Route::is('index.resume')?'active':'' }}">رزومه های ارسال شده</a></li>
            </ul>
        </li>
    @endcan

    @can('hireRequest')
        <li class="slide
        @foreach ($hireRoutes as $hireRoute)
            @if (Route::currentRouteName() == $hireRoute)
                is-expanded
            @endif
        @endforeach">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/hire.svg')}}" alt="استخدام">
                <span class="side-menu__label">استخدام</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">استخدام</a></li>
                <li><a href="{{ route('emp.hire.pos.job') }}" class="slide-item {{ Route::is('emp.hire.pos.job')?'active':'' }}">موقعیت‌ ها</a></li>
                <li><a href="{{ route('emp.hire.req.job') }}" class="slide-item {{ Route::is('emp.hire.req.job')?'active':'' }}">درخواست های استخدام</a></li>
                {{-- <li><a href="{{ route('intern.request.pooyesh') }}" class="slide-item {{ Route::is('intern.request.pooyesh')?'active':'' }}">درخواست های پویش</a></li> --}}
                {{-- <li><a href="{{ route('index.resume') }}" class="slide-item {{ Route::is('index.resume')?'active':'' }}">رزومه های ارسال شده</a></li> --}}
            </ul>
        </li>
    @endcan

    @if (Auth::user()->canAny($public))
        <li class="slide
        @foreach ($publicRoutes as $publicRoute)
            @if (Route::currentRouteName() == $publicRoute)
                is-expanded
            @endif
        @endforeach">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/public.svg')}}" alt="عمومی">
                <span class="side-menu__label">عمومی</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">

                <li class="side-menu-label1"><a href="javascript:void(0)">عمومی</a></li>
                {{-- <li><a href="{{route('employee.unconfirmed-stds')}}" class="slide-item {{Route::is('employee.unconfirmed-stds')?'active':''}}">دانشجویان تایید نشده</a></li> --}}
                {{-- <li><a href="{{route('employee.unconfirmed-companies')}}" class="slide-item {{Route::is('employee.unconfirmed-companies')?'active':''}}">شرکت های تایید نشده</a></li> --}}
                {{-- <li><a href="{{route('employee.co.list')}}" class="slide-item {{Route::is('employee.co.list')?'active':''}}">لیست شرکت ها</a></li> --}}
                {{-- <li><a href="{{route('employee.std.list')}}" class="slide-item {{Route::is('employee.std.list')?'active':''}}">لیست دانشجویان</a></li> --}}
                {{-- <li><a href="{{route('emp.graders.list')}}" class="slide-item {{Route::is('emp.graders.list')?'active':''}}">لیست دانش آموزان</a></li> --}}
                <li><a href="{{route('employee.personality-tests.result-show')}}" class="slide-item {{Route::is('employee.personality-tests.result-show')?'active':''}}">نتایج تست های شخصیتی</a></li>
                {{-- @can('unconfirmedJobs')<li><a href="{{route('employee.unconfirmed-job-positions')}}" class="slide-item {{Route::is('employee.unconfirmed-job-positions')?'active':''}}">موقعیت های شغلی تایید نشده</a></li>@endcan --}}
                {{-- @can('pooyeshRequest')<li><a href="{{route('employee.pooyesh-reqs')}}" class="slide-item {{Route::is('employee.pooyesh-reqs')?'active':''}}"> درخواست های پویش</a></li>@endcan --}}
                {{-- @can('hireRequest')<li><a href="{{route('employee.hire-reqs')}}" class="slide-item {{Route::is('employee.hire-reqs')?'active':''}}"> درخواست های استخدام</a></li>@endcan --}}
                {{-- @can('hireRequest')<li><a href="{{route('employee.std-resume-list.show')}}" class="slide-item {{Route::is('employee.std-resume-list.show')?'active':''}}"> رزومه های ارسال شده</a></li>@endcan --}}
                @can('manageRegulations')<li><a href="{{route('files.index')}}" class="slide-item {{Route::is('files.index')?'active':''}}"> فرم ها و آیین نامه ها</a></li>@endcan
                @can('manageNotifications')<li><a href="{{route('emp.announce.list')}}" class="slide-item {{Route::is('emp.announce.list')?'active':''}}"> اطلاعیه ها</a></li>@endcan
                {{-- @can('signature')<li><a href="{{route('emp.signature.create')}}" class="slide-item {{Route::is('emp.signature.create')?'active':''}}"> افزودن امضا </a></li>@endcan --}}
            </ul>
        </li>
    @endif

    @can('manageEvents')
        <li class="slide sadra ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/events.svg')}}" alt="رویداد ها">
                <span class="side-menu__label">نمایشگاه کار</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">نمایشگاه کار</a></li>
                <li><a href="{{route('create.sadra')}}" class="slide-item {{Route::is('create.sadra')?'active':''}}">ثبت صدرا</a></li>
                <li><a href="{{route('index.sadra')}}" class="slide-item {{Route::is('index.sadra')?'active':''}}">لیست  صدراها</a></li>
                <li><a href="{{route('employee.buy-plan.index')}}" class="slide-item {{Route::is('employee.buy-plan.index')?'active':''}}">لیست پلن های ثبت نام </a></li>
                <li><a href="{{route('index.discount')}}" class="slide-item {{Route::is('index.discount')?'active':''}}"> کد تخفیف </a></li>
                {{-- <li><a href="{{route('employee.sadra.report')}}" class="slide-item {{Route::is('employee.sadra.report')?'active':''}}">گزارش </a></li> --}}
                {{-- <li><a href="{{route('employee.sadra.requests')}}" class="slide-item {{Route::is('employee.sadra.requests')?'active':''}}">بررسی درخواست های ثبت نام </a></li> --}}
                {{-- <li><a href="{{route('employee.sadra.done-requests')}}" class="slide-item {{Route::is('employee.sadra.done-requests')?'active':''}}">ثبت نام های نهایی شده </a></li> --}}
            </ul>
        </li>
    @endcan

    @can('manageEvents')
        <li class="slide sadra ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/events.svg')}}" alt="رویداد ها">
                <span class="side-menu__label">رویداد ها</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">رویداد ها</a></li>
                <li><a href="{{route('create.event')}}" class="slide-item {{Route::is('create.event')?'active':''}}">ثبت رویداد</a></li>
                <li><a href="{{route('index.event')}}" class="slide-item {{Route::is('index.event')?'active':''}}">لیست  رویدادها</a></li>
            </ul>
        </li>
    @endcan

        
    @can('manageArticles')
        <li class="slide
        @foreach ($magRoutes as $magRoute)
            @if (Route::currentRouteName() == $magRoute)
                is-expanded
            @endif
        @endforeach">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/communication.png')}}" alt="مجله">
                <span class="side-menu__label">مجله</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">

                <li class="side-menu-label1"><a href="javascript:void(0)">مجله</a></li>
                <li><a href="{{route('index.article.blog')}}" class="slide-item {{Route::is('index.article.blog')?'active':''}}">مقاله ها</a></li>
                <li><a href="{{route('create.article.blog')}}" class="slide-item {{Route::is('create.article.blog')?'active':''}}">مقاله جدید</a></li>
                <li><a href="{{route('all.category.mag')}}" class="slide-item {{Route::is('all.category.mag')?'active':''}}">دسته بندی مقالات</a></li>
                <li><a href="{{route('admin.comments')}}" class="slide-item {{Route::is('admin.comments')?'active':''}}">دیدگاه ها</a></li>
            </ul>
        </li>
    @endcan


    @canany(['support' , 'protection'])
        <li class="slide supp
        @foreach ($supRoutes as $supRoute)
            @if (Route::currentRouteName() == $supRoute)
                is-expanded
            @endif
        @endforeach
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 20px" src="{{asset('assets/panel/images/icon/employee/support.svg')}}" alt="پشتیبانی">
                <span class="side-menu__label">پشتیبانی</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">پشتیبانی</a></li>
                @can('support')
                    <li><a href="{{route('inbox.ticket')}}" class="slide-item {{Route::is('inbox.ticket')?'active':''}}"> لیست تیکت ها</a></li>
                @elsecan('protection')
                    <li><a href="{{route('co-user.rejected.sent.ticket')}}" class="slide-item {{Route::is('co-user.rejected.sent.ticket')?'active':''}}"> لیست تیکت ها</a></li>
                @endcan
            </ul>
        </li>
    @endcanany

    @can('accounting')
        <li class="slide other
        @foreach ($accountingRoutes as $accountingRoute)
            @if (Route::currentRouteName() == $accountingRoute)
                is-expanded
            @endif
        @endforeach
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3 ms-2" style="width: 13px" src="{{asset('assets/panel/images/icon/employee/reports.svg')}}" alt="حسابداری">
                <span class="side-menu__label">حسابداری</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">حسابداری</a></li>
                <li><a href="{{route('mbti.accounting')}}" class="slide-item {{Route::is('mbti.accounting')?'active':''}}">تست mbti</a></li>
                <li><a href="{{route('package.accounting')}}" class="slide-item {{Route::is('package.accounting')?'active':''}}">پکیج های شرکت</a></li>
            </ul>
        </li>
    @endcan

    @can('readLog')
        <li class="slide other
        @foreach ($readLogRoutes as $readLogRoute)
            @if (Route::currentRouteName() == $readLogRoute)
                is-expanded
            @endif
        @endforeach
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3 ms-2" style="width: 13px" src="{{asset('assets/panel/images/icon/employee/reports.svg')}}" alt="گزارشات">
                <span class="side-menu__label">گزارشات</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">گزارشات</a></li>
                <li><a href="{{route('employee.co-log-activity.list')}}" class="slide-item {{Route::is('employee.co-log-activity.list')?'active':''}}"> گزارش فعالیت شرکت ها</a></li>
                <li><a href="{{route('employee.std-log-activity.list')}}" class="slide-item {{Route::is('employee.std-log-activity.list')?'active':''}}"> گزارش فعالیت کارجویان</a></li>
                <li><a href="{{route('employee.emp-log-activity.list')}}" class="slide-item {{Route::is('employee.emp-log-activity.list')?'active':''}}"> گزارش فعالیت کارکنان</a></li>
            </ul>
        </li>
    @endcan

    @can('secretariat')
        <li class="slide role-permission
        @if (Route::currentRouteName() == Route::is('show.notprinted.secretariat'))
            is-expanded
        @endif
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/print.svg')}}" alt="دبیرخانه">
                <span class="side-menu__label">دبیرخانه</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">دبیرخانه</a></li>
                <li><a href="{{route('show.notprinted.secretariat')}}" class="slide-item {{Route::is('show.notprinted.secretariat')?'active':''}}">قرارداد ها</a></li>
            </ul>
        </li>
    @endcan

    @can('amozeshyarExpert')
        <li class="slide role-permission
        @foreach ($amozeshyarExpert as $amozeshyarExper)
            @if (Route::currentRouteName() == $amozeshyarExper)
                is-expanded
            @endif
        @endforeach
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/employee/amozeshyarexpert.svg')}}" alt=" کارشناس آموزشیار ">
                <span class="side-menu__label"> کارشناس آموزشیار </span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)"> کارشناس آموزشیار </a></li>
                <li><a href="{{route('wait.for.review.amozeshyar.expert')}}" class="slide-item {{Route::is('wait.for.review.amozeshyar.expert')?'active':''}}">در انتظار اخذ واحد</a></li>
                <li><a href="{{route('course.unit.registered.amozeshyar.expert')}}" class="slide-item {{Route::is('course.unit.registered.amozeshyar.expert')?'active':''}}">ثبت نمره</a></li>
            </ul>
        </li>
    @endcan

    @can('protection')
        <li class="slide role-permission
        @foreach ($protectionRoutes as $protectionRoute)
            @if (Route::currentRouteName() == $protectionRoute)
                is-expanded
            @endif
        @endforeach
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 16px" src="{{asset('assets/panel/images/icon/employee/secure.svg')}}" alt="حراست">
                <span class="side-menu__label">حراست</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">حراست</a></li>
                <li><a href="{{route('co.list.protection')}}" class="slide-item {{Route::is('co.list.protection')?'active':''}}">لیست شرکت ها</a></li>
                <li><a href="{{route('list.rejected.protection')}}" class="slide-item {{Route::is('list.rejected.protection')?'active':''}}">شرکت های رد شده</a></li>
            </ul>
        </li>
    @endcan

    @can('managePermission')
        <li class="slide role-permission
        @foreach ($managePermissionRoutes as $managePermissionRoute)
            @if (Route::currentRouteName() == $managePermissionRoute)
                is-expanded
            @endif
        @endforeach
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 20px" src="{{asset('assets/panel/images/icon/employee/role.svg')}}" alt="نقش ها و دسترسی ها">
                <span class="side-menu__label">نقش ها و دسترسی ها</span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">نقش ها و درسترسی ها</a></li>
                <li><a href="{{route('employee.role-create')}}" class="slide-item {{Route::is('employee.role-create')?'active':''}}">تعریف نقش</a></li>
                <li><a href="{{route('employee.role.edit')}}" class="slide-item {{Route::is('employee.role.edit')?'active':''}}">ویرایش نقش</a></li>
                <li><a href="{{route('employee.show.role.assign')}}" class="slide-item {{Route::is('employee.show.role.assign')?'active':''}}">انتصاب نقش به کاربر</a></li>
            </ul>
        </li>
    @endcan
</ul>
