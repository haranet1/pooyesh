@extends('panel.employee.master')
@section('title','لیست دانش آموزان')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست دانش آموزان</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
               {{-- <input type="text" id="search" onkeyup="search()" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($students)>0)
                            <table class="table border-top table-bordered mb-0" id="table">
                                <thead>
                                <tr>
                                    <th>نام و نام خانوادگی </th>
                                    <th>موبایل</th>
                                    <th>موبایل والدین</th>
                                    <th>پایه تحصیلی</th>
                                    <th>گرایش تحصیلی</th>
                                    <th>مدرسه</th>
{{--                                    <th class="text-center">عملکردها</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $student)
                                    <tr>
                                        <td class="text-nowrap align-middle"> {{$student->name." ".$student->family}}</td>
                                        <td class="text-nowrap align-middle"> {{$student->mobile}}</td>
                                        <td class="text-nowrap align-middle"> {{$student->parent_mobile}}</td>
                                        <td class="text-nowrap align-middle"> {{$student->grade}}</td>
                                        <td class="text-nowrap align-middle"> {{$student->edu_branch}}</td>
                                        <td class="text-nowrap align-middle"> {{$student->school_id}}</td>
{{--                                        <td class="text-center align-middle">--}}
{{--                                            <div class="btn-group align-top">--}}
{{--                                                <a href="" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>--}}
{{--                                            </div>--}}
{{--                                        </td>--}}
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
               {{$students->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection

