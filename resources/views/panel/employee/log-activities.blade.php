@extends('panel.employee.master')
@section('title','لیست فعالیت ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان/ لیست فعالیت های کاربران سایت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"></a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
                <input type="text" id="search" class="form-control"
                       placeholder="جستجو با نام یا نام خانوادگی">

                <div class="input-group-text btn btn-primary" onclick="search()">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card">
                @if(Session::has('success'))
                    <div class="alert alert-success mt-2 text-center">
                        <h5>{{Session::pull('success')}}</h5>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger mt-2 text-center">
                        <h5>{{Session::pull('error')}}</h5>
                    </div>
                @endif
                <div class="card-header border-bottom-0">
                    <div class="">
                        <a href="{{route('employee.activity-log.export-excel',$model)}}" class="btn btn-success m-2">خروجی اکسل</a>

                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($logs)>0)
                            <table class="table border-top table-bordered mb-0" id="table">
                                <thead>
                                <tr>
                                    <th>تاریخ</th>
                                    <th>کاربر</th>
                                    <th>آی پی</th>
                                    <th>عنوان</th>
                                    <th>آدرس</th>
                                    <th>متود</th>
                                    <th>اطلاعات دستگاه</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($logs as $log)
                                    <tr>
                                        <td class="text-nowrap align-middle"
                                            dir="ltr"> {{verta($log->created_at)->formatJalaliDatetime()}}</td>
                                        <td class="text-nowrap align-middle"> {{$log->user->name." ".$log->user->family}}</td>
                                        <td class="text-nowrap align-middle"> {{$log->ip}}</td>
                                        <td class="text-nowrap align-middle"> {{$log->subject}}</td>
                                        <td class="text-nowrap align-middle"> {{$log->url}}</td>
                                        <td class="text-nowrap align-middle"> {{$log->method}}</td>
                                        <td class="text-nowrap align-middle"> {{$log->agent}}</td>

                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a href="javascript:void(0)" class="btn btn-danger">حذف</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$logs->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        function search() {
            var table = document.getElementById('table')
            var key = $('#search').val()

            $('#table tr').remove()
            table.innerHTML = '<tr>\n' +
                '                            <th>تاریخ</th>\n' +
                '                            <th>کاربر</th>\n' +
                '                            <th>آی پی </th>\n' +
                '                            <th>عنوان</th>\n' +
                '                            <th>آدرس</th>\n' +
                '                            <th>متود</th>\n' +
                '                            <th>اطلاعات دستگاه</th>\n' +
                '                            <th>عملکردها</th>\n' +
                '                        </tr>'


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: 'POST',
                url: '{{route('employee.ajax.search')}}',
                data: {
                    do: 'search-logs-by-user-name',
                    key: key,
                    model: '{{$model}}'
                },
                success: function (response) {
                    response.forEach(function (re) {
                        var row = table.insertRow(-1);
                        var td0 = row.insertCell(0)
                        var td1 = row.insertCell(1)
                        var td2 = row.insertCell(2)
                        var td3 = row.insertCell(3)
                        var td4 = row.insertCell(4)
                        var td5 = row.insertCell(5)
                        var td6 = row.insertCell(6)
                        var td7 = row.insertCell(7)


                        const date = new Date(re['created_at']);
                        const fmt = new Intl.DateTimeFormat("fa", {
                            year: "numeric",
                            month: "numeric",
                            day: "numeric",
                            hour: "numeric",
                            minute: "numeric",
                            second: "numeric"
                        });

                        td0.innerHTML = fmt.format(date)
                        td1.innerHTML = re['user']['name'] + " " + re['user']['family']
                        td2.innerHTML = re['ip']
                        td3.innerHTML = re['subject']
                        td4.innerHTML = re['url']
                        td5.innerHTML = re['method']
                        td6.innerHTML = re['agent']
                        td7.innerHTML = '<div class="btn-group align-top">' +
                            '  <a href="javascript:void(0)"  class="btn btn-danger">حذف</a> </div>'
                    })
                },
                error: function (data) {
                    console.log(data)
                }
            })
        }
    </script>
@endsection
