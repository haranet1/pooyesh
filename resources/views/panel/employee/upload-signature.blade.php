@extends('panel.employee.master')
@section('title','ثبت امضا')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ثبت امضا</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">افزودن امضای دانشگاه</h4>
                </div>
                @if(Session::has('success'))
                <div class="alert alert-success text-center">{{Session::pull('success')}}</div>
                @endif
                <div class="card-body">
                    <h5 class="text-primary mb-3">
                        امضای دانشگاه برای قرارگرفتن در قراردادها استفاده میشود، لطفا امضای دانشگاه را از این قسمت آپلود نمایید.
                    </h5>
                    @if(\Illuminate\Support\Facades\Auth::user()->groupable->university->signature)
                    <div>
                         <a class="btn btn-outline-warning" href="{{asset(\Illuminate\Support\Facades\Auth::user()->groupable->university->signature->path)}}">امضای کنونی</a>
                        <h6 class="my-2">برای تغییر امضای کنونی لطفا یک امضای جدید آپلود کنید.</h6>
                    </div>
                    @endif

                    <form class="form-horizontal mt-5" action="{{route('emp.signature.store')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">فایل</label>
                            <div class="col-md-9">
                                <input name="file" type="file" class="form-control" >
                                @error('file') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">آپلود</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script src="{{asset('assets/panel/js/company.js')}}"></script>
@endsection
