@extends('panel.employee.master')
@section('title','ویرایش اطلاعات شخصی')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ویرایش اطلاعات شخصی</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">اطلاعات شخصی</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('employee.profile.update')}}" method="POST">
                        @csrf
                        @method('put')
                    <div class="form-row">
                        <div class="form-group col-md-6 mb-0">
                            <div class="form-group">
                                <label for="name">نام</label>
                                <input name="name" type="text" value="{{$user->name}}" class="form-control" >
                                @error('name')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-0">
                            <div class="form-group">
                                <label for="family">نام خانوادگی</label>
                                <input name="family" type="text" value="{{$user->family}}" class="form-control" >
                                @error('family')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 mb-0">
                            <div class="form-group">
                                <label for="mobile">موبایل</label>
                                <input name="mobile" type="text" value="{{$user->mobile}}" class="form-control" >
                                @error('mobile')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-0">
                            <div class="form-group">
                                <label for="password">کلمه عبور</label>
                                <input type="password" name="password" class="form-control">
                                @error('password')<span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-footer mt-2">
                        <button type="submit" class="btn btn-primary">ثبت تغییرات</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

