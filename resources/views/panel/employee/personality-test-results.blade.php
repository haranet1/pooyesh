@extends('panel.employee.master')
@section('title','نتایج تست های شخصیت')
@section('main')
    <div class="page-header">
        
    </div>
    {{-- onchange="searchPersonalityTest()" --}}
    <div class="row mt-3">
        <div class="col-6 col-md-4 col-lg-3">
            <div class="input-group mb-5">
                <select class="form-control form-select" name="grade" id="grade">
                    <option value="0">مقطع تحصیلی</option>
                    @foreach ($gradeTypes as $grade)
                        <option value="{{$grade}}">{{$grade}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-6 col-md-4 col-lg-3">
            <div class="input-group mb-5">
                <select class="form-control form-select" name="major" id="major">
                    <option value="0">رشته تحصیلی</option>
                    @foreach ($majorTypes as $major)
                        <option value="{{$major}}">{{$major}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <section class="mt-3">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت MBTI
                            {{-- <span class="mx-2">تعداد تست : {{$mbti_test_user}}</span> --}}
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn btn-success">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="mbtiChart"></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-6">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت Enneagram
                            <span class="mx-2">تعداد تست : {{$enneagram_test_user}}</span>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn btn-primary">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="enneagramChart" ></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-6">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت GHQ
                            <span class="mx-2">تعداد تست : {{$ghq_test_user}}</span>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn text-white" style="background-color: #af46af">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="ghqChart" ></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-6">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت Neo
                            <span class="mx-2">تعداد تست : {{$neo_test_user}}</span>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn text-white" style="background-color:#eb8117; !important">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="neoChart"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-6">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <div class="card-title">
                            <i class="fa fa-area-chart"></i>
                            نمودار تست شخصیت Haaland
                            <span class="mx-2">تعداد تست : {{$haaland_test_user}}</span>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('employee.testers-list')}}" class="btn text-white" style="background-color:#d8a516; !important">
                                <i class="fa fa-eye"></i> مشاهده</a>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="min-height: 415px; !important">
                        <div id="haalandChart" ></div>
                    
                    </div>
                </div>
            </div>
        </div> --}}
    </section>
@endsection

@section('script')
    <script>
        const searchUrl = "{{route('employee.personality-tests.search')}}";
        const csrf = "{{csrf_token()}}";
    </script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <script>
        function mbtiChart(){
            let x = {{$mbtiCountOfType}};
            let y = JSON.parse({!!json_encode($mbtiType)!!});
                var options = {
                    series: [{
                        data: x,
                    }],
                    chart: {
                        offsetX: -20,
                        offsetY: 20,
                        height: 350,
                        type: 'bar',
                        events: {
                            click: function(chart, w, e) {
                            // console.log(chart, w, e)
                            }
                        },
                    },
                    series: [
                        {
                            name: 'تعداد',
                            data: x
                        }
                    ],
                    // colors: colors,
                    plotOptions: {
                    bar: {
                        columnWidth: '45%',
                        distributed: true,
                        // horizontal: true,
                        dataLabels:{
                            position: 'center',
                        }
                    }
                    },
                    dataLabels: {
                    enabled: true
                    },
                    legend: {
                    show: false
                    },
                    xaxis: {
                    categories: y,
                    labels: {
                        style: {
                        //   colors: colors,
                        fontSize: '16px'
                        }
                    }
                    },
                    responsive: [{
                    breakpoint: 480,
                    options: {
                        legend: {
                            show: false,
                        }
                    }
                    }],
                    };

            var chart = new ApexCharts(document.querySelector("#mbtiChart"), options);
            chart.render();
        }
        
    </script>
    {{-- <script>
        let enneagramCount = {{$enneagramCountOfType}};
        let enneagramType = JSON.parse({!!json_encode($enneagramType)!!});
        var options = {
            series: enneagramCount,
            chart: {
                offsetX: -30,
                offsetY: 40,
                width: 420,
                type: 'pie',
            },
            labels: enneagramType,
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        offsetX:20,
                        width: 350
                    },
                    legend: {
                    position: 'bottom'
                    }
                }
            }]
        };

        var chart = new ApexCharts(document.querySelector("#enneagramChart"), options);
        chart.render(); 
    </script>

    <script>
        let neoAvg = JSON.parse({!!json_encode($neoAvg)!!});
        let neoType = JSON.parse({!!json_encode($neoType)!!});
        var options = {
          series: [{
            name: "میانگین",
            data: neoAvg
        }],
          chart: {
          height: 350,
          type: 'line',
          
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: true
        },
        stroke: {
            colors:['#B466F2'],
          curve: 'straight'
        },
        grid: {
          row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
          },
        },
        xaxis: {
          categories: neoType,
        }
        };

        var chart = new ApexCharts(document.querySelector("#neoChart"), options);
        chart.render();
    </script>

    <script>
        
        let haalandAvg = JSON.parse({!!json_encode($haalandAvg)!!});
        let haalandType = JSON.parse({!!json_encode($haalandType)!!});
        var options = {
          series: [{
          data: haalandAvg
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: true,
          }
        },
        dataLabels: {
          enabled: true
        },
        xaxis: {
          categories: haalandType,
        }
        };

        var chart = new ApexCharts(document.querySelector("#haalandChart"), options);
        chart.render();
    </script>

    <script>
        let ghqAvg = JSON.parse({!!json_encode($ghqAvg)!!});
        let ghqType = JSON.parse({!!json_encode($ghqType)!!});
                var options = {
          series: [{
          name: 'میانگین',
          data: ghqAvg
        }],
          chart: {
          height: 350,
          type: 'bar',
        },
        plotOptions: {
          bar: {
            borderRadius: 10,
            dataLabels: {
              position: 'top', // top, center, bottom
            },
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val + "%";
          },
          offsetY: -20,
          style: {
            fontSize: '12px',
            colors: ["#304758"]
          }
        },
        
        xaxis: {
          categories: ghqType,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
        yaxis: {
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false,
          },
          labels: {
            show: false,
            formatter: function (val) {
              return val + "%";
            }
          }
        
        },
        };

        var chart = new ApexCharts(document.querySelector("#ghqChart"), options);
        chart.render();
    </script> --}}

    <script src="{{asset('assets/panel/js/personality-search.js')}}"></script>
@endsection