@extends('panel.employee.master')
@section('title' , 'ویرایش نقش')
@section('main')
    <div class="page-header">
        <h1 class="page-title">ویرایش نقش ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">تعریف نقش</a></li>
            </ol>
        </div>
    </div>
    
    <div class="row row-cards justify-content-center ">
        <div class="col-md-12 col-xl-12">
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="alert alert-danger alert-dismissible">
                            <h4><i class="icon fa fa-ban"></i>خطا !</h4>
                            @foreach($errors->all() as $error)
                                {{ $error }} <br>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <div class="card">
                {{-- <div class="card-header">
                    <h4 class="card-title">تعریف نقش</h4>
                </div> --}}
                <div class="card-body">
                    {{-- <form action="{{route('employee.role.store')}}" method="POST" class="form-horizontal">
                        @csrf
                        <div class=" row mb-4">
                            <label for="inputName" class="col-md-3 form-label">نام نقش</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="name" id="inputName" placeholder="نام" autocomplete="username">
                                @error('name') <p class="text-danger ">{{$message}}</p> @enderror
                            </div>
                        </div>
                        <div class=" row mb-4">
                            <label for="" class="col-md-3 form-label">لیست دسترسی ها</label>
                        </div>
                        <div class=" row mb-4">
                            @foreach ($permissions as $per)
                                <div class="col-12 col-md-6">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="{{'permission'.$per->id}}" value="{{$per->name}}">
                                        <span class="custom-control-label">{{$per->fa_name}}</span>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <div class="mb-0 mt-4 row">
                            <div class="col text-center">
                                <button type="submit" class="btn btn-primary w-25">ثبت</button>
                            </div>
                        </div>
                    </form> --}}
                    <table class="table border-top table-bordered mb-0 table table-striped">
                        <thead>
                            <tr>
                                <th>ردیف</th>
                                <th>نقش</th>
                                <th class="align-center">دسترسی ها</th>
                                <th>افزودن دسترسی</th>
                                <th>حذف دسترسی</th>
                            </tr>
                        </thead>
                        <tbody id="un-company-table-body">
                        @foreach($roles as $role)
                            @php
                                $rolePermissions = [];
                                foreach ($role->getPermissionNames() as $per) {
                                    array_push($rolePermissions, $per);
                                }
                            @endphp
                            <tr>
                                <td class="text-nowrap align-middle">{{$loop->index + 1}}</td>
                                <td class="text-nowrap align-middle"> {{$role->name}}</td>
                                <td class="text-nowrap align-middle">
                                    <a class="btn btn-sm btn-info"
                                        data-bs-target="#modal-{{$role->id}}"
                                        data-bs-toggle="modal"
                                        href="javascript:void(0)">
                                        مشاهده دسترسی ها</a>
                                </td>
                                <td class="text-center align-middle">
                                    <form action="{{route('employee.role.update')}}" method="POST" class="form-horizontal">
                                        @csrf
                                        <div class="row justify-content-center">
                                            <div class="col-7">
                                                <input type="hidden" name="role" value="{{$role->id}}">
                                                <select name="permission" class="form-control form-select" data-bs-placeholder="انتخاب نقش">
                                                    <option selected value="">انتخاب دسترسی</option>
                                                    @foreach ($permissions as $permission)
                                                        @if (!in_array($permission->name , $rolePermissions))
                                                            <option value="{{$permission->id}}">{{$permission->fa_name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @error('role') <p class="text-danger ">{{$message}}</p> @enderror
                                            </div>
                                            <div class="col-2">
                                                <button type="submit" class="btn btn-success px-4">ثبت</button>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                                <td class="text-center align-middle">
                                    <form action="{{route('employee.role.delete')}}" method="POST" class="form-horizontal">
                                        @csrf
                                        <div class="row justify-content-center">
                                            <div class="col-7">
                                                <input type="hidden" name="role" value="{{$role->id}}">
                                                <select name="permission" class="form-control form-select" data-bs-placeholder="انتخاب نقش">
                                                    <option selected value="">انتخاب نقش</option>
                                                    @foreach ($permissions as $permission)
                                                        @if (in_array($permission->name , $rolePermissions))
                                                            <option value="{{$permission->id}}">{{$permission->fa_name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @error('deleteRole') <p class="text-danger ">{{$message}}</p> @enderror
                                            </div>
                                            <div class="col-2">
                                                <button type="delete" class="btn btn-danger px-4">حذف</button>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            <div class="modal fade" id="modal-{{$role->id}}">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content modal-content-demo">
                                        <div class="modal-header">
                                            <h6 class="modal-title">
                                                لیست دسترسی های تعریف شده برای نقش: {{$role->name}}
                                            </h6>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card-body">
                                                @foreach(get_permission_fa_name_via_role($rolePermissions) as $per)
                                                    <div>
                                                        <span>عنوان: </span>
                                                        <span class="text-primary p-2">{{$per}}</span>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div
                                            class="modal-footer">
                                            <button
                                                class="btn btn-light"
                                                data-bs-dismiss="modal">
                                                بستن
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{-- @dd('hi') --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection