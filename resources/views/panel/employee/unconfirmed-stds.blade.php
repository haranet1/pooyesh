@extends('panel.employee.master')
@section('title','دانشجویان تایید نشده')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">دانشجویان تایید نشده</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                   {{-- <h2 class="card-title">1 - 30 از 546 دانشجو</h2> --}}
                    <div class="">
                        <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedUsers()">تایید دانشجویان انتخاب شده</button>

                        {{-- <select class="form-control select2 w-100">
                            <option value="asc">آخرین</option>
                            <option value="desc">قدیمی ترین</option>
                        </select> --}}
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($students) > 0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th title="انتخاب همه"><input id="selectAll" type="checkbox" onchange="selectAll()"></th>
                                <th>نام و نام خانوادگی </th>
                                <th>تلفن</th>
                                <th>سن</th>
                                <th>دانشگاه</th>
                                <th>رشته</th>
                                <th>شهر</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $std)
                            <tr>
                                <td><input type="checkbox" id="{{$std->id}}" class="checkbox" onclick="checkUncheck(this)"></td>
                                <td class="text-nowrap align-middle">{{$std->name." ".$std->family}}</td>
                                <td class="text-nowrap align-middle">{{$std->mobile}}</td>
                                <td class="text-nowrap align-middle">{{$std->groupable->age}}</td>
                                <td class="text-nowrap align-middle">{{$std->groupable->university->name}}</td>
                                <td class="text-nowrap align-middle">{{$std->groupable->major}}</td>
                                @if ($std->groupable->city)
                                    <td class="text-nowrap align-middle">{{$std->groupable->city->name}}</td>
                                @endif
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="{{route('candidate.single',$std->id)}}" class="btn btn-sm btn-primary badge" type="button">پروفایل </a>
                                        <a href="javascript:void(0)" onclick="confirmUser({{$std->id}})" class="btn btn-sm btn-success badge verify-std" type="button">تایید </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="mb-5">
                {{$students->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $('.checkbox').on('change',function (){
            if ($(this).is(':checked')) {
                users.push(this.id)
            } else {
                for (let i=0; i<users.length; i++){
                    if (users[i] === this.id)
                        users.splice(i,1)
                }
            }
        })
    </script>
    @endsection
