@extends('panel.employee.master')
@section('title','ویرایش کد تخفیف')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ویرایش کد تخفیف</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ویرایش کد تخفیف</h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('employee.discount.update',$discount)}}" method="post" >
                        @csrf
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">عنوان</label>
                            <div class="col-md-9">
                                <input name="name" value="{{$discount->name}}" type="text" class="form-control" >
                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">مقدار</label>
                            <div class="col-md-9">
                                <input name="value" value="{{$discount->value}}" type="text" class="form-control" >
                                @error('value') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">نوع</label>
                            <div class="col-md-9">
                                <select class="form-select form-control" name="is_percent" id="">
                                    <option @if($discount->is_percent) selected @endif class="form-control" value="1">درصد</option>
                                    <option  @if(!$discount->is_percent) selected @endif class="form-control" value="0">ریال</option>
                                </select>
                                @error('is_percent') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ شروع </label>
                            <div class="col-md-9">
                                <input name="begin" value="{{str_replace('-','/',verta($discount->begin)->formatDate())}}" type="text" id="start" class="form-control" >
                                @error('begin') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">تاریخ پایان</label>
                            <div class="col-md-9">
                                <input name="end" value="{{str_replace('-','/',verta($discount->end)->formatDate())}}" type="text" id="end" class="form-control" >
                                @error('end') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">توضیحات</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description" id="" cols="30" rows="10">{{$discount->description}}</textarea>
                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $('#start').persianDatepicker({
            altField: '#start',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
        $('#end').persianDatepicker({
            altField: '#end',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
    </script>
@endsection
