@extends('panel.employee.master')
@section('title','افزودن اطلاعیه جدید')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> افزودن اطلاعیه جدید</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ثبت اطلاعیه جدید</h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('emp.announce.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">عنوان</label>
                            <div class="col-md-9">
                                <input name="title" type="text" class="form-control" >
                                @error('title') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email"> فایل</label>
                            <div class="col-md-9">
                                <input name="file" type="file"  class="form-control" >
                                @error('file') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">متن</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="content" id="content" cols="30" rows="10"></textarea>
                                @error('content') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
@endsection

