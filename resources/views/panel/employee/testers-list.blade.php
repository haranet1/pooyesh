@extends('panel.employee.master')
@section('title','لیست شرکت ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست کاربرانی که تست شخصیت دادند</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست کاربرانی که تست شخصیت دادند (تایید نشده ها)</h3>
                </div>
                <div class="card-body">
                    <div class="card-pay">
                        <ul class="tabs-menu nav">
                            <li class=""><a href="#tab20" class="active" data-bs-toggle="tab">تست MBTI</a></li>
                            <li><a href="#tab21" data-bs-toggle="tab" class="">تست اینیاگرام</a></li>
                        </ul>
                        <div class="tab-content">
                            {{-- mbti list --}}
                            <div class="tab-pane active show" id="tab20">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        <div class="col-10">
                                            <div class="input-group">
                                                <input type="text" id="test_mbti_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="test_mbti_family_search" class="form-control" placeholder="نام خانوادگی :" style="border-radius: 0px;">
                                                <input type="text" id="test_mbti_mobile_search" class="form-control" placeholder="تلفن همراه :" style="border-radius: 0px;">
                                                {{-- <input type="text" id="test_mbti_province_search" class="form-control" placeholder="استان :" style="border-radius: 0px;"> --}}
                                                <select id="test_mbti_sex_search" class="form-control" style="border-radius: 0px;">
                                                    <option value="0">جنسیت :</option>
                                                    <option value="male">مرد</option>
                                                    <option value="female">زن</option>
                                                </select>
                                                <select id="test_mbti_role_search" class="form-control" style="border-radius: 0px;">
                                                    <option value="0">نقش :</option>
                                                    <option value="company">شرکت</option>
                                                    <option value="applicant">کارجو</option>
                                                    <option value="student">دانشجو</option>
                                                </select>
                                                <div class="input-group-prepend">
                                                    <div onclick="testerMbtiSearch()" id="test_mbti_btn_search" class="btn btn-info form-control" style="border-top-right-radius: 0px;border-bottom-right-radius: 0px;">جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <a href="{{route('employee.test.mbti.export-excel')}}" class="btn btn-secondary">خروجی اکسل</a>
                                        </div>
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$mbtiUsers->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg">
                                        @if(count($mbtiUsers)>0)
                                            <table class="table border-top table-bordered mb-0 table table-striped" id="table">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>نام و نام خانوادگی </th>
                                                    <th>تلفن</th>
{{--                                                    <th>جنسیت</th>--}}
                                                    <th>ایمیل</th>
                                                    <th>نقش</th>
                                                    <th>نتیجه تست</th>
                                                    {{-- <th>سن</th> --}}
                                                    {{-- <th>دانشگاه</th> --}}
                                                    {{-- <th>رشته</th> --}}
                                                    {{-- <th>شهر</th> --}}
                                                    <th class="text-center">عملکردها</th>
                                                </tr>
                                                </thead>
                                                <tbody id="test-mbti-table-body">
                                                @php
                                                    $counter = (($mbtiUsers->currentPage() -1) * $mbtiUsers->perPage()) + 1;
                                                @endphp
                                                @foreach($mbtiUsers as $mbtiUser)
                                                    <tr>
                                                        <td class="text-nowrap align-middle"> {{$counter++}}</td>
                                                        <td class="text-nowrap align-middle">{{$mbtiUser->name." ".$mbtiUser->family}}</td>
                                                        <td class="text-nowrap align-middle">{{$mbtiUser->mobile}}</td>
                                                       {{-- @if ($mbtiUser->sex)
                                                            <td class="text-nowrap align-middle">{{change_sex_to_fa($mbtiUser->sex)}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">ثبت نشده</td>
                                                        @endif--}}
                                                        @if ($mbtiUser->email)
                                                            <td class="text-nowrap align-middle">{{$mbtiUser->email}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">ثبت نشده</td>
                                                        @endif
                                                        <td class="text-nowrap align-middle">{{user_has_role_fa($mbtiUser)}}</td>
                                                        @if ($mbtiUser->mbti_test)
                                                            <td class="text-nowrap align-middle">{{$mbtiUser->mbti_test->personality_type}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">نتیجه ثبت نشده</td>
                                                        @endif
                                                        <td class="text-center align-middle">
                                                            <div class="btn-group align-top">
                                                                {{-- <a href="{{route('candidate.single',$mbtiUser->id)}}" class="btn btn-sm btn-primary badge" type="button">پروفایل </a> --}}
                                                                <div class="btn-group">
                                                                    <a href="{{route('candidate.single',$mbtiUser->id)}}" class="btn btn-sm btn-success d-flex align-items-center">مشاهده پروفایل</a>
                                                                    {{-- <button type="button" onclick="confirmUser({{$mbtiUser->id}})" class="btn btn-success">تایید</button> --}}
                                                                    <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                                                      <span class="visually-hidden">Toggle Dropdown</span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a class="dropdown-item" onclick="assignToCompany({{$mbtiUser->id}})" href="javascript:void(0)">انتصاب به نقش شرکت</a></li>
                                                                        <li><a class="dropdown-item" onclick="assignToApplicant({{$mbtiUser->id}})" href="javascript:void(0)">انتصاب به نقش کارجو</a></li>
                                                                        <li><a class="dropdown-item" onclick="assignToStudent({{$mbtiUser->id}})" href="javascript:void(0)">انتصاب به نقش دانشجو</a></li>
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-warning text-center">
                                                {{__('public.no_info')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-5">
                                   {{$mbtiUsers->links('pagination.panel')}}
                                </div>
                            </div>
                            {{-- enneagram list --}}
                            <div class="tab-pane" id="tab21">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        <div class="col-10">
                                            <div class="input-group">
                                                <input type="text" id="test_enne_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="test_enne_family_search" class="form-control" placeholder="نام خانوادگی :" style="border-radius: 0px;">
                                                <input type="text" id="test_enne_mobile_search" class="form-control" placeholder="تلفن همراه :" style="border-radius: 0px;">
                                                {{-- <input type="text" id="test_mbti_province_search" class="form-control" placeholder="استان :" style="border-radius: 0px;"> --}}
                                                <select id="test_enne_sex_search" class="form-control" style="border-radius: 0px;">
                                                    <option value="0">جنسیت :</option>
                                                    <option value="male">مرد</option>
                                                    <option value="female">زن</option>
                                                </select>
                                                <select id="test_enne_role_search" class="form-control" style="border-radius: 0px;">
                                                    <option value="0">نقش :</option>
                                                    <option value="company">شرکت</option>
                                                    <option value="applicant">کارجو</option>
                                                    <option value="student">دانشجو</option>
                                                </select>
                                                <div class="input-group-prepend">
                                                    <div onclick="testerEnnagramSearch()" id="test_enne_btn_search" class="btn btn-info form-control" style="border-top-right-radius: 0px;border-bottom-right-radius: 0px;">جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <a href="{{route('employee.test.enneagram.export-excel')}}" class="btn btn-secondary">خروجی اکسل</a>
                                        </div>
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$enneagramUsers->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg">
                                        @if(count($enneagramUsers) > 0)
                                            <table class="table border-top table-bordered mb-0 table table-striped" id="table">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>نام و نام خانوادگی </th>
                                                    <th>تلفن</th>
{{--                                                    <th>جنسیت</th>--}}
                                                    <th>ایمیل</th>
                                                    <th>نقش</th>
                                                    <th>نتیجه تست</th>
                                                    {{-- <th>سن</th> --}}
                                                    {{-- <th>دانشگاه</th> --}}
                                                    {{-- <th>رشته</th> --}}
                                                    {{-- <th>شهر</th> --}}
                                                    <th class="text-center">عملکردها</th>
                                                </tr>
                                                </thead>
                                                <tbody id="test-enneagram-table-body">
                                                @php
                                                    $counter = (($enneagramUsers->currentPage() -1) * $enneagramUsers->perPage()) + 1;
                                                @endphp
                                                @foreach($enneagramUsers as $enneagramUser)
                                                    <tr>
                                                        <td class="text-nowrap align-middle"> {{$counter++}}</td>
                                                        <td class="text-nowrap align-middle">{{$enneagramUser->name." ".$enneagramUser->family}}</td>
                                                        <td class="text-nowrap align-middle">{{$enneagramUser->mobile}}</td>
                                                       {{-- @if ($enneagramUser->sex)
                                                            <td class="text-nowrap align-middle">{{change_sex_to_fa($enneagramUser->sex)}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">ثبت نشده</td>
                                                        @endif--}}
                                                        @if ($enneagramUser->email)
                                                            <td class="text-nowrap align-middle">{{$enneagramUser->email}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">ثبت نشده</td>
                                                        @endif
                                                        <td class="text-nowrap align-middle">{{user_has_role_fa($enneagramUser)}}</td>
                                                        @if ($enneagramUser->enneagram_test)
                                                            <td class="text-nowrap align-middle">{{enneagram_result_fa($enneagramUser->enneagram_test->main_character)}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">نتیجه ثبت نشده</td>
                                                        @endif
                                                        <td class="text-center align-middle">
                                                            <div class="btn-group align-top">
                                                                <div class="btn-group">
                                                                    <button type="button" onclick="confirmUser({{$enneagramUser->id}})" class="btn btn-success">تایید</button>
                                                                    <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                                                      <span class="visually-hidden">Toggle Dropdown</span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a class="dropdown-item" onclick="assignToCompany({{$enneagramUser->id}})" href="javascript:void(0)">انتصاب به نقش شرکت</a></li>
                                                                        <li><a class="dropdown-item" onclick="assignToApplicant({{$enneagramUser->id}})" href="javascript:void(0)">انتصاب به نقش کارجو</a></li>
                                                                        <li><a class="dropdown-item" onclick="assignToStudent({{$enneagramUser->id}})" href="javascript:void(0)">انتصاب به نقش دانشجو</a></li>
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-warning text-center">
                                                {{__('public.no_info')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-5">
                                   {{$enneagramUsers->links('pagination.panel')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                        <a href="{{route('employee.company.export-excel')}}" class="btn btn-success">خروجی اکسل</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($companies)>0)
                            <table class="table border-top table-bordered mb-0" id="table">
                                <thead>
                                <tr>
                                    <th>نام شرکت</th>
                                    <th>نام ثبت کننده شرکت در سامانه</th>
                                    <th>مدیر عامل</th>
                                    <th>حوزه کاری</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $company)
                                    <tr>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->name}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->name." ".$company->family}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->ceo}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->activity_field}}</td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a href="{{route('company.single',$company->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div> --}}
            {{-- <div class="mb-5">
               {{$companies->links('pagination.panel')}}
            </div> --}}
        </div>

    </div>
@endsection
@section('script')
    <script>
        function search() {
            var table = document.getElementById('table')
            var key = $('#search').val()

            $('#table tr').remove()
            table.innerHTML = '<tr>\n' +
                '                            <th>نام شرکت</th>\n' +
                '                            <th>نام ثبت کننده شرکت در سامانه</th>\n' +
                '                            <th>مدیرعامل </th>\n' +
                '                            <th>حوزه کاری</th>\n' +
                '                            <th>عملکردها</th>\n' +
                '                        </tr>'


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: 'POST',
                url: '{{route('employee.ajax.search')}}',
                data: {
                    do: 'search-companies',
                    key: key
                },
                success: function (response) {
                    response['data'].forEach(function (re) {
                        var route = "{{route('company.single',['id' => ':id'])}}"
                        route = route.replace(':id', re['id'])
                        var row = table.insertRow(-1);
                        var td0 = row.insertCell(0)
                        var td1 = row.insertCell(1)
                        var td2 = row.insertCell(2)
                        var td3 = row.insertCell(3)
                        var td4 = row.insertCell(4)
                        td0.innerHTML = re['groupable']['name']
                        td1.innerHTML = re['name']
                        td2.innerHTML = re['groupable']['ceo']
                        td3.innerHTML = re['groupable']['activity_field']
                        td4.innerHTML = '<div class="btn-group align-top">' +
                            ' <a href="' + route + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل شرکت </a> </div>'
                    })
                },
                error: function (data) {
                    console.log(data)
                }
            })
        }
    </script>
@endsection
