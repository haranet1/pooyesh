@extends('panel.employee.master')
@section('title','ثبت دانشجو/شرکت')
@section('main')
    <style>
        .product-variants {
            margin: 0px 0 10px;
            color: #6f6f6f;
            font-size: 13px;
            line-height: 1.692;
        }
        ul.js-product-variants {
            margin: 20px 7px;
            display: inline-block;
            vertical-align: middle;
        }
        ul.js-product-variants li {
            margin: 0 8px 0 0;
            display: inline-block;
        }

        .product-variant>span {
            font-size: inherit;
            color: inherit;
            padding-left: 15px;
            margin-top: 3px;
            float: right;
        }

        .product-variants {
            margin-right: -8px;
            list-style: none;
            padding: 0;
            display: inline-block;
            margin-bottom: 0;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }



        .ui-variant input[type=radio] {
            visibility: hidden;
            position: absolute;
        }

        .ui-variant--check {
            cursor: pointer;
            border: 1px solid transparent;
            border-radius: 10px;
            color: #6f6f6f;
            padding: 3px 10px;
            font-size: 13px;
            font-size: .929rem;
            line-height: 1.692;
            display: block;
            -webkit-box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
            box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
        }

        .ui-variant--color .ui-variant--check {
            padding-right: 37px;
        }

        input[type=radio]:checked+.ui-variant--check {
            border-color: #2c90f6;
            background-color: #2c91f8;
            color: white;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .ui-variant--color .ui-variant-shape {
            width: 18px;
            height: 18px;
            position: absolute;
            right: 8px;
            top: 8px;
            border-radius: 50%;
            content: "";
            cursor: pointer;
        }

    </style>
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">ثبت دانشجو/شرکت</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center ">
        <div class="col-lg-8 col-xl-8 ">
            <div class="card p-5">
                <div class="panel panel-primary">
                    @if(Session::has('success'))
                        <div class="alert alert-success text-center">
                            {{Session::pull('success')}}
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger text-center">
                            {{Session::pull('error')}}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="row">
                            <div class="col-md-8 col-md-offset-1">
                                <div class="alert alert-danger alert-dismissible">
                                    <h4><i class="icon fa fa-ban"></i>خطا !</h4>
                                    @foreach($errors->all() as $error)
                                        {{ $error }} <br>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="tab-menu-heading tab-menu-heading-boxed">
                        <div class="tabs-menu-boxed">

                            <ul class="nav panel-tabs">
                                <li><a href="#tab25" class="active" data-bs-toggle="tab">ثبت دستی</a></li>
                                <li><a href="#tab26" data-bs-toggle="tab" class="">ثبت یکجا با فایل اکسل</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab25">
                                <div class="wrap-login100 p-6">
                                    <form action="{{route('employee.store.user')}}" method="POST" class="login100-form validate-form">
                                        @csrf
                                        @error('name') <small class="text-danger ">{{$message}}</small> @enderror
                                        <div class="wrap-input100 validate-input input-group">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-account" aria-hidden="true"></i>
                                            </a>
                                            <input name="name" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="نام">
                                        </div>

                                        @error('family') <small class="text-danger ">{{$message}}</small> @enderror
                                        <div class="wrap-input100 validate-input input-group">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-account" aria-hidden="true"></i>
                                            </a>
                                            <input name="family" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder="نام خانوادگی">
                                        </div>

                                        @error('sex') <small class="text-danger ">{{$message}}</small> @enderror
                                        <div class="wrap-input100 validate-input input-group">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-account" aria-hidden="true"></i>
                                            </a>
                                            <select name="sex" class="form-control form-select select2" >
                                                <option label="جنسیت">جنسیت</option>
                                                <option value="male">مرد</option>
                                                <option value="female">زن</option>
                                            </select>
                                        </div>

                                        @error('email') <small class="text-danger ">{{$message}}</small> @enderror
                                        <div class="wrap-input100 validate-input input-group">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-email" aria-hidden="true"></i>
                                            </a>
                                            <input name="email" class="input100 border-start-0 ms-0 form-control" type="email"
                                                   placeholder=" ایمیل">
                                        </div>

                                        @error('mobile') <small class="text-danger ">{{$message}}</small> @enderror
                                        <div class="wrap-input100 validate-input input-group">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-phone" aria-hidden="true"></i>
                                            </a>
                                            <input name="mobile" class="input100 border-start-0 ms-0 form-control" type="text"
                                                   placeholder=" موبایل">
                                        </div>

                                        @error('password') <small class="text-danger ">{{$message}}</small> @enderror
                                        <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="zmdi zmdi-eye" aria-hidden="true"></i>
                                            </a>
                                            <input name="password" class="input100 border-start-0 ms-0 form-control" type="password"
                                                   placeholder="کلمه عبور">
                                        </div>

                                        @error('role') <small class="text-danger ">{{$message}}</small> @enderror
                                        <div class="wrap-input100 validate-input input-group">
                                            <div class="product-variants">
                                                <span>نوع کاربری: </span>
                                                <ul class="js-product-variants">
                                                    <li class="ui-variant">
                                                        <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                            <input type="radio" value="employee" name="role"class="variant-selector">
                                                            <span class="ui-variant--check">کارمند</span>
                                                        </label>
                                                    </li>
                                                    <li class="ui-variant">
                                                        <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-building"></i>
                                                            </span>
                                                            <input type="radio" value="company" name="role"class="variant-selector">
                                                            <span class="ui-variant--check">شرکت</span>
                                                        </label>
                                                    </li>
                                                    <li class="ui-variant">
                                                        <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                            <input type="radio" value="student" name="role"class="variant-selector">
                                                            <span class="ui-variant--check">دانشجو</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="container-login100-form-btn">
                                            <button type="submit" class="login100-form-btn btn-primary">ثبت</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab26">
                                <div class="col-12  ">
                                    <div class="card text-white bg-success mb-3">
                                        <div class="card-header">راهنما</div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <h5 class="text-white"> توضیحات</h5>
                                                    <p>
                                                        فایل نمونه را دانلود کرده و خانه های آن را طبق نمونه کامل
                                                        نمایید، سپس فایل خود را در محل مشخص شده آپلود نمایید و دکمه
                                                        ثبت  را بزنید.
                                                    </p>
                                                    <p>
                                                        دقت داشته باشید که فایل شما باید سر ستونهایی دقیقا مشابه
                                                        فایل نمونه داشته باشد.
                                                    </p>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    <h5 class="text-white"> راهنمای سر ستون ها</h5>
                                                    <ul class="list-unstyled text-center ">
                                                        <li>name: نام کاربر را در این ستون وارد کنید</li>
                                                        <li>family: نام خانوادگی کاربر را در این ستون وارد کنید</li>
                                                        <li>mobile: شماره موبایل کاربر را در این ستون وارد کنید</li>
                                                        <li>password: کلمه عبور کاربر را در این ستون وارد کنید</li>
                                                    </ul>
                                                    <a class="btn btn-light my-2"
                                                       href="{{asset('files/users.xlsx')}}" download>دانلود
                                                        فایل نمونه</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <form class="form-group text-center mt-3"
                                          action="{{route('employee.store.user.excel')}}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="wrap-input100 validate-input input-group">
                                            <div class="row product-variants w-100 aligh-items-center">
                                                <div class="col-12 d-flex justify-content-start">
                                                    <span class="align-self-center">نوع کاربران: </span>
                                                    <ul class="js-product-variants">
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-building"></i>
                                                                </span>
                                                                <input type="radio" value="company" name="role"
                                                                    class="variant-selector">
                                                                <span class="ui-variant--check">شرکت</span>
                                                            </label>
                                                        </li>
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-user"></i>
                                                                </span>
                                                                <input type="radio" value="student" name="role"class="variant-selector">
                                                                <span class="ui-variant--check">کارجو</span>
                                                            </label>
                                                        </li>
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-user"></i>
                                                                </span>
                                                                <input type="radio" value="student" name="role"class="variant-selector">
                                                                <span class="ui-variant--check">دانشجو</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-12 d-flex justify-content-start">
                                                    <span class="align-self-center">ارسال پیامک اطلاع رسانی (SMS):</span>
                                                    <ul class="js-product-variants">
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                                <input type="radio" value="1" name="sms"
                                                                    class="variant-selector">
                                                                <span class="ui-variant--check">ارسال شود</span>
                                                            </label>
                                                        </li>
                                                        <li class="ui-variant">
                                                            <label class="ui-variant ui-variant--color">
                                                                <span class="ui-variant-shape">
                                                                    <i class="fa fa-times"></i>
                                                                </span>
                                                                <input type="radio" value="0" name="sms"class="variant-selector">
                                                                <span class="ui-variant--check">ارسال نشود</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="file" name="users" class="form-control" required
                                               oninvalid="this.setCustomValidity('لطفا یک فایل انتخاب کنید')"
                                               oninput="setCustomValidity('')">
                                        <br>
                                        <button class="btn btn-success">ثبت </button>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
