@extends('panel.employee.master')
@section('title','ثبت دانشجو/شرکت')
@section('main')
    <style>
        .product-variants {
            margin: 0px 0 10px;
            color: #6f6f6f;
            font-size: 13px;
            line-height: 1.692;
        }
        ul.js-product-variants {
            margin: 20px 7px;
            display: inline-block;
            vertical-align: middle;
        }
        ul.js-product-variants li {
            margin: 0 8px 0 0;
            display: inline-block;
        }

        .product-variant>span {
            font-size: inherit;
            color: inherit;
            padding-left: 15px;
            margin-top: 3px;
            float: right;
        }

        .product-variants {
            margin-right: -8px;
            list-style: none;
            padding: 0;
            display: inline-block;
            margin-bottom: 0;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }



        .ui-variant input[type=radio] {
            visibility: hidden;
            position: absolute;
        }

        .ui-variant--check {
            cursor: pointer;
            border: 1px solid transparent;
            border-radius: 10px;
            color: #6f6f6f;
            padding: 3px 10px;
            font-size: 13px;
            font-size: .929rem;
            line-height: 1.692;
            display: block;
            -webkit-box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
            box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
        }

        .ui-variant--color .ui-variant--check {
            padding-right: 37px;
        }

        input[type=radio]:checked+.ui-variant--check {
            border-color: #2c90f6;
            background-color: #2c91f8;
            color: white;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .ui-variant--color .ui-variant-shape {
            width: 18px;
            height: 18px;
            position: absolute;
            right: 8px;
            top: 8px;
            border-radius: 50%;
            content: "";
            cursor: pointer;
        }

    </style>
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">ثبت دانشجو/شرکت</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center ">
        <div class="col-lg-8 col-xl-8 ">
            <div class="card p-5">
                <div class="panel panel-primary">
                    @if(Session::has('success'))
                        <div class="alert alert-success text-center">
                            {{Session::pull('success')}}
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger text-center">
                            {{Session::pull('error')}}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="row">
                            <div class="col-md-8 col-md-offset-1">
                                <div class="alert alert-danger alert-dismissible">
                                    <h4><i class="icon fa fa-ban"></i>خطا !</h4>
                                    @foreach($errors->all() as $error)
                                        {{ $error }} <br>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="col-12  ">
                    <h3>اضافه کردن کاربران خارجی</h3>
                    {{-- <div class="card text-white bg-success mb-3">
                        <div class="card-header">راهنما</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <h5 class="text-white"> توضیحات</h5>
                                    <p>
                                        فایل نمونه را دانلود کرده و خانه های آن را طبق نمونه کامل
                                        نمایید، سپس فایل خود را در محل مشخص شده آپلود نمایید و دکمه
                                        ثبت  را بزنید.
                                    </p>
                                    <p>
                                        دقت داشته باشید که فایل شما باید سر ستونهایی دقیقا مشابه
                                        فایل نمونه داشته باشد.
                                    </p>
                                </div>
                                <div class="col-12 col-md-6">
                                    <h5 class="text-white"> راهنمای سر ستون ها</h5>
                                    <ul class="list-unstyled text-center ">
                                        <li>name: نام کاربر را در این ستون وارد کنید</li>
                                        <li>family: نام خانوادگی کاربر را در این ستون وارد کنید</li>
                                        <li>mobile: شماره موبایل کاربر را در این ستون وارد کنید</li>
                                        <li>password: کلمه عبور کاربر را در این ستون وارد کنید</li>
                                    </ul>
                                    <a class="btn btn-light my-2"
                                       href="{{asset('files/users.xlsx')}}" download>دانلود
                                        فایل نمونه</a>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                    <form class="form-group text-center mt-3"
                          action="{{route('employee.import.excel.extra-user.store')}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="wrap-input100 validate-input input-group">
                            <div class="row product-variants w-100 aligh-items-center">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <input type="file" name="users" class="form-control" required
                               oninvalid="this.setCustomValidity('لطفا یک فایل انتخاب کنید')"
                               oninput="setCustomValidity('')">
                                <br>
                            </div>
                            <div class="col-4">
                                <button class="btn btn-success">ثبت </button>
                            </div>
                        </div>
                    </form>
                    <div class="row mt-5">
                        <div class="col-3">
                            <a href="{{route('employee.import.excel.extra-user.update-ncode-user')}}" class="btn btn-info">بروزرسانی کد ملی</a>
                        </div>
                        <div class="col-3">
                            <a href="{{route('employee.import.excel.extra-user.update-major-user')}}" class="btn btn-primary">بروزرسانی رشته</a>
                        </div>
                        <div class="col-3">
                            <a href="{{route('employee.import.excel.extra-user.update-grade-user')}}" class="btn btn-secondary">بروزرسانی مقطع</a>
                        </div>
                        <div class="col-3">
                            <a href="{{route('employee.import.excel.extra-user.export-unregistered-students')}}" class="btn btn-dark">خروجی دانشجویان ثبت نشده</a>
                        </div>
                    </div>
                </div>

                
            </div>
        </div>
    </div>
@endsection
