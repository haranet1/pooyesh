@extends('panel.employee.master')
@section('title','تعریف نقش')
@section('main')
    
    <div class="page-header">
        <h1 class="page-title">نقش ها و دسترسی ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">تعریف نقش</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center ">
        <div class="col-md-12 col-xl-9">
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="alert alert-danger alert-dismissible">
                            <h4><i class="icon fa fa-ban"></i>خطا !</h4>
                            @foreach($errors->all() as $error)
                                {{ $error }} <br>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">تعریف نقش</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('employee.role.store')}}" method="POST" class="form-horizontal">
                        @csrf
                        <div class=" row mb-4">
                            <label for="inputName" class="col-md-3 form-label">نام نقش</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="name" id="inputName" placeholder="نام" autocomplete="username">
                                @error('name') <p class="text-danger ">{{$message}}</p> @enderror
                            </div>
                        </div>
                        <div class=" row mb-4">
                            <label for="" class="col-md-3 form-label">لیست دسترسی ها</label>
                        </div>
                        <div class=" row mb-4">
                            @foreach ($permissions as $per)
                                <div class="col-12 col-md-6">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="{{'permission'.$per->id}}" value="{{$per->name}}">
                                        <span class="custom-control-label">{{$per->fa_name}}</span>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <div class="mb-0 mt-4 row">
                            <div class="col text-center">
                                <button type="submit" class="btn btn-primary w-25">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection