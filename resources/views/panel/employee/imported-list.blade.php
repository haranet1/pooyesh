@extends('panel.employee.master')
@section('title','لیست شرکت ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست کاربران ایمپورت شده</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            {{-- <div class="input-group w-25 mb-5">
                <input type="text" id="search" onkeyup="search()" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div> --}}
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست کاربران ایمپورت شده</h3>
                </div>
                <div class="card-body">
                    <div class="card-pay">
                        <ul class="tabs-menu nav">
                            <li class=""><a href="#tab20" class="active" data-bs-toggle="tab">شرکت ها</a></li>
                            <li><a href="#tab21" data-bs-toggle="tab" class="">کارجویان</a></li>
                            <li><a href="#tab22" data-bs-toggle="tab" class="">دانشجویان</a></li>
                        </ul>
                        <div class="tab-content">
                            {{-- company list --}}
                            <div class="tab-pane active show" id="tab20">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        <div class="col-10">
                                            <div class="input-group">
                                                <input type="text" id="im_co_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="im_co_family_search" class="form-control" placeholder="نام خانوادگی :" style="border-radius: 0px;">
                                                <input type="text" id="im_co_mobile_search" class="form-control" placeholder="تلفن همراه :" style="border-radius: 0px;">
                                                {{-- <input type="text" id="im_co_province_search" class="form-control" placeholder="استان :" style="border-radius: 0px;"> --}}
                                                {{-- <input type="text" id="im_co_coname_search" class="form-control" placeholder="اسم شرکت :" style="border-radius: 0px;"> --}}
                                                {{-- <input type="text" id="im_co_workfield_search" class="form-control" placeholder="حوضه کاری :" style="border-radius: 0px;"> --}}
                                                <div class="input-group-prepend">
                                                    <div onclick="importedCompanySearch()" id="im_co_btn_search" class="btn btn-info form-control" style="border-top-right-radius: 0px;border-bottom-right-radius: 0px;">جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <a href="{{route('employee.imported.company.export-excel')}}" class="btn btn-secondary">خروجی اکسل</a>
                                        </div>
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$companies->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg">
                                        @if(count($companies)>0)
                                            <table class="table border-top table-bordered mb-0 table table-striped" id="table">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>نام ثبت کننده شرکت در سامانه</th>
                                                    {{-- <th>مدیر عامل</th> --}}
                                                    {{-- <th>حوزه کاری</th> --}}
                                                    <th>تلفن همراه</th>
                                                    {{-- <th class="text-center">عملکردها</th> --}}
                                                </tr>
                                                </thead>
                                                <tbody id="im_company-table-body">
                                                @php
                                                    $counter = (($companies->currentPage() -1) * $companies->perPage()) + 1;
                                                @endphp
                                                @foreach($companies as $company)
                                                    <tr>
                                                        <td class="text-nowrap align-middle"> {{$counter++}}</td>
                                                        <td class="text-nowrap align-middle"> {{$company->name." ".$company->family}}</td>
                                                        {{-- <td class="text-nowrap align-middle"> {{$company->groupable->ceo}}</td> --}}
                                                        {{-- <td class="text-nowrap align-middle"> {{$company->groupable->activity_field}}</td> --}}
                                                        <td class="text-nowrap align-middle"> {{$company->mobile}}</td>
                                                        {{-- <td class="text-center align-middle">
                                                            <div class="btn-group align-top">
                                                                <a href="{{route('company.single',$company->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>
                                                            </div>
                                                        </td> --}}
                                                    </tr>
                                                @endforeach
                
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-warning text-center">
                                                {{__('public.no_info')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-5">
                                   {{$companies->links('pagination.panel')}}
                                </div>
                            </div>
                            {{-- applicant list --}}
                            <div class="tab-pane" id="tab21">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        <div class="col-10">
                                            <div class="input-group">
                                                <input type="text" id="im_app_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="im_app_family_search" class="form-control" placeholder="نام خانوادگی :" style="border-radius: 0px;">
                                                <input type="text" id="im_app_mobile_search" class="form-control" placeholder="تلفن همراه :" style="border-radius: 0px;">
                                                <div class="input-group-prepend">
                                                    <div onclick="importedApplicantSearch()" id="im_app_btn_search" class="btn btn-info form-control" style="border-top-right-radius: 0px;border-bottom-right-radius: 0px;">جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <a href="{{route('employee.imported.applicant.export-excel')}}" class="btn btn-secondary">خروجی اکسل</a>
                                        </div>
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$applicants->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg">
                                        @if(count($applicants) > 0)
                                            <table class="table border-top table-bordered mb-0 table table-striped" id="table">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>نام و نام خانوادگی </th>
                                                    <th>تلفن</th>
                                                    <th>جنسیت</th>
                                                    <th>ایمیل</th>
                                                    {{-- <th>سن</th> --}}
                                                    {{-- <th>دانشگاه</th> --}}
                                                    {{-- <th>رشته</th> --}}
                                                    {{-- <th>شهر</th> --}}
                                                    {{-- <th class="text-center">عملکردها</th> --}}
                                                </tr>
                                                </thead>
                                                <tbody id="im_applicant-table-body">
                                                @php
                                                    $counter = (($applicants->currentPage() -1) * $applicants->perPage()) + 1;
                                                @endphp
                                                @foreach($applicants as $applicant)
                                                    <tr>
                                                        <td class="text-nowrap align-middle"> {{$counter++}}</td>
                                                        <td class="text-nowrap align-middle">{{$applicant->name." ".$applicant->family}}</td>
                                                        <td class="text-nowrap align-middle">{{$applicant->mobile}}</td>
                                                        @if ($applicant->sex)
                                                            <td class="text-nowrap align-middle">{{$applicant->sex}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">ثبت نشده</td>
                                                        @endif
                                                        @if ($applicant->email)
                                                            <td class="text-nowrap align-middle">{{$applicant->email}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">ثبت نشده</td>
                                                        @endif
                                                        {{-- <td class="text-nowrap align-middle">{{$applicant->groupable->age}}</td> --}}
                                                        {{-- @if ($applicant->groupable->university) --}}
                                                            {{-- <td class="text-nowrap align-middle">{{$applicant->groupable->university->name}}</td> --}}
                                                        {{-- @else --}}
                                                            {{-- <td class="text-nowrap align-middle">دانشگاه ثبت نشده</td> --}}
                                                        {{-- @endif --}}
                                                        {{-- <td class="text-nowrap align-middle">{{$applicant->groupable->major}}</td> --}}
                                                        {{-- @if ($applicant->groupable->city) --}}
                                                            {{-- <td class="text-nowrap align-middle">{{$applicant->groupable->city->name}}</td> --}}
                                                        {{-- @else --}}
                                                            {{-- <td class="text-nowrap align-middle">شهر ثبت نشده</td> --}}
                                                        {{-- @endif --}}
                                                        {{-- <td class="text-center align-middle">
                                                            <div class="btn-group align-top">
                                                                <a href="{{route('candidate.single',$applicant->id)}}" class="btn btn-sm btn-primary badge" type="button">پروفایل </a>
                                                            </div>
                                                        </td> --}}
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-warning text-center">
                                                {{__('public.no_info')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-5">
                                   {{$applicants->links('pagination.panel')}}
                                </div>
                            </div>
                            {{-- student list --}}
                            <div class="tab-pane" id="tab22">
                                <div class="e-table px-5 pb-5">
                                    <div class="row w-100 mb-5">
                                        <div class="col-10">
                                            <div class="input-group">
                                                <input type="text" id="im_std_name_search" class="form-control" placeholder="نام :">
                                                <input type="text" id="im_std_family_search" class="form-control" placeholder="نام خانوادگی :" style="border-radius: 0px;">
                                                <input type="text" id="im_std_mobile_search" class="form-control" placeholder="تلفن همراه :" style="border-radius: 0px;">
                                                <div class="input-group-prepend">
                                                    <div onclick="importedStudentSearch()" id="im_std_btn_search" class="btn btn-info form-control" style="border-top-right-radius: 0px;border-bottom-right-radius: 0px;">جستجو</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <a href="{{route('employee.imported.std.export-excel')}}" class="btn btn-secondary">خروجی اکسل</a>
                                        </div>
                                    </div>
                                    <div class="row w-100 mb-2">
                                        <span>تعداد کل : {{$students->total()}}</span>
                                    </div>
                                    <div class="table-responsive table-lg">
                                        @if(count($students) > 0)
                                            <table class="table border-top table-bordered mb-0 table table-striped" id="table">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>نام و نام خانوادگی </th>
                                                    <th>تلفن</th>
                                                    <th>جنسیت</th>
                                                    <th>ایمیل</th>
                                                    {{-- <th>سن</th>
                                                    <th>دانشگاه</th>
                                                    <th>رشته</th>
                                                    <th>شهر</th>
                                                    <th class="text-center">عملکردها</th> --}}
                                                </tr>
                                                </thead>
                                                <tbody id="im_student-table-body">
                                                @php
                                                    $counter = (($students->currentPage() -1) * $students->perPage()) + 1;
                                                @endphp
                                                @foreach($students as $std)
                                                    <tr>
                                                        <td class="text-nowrap align-middle"> {{$counter++}}</td>
                                                        <td class="text-nowrap align-middle">{{$std->name." ".$std->family}}</td>
                                                        <td class="text-nowrap align-middle">{{$std->mobile}}</td>
                                                        @if ($std->sex)
                                                            <td class="text-nowrap align-middle">{{$std->sex}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">تایین نشده</td>
                                                        @endif
                                                        @if ($std->email)
                                                            <td class="text-nowrap align-middle">{{$std->email}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">تایین نشده</td>
                                                        @endif
                                                        {{-- <td class="text-nowrap align-middle">{{$std->groupable->age}}</td>
                                                        @if ($std->groupable->university)
                                                            <td class="text-nowrap align-middle">{{$std->groupable->university->name}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">دانشگاه تایین نشده</td>
                                                        @endif
                                                        <td class="text-nowrap align-middle">{{$std->groupable->major}}</td>
                                                        @if ($std->groupable->city)
                                                            <td class="text-nowrap align-middle">{{$std->groupable->city->name}}</td>
                                                        @else
                                                            <td class="text-nowrap align-middle">شهر تایین نشده</td>
                                                        @endif
                                                        <td class="text-center align-middle">
                                                            <div class="btn-group align-top">
                                                                <a href="{{route('candidate.single',$std->id)}}" class="btn btn-sm btn-primary badge" type="button">پروفایل </a>
                                                            </div>
                                                        </td> --}}
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-warning text-center">
                                                {{__('public.no_info')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-5">
                                   {{$students->links('pagination.panel')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                        <a href="{{route('employee.company.export-excel')}}" class="btn btn-success">خروجی اکسل</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($companies)>0)
                            <table class="table border-top table-bordered mb-0" id="table">
                                <thead>
                                <tr>
                                    <th>نام شرکت</th>
                                    <th>نام ثبت کننده شرکت در سامانه</th>
                                    <th>مدیر عامل</th>
                                    <th>حوزه کاری</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $company)
                                    <tr>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->name}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->name." ".$company->family}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->ceo}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->activity_field}}</td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a href="{{route('company.single',$company->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div> --}}
            {{-- <div class="mb-5">
               {{$companies->links('pagination.panel')}}
            </div> --}}
        </div>

    </div>
@endsection
@section('script')
    <script>
        let im_companySingle = "{{route('company.single',':id')}}";
        let im_appSingle = "{{route('candidate.single',':id')}}";
        let im_stdSingle = "{{route('candidate.single',':id')}}";
    </script>
    <script src="{{asset('assets/panel/js/employee.js')}}"></script>
    <script>
        function search() {
            var table = document.getElementById('table')
            var key = $('#search').val()

            $('#table tr').remove()
            table.innerHTML = '<tr>\n' +
                '                            <th>نام شرکت</th>\n' +
                '                            <th>نام ثبت کننده شرکت در سامانه</th>\n' +
                '                            <th>مدیرعامل </th>\n' +
                '                            <th>حوزه کاری</th>\n' +
                '                            <th>عملکردها</th>\n' +
                '                        </tr>'


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: 'POST',
                url: '{{route('employee.ajax.search')}}',
                data: {
                    do: 'search-companies',
                    key: key
                },
                success: function (response) {
                    response['data'].forEach(function (re) {
                        var route = "{{route('company.single',['id' => ':id'])}}"
                        route = route.replace(':id', re['id'])
                        var row = table.insertRow(-1);
                        var td0 = row.insertCell(0)
                        var td1 = row.insertCell(1)
                        var td2 = row.insertCell(2)
                        var td3 = row.insertCell(3)
                        var td4 = row.insertCell(4)
                        td0.innerHTML = re['groupable']['name']
                        td1.innerHTML = re['name']
                        td2.innerHTML = re['groupable']['ceo']
                        td3.innerHTML = re['groupable']['activity_field']
                        td4.innerHTML = '<div class="btn-group align-top">' +
                            ' <a href="' + route + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل شرکت </a> </div>'
                    })
                },
                error: function (data) {
                    console.log(data)
                }
            })
        }
    </script>
@endsection
