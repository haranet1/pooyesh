@extends('panel.employee.master')
@section('title','تعریف فایل جدید')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> تعریف فایل جدید</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ثبت فایل جدید</h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('files.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">عنوان</label>
                            <div class="col-md-9">
                                <input name="name" type="text" class="form-control" >
                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4 justify-content-center">
                            <label class="form-label" for="example-email">قابل دانلود توسط کارجویان</label>
                            <div class=" align-items-center ">
                                <div class="material-switch ">
                                    <input id="std_can_dl" name="std_can_dl" type="checkbox">
                                    <label for="std_can_dl" class="label-success"></label>
                                </div>
                                @error('std_can_dl') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4 justify-content-center">
                            <label class=" form-label" for="example-email"> قابل دانلود توسط شرکت ها</label>
                            <div class="">
                                <div class="material-switch ">
                                    <input id="co_can_dl" name="co_can_dl" type="checkbox">
                                    <label for="co_can_dl" class="label-success"></label>
                                </div>
                                @error('file') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email"> فایل</label>
                            <div class="col-md-9">
                                <input name="file" type="file"  class="form-control" >
                                @error('file') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
@endsection

