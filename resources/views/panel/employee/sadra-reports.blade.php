@extends('panel.employee.master')
@section('title','گزارش صدرا')
@section('main')
    <div class="page-header">
        <h1 class="page-title"> داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">گزارش ثبت نامی های صدرا</a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
            <div class="row">
                @if(count($events)>0)
                    @foreach($events as $sadra => $groupedEvents)
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3" >
                                <div class="card m-b-20">
                                    <div class="card-header">
                                        <h4 class="card-title">{{$sadra}}</h4>
                                        <div class="card-options">
                                            <a href="javascript:void(0)" class="card-options-collapse" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body card-scroll scroll-auto overflow-hidden overflow-y-scroll" style="height: 400px">
                                        <div class=" ">
                                            @foreach($groupedEvents as $event)
                                                <div class="media pb-1 align-items-center my-2 border-bottom">
                                                    <img class="avatar brround avatar-md me-3" alt="avatra-img" src="{{asset('assets/front/images/company-logo-1.png')}}">
                                                    <div class="media-body">
                                                        <a href="{{route('company.single',$event->company->id)}}" class="text-default fw-semibold">{{$event->company->groupable->name}}</a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <h6>تعداد کل ثبت نام ها: {{count($groupedEvents)}}</h6>
                                        <a href="{{route('employee.exhibition.export-excel',$event->sadra->id)}}" class="btn btn-success">خروجی اکسل</a>

                                    </div>
                                </div>
                            </div>
                    @endforeach

                @else
                    <div class="alert alert-warning text-center">
                        {{__('public.no_info')}}
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
