@extends('panel.employee.master')
@section('title','شرکت های تایید نشده')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">شرکت های تایید نشده</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">

            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                        <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedUsers()">تایید شرکت‌های انتخاب شده</button>

                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($companies)>0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th title="انتخاب همه"><input id="selectAll" type="checkbox" onchange="selectAll()"></th>
                                <th class="text-center">عکس</th>
                                <th>نام شرکت</th>
                                <th>نام ثبت کننده شرکت در سامانه</th>
                                <th>مدیر عامل</th>
                                <th>حوزه کاری</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($companies as $company)
                            <tr>
                                <td><input type="checkbox" id="{{$company->id}}" class="checkbox" onclick="checkUncheck(this)"></td>
                                <td class="align-middle text-center">
                                    @if($company->logo)
                                    <img alt="image" class="avatar avatar-md br-7" src="{{asset($company->logo->path)}}">
                                    @else
                                        <img alt="image" class="avatar avatar-md br-7" src="{{asset('assets/front/images/company-logo-1.png')}}">
                                    @endif
                                </td>
                                <td class="text-nowrap align-middle"> {{$company->groupable->name}}</td>
                                <td class="text-nowrap align-middle"> {{$company->name." ".$company->family}}</td>
                                <td class="text-nowrap align-middle"> {{$company->groupable->ceo}}</td>
                                <td class="text-nowrap align-middle"> {{$company->groupable->activity_field}}</td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="{{route('company.single',$company->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>
                                        <a href="javascript:void(0)" class="btn btn-sm btn-success badge" type="button" onclick="confirmUser({{$company->id}})">تایید </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$companies->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
