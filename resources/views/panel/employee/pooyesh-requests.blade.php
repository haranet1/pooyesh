@extends('panel.employee.master')
@section('title','لیست درخواست ها')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/timeline-style.css')}}"/>
@endsection
@section('main')

    <div class="page-header">
        <h1 class="page-title">لیست درخواست ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست درخواست ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group  mb-5">
                <form class="d-flex" action="{{route('emp.pooyesh-requests.search')}}" method="POST">
                    @csrf
                    <input type="text" name="sender" class="form-control" placeholder="جستجو با نام فرستنده">
                    <button class="btn btn-primary mx-3" type="submit"><i class="icon-magnifier"></i></button>
                </form>

                <form class="d-flex" action="{{route('emp.pooyesh-requests.search')}}" method="POST">
                    @csrf
                    <input type="text" name="receiver" class="form-control" placeholder="جستجو با نام گیرنده">
                    <button class="btn btn-primary mx-3" type="submit"><i class="icon-magnifier"></i></button>
                </form>
            </div>
            <div class="card py-1">
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg justify-content-center text-center">
                        @if(count($requests)>0)
                            <table class="table border-top table-bordered mb-0" id="table">
                                <thead>
                                <tr>
                                    <th>فرستنده</th>
                                    <th>گیرنده</th>
                                    <th>وضعیت اولیه</th>
                                    <th>تاریخ</th>

                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                        <td class="text-nowrap align-middle">
                                            @if($request->sender_type == 'co')
                                                <span class="badge bg-warning">شرکت</span>
                                                {{$request->sender->groupable->name}}
                                            @elseif($request->sender_type == 'std')
                                                <span class="badge bg-info">دانشجو</span>
                                                {{$request->sender->name." ".$request->sender->family}}
                                            @elseif ($request->sender_type == 'apl')
                                                <span class="badge bg-info">کارجو</span>
                                                {{$request->sender->name." ".$request->sender->family}}
                                            @endif
                                        </td>
                                        <td class="text-nowrap align-middle">
                                            @if($request->receiver_type == 'co')
                                                <span class="badge bg-warning">شرکت</span>
                                                {{$request->receiver->groupable->name}}
                                            @elseif($request->receiver_type == 'std')
                                                <span class="badge bg-info">دانشجو</span>
                                                {{$request->receiver->name." ".$request->receiver->family}}
                                            @elseif ($request->receiver_type == 'apl')
                                                <span class="badge bg-info">کارجو</span>
                                                {{$request->receiver->name." ".$request->receiver->family}}
                                            @endif
                                        </td>
                                        <td class="text-nowrap align-middle">
                                            @if(is_null($request->status))
                                                <span class="badge bg-primary"> در انتظار بررسی توسط شرکت</span>
                                            @else
                                                @if($request->status == 1)
                                                    <span class="badge bg-success">قبول شده</span>
                                                @elseif($request->status == 0)
                                                    <span class="badge bg-danger"> رد شده </span>
                                                @endif
                                            @endif
                                        </td>
                                        <td class="text-nowrap align-middle"> {{verta($request->created_at)->formatDate()}}</td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                @if($request->process)
                                                    @if(!str_contains(getPooyeshProcessStatus($request->process->id),'لغو'))
                                                        @if(is_null($request->process->accepted_by_uni_at) && is_null($request->process->rejected_by_uni_at))
                                                            @if($request->status == 1)
                                                                <a class="btn btn-sm btn-success"
                                                                   onclick="acceptPooyeshReq({{$request->id}})"
                                                                   href="javascript:void(0)">تایید
                                                                    و ارسال قرارداد</a>
                                                                <a class="btn btn-sm btn-danger"
                                                                   onclick="rejectPooyeshReq({{$request->id}})"
                                                                   href="javascript:void(0)">رد
                                                                    کردن </a>
                                                            @else
                                                                <a class="btn btn-sm btn-success disabled"
                                                                   href="javascript:void(0)">تایید و ارسال قرارداد</a>
                                                                <a class="btn btn-sm btn-danger disabled"
                                                                   href="javascript:void(0)">رد کردن </a>
                                                            @endif
                                                        @elseif(is_null($request->process->rejected_by_uni_at) && $request->process->done_at)
                                                            <a class="send-certificate btn btn-sm btn-success"
                                                               onclick="sendPooyeshCertificateToStd({{$request->id}})"
                                                               href="javascript:void(0)">تایید نهایی و ارسال گواهی</a>
                                                        @endif
                                                    @endif
                                                @endif
                                            </div>

                                            <div class="btn-group align-top">
                                                <a @if($request->status == 1) class="btn btn-sm btn-primary"
                                                   data-bs-target="#modal-{{$request->id}}"
                                                   data-bs-toggle="modal"
                                                   href="javascript:void(0)"
                                                   @else class="disabled btn btn-sm btn-primary" @endif>
                                                    روند درخواست</a>

                                            </div>
                                        </td>
                                        <div class="modal fade "
                                             id="modal-{{$request->id}}">
                                            <div
                                                class="modal-dialog modal-lg"
                                                role="document">
                                                <div
                                                    class="modal-content modal-content-demo">
                                                    <div
                                                        class="modal-header">
                                                        <h6 class="modal-title">
                                                            روند پویش
                                                            کارجو {{$request->sender->name." ".$request->sender->family}}</h6>
                                                        <button
                                                            aria-label="Close"
                                                            class="btn-close"
                                                            data-bs-dismiss="modal">
                                                            <span
                                                                aria-hidden="true ">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div
                                                            class="card-body">
                                                            <div class="vtimeline">
                                                                <div class="row gutters">
                                                                    <div
                                                                        class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                            @if($request->process)
                                                                                <!-- Timeline start -->
                                                                                    <div class="timeline">
                                                                                        <div class="timeline-row">
                                                                                            <div
                                                                                                class="timeline-time">
                                                                                                {{verta($request->created_at)->formatDate()}}
                                                                                                <small>{{verta($request->created_at)->formatTime()}}</small>
                                                                                            </div>
                                                                                            <div
                                                                                                class="timeline-dot fb-bg"></div>
                                                                                            <div
                                                                                                class="timeline-content ">
                                                                                                <i class="fa fa-check"></i>
                                                                                                <h4>ارسال درخواست
                                                                                                    توسط {{$request->sender_type == 'co'?'شرکت' :'کارجو'}} </h4>
                                                                                            </div>
                                                                                        </div>
                                                                                        @if($request->process->rejected_by_co_at)
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    {{verta($request->process->rejected_by_co_at)->formatDate()}}
                                                                                                    <small>{{verta($request->process->rejected_by_co_at)->formatTime()}}</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-warning ">
                                                                                                    <i class="fa fa-check"></i>
                                                                                                    <h4>رد شده توسط
                                                                                                        شرکت</h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif
                                                                                        @if($request->process->rejected_by_std_at)
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    {{verta($request->process->rejected_by_std_at)->formatDate()}}
                                                                                                    <small>{{verta($request->process->rejected_by_std_at)->formatTime()}}</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-warning ">
                                                                                                    <i class="fa fa-check"></i>
                                                                                                    <h4>رد شده توسط
                                                                                                        کارجو</h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                        @if($request->sender_type == 'std' && $request->process->accepted_by_co_at == null)
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    <small>...</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-muted">
                                                                                                    <i class="fa fa-hourglass"></i>
                                                                                                    <h4>در انتظار
                                                                                                        تایید توسط
                                                                                                        شرکت </h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif
                                                                                        @if($request->sender_type == 'std' && $request->process->accepted_by_co_at)
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    {{verta($request->process->accepted_by_co_at)->formatDate()}}
                                                                                                    <small>{{verta($request->process->accepted_by_co_at)->formatTime()}}</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content ">
                                                                                                    <i class="fa fa-check"></i>
                                                                                                    <h4> تایید توسط
                                                                                                        شرکت </h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif


                                                                                        @if($request->sender_type == 'co' && $request->process->accepted_by_std_at == null)
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    <small>...</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-muted">
                                                                                                    <i class="fa fa-hourglass"></i>
                                                                                                    <h4>در انتظار
                                                                                                        تایید توسط
                                                                                                        کارجو </h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif
                                                                                        @if($request->sender_type == 'co' && $request->process->accepted_by_std_at)
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    {{verta($request->process->accepted_by_std_at)->formatDate()}}
                                                                                                    <small>{{verta($request->process->accepted_by_std_at)->formatTime()}}</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content ">
                                                                                                    <i class="fa fa-check"></i>
                                                                                                    <h4> تایید توسط
                                                                                                        کارجو </h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                        @if(is_null($request->process->rejected_by_uni_at)  && is_null($request->process->accepted_by_uni_at))
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    <small>...</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-muted">
                                                                                                    <i class="fa fa-hourglass"></i>
                                                                                                    <h4>در انتظار
                                                                                                        تایید
                                                                                                        دانشگاه</h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                        @if(!is_null($request->process->accepted_by_uni_at))
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    {{verta($request->process->accepted_by_uni_at)->formatDate()}}
                                                                                                    <small>{{verta($request->process->accepted_by_uni_at)->formatTime()}}</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content ">
                                                                                                    <i class="fa fa-check"></i>
                                                                                                    <h4>تایید توسط
                                                                                                        دانشگاه </h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                        @if(!is_null($request->process->rejected_by_uni_at))
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    {{verta($request->process->rejected_by_uni_at)->formatDate()}}
                                                                                                    <small>{{verta($request->process->rejected_by_uni_at)->formatTime()}}</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-danger ">
                                                                                                    <i class="fa fa-times"></i>
                                                                                                    <h4>رد شده توسط
                                                                                                        دانشگاه </h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                        @if (!is_null($request->process->accepted_by_uni_at) && is_null($request->process->rejected_by_uni_at) && is_null($request->process->contract_printed_at))
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    <small>...</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-muted">
                                                                                                    <i class="fa fa-hourglass"></i>
                                                                                                    <h4>مراجعه دانشجو به دبیرخانه</h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                        @if (!is_null($request->process->contract_printed_at))
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    <small>...</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-muted">
                                                                                                    <i class="fa fa-hourglass"></i>
                                                                                                    <h4>تحویل قرارداد به دانشجو</h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                        @if(!is_null($request->process->canceled_by_std_at))
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    {{verta($request->process->canceled_by_std_at)->formatDate()}}
                                                                                                    <small>{{verta($request->process->canceled_by_std_at)->formatTime()}}</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-danger ">
                                                                                                    <i class="fa fa-times"></i>
                                                                                                    <h4>لغو توسط
                                                                                                        کارجو </h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                        @if(!is_null($request->process->canceled_by_co_at))
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    {{verta($request->process->canceled_by_co_at)->formatDate()}}
                                                                                                    <small>{{verta($request->process->canceled_by_co_at)->formatTime()}}</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content bg-danger ">
                                                                                                    <i class="fa fa-times"></i>
                                                                                                    <h4>لغو توسط
                                                                                                        شرکت </h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                        @if(!is_null($request->process->done_at))
                                                                                            <div
                                                                                                class="timeline-row">
                                                                                                <div
                                                                                                    class="timeline-time">
                                                                                                    {{verta($request->process->done_at)->formatDate()}}
                                                                                                    <small>{{verta($request->process->done_at)->formatTime()}}</small>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="timeline-dot fb-bg"></div>
                                                                                                <div
                                                                                                    class="timeline-content ">
                                                                                                    <i class="fa fa-check"></i>
                                                                                                    <h4>اتمام </h4>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                    </div>
                                                                                    <!-- Timeline end -->
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="modal-footer">
                                                        <button
                                                            class="btn btn-light"
                                                            data-bs-dismiss="modal">
                                                            بستن
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center mt-3 py-2 ">
                                <h5>{{__('public.no_info')}}</h5>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection


