@extends('panel.employee.master')
@section('title','موقعیت های شغلی تایید نشده')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">موقعیت های شغلی تایید نشده</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
{{--                    <h2 class="card-title">1 - 30 از 546 دانشجو</h2>--}}
                    <div class="">
                        <button class="btn btn-success" id="confirm_all" onclick="confirmSelectedJobPositions()">تایید موقعیت های شغلی انتخاب شده</button>

                        {{--                        <select class="form-control select2 w-100">--}}
{{--                            <option value="asc">آخرین</option>--}}
{{--                            <option value="desc">قدیمی ترین</option>--}}
{{--                        </select>--}}
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($job_positions) > 0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th title="انتخاب همه"><input id="selectAll" type="checkbox" onchange="selectAll()"></th>
                                <th>عنوان</th>
                                <th>شرکت</th>
                                <th>محل</th>
                                <th>نوع</th>
                                <th>حقوق</th>
                                <th>تجربه</th>
                                <th>سطح</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($job_positions as $job_position)
                            <tr>
                                <td><input type="checkbox" id="{{$job_position->id}}" class="checkbox" onclick="checkUncheck(this)"></td>
                                <td class="text-nowrap align-middle">{{$job_position->title}}</td>
                                <td class="text-nowrap align-middle">{{$job_position->company->groupable->name}}</td>
                                <td class="text-nowrap align-middle">{{$job_position->location}}</td>
                                <td class="text-nowrap align-middle">{{$job_position->type}}</td>
                                <td class="text-nowrap align-middle">{{$job_position->salary}}</td>
                                <td class="text-nowrap align-middle">{{$job_position->experience}}</td>
                                <td class="text-nowrap align-middle">{{$job_position->level}}</td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="{{route('job',$job_position->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده </a>
                                        <a href="javascript:void(0)" onclick="confirmJobPosition({{$job_position->id}})" class="btn btn-sm btn-success badge verify-std" type="button">تایید </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="mb-5">
                {{$job_positions->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $('.checkbox').on('change',function (){
            if ($(this).is(':checked')) {
                users.push(this.id)
            } else {
                for (let i=0; i<users.length; i++){
                    if (users[i] === this.id)
                        users.splice(i,1)
                }
            }
        })
    </script>
    @endsection
