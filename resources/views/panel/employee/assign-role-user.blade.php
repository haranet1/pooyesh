@extends('panel.employee.master')
@section('title','تعریف نقش')
@section('main')
    
    <div class="page-header">
        <h1 class="page-title">نقش ها و دسترسی ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">تعریف نقش</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center ">
        <div class="col-md-12 col-xl-12">
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="alert alert-danger alert-dismissible">
                            <h4><i class="icon fa fa-ban"></i>خطا !</h4>
                            @foreach($errors->all() as $error)
                                {{ $error }} <br>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">تعریف نقش</h4>
                </div>
                <div class="card-body">
                    <table class="table border-top table-bordered mb-0 table table-striped">
                        <thead>
                        <tr>
                            <th>ردیف</th>
                            <th>نام کاربر</th>
                            <th>نقش ها</th>
                            <th>افزودن نقش</th>
                            <th>حذف نقش</th>
                        </tr>
                        </thead>
                        <tbody id="un-company-table-body">
                        @foreach($users as $user)
                        <tr>
                            <td class="text-nowrap align-middle">{{$loop->index + 1}}</td>
                            <td class="text-nowrap align-middle"> {{$user->name." ".$user->family}}</td>
                            <td class="text-nowrap align-middle">
                                {{get_role_names($user->getRoleNames())}}
                            </td>
                            <td class="text-center align-middle">
                                <form action="{{route('employee.role.assign')}}" method="POST" class="form-horizontal">
                                    @csrf
                                    <div class="row justify-content-center">
                                        <div class="col-7">
                                            <input type="hidden" name="user" value="{{$user->id}}">
                                            <select name="role" class="form-control form-select" data-bs-placeholder="انتخاب نقش">
                                                <option selected value="">انتخاب نقش</option>
                                                {{-- @foreach ($roles as $key => $value)
                                                    @if (!in_array($value , $user->getRoleNames()->toArray()))
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endif
                                                @endforeach --}}
                                                @foreach ($roles as $key => $value)
                                                    @if (!$user->roles->contains($key))
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @error('role') <p class="text-danger ">{{$message}}</p> @enderror
                                        </div>
                                        <div class="col-2">
                                            <button type="submit" class="btn btn-success px-4">ثبت</button>
                                        </div>
                                    </div>
                                </form>
                            </td>
                            <td class="text-center align-middle">
                                <form action="{{route('employee.role.remove')}}" method="POST" class="form-horizontal">
                                    @csrf
                                    <div class="row justify-content-center">
                                        <div class="col-7">
                                            <input type="hidden" name="user" value="{{$user->id}}">
                                            <select name="deleteRole" class="form-control form-select" data-bs-placeholder="انتخاب نقش">
                                                <option selected value="">انتخاب نقش</option>
                                                @foreach ($user->getRoleNames() as $role)
                                                    <option value="{{$role}}">{{$role}}</option>                  
                                                @endforeach
                                                {{-- @foreach ($roles as $key => $value)
                                                    @if (!$user->roles->contains($key))
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endif
                                                @endforeach --}}
                                            </select>
                                            @error('deleteRole') <p class="text-danger ">{{$message}}</p> @enderror
                                        </div>
                                        <div class="col-2">
                                            <button type="delete" class="btn btn-danger px-4">حذف</button>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $( '#multiple-select-field' ).select2( {
            theme: "bootstrap-5",
            width: $( this ).data( 'width' ) ? $( this ).data( 'width' ) : $( this ).hasClass( 'w-100' ) ? '100%' : 'style',
            placeholder: $( this ).data( 'placeholder' ),
            closeOnSelect: false,
        } );
    </script>
@endsection