@extends('panel.employee.master')
@section('title','درخواست های ثبت نام نمایشگاه')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست درخواست های ثبت نام نمایشگاه</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="card-header border-bottom-0">
                    <a href="{{route('employee.exhibition-register-req.export-excel')}}" class="btn btn-success">خروجی اکسل</a>

                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($requests) > 0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>رویداد</th>
                                    <th>شرکت</th>
                                    <th>نام و نام خانوادگی</th>
                                    <th>غرفه انتخابی</th>
                                    <th>هزینه ثبت نام (تومان)</th>
                                    <th>تاریخ ثبت نام</th>
                                    <th>وضعیت</th>
                                    <th class="text-center">امکانات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    @if(verta(now())->lessThan(verta($request->sadra->end_register_at)->addWeeks(3)))
                                        <tr>
                                            <td class="text-nowrap align-middle">{{$request->sadra->title}}</td>
                                            <td class="text-nowrap align-middle">{{$request->companyInfo->name}}</td>
                                            <td class="text-nowrap align-middle">{{$request->companyInfo->user->name." ".$request->companyInfo->user->family}} <br> {{$request->companyInfo->user->mobile}}</td>
                                            <td class="text-nowrap align-middle">{{$request->booth->plan->name}} <br> {{"غرفه: ". $request->booth->name}}</td>
                                            <td class="text-nowrap align-middle">{{number_format($request->booth->plan->price)}}</td>
                                            <td class="text-nowrap align-middle">{{verta($request->created_at)->formatDate()}}</td>
                                            <td class="text-nowrap align-middle">
                                                @if (is_null($request->status))
                                                    <span class="badge bg-primary">در انتظار پرداخت</span>
                                                @endif
                                            </td>
                                            <td class="text-nowrap align-middle text-center">
                                                <a href="javascript:void(0)" onclick="checkPayment({{$request->id}})" class="btn btn-primary">بررسی پرداخت</a>
                                                <a href="javascript:void(0)" onclick="declineSadraReg({{$request->id}})" class="btn btn-danger">رد درخواست</a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $('#plan').on('change',function (){
            $('#booth').html("")
            const plan_id =this.value
            if (plan_id){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN':'{{csrf_token()}}'
                    },
                    url: '{{route('company.ajax')}}',
                    data: {
                        do: 'get-boot-by-plan-id',
                        plan_id:plan_id
                    },
                    dataType: 'json',
                    success: function (response) {
                        if (response){
                            let data =""
                            response.forEach(function (resp){
                                data += '<option class="form-control" value="'+resp['id']+'">'+resp['name']+'</option>'
                            })
                            $('#booth').html(data)
                        }
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            }
        })
    </script>
@endsection
