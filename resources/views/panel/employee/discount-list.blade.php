@extends('panel.employee.master')
@section('title','لیست کدهای تخفیف')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست کدهای تخفیف</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                        <a href="{{route('create.discount')}}" class="btn btn-success">افزودن کد تخفیف</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($discounts)>0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>عنوان کد</th>
                                    <th>نوع تخفیف</th>
                                    <th>مقدار</th>
                                    <th>شروع</th>
                                    <th>پایان</th>
                                    <th>توضیحات</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($discounts as $discount)
                                    <tr>
                                        <td class="text-nowrap align-middle"> {{$discount->name}}</td>
                                        <td class="text-nowrap align-middle">
                                            @if($discount->is_percent)
                                                <span class="badge bg-warning">% درصد</span>
                                            @else
                                                <span class="badge bg-primary">تومانی</span>
                                            @endif
                                        </td>
                                        <td class="text-center align-middle">
                                            @if($discount->is_percent)
                                            {{$discount->value}}
                                            @else
                                                {{number_format($discount->value)}}
                                            @endif
                                        </td>
                                        <td class="text-center align-middle">{{verta($discount->begin)->formatDate()}}</td>
                                        <td class="text-center align-middle">{{verta($discount->end)->formatDate()}}</td>
                                        @if ($discount->description)
                                            <td class="text-nowrap align-middle"> {{Str::limit($discount->description,'30','...')}}</td>
                                        @else
                                            <td class="text-nowrap align-middle text-center"> - - - </td>
                                        @endif
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a class="btn btn-primary" href="{{route('edit.discount',$discount)}}">ویرایش</a>
                                                <a class="btn btn-danger delete" onclick="deleteDiscount({{$discount->id}})" href="javascript:void(0)">حذف</a>

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$discounts->links('pagination.panel')}}
            </div>
        </div>

    </div>

    <input type="hidden" id="destroyDiscountUrl" value="{{ route('destroy.discount') }}">
@endsection
