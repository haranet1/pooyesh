@extends('panel.employee.master')
@section('title','ویرایش پلن')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ویرایش پلن </a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">ویرایش پلن </h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('employee.buy-plan.update',$plan)}}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">عنوان</label>
                            <div class="col-md-9">
                                <input name="name"value="{{$plan->name}}" type="text" class="form-control" >
                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email"> قیمت (تومان) </label>
                            <div class="col-md-9">
                                <input name="price" value="{{$plan->price}}" type="text" class="form-control" >
                                @error('price') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">نقشه غرفه ها</label>
                            <div class="col-md-9">
                                <input name="map" type="file" class="form-control" >
                                @error('map') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">متراژ اتاق</label>
                            <div class="col-md-9">
                                <input name="dimensions" value="{{$plan->dimensions}}" type="text"  class="form-control" >
                                @error('dimensions') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email"> تعداد میز</label>
                            <div class="col-md-9">
                                <input name="tables_qty" value="{{$plan->tables_qty}}" type="text"  class="form-control" >
                                @error('tables_qty') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email"> تعداد صندلی</label>
                            <div class="col-md-9">
                                <input name="chairs_qty"  value="{{$plan->chairs_qty}}" type="text"  class="form-control" >
                                @error('chairs_qty') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                               <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $('#start').persianDatepicker({
            altField: '#start',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
        $('#end').persianDatepicker({
            altField: '#end',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
    </script>
@endsection
