@extends('panel.employee.master')
@section('title','لیست درخواست ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">لیست درخواست ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست درخواست ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="page-options ms-auto">
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($requests)>0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>فرستنده</th>
                                <th>گیرنده</th>
                                <th>نوع درخواست</th>
                                <th>تاریخ</th>

                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($requests as $request)
                            <tr>
                                <td class="text-nowrap align-middle">
                                    @if($request->sender_type == 'co')
                                        <span class="badge bg-warning">شرکت</span>
                                        {{$request->sender->groupable->name}}
                                    @elseif($request->sender_type == 'std')
                                        <span class="badge bg-info">دانشجو</span>
                                        {{$request->sender->name." ".$request->sender->family}}
                                    @elseif($request->sender_type == 'app')
                                        <span class="badge bg-info">کارجو</span>
                                        {{$request->sender->name." ".$request->sender->family}}
                                    @endif
                                </td>
                                <td class="text-nowrap align-middle">
                                    @if($request->receiver_type == 'co')
                                        <span class="badge bg-warning">شرکت</span>
                                        {{$request->receiver->groupable->name}}
                                    @elseif($request->receiver_type == 'std')
                                        <span class="badge bg-info">کارجو</span>
                                        {{$request->receiver->name." ".$request->receiver->family}}
                                    @elseif($request->sender_type == 'app')
                                        <span class="badge bg-info">کارجو</span>
                                        {{$request->sender->name." ".$request->sender->family}}
                                    @endif
                                </td>
                                <td class="text-nowrap align-middle">
                                    @if($request->type == 'intern')
                                        <span class="badge bg-success">کارآموزی</span>
                                    @elseif($request->type == 'full-time' || $request->type == 'part-time' || $request->type == 'remote')
                                        <span class="badge bg-primary">استخدام</span>
                                    @endif
                                </td>
                                <td class="text-nowrap align-middle"> {{verta($request->created_at)->formatDate()}}</td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="#" class="btn btn-sm btn-primary badge" type="button">تایید و ارسال قرارداد </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
