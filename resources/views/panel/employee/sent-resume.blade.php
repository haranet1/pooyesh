@extends('panel.employee.master')
@section('title' , 'لیست رزومه ها')
@section('css')
    {{-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" /> --}}
    <link rel="stylesheet" href="{{asset('assets/panel/css/sent-resume-styles.css')}}">
@endsection

@section('main')
    <div class="page-header">
        <h1 class="page-title">لیست درخواست های اشتغال</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست درخواست های اشتغال</a></li>
            </ol>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
          <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
          <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه لیست درخواست های استخدامی که برای شما فرستاده شده است نمایش داده میشود.</strong>
            </p>
          </div>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    @if (count($requests) > 0)
                        <div class="container">
                            <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                                @foreach ($requests as $request)
                                    <div class="col-12 col-md-4  mb-3">
                                        <div class="custom-card radius-10 border-0 border-3 border-info shadow-lg">
                                            <div class="card-body">
                            
                                                <!-- Profile picture and short information -->
                                                <div class="d-flex align-items-center position-relative pb-3">
                                                    <div class="flex-shrink-0">
                                                        <img class="img-md rounded-circle" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="Profile Picture" loading="lazy">
                                                    </div>
                                                    <div class="flex-grow-1 ms-3">
                                                        ارسال کننده :
                                                        @if ($request->sender->groupable_type == App\Models\StudentInfo::class)
                                                            <span class="badge bg-success">دانشجو</span>
                                                        @elseif ($request->sender->groupable_type == App\Models\ApplicantInfo::class)
                                                            <span class="badge bg-warning">کارجو</span>
                                                        @endif
                                                        <span href="{{route('candidate.single',$request->sender_id)}}" class="h5 stretched-link btn-link">{{$request->sender->name}} {{$request->sender->family}}</span>
                                                        <br>
                                                        گیرنده :
                                                        <span href="#" class="h5 stretched-link btn-link">{{$request->receiver->name}} {{$request->receiver->family}}</span>
                                                    </div>
                                                </div>
                                                
                                                <!-- END : Profile picture and short information -->
                            
                                                <!-- Options buttons -->
                                                <div class="mt-3 pt-2 text-center border-top">
                                                    <div class="d-flex justify-content-center gap-3">
                                                        <a href="{{route('candidate.single',$request->sender_id)}}" target="_blank" class="btn btn-sm btn-info btn-lg">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                            @if ($request->sender->groupable_type == App\Models\StudentInfo::class)
                                                                مشاهده دانشجو
                                                            @elseif ($request->sender->groupable_type == App\Models\ApplicantInfo::class)
                                                                مشاهده کارجو
                                                            @endif
                                                        </a>
                                                        <a href="{{route('company.single',$request->receiver_id)}}" target="_blank" class="btn btn-sm btn-primary btn-lg">
                                                            <i class="fa fa-eye" aria-hidden="true"></i> مشاهده شرکت
                                                        </a>
                                                    </div>
                                                </div>
                                                <!-- END : Options buttons -->
                            
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @else
                        <div class="alert alert-warning text-center">
                            {{__('public.no_info')}}
                        </div>
                    @endif
                    <div class="mb-5">
                        {{$requests->links('pagination.panel')}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        const csrf = "{{csrf_token()}}"
        const acceptUrl = "{{route('front.ajax.accept.resume')}}"
        const rejectUrl = "{{route('front.ajax.reject.resume')}}"
    </script>
    <script src="{{asset('assets/panel/js/company-sent-resume.js')}}"></script>
@endsection