@extends('panel.employee.master')
@section('title','لیست شرکت ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست شرکت ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
                <input type="text" id="search" onkeyup="search()" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                        <a href="{{route('employee.company.export-excel')}}" class="btn btn-success">خروجی اکسل</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($companies)>0)
                            <table class="table border-top table-bordered mb-0" id="table">
                                <thead>
                                <tr>
                                    <th>نام شرکت</th>
                                    <th>نام ثبت کننده شرکت در سامانه</th>
                                    <th>مدیر عامل</th>
                                    <th>حوزه کاری</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $company)
                                    <tr>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->name}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->name." ".$company->family}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->ceo}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->activity_field}}</td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a href="{{route('company.single',$company->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
               {{$companies->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        function search() {
            var table = document.getElementById('table')
            var key = $('#search').val()

            $('#table tr').remove()
            table.innerHTML = '<tr>\n' +
                '                            <th>نام شرکت</th>\n' +
                '                            <th>نام ثبت کننده شرکت در سامانه</th>\n' +
                '                            <th>مدیرعامل </th>\n' +
                '                            <th>حوزه کاری</th>\n' +
                '                            <th>عملکردها</th>\n' +
                '                        </tr>'


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: 'POST',
                url: '{{route('employee.ajax.search')}}',
                data: {
                    do: 'search-companies',
                    key: key
                },
                success: function (response) {
                    response['data'].forEach(function (re) {
                        var route = "{{route('company.single',['id' => ':id'])}}"
                        route = route.replace(':id', re['id'])
                        var row = table.insertRow(-1);
                        var td0 = row.insertCell(0)
                        var td1 = row.insertCell(1)
                        var td2 = row.insertCell(2)
                        var td3 = row.insertCell(3)
                        var td4 = row.insertCell(4)
                        td0.innerHTML = re['groupable']['name']
                        td1.innerHTML = re['name']
                        td2.innerHTML = re['groupable']['ceo']
                        td3.innerHTML = re['groupable']['activity_field']
                        td4.innerHTML = '<div class="btn-group align-top">' +
                            ' <a href="' + route + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل شرکت </a> </div>'
                    })
                },
                error: function (data) {
                    console.log(data)
                }
            })
        }
    </script>
@endsection
