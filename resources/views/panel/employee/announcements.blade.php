@extends('panel.employee.master')
@section('title','لیست اطلاعیه ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست اطلاعیه ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">

            </div>
            <div class="card">
                @if(Session::has('success'))
                    <div class="alert alert-success mt-2 text-center">
                        <h5>{{Session::pull('success')}}</h5>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger mt-2 text-center">
                        <h5>{{Session::pull('error')}}</h5>
                    </div>
                @endif
                <div class="card-header border-bottom-0">

                    <div class="">
                       <a href="{{route('emp.announce.create')}}" class="btn btn-success">افزودن اطلاعیه جدید</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($announces)>0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>عنوان</th>
                                    <th>فایل</th>
                                    <th>متن</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($announces as $announce)
                                    <tr>
                                        <td class="text-nowrap align-middle"> {{$announce->title}}</td>
                                        <td class="text-nowrap align-middle">
                                            @if($announce->file)
                                            <a href="{{asset($announce->file->path)}}">لینک فایل</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                           {{Str::limit($announce->content,'30','...')}}
                                        </td>

                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a  class="btn btn-primary "
                                                    data-bs-target="#modal-ann-{{$announce->id}}"
                                                    data-bs-toggle="modal"
                                                    href="javascript:void(0)">
                                                    مشاهده متن</a>
                                                <a href="javascript:void(0)" onclick="deleteAnnouncement({{$announce->id}})" class="btn btn-danger">حذف</a>
                                            </div>
                                            <div class="modal fade" id="modal-ann-{{$announce->id}}">
                                                <div
                                                    class="modal-dialog" role="document">
                                                    <div class="modal-content modal-content-demo">
                                                        <div class="modal-header">
                                                            <h6 class="modal-title text-primary">متن اطلاعیه: {{$announce->title}}</h6>
                                                        </div>
                                                        <div class="modal-body">
                                                           <p>
                                                               {{$announce->content}}
                                                           </p>
                                                        </div>
                                                        <div
                                                            class="modal-footer">
                                                            <button
                                                                class="btn btn-light"
                                                                data-bs-dismiss="modal">
                                                                بستن
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$announces->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
