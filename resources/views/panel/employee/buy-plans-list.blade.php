@extends('panel.employee.master')
@section('title','لیست پلن ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد کارکنان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست پلن ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">

            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                        {{--<a href="{{route('employee.buy-plan.create')}}" class="btn btn-success">افزودن پلن جدید</a>--}}
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($plans)>0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>نقشه غرفه ها</th>
                                    <th>عنوان</th>
                                    <th>مربوط به رویداد</th>
                                    <th>قیمت</th>
                                    <th>ابعاد</th>
                                    <th>تعداد میز</th>
                                    <th>تعداد صندلی</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($plans as $plan)
                                    <tr>
                                        <td>
                                            @if($plan->map)
                                                <a href="{{url($plan->map->path)}}"> <img alt="image"
                                                                                          class="avatar avatar-md br-7"
                                                                                          src="{{asset($plan->map->path)}}"></a>
                                            @else
                                                <span>بدون نقشه</span>
                                            @endif
                                        </td>
                                        <td class="text-nowrap align-middle"> {{$plan->name}}</td>
                                        <td class="text-nowrap align-middle"> {{$plan->sadra->title}}</td>
                                        <td class="text-nowrap align-middle"> {{number_format($plan->price)}}</td>
                                        <td class="text-nowrap align-middle"> {{$plan->dimensions}}</td>
                                        <td class="text-nowrap align-middle"> {{$plan->tables_qty }}</td>
                                        <td class="text-nowrap align-middle"> {{$plan->chairs_qty}}</td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a class="btn btn-sm btn-primary"
                                                   href="{{route('employee.buy-plan.edit',$plan)}}">ویرایش</a>
                                                <a class="btn btn-sm btn-success" onclick="addSadraBooth({{$plan->id}})"
                                                   href="javascript:void(0)">افزودن غرفه</a>
                                                {{--                                                <a class="btn btn-sm btn-secondary" onclick="showSadraBooth({{$plan->id}})" href="javascript:void(0)">نمایش غرفه ها</a>--}}
                                                <a class="btn btn-sm btn-secondary"
                                                   data-bs-target="#modal-{{$plan->id}}"
                                                   data-bs-toggle="modal"
                                                   href="javascript:void(0)">
                                                    مشاهده غرفه ها</a>
                                                <form action="{{route('employee.buy-plan.destroy',$plan)}}"
                                                      method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn btn-sm btn-danger"> حذف</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="modal-{{$plan->id}}">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content modal-content-demo">
                                                <div class="modal-header">
                                                    <h6 class="modal-title">
                                                        لیست غرفه های تعریف شده برای پلن: {{$plan->name}}
                                                    </h6>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="card-body">
                                                        @if($booths=getBuyPlanBooths($plan->id))
                                                            @foreach($booths as $booth)
                                                                <div>
                                                                    <span>عنوان: </span>
                                                                    <span class="text-primary p-2">{{$booth->name}}</span>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="alert alert-danger">
                                                                <h4>هنوز برای این پلن، غرفه ای تعریف نکرده اید!</h4>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div
                                                    class="modal-footer">
                                                    <button
                                                        class="btn btn-light"
                                                        data-bs-dismiss="modal">
                                                        بستن
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$plans->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
