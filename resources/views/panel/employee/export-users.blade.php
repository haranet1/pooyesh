@extends('panel.employee.master')
@section('title','خروجی کاربران')
@section('main')

    <div class="row row-cards mt-4">
        <div class="col-lg-12 col-xl-12">
            
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"></h3>
                </div>
                <div class="card-body">
                    <div class="card-pay">
                        <ul class="tabs-menu nav">
                            <li class=""><a href="#tab20" class="active" data-bs-toggle="tab">شرکت ها</a></li>
                            <li><a href="#tab21" data-bs-toggle="tab" class="">کارجویان</a></li>
                            <li><a href="#tab22" data-bs-toggle="tab" class="">دانشجویان</a></li>
                        </ul>
                        <div class="tab-content">
                            {{-- company list --}}
                            <div class="tab-pane active show" id="tab20">
                                <div class="e-table px-5 pb-5">
                                    <div class="col-1">
                                        <a href="{{route('employee.export.all.companies')}}" class="btn btn-secondary">خروجی اکسل</a>
                                    </div>
                                </div>
                            </div>
                            {{-- applicant list --}}
                            <div class="tab-pane" id="tab21">
                                <div class="e-table px-5 pb-5">
                                    <div class="col-1">
                                        <a href="{{route('employee.unconfirmed.applicant.export-excel')}}" class="btn btn-secondary">خروجی اکسل</a>
                                    </div>
                                </div>
                            </div>
                            {{-- student list --}}
                            <div class="tab-pane" id="tab22">
                                <div class="e-table px-5 pb-5">
                                    <div class="row">
                                        <div class="col-1 m-2">
                                            <a href="{{route('employee.unconfirmed.std.export-excel')}}" class="btn btn-secondary">خروجی اکسل</a>
                                        </div>
                                        <div class="col-3 m-2">
                                            <a href="{{route('employee.export.all.mbti.students')}}" class="btn btn-info">خروجی اکسل دانشجویان همراه با تست mbti</a>
                                        </div>
                                        <div class="col-2 m-2">
                                            <a href="{{route('employee.export.all.mbti.users')}}" class="btn btn-primary">خروجی کاربران دارای تست mbti</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                        <a href="{{route('employee.company.export-excel')}}" class="btn btn-success">خروجی اکسل</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($companies)>0)
                            <table class="table border-top table-bordered mb-0" id="table">
                                <thead>
                                <tr>
                                    <th>نام شرکت</th>
                                    <th>نام ثبت کننده شرکت در سامانه</th>
                                    <th>مدیر عامل</th>
                                    <th>حوزه کاری</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $company)
                                    <tr>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->name}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->name." ".$company->family}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->ceo}}</td>
                                        <td class="text-nowrap align-middle"> {{$company->groupable->activity_field}}</td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a href="{{route('company.single',$company->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div> --}}
            {{-- <div class="mb-5">
               {{$companies->links('pagination.panel')}}
            </div> --}}
        </div>

    </div>
@endsection
@section('script')
    <script>
        let companySingle = "{{route('company.single',':id')}}";
        let appSingle = "{{route('candidate.single',':id')}}";
        let stdSingle = "{{route('candidate.single',':id')}}";
    </script>
    <script src="{{asset('assets/panel/js/employee.js')}}"></script>
    <script>
        function search() {
            var table = document.getElementById('table')
            var key = $('#search').val()

            $('#table tr').remove()
            table.innerHTML = '<tr>\n' +
                '                            <th>نام شرکت</th>\n' +
                '                            <th>نام ثبت کننده شرکت در سامانه</th>\n' +
                '                            <th>مدیرعامل </th>\n' +
                '                            <th>حوزه کاری</th>\n' +
                '                            <th>عملکردها</th>\n' +
                '                        </tr>'


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: 'POST',
                url: '{{route('employee.ajax.search')}}',
                data: {
                    do: 'search-companies',
                    key: key
                },
                success: function (response) {
                    response['data'].forEach(function (re) {
                        var route = "{{route('company.single',['id' => ':id'])}}"
                        route = route.replace(':id', re['id'])
                        var row = table.insertRow(-1);
                        var td0 = row.insertCell(0)
                        var td1 = row.insertCell(1)
                        var td2 = row.insertCell(2)
                        var td3 = row.insertCell(3)
                        var td4 = row.insertCell(4)
                        td0.innerHTML = re['groupable']['name']
                        td1.innerHTML = re['name']
                        td2.innerHTML = re['groupable']['ceo']
                        td3.innerHTML = re['groupable']['activity_field']
                        td4.innerHTML = '<div class="btn-group align-top">' +
                            ' <a href="' + route + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل شرکت </a> </div>'
                    })
                },
                error: function (data) {
                    console.log(data)
                }
            })
        }
    </script>
@endsection



