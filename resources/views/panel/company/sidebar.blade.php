@php
    $hireRoutes = [
        Route::is('company.std-resume-list.show'),
        Route::is('co.hire.pos.job'),
        Route::is('co.new.hire.req.job'),
        Route::is('co.accepted.hire.req.job'),
        Route::is('company.applicants.list'),
    ];
    $pooyeshRoutes = [
        Route::is('co.intern.pos.job'),
        Route::is('co.new.intern.req.job'),
        Route::is('co.accepted.intern.req.job'),
        Route::is('company.students.list'),
    ];
    $exhibitionRoutes = [
        Route::is('company.products.index'),
        Route::is('company.sadra'),
        Route::is('registred.company.sadra'),
    ];
    $eventRoutes = [
        Route::is('list.event'),
        Route::is('registred.event'),
    ];
    $supRoutes = [
        Route::is('compose.sup.ticket'),
        Route::is('inbox.ticket'),
    ];
@endphp
<ul class="side-menu">
    <li class="sub-category">
        <h3>اصلی</h3>
    </li>
    <li class="slide">
        <a class="side-menu__item has-link" data-bs-toggle="slide" href="{{route('company.panel')}}">
            {{-- <i class="side-menu__icon fe fe-home"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/dashboard.svg')}}" alt="داشبورد">
            <span class="side-menu__label">داشبورد</span>
        </a>

        <a class="side-menu__item has-link" data-bs-toggle="slide" target="_blank" href="{{route('home')}}">
            {{-- <i class="side-menu__icon fe fe-home"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/home.svg')}}" alt="استخدام و سرمایه گذاری">
            <span class="side-menu__label">صفحه اصلی سایت</span>
        </a>

        <a class="side-menu__item has-link" data-bs-toggle="slide" target="_blank" href="{{route('index.package.company')}}">
            <img class="me-3" style="width: 20px" src="{{asset('assets/panel/images/icon/SVG/technical-service.png')}}" alt="کارآموزی و استخدامی">
            <span class="side-menu__label">بسته های خدماتی</span>
        </a>
    </li>

    <li class="sub-category">
        <h3> امکانات</h3>
    </li>

    <li class="slide company">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/company-profile.svg')}}" alt="پروفایل شرکت">
            <span class="side-menu__label">پروفایل شرکت</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">پروفایل شرکت</a></li>
            <li><a href="{{route('company.info')}}" class="slide-item {{Route::is('company.info')?'active':''}}">اطلاعات شرکت</a></li>
            <li><a href="{{route('company.portfolio.index')}}" class="slide-item {{Route::is('company.portfolio.index')?'active':''}}">پروژه های نمونه</a></li>
            <li><a href="{{route('company.single',\Illuminate\Support\Facades\Auth::id())}}" class="slide-item">صفحه اختصاصی شرکت </a></li>
            <li><a href="{{route('company.tickets')}}" class="slide-item {{Route::is('company.tickets')?'active':''}}">پیام های دریافتی</a></li>
            <li><a href="{{route('company.signature.create')}}" class="slide-item {{Route::is('company.signature.create')?'active':''}}">افزودن امضا</a></li>
        </ul>
    </li>

    <li class="slide hire 
    @foreach ($hireRoutes as $hireRoute)
        @if (Route::currentRouteName() == $hireRoute)
            is-expanded
        @endif
    @endforeach
    ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/dile2.svg')}}" alt="استخدام و سرمایه گذاری">
            <span class="side-menu__label">استخدام</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">استخدام</a></li>
            {{-- <li><a href="{{route('company.projects.list')}}" class="slide-item {{Route::is('company.projects.list')?'active':''}}">نمایش پروژه ها</a></li>
            <li><a href="{{route('company.projects.create')}}" class="slide-item {{Route::is('company.projects.create')?'active':''}}">تعریف پروژه</a></li>
            <li><a href="{{route('company.idea.create')}}" class="slide-item {{Route::is('company.idea.create')?'active':''}}"> ارائه ایده و ایجاد استارتاپ</a></li>
            <li><a href="{{route('company.idea.index')}}" class="slide-item {{Route::is('company.idea.index')?'active':''}}"> لیست ایده ها و استارتاپ ها</a></li> --}}
            {{-- <li><a href="{{route('company.std-resume-list.show')}}" class="slide-item {{Route::is('company.std-resume-list.show')?'active':''}}"> درخواست های اشتغال </a></li> --}}
            <li><a href="{{route('co.hire.pos.job')}}" class="slide-item {{Route::is('co.hire.pos.job')?'active':''}}"> لیست موقعیت های شغلی </a></li>
            <li><a href="{{route('co.new.hire.req.job')}}" class="slide-item {{Route::is('co.new.hire.req.job')?'active':''}}">  درخواست های جدید </a></li>
            <li><a href="{{route('co.accepted.hire.req.job')}}" class="slide-item {{Route::is('co.accepted.hire.req.job')?'active':''}}"> سابقه درخواست ها </a></li>
            <li><a href="{{route('company.applicants.list')}}" class="slide-item {{Route::is('company.applicants.list')?'active':''}}"> متقاضیان استخدام  </a></li>

        </ul>
    </li>

    <li class="slide pooyesh
    @foreach ($pooyeshRoutes as $pooyeshRoute)
        @if (Route::currentRouteName() == $pooyeshRoute)
            is-expanded
        @endif
    @endforeach
    ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/intern.svg')}}" alt="استخدام و سرمایه گذاری">

            <span class="side-menu__label">طرح پویش <span style="font-size: 10px">(کارآموزی)</span></span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">پویش</a></li>
{{--            <li><a href="{{route('company.job.intern.create')}}" class="slide-item {{Route::is('company.job.intern.create')?'active':''}}">ثبت نیاز کارآموزی</a></li>--}}
            <li><a href="{{route('co.intern.pos.job')}}" class="slide-item {{Route::is('co.intern.pos.job')?'active':''}}">موقعیت های کارآموزی</a></li>
            <li><a href="{{route('co.new.intern.req.job')}}" class="slide-item {{Route::is('co.new.intern.req.job')?'active':''}}">  درخواست های جدید </a></li>
            <li><a href="{{route('co.accepted.intern.req.job')}}" class="slide-item {{Route::is('co.accepted.intern.req.job')?'active':''}}"> سابقه درخواست ها </a></li>
            <li><a href="{{route('company.students.list')}}" class="slide-item {{Route::is('company.students.list')?'active':''}}">متقاضیان کارآموزی</a></li>


        </ul>
    </li>

    <li class="slide exhibition
    @foreach ($exhibitionRoutes as $exhibitionRoute)
        @if (Route::currentRouteName() == $exhibitionRoute)
            is-expanded
        @endif
    @endforeach
    ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/Exhibition.svg')}}" alt="استخدام و سرمایه گذاری">

            <span class="side-menu__label">نمایشگاه کار (صدرا)</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">نمایشگاه کار (صدرا)</a></li>

            <li><a href="{{route('company.products.index')}}" class="slide-item {{Route::is('company.products.index')?'active':''}}">محصولات/ خدمات </a></li>
            <li><a href="{{route('company.sadra')}}" class="slide-item {{Route::is('company.sadra')?'active':''}}">شرکت در نمایشگاه </a></li>
            <li><a href="{{route('registred.company.sadra')}}" class="slide-item {{Route::is('registred.company.sadra')?'active':''}}">درخواست های ثبت نام در نمایشگاه</a></li>
        </ul>
    </li>

    <li class="slide 
    @foreach ($eventRoutes as $eventRoute)
        @if (Route::currentRouteName() == $eventRoute)
            is-expanded
        @endif
    @endforeach
    ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/Exhibition.svg')}}" alt="رویداد ها">

            <span class="side-menu__label">رویداد ها</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">رویداد ها</a></li>

            <li><a href="{{route('list.event')}}" class="slide-item {{Route::is('list.event')?'active':''}}">شرکت در رویداد </a></li>
            <li><a href="{{route('registred.event')}}" class="slide-item {{Route::is('registred.event')?'active':''}}">رویداد های ثبت نام شده</a></li>
        </ul>
    </li>

    <li class="slide supp-menu 
    @foreach ($supRoutes as $supRoute)
        @if (Route::currentRouteName() == $supRoute)
            is-expanded
        @endif
    @endforeach
    ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/support.svg')}}" alt="پشتیبانی">
            <span class="side-menu__label">  پشتیبانی </span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)"> پشتیبانی </a></li>
            <li><a href="{{route('compose.sup.ticket')}}" class="slide-item {{Route::is('compose.sup.ticket')?'active':''}}">ارسال تیکت</a></li>
            <li><a href="{{route('inbox.ticket')}}" class="slide-item {{Route::is('inbox.ticket')?'active':''}}">لیست تیک ها</a></li>
            {{-- <li><a href="{{route('std.company_ticket.index')}}" class="slide-item {{Route::is('std.company_ticket.index')?'active':''}}"> تیکت های ارسالی به شرکت ها </a></li> --}}
        </ul>
    </li>

    {{--<li class="slide ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            <i class="side-menu__icon fe fe-slack"></i>
            <span class="side-menu__label"> درخواست خدمات آموزشی</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">درخواست خدمات آموزشی </a></li>
            <li><a href="" class="slide-item">آموزش کارکنان</a></li>
            <li><a href="" class="slide-item">درخواست مشاوره</a></li>
            <li><a href="" class="slide-item">مشاهده کلیه دوره های مهارتی</a></li>
        </ul>
    </li>--}}

</ul>
