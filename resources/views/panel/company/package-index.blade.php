@extends('panel.company.master')
@section('title' , $title)
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection
@section('main')
<style>
    .custom-card1 {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-clip: border-box;
        border-radius: .7rem;
        margin-bottom: 1.5rem;
        box-shadow: rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset;
    }
    .bg-hire-gr {
        background: linear-gradient(to bottom right,#06005d 20%,#7875A8 100%) !important;
    }
    .bg-intern-gr {
        background: linear-gradient(to bottom right,#1E6500 20%,#52c023 100%) !important;
    }
    .bg-mixed-gr {
        background: linear-gradient(to bottom right,#fff200 0%,#ff9900 60%) !important;
    }
    .price-p {
        border-top: 1px solid #6c5ffc;
        padding-inline: 10px;
        width: 90%;
    }
    .text-hire{
        color: #06005d;
    }
    .text-intern{
        color: #1E6500;
    }
    .text-mixed{
        color: #ffb300;
    }
    .btn-hire{
        background-image: linear-gradient(to bottom right,#06005d 20%,#7875A8 100%) !important;
    }
    .btn-intern{
        background-image:  linear-gradient(to bottom right,#1E6500 20%,#52c023 100%) !important;
    }
    .btn-mixed{
        background-image: linear-gradient(to bottom right,#fff200 0%,#ff9900 60%) !important;
    }
    .button-base {
        align-items: center;
        appearance: none;
        border: 0;
        border-radius: 6px;
        box-shadow: rgba(45, 35, 66, .4) 0 2px 4px,rgba(45, 35, 66, .3) 0 7px 13px -3px,rgba(58, 65, 111, .5) 0 -3px 0 inset;
        box-sizing: border-box;
        color: #fff;
        cursor: pointer;
        display: inline-flex;
        height: 40px;
        justify-content: center;
        line-height: normal;
        list-style: none;
        overflow: hidden;
        padding-left: 18px;
        padding-right: 18px;
        position: relative;
        text-decoration: none;
        transition: box-shadow .15s,transform .15s;
        touch-action: manipulation;
        white-space: nowrap;
        will-change: box-shadow,transform;
        font-size: 16px;
    }
    .button-base:focus {
        box-shadow: rgba(45, 35, 66, 0.4) 0 2px 4px, rgba(45, 35, 66, 0.3) 0 7px 13px -3px;
    }

    .button-base:hover {
        box-shadow: rgba(45, 35, 66, 0.4) 0 4px 8px, rgba(45, 35, 66, 0.3) 0 7px 13px -3px,;
        transform: translateY(-2px);
    }

    .button-base:active {
        transform: translateY(2px);
    }
</style>
<div class="page-header">
    <h1 class="page-title">{{ $title }}</h1>
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد / طرح پویش</a></li>
        </ol>
    </div>
</div>

<div class="row row-cards mt-4">
    <div class="col-12">

    @if(Session::has('success'))
        <div class="alert alert-success mt-2 text-center">
            <h5>{{Session::pull('success')}}</h5>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger mt-2 text-center">
            <h5>{{Session::pull('error')}}</h5>
        </div>
    @endif

        <div class="row justify-content-center">
            @if ($intern_packages->count() > 0)
            <div class="col-12 col-md-4 col-lg-3">
                <h4 class="text-primary text-center fw-bold mb-5">بسته های کارآموزی</h4>
                @foreach ($intern_packages as $intern)
                <div class="custom-card1 bg-white overflow-hidden">
                    <div class="card-header p-3 bg-intern-gr">
                        <div class="flex-shrink-0">
                            <img class="img-md" width="45px" height="45px" src="{{asset('assets/panel/images/social-media.png')}}" alt="laptop icon" loading="lazy">
                        </div>
                        <div class="card-title text-center ms-2 text-white flex-grow-1">
                            {{$intern->name}}
                        </div>
                    </div>
                    <div class="card-body px-2">
                        <!-- intern info -->
                        <div class="d-flex align-items-center position-relative">
                            <div class="flex-grow-1 ms-3 ">
                                @if ($intern->additional_internship_positions > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-intern" aria-hidden="true"></i>ثبت <span class="fw-bold text-pinterest">{{$intern->additional_internship_positions}}</span> موقعیت کارآموزی جدید</h5>
                                @endif
                                @if ($intern->additional_internship_requests > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-intern" aria-hidden="true"></i>ارسال <span class="fw-bold text-pinterest">{{ $intern->additional_internship_requests }}</span> درخواست کارآموزی جدید</h5>
                                @endif
                                @if ($intern->additional_internship_confirms > 0)

                                    <h5><i class="fa fa-circle fs-10 me-2 text-intern" aria-hidden="true"></i>امکان تایید <span class="fw-bold text-pinterest">{{ $intern->additional_internship_confirms }}</span> درخواست کارآموزی</h5>
                                @endif

                                <div class="text-center price-p mx-auto">
                                    <h5 class="fw-bold mt-3">قیمت : {{ number_format($intern->price) }} تومان</h5>
                                </div>
                            </div>

                        </div>
                        
                        <!-- END : package info -->
    
                        <!-- Options buttons -->
                        <div class="mt-1 pt-2 text-center">
                            <div class="d-flex justify-content-center">
                                <button data-bs-toggle="modal" data-bs-target="#gateway-modal-{{$intern->id}}" type="button" class="button-base btn-intern">
                                    <i class="fa fa-shopping-cart me-2" aria-hidden="true"></i> خرید
                                </button>
                            </div>
                        </div>
                        <div class="modal fade" id="gateway-modal-{{$intern->id}}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content modal-content-demo">
                                    <div class="modal-header">
                                        <h6 class="modal-title">انتخاب درگاه خرید</h6>
                                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form method="POST" action="{{ route('company.package.config') }}">
                                        @csrf
                                        <div class="modal-body fieldset">
                                            <input type="hidden" name="package_id" value="{{ $intern->id }}">
                                            <div class="radio-item">
                                                <label for="iaun">
                                                    <input type="radio" id="iaun" name="gateway" value="iaun" checked>
                                                    <span>درگاه دانشگاه</span>
                                                </label>
                                                {{-- <label for="zarinpal">
                                                    <input type="radio" id="zarinpal" name="gateway" value="zarinpal">
                                                    <span>درگاه زرین پال</span>
                                                </label> --}}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn ripple btn-primary">
                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> تکمیل پرداخت
                                                </button>
                                                <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END : Options buttons -->
    
                    </div>
                </div>
                @endforeach
            </div>
            @endif

            @if ($hire_packages->count() > 0)
            <div class="col-12 col-md-4 col-lg-3">
                <h4 class="text-primary text-center fw-bold mb-5">بسته های استخدامی</h4>
                @foreach ($hire_packages as $hire)
                <div class="custom-card1 bg-white overflow-hidden">
                    <div class="card-header p-3 bg-hire-gr">
                        <div class="flex-shrink-0">
                            <img class="img-md" width="45px" height="45px" src="{{asset('assets/panel/images/head-hunting.png')}}" alt="laptop icon" loading="lazy">
                        </div>
                        <div class="card-title text-center ms-2 text-white flex-grow-1">
                            {{$hire->name}}
                        </div>
                    </div>
                    <div class="card-body px-2">
                        <!-- intern info -->
                        <div class="d-flex align-items-center position-relative">
                            <div class="flex-grow-1 ms-3 ">

                                @if ($hire->additional_hire_positions > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-hire" aria-hidden="true"></i>ثبت <span class="fw-bold text-pinterest">{{$hire->additional_hire_positions}}</span> موقعیت استخدامی جدید</h5>
                                @endif
                                @if ($hire->additional_hire_requests > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-hire" aria-hidden="true"></i>ارسال <span class="fw-bold text-pinterest">{{ $hire->additional_hire_requests }}</span> درخواست استخدامی جدید</h5>
                                @endif
                                @if ($hire->additional_hire_confirms > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-hire" aria-hidden="true"></i>امکان تایید <span class="fw-bold text-pinterest">{{ $hire->additional_hire_confirms }}</span> درخواست استخدامی</h5>
                                @endif

                                <div class="text-center price-p mx-auto">
                                    <h5 class="fw-bold mt-3">قیمت : {{ number_format($hire->price) }} تومان</h5>
                                </div>
                            </div>

                        </div>
                        
                        <!-- END : package info -->
    
                        <!-- Options buttons -->
                        <div class="mt-1 pt-2 text-center">
                            <div class="d-flex justify-content-center">
                                <button data-bs-toggle="modal" data-bs-target="#gateway-hire-{{$hire->id}}" type="button" class="button-base btn-hire">
                                    <i class="fa fa-shopping-cart me-2" aria-hidden="true"></i> خرید
                                </button>
                            </div>
                        </div>
                        <div class="modal fade" id="gateway-hire-{{$hire->id}}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content modal-content-demo">
                                    <div class="modal-header">
                                        <h6 class="modal-title">انتخاب درگاه خرید</h6>
                                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form method="POST" action="{{ route('company.package.config') }}">
                                        @csrf
                                        <div class="modal-body fieldset">
                                            <input type="hidden" name="package_id" value="{{ $hire->id }}">
                                            <div class="radio-item">
                                                <label for="iaun">
                                                    <input type="radio" id="iaun" name="gateway" value="iaun" checked>
                                                    <span>درگاه دانشگاه</span>
                                                </label>
                                                {{-- <label for="zarinpal">
                                                    <input type="radio" id="zarinpal" name="gateway" value="zarinpal">
                                                    <span>درگاه زرین پال</span>
                                                </label> --}}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn ripple btn-primary">
                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> تکمیل پرداخت
                                                </button>
                                                <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END : Options buttons -->
    
                    </div>
                </div>
                @endforeach
            </div>
            @endif

            @if ($combined_packages->count() > 0)
            <div class="col-12 col-md-4 col-lg-3">
                <h4 class="text-primary text-center fw-bold mb-5">بسته های ویژه</h4>
                @foreach ($combined_packages as $combined)
                <div class="custom-card1 bg-white overflow-hidden">
                    <div class="card-header p-3 bg-mixed-gr">
                        <div class="flex-shrink-0">
                            <img class="img-md" width="45px" height="45px" src="{{asset('assets/panel/images/puzzle.png')}}" alt="laptop icon" loading="lazy">
                        </div>
                        <div class="card-title text-center ms-2 text-white flex-grow-1">
                            {{$combined->name}}
                        </div>
                    </div>
                    <div class="card-body px-2">
                        <!-- intern info -->
                        <div class="d-flex align-items-center position-relative">
                            <div class="flex-grow-1 ms-3 ">

                                @if ($combined->additional_hire_positions > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-mixed" aria-hidden="true"></i>ثبت <span class="fw-bold text-pinterest">{{$combined->additional_hire_positions}}</span> موقعیت استخدامی جدید</h5>
                                @endif
                                @if ($combined->additional_hire_requests > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-mixed" aria-hidden="true"></i>ارسال <span class="fw-bold text-pinterest">{{ $combined->additional_hire_requests }}</span> درخواست استخدامی جدید</h5>
                                @endif
                                @if ($combined->additional_hire_confirms > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-mixed" aria-hidden="true"></i>امکان تایید <span class="fw-bold text-pinterest">{{ $combined->additional_hire_confirms }}</span> درخواست استخدامی</h5>
                                @endif

                                @if ($combined->additional_internship_positions > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-mixed" aria-hidden="true"></i>ثبت <span class="fw-bold text-pinterest">{{$combined->additional_internship_positions}}</span> موقعیت کارآموزی جدید</h5>
                                @endif
                                @if ($combined->additional_internship_requests > 0)
                                    <h5><i class="fa fa-circle fs-10 me-2 text-mixed" aria-hidden="true"></i>ارسال <span class="fw-bold text-pinterest">{{ $combined->additional_internship_requests }}</span> درخواست کارآموزی جدید</h5>
                                @endif
                                @if ($combined->additional_internship_confirms > 0)

                                    <h5><i class="fa fa-circle fs-10 me-2 text-mixed" aria-hidden="true"></i>امکان تایید <span class="fw-bold text-pinterest">{{ $combined->additional_internship_confirms }}</span> درخواست کارآموزی</h5>
                                @endif

                                <div class="text-center price-p mx-auto">
                                    <h5 class="fw-bold mt-3">قیمت : {{ number_format($combined->price) }} تومان</h5>
                                </div>
                            </div>

                        </div>
                        
                        <!-- END : package info -->
    
                        <!-- Options buttons -->
                        <div class="mt-1 pt-2 text-center">
                            <div class="d-flex justify-content-center">
                                <button data-bs-toggle="modal" data-bs-target="#gateway-mixed-{{$combined->id}}" type="button" class="button-base btn-mixed">
                                    <i class="fa fa-shopping-cart me-2" aria-hidden="true"></i> خرید
                                </button>
                            </div>
                        </div>
                        <div class="modal fade" id="gateway-mixed-{{$combined->id}}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content modal-content-demo">
                                    <div class="modal-header">
                                        <h6 class="modal-title">انتخاب درگاه خرید</h6>
                                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form method="POST" action="{{ route('company.package.config') }}">
                                        @csrf
                                        <div class="modal-body fieldset">
                                            <input type="hidden" name="package_id" value="{{ $combined->id }}">
                                            <div class="radio-item">
                                                <label for="iaun">
                                                    <input type="radio" id="iaun" name="gateway" value="iaun" checked>
                                                    <span>درگاه دانشگاه</span>
                                                </label>
                                                {{-- <label for="zarinpal">
                                                    <input type="radio" id="zarinpal" name="gateway" value="zarinpal">
                                                    <span>درگاه زرین پال</span>
                                                </label> --}}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn ripple bg-primary text-white">
                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> تکمیل پرداخت
                                                </button>
                                                <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END : Options buttons -->
    
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div>


@endsection

