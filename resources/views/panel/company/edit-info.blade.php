@extends('panel.company.master')
@section('title','ویرایش اطلاعات شرکت')
@section('main')
    <div class="page-header">
        <h1 class="page-title">ویرایش اطلاعات شرکت</h1>
        <div>
            <ol class="breadcrumb">
                {{--<li class="breadcrumb-item"><a href="javascript:void(0)">رزومه</a></li>--}}
            </ol>
        </div>
    </div>
    <div class="row justify-content-center p-3">
        <div class="col-md-8 col-xl-8 col-sm-6">
            <div class="card">
                <div class="p-3">
                    <form class="form-control" action="{{route('company.info.update')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <h4 class="mb-5 mt-3 fw-bold">درباره شرکت :</h4>
                        <p>@php echo $user->groupable->about @endphp</p>
                        <textarea class="form-control" placeholder="" name="about" id="" cols="30"
                                  rows="4"></textarea>

                        <h4 class="mb-5 mt-3 fw-bold">اطلاعات بیشتر :</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="fw-bold">نام شرکت</td>
                                    <td>
                                        @if ($user->status == 1)
                                            <input name="name" type="hidden" value="{{$user->groupable->name}}">
                                            <input class="form-control mb-2" disabled name="name" type="text" value="{{$user->groupable->name}}">
                                            <span class="text-info ">به دلیل تایید شدن شرکت شما توسط سامانه، امکان تغییر نام را ندارید.</span>
                                        @else
                                            <input class="form-control mb-2" name="name" type="text" value="{{$user->groupable->name}}">
                                        @endif
                                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold"> وب سایت</td>
                                    <td>
                                        <input class="form-control" name="website" type="text"
                                               value="{{$user->groupable->website}}">
                                        @error('website') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold">شماره تلفن</td>
                                    <td>
                                        <input class="form-control" name="phone" type="text" value="{{$user->groupable->phone}}">
                                        @error('phone')
                                            <span class="text-danger">{{$message}}</span> 
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold"> ایمیل</td>
                                    <td>
                                        @if ($user->hasVerifiedEmail())
                                            <input  name="email" type="hidden" value="{{$user->email}}">
                                            <input class="form-control mb-2" disabled name="email" type="email" value="{{$user->email}}">
                                        @endif
                                        @if ($user->hasVerifiedEmail())
                                            <span class="text-info">به دلیل تایید شدن ایمیل شما توسط سامانه، امکان تغییر آن را ندارید.</span>
                                        @endif
                                        @error('email') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold">گروه ما</td>
                                    <td>
                                        <input class="form-control" name="team" type="text" value="{{$user->groupable->team}}">
                                        @error('team') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold">جوایز و افتخارات</td>
                                    <td>
                                        <input class="form-control" name="rewards" type="text"
                                               value="{{$user->groupable->rewards}}">
                                        @error('rewards') <span class="text-danger">{{$message}}</span> @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold">لوگو شرکت</td>
                                    <td>
                                        <div class="row align-items-center">
                                            @if ($user->groupable->logo)
                                                <div class="col-2">
                                                    <div class="border" style="border-radius: 15px">
                                                        <img src="{{asset($user->groupable->logo->path)}}" alt="company-logo" >
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-10">
                                                <input type="file" name="logo" class="form-control">
                                                @error('logo') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-bold">هدر شرکت (پس زمینه)</td>
                                    <td>
                                        <div class="row align-items-center">
                                            @if ($user->groupable->header)
                                                <div class="col-2">
                                                    <div class="border" style="border-radius: 15px">
                                                        <img src="{{asset($user->groupable->header->path)}}" alt="company-logo">
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-10">
                                                <input type="file" name="header" class="form-control">
                                                @error('header') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <button type="submit" class="btn btn-success">ذخیره اطلاعات</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection
