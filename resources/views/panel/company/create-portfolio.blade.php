@extends('panel.company.master')
@section('title','افزودن پروژه نمونه')
@section('main')
    <div class="page-header">
        <h1 class="page-title">افزودن پروژه نمونه</h1>
        <div>
            <ol class="breadcrumb">
{{--                <li class="breadcrumb-item"><a href="javascript:void(0)">رزومه</a></li>--}}
            </ol>
        </div>
    </div>
    <div class="row justify-content-center p-3">
        <div class="col-md-8 col-xl-8 col-sm-6">

            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">افزودن پروژه نمونه</h4>
                </div>
                @if(Session::has('success'))
                    <div class="alert alert-success text-center">{{Session::pull('success')}}</div>
                @endif
                <div class="card-body">
                        <form class="form-horizontal" action="{{route('company.portfolio.store')}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">عنوان</label>
                                <div class="col-md-9">
                                    <input name="name" type="text" class="form-control" >
                                    @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">مهارت های استفاده شده</label>
                                <div class="col-md-9">
                                    <input name="skills" type="text" class="form-control" >
                                    @error('skills') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">تصویر</label>
                                <div class="col-md-9">
                                    <input name="photo" type="file"  class="form-control" >
                                    @error('photo') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>

                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">توضیحات</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="desc" id="" cols="30" rows="10"></textarea>
                                    @error('desc') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>

                            <div class=" row mb-0 justify-content-center ">
                                <div class="col justify-content-center text-center ">
                                    <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>

    </div>


@endsection
