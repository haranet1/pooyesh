@extends('panel.company.master')
@section('title','رویدادها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست رویدادها</a></li>
            </ol>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
        <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
        <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه لیست نمایشگاه های کار نمایش داده میشود.</strong>
            </p>
        </div>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
                {{-- <input type="text" class="form-control" placeholder="جستجو">
                 <div class="input-group-text btn btn-primary">
                     <i class="fa fa-search" aria-hidden="true"></i>
                 </div>--}}
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">
                <div class="card-header border-bottom-0">
                </div>
                <div class="card-body">
                    @if(count($events) > 0)
                        @foreach($events as $event)
                            {{-- @php
                                $eventExist = false;
                            @endphp --}}
                            {{-- @foreach ($company_sadra as $sadra)
                                @if ($event->id == $sadra->sadra_id && ($sadra->status == 1 || is_null($sadra->status)))
                                    @php
                                        $eventExist = true;
                                    @endphp
                                    @break
                                @endif
                            @endforeach --}}
                            <div class="col-12">
                                <div class="card"
                                    style="border: 1px dashed black !important;">
                                    <div class="row justify-content-center">
                                        {{-- image --}}
                                        <div class="col-md-2 p-0 m-3 text-center">
                                            @if($event->photo)
                                                <a target="_blank" href="{{url($event->photo->path)}}">
                                                    <img class="img-fluid  "
                                                        src="{{asset($event->photo->path)}}"
                                                        alt="logo">
                                                </a>

                                            @else
                                                <img class="img-fluid"
                                                    src="{{asset('assets/images/default-image.svg')}}"
                                                    alt="avatar">
                                            @endif

                                        </div>
                                        {{-- description --}}
                                        <div class="col-md-6">
                                            <h3 class="mt-5 text-info">{{$event->title}}</h3>
                                            <h6 class="text-nowrap" dir="ltr">از
                                                تاریخ: {{verta($event->start_at)->format('Y/m/d')}}
                                                تا {{verta($event->end_at)->format('Y/m/d')}}</h6>
                                            @if($event->map)
                                                <a target="_blank" href="{{url($event->map->path)}}"> نقشه رویداد</a>
                                            @endif

                                            @if($event->start_register_at)
                                            <h6 class="text-nowrap my-1" dir="ltr">شروع ثبت نام از
                                                تاریخ: {{verta($event->start_register_at)->format('Y/m/d')}}
                                                تا {{verta($event->end_register_at)->format('Y/m/d')}}</h6>
                                            @endif

                                            <h6>{{$event->description}}</h6>
                                        </div>
                                        {{-- submit --}}
                                        
                                        <div class="col-md-3 d-flex align-items-center justify-content-end">
                                            <div class="row">
                                                <div class="col-12 justify-content-end">
                                                    <div class="btn-group align-top">
                                                        {{-- @if ($eventExist == true)
                                                            <span class="btn btn-lg btn-dark disabled">ثبت نام شده</span> --}}
                                                        @if(verta(now())->lessThan(verta($event->end_register_at)))
                                                        {{-- <form action="{{route('company.sadra.register.show')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="event" value="{{$event->id}}">
                                                            <input type="hidden" name="user" value="{{Auth::user()->id}}">
                                                            <button type="submit" class="btn btn-lg btn-success">ثبت نام</button>
                                                        </form> --}}
                                                        {{-- <a href="{{ route('show.register.sadra' , $event->id) }}" class="btn btn-lg btn-success">ثبت نام</a> --}}
                                                        <a href="javascript:void(0)" onclick="checkCompanyHas3InternShip({{ Auth::id() }} , {{ $event->id }})" class="btn btn-lg btn-success">ثبت نام</a>
                                                        @else
                                                            <a class="btn btn-lg disabled btn-warning"
                                                            href="javascript:void(0)">
                                                                اتمام مهلت ثبت نام</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        {{-- modal --}}
                                        {{-- <div class="modal fade" id="modal-sadra-{{$event->id}}">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content modal-content-demo">
                                                    <div class="modal-header justify-content-center">
                                                        <h4 class="modal-title text-center">
                                                            ثبت نام در: <span class="text-primary">{{$event->title}}</span>
                                                        </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-8">
                                                                    <form class="form-horizontal"
                                                                        action="{{route('company.sadra.register')}}" method="post">
                                                                        @csrf
                                                                        <input type="hidden" name="sadra" value="{{$event->id}}">
                                                                        <div class="row mb-4 justify-content-center">
                                                                            <label class="col-md-3 form-label" for="plan">انتخاب
                                                                                پلن:</label>
                                                                            <div class="col-md-6">
                                                                                <select
                                                                                    class="form-select justify-content-center form-control"
                                                                                    name="plan" id="plan">
                                                                                    <option value="">انتخاب کنید</option>
                                                                                    @foreach($event->buy_plans as $plan)
                                                                                        <option class="form-control"
                                                                                                value="{{$plan->id}}">{{$plan->name}}</option>
                                                                                        @if($plan->map)
                                                                                            <a href="{{asset($plan->map->path)}}">نقشه</a>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </select>
                                                                                @error('plan') <span
                                                                                    class="text-danger">{{$message}}</span> @enderror
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mb-4 justify-content-center">
                                                                            <label class="col-md-3 form-label" for="price">هزینه ثبت نام
                                                                                (تومان):</label>
                                                                            <div class="col-md-6">
                                                                                <span id="price"></span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row mb-4 justify-content-center">
                                                                            <label class="col-md-3 form-label" for="booth">انتخاب
                                                                                غرفه:</label>
                                                                            <div class="col-md-6">
                                                                                <select class="form-select form-control"
                                                                                        name="booth" id="booth">
                                                                                    <option class="form-control" value="">ابتدا پلن
                                                                                        را انتخاب کنید
                                                                                    </option>
                                                                                </select>
                                                                                @error('booth') <span
                                                                                    class="text-danger">{{$message}}</span> @enderror
                                                                            </div>
                                                                        </div>

                                                                        <div class=" row mt-3 justify-content-center ">
                                                                            <div class="col justify-content-center text-center ">
                                                                                <button type="submit"
                                                                                        class="btn btn-success btn-lg px-5"> ثبت نام
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <div class="col-4">
                                                                    <label> نقشه: </label>
                                                                    <a name="map" href="javascript:void(0)">
                                                                        <img style="max-width: 100px;max-height: 100px" name="map"
                                                                            id="map" class="img-fluid" src="" alt="map">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="modal-footer">
                                                        <button
                                                            class="btn btn-light"
                                                            data-bs-dismiss="modal">
                                                            انصراف
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-warning text-center">
                            {{__('public.no_info')}}
                        </div>
                    @endif
                </div>

            </div>
            <div class="mb-5">
                {{$events->links('pagination.panel')}}
            </div>
        </div>
    </div>
    
@endsection
@section('script')
    <script src="{{asset('assets/panel/js/company.js')}}"></script>

    <script>

        let checkCompanySadraUrl = "{{ route('check.company.sadra') }}";
        let registerHref = '{{ route("show.register.sadra", ":id") }}';
        let createInternPositionUrl = "{{ route('create.pos.job') }}";

    </script>

    <script src="{{ asset('assets/panel/js/sadra-events.js') }}"></script>

    {{-- <script>
        $('.register').on('click', function () {
            let id = this.getAttribute('data-bs-target')
            id = id.substring(1)
            let activeModal = document.getElementById(id)
            let planInput = activeModal.querySelector('select[name="plan"]');
            planInput.addEventListener('change', function () {
                let booth = activeModal.querySelector('select[name="booth"]');
                booth.innerHTML = ""
                const plan_id = this.value
                if (plan_id) {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        },
                        url: '{{route('company.ajax')}}',
                        data: {
                            do: 'get-boot-by-plan-id',
                            plan_id: plan_id
                        },
                        dataType: 'json',
                        success: function (response) {
                            let price = document.getElementById('price')
                            price.innerHTML = ""
                            let img = activeModal.querySelector('img[name="map"]');
                            let link = activeModal.querySelector('a[name="map"]');
                            img.src = ""
                            link.href = ""

                            if (response) {
                                let data = ""
                                response['booths'].forEach(function (resp) {
                                    data += '<option class="form-control" value="' + resp['id'] + '">' + resp['name'] + '</option>'
                                })
                                booth.innerHTML = data


                                let imgsrc = '{{asset(':url')}}'
                                imgsrc = imgsrc.replace(':url', response['map'])
                                img.src = imgsrc
                                link.href = imgsrc

                                price.innerHTML = separate(response['price'])
                            }
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
                }
            })
        })
    </script> --}}

@endsection
