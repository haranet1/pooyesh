@extends('panel.company.master')
@section('title','درخواست های همکاری')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/sent-resume-styles.css')}}">
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title">درخواست های همکاری</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">درخواست های همکاری</a></li>
            </ol>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
          <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
          <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه لیست درخواست های استخدامی که برای شما فرستاده شده است نمایش داده میشود.</strong>
            </p>
          </div>
        </div>
    </div>

    <div class="row row-cards justify-content-center px-0 px-md-3">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body px-1 px-md-3">
                    <div class="panel panel-primary">
                        <div class="tab-menu-heading tab-menu-heading-boxed">
                            <div class="tabs-menu-boxed">
                                <ul class="nav panel-tabs overflow-auto d-flex flex-nowrap">
                                    <li><a id="head-tab28" href="#tab1" class="active" style="white-space: nowrap" data-bs-toggle="tab">درخواست های جدید</a></li>
                                    <li><a id="head-tab25" href="#tab2" style="white-space: nowrap" data-bs-toggle="tab">درخواست های پذیرفته شده</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body tabs-menu-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    @if(count($requests) > 0)
                                        <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                                            @foreach ($requests as $request)
                                                <div class="col-12 col-md-4  mb-3">
                                                    <div class="custom-card radius-10 border-0 border-3 border-info shadow-lg">
                                                        <div class="card-body">
                                        
                                                            <!-- Profile picture and short information -->
                                                            <div class="d-flex align-items-center position-relative pb-3">
                                                                <div class="flex-shrink-0">
                                                                    @if($request->sender->photo)
                                                                        <img class="img-fluid rounded " src="{{asset($request->sender->photo->path)}}"
                                                                            alt="avatar">
                                                                    @else
                                                                        <img class="img-md rounded-circle" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="Profile Picture" loading="lazy">
                                                                    @endif
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    @if($request->type == 'intern')
                                                                        <span class="badge bg-primary">کارآموزی</span>
                                                                    @elseif($request->type == 'full-time' || $request->type == 'part-time' || $request->type == 'remote')
                                                                        <span class="badge bg-warning">استخدام</span>
                                                                    @endif
                                                                    @if($request->job)
                                                                        <a target="_blank" href="{{route('job',$request->job->id)}}">
                                                                            <p>موقعیت شغلی: {{$request->job->title}}</p>
                                                                        </a>
                                                                    @endif
                                                                    <div class="mt-2">
                                                                        <h6>{{$request->sender->name." ".$request->sender->family}}</h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <!-- END : Profile picture and short information -->
                                        
                                                            <!-- Options buttons -->
                                                            <div class="mt-3 pt-2 text-center border-top">
                                                                <div class="d-flex justify-content-center gap-3">
                                                                    <a href="{{route('candidate.single',$request->sender_id)}}" target="_blank" class="btn btn-info">
                                                                        <i class="fa fa-eye" aria-hidden="true"></i> مشاهده
                                                                    </a>
                                                                    <input type="hidden" class="resume_id" value="{{$request->id}}">
                                                                    <a href="javascript:void(0)" id="{{$request->id}}" class="btn btn-success accept" type="button">
                                                                        <i class="fa fa-check" aria-hidden="true"></i> تایید
                                                                    </a>
                                                                    <a href="javascript:void(0)" id="{{$request->id}}" class="btn btn-danger decline" type="button">
                                                                        <i class="fa fa-close" aria-hidden="true"></i> رد
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!-- END : Options buttons -->
                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="alert alert-warning text-center">
                                            {{__('public.no_info')}}
                                        </div>
                                    @endif
                                    <div class="mb-5">
                                        {{$requests->links('pagination.panel')}}
                                    </div>
                                </div>

                                <div class="tab-pane " id="tab2">
                                    @if(count($newRequests) > 0)
                                        <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                                            @foreach ($newRequests as $request)
                                                <div class="col-12 col-md-4  mb-3">
                                                    <div class="custom-card radius-10 border-0 border-3 border-info shadow-lg">
                                                        <div class="card-body">
                                        
                                                            <!-- Profile picture and short information -->
                                                            <div class="d-flex align-items-center position-relative pb-3">
                                                                <div class="flex-shrink-0">
                                                                    @if($request->sender->photo)
                                                                        <img class="img-fluid rounded " src="{{asset($request->sender->photo->path)}}"
                                                                            alt="avatar">
                                                                    @else
                                                                        <img class="img-md rounded-circle" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="Profile Picture" loading="lazy">
                                                                    @endif
                                                                </div>
                                                                <div class="flex-grow-1 ms-3">
                                                                    @if($request->type == 'intern')
                                                                        <span class="badge bg-primary">کارآموزی</span>
                                                                    @elseif($request->type == 'full-time' || $request->type == 'part-time' || $request->type == 'remote')
                                                                        <span class="badge bg-warning">استخدام</span>
                                                                    @endif
                                                                    @if($request->job)
                                                                        <a target="_blank" href="{{route('job',$request->job->id)}}">
                                                                            <p>موقعیت شغلی: {{$request->job->title}}</p>
                                                                        </a>
                                                                    @endif
                                                                    <div class="mt-2">
                                                                        <h6>{{$request->sender->name." ".$request->sender->family}}</h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <!-- END : Profile picture and short information -->
                                        
                                                            <!-- Options buttons -->
                                                            <div class="mt-3 pt-2 text-center border-top">
                                                                <div class="d-flex justify-content-center gap-3">
                                                                    <a href="{{route('candidate.single',$request->sender_id)}}" target="_blank" class="btn btn-info">
                                                                        <i class="fa fa-eye" aria-hidden="true"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!-- END : Options buttons -->
                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="alert alert-warning text-center">
                                            {{__('public.no_info')}}
                                        </div>
                                    @endif
                                    <div class="mb-5">
                                        {{$newRequests->links('pagination.panel')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        $('.accept').on('click',function (){
            let id = $(this).attr('id');
            $.ajax({
                headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                },
                url:'{{route('company.ajax')}}',
                type:'post',
                data:{
                    do:'set-cooperation-request-status',
                    status: 1,
                    id:id
                },
                success:function(response) {
                    swal.fire('درخواست همکاری تایید شد')
                    location.reload()
                },
                error:function (response){
                    console.log(response)
                }
            })
        })
// 
        $('.decline').on('click',function (){
            let id = $(this).attr('id');
            $.ajax({
                headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                },
                url:'{{route('company.ajax')}}',
                type:'post',
                data:{
                    do:'set-cooperation-request-status',
                    status: 0,
                    id:id
                },
                success:function(response) {
                    swal.fire('درخواست همکاری رد شد')
                    location.reload()
                },
                error:function (response){
                    console.log(response)
                }
            })
        })
    </script>
@endsection
