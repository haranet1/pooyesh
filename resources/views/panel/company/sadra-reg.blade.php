@extends('panel.company.master')
@section('title','ثبت نام در رویداد')
@section('main')
    <style>
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 2px solid rgba(0, 0, 0, 0.553);
        }
    </style>
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">ثبت نام در رویداد</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-7 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">اطلاعات تکمیلی ثبت نام</h4>
                </div>

                <div class="card-body">
                    <form class="form-horizontal" action="{{route('company.sadra.register')}}" id="form" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="sadra" value="{{$event->id}}">
                        {{-- plan --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="plan">انتخاب پلن:</label>
                            <div class="col-md-6">
                                <select class="form-select justify-content-center form-control" name="plan" id="plan">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($event->buy_plans as $plan)
                                        @if($plan->deleted==0)
                                            <option class="form-control" value="{{$plan->id}}">{{$plan->name}}</option>
                                            @if($plan->map)
                                                <a href="{{asset($plan->map->path)}}">نقشه</a>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                                <span id="verifyPlan" class="text-danger"></span>
                            </div>
                        </div>
                        
                        {{-- booth --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="booth">انتخاب
                                غرفه:</label>
                            <div class="col-md-6">
                                <select class="form-select form-control" name="booth" id="booth">
                                    <option class="form-control" value="">ابتدا پلن را انتخاب کنید</option>
                                </select>
                                <span id="verifyBooth" class="text-danger"></span>
                            </div>
                        </div>

                        {{-- logo --}}
                        <script>let logoCheck = true ;</script>
                        @if (!Auth::user()->groupable->logo_id)
                            <script>logoCheck = false ;</script>
                            <div class="row mb-4 justify-content-center">
                                <label class="col-md-4 form-label" for="logo">لوگو شرکت: </label>
                                <div class="col-md-6">
                                    <div class="wrap-input validate-input input-group">
                                        <input name="logo" id="logo" class="input border-start-0 ms-0 form-control py-3" type="file">
                                    </div>
                                    <span id="verifyLogo" class="text-danger"></span>
                                </div>
                            </div>      
                        @endif

                        {{-- coordinator --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="coordinator">نام مسئول هماهنگی شرکت : </label>
                            <div class="col-md-6">
                                <div class="wrap-input validate-input input-group">
                                    <input id="coordinator" name="coordinator" class="form-control" type="text">
                                </div>
                                <span id="verifyCoordinator" class="text-danger"></span>
                            </div>
                        </div>   

                        {{-- coordinator mobile --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="coordinatorMobile">تلفن همراه مسئول هماهنگی : </label>
                            <div class="col-md-6">
                                <div class="wrap-input validate-input input-group">
                                    <input id="coordinatorMobile" name="coordinatorMobile" class="form-control" numeric type="text">
                                </div>
                                <span id="verifyCoordinatorMobile" class="text-danger"></span>
                            </div>
                        </div>   

                        {{-- contract --}}
                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="contract">وضعیت تفاهم‌نامه همکاری شرکت با دانشگاه :</label>
                            <div class="col-md-6">
                                <select class="form-select justify-content-center form-control" name="contract" id="contract">
                                    <option value="">انتخاب کنید</option>
                                    <option value="تمایل به انعقاد تفاهم‌نامه رایگان با دانشگاه">تمایل به انعقاد تفاهم‌نامه رایگان با دانشگاه</option>
                                    <option value="شرکت قبلا با دانشگاه تفاهم‌نامه همکاری دارد">شرکت قبلا با دانشگاه تفاهم‌نامه همکاری دارد</option>
                                    <option value="عدم تمایل به تفاهم‌نامه همکاری با دانشگاه">عدم تمایل به تفاهم‌نامه همکاری با دانشگاه</option>
                                </select>
                                <span id="verifyContract" class="text-danger"></span>
                            </div>
                        </div>   

                        <div class="row mb-4 justify-content-center">
                            <label class="col-md-4 form-label" for="inviter">معرف :</label>
                            <div class="col-md-6">
                                <select class="form-select justify-content-center form-control" name="inviter" id="inviter">
                                    <option value="">انتخاب کنید</option>
                                    <option class="form-control" value="دکتر ایمان صادق خانی">دکتر ایمان صادق خانی</option>
                                    <option class="form-control" value="دکتر علیرضا نقش">دکتر علیرضا نقش</option>
                                    <option class="form-control" value="دکتر بهنام صغیرزاده">دکتر بهنام صغیرزاده</option>
                                    <option class="form-control" value="دکتر رضا نصوحی">دکتر رضا نصوحی</option>
                                    <option class="form-control" value="دکتر مریم مقیمیان">دکتر مریم مقیمیان</option>
                                    <option class="form-control" value="دکتر احسان روحانی">دکتر احسان روحانی</option>
                                    <option class="form-control" value="دکتر محمد شاهقلی">دکتر محمد شاهقلی</option>
                                    <option class="form-control" value="دکتر محمدرضا یوسفی">دکتر محمدرضا یوسفی</option>
                                    <option class="form-control" value="دکتر مهدی آمون">دکتر مهدی آمون</option>
                                    <option class="form-control" value="دکتر سیدمحمدعلی زنجانی">دکتر سیدمحمدعلی زنجانی</option>
                                    <option class="form-control" value="دکتر خوشنام شجاعی">دکتر خوشنام شجاعی</option>
                                    <option class="form-control" value="دکتر مجید هاشم زاده">دکتر مجید هاشم زاده</option>
                                    <option class="form-control" value="دکتر اکبر نبی اللهی">دکتر اکبر نبی اللهی</option>
                                    <option class="form-control" value="دکتر محمد حججی">دکتر محمد حججی</option>
                                    <option class="form-control" value="دکتر حمید صابری">دکتر حمید صابری</option>
                                    <option class="form-control" value="دکتر فرهام امین شرعی">دکتر فرهام امین شرعی</option>
                                    <option class="form-control" value="دکتر ساسان زندی">دکتر ساسان زندی</option>
                                    <option class="form-control" value="دکتر مهدی منصوری">دکتر مهدی منصوری</option>
                                    <option class="form-control" value="دکتر مهناز مروی">دکتر مهناز مروی</option>
                                    <option class="form-control" value="دکتر الهام ناظمی">دکتر الهام ناظمی</option>
                                    <option class="form-control" value="سامانه فن‌یار">سامانه فن‌یار</option>
                                    <option class="form-control" value="سایر موارد">سایر موارد</option>
                                </select>
                                <span id="verifyInviter" class="text-danger"></span>
                            </div>
                        </div>

                        <hr>

                        <div class="row bb-4 justify-content-center">
                            <p>
                                در سوالات زیر رشته و شاخه مورد نیاز استخدامی شرکت خود را مشخص و تعداد حدودی نیروی کار مورد نیاز را در هر گرایش در کادر زیر آن مشخص فرمایید.
                            </p>
                        </div>
                        {{-- Engineer --}}
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">
                                    <label class="col-12 col-md-2 form-label mb-3">فنی مهندسی : </label>
                                    <label class="col-12 col-md-10 custom-control custom-checkbox-md">
                                        <input type="checkbox" class="custom-control-input" name="dontWantEngineer" id="dontWantEngineer">
                                        <span class="custom-control-label">این شرکت نیاز به نیروی کار در زمینه فنی مهندسی ندارد.</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12 col-md-10">
                                <div class="row mb-4 justify-content-center">
                                    {{-- electrical --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="electricalEngineer" id="electricalEngineer">
                                                    <span class="custom-control-label">مهندسی برق</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="electricalEngineerCount" id="electricalEngineerCount" value="0" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyelectricalEngineer" class="text-danger"></span>
                                    </div>
                                    {{-- mechanical --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="mechanicalEngineer" id="mechanicalEngineer">
                                                    <span class="custom-control-label">مهندسی مکانیک</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="mechanicalEngineerCount" id="mechanicalEngineerCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifymechanicalEngineer" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- material --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="materialEngineer" id="materialEngineer">
                                                    <span class="custom-control-label">مهندسی مواد</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="materialEngineerCount" id="materialEngineerCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifymaterialEngineer" class="text-danger"></span>
                                    </div>
                                    {{-- industriall --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="industriallEngineer" id="industriallEngineer">
                                                    <span class="custom-control-label">مهندسی صنایع</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="industriallEngineerCount" id="industriallEngineerCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyindustriallEngineer" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- civil --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="civilEngineer" id="civilEngineer">
                                                    <span class="custom-control-label">مهندسی عمران</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="civilEngineerCount" id="civilEngineerCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifycivilEngineer" class="text-danger"></span>
                                    </div>
                                    {{-- computer --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="computerEngineer" id="computerEngineer">
                                                    <span class="custom-control-label">مهندسی کامپیوتر</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="computerEngineerCount" id="computerEngineerCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifycomputerEngineer" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- medical --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="medicalEngineer" id="medicalEngineer">
                                                    <span class="custom-control-label">مهندسی پزشکی</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="medicalEngineerCount" id="medicalEngineerCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifymedicalEngineer" class="text-danger"></span>
                                    </div>
                                    {{-- otherCases --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="otherCasesEngineer" id="otherCasesEngineer">
                                                    <span class="custom-control-label">سایر موارد</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="otherCasesEngineerCount" id="otherCasesEngineerCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyotherCasesEngineer" class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                        </div>   

                        <hr>

                        {{-- Humanities --}}
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">
                                    <label class="col-12 col-md-2 form-label mb-3">علوم انسانی : </label>
                                    <label class="col-12 col-md-10 custom-control custom-checkbox-md">
                                        <input type="checkbox" class="custom-control-input" name="dontWantHumanities" id="dontWantHumanities">
                                        <span class="custom-control-label">این شرکت نیازی به استخدام در رشته علوم انسانی ندارد.</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12 col-md-10">
                                <div class="row mb-4 justify-content-center">
                                    {{-- Management --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="management" id="management">
                                                    <span class="custom-control-label">مدیریت</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="managementCount" id="managementCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifymanagement" class="text-danger"></span>
                                    </div>
                                    {{-- Rights --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="rights" id="rights">
                                                    <span class="custom-control-label">حقوق</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="rightsCount" id="rightsCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyrights" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- accounting --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="accounting " id="accounting">
                                                    <span class="custom-control-label">حسابداری</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="accountingCount" id="accountingCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyaccounting" class="text-danger"></span>
                                    </div>
                                    {{-- literature --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="literature" id="literature">
                                                    <span class="custom-control-label">ادبیات فارسی</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="literatureCount" id="literatureCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyliterature" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- English --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="english" id="english">
                                                    <span class="custom-control-label">زبان انگلیسی</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="englishCount" id="englishCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyenglish" class="text-danger"></span>
                                    </div>
                                    {{-- History and geography --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="historyAndGeography" id="historyAndGeography">
                                                    <span class="custom-control-label">تاریخ و جغرافیا</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="historyAndGeographyCount" id="historyAndGeographyCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyhistoryAndGeography" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- otherCases Humanities --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="otherCasesHumanities" id="otherCasesHumanities">
                                                    <span class="custom-control-label">سایر موارد</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="otherCasesHumanitiesCount" id="otherCasesHumanitiesCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyotherCasesHumanities" class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                        </div>   

                        <hr>
                        
                        {{-- Medical sciences --}}
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">
                                    <label class="col-12 col-md-2 form-label mb-3">علوم پزشکی : </label>
                                    <label class="col-12 col-md-10 custom-control custom-checkbox-md">
                                        <input type="checkbox" class="custom-control-input" name="dontWantMedical" id="dontWantMedical">
                                        <span class="custom-control-label">این شرکت نیازی به استخدام در رشته علوم پزشکی ندارد.</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12 col-md-10">
                                <div class="row mb-4 justify-content-center">
                                    {{-- medical --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="medical" id="medical">
                                                    <span class="custom-control-label">پزشکی</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="medicalCount" id="medicalCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifymedical" class="text-danger"></span>
                                    </div>
                                    {{-- nursing --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="nursing" id="nursing">
                                                    <span class="custom-control-label">پرستاری</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="nursingCount" id="nursingCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifynursing" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- Midwifery --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="midwifery" id="midwifery">
                                                    <span class="custom-control-label">مامایی</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="midwiferyCount" id="midwiferyCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifymidwifery" class="text-danger"></span>
                                    </div>
                                    {{-- surgeryRoom --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="surgeryRoom" id="surgeryRoom">
                                                    <span class="custom-control-label">اطاق عمل</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="surgeryRoomCount" id="surgeryRoomCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifysurgeryRoom" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- hygiene --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="hygiene" id="hygiene">
                                                    <span class="custom-control-label">بهداشت</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="hygieneCount" id="hygieneCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyhygiene" class="text-danger"></span>
                                    </div>
                                    {{-- Psychology --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="psychology" id="psychology">
                                                    <span class="custom-control-label">روانشناسی</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="psychologyCount" id="psychologyCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifypsychology" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- otherCases Humanities --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="otherCasesMedical" id="otherCasesMedical">
                                                    <span class="custom-control-label">سایر موارد</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="otherCasesMedicalCount" id="otherCasesMedicalCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyotherCasesMedical" class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        
                        <hr>
                         
                        {{-- Science --}}
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">
                                    <label class="col-12 col-md-2 form-label mb-3">علوم پایه : </label>
                                    <label class="col-12 col-md-10 custom-control custom-checkbox-md">
                                        <input type="checkbox" class="custom-control-input" name="dontWantScience" id="dontWantScience">
                                        <span class="custom-control-label">این شرکت نیازی به استخدام در رشته علوم پایه ندارد.</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12 col-md-10">
                                <div class="row mb-4 justify-content-center">
                                    {{-- Math --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="math" id="math">
                                                    <span class="custom-control-label">ریاضی</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="mathCount" id="mathCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifymath" class="text-danger"></span>
                                    </div>
                                    {{-- physics --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="physics" id="physics">
                                                    <span class="custom-control-label">فیزیک</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="physicsCount" id="physicsCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyphysics" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- chemistry --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="chemistry" id="chemistry">
                                                    <span class="custom-control-label">شیمی</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="chemistryCount" id="chemistryCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifychemistry" class="text-danger"></span>
                                    </div>
                                    {{-- biology --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="biology" id="biology">
                                                    <span class="custom-control-label">زیست شناسی</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="biologyCount" id="biologyCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifybiology" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- otherCases Science --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="otherCasesScience" id="otherCasesScience">
                                                    <span class="custom-control-label">سایر موارد</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="otherCasesScienceCount" id="otherCasesScienceCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyotherCasesScience" class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <hr>

                        {{-- Marketing and sales --}}
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">
                                    <label class="col-12 col-md-2 form-label mb-3" style="white-space: nowrap">بازاریابی و فروش: </label>
                                    <label class="col-12 col-md-10 custom-control custom-checkbox-md">
                                        <input type="checkbox" class="custom-control-input" name="dontWantMarketing" id="dontWantMarketing">
                                        <span class="custom-control-label">این شرکت در زمینه بازاریابی و فروش نیازی ندارد.</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12 col-md-10">
                                <div class="row mb-4 justify-content-center">
                                    {{-- General for all disciplines --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="general" id="general">
                                                    <span class="custom-control-label">عمومی همه رشته ها</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="generalCount" id="generalCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifygeneral" class="text-danger"></span>
                                    </div>
                                    {{-- Expertise in a specific field --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="expertise" id="expertise">
                                                    <span class="custom-control-label">تخصص در گرایش خاص</span>
                                                </label>
                                            </div>
                                            <div class="col-5 px-0">
                                                <input name="expertiseCount" id="expertiseCount" type="text" class="form-control d-none" placeholder="تعداد">
                                            </div>
                                        </div>
                                        <span id="verifyexpertise" class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <hr>

                        {{-- Type of cooperation with the university --}}
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12">
                                <div class="row justify-content-center align-items-center">
                                    <label class="col-12 form-label mb-3">نوع همکاری شرکت با دانشگاه : </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 justify-content-center">
                            <div class="col-12 col-md-10">
                                <div class="row mb-4 justify-content-center">
                                    {{-- Student --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="student">
                                                    <span class="custom-control-label">‌دانشجو(طرح‌پویش)</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- graduate --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="graduate">
                                                    <span class="custom-control-label">فارغ التحصیل</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4 justify-content-center">
                                    {{-- Advice from professors --}}
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="adviceFromProfessors">
                                                    <span class="custom-control-label">مشاوره اساتید</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- otherCases Science --}}
                                    <div class="col-12 col-md-6 mt-4 mt-md-0 px-0">
                                        <div class="row border" style="border-radius: 15px; margin-left: 0.2rem !important; margin-right: 0.2rem !important">
                                            <div class="col px-0">
                                                <label class="custom-control custom-checkbox-md">
                                                    <input type="checkbox" class="custom-control-input" name="otherCasesCooperation">
                                                    <span class="custom-control-label">سایر موارد</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <hr>

                        {{-- submit --}}
                        <div class=" row mt-3 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                                <a href="javascript:void(0)" onclick="Check()" class="btn btn-success btn-lg px-5"> ثبت نام
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-lg-12" >
            <div class="card" style="position: sticky !important; top: 100px">
                <div class="card-header"></div>
                <div class="card-body">
                    {{-- company --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">نام شرکت : </label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span>{{Auth::user()->groupable->name}}</span>
                        </div>
                    </div>
                    {{-- company --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">زمینه فعالیت : </label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span>{{Auth::user()->groupable->activity_field}}</span>
                        </div>
                    </div>
                    {{-- ceo --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">نام مدیر عامل شرکت : </label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span>{{Auth::user()->groupable->ceo}}</span>
                        </div>
                    </div>

                    {{-- map --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">نقشه:</label>
                        <div class="col-6">
                            <a name="map" href="javascript:void(0)" target="_blank">
                                <img style="max-width: 100px;max-height: 200px" name="map" id="map" class="img-fluid border border-danger"  src="">
                            </a>
                            <span class="d-block text-info">روی نقشه کلیک کنید</span>
                        </div>
                    </div>
                    {{-- price --}}
                    <div class="row mb-4 justify-content-center">
                        <label class="col-md-6 form-label" for="price">هزینه ثبت نام
                            (تومان) :</label>
                        <div class="col-md-6 d-flex align-items-center">
                            <span id="price"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        let plan = document.getElementById('plan');
        plan.addEventListener('change', function(){
            let planValue = document.getElementById('plan').value;
            let booth = document.getElementById('booth');
            booth.innerHTML = "";

            if(planValue){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    url: "{{route('company.ajax.sadra.get.booth')}}",
                    data: {
                        id : planValue
                    },
                    dataType: "json",
                    success: function (response) {
                        let price = document.getElementById('price');
                        price.innerHTML = "";
                        let img = document.querySelector('img[name="map"]');
                        let link = document.querySelector('a[name="map"]');
                        img.src = "";
                        link.href = "";

                        if(response){
                            let data = ""
                            response['booths'].forEach(function(resp){
                                data += '<option class="form-control" value="' + resp['id'] + '">' + resp['name'] + '</option>'
                            });
                            booth.innerHTML = data;

                            let imgsrc = '{{asset(':url')}}';
                            imgsrc = imgsrc.replace(':url', response['map']);
                            img.src = imgsrc
                            link.href = imgsrc;

                            price.innerHTML = response['price'];
                        }
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            }
        });
    </script>

    <script>
        // Engineer
            let electricalEngineer = document.getElementById('electricalEngineer');
            let electricalEngineerCount = document.getElementById('electricalEngineerCount');
            electricalEngineer.addEventListener('change', function(){
                if(electricalEngineer.checked) electricalEngineerCount.classList.remove('d-none');
                else electricalEngineerCount.classList.add('d-none');
            });

            let mechanicalEngineer = document.getElementById('mechanicalEngineer');
            let mechanicalEngineerCount = document.getElementById('mechanicalEngineerCount');
            mechanicalEngineer.addEventListener('change', function(){
                if(mechanicalEngineer.checked) mechanicalEngineerCount.classList.remove('d-none');
                else mechanicalEngineerCount.classList.add('d-none');
            });

            let materialEngineer = document.getElementById('materialEngineer');
            let materialEngineerCount = document.getElementById('materialEngineerCount');
            materialEngineer.addEventListener('change', function(){
            if(materialEngineer.checked) materialEngineerCount.classList.remove('d-none');
            else materialEngineerCount.classList.add('d-none');
            });
            
            let industriallEngineer = document.getElementById('industriallEngineer');
            let industriallEngineerCount = document.getElementById('industriallEngineerCount');
            industriallEngineer.addEventListener('change', function(){
                if(industriallEngineer.checked) industriallEngineerCount.classList.remove('d-none');
                else industriallEngineerCount.classList.add('d-none');
            });

            let civilEngineer = document.getElementById('civilEngineer');
            let civilEngineerCount = document.getElementById('civilEngineerCount');
            civilEngineer.addEventListener('change', function(){
                if(civilEngineer.checked) civilEngineerCount.classList.remove('d-none');
                else civilEngineerCount.classList.add('d-none');
            });
            
            let computerEngineer = document.getElementById('computerEngineer');
            let computerEngineerCount = document.getElementById('computerEngineerCount');
            computerEngineer.addEventListener('change', function(){
                if(computerEngineer.checked) computerEngineerCount.classList.remove('d-none');
                else computerEngineerCount.classList.add('d-none');
            });
            
            let medicalEngineer = document.getElementById('medicalEngineer');
            let medicalEngineerCount = document.getElementById('medicalEngineerCount');
            medicalEngineer.addEventListener('change', function(){
                if(medicalEngineer.checked) medicalEngineerCount.classList.remove('d-none');
                else medicalEngineerCount.classList.add('d-none');
            });
            
            let otherCasesEngineer = document.getElementById('otherCasesEngineer');
            let otherCasesEngineerCount = document.getElementById('otherCasesEngineerCount');
            otherCasesEngineer.addEventListener('change', function(){
                if(otherCasesEngineer.checked) otherCasesEngineerCount.classList.remove('d-none');
                else otherCasesEngineerCount.classList.add('d-none');
            });

            let dontWantEngineer = document.getElementById('dontWantEngineer');
            dontWantEngineer.addEventListener('change', function(){
                
                if(dontWantEngineer.checked){
                    electricalEngineer.disabled = true;
                    mechanicalEngineer.disabled = true;
                    materialEngineer.disabled = true;
                    industriallEngineer.disabled = true;
                    civilEngineer.disabled = true;
                    computerEngineer.disabled = true;
                    medicalEngineer.disabled = true;
                    otherCasesEngineer.disabled = true;
                }
                if(!dontWantEngineer.checked){
                    electricalEngineer.disabled = false;
                    mechanicalEngineer.disabled = false;
                    materialEngineer.disabled = false;
                    industriallEngineer.disabled = false;
                    civilEngineer.disabled = false;
                    computerEngineer.disabled = false;
                    medicalEngineer.disabled = false;
                    otherCasesEngineer.disabled = false;
                }
            });
        // end Engineer

        // Humanities
            let management = document.getElementById('management');
            let managementCount = document.getElementById('managementCount');
            management.addEventListener('change', function(){
                if(management.checked) managementCount.classList.remove('d-none');
                else managementCount.classList.add('d-none');
            });

            let rights = document.getElementById('rights');
            let rightsCount = document.getElementById('rightsCount');
            rights.addEventListener('change', function(){
                if(rights.checked) rightsCount.classList.remove('d-none');
                else rightsCount.classList.add('d-none');
            });

            let accounting = document.getElementById('accounting');
            let accountingCount = document.getElementById('accountingCount');
            accounting.addEventListener('change', function(){
                if(accounting.checked) accountingCount.classList.remove('d-none');
                else accountingCount.classList.add('d-none');
            });

            let literature = document.getElementById('literature');
            let literatureCount = document.getElementById('literatureCount');
            literature.addEventListener('change', function(){
                if(literature.checked) literatureCount.classList.remove('d-none');
                else literatureCount.classList.add('d-none');
            });

            let english = document.getElementById('english');
            let englishCount = document.getElementById('englishCount');
            english.addEventListener('change', function(){
                if(english.checked) englishCount.classList.remove('d-none');
                else englishCount.classList.add('d-none');
            });

            let historyAndGeography = document.getElementById('historyAndGeography');
            let historyAndGeographyCount = document.getElementById('historyAndGeographyCount');
            historyAndGeography.addEventListener('change', function(){
                if (historyAndGeography.checked) historyAndGeographyCount.classList.remove('d-none');
                else historyAndGeographyCount.classList.add('d-none');
            });

            let otherCasesHumanities = document.getElementById('otherCasesHumanities');
            let otherCasesHumanitiesCount = document.getElementById('otherCasesHumanitiesCount');
            otherCasesHumanities.addEventListener('change', function(){
                if(otherCasesHumanities.checked) otherCasesHumanitiesCount.classList.remove('d-none');
                else otherCasesHumanitiesCount.classList.add('d-none');
            });

            
            let dontWantHumanities = document.getElementById('dontWantHumanities');
            dontWantHumanities.addEventListener('change', function(){
            if(dontWantHumanities.checked){
                    management.disabled = true;     
                    rights.disabled = true;     
                    accounting.disabled = true;     
                    literature.disabled = true;     
                    english.disabled = true;     
                    historyAndGeography.disabled = true;     
                    otherCasesHumanities.disabled = true;
            }
            if(!dontWantHumanities.checked){
                    management.disabled = false;     
                    rights.disabled = false;     
                    accounting.disabled = false;     
                    literature.disabled = false;     
                    english.disabled = false;     
                    historyAndGeography.disabled = false;     
                    otherCasesHumanities.disabled = false;
            }
            });
        // end Humanities

        // Medical sciences
            let medical = document.getElementById('medical');
            let medicalCount = document.getElementById('medicalCount');
            medical.addEventListener('change', function(){
                if(medical.checked) medicalCount.classList.remove('d-none');
                else medicalCount.classList.add('d-none');
            });

            let nursing = document.getElementById('nursing');
            let nursingCount = document.getElementById('nursingCount');
            nursing.addEventListener('change', function(){
                if(nursing.checked) nursingCount.classList.remove('d-none');
                else nursingCount.classList.add('d-none'); 
            });

            let midwifery = document.getElementById('midwifery');
            let midwiferyCount = document.getElementById('midwiferyCount');
            midwifery.addEventListener('change', function(){
                if(midwifery.checked) midwiferyCount.classList.remove('d-none');
                else midwiferyCount.classList.add('d-none');
            });

            let surgeryRoom = document.getElementById('surgeryRoom');
            let surgeryRoomCount = document.getElementById('surgeryRoomCount');
            surgeryRoom.addEventListener('change', function(){
                if(surgeryRoom.checked) surgeryRoomCount.classList.remove('d-none');
                else surgeryRoomCount.classList.add('d-none');
            });

            let hygiene = document.getElementById('hygiene');
            let hygieneCount = document.getElementById('hygieneCount');
            hygiene.addEventListener('change', function(){
                if(hygiene.checked) hygieneCount.classList.remove('d-none');
                else hygieneCount.classList.add('d-none');
            });

            let psychology = document.getElementById('psychology');
            let psychologyCount = document.getElementById('psychologyCount');
            psychology.addEventListener('change', function(){
                if(psychology.checked) psychologyCount.classList.remove('d-none');
                else psychologyCount.classList.add('d-none');
            });

            let otherCasesMedical = document.getElementById('otherCasesMedical');
            let otherCasesMedicalCount = document.getElementById('otherCasesMedicalCount');
            otherCasesMedical.addEventListener('change', function(){
                if(otherCasesMedical.checked) otherCasesMedicalCount.classList.remove('d-none');
                else otherCasesMedicalCount.classList.add('d-none');
            });

            let dontWantMedical = document.getElementById('dontWantMedical');
            dontWantMedical.addEventListener('change', function(){
                if(dontWantMedical.checked){
                    medical.disabled = true;
                    nursing.disabled = true;
                    midwifery.disabled = true;
                    surgeryRoom.disabled = true;
                    hygiene.disabled = true;
                    psychology.disabled = true;
                    otherCasesMedical.disabled = true;
                }else{
                    medical.disabled = false;
                    nursing.disabled = false;
                    midwifery.disabled = false;
                    surgeryRoom.disabled = false;
                    hygiene.disabled = false;
                    psychology.disabled = false;
                    otherCasesMedical.disabled = false;
                }
            });
        // end Medical sciences
        
        // Science
            let math = document.getElementById('math');
            let mathCount = document.getElementById('mathCount');
            math.addEventListener('change' , function(){
                if(math.checked) mathCount.classList.remove('d-none');
                else mathCount.classList.add('d-none');
            });

            let physics = document.getElementById('physics');
            let physicsCount = document.getElementById('physicsCount');
            physics.addEventListener('change' , function(){
                if(physics.checked) physicsCount.classList.remove('d-none');
                else physicsCount.classList.add('d-none');
            });

            let chemistry = document.getElementById('chemistry');
            let chemistryCount = document.getElementById('chemistryCount');
            chemistry.addEventListener('change' , function(){
                if(chemistry.checked) chemistryCount.classList.remove('d-none');
                else chemistryCount.classList.add('d-none');
            });

            let biology = document.getElementById('biology');
            let biologyCount = document.getElementById('biologyCount');
            biology.addEventListener('change' , function(){
                if(biology.checked) biologyCount.classList.remove('d-none');
                else biologyCount.classList.add('d-none');
            });

            let otherCasesScience = document.getElementById('otherCasesScience');
            let otherCasesScienceCount = document.getElementById('otherCasesScienceCount');
            otherCasesScience.addEventListener('change' , function(){
                if(otherCasesScience.checked) otherCasesScienceCount.classList.remove('d-none');
                else otherCasesScienceCount.classList.add('d-none');
            });

            let dontWantScience = document.getElementById('dontWantScience');
            dontWantScience.addEventListener('change' , function(){
                if(dontWantScience.checked){
                    math.disabled = true;
                    physics.disabled = true;
                    chemistry.disabled = true;
                    biology.disabled = true;
                    otherCasesScience.disabled = true;
                }else{
                    math.disabled = false;
                    physics.disabled = false;
                    chemistry.disabled = false;
                    biology.disabled = false;
                    otherCasesScience.disabled = false;
                }
            });
            
        // end Science

        // Marketing and sales
            let general = document.getElementById('general');
            let generalCount = document.getElementById('generalCount');
            general.addEventListener('change', function(){
                if(general.checked) generalCount.classList.remove('d-none');
                else generalCount.classList.add('d-none');
            });

            let expertise = document.getElementById('expertise');
            let expertiseCount = document.getElementById('expertiseCount');
            expertise.addEventListener('change', function(){
                if(expertise.checked) expertiseCount.classList.remove('d-none');
                else expertiseCount.classList.add('d-none');
            });

            let dontWantMarketing = document.getElementById('dontWantMarketing');
            dontWantMarketing.addEventListener('change', function(){
                if(dontWantMarketing.checked){
                    general.disabled = true;
                    expertise.disabled = true;
                }else{
                    general.disabled = false;
                    expertise.disabled = false;
                }
            });
        // end Marketing and sales
        

        function Check(){
            let noError = true;
            let form = document.getElementById('form');
            let plan = document.getElementById('plan');
            let booth = document.getElementById('booth');
            let coordinator = document.getElementById('coordinator');
            let coordinatorMobile = document.getElementById('coordinatorMobile');
            let contract = document.getElementById('contract');
            let inviter = document.getElementById('inviter');
            if(!logoCheck){
                let logo = document.getElementById('logo');
                let verifyLogo = document.getElementById('verifyLogo');
                verifyLogo.innerHTML = "";
            }
            // verify span 
                let verifyPlan = document.getElementById('verifyPlan');
                verifyPlan.innerHTML = "";
                let verifyBooth = document.getElementById('verifyBooth');
                verifyBooth.innerHTML = "";
                let verifyCoordinator = document.getElementById('verifyCoordinator');
                verifyCoordinator.innerHTML = "";
                let verifyCoordinatorMobile = document.getElementById('verifyCoordinatorMobile');
                verifyCoordinatorMobile.innerHTML = "";
                let verifyContract = document.getElementById('verifyContract');
                verifyContract.innerHTML = "";
                let verifyInviter = document.getElementById('verifyInviter');
                verifyInviter.innerHTML = "";
                let verifyelectricalEngineer = document.getElementById('verifyelectricalEngineer');
                verifyelectricalEngineer.innerHTML = "";
                let verifymechanicalEngineer = document.getElementById('verifymechanicalEngineer');
                verifymechanicalEngineer.innerHTML = "";
                let verifymaterialEngineer = document.getElementById('verifymaterialEngineer');
                verifymaterialEngineer.innerHTML = "";
                let verifyindustriallEngineer = document.getElementById('verifyindustriallEngineer');
                verifyindustriallEngineer.innerHTML = "";
                let verifycivilEngineer = document.getElementById('verifycivilEngineer');
                verifycivilEngineer.innerHTML = "";
                let verifycomputerEngineer = document.getElementById('verifycomputerEngineer');
                verifycomputerEngineer.innerHTML = "";
                let verifymedicalEngineer = document.getElementById('verifymedicalEngineer');
                verifymedicalEngineer.innerHTML = "";
                let verifyotherCasesEngineer = document.getElementById('verifyotherCasesEngineer');
                verifyotherCasesEngineer.innerHTML = "";
                let verifymanagement = document.getElementById('verifymanagement');
                verifymanagement.innerHTML = "";
                let verifyrights = document.getElementById('verifyrights');
                verifyrights.innerHTML = "";
                let verifyaccounting = document.getElementById('verifyaccounting');
                verifyaccounting.innerHTML = "";
                let verifyliterature = document.getElementById('verifyliterature');
                verifyliterature.innerHTML = "";
                let verifyenglish = document.getElementById('verifyenglish');
                verifyenglish.innerHTML = "";
                let verifyhistoryAndGeography = document.getElementById('verifyhistoryAndGeography');
                verifyhistoryAndGeography.innerHTML = "";
                let verifyotherCasesHumanities = document.getElementById('verifyotherCasesHumanities');
                verifyotherCasesHumanities.innerHTML = "";
                let verifymedical = document.getElementById('verifymedical');
                verifymedical.innerHTML = "";
                let verifynursing = document.getElementById('verifynursing');
                verifynursing.innerHTML = "";
                let verifymidwifery = document.getElementById('verifymidwifery');
                verifymidwifery.innerHTML = "";
                let verifysurgeryRoom = document.getElementById('verifysurgeryRoom');
                verifysurgeryRoom.innerHTML = "";
                let verifyhygiene = document.getElementById('verifyhygiene');
                verifyhygiene.innerHTML = "";
                let verifypsychology = document.getElementById('verifypsychology')
                verifypsychology.innerHTML = "";
                let verifyotherCasesMedical = document.getElementById('verifyotherCasesMedical')
                verifyotherCasesMedical.innerHTML = "";
                let verifymath = document.getElementById('verifymath')
                verifymath.innerHTML = "";
                let verifyphysics = document.getElementById('verifyphysics')
                verifyphysics.innerHTML = "";
                let verifychemistry = document.getElementById('verifychemistry')
                verifychemistry.innerHTML = "";
                let verifybiology = document.getElementById('verifybiology')
                verifybiology.innerHTML = "";
                let verifyotherCasesScience = document.getElementById('verifyotherCasesScience')
                verifyotherCasesScience.innerHTML = "";
                let verifygeneral = document.getElementById('verifygeneral')
                verifygeneral.innerHTML = "";
                let verifyexpertise = document.getElementById('verifyexpertise')
                verifyexpertise.innerHTML = "";
            // end verify span

            // plan part 
                //plan
                    if(plan.value == ""){
                        verifyPlan.innerHTML = "لطفا پلن مورد نظر خود را انتخاب کنید.";
                        window.scrollTo(0, 0);
                        noError = false;
                    }
                //booth
                    if(booth.value == ""){
                        verifyBooth.innerHTML = "لطفا غرفه مورد نظر خود را انتخاب کنید."
                        window.scrollTo(0, 0);
                        noError = false;
                    }else{
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': '{{csrf_token()}}'
                            },
                            url: "{{route('company.ajax.sadra.cheak.booth.reserved')}}",
                            data: {
                                boothId : booth.value
                            },
                            dataType: "json",
                            success: function (response) {
                                console.log(response);
                                if(response == 0){
                                    verifyBooth.innerHTML = "غرفه مورد نظر شما رزور شده است."
                                    window.scrollTo(0, 0);
                                    noError = false;
                                }
                            },
                            error: function (response) {
                                console.log(response)
                            }
                        });
                    }
                    
                //logo
                    if(!logoCheck){
                        if(logo.value == ""){
                            verifyLogo.innerHTML = "لطفا لوگو شرکت خود را بارگزاری کنید.";
                            window.scrollTo(0, 0);
                            noError = false;
                        }
                    }
                //Coordinator
                    if(coordinator.value == ""){
                        verifyCoordinator.innerHTML = "لطفا نام مسئول هماهنگی شرکت را وارد کنید.";
                        window.scrollTo(0, 0);
                        noError = false;
                    }
                //CoordinatorMobile
                    if(coordinatorMobile.value == ""){
                        verifyCoordinatorMobile.innerHTML = "لطفا تلفن همراه مسئول هماهنگی را وارد کنید.";
                        window.scrollTo(0, 0);
                        noError = false;
                    }
                    if(isNaN(coordinatorMobile.value)){
                        verifyCoordinatorMobile.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 0);
                        noError = false;
                    }
                // Inviter
                    if(inviter.value == ""){
                        verifyInviter.innerHTML = "لطفا مسئول هماهنگ کننده را انتخاب کنید."
                        window.scrollTo(0, 0);
                        noError = false;
                    }
                // Contract
                    if(contract.value == ""){
                        verifyContract.innerHTML = "لطفا وضعیت تفاهم‌نامه را انتخاب کنید.";
                        window.scrollTo(0, 0);
                        noError = false;
                    }
            // end plan part

            // Engineer
                // electricalEngineer
                    if(electricalEngineer.checked && electricalEngineerCount.value == "0"){
                        verifyelectricalEngineer.innerHTML = "لطفا تعداد دانشجو گرایش مهندسی برق را وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                    if(electricalEngineer.checked && isNaN(electricalEngineerCount.value)){
                        verifyelectricalEngineer.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                
                // mechanicalEngineer
                    if(mechanicalEngineer.checked && mechanicalEngineerCount.value == ""){
                        verifymechanicalEngineer.innerHTML = "لطفا تعداد دانشجو گرایش مهندسی مکانیک را وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                    if(mechanicalEngineer.checked && isNaN(mechanicalEngineerCount.value)){
                        verifymechanicalEngineer.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                // materialEngineer
                    if(materialEngineer.checked && materialEngineerCount.value == ""){
                        verifymaterialEngineer.innerHTML = "لطفا تعداد دانشجو گرایش مهندسی مواد را وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                    if(materialEngineer.checked && isNaN(materialEngineerCount.value)){
                        verifymaterialEngineer.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                // industriallEngineer
                    if(industriallEngineer.checked && industriallEngineerCount.value == ""){
                        verifyindustriallEngineer.innerHTML = "لطفا تعداد دانشجو گرایش مهندسی صنایع را وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                    if(industriallEngineer.checked && isNaN(industriallEngineerCount.value)){
                        verifyindustriallEngineer.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                // civilEngineer
                    if(civilEngineer.checked && civilEngineerCount.value == ""){
                        verifycivilEngineer.innerHTML = "لطفا تعداد دانشجو گرایش مهندسی عمران را وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                    if(civilEngineer.checked && isNaN(civilEngineerCount.value)){
                        verifycivilEngineer.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                // computerEngineer
                    if(computerEngineer.checked && computerEngineerCount.value == ""){
                        verifycomputerEngineer.innerHTML = "لطفا تعداد دانشجو گرایش مهندسی کامپیوتر را وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                    if(computerEngineer.checked && isNaN(computerEngineerCount.value)){
                        verifycomputerEngineer.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                // medicalEngineer
                    if(medicalEngineer.checked && medicalEngineerCount.value == ""){
                        verifymedicalEngineer.innerHTML = "لطفا تعداد دانشجو گرایش مهندسی پزشکی را وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                    if(medicalEngineer.checked && isNaN(medicalEngineerCount.value)){
                        verifymedicalEngineer.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                // otherCasesEngineer
                    if(otherCasesEngineer.checked && otherCasesEngineerCount.value == ""){
                        verifyotherCasesEngineer.innerHTML = "لطفا تعداد سایر موارد فنی مهندسی را وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
                    if(otherCasesEngineer.checked && isNaN(otherCasesEngineerCount.value)){
                        verifyotherCasesEngineer.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 500);
                        noError = false;
                    }
            // end Engineer

            // Humanities
                // management
                    if(management.checked && managementCount.value == ""){
                        verifymanagement.innerHTML = "لطفا تعداد دانشجو گرایش مدیریت را وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                    if(management.checked && isNaN(managementCount.value)){
                        verifymanagement.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                // rights
                    if(rights.checked && rightsCount.value == ""){
                        verifyrights.innerHTML = "لطفا تعداد دانشجو گرایش حقوق را وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                    if(rights.checked && isNaN(rightsCount.value)){
                        verifyrights.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                // accounting
                    if(accounting.checked && accountingCount.value == ""){
                        verifyaccounting.innerHTML = "لطفا تعداد دانشجو گرایش حسابداری را وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                    if(accounting.checked && isNaN(accountingCount.value)){
                        verifyaccounting.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                // literature
                    if(literature.checked && literatureCount.value == ""){
                        verifyliterature.innerHTML = "لطفا تعداد دانشجو گرایش ادبیات فارسی را وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                    if(literature.checked && isNaN(literatureCount.value)){
                        verifyliterature.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                // english
                    if(english.checked && englishCount.value == ""){
                        verifyenglish.innerHTML = "لطفا تعداد دانشجو گرایش انگلیسی را وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                    if(english.checked && isNaN(englishCount.value)){
                        verifyenglish.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                // historyAndGeography
                    if(historyAndGeography.checked && historyAndGeographyCount.value == ""){
                        verifyhistoryAndGeography.innerHTML = "لطفا تعداد دانشجو گرایش تاریخ و جغرافیا را وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                    if(historyAndGeography.checked && isNaN(historyAndGeographyCount.value)){
                        verifyhistoryAndGeography.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                // otherCasesHumanities
                    if(otherCasesHumanities.checked && otherCasesHumanitiesCount.value == ""){
                        verifyotherCasesHumanities.innerHTML = "لطفا تعداد سایر موارد علوم انسانی را وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
                    if(otherCasesHumanities.checked && isNaN(otherCasesHumanitiesCount.value)){
                        verifyotherCasesHumanities.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 800);
                        noError = false;
                    }
            // end Humanities
            
            // Medical sciences
                // medical
                    if(medical.checked && medicalCount.value == ""){
                        verifymedical.innerHTML = "لطفا تعداد دانشجو گرایش پزشکی را وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                    if(medical.checked && isNaN(medicalCount.value)){
                        verifymedical.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                // nursing
                    if(nursing.checked && nursingCount.value == ""){
                        verifynursing.innerHTML = "لطفا تعداد دانشجو گرایش پرستاری را وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                    if(nursing.checked && isNaN(nursingCount.value)){
                        verifynursing.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                // midwifery
                    if(midwifery.checked && midwiferyCount.value == ""){
                        verifymidwifery.innerHTML = "لطفا تعداد دانشجو گرایش مامایی را وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                    if(midwifery.checked && isNaN(midwiferyCount.value)){
                        verifymidwifery.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                // surgeryRoom
                    if(surgeryRoom.checked && surgeryRoomCount.value == ""){
                        verifysurgeryRoom.innerHTML = "لطفا تعداد دانشجو گرایش اطاق عمل را وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                    if(surgeryRoom.checked && isNaN(surgeryRoomCount.value)){
                        verifysurgeryRoom.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                // hygiene
                    if(hygiene.checked && hygieneCount.value == ""){
                        verifyhygiene.innerHTML = "لطفا تعداد دانشجو گرایش بهداشت را وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                    if(hygiene.checked && isNaN(hygieneCount.value)){
                        verifyhygiene.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                // psychology
                    if(psychology.checked && psychologyCount.value == ""){
                        verifypsychology.innerHTML = "لطفا تعداد دانشجو گرایش روانشناسی را وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                    if(psychology.checked && isNaN(psychologyCount.value)){
                        verifypsychology.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                // otherCasesMedical
                    if(otherCasesMedical.checked && otherCasesMedicalCount.value == ""){
                        verifyotherCasesMedical.innerHTML = "لطفا تعداد سایر موارد علوم پزشکی را وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
                    if(otherCasesMedical.checked && isNaN(otherCasesMedicalCount.value)){
                        verifyotherCasesMedical.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1100);
                        noError = false;
                    }
            // end Medical sciences

            // Science
                // math
                    if(math.checked && mathCount.value == ""){
                        verifymath.innerHTML = "لطفا تعداد دانشجو گرایش ریاضی را وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }
                    if(math.checked && isNaN(mathCount.value)){
                        verifymath.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }    
                // physics
                    if(physics.checked && physicsCount.value == ""){
                        verifyphysics.innerHTML = "لطفا تعداد دانشجو گرایش فیزیک را وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }
                    if(physics.checked && isNaN(physicsCount.value)){
                        verifyphysics.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }    
                // chemistry
                    if(chemistry.checked && chemistryCount.value == ""){
                        verifychemistry.innerHTML = "لطفا تعداد دانشجو گرایش شیمی را وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }
                    if(chemistry.checked && isNaN(chemistryCount.value)){
                        verifychemistry.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }    
                // biology
                    if(biology.checked && biologyCount.value == ""){
                        verifybiology.innerHTML = "لطفا تعداد دانشجو گرایش زیست شناسی را وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }
                    if(biology.checked && isNaN(biologyCount.value)){
                        verifybiology.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }    
                // otherCasesScience
                    if(otherCasesScience.checked && otherCasesScienceCount.value == ""){
                        verifyotherCasesScience.innerHTML = "لطفا تعداد سایر موارد علوم پایه را وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }
                    if(otherCasesScience.checked && isNaN(otherCasesScienceCount.value)){
                        verifyotherCasesScience.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1400);
                        noError = false;
                    }    
            // end Science
            
            // Marketing and sales
                // general
                    if(general.checked && generalCount.value == ""){
                        verifygeneral.innerHTML = "لطفا تعداد دانشجو گرایش ریاضی را وارد کنید.";
                        window.scrollTo(0, 1700);
                        noError = false;
                    }
                    if(general.checked && isNaN(generalCount.value)){
                        verifygeneral.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1700);
                        noError = false;
                    }  
                // expertise
                    if(expertise.checked && expertiseCount.value == ""){
                        verifyexpertise.innerHTML = "لطفا تعداد دانشجو گرایش ریاضی را وارد کنید.";
                        window.scrollTo(0, 1700);
                        noError = false;
                    }
                    if(expertise.checked && isNaN(expertiseCount.value)){
                        verifyexpertise.innerHTML = "لطفا مقدار عددی وارد کنید.";
                        window.scrollTo(0, 1700);
                        noError = false;
                    }  
                
            // end Marketing and sales

                    
            if(noError) form.submit();
            
        }
    </script>
@endsection
