@extends('panel.company.master')
@section('title','لیست پروژه ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست پروژه ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">

            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="card-header border-bottom-0">

                    <div class=" ">
                        <a class="btn btn-success" href="{{route('company.projects.create')}}">افزودن پروژه جدید</a>
                        <a class="btn btn-primary" href="{{route('company.project-cats.create')}}">افزودن دسته بندی جدید</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($projects) > 0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>عنوان </th>
                                <th>سفارش دهنده</th>
                                <th>بودجه</th>
                                <th>توضیحات</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                            <tr>
                                <td class="text-nowrap align-middle">{{$project->title}}</td>
                                <td class="text-nowrap align-middle">{{$project->owner->name." ".$project->owner->family}}</td>
                                <td class="text-nowrap align-middle"><span>{{$project->budget}}</span></td>
                                <td class="text-nowrap align-middle">{{Str::limit($project->description,'20','...')}}</td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="{{route('projects.single',[$project->id,$project->title])}}" class="btn btn-sm btn-primary badge" type="button">مشاهده </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif

                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$projects->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
