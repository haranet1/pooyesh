@extends('panel.company.master')
@section('title','ویرایش محصول/خدمات')
@section('main')
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ویرایش محصول/خدمات</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">ویرایش محصول/خدمات</h3>
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal" action="{{route('company.products.update',$product)}}" enctype="multipart/form-data" method="POST">
                                @csrf
                                <div class=" row mb-4">
                                    <label class="col-md-3 form-label">عنوان</label>
                                    <div class="col-md-9">
                                        <input value="{{$product->name}}" name="name" type="text" class="form-control" >
                                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                                <div class=" row mb-4">
                                    <label class="col-md-3 form-label">تصویر</label>
                                    <div class="col-md-9">
                                        <input name="photo" type="file" class="form-control" >
                                        @error('photo') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">نوع</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-select" name="type" id="">
                                            <option @if($product->type=='products') selected @endif  value="products">محصول</option>
                                            <option @if($product->type=='service') selected @endif value="service">خدمات</option>
                                        </select>
                                        @error('type') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>

                                <div class=" row mb-4">
                                    <label class="col-md-3 form-label">توضیحات</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="description" id="" cols="30" rows="10">
                                            {{$product->description}}
                                        </textarea>
                                        @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>

                                <div class=" row mb-0 justify-content-center ">
                                    <div class="col justify-content-center text-center ">
                                        <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
