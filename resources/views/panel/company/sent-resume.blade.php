@extends('panel.company.master')
@section('title' , 'لیست رزومه ها')
@section('css')
    {{-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" /> --}}
    <link rel="stylesheet" href="{{asset('assets/panel/css/sent-resume-styles.css')}}">
@endsection

@section('main')
    <div class="page-header">
        <h1 class="page-title">لیست درخواست های اشتغال</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست درخواست های اشتغال</a></li>
            </ol>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
          <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
          <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه لیست درخواست های استخدامی که برای شما فرستاده شده است نمایش داده میشود.</strong>
            </p>
          </div>
        </div>
    </div>

    <div class="row row-cards justify-content-center px-0 px-md-3">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body px-1 px-md-3">
                    <div class="panel panel-primary">
                        <div class="tab-menu-heading tab-menu-heading-boxed">
                            <div class="tabs-menu-boxed">
                                <ul class="nav panel-tabs overflow-auto d-flex flex-nowrap">
                                    <li><a id="head-tab11" href="#tab1" class="active" style="white-space: nowrap" data-bs-toggle="tab">درخواست های جدید</a></li>
                                    <li><a id="head-tab12" href="#tab2" style="white-space: nowrap" data-bs-toggle="tab">درخواست های پذیرفته شده</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body tabs-menu-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    @if (count($requests) > 0)
                                        <div class="container">
                                            <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                                                @foreach ($requests as $request)
                                                    <div class="col-12 col-md-4  mb-3">
                                                        <div class="custom-card radius-10 border-0 border-3 border-info shadow-lg">
                                                            <div class="card-body">
                                            
                                                                <!-- Profile picture and short information -->
                                                                <div class="d-flex align-items-center position-relative pb-3">
                                                                    <div class="flex-shrink-0">
                                                                        <img class="img-md rounded-circle" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="Profile Picture" loading="lazy">
                                                                    </div>
                                                                    <div class="flex-grow-1 ms-3">
                                                                        <a href="#" class="h5 stretched-link btn-link">{{$request->sender->name}} {{$request->sender->family}}</a>
                                                                        <p class="text-muted m-0">{{$request->sender->groupable->grade}} {{$request->sender->groupable->major}}</p>
                                                                    </div>
                                                                </div>
                                                                @if ($request->sender->groupable)
                                                                    <p class="mb-0 font-13 text-gray">
                                                                        از 
                                                                        @if ($request->sender->groupable->province)
                                                                            استان
                                                                            {{$request->sender->groupable->province->name}}
                                                                        @endif
                                                                        @if ($request->sender->groupable->city)
                                                                            شهر
                                                                            {{$request->sender->groupable->city->name}}
                                                                        @endif
                                                                    </p>
                                                                @endif
                                                                <!-- END : Profile picture and short information -->
                                            
                                                                <!-- Options buttons -->
                                                                <div class="mt-3 pt-2 text-center border-top">
                                                                    <div class="d-flex justify-content-center gap-3">
                                                                        <a href="{{route('candidate.single',$request->sender_id)}}" target="_blank" class="btn btn-info">
                                                                            <i class="fa fa-eye" aria-hidden="true"></i> مشاهده
                                                                        </a>
                                                                        <input type="hidden" class="resume_id" value="{{$request->id}}">
                                                                        <a href="javascript:void(0)" class="btn btn-success" onclick="acceptResume({{$request->id}})">
                                                                            <i class="fa fa-check" id="accept" aria-hidden="true"></i> تایید
                                                                        </a>
                                                                        <a href="javascript:void(0)" class="btn btn-danger"  onclick="rejectResume()">
                                                                            <i class="fa fa-close" id="reject" aria-hidden="true"></i> رد
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <!-- END : Options buttons -->
                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @else
                                        <div class="alert alert-warning text-center">
                                            {{__('public.no_info')}}
                                        </div>
                                    @endif
                                    <div class="mb-5">
                                        {{$requests->links('pagination.panel')}}
                                     </div>
                                </div>
                                <div class="tab-pane" id="tab2">
                                    @if (count($acceptedRequests) > 0)
                                        @foreach ($acceptedRequests as $request)
                                            <div class="col-12 col-md-4  mb-3">
                                                <div class="custom-card radius-10 border-0 border-3 border-info shadow-lg">
                                                    <div class="card-body">
                                    
                                                        <!-- Profile picture and short information -->
                                                        <div class="d-flex align-items-center position-relative pb-3">
                                                            <div class="flex-shrink-0">
                                                                <img class="img-md rounded-circle" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="Profile Picture" loading="lazy">
                                                            </div>
                                                            <div class="flex-grow-1 ms-3">
                                                                <a href="#" class="h5 stretched-link btn-link">{{$request->sender->name}} {{$request->sender->family}}</a>
                                                                <p class="text-muted m-0">{{$request->sender->groupable->grade}} {{$request->sender->groupable->major}}</p>
                                                            </div>
                                                        </div>
                                                        @if ($request->sender->groupable)
                                                            <p class="mb-0 font-13 text-gray">
                                                                از 
                                                                @if ($request->sender->groupable->province)
                                                                    استان
                                                                    {{$request->sender->groupable->province->name}}
                                                                @endif
                                                                @if ($request->sender->groupable->city)
                                                                    شهر
                                                                    {{$request->sender->groupable->city->name}}
                                                                @endif
                                                            </p>
                                                        @endif
                                                        <!-- END : Profile picture and short information -->
                                    
                                                        <!-- Options buttons -->
                                                        <div class="mt-3 pt-2 text-center border-top">
                                                            <div class="d-flex justify-content-center gap-3">
                                                                <a href="{{route('candidate.single',$request->sender_id)}}" target="_blank" class="btn btn-info">
                                                                    <i class="fa fa-eye" aria-hidden="true"></i> مشاهده
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- END : Options buttons -->
                                    
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="alert alert-warning text-center">
                                            {{__('public.no_info')}}
                                        </div>
                                    @endif
                                    <div class="mb-5">
                                        {{$acceptedRequests->links('pagination.panel')}}
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        const csrf = "{{csrf_token()}}"
        const acceptUrl = "{{route('front.ajax.accept.resume')}}"
        const rejectUrl = "{{route('front.ajax.reject.resume')}}"
    </script>
    <script src="{{asset('assets/panel/js/company-sent-resume.js')}}"></script>
@endsection