@extends('panel.company.master')
@section('title','محصولات / خدمات')
@section('main')
    <div class="page-header">
        <h1 class="page-title"> محصولات / خدمات</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">محصولات / خدمات</a></li>
            </ol>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
        <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
        <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه لیست محصولات و خدمات شرکت شما نمایش داده میشود.</strong>
            </p>
        </div>
        </div>
    </div>

    <div class="row ">
        @if(Session::has('error'))
            <div class="alert alert-danger  text-center">
                {{Session::pull('error')}}
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success  text-center">
                {{Session::pull('success')}}
            </div>
        @endif
        <div class="card-header border-bottom-0">
            <div class=" ">
                <a class="btn btn-success" href="{{route('company.products.create')}}">افزودن محصول/خدمات</a>
            </div>
        </div>
    </div>
    <div class="row">
        @if(count($products)>0)
            @foreach($products as $product)
        <div class="col-md-6 col-xl-4 col-sm-6">
            <div class="card">
                <div class="product-grid6">
                    <div class="product-image6 p-5">
                        <ul class="icons">
                            <li>
                                <a href="#" class="btn btn-primary"> <i class="fe fe-eye"> </i> </a>
                            </li>
                            <li title="ویرایش"><a title="ویرایش" href="{{route('company.products.edit',$product)}}" class="btn btn-success"><i class="fe fe-edit"></i></a></li>
                            <li><a href="javascript:void(0)" class="btn btn-danger"><i class="fe fe-x"></i></a></li>
                        </ul>
                        <a href="#">
                            @if($product->photo)
                                <img class="img-fluid br-7 w-100" src="{{asset($product->photo->path)}}" alt="img">
                            @else
                            <img class="img-fluid br-7 w-100" src="{{asset('assets/images/default-image.svg')}}" alt="img">
                            @endif
                        </a>
                    </div>
                    <div class="card-body pt-0">

                        <div class="product-content text-center">
                            <h1 class="title fw-bold fs-20"><a href="#">{{$product->name}}</a></h1>
                            @if($product->type =='product')
                                <span class="badge bg-success">محصول</span>
                            @else
                                <span class="badge bg-info">خدمات</span>
                            @endif
                            <p class="mt-2">{{Str::limit($product->description,'30','...')}}</p>
                            {{--<div class="mb-2 text-warning">
                                <i class="fa fa-star text-warning"></i>
                                <i class="fa fa-star text-warning"></i>
                                <i class="fa fa-star text-warning"></i>
                                <i class="fa fa-star-half-o text-warning"></i>
                                <i class="fa fa-star-o text-warning"></i>
                            </div>--}}
                        </div>
                    </div>
                    <div class="card-footer text-center">
{{--                        <a href="cart.html" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart mx-2"></i>مشاهده</a>--}}
{{--                        <a href="wishlist.html" class="btn btn-outline-primary mb-1"><i class="fe fe-heart mx-2 wishlist-icon"></i>افزودن به علاقه مندی</a>--}}
                    </div>
                </div>
            </div>
        </div>
            @endforeach
        @else
            <div class="alert alert-warning text-center w-100">
                {{__('public.no_info')}}
            </div>
        @endif


        <div class="mb-5">
            <div class="float-end">
                <ul class="pagination ">
                    <li class="page-item page-prev disabled">
                        <a class="page-link" href="javascript:void(0)" tabindex="-1">قبلی</a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">4</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">5</a></li>
                    <li class="page-item page-next">
                        <a class="page-link" href="javascript:void(0)">بعدی</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


@endsection
