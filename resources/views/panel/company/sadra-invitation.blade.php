@extends('panel.company.master')
@section('title','رویدادها')
@section('main')
<style>
    .item-span{
        font-size: 1rem;
    }

    @media screen and (max-width: 500px) {
        .c-h4{
            font-size: 0.8rem !important;
        }
        .c-m-400{
            padding-right: 0.1rem !important;
            padding-left: 0.1rem !important;
        }
        .c-m-400-l2{
            padding-right: 0.1rem !important;
            padding-left: 0.4rem !important;
        }
        .c-p-400{
            font-size: 0.8rem !important;
            line-height: 1rem !important;
        }
        .c-row-1{
            justify-content: center !important;
        }
        .bg-400{
            background-image: url("{{asset('assets/panel/images/invitation/bg-400.svg')}}") !important;
            background-repeat: repeat !important;
        }
        .c-m-400-c{
            padding: 0.5rem !important;
        }
    }
</style>

    <div class="row row-cards justify-content-center mt-1">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="row">
                <div class="col-12 c-m-400">
                    <div class="card shadow-ld bg-400" style="background-repeat: repeat; background-image: url({{asset('assets/panel/images/invitation/bg.svg')}})">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-10 p-5 c-m-400-c">
                                <div class="card mb-2 mb-md-3" style="background-color: rgba(255, 255, 255, 0.95);">
                                    <div class="row justify-content-between align-items-center m-3 mt-5 c-row-1">
                                        <div class="col-6 col-md-2">
                                            <img src="{{asset('assets/panel/images/invitation/uni-logo-2.svg')}}" alt="jobist-logo">
                                        </div>
                                        <div class="col-6 col-md-3">
                                            <img src="{{asset('assets/panel/images/brand/jobist/text-logo.svg')}}" alt="jobist-logo">
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <div class="col-md-10">
                                                <img src="{{asset('assets/panel/images/invitation/sadra-logo.png')}}" alt="sadra-logo">
                                            </div>
                                            <span style="font-size: 0.9rem">تاریخ :</span>
                                            <span style="font-size: 0.9rem">{{verta()->formatJalaliDate($payment->updated_at)}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2 mb-md-3" style="background-color: rgba(255, 255, 255, 0.95);">
                                    <div class="row m-3 mt-5 justify-content-center">
                                        <div class="col-12 col-md-11">
                                            <div class="row align-items-center">
                                                <div class="col-3 col-md-1 c-m-400-l2">
                                                    <img src="{{asset($company->companyInfo->logo->path)}}" alt="company-logo" style="border: 1px solid darkcyan; border-radius: 15px">
                                                </div>
                                                <div class="col-9 col-md-10 c-m-400">
                                                    {{-- <h4 class="c-h4" style="font-size: 1.4rem;">
                                                        <strong>
                                                            <span>جناب</span>
                                                            <span>{{$company->companyInfo->ceo}}</span>
                                                        </strong>  
                                                    </h4> --}}
                                                    <h4 class="c-h4" style="font-size: 1.4rem;">
                                                        <strong>
                                                            <span>مدیر عامل محترم شرکت</span>
                                                            <span>{{$company->companyInfo->name}}</span>
                                                        </strong>
                                                    </h4>
                                                </div>
                                            </div>
                                            <p class="mt-4 mb-1">با سلام و احترام</p>
                                            <p class="c-p-400 mb-1" align="justify" style="font-size: 1.1rem; line-height: 2rem">
                                                پس از حمد خدا و درود و صلوات بر محمد و آل محمد (ص)، به استحضار می‌رساند دانشگاه آزاد اسلامی واحد جامع نجف آباد در نظر دارد با توسعه ارتباط میان دانشگاه و صنعت، ضمن فراهم سازی بستر مناسب برای اشتغال دانشجویان و فارغ التحصیلان دانشگاه، جهت تامین نیروی انسانی متخصص و مورد نیاز صنعت اقدام نماید. بدین منظور {{ $payment->order->sadra->title }} با حضور بیش از 100 صنعت معتبر و نیازسنجی بیش از 1000 فرصت شغلی و کارآموزی طرح پویش، در تاریخ 
                                                    
                                                    <span>{{ verta($payment->order->sadra->start_at)->formatJalaliDate() }} لغایت {{ verta($payment->order->sadra->end_at)->formatJalaliDate() }}</span>
                                                 سال جاری از ساعت 8 لغایت 15 در سالن شهید حججی این دانشگاه برگزار می‌گردد. لذا بدینوسیله از حضورتعالی یا نماینده آن شرکت محترم دعوت می‌گردد در این رویداد شرکت فرمایید. 
                                            </p>
                                            @if ($company->inviter && ($company->inviter != "سامانه جابیست" || $company->inviter != "سایر موارد"))
                                                <p class="c-p-400 mt-1" align="justify" style="font-size: 1.1rem; line-height: 2rem">
                                                    همچنین
                                                    <strong>{{$company->inviter}}</strong>
                                                    جهت هماهنگی های لازم حضورتان معرفی می‌گردند.
                                                </p>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2 mb-md-3" style="background-color: rgba(255, 255, 255, 0.95);">
                                    <div class="row m-3 justify-content-center align-items-center">
                                        <div class="col-12 col-md-11">
                                            <div class="row align-items-center">
                                                <div class="col-4 col-md-2">
                                                    <div class="row align-items-center">
                                                        <div class="col-12 col-md-8">
                                                            <img src="{{asset('assets/panel/images/invitation/qr-code.svg')}}" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-8 col-md-10">
                                                    <div class="row align-items-center mt-1">
                                                        <div class="col-3 col-md-1 px-0">
                                                            <div class="row justify-content-center">
                                                                <div class="col-8">
                                                                    <img src="{{asset('assets/panel/images/invitation/web-icon.svg')}}" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-md-10 ps-0">
                                                            <h4 class="m-0 c-h4">www.jobist.ir</h4>
                                                        </div>
                                                    </div>
                                                    <div class="row align-items-center mt-1">
                                                        <div class="col-3 col-md-1 px-0">
                                                            <div class="row justify-content-center">
                                                                <div class="col-8">
                                                                    <img src="{{asset('assets/panel/images/invitation/location-icon.svg')}}" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-md-10 ps-0">
                                                            <h5 class="m-0 c-h4">نجف آباد - بلوار دانشگاه - دانشگاه آزاد اسلامی واحد نجف آباد</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-2">
                            <a id="print" class="btn btn-primary d-print-none" href="javascript:void(0)" style="font-size: 1.1rem; opacity: 100%;">چاپ دعوتنامه</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{-- {{$events->links('pagination.panel')}} --}}
            </div>
        </div>
    </div>
    
@endsection
@section('script')
    <script src="{{asset('assets/panel/js/company.js')}}"></script>
    
    <script>
        document.getElementById("print").onclick = function jsFunc() {
            window.print();
        }
    </script>
@endsection
