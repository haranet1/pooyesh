@extends('panel.company.master')
@section('title','لیست کلیه متقاضیان')
@section('main')
    {{-- header --}}
    <div class="page-header">
        <h1 class="page-title">جستجوی کارجویان</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست   متقاضیان</a></li>
            </ol>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
        <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
        <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه می‌توانید کارآموزان را جستجو و مشاهده نمایید</strong>
            </p>
        </div>
        </div>
    </div>

    <div class="row">
        {{-- province --}}
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="input-group mb-5">
                <select class="form-control form-select" onchange="UpdateVars()" name="province" id="province">
                    <option value="0">انتخاب استان</option>
                    @foreach ($provinces as $province)
                        <option value="{{$province->id}}">{{$province->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        {{-- city --}}
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="input-group mb-5">
                <select class="form-control form-select" onchange="UpdateVars()" name="city" id="city">
                    <option value="0">انتخاب شهر</option>
                    @foreach ($citys as $city)
                        <option value="{{$city->id}}">{{$city->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        {{-- grade --}}
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="input-group mb-5">
                <select class="form-control form-select" onchange="UpdateVars()" name="grade" id="grade">
                    <option value="0">مقطع تحصیلی</option>
                    <option value="دکترا">دکترا</option>
                    <option value="کارشناسی ارشد">کارشناسی ارشد</option>
                    <option value="کارشناسی">کارشناسی</option>
                    <option value="کاردانی">کاردانی</option>
                    <option value="دیپلم">دیپلم</option>
                </select>
            </div>
        </div>
        {{-- experince --}}
        {{-- <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="input-group mb-5">
                <select class="form-control form-select" onchange="UpdateVars()" name="experince" id="experince">
                    <option value="0">سابقه کاری</option>
                    <option value="2">تا 2 سال</option>
                    <option value="5">3 تا 5 سال</option>
                    <option value="10">6 تا 10 سال</option>
                </select>
            </div>
        </div> --}}
        {{-- sex --}}
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="input-group mb-5">
                <select class="form-control form-select" onchange="UpdateVars()" name="sex" id="sex">
                    <option value="0">جنسیت</option>
                    <option value="male">مرد</option>
                    <option value="female">زن</option>
                </select>
            </div>
        </div>
        {{-- marige --}}
        {{-- <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="input-group mb-5">
                <select class="form-control form-select" onchange="UpdateVars()" name="marige" id="marige">
                    <option value="0">وضعیت تاهل</option>
                    <option value="مجرد">مجرد</option>
                    <option value="متاهل">متاهل</option>
                </select>
            </div>
        </div> --}}
        {{-- age --}}
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="input-group mb-5">
                <select class="form-control form-select" onchange="UpdateVars()" name="age" id="age">
                    <option value="0">رنج سنی</option>
                    <option value="20">تا 20 سال</option>
                    <option value="30">21 تا 30 سال</option>
                    <option value="40">31 تا 40 سال</option>
                </select>
            </div>
        </div>
    </div>
    {{-- cards part --}}
    
    <div class="row" id="rowId">
        @foreach ($students as $std)
            <div class="col col-md-9 col-lg-7 col-xl-5">
                <div class="card" style="border-radius: 15px;">
                <div class="card-body p-4">
                    <div class="d-flex text-black">
                        <div class="flex-shrink-0">
                            <img src="{{asset('assets/panel/images/icon/SVG/u1.svg')}}"
                            alt="Generic placeholder image" class="img-fluid"
                            style="width: 180px; border-radius: 10px;">
                        </div>
                        <div class="flex-grow-1 ms-3">
                            <h4 class="mb-1">{{$std->name}} {{$std->family}}</h4>
                            <p class="mb-2 pb-1"  style="color: #2b2a2a; font-size: 0.8rem;">
                                {{$std->lastGrade}} {{$std->lastMajor}} {{$std->lastUniversity}}</p>
                            <div class="row d-flex justify-content-start rounded-3 p-2 mb-2 ms-1"
                            style="background-color: #efefef;">
                                <div class="col-3 pe-0">
                                    <p class="small text-muted mb-1">استان</p>
                                    <p class="mb-0">{{$std->groupable->province->name}}</p>
                                </div>
                                <div class="col-4 pe-0">
                                    <p class="small text-muted mb-1">شهر</p>
                                    <p class="mb-0">@if($std->groupable->city){{$std->groupable->city->name}}@endif</p>
                                </div>
                                <div class="col-2 pe-0">
                                    <p class="small text-muted mb-1">سن</p>
                                    <p class="mb-0">{{$std->newAge}}</p>
                                </div>
                                <div class="col-3 pe-0">
                                    <p class="small text-muted mb-1">جنسیت</p>
                                    <p class="mb-0">{{change_sex_to_fa($std->sex)}}</p>
                                </div>
                            </div>
                            <div class="d-flex pt-1">
                            <a  class="btn btn-primary flex-grow-1">مشاهده پروفایل</a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        @endforeach
        
    </div>
@endsection


@section('script')
    <script>
        const imageUrl = "{{asset('assets/panel/images/icon/SVG/u1.svg')}}";
        const csrf = "{{csrf_token()}}";
        const url = "{{route('company.ajax.search')}}";
        var singlePage = "{{route('candidate.single',['id' => ':id'])}}";
        var urlR = "{{route('std_search')}}";
        var stdUrl = "{{route('std_search')}}";
        var aplUrl = "{{route('apl_search')}}";
        
        
    </script>

    <script src="{{asset('assets/panel/js/std-list.js')}}"></script>

    {{-- <script>
        function search() {
            var table = document.getElementById('table')
            var key = $('#search').val()

            $('#table tr').remove()
            table.innerHTML = '<tr>\n' +
                '                            <th>نام و نام خانوادگی</th>\n' +
                '                            <th>سن </th>\n' +
                '                            <th>رشته </th>\n' +
                '                            <th>عملکردها</th>\n' +
                '                        </tr>'


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: 'POST',
                url: '{{route('company.ajax.search')}}',
                data: {
                    do: 'search-students',
                    key: key
                },
                success: function (response) {
                    console.log(response)
                    response['data'].forEach(function (re) {
                        var route = "{{route('candidate.single',['id' => ':id'])}}"
                        route = route.replace(':id', re['id'])
                        var row = table.insertRow(-1);
                        var td0 = row.insertCell(0)
                        var td1 = row.insertCell(1)
                        var td2 = row.insertCell(2)
                        var td3 = row.insertCell(3)
                        td0.innerHTML = re['name']+" "+re['family']
                        td1.innerHTML = re['groupable']['age']
                        td2.innerHTML = re['groupable']['major']
                        td3.innerHTML = '<div class="btn-group align-top">' +
                            ' <a href="' + route + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل  </a> </div>'
                    })
                },
                error: function (data) {
                    console.log(data)
                }
            })
        }
    </script> --}}
@endsection
