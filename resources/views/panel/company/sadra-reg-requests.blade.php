@extends('panel.company.master')
@section('title','درخواست های صدرا')
@section('main')
    <style>
        .card-number {
            display: inline-block;
            font-size: 18px;
            padding: 10px;
            background-color: #f1f1f1;
            border-radius: 4px;
            color: #333;
            letter-spacing: 1px;
            font-family: Arial, sans-serif;
        }
    </style>
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست رویدادهای ثبت نام شده</a></li>
            </ol>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
        <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
        <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه لیست درخواست های شما که برای شرکت در نمایشگاه های کار ثبت کرده اید نمایش داده میشود.</strong>
            </p>
        </div>
        </div>
    </div>

<div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger  text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success  text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="e-table px-5 pb-5 pt-5">
                    <div class="table-responsive table-lg d-none d-md-block">
                        @if(count($requests) > 0)
                            <table class="table border-top table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>رویداد</th>
                                    <th>غرفه انتخابی</th>
                                    <th>تاریخ ثبت نام</th>
                                    <th>قیمت</th>
                                    <th>وضعیت</th>
                                    <th class="text-center">امکانات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                        <td class="text-nowrap align-middle">{{$request->sadra->title}}</td>
                                        <td class="text-nowrap align-middle">{{$request->booth->name}}</td>
                                        <td class="text-nowrap align-middle">{{verta($request->created_at)->formatDate()}}</td>
                                        <td class="text-nowrap align-middle">{{$request->booth->plan->price}}</td>
                                        <td class=" text-center text-nowrap align-middle">
                                            @if(verta(now())->lessThan(verta($request->sadra->end_register_at)))
                                                @if($request->status === 1)
                                                    <span class="badge bg-success">پرداخت شده</span>
                                                @elseif($request->status === 0)
                                                    <span class="badge bg-danger"> رد شده</span>
                                                @else
                                                    <span class="badge bg-primary">در انتظار پرداخت</span>
                                                @endif
                                            @else
                                                <span class="badge bg-dark">رویداد پایان یافت</span>
                                            @endif
                                        </td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                @if(verta(now())->lessThan(verta($request->sadra->end_register_at)))
                                                    @if (is_null($request->status) && is_null($request->receipt_id))
                                                        <input type="hidden" id="company_sadra_id" value="{{ $request->id }}">
                                                        <a href="javascript:void(0)" class="btn btn-danger reject-btn">
                                                            انصراف
                                                        </a>
                                                        <input type="hidden" id="payUrl-{{$request->id}}" value="{{route('company.sadra.prepay',$request->id)}}">
                                                        <a onclick="PayBtn({{$request->id}})"  class="btn register btn-success pay-btn" 
                                                        {{-- href="{{route('company.sadra.prepay',$request->id)}}" --}}
                                                        href="javascript:void(0)">
                                                        پرداخت</a>
                                                    @endif
                                                    @if ($request->status === 1 && $request->payedPayment)
                                                        <a  class="btn register btn-info"
                                                        href="{{route('company.sadra.payment.result-show',$request->payedPayment->id)}}"
                                                        href="javascript:void(0)">
                                                        مشاهده رسید</a>
                                                        <a  class="btn register btn-success"
                                                        href="{{route('company.sadra.invitation',$request->payedPayment->id)}}"
                                                        href="javascript:void(0)">
                                                        مشاهده دعوتنامه</a>
                                                    @endif
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="modal-sadra-{{$request->id}}">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content modal-content-demo">
                                                <div class="modal-header justify-content-center">
                                                    <h4 class="modal-title text-center">
                                                        پس از پرداخت هزینه لطفا از رسید پرداخت عکس گرفته و اینجا بارگذاری کنید.
                                                    </h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="card-body">
                                                        <h3>مبلغ قابل پرداخت:
                                                            <span>{{number_format($request->booth->plan->price)}}</span>
                                                            تومان
                                                        </h3>
                                                        <h5>شماره حساب:
                                                            <span class="card-number">{{config('setting.payment.account-number')}}</span>
                                                        </h5>
                                                        <h5>نام صاحب حساب:
                                                            <span>{{config('setting.payment.card-owner')}}</span>
                                                        </h5>

                                                        <form class="form-horizontal mt-4" action="{{route('company.sadra.register.final')}}" enctype="multipart/form-data" method="post">
                                                            @csrf
                                                            <input type="hidden" name="sadra_req" value="{{$request->id}}">
                                                            <div class="row mb-4 justify-content-center">
                                                                <div class="col-md-6">
                                                                    <input required type="file" name="receipt" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class=" row mt-3 justify-content-center ">
                                                                <div class="col justify-content-center text-center ">
                                                                    <button type="submit"
                                                                            class="btn btn-success btn-lg px-5"> ثبت نام
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                                <div
                                                    class="modal-footer">
                                                    <button
                                                        class="btn btn-light"
                                                        data-bs-dismiss="modal">
                                                        انصراف
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif

                    </div>
                    @if(count($requests) > 0)
                    @foreach ($requests as $request)
                        <div class="table-responsive table-lg d-flex d-md-none mb-5">
                            <table class="table border-top table-bordered mb-0">
                                <tr>
                                    <th>
                                        رویداد
                                    </th>
                                    <td>
                                        {{$request->sadra->title}}
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        غرفه انتخابی
                                    </th>
                                    <td>
                                        {{$request->booth->name}}
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        تاریخ ثبت نام
                                    </th>
                                    <td>
                                        {{verta($request->created_at)->formatDate()}}
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        قیمت
                                    </th>
                                    <td>
                                        {{$request->booth->plan->price}}
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        وضعیت
                                    </th>
                                    <td>
                                        @if(verta(now())->lessThan(verta($request->sadra->end_register_at)))
                                            @if($request->status === 1)
                                                <span class="badge bg-success">پرداخت شده</span>
                                            @elseif($request->status === 0)
                                                <span class="badge bg-danger"> رد شده</span>
                                            @else
                                                <span class="badge bg-primary">در انتظار پرداخت</span>
                                            @endif
                                        @else
                                            <span class="badge bg-dark">رویداد پایان یافت</span>
                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        امکانات
                                    </th>
                                    <td>
                                        <div class="btn-group align-top">
                                            @if(verta(now())->lessThan(verta($request->sadra->end_register_at)))
                                                @if (is_null($request->status) && is_null($request->receipt_id))
                                                    <a href="javascript:void(0)" class="btn btn-danger reject-btn">
                                                        انصراف
                                                    </a>
                                                    <a  class="btn register btn-success pay-btn"
                                                    {{-- href="{{route('company.sadra.prepay',$request->id)}}" --}}
                                                    href="javascript:void(0)">
                                                    پرداخت</a>
                                                @endif
                                                @if ($request->status === 1 && $request->payedPayment)
                                                    <a  class="btn register btn-info"
                                                    href="{{route('company.sadra.payment.result-show',$request->payedPayment->id)}}"
                                                    href="javascript:void(0)">
                                                    مشاهده رسید</a>
                                                    <a  class="btn register btn-success"
                                                    href="{{route('company.sadra.invitation',$request->payedPayment->id)}}"
                                                    href="javascript:void(0)">
                                                    مشاهده دعوتنامه</a>
                                                @endif
                                            @endif
                                        </div>
                                    </td>
                                </tr>


                            </table>
                        </div>
                        @endforeach
                            
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                </div>
            </div>
            <div class="mb-5">
               {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $('#plan').on('change',function (){
            $('#booth').html("")
            const plan_id =this.value
            if (plan_id){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN':'{{csrf_token()}}'
                    },
                    url: '{{route('company.ajax')}}',
                    data: {
                        do: 'get-boot-by-plan-id',
                        plan_id:plan_id
                    },
                    dataType: 'json',
                    success: function (response) {
                        if (response){
                            let data =""
                            response.forEach(function (resp){
                                data += '<option class="form-control" value="'+resp['id']+'">'+resp['name']+'</option>'
                            })
                            $('#booth').html(data)
                        }
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            }
        })

        $('.reject-btn').on('click' , function () {

            let company_sadra_id = document.getElementById('company_sadra_id').value;

            Swal.fire({
                icon: 'warning',
                title: 'آیا مطمئن هستید؟',
                text: "ثبت نام شما در این رویداد لغو خواهد شد.",
                showCancelButton: true,
                confirmButtonColor: '#198f20',
                cancelButtonColor: '#d33',
                cancelButtonText: 'خیر',
                confirmButtonText: 'بله'
            }).then((result) => {
                if(result.isConfirmed){
                    $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN':'{{csrf_token()}}'
                    },
                    url: "{{route('co.reject.company.sadra')}}",
                    data: {
                        company_sadra_id : company_sadra_id,
                    },
                    dataType: 'json',
                    beforeSend: function(){
                        $('#global-loader').removeClass('d-none');
                        $('#global-loader').addClass('d-block');
                        $('#global-loader').css('opacity', 0.6);
                    },
                    success: function (response) {
                        $('#global-loader').removeClass('d-block');
                        $('#global-loader').addClass('d-none');
                        $('#global-loader').css('opacity', 1);
                        
                        if(response == true) {
                            location.reload()
                        }else{
                            Swal.fire({
                                icon : 'warning',
                                title : 'خطا !',
                                text : 'امکان انصراف بعد از پرداخت وجود ندارد.',
                            })
                        }

                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
                }
            })

        })


        function PayBtn(request_id){
            
            let tagId = "payUrl-"+ request_id;

            let payUrl = document.getElementById(tagId).value

            Swal.fire({
                icon : 'warning',
                title : 'توجه !',
                text : 'پس از انجام عملیات پرداخت حتما منتظر بمانید تا به صفحه دعوتنامه هدایت شوید و به هیچ عنوان صفحات را refresh نکنید یا از صفحات پیشِ رو خارج نشوید.' ,
                confirmButtonText : 'پرداخت',
            }).then((result) => {
                if(result.isConfirmed){
                    window.location.href = payUrl;
                }
            });

        }

    </script>
@endsection
