@extends('panel.company.master')
@section('title','پنل شرکت')

@section('main')
    <style>
        .shadow-limit{
            box-shadow: rgba(0, 0, 0, 0.25) 0px 0.0625em 0.0625em, rgba(0, 0, 0, 0.25) 0px 0.125em 0.5em, rgba(255, 255, 255, 0.1) 0px 0px 0px 1px inset;
        }

    </style>
    <div class="page-header">
        <h1 class="page-title">داشبورد شرکت</h1>
        
        
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد اصلی</a></li>
            </ol>
        </div>
        
    </div>
    @include('alert.email-alert')

    <div class="row mb-4">
        <div class="col-12">
            @if(Session::has('success'))
                
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
                
            @endif
            @if(Session::has('error'))
                
                <div class="alert alert-danger">
                    {{Session::pull('error')}}
                </div>
                
            @endif
        </div>
    </div>


    <div class="card overflow-hidden">
        <div class="card-header justify-content-between">
            <div class="card-title">
            امکانات و خدمات جابیست برای شما
            </div>
            <div class="card-toolbar">
                <a  class="btn btn-primary " href="{{route('index.package.company')}}">
                <i class="fa fa-shopping-cart"></i> بسته خدماتی</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row justify-content-center">
                @foreach($chartsData as $index => $chart)
                    <div class="col-xl-2 col-sm-4 col-12 px-1">
                        <div class="br-7" id="chart{{ $index }}"></div>
                    </div>
                @endforeach


            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">تعداد شرکت ها</h6>
                                    <h2 class="mb-0 number-font">{{$companies}} </h2>
                                    <h6>شرکت</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img style="width: 70px" class="img-fluid" src="{{asset('assets/images/icons8-company-100.png')}}" alt="company">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 style="white-space: nowrap" class="">تعداد موقعیت های شغلی</h6>
                                    <h2 class="mb-0 number-font">{{$job_position}} </h2>
                                    <h6>موقعیت شغلی</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img style="width: 70px" class="img-fluid" src="{{asset('assets/images/icons8-idea.svg')}}" alt="idea">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 style="white-space: nowrap" class="">تعداد موقعیت های کارآموزی</h6>
                                    <h2 class="mb-0 number-font">{{$intern}}</h2>
                                    <h6>موقعیت کارآموزی</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img class="img-fluid" style="width: 70px" src="{{asset('assets/images/icons8-project-100.png')}}" alt="project">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">تعداد کارجویان</h6>
                                    <h2 class="mb-0 number-font">{{$students}} </h2>
                                    <h6>کارجو</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img style="width: 70px" class="img-fluid" src="{{asset('assets/images/icons8-std-96.png')}}" alt="student">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($events->count() > 0)
        <div class="row justify-content-start">
            <div class="col-lg-12 col-md-12 my-2  col-sm-12 col-xl-12" >
                {{-- <div id="pxp-blog-featured-posts-carousel"
                    class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-inner justify-content-center py-2" style="border:2px solid #52c679;border-radius: 10px">
                        @foreach($events as $event)
                            <div class="carousel-item justify-content-center " data-bs-interval="10000">
                                <a class="d-flex justify-content-center text-center" href="{{route('company.sadra')}}">
                                    <img style="max-height:220px;max-width: 727px;"
                                        src="{{ asset($event->banner->path) }}"
                                        class="d-block w-100 img-fluid " alt="banner">
                                </a>
                            </div>
                            @php($flag = false)
                        @endforeach
                    </div>
                </div> --}}
                @foreach ($events as $event)
                    @if ($event->infos->count() > 0)
                        <div class="card">
                            <div class="card-body">
                                <div class=""  style="overflow: hidden; max-height: 400px" >
                                    <a href="{{ route('lets.register.event' , $event->id) }}">
                                        <img src="{{ asset($event->banner->path) }}" class="w-100" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    @endif
    @if(count($sadras) > 0)
        @php($flag = true)
        <div class="row justify-content-start">
            <div class="col-lg-12 col-md-12 my-2  col-sm-12 col-xl-12" >
                <div id="pxp-blog-featured-posts-carousel"
                     class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-inner justify-content-center py-2" style="border:2px solid #52c679;border-radius: 10px">
                        @foreach($sadras as $sadra)
                            <div class="carousel-item justify-content-center {{$flag?'active':''}}" data-bs-interval="10000">
                                <a class="d-flex justify-content-center text-center" href="{{route('company.sadra')}}">
                                    <img style="max-height:220px;max-width: 727px;"
                                         src="{{asset($sadra->banner->path)}}"
                                         class="d-block w-100 img-fluid " alt="banner">
                                </a>
                            </div>
                            @php($flag = false)
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    {{--<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
            <div class="card text-white bg-success text-center">
                <a class="text-white" href="{{route('company.sadra')}}">
                <div class="card-body">
                    <h4 class="card-title">شرکت در نمایشگاه</h4>
                    <h3 class="card-text">اولین نمایشگاه فرانچایز و توسعه کسب و کار</h3>
                    <p class="table-warning py-2"><i class="fa fa-certificate"></i> برای ثبت نام با 30 درصد تخفیف کلیک کنید <i class="fa fa-certificate"></i> </p>
                </div>
                </a>
            </div>
        </div>
    </div>--}}

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card text-white bg-info overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        <i class="fa fa-comment"></i>
                        فرم ها - آیین نامه ها
                    </div>
                    <div class="card-toolbar">
                        {{--                        <a class="btn btn-light" href="#"><i class="fa fa-eye"></i> مشاهده</a>--}}
                        <a  class="btn btn-light "
                            data-bs-target="#modal-files"
                            data-bs-toggle="modal"
                            href="javascript:void(0)">
                            <i class="fa fa-eye"></i> مشاهده</a>
                    </div>
                </div>
                <div class="card-body" style="overflow-y: scroll; max-height: 300px;">
                    <p class="mb-5"> فرم ها و آیین نامه های مورد نیاز خود را از این قسمت مشاهده و دریافت نمایید.</p>

                    @if(count($files)> 0)
                        @foreach($files as $file)
                            <a href="{{asset($file->path)}}">
                                <h4 class="text-light border">
                                    <i class="fa fa-caret-left"></i>
                                    {{$file->name}}
                                </h4>
                            </a>
                        @endforeach
                    @endif
                </div>
                <div class="modal fade " id="modal-files">
                    <div
                        class="modal-dialog" role="document">
                        <div class="modal-content modal-content-demo">
                            <div class="modal-header">
                                <h6 class="modal-title text-primary">لیست آیین نامه ها و فرم ها</h6>
                            </div>
                            <div class="modal-body">
                                @if(count($files)> 0)
                                    <ol class="list-group">
                                        @foreach($files as $file)
                                            <li class="list-group-item my1">
                                                <a href="{{asset($file->path)}}"><h6 class="m-0">{{$file->name}}</h6></a>
                                            </li>
                                        @endforeach
                                    </ol>
                                @endif
                            </div>
                            <div
                                class="modal-footer">
                                <button
                                    class="btn btn-light"
                                    data-bs-dismiss="modal">
                                    بستن
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card text-white bg-danger overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        <i class="fe fe-headphones"></i>
                        پیام های دریافتی
                    </div>
                    <div class="card-toolbar">
                        <a class="btn btn-light" href="{{route('company.tickets')}}"><i class="fe fe-headphones"></i> پیام های دریافتی</a>
                    </div>
                </div>
                <div class="card-body" style="overflow-y: scroll; max-height: 300px;">
                    <p class="mb-5">آخرین  پیام های دریافتی را از این قسمت مشاهده نمایید.</p>
                    
                    @if(count($tickets)> 0)
                        @foreach($tickets as $ticket)
                            <div class="d-flex border-bottom pb-2 mt-2" >
                                <div class="me-3 notifyimg   ">
                                    <i class="fe fe-mail"></i>
                                </div>
                                <div class="mt-1 wd-80p">
                                    <h5 class=" text-light mb-1">
                                        موضوع:
                                        {{$ticket->subject}}
                                    </h5>
                                    <span class="notification-subtext text-light">{{verta($ticket->created_at)->formatDifference()}}</span>
                                    <span>فرستنده:</span>
                                    <span class="notification-subtext text-light"> {{$ticket->sender->name." ".$ticket->sender->family}}  </span>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card   overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        اطلاعیه ها
                    </div>
                    <div class="card-toolbar">

                    </div>
                </div>
                <div class="card-body d-flex flex-column p-2" style="height: 400px">
                    <div class="card-scroll scroll-auto p-5 pr-6 overflow-auto">
                        @if(count($announcements) > 0)
                            <ol class="list-group">
                                @foreach($announcements as $announce)

                                    <li class="list-group-item my-1">
                                        <i class="fa fa-caret-left"></i>
                                        <a data-bs-target="#modal-announce-{{$announce->id}}"
                                           data-bs-toggle="modal"
                                           href="javascript:void(0)">
                                            {{$announce->title}}
                                        </a>

                                    </li>
                                    <div class="modal fade" id="modal-announce-{{$announce->id}}">
                                        <div
                                            class="modal-dialog" role="document">
                                            <div class="modal-content modal-content-demo">
                                                <div class="modal-header">
                                                    <h6 class="modal-title text-primary">متن اطلاعیه: {{$announce->title}}</h6>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        {{$announce->content}}
                                                    </p>
                                                    @if($announce->file)
                                                        <a download href="{{asset($announce->file->path)}}">دانلود فایل</a>
                                                    @endif
                                                </div>
                                                <div
                                                    class="modal-footer">
                                                    <button
                                                        class="btn btn-light"
                                                        data-bs-dismiss="modal">
                                                        بستن
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </ol>
                        @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card   overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        اخبار
                    </div>
                    <div class="card-toolbar">
                    </div>
                </div>
                <div class="card-body d-flex flex-column p-2" style="height: 400px">
                    <div class="card-scroll scroll-auto p-5 pr-6 overflow-auto">
                        @if(count($news) > 0)
                            <ol class="list-group">
                                @foreach($news as $news_item)
                                    <li class="list-group-item my-1">
                                        <i class="fa fa-caret-left"></i>
                                        {{$news_item->title}}
                                    </li>
                                @endforeach
                            </ol>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3>پروفایل شرکت</h3>
    <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.info')}}">
                <div class="card text-white bg-primary">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/co-info.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">اطلاعات شرکت</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.portfolio.index')}}">
                <div class="card text-white bg-primary">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/my-reqs.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">پروژه های نمونه</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.single',\Illuminate\Support\Facades\Auth::id())}}">
                <div class="card text-white bg-primary">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/co-profile.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">صفحه اختصاصی شرکت</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.tickets')}}">
                <div class="card text-white bg-primary">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/inbox.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">پیام های دریافتی</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <h3>استخدام</h3>
    <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">

        {{-- <div class="col-md-3 col-xl-3">
            <a href="{{route('company.projects.list')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">نمایش پروژه ها</h4>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.projects.create')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">تعریف پروژه</h4>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.idea.create')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">ارائه ایده و ایجاد استارتاپ</h4>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.idea.index')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">لیست ایده ها و استارتاپ ها</h4>
                    </div>
                </div>
            </a>
        </div> --}}

        <div class="col-md-3 col-xl-3">
            <a href="{{route('co.hire.pos.job')}}">
                <div class="card text-white bg-success">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/job-list.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle"> لیست موقعیت های شغلی</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('create.pos.job')}}">
                <div class="card text-white bg-success">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/job-req.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">ثبت موقعیت شغلی</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

       {{-- <div class="col-md-3 col-xl-3">
            <a href="{{route('company.job_category.create')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">ثبت گروه شغلی</h4>
                    </div>
                </div>
            </a>
        </div>--}}

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.cooperate-requests')}}">
                <div class="card text-white bg-success">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/my-reqs.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">درخواست های من</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.students.list')}}">
                <div class="card text-white bg-success">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/all-req.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">مشاهده متقاضیان</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

    </div>

    <h3>طرح پویش (اشتغال، همزمان با تحصیل)</h3>
    <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">

        <div class="col-md-3 col-xl-3">
            <a href="{{route('create.pos.job')}}">
                <div class="card text-white bg-secondary">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/intern-reg.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">ثبت نیاز کارآموزی</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.students.list')}}">
                <div class="card text-white bg-secondary">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/all-intern.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">کلیه متقاضیان</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('co.received.intern.req.job')}}">
                <div class="card text-white bg-secondary">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/intern-req.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">درخواست های کارجویان</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>


    </div>

    <h3>نمایشگاه کار</h3>
    <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.products.index')}}">
                <div class="card text-white bg-danger">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/product.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">محصولات و خدمات</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('company.sadra')}}">
                <div class="card text-white bg-danger">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/event.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">شرکت در نمایشگاه</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-xl-3">
            <a href="{{route('registred.company.sadra')}}">
                <div class="card text-white bg-danger">
                    <div class="row card-body text-center" style="padding:15px 0 15px 15px">
                        <div class="col-4">
                            <img src="{{asset('assets/front/images/SVG/register-req.svg')}}" alt="">
                        </div>
                        <div class="col-8 d-flex align-items-center px-0">
                            <h4  class="card-title border-right py-3 align-middle">درخواست های ثبت نام نمایشگاه ها</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    
@endsection
@section('script')
    <script src="{{ asset('assets/panel/js/verify-email.js') }}"></script>
    <script>
        function convertToPersianNumber(number) 
        {
            const persianDigits = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
            return number.toString().replace(/[0-9]/g, (digit) => persianDigits[digit]);
        }
        var chartsData = @json($chartsData);
      
        var baseOptions = {
            // series: [80],
            chart: {
            height: 120,
            type: 'radialBar',
            toolbar: {
                show: false
            }
            },
        
            plotOptions: {
                radialBar: {
                    inverseOrder: false,
                    startAngle: -135,
                    endAngle: 135,
                    offsetX: 0,
                    offsetY: 0,
                    hollow: {
                        margin: 0,
                        size: '55%',
                        background: 'transparent',
                        position: 'front',
                        dropShadow: {
                            enabled: true,
                            top: 3,
                            left: 0,
                            blur: 4,
                            opacity: 0.24
                        }
                    },
                    track: {
                        show: true,
                        background: '#9e2a2b',
                        strokeWidth: '90%',
                        opacity: 1,
                        margin: 0, 
                        dropShadow: {
                            enabled: true,
                            top: -3,
                            left: 0,
                            blur: 4,
                            opacity: 0.35
                        }
                    },
                
                    dataLabels: {
                        show: true,
                        name: {
                                show: true,
                                fontSize: '14px',
                                fontFamily: 'main-font, serif',
                                fontWeight: 600,
                                color: '#777' ,
                                offsetY: 60
                            },
                        value: {
                                show: true,
                                fontSize: '18px',
                                fontFamily: 'main-font, serif',
                                fontWeight: 600,
                                color: '#777',
                                offsetY: -10,
                        }

                    }
                }
            },
            fill: {
            type: 'gradient',
            gradient: {
                shade: 'light',
                type: 'vertical',
                shadeIntensity: 0.5,
                inverseColors: false,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100]
            }
            },
            stroke: {
            lineCap: 'round'
            },
        };

        chartsData.forEach((chartData, index) => {
            let options = JSON.parse(JSON.stringify(baseOptions));

            options.series = [chartData.series];
            options.labels = [chartData.label];
            if (chartData.series > 75) {
                options.fill.gradient.gradientToColors = ['#298102'];
                options.fill.colors = '#6ef035';
            } else if(chartData.series > 25) {
                options.fill.gradient.gradientToColors = ['#0a53ff'];
                options.fill.colors = '#90e0ef';
            } else {
                options.fill.gradient.gradientToColors = ['#ff0000'];
                options.fill.colors = '#f38375';
            }
            options.plotOptions.radialBar.dataLabels.value.formatter = function() {
                return convertToPersianNumber(chartData.value); 
            };

            var chart = new ApexCharts(document.querySelector(`#chart${index}`), options);
            chart.render();
        });
      
    
    </script>
@endsection
