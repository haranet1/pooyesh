@extends('panel.company.master')
@section('title','اطلاعات شرکت')
@section('main')
    <div class="page-header">
        <h1 class="page-title">اطلاعات شرکت</h1>
        <div>
            <ol class="breadcrumb">
{{--                <li class="breadcrumb-item"><a href="javascript:void(0)">رزومه</a></li>--}}
            </ol>
        </div>
    </div>
    <div class="row justify-content-center p-3">
        <div class="col-md-8 col-xl-8 col-sm-6">
            @if(Session::has('success'))
            <div class="alert alert-success text-center">
                {{Session::pull('success')}}
            </div>
            @endif
            <div class="alert alert-info alert-dismissible fade-show">
                <div class="d-flex align-items-center">
                  <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
                  <div class="flex-grow-1">
                    <p class="f-16" >
                        <strong>در این صفحه اطلاعات شرکت شما نمایش داده میشود.</strong>
                    </p>
                  </div>
                </div>
            </div>
            <div class="card">
                <div class="p-3">
                    <h4 class="mb-5 mt-3 fw-bold">درباره شرکت :</h4>
                    <p class="mb-3 fs-15">@php echo $info->about @endphp</p>

                    <h4 class="mb-5 mt-3 fw-bold">اطلاعات بیشتر :</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td class="fw-bold">نام شرکت</td>
                                <td> {{$info->name}}</td>
                            </tr>
                            <tr>
                                <td class="fw-bold"> وب سایت</td>
                                <td><a href="#">{{$info->website}}</a></td>
                            </tr>
                            <tr>
                                <td class="fw-bold">شماره تلفن  </td>
                                <td>{{$info->phone}}</td>
                            </tr>
                            <tr>
                                <td class="fw-bold"> ایمیل</td>
                                <td> {{$user->email}}</td>
                            </tr>
                            <tr>
                                <td class="fw-bold">گروه ما</td>
                                <td>
                                    {{$info->team}}
                                </td>
                            </tr>
                            <tr>
                                <td class="fw-bold">جوایز و افتخارات </td>
                                <td>
                                    {{$info->rewards}}
                                </td>
                            </tr>
                            <tr>
                                <td class="fw-bold">امضای شرکت</td>
                                <td >
                                    @if($info->signature)
                                    <a class="btn btn-success" href="{{asset($info->signature->path)}}">مشاهده</a>
                                    <a class="btn btn-primary" href="{{route('company.signature.create')}}">تغییر امضا</a>
                                    @else
                                        <a class="btn btn-success" href="{{route('company.signature.create')}}">افزودن امضا</a>

                                    @endif
                                </td>
                            </tr>
                        </table>
                        <a class="btn btn-primary" href="{{route('company.info.edit')}}">ویرایش اطلاعات</a>


                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
