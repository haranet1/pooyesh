@extends('panel.company.master')
@section('title','نمونه کارها')
@section('main')
    <style>
        .img-custom-size{
            max-width: 330px;
            min-width: 330px;
            max-height: 330px;
            min-height: 330px;
        }
    </style>
    <div class="page-header">
        <h1 class="page-title"> نمونه کارها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">نمونه کارها</a></li>
            </ol>
        </div>
    </div>
    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
          <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
          <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه نمونه کارها و نمونه پروژه های شرکت شما نمایش داده میشود.</strong>
            </p>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 my-2 ">
            <a class="btn btn-success" href="{{route('company.portfolio.create')}}">افزودن پروژه نمونه</a>
        </div>
        @if(count($samples) >0)
            @foreach($samples as $sample)
                <div class="col-md-6 col-xl-4 col-sm-6">
                    <div class="card">
                        <div class="product-grid6">
                            <div class="product-image6 p-5">
                                <ul class="icons">
                                   {{-- <li>
                                        <a href="#" class="btn btn-primary"> <i class="fe fe-eye"> </i> </a>
                                    </li>
                                    <li><a href="#" class="btn btn-success"><i class="fe fe-edit"></i></a></li>
                                    <li><a href="javascript:void(0)" class="btn btn-danger"><i class="fe fe-x"></i></a>
                                    </li>--}}
                                </ul>
                                <a href="#">
                                    @if($sample->photo)
                                        <img class="img-fluid img-custom-size br-7 w-100" src="{{asset($sample->photo->path)}}"
                                             alt="img">
                                    @else
                                    <img class="img-fluid img-custom-size br-7 w-100" src="{{asset('assets/images/default-image.svg')}}"
                                         alt="img">
                                    @endif
                                </a>
                            </div>
                            <div class="card-body pt-0">
                                <div class="product-content text-center">
                                    <h1 class="title fw-bold fs-20"><a href="#">{{$sample->title}}</a></h1>
                                    <div class="mb-2 text-warning">
                                        {{Str::limit($sample->description,'40','...')}}
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                {{--                        <a href="cart.html" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart mx-2"></i>مشاهده</a>--}}
                                {{--                        <a href="wishlist.html" class="btn btn-outline-primary mb-1"><i class="fe fe-heart mx-2 wishlist-icon"></i>افزودن به علاقه مندی</a>--}}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="alert alert-warning my-3 text-center w-100">هنوز پروژه نمونه ای اضافه نکرده اید </div>
        @endif

        <div class="mb-5">
            {{$samples->links('pagination.panel')}}
        </div>
    </div>


@endsection
