@extends('panel.company.master')
@section('title','لیست پیام های دریافتی')
@section('main')
    <div class="page-header">
        <h1 class="page-title">پیام های دریافتی</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">لیست پیام های دریافتی</a></li>
            </ol>
        </div>
    </div>
    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
          <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
          <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه لیست پیام هایی که برای شما فرستاده شده است نمایش داده میشود.</strong>
            </p>
          </div>
        </div>
    </div>
    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
                {{--<input type="text" class="form-control" id="search" onkeyup="search()" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="card-header border-bottom-0">

                    <div class=" ">

                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($tickets) > 0)
                        <table class="table border-top table-bordered mb-0" id="table">
                            <thead>
                            <tr>
                                <th>موضوع </th>
                                <th>فرستنده</th>
                                <th>ایمیل فرستنده</th>
                                <th>متن</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tickets as $ticket)
                            <tr>
                                <td class="text-nowrap align-middle">{{$ticket->subject}}</td>
                                <td class="text-nowrap align-middle">{{$ticket->sender->name." ".$ticket->sender->family}}</td>
                                <td class="text-nowrap align-middle"><span>{{$ticket->sender_email}}</span></td>
                                <td class="text-nowrap align-middle">{{Str::limit($ticket->content,'20','...')}}</td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="javascript:void(0)" onclick="deleteTicket({{$ticket->id}})"  class="btn btn-sm btn-danger badge" type="button">حذف </a>
                                        @if(is_null($ticket->read_at))
                                            <a href="{{route('company.tickets.read',$ticket)}})" class="btn btn-sm btn-success badge" type="button">خوانده شد </a>
                                        @endif
                                            <a class="btn btn-primary " data-bs-target="#modal-ticket-{{$ticket->id}}" data-bs-toggle="modal" href="javascript:void(0)">
                                            مشاهده پیام </a>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade" id="modal-ticket-{{$ticket->id}}">
                                <div
                                    class="modal-dialog" role="document">
                                    <div class="modal-content modal-content-demo">
                                        <div class="modal-header">
                                            <h6 class="modal-title text-primary">متن پیام</h6>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                {{ $ticket->content}}
                                            </p>
                                            <hr class="bg-primary my-4">
                                                <h6>ارسال پاسخ:</h6>
                                                <form class="py-1" action="{{route('company.tickets.send_reply')}}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="parent_id" value="{{$ticket->id}}">
                                                    <textarea class="form-control bi-textarea" name="content" rows="6" cols="4" ></textarea>
                                                    <button class="btn mt-2 btn-success" type="submit">ارسال</button>
                                                </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button
                                                class="btn btn-light"
                                                data-bs-dismiss="modal">
                                                بستن
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif

                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$tickets->links()}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        function deleteTicket(id){
            Swal.fire({
                title: 'آیا مطمئن هستید؟',
                text: "پیام حذف خواهد شد!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#198f20',
                cancelButtonColor: '#d33',
                cancelButtonText: 'انصراف',
                confirmButtonText: 'بله'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        headers:{
                            'X-CSRF-TOKEN':'{{csrf_token()}}'
                        },
                        url:"{{route('company.ajax.ticket.delete-ticket')}}",
                        type:'post',
                        data:{
                            id:id,
                        },
                        success:function (response){
                            location.reload()
                        },
                        error:function (response){
                            console.log(response)
                        }
                    })
                }
            })
        }
    </script>

@endsection
