@extends('panel.company.master')
@section('title','درخواست های کاراموزی ا')
@section('main')
    <div class="page-header">
        <h1 class="page-title">درخواست های کاراموزی ارسال شده</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">درخواست های کاراموزی ارسال شده</a></li>
            </ol>
        </div>
    </div>

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
        <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
        <div class="flex-grow-1">
            <p class="f-16" >
                <strong>در این صفحه لیست درخواست های کارآموزی (پویش) که شما به دانشجویان فرستاده اید نمایش داده میشود.</strong>
            </p>
        </div>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="row row-cards justify-content-center">
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="panel panel-primary">
                            <div class="tab-menu-heading tab-menu-heading-boxed">
                                <div class="tabs-menu-boxed">
                                    <ul class="nav panel-tabs">
                                        <li><a href="#tab25" class="active" data-bs-toggle="tab">درخواست های ارسالی</a>
                                        </li>
                                        <li><a href="#tab26" data-bs-toggle="tab">درخواست های پذیرفته شده</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body tabs-menu-body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab25">
                                        <div class="card">
                                            <div class="card-body">
                                                @if(count($requests) > 0)
                                                    @foreach($requests as $request)
                                                        @if( is_null($request->status))
                                                            <div class="col-12">
                                                                <div class="card"
                                                                     style="border: 1px dashed black !important;">
                                                                    <div class="row  justify-content-center">
                                                                        <div class="col-md-2 p-0 m-3 text-center">
                                                                            @if($request->receiver->photo)
                                                                                <img class="img-fluid rounded "
                                                                                     src="{{asset($request->receiver->photo->path)}}"
                                                                                     alt="avatar">
                                                                            @else
                                                                                <img class="img-fluid rounded "
                                                                                     src="{{asset('assets/front/images/user-avatar.svg')}}"
                                                                                     alt="avatar">
                                                                            @endif

                                                                            <span
                                                                                class="badge bg-primary">کارآموزی</span>
                                                                        </div>

                                                                        <div
                                                                            class="col-md-3 d-flex  align-items-center ">
                                                                            <h6>{{$request->receiver->name." ".$request->receiver->family}}</h6>
                                                                        </div>
                                                                        <div
                                                                            class="col-md-3 d-flex align-items-center  ">
                                                                            <div class="row">
                                                                                <div class="col-12 justify-content-end">
                                                                                    <div class="btn-group ">
                                                                                        <a href="{{route('candidate.single',$request->receiver_id)}}"
                                                                                           class="btn btn-sm my-1 btn-primary badge"
                                                                                           type="button">نمایش
                                                                                            پروفایل </a>
                                                                                        {{--<a href="javascript:void(0)" id="{{$request->sender->id}}" class="std-id btn btn-sm my-1 btn-info badge" type="button">
                                                                                            درخواست معرفی نامه
                                                                                             </a>--}}
                                                                                        @if($request->proccess && $request->proccess->accepted_by_std_at)
                                                                                        <a href="{{route('company.get-std-letter',$request->receiver_id)}}"
                                                                                           class="btn btn-sm my-1 btn-primary badge"
                                                                                           type="button">درخواست معرفی
                                                                                            نامه
                                                                                        </a>
                                                                                            @endif

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <div
                                                        class="alert alert-warning w-100 text-center">{{__('public.no_info')}}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab26">
                                        <div class="card">
                                            <div class="card-body">
                                                @if(count($requests) > 0)
                                                    @foreach($requests as $request)
                                                        @if(!is_null($request->status))

                                                            <div class="col-12">
                                                                <div class="card"
                                                                     style="border: 1px dashed black !important;">
                                                                    <div class="row  justify-content-center">
                                                                        <div class="col-md-2 p-0 m-3 text-center">
                                                                            @if($request->receiver->photo)
                                                                                <img class="img-fluid rounded "
                                                                                     src="{{asset($request->receiver->photo->path)}}"
                                                                                     alt="avatar">
                                                                            @else
                                                                                <img class="img-fluid rounded "
                                                                                     src="{{asset('assets/front/images/user-avatar.svg')}}"
                                                                                     alt="avatar">
                                                                            @endif

                                                                            <span
                                                                                class="badge bg-primary">کارآموزی</span>
                                                                        </div>

                                                                        <div
                                                                            class="col-md-3 d-flex  align-items-center ">
                                                                            <h6>{{$request->receiver->name." ".$request->receiver->family}}</h6>
                                                                        </div>
                                                                        <div
                                                                            class="col-md-3 d-flex align-items-center  ">
                                                                            <div class="row">
                                                                                <div class="col-12 justify-content-end">
                                                                                    <div class="btn-group ">
                                                                                        <a href="{{route('candidate.single',$request->receiver_id)}}"
                                                                                           class="btn btn-sm my-1 btn-primary badge"
                                                                                           type="button">نمایش
                                                                                            پروفایل </a>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-12 justify-content-end">
                                                                                    <div class="btn-group ">
                                                                                        <a class="btn btn-primary"
                                                                                           data-bs-target="#modaldemo-{{$request->id}}"
                                                                                           data-bs-toggle="modal"
                                                                                           href="javascript:void(0)">نمایش
                                                                                            روند درخواست</a>
                                                                                    </div>
                                                                                    @if($request->contract)
                                                                                        <a href="{{asset($request->contract->path)}}" download
                                                                                           class="btn btn-sm btn-success badge" type="button">دانلود
                                                                                            قرارداد</a>
                                                                                    @endif
                                                                                    @if(is_null($request->process->accepted_by_uni_at) && is_null($request->process->rejected_by_uni_at))
                                                                                        <span class="badge bg-warning"> در انتظار بررسی توسط دانشگاه</span>
                                                                                    @endif
                                                                                    <div class="btn-group my-2 ">
                                                                                        @if(!is_null($request->process->accepted_by_uni_at) || !is_null($request->process->rejected_by_uni_at))
                                                                                            @if(getPooyeshProcessStatus($request->process->id) !=='اتمام' && !str_contains(getPooyeshProcessStatus($request->process->id),'لغو') )
                                                                                                <a class="btn cancel btn-danger"
                                                                                                   id="{{$request->id}}"
                                                                                                   href="javascript:void(0)">لغو</a>

                                                                                                <a class="btn done btn-success"
                                                                                                   id="{{$request->id}}"
                                                                                                   href="javascript:void(0)">اتمام</a>
                                                                                            @endif
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="modal fade "
                                                                                         id="modaldemo-{{$request->id}}">
                                                                                        <div
                                                                                            class="modal-dialog modal-lg"
                                                                                            role="document">
                                                                                            <div
                                                                                                class="modal-content modal-content-demo">
                                                                                                <div
                                                                                                    class="modal-header">
                                                                                                    <h6 class="modal-title">
                                                                                                        روند پویش
                                                                                                        کارجو {{$request->receiver->name." ".$request->receiver->family}}</h6>
                                                                                                    <button
                                                                                                        aria-label="Close"
                                                                                                        class="btn-close"
                                                                                                        data-bs-dismiss="modal">
                                                                                                        <span
                                                                                                            aria-hidden="true ">&times;</span>
                                                                                                    </button>
                                                                                                </div>
                                                                                                <div class="modal-body">
                                                                                                    <div
                                                                                                        class="card-body">
                                                                                                        <div
                                                                                                            class="vtimeline">
                                                                                                            @php($timeline = getPooyeshProcess($request->id))
                                                                                                            @php($flag = 0)
                                                                                                            @foreach($timeline as $key => $value)
                                                                                                                @if($flag % 2 == 0)
                                                                                                                    <div
                                                                                                                        class="timeline-wrapper timeline-wrapper-primary">
                                                                                                                        <div
                                                                                                                            class="avatar avatar-md timeline-badge">
<span class="timeline-icon"><svg style="width:25px;height:25px" viewBox="0 0 24 24">
<path fill="currentColor"
      d="M4,2A2,2 0 0,0 2,4V11C2,11.55 2.22,12.05 2.59,12.42L11.59,21.42C11.95,21.78 12.45,22 13,22C13.55,22 14.05,21.78 14.41,21.41L21.41,14.41C21.78,14.05 22,13.55 22,13C22,12.45 21.77,11.94 21.41,11.58L12.41,2.58C12.05,2.22 11.55,2 11,2H4V2M11,4L20,13L13,20L4,11V4H11V4H11M6.5,5A1.5,1.5 0 0,0 5,6.5A1.5,1.5 0 0,0 6.5,8A1.5,1.5 0 0,0 8,6.5A1.5,1.5 0 0,0 6.5,5M10.95,10.5C9.82,10.5 8.9,11.42 8.9,12.55C8.9,13.12 9.13,13.62 9.5,14L13,17.5L16.5,14C16.87,13.63 17.1,13.11 17.1,12.55A2.05,2.05 0 0,0 15.05,10.5C14.5,10.5 13.97,10.73 13.6,11.1L13,11.7L12.4,11.11C12.03,10.73 11.5,10.5 10.95,10.5Z"/>
</svg></span>
                                                                                                                        </div>
                                                                                                                        <div
                                                                                                                            class="timeline-panel">
                                                                                                                            <div
                                                                                                                                class="timeline-heading">
                                                                                                                                <h6 class="timeline-title">{{$key}}</h6>
                                                                                                                            </div>
                                                                                                                            <div
                                                                                                                                class="timeline-body">
                                                                                                                                <p></p>
                                                                                                                            </div>
                                                                                                                            <div
                                                                                                                                class="timeline-footer d-flex align-items-center flex-wrap">
                                                                                                                                <span
                                                                                                                                    class="ms-auto"><i
                                                                                                                                        class="fe fe-calendar text-muted mx-1"></i>{{verta($value)->formatDate()}}</span>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                @else
                                                                                                                    <div
                                                                                                                        class="timeline-wrapper timeline-inverted timeline-wrapper-secondary">
                                                                                                                        <div
                                                                                                                            class="avatar avatar-md timeline-badge">
 <span class="timeline-icon"><svg style="width:26px;height:26px" viewBox="0 0 24 24">
<path fill="currentColor"
      d="M12 4C14.2 4 16 5.8 16 8C16 10.1 13.9 13.5 12 15.9C10.1 13.4 8 10.1 8 8C8 5.8 9.8 4 12 4M12 2C8.7 2 6 4.7 6 8C6 12.5 12 19 12 19S18 12.4 18 8C18 4.7 15.3 2 12 2M12 6C10.9 6 10 6.9 10 8S10.9 10 12 10 14 9.1 14 8 13.1 6 12 6M20 19C20 21.2 16.4 23 12 23S4 21.2 4 19C4 17.7 5.2 16.6 7.1 15.8L7.7 16.7C6.7 17.2 6 17.8 6 18.5C6 19.9 8.7 21 12 21S18 19.9 18 18.5C18 17.8 17.3 17.2 16.2 16.7L16.8 15.8C18.8 16.6 20 17.7 20 19Z"/>
</svg></span>
                                                                                                                        </div>
                                                                                                                        <div
                                                                                                                            class="timeline-panel">
                                                                                                                            <div
                                                                                                                                class="timeline-heading">
                                                                                                                                <h6 class="timeline-title">{{$key}}</h6>
                                                                                                                            </div>
                                                                                                                            <div
                                                                                                                                class="timeline-body">

                                                                                                                            </div>
                                                                                                                            <div
                                                                                                                                class="timeline-footer d-flex align-items-center flex-wrap">
                                                                                                                                <span
                                                                                                                                    class="ms-auto"><i
                                                                                                                                        class="fe fe-calendar text-muted mx-1"></i> {{verta($value)->formatDate()}}</span>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                @endif
                                                                                                                @php($flag++)
                                                                                                            @endforeach

                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="modal-footer">
                                                                                                    <button
                                                                                                        class="btn btn-light"
                                                                                                        data-bs-dismiss="modal">
                                                                                                        بستن
                                                                                                    </button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <div class="alert alert-warning w-100 text-center">درخواستی جهت نمایش
                                                        وجود ندارد
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        $('.accept').on('click', function () {
            let id = $(this).attr('id')
            Swal.fire({
                title: 'درخواست کارجو تایید خواهد شد!',
                showCancelButton: true,
                confirmButtonText: 'تایید',
                cancelButtonText: `انصراف`,
                icon: 'question',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        },
                        url: '{{route('company.ajax')}}',
                        type: 'post',
                        data: {
                            do: 'set-cooperation-request-status',
                            status: 1,
                            id: id
                        },
                        success: function (response) {
                            Swal.fire(
                                'موفق',
                                'درخواست همکاری تایید شد!',
                                'success'
                            )
                            location.reload()
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    })

                }
            })

        })

        $('.decline').on('click', function () {
            let id = $(this).attr('id')
            Swal.fire({
                title: 'درخواست کارجو رد خواهد شد!',
                showCancelButton: true,
                confirmButtonText: 'تایید',
                cancelButtonText: `انصراف`,
                icon: 'question',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        },
                        url: '{{route('company.ajax')}}',
                        type: 'post',
                        data: {
                            do: 'set-cooperation-request-status',
                            status: 0,
                            id: id
                        },
                        success: function (response) {
                            Swal.fire(
                                'موفق',
                                'درخواست همکاری رد شد!',
                                'success'
                            )
                            location.reload()
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    })


                }
            })

        })

        $('.cancel').on('click', function () {
            let id = $(this).attr('id')
            Swal.fire({
                title: 'آیا مایل به لغو این درخواست هستید؟',
                showCancelButton: true,
                confirmButtonText: 'بله',
                cancelButtonText: `خیر`,
                icon: 'question',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        },
                        url: '{{route('company.ajax')}}',
                        type: 'post',
                        data: {
                            do: 'cancel-pooyesh-request-status',
                            id: id
                        },
                        success: function (response) {
                            Swal.fire(
                                'موفق',
                                'درخواست لغو شد!',
                                'success'
                            )
                            location.reload()
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    })


                }
            })

        })

        $('.done').on('click', function () {
            let id = $(this).attr('id')
            Swal.fire({
                title: 'آیا مایل به انجام این عملیات هستید؟',
                text: 'وضعیت کارآموزی این کارجو به "تمام شده" تغییر وضعیت خواهد داد.',
                showCancelButton: true,
                confirmButtonText: 'بله',
                cancelButtonText: `خیر`,
                icon: 'question',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        },
                        url: '{{route('company.ajax')}}',
                        type: 'post',
                        data: {
                            do: 'done-pooyesh-request-status',
                            id: id
                        },
                        success: function (response) {
                            Swal.fire(
                                'موفق',
                                'عملیات با موفقیت انجام شد!',
                                'success'
                            )
                            location.reload()
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    })


                }
            })

        })

    </script>

    {{--  <script>
          $('.btn.std-id').on('click',function (){
              let id = this.id
              Swal.fire({
                  title: 'مایا به دریافت معرفی نامه این کارجو هستید؟',
                  showCancelButton: true,
                  confirmButtonText: 'بله',
                  cancelButtonText: `خیر`,
                  icon: 'question',
              }).then((result) => {
                  if (result.isConfirmed) {
                      $.ajax({
                          headers:{
                              'X-CSRF-TOKEN':'{{csrf_token()}}'
                          },
                          url:'{{route('company.ajax')}}',
                          type:'post',
                          data:{
                              do:'get-std-intro-letter',
                              company_id: '{{\Illuminate\Support\Facades\Auth::id()}}',
                              id:id
                          },
                          success:function(response) {
                              console.log(response);
                          },
                          error:function (response){
                              console.log(response)
                          }
                      })


                  }
              })

          })
      </script>--}}
@endsection
