@extends('panel.student.master')

@section('title','لیست شرکت ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">فهرست شرکت ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد</a></li>
                <li class="breadcrumb-item active" aria-current="page">فهرست شرکت ها</li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
                <input type="text" onkeyup="search()" id="search" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="page-options ms-auto">
                        {{-- <select class="form-control select2 w-100">
                             <option value="asc">آخرین</option>
                             <option value="desc">قدیمی ترین</option>
                         </select>--}}
                    </div>
                </div>
                @if(count($companies) > 0)
                    <div class="e-table px-5 pb-5">
                        <div class="table-responsive table-lg">
                            <table class="table border-top table-bordered mb-0" id="table">
                                <thead>
                                <tr>
                                    <th>نام شرکت</th>
                                    <th> فیلد کاری</th>
                                    <th> محل</th>
                                    <th> موقعیت های شغلی باز</th>
                                    <th class="text-center">عملکردها</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $company)
                                    <tr>
                                        <td class="text-nowrap align-middle">{{$company->groupable->name}}</td>
                                        <td class="text-nowrap align-middle">{{$company->groupable->activity_field}}</td>
                                        <td class="text-nowrap align-middle">{{$company->groupable->city}}</td>
                                        <td class="text-nowrap align-middle">{{count($company->job_positions)}}</td>
                                        <td class="text-center align-middle">
                                            <div class="btn-group align-top">
                                                <a href="{{route('company.single',$company->id)}}"
                                                   class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل
                                                    شرکت </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                @else
                    <div class="row m-3 justify-content-center">
                        <div class="col-8">
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="mb-5">
                {{$companies->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        function search() {
            var table = document.getElementById('table')
            var key = $('#search').val()

            $('#table tr').remove()
            table.innerHTML = '<tr>\n' +
                '                            <th>نام شرکت</th>\n' +
                '                            <th>فیلد کاری</th>\n' +
                '                            <th>محل </th>\n' +
                '                            <th>موقعیت های شغلی باز</th>\n' +
                '                            <th>عملکردها</th>\n' +
                '                        </tr>'


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: 'POST',
                url: '{{route('students.ajax.search')}}',
                data: {
                    do: 'search-companies',
                    key: key
                },
                success: function (response) {
                    response['data'].forEach(function (re) {
                        var route = "{{route('company.single',['id' => ':id'])}}"
                        route = route.replace(':id', re['user']['id'])
                        var row = table.insertRow(-1);
                        var td0 = row.insertCell(0)
                        var td1 = row.insertCell(1)
                        var td2 = row.insertCell(2)
                        var td3 = row.insertCell(3)
                        var td4 = row.insertCell(4)
                        td0.innerHTML = re['name']
                        td1.innerHTML = re['activity_field']
                        td2.innerHTML = re['city']
                        td3.innerHTML = re['user']['job_positions'].length
                        td4.innerHTML = '<div class="btn-group align-top">' +
                            ' <a href="' + route + '" class="btn btn-sm btn-primary badge" type="button">مشاهده پروفایل شرکت </a> </div>'
                    })
                },
                error: function (data) {
                    console.log(data)
                }
            })


        }
    </script>
@endsection
