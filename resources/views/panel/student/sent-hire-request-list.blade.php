@extends('panel.student.master')

@section('title','لیست درخواستها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">فهرست درخواستها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد</a></li>
                <li class="breadcrumb-item active" aria-current="page">فهرست درخواستها</li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">

            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="page-options ms-auto">
                       {{-- <select class="form-control select2 w-100">
                            <option value="asc">آخرین</option>
                            <option value="desc">قدیمی ترین</option>
                        </select>--}}
                    </div>
                </div>
                @if(count($requests) > 0)
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>گیرنده</th>
                                <th> شغل</th>
                                <th> نوع درخواست</th>
                                <th>وضعیت</th>
                                <th>تاریخ ارسال</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($requests as $request)
                            <tr>
                                <td class="text-nowrap align-middle">{{$request->receiver->groupable->name}}</td>
                                <td class="text-nowrap align-middle">{{$request->job->title}}</td>
                                <td class="text-nowrap align-middle">
                                    @if($request->type!='intern')
                                        <span class="badge bg-primary">استخدام</span>
                                    @elseif($request->type == 'intern')
                                        <span class="badge bg-success">کارآموزی</span>
                                    @endif

                                </td>
                                <td>
                                    @if($request->status === 1)
                                        <span class="badge bg-success">قبول شده</span>
                                    @elseif($request->status === 0)
                                        <span class="badge bg-danger"> رد شده</span>
                                    @else
                                        <span class="badge bg-primary">در انتظار بررسی</span>
                                    @endif
                                </td>
                                <td class="text-nowrap align-middle">{{verta($request->created_at)->formatDate()}}</td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="{{route('company.single',$request->receiver->id)}}" class="btn btn-sm btn-primary badge" data-target="#user-form-modal" data-bs-toggle="" type="button">مشاهده پروفایل شرکت </a>

                                    </div>

                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @else
                    <div class="row m-3 justify-content-center">
                        <div class="col-8">
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
