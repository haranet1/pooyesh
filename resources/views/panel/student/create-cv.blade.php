@extends('panel.student.master')
@section('title','ساخت رزومه')
@section('main')
<style>
    .autocomplete {
    /*the container must be positioned relative:*/
    position: relative;
    display: inline-block;
    }

    .autocomplete-items {
    position: absolute;
    border: 1px solid #d4d4d4;
    border-bottom: none;
    border-top: none;
    z-index: 99;
    /*position the autocomplete items to be the same width as the container:*/
    top: 100%;
    left: 0;
    right: 0;
    }
    .autocomplete-items div {
    padding: 10px;
    cursor: pointer;
    background-color: #fff;
    border-bottom: 1px solid #d4d4d4;
    }
    .autocomplete-items div:hover {
    /*when hovering an item:*/
    background-color: #e9e9e9;
    }
    .autocomplete-active {
    /*when navigating through the items using the arrow keys:*/
    background-color: DodgerBlue !important;
    color: #ffffff;
    }
</style>
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        @if(Session::has('success'))
            <div class="row">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="row">
                <div class="alert alert-danger">
                    {{Session::pull('error')}}
                </div>
            </div>
        @endif
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ساخت رزومه</a></li>
            </ol>
        </div>
    </div>
    
    <div class="row row-cards justify-content-center">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <h3 class="card-title">ساخت رزومه</h3>
                    @if (Auth::user()->isConfirmed())
                        <a href="{{route('candidate.single',Auth::id())}}" class="btn btn-warning">نمایش رزومه</a>
                    @else 
                        <a href="" class="btn btn-warning disabled">برای نمایش رزومه پروفایل شما باید توسط مدیر تایید شود</a>
                    @endif
                </div>
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="tab-menu-heading tab-menu-heading-boxed">
                            <div class="tabs-menu-boxed">
                                <ul class="overflow-auto nav panel-tabs d-flex flex-nowrap">
                                    <li><a id="head-tab28" href="#tab28" class="active" style="white-space: nowrap" data-bs-toggle="tab">اطلاعات آکادمیک</a></li>
                                    <li><a id="head-tab25" href="#tab25" style="white-space: nowrap" data-bs-toggle="tab">مهارت ها</a></li>
                                    <li><a id="head-tab26" href="#tab26" style="white-space: nowrap" data-bs-toggle="tab">تخصص ها</a></li>
                                    <li><a id="head-tab27" href="#tab27" style="white-space: nowrap" data-bs-toggle="tab">زبان های خارجی</a></li>
                                    <li><a id="head-tab30" href="#tab30" style="white-space: nowrap" data-bs-toggle="tab">سوابق کاری</a></li>
                                    <li><a id="head-tab31" href="#tab31" style="white-space: nowrap" data-bs-toggle="tab">شبکه های اجتماعی</a></li>
                                    <li><a id="head-tab29" href="#tab29" style="white-space: nowrap" data-bs-toggle="tab">دوره ها و گواهینامه ها</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="panel-body tabs-menu-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab28">
                                    <form action="">
                                        <div class="mb-4  row justify-content-end">
                                            <div class="row w-100 d-flex">
                                                <div class="col-md-2" style="padding-left: 0.25rem !important; padding-right: 0.25rem !important;">
                                                    <select  class="my-2 form-control form-select" name="grade" id="grade">
                                                        <option value="">انتخاب مقطع</option>
                                                        <option value="دکترا">دکترا</option>
                                                        <option value="کارشناسی ارشد">کارشناسی ارشد</option>
                                                        <option value="کارشناسی">کارشناسی</option>
                                                        <option value="کاردانی">کاردانی</option>
                                                        <option value="دیپلم">دیپلم</option>
                                                    </select>
                                                    <span class="text-danger" id="gradeError"></span>
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0.25rem !important; padding-right: 0.25rem !important;">
                                                    <input type="text"  name="major" id="major" class="my-2 form-control" placeholder="رشته">
                                                    <span class="text-danger" id="majorError"></span>
                                                </div>
                                                <div class="col-md-3 px-25rem" style="padding-left: 0.25rem !important; padding-right: 0.25rem !important;">
                                                    <input type="text"  name="university" id="university" class="my-2 form-control" placeholder="دانشگاه / موسسه آموزشی">
                                                    <span class="text-danger" id="universityError"></span>
                                                </div>
                                                <div class="col-md-2 px-25rem" style="padding-left: 0.25rem !important; padding-right: 0.25rem !important;">
                                                    <input type="text" name="end" id="end" class="my-2 form-control" placeholder="سال اخذ مدرک">
                                                    <span class="text-danger" id="endError"></span>
                                                </div>
                                                <div class="col-md-2 px-25rem" style="padding-left: 0.25rem !important; padding-right: 0.25rem !important;">
                                                    <input type="text" name="avg" id="avg" class="mt-2 mb-3 form-control" placeholder="معدل">
                                                    <span class="text-danger" id="avgError"></span>
                                                </div>
                                                <div class="px-0 col-md-1 align-items-center">
                                                    <a href="javascript:void(0)" onclick="addGrade()" class="mx-1 mt-2 btn btn-success"><i class="fa fa-check"></i></a>
                                                </div>
                                            </div>
                                            @error('grade') <span class="text-danger">{{$message}}</span> @enderror
                                            <div class="mt-2 col-12">
                                    
                                                <table id="gradeTb" class="<?php count(Auth::user()->academicInfos) > 0 ? '' : 'd-none' ?>mt-3 table table-bordered text-center w-100 table-responsive-md">
                                                    <tr class="">
                                                        <th style="white-space: nowrap">مقطع تحصیلی</th>
                                                        <th style="white-space: nowrap">رشته</th>
                                                        <th style="white-space: nowrap">دانشگاه / موسسه آموزشی</th>
                                                        <th style="white-space: nowrap">سال اخذ مدرک</th>
                                                        <th style="white-space: nowrap">معدل</th>
                                                        <th style="white-space: nowrap">حذف</th>
                                                    </tr>
                                                    @forelse (Auth::user()->academicInfos as $academicInfo)
                                                    <tr class="">
                                                        <th style="white-space: nowrap">{{$academicInfo->grade}}</th>
                                                        <th style="white-space: nowrap">{{$academicInfo->major}}</th>
                                                        <th style="white-space: nowrap">{{$academicInfo->university}}</th>
                                                        <th style="white-space: nowrap">{{$academicInfo->end_date}}</th>
                                                        <th style="white-space: nowrap">{{$academicInfo->avg}}</th>
                                                        <th style="white-space: nowrap">
                                                            <a href="javascript:void(0)" onClick="deleteAcInfo({{$academicInfo->id}},this)" class="btn btn-danger">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </th>
                                                    </tr>
                                                    @empty
                                                        
                                                    @endforelse
                                                </table>
                                                
                                            </div>
                                            <div class="mt-3 col-4 col-md-2">
                                                <a href="javascript:void(0)" class="btn btn-primary w-100 d-none" onclick="storeAcademicInfo()" id="AcademicBtn">ثبت</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="tab-pane " id="tab25">
                                    <div class="mb-4  row">
                                            {{-- skills --}}
                                            <div class="col-md-12 d-flex">
                                                <div class="row w-100 d-flex">
                                                    <label class="col-md-1 col-12 form-label" style="white-space: nowrap">عنوان مهارت :</label>
                                                    <div class="col-md-3 col-12">
                                                        <div class="autocomplete w-100">
                                                            <input name="skills" id="skills" autocomplete="off"  type="text" class="form-control " >
                                                            <input name="skill" id="skill" type="hidden">
                                                        </div>
                                                    </div>
                                                    <label class="col-md-1 col-12 form-label" style="white-space: nowrap">سطح مهارت :</label>
                                                    <div class="col-md-3 col-12">
                                                        <select class="form-control form-select" name="skill_level" id="skill_level">
                                                            <option value="عالی">عالی</option>
                                                            <option value="خوب">خوب</option>
                                                            <option value="متوسط">متوسط</option>
                                                            <option value="ضعیف">ضعیف</option>
                                                        </select>
                                                    </div>
                                                    <div class="px-0 col-md-1 align-items-center">
                                                        <a href="javascript:void(0)" id="addSkillBtn" onclick="addSkill()" class="mx-1 mt-3 btn disabled btn-success mt-md-0"><i class="fa fa-check"></i></a>
                                                    </div>

                                                    
                                                </div>
                                            </div>
                                            <div class="mt-2 col-12">
                                            <div class="selectgroup selectgroup-pills w-100" id="skill-list">
                                                @forelse (Auth::user()->skills as $skill)
                                                    <label class="selectgroup-item">
                                                        <span class="selectgroup-button"> 
                                                            <a href="javascript:void(0)" onclick="deleteExistingSkill({{$skill->id}},this,'skills')">
                                                                <i class="fa fa-trash text-danger"></i>
                                                            {{-- </a>  {{$skill->name}}  | {{$skill->pivot->skill_level}} --}}
                                                        </span>
                                                    </label>
                                                @empty
                                                    
                                                @endforelse
                                            </div>
                                            <div class="px-2">
                                                <a href="javascript:void(0)" id="submitSkills" class="mt-3 d-none btn btn-primary" onclick="storeSkills()">ثبت مهارت ها</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab26">
                                    <div class="mb-4  row">
                                        {{-- Expertises --}}
                                        <div class="col-md-12 d-flex">
                                            <div class="row w-100 d-flex">
                                                <label class="col-md-1 col-12 form-label" style="white-space: nowrap">عنوان تخصص :</label>
                                                <div class="col-md-3 col-12">
                                                    <div class="autocomplete w-100">
                                                        <input name="expertises" id="expertises" autocomplete="off"  type="text" class="form-control " >
                                                        <input name="expertise" id="expertise" type="hidden">
                                                    </div>
                                                </div>
                                                <label class="col-md-1 col-12 form-label" style="white-space: nowrap">سطح تخصص :</label>
                                                <div class="col-md-3 col-12">
                                                    <select class="form-control form-select" name="expertise_level" id="expertise_level">
                                                        <option value="عالی">عالی</option>
                                                        <option value="خوب">خوب</option>
                                                        <option value="متوسط">متوسط</option>
                                                        <option value="ضعیف">ضعیف</option>
                                                    </select>
                                                </div>
                                                <div class="px-0 col-md-1 align-items-center">
                                                    <a href="javascript:void(0)" id="addExpertiseBtn" onclick="addExpertise()" class="mx-1 mt-3 btn disabled btn-success mt-md-0"><i class="fa fa-check"></i></a>
                                                </div>

                                                
                                            </div>
                                        </div>
                                        <div class="mt-2 col-12">
                                        <div class="selectgroup selectgroup-pills w-100" id="expertise-list">
                                            @forelse (Auth::user()->expertises as $expertise)
                                            <label class="selectgroup-item">
                                                <span class="selectgroup-button"> 
                                                    <a href="javascript:void(0)" onclick="deleteExistingSkill({{$expertise->id}},this,'expertises')">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>  {{$expertise->name}}  | {{$expertise->pivot->skill_level}}
                                                </span>
                                            </label>
                                            @empty
                                                
                                            @endforelse
                                        </div>
                                        <div class="px-2">
                                            <a href="javascript:void(0)" id="submitExpertises" class="mt-3 d-none btn btn-primary" onclick="storeExpertises()">ثبت تخصص ها</a>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <div class="tab-pane" id="tab27">
                                    <div class="mb-4  row">
                                        {{-- lang --}}
                                        <div class="col-md-12 d-flex">
                                            <div class="row w-100 d-flex">
                                            <label class="col-md-1 col-12 form-label" style="white-space: nowrap">عنوان :</label>
                                                <div class="col-md-3 col-12">
                                                    <select class="form-control form-select" name="lang-title" id="lang-title">
                                                        @foreach ($langs as $lang)
                                                            <option value="{{$lang->id}}">{{$lang->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <label class="col-md-1 col-12 form-label" style="white-space: nowrap">سطح زبان :</label>
                                                <div class="col-md-3 col-12">
                                                    <select class="form-control form-select" name="lang_level" id="lang_level">
                                                        <option value="عالی">عالی</option>
                                                        <option value="خوب">خوب</option>
                                                        <option value="متوسط">متوسط</option>
                                                        <option value="ضعیف">ضعیف</option>
                                                    </select>
                                                </div>
                                                <div class="px-0 col-md-1 align-items-center">
                                                    <a href="javascript:void(0)" id="addLangBtn" onclick="addLang()" class="mx-1 mt-3 btn btn-success mt-md-0"><i class="fa fa-check"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-2 col-12">
                                            <div class="selectgroup selectgroup-pills w-100" id="lang-list">
                                                @forelse (Auth::user()->languages as $language)
                                                    <label class="selectgroup-item">
                                                        <span class="selectgroup-button"> 
                                                            <a href="javascript:void(0)" onclick="deleteExistingLang({{$language->id}},this)">
                                                                <i class="fa fa-trash text-danger"></i>
                                                            </a>  {{$language->name}}  | {{$language->pivot->level}}
                                                        </span>
                                                    </label>
                                                @empty
                                                    
                                                @endforelse
                                            </div>
                                            <div class="px-2">
                                                <a href="javascript:void(0)" id="submitLangs" class="mt-3 d-none btn btn-primary" onclick="storeLangs()">ثبت زبان ها</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab29">
                                    <div class="mb-4  row">
                                        {{-- certificate --}}
                                        <div class="col-md-12 d-flex">
                                            <div class="row w-100 d-flex">
                                            <label class="col-md-1 col-12 form-label" style="white-space: nowrap">عنوان :</label>
                                                <div class="col-md-3">
                                                    <input name="certificateTitle" id="certificateTitle" type="text" class="form-control " >
                                                    <span class="text-danger" id="titleError"></span>
                                                </div>
                                                <label class="col-md-1 col-12 form-label" style="white-space: nowrap">تصویر مدرک :</label>
                                                <div class="col-md-4">
                                                    <input type="file" accept=".jpg , .jpeg , .png , .svg" class="form-control" name="certificatePhoto" id="certificatePhoto">
                                                    <span class="text-danger" id="photoError"></span>
                                                </div>
                                                <div class="px-0 col-md-1 align-items-center">
                                                    <a href="javascript:void(0)" id="addCerBtn" onclick="addCertificate()"  class="mx-1 mt-3 btn btn-success mt-md-0"><i class="fa fa-check"></i></a>
                                                </div>

                                                
                                                </div>
                                            </div>
                                            <div class="mt-2 col-12">
                                            <div class="mt-5 selectgroup selectgroup-pills w-100" id="certificate-list">
                                                @forelse (Auth::user()->certificates as $certificate )
                                                    
                                            
                                            <div class="p-2 m-2 text-center border" id="{{$certificate->id}}" style="border-radius: 7px;">
                                                <a target="_blank" href="{{$certificate->photo->path}}">
                                                    <img style="max-height: 100px; min-height: 100px; max-width: 200px; min-width: 200px;" src="{{asset($certificate->photo->path)}}" class="mb-4 img-fluid"></a>
                                                    <br>
                                                    <span class="">{{$certificate->title}}</span>
                                                    <br>
                                                    <span class="mt-2 text-danger" onclick="deleteCertificate({{$certificate->id}})" style="cursor:pointer">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </span>
                                                </div>
                                                @empty
                                                    
                                                @endforelse
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab30">
                                    {{-- Work Experience --}}
                                    <div class="mb-4  row justify-content-end">
                                        <div class="col-md-12 d-flex">
                                            <div class="row w-100 d-flex ">
                                                <div class="my-2 col-md-3">
                                                    <input name="experienceTitle" placeholder="عنوان" id="experienceTitle" type="text" class="form-control " >
                                                    <span class="text-danger" id="experienceError"></span>
                                                </div>
                                                <div class="my-2 col-md-3">
                                                    <input name="experienceCompany" id="experienceCompany" placeholder="نام سازمان" type="text" class="form-control " >
                                                    <span class="text-danger" id="CompanyError"></span>
                                                </div>

                                                <div class="my-2 col-md-2">
                                                    <input type="text" name="start" id="experienceStart" class="form-control" placeholder="سال شروع">
                                                    <span class="text-danger" id="startExpError"></span>
                                                </div>

                                                <div class="my-2 col-md-2">
                                                    <input type="text" name="end" id="experienceEnd" class="form-control" placeholder="سال پایان">
                                                    <span class="text-danger" id="endExpError"></span>
                                                </div>
                                            
                                                <div class="px-0 col-md-1 align-items-center">
                                                    <a href="javascript:void(0)" id="addExpBtn" onclick="addExperience()"  class="mx-1 mt-2 btn btn-success"><i class="fa fa-check"></i></a>
                                                </div>
                                                <div class="mt-2 col-12">
                                                    <table id="experienceTb" class="<?php count(Auth::user()->work_experiences) > 0 ? '' : 'd-none' ?> mt-3 table table-bordered text-center  w-100 table-responsive-md">
                                                        <tr class="">
                                                            <th>عنوان</th>
                                                            <th>نام سازمان</th>
                                                            <th>سال شروع</th>
                                                            <th>سال پایان</th>
                                                            <th>حذف</th>
                                                        </tr>
                                                        @forelse (Auth::user()->work_experiences as $work_exp )
                                                        <tr class="">
                                                            <th>{{$work_exp->title}}</th>
                                                            <th>{{$work_exp->company}} </th>
                                                            <th> {{verta($work_exp->start_date)->formatDate()}}</th>
                                                            <th> {{verta($work_exp->end_date)->formatDate()}}</th>
                                                            <th>
                                                                <a href="javascript:void(0)" onclick="deleteExistingExp({{$work_exp->id}},this)" class="btn btn-danger">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </th>
                                                        </tr>
                                                        @empty
                                                            
                                                        @endforelse

                                                       
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-3 col-4 col-md-2">
                                            <a href="javascript:void(0)" id="submitexperience" class="btn btn-primary w-100 d-none" onclick="storeExperience()">ثبت سوابق کاری</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab31">
                                    <div class="mb-4  row">
                                        {{-- socialNetwork --}}
                                        <div class="col-md-11 d-flex">
                                            <div class="row w-100 d-flex">
                                            <label class="col-md-2 col-12 form-label" style="white-space: nowrap">شبکه اجتماعی :</label>
                                                <div class="col-md-3 col-12">
                                                    <select class="form-control form-select" name="socialNetworkTitle" id="socialNetworkTitle">
                                                        <option value="instagram">اینستاگرام</option>
                                                        <option value="telegram">تلگرام</option>
                                                        <option value="linkedin">لینکدین</option>
                                                        <option value="twitter">توییتر</option>
                                                        <option value="github">گیت هاب</option>
                                                        <option value="eitaa">ایتا</option>
                                                    </select>
                                                </div>
                                                <label class="col-md-2 col-12 form-label"style="white-space: nowrap" >شناسه کاربری (ID) :</label>
                                                <div class="col-md-3 col-12">
                                                    <input type="type" class="form-control" name="socialNetworkLink" id="socialNetworkLink">
                                                    <span class="text-danger" id="linkError"></span>
                                                </div>
                                                <div class="px-0 col-md-1 align-items-center">
                                                    <a href="javascript:void(0)" id="addSocialNetworkBtn" onclick="addSocialNetwork()" class="mx-1 mt-3 btn btn-success mt-md-0"><i class="fa fa-check"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-2 col-12">
                                            <div class="selectgroup selectgroup-pills w-100" id="socialNetwork-list">
                                                @forelse (Auth::user()->social_networks as $Social)
                                                    <label class="selectgroup-item">
                                                        <span class="selectgroup-button"> 
                                                            <a href="javascript:void(0)" onclick="deleteExistingSocial({{$Social->id}},this)">
                                                                <i class="fa fa-trash text-danger"></i>
                                                            </a>  {{__('socialNetworks.' . $Social->name)}}  | {{$Social->link}}
                                                        </span>
                                                    </label>
                                                @empty
                                                    
                                                @endforelse
                                            </div>
                                            <div class="px-2">
                                                <a href="javascript:void(0)" id="submitSocialNetwork" class="mt-3 d-none btn btn-primary" onclick="storeSocialNetwork()">ثبت</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
@section('script')
    
    {{-- Skill script --}}

    <script>
            let skillname = document.getElementById('skills')
            skillname.addEventListener('input', function(){
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': config.csrf
                },
                url:config.routes.ajax,
                data: {
                do:'get-skills',
                    skill_title: skillname.value
                },
                dataType: 'json',
                success: function (response) {
                    autocompleteSkill(skillname,response);
                    

                },
                error: function (response) {
                console.log(response)
                }
                });
            })

            function autocompleteSkill(inp, arr) {
                var existingElement = document.getElementById("nameautocomplete-list")
                if (existingElement)
                    existingElement.remove()

                var currentFocus;
                var a, b = null
                currentFocus = -1;
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items border border-primary  bg-light py-2 px-1");
            
                inp.parentNode.appendChild(a);
            
                for (var i = 0; i < arr.length; i++) {
                b = document.createElement("DIV");
                b.innerHTML = "<strong>" + arr[i]['name'] + "</strong>";
                b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + "'>";
                b.addEventListener("click", function (e) {
                    inp.value = this.getElementsByTagName("input")[0].id;
                    skill = document.getElementById('skill')
                    skill.value = this.getElementsByTagName("input")[0].id
                    document.getElementById("skills").value = this.getElementsByTagName("input")[0].value
                    closeAllLists();
                    var skillBtn = document.getElementById('addSkillBtn')
                    skillBtn.classList.remove('disabled')
                });
                a.appendChild(b);
                }

                inp.addEventListener("keydown", function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    currentFocus++;
                    addActive(x);
                } else if (e.keyCode == 38) { 
                    currentFocus--;
                    addActive(x);
                } else if (e.keyCode == 13) {
                    e.preventDefault();
                    if (currentFocus > -1) {
                        if (x) x[currentFocus].click();
                    }
                }
                });

                function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");

                }

                function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
                }

                function closeAllLists(elmnt) {
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                
                }

                document.addEventListener("click", function (e) {
                closeAllLists(e.target);
                });

            
            }

    </script>

    {{-- Expertise script --}}
    
    <script>
        let expertisename = document.getElementById('expertises')
        expertisename.addEventListener('input', function(){
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': config.csrf
            },
            url:config.routes.ajax,
            data: {
            do:'get-expertises',
                expertises_title: expertisename.value
            },
            dataType: 'json',
            success: function (response) {
                autocompleteExpertise(expertisename,response);
            },
            error: function (response) {
            console.log(response)
            }
            });
        })

        function autocompleteExpertise(inp, arr) {
            var existingElement = document.getElementById("nameautocomplete-list")
            if (existingElement)
                existingElement.remove()

            var currentFocus;

            var a, b = null

            currentFocus = -1;
        
            a = document.createElement("DIV");
            a.setAttribute("id", inp.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items border border-primary  bg-light py-2 px-1");
            inp.parentNode.appendChild(a);
            for (var i = 0; i < arr.length; i++) {
                b = document.createElement("DIV");
                b.innerHTML = "<strong>" + arr[i]['name'] + "</strong>";
                b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + "'>";
                b.addEventListener("click", function (e) {
                    inp.value = this.getElementsByTagName("input")[0].id;
                    expertise = document.getElementById('expertise')
                    expertise.value = this.getElementsByTagName("input")[0].id
                    document.getElementById("expertises").value = this.getElementsByTagName("input")[0].value
                    closeAllLists();
                    var skillBtn = document.getElementById('addExpertiseBtn')
                    skillBtn.classList.remove('disabled')
                });
                a.appendChild(b);
            }

            inp.addEventListener("keydown", function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    currentFocus++;
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    currentFocus--;
                    addActive(x);
                } else if (e.keyCode == 13) {
                    e.preventDefault();
                    if (currentFocus > -1) {
                        if (x) x[currentFocus].click();
                    }
                }
            });

            function addActive(x) {
                if (!x) return false;
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                x[currentFocus].classList.add("autocomplete-active");

            }

            function removeActive(x) {
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }

            function closeAllLists(elmnt) {

                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
                
            }

            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });

            
        }

    </script>

    {{-- Certificate script --}}

    <script>
        const idCertArray = [];
        const CertArray = [];

        function addCertificate(){
            document.getElementById('titleError').innerHTML = ''
            document.getElementById('photoError').innerHTML = ''

            var title = document.getElementById('certificateTitle').value;
            var photo = document.getElementById('certificatePhoto').files[0];
            var maxFileSize = 1024 * 1024 ;
            if(photo.size > maxFileSize){
                document.getElementById('photoError').innerHTML = 'حجم تصویر نباید بیشتر از 1 مگابایت باشد'
                return false;
            }

            var validExtentions = ['jpg' , 'jpeg' , 'png' , 'svg'];
            var fileExtention = photo.name.split('.').pop().toLowerCase();
            if(title == ''){
                document.getElementById('titleError').innerHTML = 'لطفا عنوان مدرک را وارد کنید'
                return false;
            }

            if(photo == ''|| photo == null){
                document.getElementById('photoError').innerHTML = 'لطفا تصویر مدرک را وارد کنید'
                return false;
            }

            if (validExtentions.indexOf(fileExtention) === -1){
                document.getElementById('photoError').innerHTML = 'فرمت تصویر انتخاب شده صحیح نمیباشد'
                return false;
            }
            var formData = new FormData();
            formData.append('title', title);
            formData.append('photo', photo);

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': config.csrf
                },
                url:config.routes.uploadCertificate,
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function(){
                    $('#global-loader').removeClass('d-none');
                    $('#global-loader').addClass('d-block');
                    $('#global-loader').css('opacity', 0.6);
                },
                success: function (response) {
                    $('#global-loader').removeClass('d-block');
                    $('#global-loader').addClass('d-none');
                    $('#global-loader').css('opacity', 1);
                    $('#certificateTitle').val("")
                    $('#certificatePhoto').val("")
                    var list = document.getElementById('certificate-list');
                    var element = document.createElement('div');
                    element.classList.add('p-2','border','text-center','m-2');
                    element.setAttribute('id', response['id'])
                    element.style.borderRadius = '7px';    
                    var res = response;
                    var path = '{{asset(':url')}}'
                    path = path.replace(':url', res['path']);
                    
                    
                    element.innerHTML = '<a target="_blank" href="'+path+'"><img style="max-height: 100px; min-height: 100px; max-width: 200px; min-width: 200px;" src="'+path+'" class="mb-4 img-fluid"></img></a><br><span class="">'+res['title']+'</span><br><span class="mt-2 text-danger" onclick="deleteCertificate('+res['id']+')" style="cursor:pointer"><i class="fa fa-trash text-danger"></i></span>';
                    list.append(element)


                    
                    
                },
                error: function (xhr, status, error) {
                    console.log(xhr);
                    if(xhr.status === 422){
                        $('#global-loader').removeClass('d-block');
                        $('#global-loader').addClass('d-none');
                        $('#global-loader').css('opacity', 1);
                        document.getElementById('photoError').innerHTML = xhr.responseJSON.errors.photo
                    }
                }
            });

        }
    </script>


    {{-- date picker --}}

    <script>
        $('#experienceStart').persianDatepicker({
            altField: '#experienceStart',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
        $('#experienceEnd').persianDatepicker({
            altField: '#experienceEnd',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,
        });
    </script>

@endsection

