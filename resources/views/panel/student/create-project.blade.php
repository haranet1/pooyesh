@extends('panel.student.master')
@section('title','تعریف پروژه')
@section('main')
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> تعریف پروژه</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header text-center justify-content-center">
                    <h4 class="card-title text-primary">تعریف پروژه جدید</h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('std.projects.store')}}" method="post">
                        @csrf
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">عنوان</label>
                            <div class="col-md-9">
                                <input name="title" value="{{old('title')}}" type="text" class="form-control" >
                                @error('title')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label class="col-md-3 form-label" for="example-email">دسته بندی</label>
                            <div class="col-md-9">
                                <select class="form-control form-select" name="category" id="">
                                    @foreach($cats as $cat)
                                        <option value="{{$cat->id}}">{{$cat->title}}</option>
                                    @endforeach
                                </select>
                                @error('category')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">مهارت های مورد نیاز</label>
                            <div class="col-md-9">
                                <input name="skills" type="text" class="form-control" >
                                @error('skills')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">بودجه (تومان)</label>
                            <div class="col-md-9">
                                <input name="budget" value="{{old('budget')}}" type="text" class="form-control" >
                                @error('budget')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class=" row mb-4">
                            <label class="col-md-3 form-label">توضیحات</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="desc" id="" cols="30" rows="10">{{old('desc')}}</textarea>
                                @error('desc')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class=" row mb-0 justify-content-center ">
                            <div class="col justify-content-center text-center ">
                                <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
