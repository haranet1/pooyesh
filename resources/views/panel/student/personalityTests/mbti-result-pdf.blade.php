<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <title></title>
    <link id="style" href="{{public_path('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
    <style>
        .scroll-box {
        max-height: 300px; /* Set the maximum height of the scrollable box */
        overflow: auto;   /* Enable scrolling when content overflows */
        }

        .item-list {
        list-style: none;
        padding: 0;
        margin: 0;
        }

        .item-list li {
        padding: 5px 0;
        border-bottom: 1px solid #eee;
        }
    </style>
   
</head>
<body style="font-family: fa" size="A4"  >
<div class="container ">
    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">
                <img src="{{public_path('assets/images/mbti-images/'.$personality_type->photo_path)}}" alt="image">

 
                <div class="row mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6">
                        <canvas id="chart1"></canvas>
                    </div>

                    <div class="col-12 col-md-6">
                        <h2 class="mt-3 c-h-size">شخصیت {{$mbtiResult->personality_type}} ({{$personality_type->fa_title}})</h3>
                        <p align="justify" class="c-font-size">{{$personality_type->analyse}}</p>
                    </div>
                </div>

                <div class="row mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6">
                        <h2 class="c-h-size">شخصیت {{$mbtiResult->personality_type}} در محل کار:</h2>
                        <p align="justify" class="c-font-size">{{$personality_type->work_analyse}}</p>
                    </div>

                    <div class="col-12 col-md-6">
                        <canvas id="chart2"></canvas>
                    </div>
                </div>

                <div class="row  mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                        <img class="c-img-margin"  style="max-width: 80%" src="{{public_path('assets/images/mbti-work-image.svg')}}" alt="">
                    </div>

                    <div class="col-12 col-md-6">
                        <h2 class="c-h-size">شخصیت {{$mbtiResult->personality_type}} در روابط اجتماعی:</h2>
                        <p align="justify" class="c-font-size">{{$personality_type->relations_analyse}}</p>
                    </div>
                </div>

                <div class="row mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;"> 
                    <div class="col-12 col-md-6">
                        <h2 class="c-h-size">توصیه هایی برای شما:</h2>
                        <div class="scroll-box  custom-size">
                            <ul class="item-list">
                                @foreach ($personality_type->other as $item)
                                    <li class="c-font-size">{{$item}}</li>
                                @endforeach
                            </ul>
                        </div>
                        <ul>
                            
                        </ul>
                    </div>
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                        {{-- <img class="c-img-margin" style="max-width: 60%" src="{{public_path('assets/images/mbti-other-image.svg')}}" alt=""> --}}
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
<script src="{{public_path('assets/panel/js/jquery.min.js')}}"></script>

<script src="{{public_path('assets/panel/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{public_path('assets/panel/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/chart.js"></script> --}}
<script src="{{public_path('assets/panel/js/chart2.js')}}"></script>
<script>
    var chart1 = document.getElementById('chart1');
    var mychart = new Chart(chart1,{
        type: 'bar',
        data: {
            labels:['برون گرا','درون گرا','شهودی','حسی','منطقی','احساسی','قضاوت گر','ادراکی'],
            datasets:[{
                label: '',
                data: [{{percent(15,$mbtiResult->E)}},{{percent(15,$mbtiResult->I)}},{{percent(15,$mbtiResult->N)}},{{percent(15,$mbtiResult->S)}},{{percent(15,$mbtiResult->T)}},{{percent(15,$mbtiResult->F)}},{{percent(15,$mbtiResult->J)}},{{percent(15,$mbtiResult->P)}}],
                backgroundColor: [
                    'rgba(227, 52, 47, 0.5)',
                    'rgba(246, 153, 63, 0.5)',
                    'rgba(255, 237, 74, 0.5)',
                    'rgba(56, 193, 114, 0.5)',
                    'rgba(77, 192, 181, 0.5)',
                    'rgba(52, 144, 220, 0.5)',
                    'rgba(149, 97, 2267, 0.5)',
                    'rgba(246, 109, 155, 0.5)'
                ],
                borderColor:['rgb(227, 52, 47)',
                            'rgb(246, 153, 63)',
                            'rgb(255, 237, 74)',
                            'rgb(56, 193, 114)',
                            'rgb(77, 192, 181)',
                            'rgb(52, 144, 220)',
                            'rgb(149, 97, 2267)',
                            'rgb(246, 109, 155)'],
                borderWidth: 1,

            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: 1,
            legend: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        display: false ,
                        drawBorder: false
                    },
                    ticks: {
                        max: 100,
                        min: 0,
                        stepSize: 10,
                        display: false
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false ,// Hide horizontal grid lines
                        drawBorder: false
                    }
                }]
            }
        }
    })
</script>

<script>
    var chart2 = document.getElementById('chart2').getContext('2d');
    var myChart = new Chart(chart2, {
        type: 'doughnut',
        data: {
            labels: [
                'برون گرا',
                'درون گرا',
                'شهودی',
                'حسی',
                'منطقی',
                'احساسی',
                'قضاوت گر',
                'ادراکی'
            ],
            datasets: [{
                data: [{{percent(15,$mbtiResult->E)}},{{percent(15,$mbtiResult->I)}},{{percent(15,$mbtiResult->N)}},{{percent(15,$mbtiResult->S)}},{{percent(15,$mbtiResult->T)}},{{percent(15,$mbtiResult->F)}},{{percent(15,$mbtiResult->J)}},{{percent(15,$mbtiResult->P)}}],
                backgroundColor: [
                    'rgba(227, 52, 47, 0.8)',
                    'rgba(246, 153, 63, 0.8)',
                    'rgba(255, 237, 74, 0.8)',
                    'rgba(56, 193, 114, 0.8)',
                    'rgba(77, 192, 181, 0.8)',
                    'rgba(52, 144, 220, 0.8)',
                    'rgba(149, 97, 2267, 0.8)',
                    'rgba(246, 109, 155, 0.8)'
                ],
            }]
        },
        options:{
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: 1
        }
    });
</script>
    
</body>
</html>

