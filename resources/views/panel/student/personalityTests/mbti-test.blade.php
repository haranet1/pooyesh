@extends('panel.student.master')


@section('title','تست MBTI')

@section('main')
<style>
    @media (min-width:1200px){
        .f-16{
            font-size: 1.2rem;
        }
        .f-12{
            font-size: 0.95rem;
        }
    }
    @media (max-width:360px){
        .nowrap {
            font-size: 16px;
            white-space: nowrap;
        }
        .f-16{
            font-size: 16px;
            margin-bottom: 0.2rem;
        }
        .f-12{
            font-size: 12px;
            margin-top: 0.2rem;
        }
    }
</style>

        <div id="progress" class="row align-items-center pb-3 " style="z-index:100;box-sizing: border-box; position: fixed; width:80%;background-color: #cfe2fa; left:45px;">
            <div class="col-2 col-md-1 mt-2 d-flex justify-content-end">
                <span class="mx-2" id="prog1">0</span>
                <span style="white-space: nowrap !important">از 60</span>
            </div>
            <div class="col-8 col-md-10">
                <div class="progress progress-striped mt-3" dir="rtl" style="height:25px;">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0"
                         aria-valuemin="0" aria-valuemax="60" style="">
                        <span class="prog" id="prog2">0</span>
                    </div>
                </div>
            </div>
            <div class="col-2 col-md-1 mt-2">
                <span>پایان آزمون</span>
            </div>
        </div>

    <div class="page-header mt-0">
        <div>

        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="card py-5 px-md-5 px-sm-0">
                <div class="alert alert-info alert-dismissible fade-show">
                    <div class="d-flex align-items-center">
                      <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
                      <div class="flex-grow-1">
                        <p class="f-16" >
                          <strong>لطفا گزینه ای که بیشترین انطباق با شخصیت شما دارد را انتخاب کنید</strong>
                        </p>
                        <p class="f-12"> (توجه داشته باشید که همه سوالات را پاسخ دهید) </p>
                      </div>
                    </div>
                </div>

                <form id="mbtiForm" action="{{route('mbti.test.result')}}" method="GET" class="mt-3">
                    @csrf
                    @for ($i = 1; $i<=60 ; $i++)
                        <div id="{{$i}}" class="row m-3 border" style="border-radius:15px; margin-bottom:130px !important;">
                            <h4 class="nowrap">{{$i}} : کدام گزینه شما را بهتر توصیف میکند ؟</h4>
                            <div class="form-check my-3 my-md-0">
                                <input class="form-check-input" type="radio" name="q{{$i}}" id="q{{($i*2)-1}}" value="{{$questions[($i*2)-1]['category']}}"
                                @if (old('q'.$i) && old('q'.$i) == $questions[($i*2)-1]['category'])checked @endif
                                onclick="updateProgress({{$i}});">
                                <label for="q{{($i*2)-1}}" class="form-check-label text-primary">
                                    {{$questions[($i*2)-1]['title']}}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="q{{$i}}" id="q{{$i*2}}" value="{{$questions[$i*2]['category']}}"
                                @if (old('q'.$i) && old('q'.$i) == $questions[$i*2]['category'])checked @endif
                                onclick="updateProgress({{$i}});">
                                <label for="q{{$i*2}}" class="form-check-label text-primary">
                                    {{$questions[$i*2]['title']}}
                                </label>
                            </div>
                            @error('q'.$i)
                                    <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    @endfor
                    @auth
                        <div class="row justify-content-center mt-5">
                            <button class="btn btn-success w-25" type="submit">پایان آزمون</button>
                        </div>
                    @else
                        <div class="row justify-content-center mt-5">
                            <a onclick="getSession()" class="btn w-25 btn-success"data-bs-target="#modal"data-bs-toggle="modal"href="javascript:void(0)">پایان آزمون</a>
                        </div>
                        <div class="modal fade"id="modal">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content modal-content-demo">
                                    <div class="modal-header">
                                        <h6 class="modal-title"></h6>
                                        {{-- <button aria-label="Close" class="btn-close"data-bs-dismiss="modal">
                                                <span aria-hidden="true ">&times;</span>
                                        </button> --}}
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-body">

                                            <div class="row mt-5 d-flex justify-content-center text-center">
                                                <div class="alert alert-info alert-dismissible fade-show">
                                                    <div class="d-flex align-items-center">
                                                      <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
                                                      <div class="flex-grow-1">
                                                        <p class="f-16" >
                                                          <strong>برای مشاهده نتیجه لطفا اطلاعات خواسته شده را تکمیل کنید</strong>
                                                        </p>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-6" id="formRow">
                                                    <div class="row d-flex justify-content-center mt-5">
                                                        <div class="col-12">
                                                            <input id="name" type="text" class="form-control form-input" placeholder="نام :">
                                                            <span id="nameError" class="text-danger"></span>
                                                        </div>
                                                    </div>
                                                    <div class="row d-flex justify-content-center mt-5">
                                                        <div class="col-12">
                                                            <input id="family" type="text" class="form-control form-input" placeholder="نام خانوادگی :">
                                                            <span id="familyError" class="text-danger"></span>
                                                        </div>
                                                    </div>
                                                    <div class="row d-flex justify-content-center mt-5">
                                                        <div class="col-12">
                                                            <input id="mobile" type="text" inputmode="numeric" class="form-control form-input" placeholder="موبایل :">
                                                            <span id="mobileError" class="text-danger"></span>
                                                        </div>
                                                    </div>
                                                    <div id="n_codeRow" class="row justify-content-center mt-5">
                                                        <div class="col-12">
                                                            <input id="n_code" type="text" inputmode="numeric" class="form-control form-input" placeholder="کد ملی :">
                                                            <span id="n_codeError" class="text-danger"></span>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-center mt-5">
                                                        <div class="form-group">
                                                            <label class="custom-switch form-switch mb-0">
                                                                <input type="checkbox" checked name="isStudent" id="isStudent" class="custom-switch-input">
                                                                <span class="custom-switch-indicator custom-switch-indicator-md"></span>
                                                                <span class="custom-switch-description">دانشجو دانشگاه آزاد هستم</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row  mt-5">
                                                        <div class="col-12 d-flex justify-content-center">
                                                            <a href="javascript:void(0)" id="btnSubmit" type="submit" class="btn btn-success text-white">ثبت اطلاعات</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="verifyRow" class="row justify-content-center d-none">
                                                    <div class="col-12 col-lg-6">
                                                        <div class="row d-flex justify-content-center mt-5">
                                                            <div class="alert alert-warning alert-dismissible fade-show">
                                                                <div class="d-flex align-items-center">
                                                                <img class="me-4" src="{{asset('assets/images/warning.svg')}}" style="max-width: 33px" alt="info-icon">
                                                                <div class="flex-grow-1">
                                                                    <p style="color: #fd7e14">پیامک تایید تلفن همراه برای شما ارسال شد، لطفا کد ارسالی را در زیر وارد نمایید.</p>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row d-flex justify-content-center mt-5">
                                                            <div class="col-12">
                                                                <input id="verifyMobile" type="text" inputmode="numeric" class="form-control form-input" placeholder="کد تایید :">
                                                                <span id="verifyMobileError" class="text-danger"></span>
                                                            </div>
                                                        </div>
                                                        <div class="row  mt-5">
                                                            <div class="col-12 d-flex justify-content-center">
                                                                <a href="javascript:void(0)" id="btnVerify" class="btn btn-success text-white mx-2">تایید کد</a>
                                                                <a href="javascript:void(0)" id="btnEdit" class="btn btn-primary text-white mx-2">ویرایش اطلاعات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        {{-- <button class="btn btn-danger" data-bs-dismiss="modal"> بستن </button> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endauth
                </form>
            </div>
        </div>
    </div>
    @endsection

    @section('script')
    <script>
        const Csrf = "{{csrf_token()}}";
        let customRegister = "{{route('auth.customRegister')}}";
        let customVerifyUser = "{{route('auth.customLogin')}}";
        let loginUrl = "{{ route('login') }}"
    </script>

    <script>
        var radios = $('input[type="radio"][class="form-check-input"]:checked').length;
        updateProgressNumbers();
        updateProgress(radios);

        $('input[type="radio"]').on('click', function(){
            updateProgressNumbers();
            myScroll($(this));
        });

        function updateProgressNumbers() {
            var radios = $('input[type="radio"][class="form-check-input"]:checked').length;
            var prog1 = $('#prog1');
            var prog2 = $('#prog2');

            var currentProg1 = parseInt(prog1.text());
            var currentProg2 = parseInt(prog2.text());

            prog1.text(radios);
            prog2.text(radios);
        }

        function myScroll(element){
            var targetDiv = element.parent().parent();
            var id = targetDiv.attr('id');
            id = parseInt(id);

            $('html, body').animate({
                scrollTop: $('#' + id).offset().top
            }, 500);

            targetDiv.removeClass('border-primary border-4');
            id++;
            $('#' + id).addClass('border-primary border-5');
        }

        function updateProgress(step) {
            var progressBar =  $('.progress-bar');
            var width = (step / 60) * 100; // Calculate the percentage width

            progressBar.css('width', width + "%");
        }
    </script>
    <script>
        var customWidth = $('.page-header').width();
        $('#progress').width(customWidth);
    </script>

    <script>
        let verifyelement = document.getElementById('verifyRow');
        let n_code_element = document.getElementById('n_codeRow');
        let formelement = document.getElementById('formRow');
        let userId = 0;
        let timeOut = false;
        let is_student = document.getElementById('isStudent');

        is_student.addEventListener('click', function(){
            if(is_student.checked){
            n_code_element.classList.remove('d-none');
            }else{
                n_code_element.classList.add('d-none');
            }
        });

        document.getElementById('btnSubmit').addEventListener('click', function(){
            submitInfo();
        });

        document.getElementById('btnEdit').addEventListener('click', function() {
            verifyelement.classList.add('d-none');
            formelement.classList.remove('d-none');
        });
        document.getElementById('btnVerify').addEventListener('click', function() {
            verifyUser();
        });
    </script>
    <script>
        function getSession(){
            var radios = $('input[type="radio"][class="form-check-input"]:checked').attr();
{{--            @php--}}
{{--            session(['mbti'=> ])--}}
{{--            @endphp--}}
        }
    </script>
    <script src="{{asset('assets/panel/js/mbti-test.js')}}"></script>

    {{-- <script>
        function register(){
            var name = document.getElementById('name').value;
            var family = document.getElementById('family').value;
            var mobile = document.getElementById('mobile').value;
            var nameError = document.getElementById('nameError');
            var familyError = document.getElementById('familyError');
            var mobileError = document.getElementById('mobileError');
            nameError.innerHTML = '';
            familyError.innerHTML = '';
            mobileError.innerHTML = '';
            if(name == ''){
                nameError.innerHTML = 'لطفا نام خود را وارد کنید';
                return false;
            }
            if(family == ''){
                familyError.innerHTML = 'نام خانوادگی خود را وارد کنید';
                return false;

            }
            if(mobile == ''){
                mobileError.innerHTML = 'لطفا شماره موبایل خود را وارد کنید';
                return false;

            }
            if(isNaN(mobile)){
                mobileError.innerHTML = 'لطفا شماره موبایل خود را به صورت صحیح وارد کنید';
                return false;

            }

            document.getElementById('btnSubmit').addEventListener('click', function(){
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : "{{csrf_token()}}"
                    },
                    // _token : "{{csrf_token()}}",
                    type: "post",
                    url: "{{route('auth.customRegister')}}",
                    data: {
                        name : name,
                        family : family,
                        mobile : mobile,
                        password : mobile,
                    },
                    dataType: "json",
                    success: function (response) {
                        let form = document.getElementById('mbtiForm');
                        form.submit();
                    },
                    error: function (xhr, status, error) {
                        if(xhr.status === 422){
                            document.getElementById('mobileError').innerHTML = xhr.responseJSON.errors.mobile
                        }
                    }

                });
            });

        }
    </script> --}}
@endsection
