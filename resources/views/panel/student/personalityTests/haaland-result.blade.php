@extends('panel.student.master')

@section('title','نتیجه تست هالند')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')
    <style>
        .scroll-box {
        max-height: 300px; /* Set the maximum height of the scrollable box */
        overflow: auto;   /* Enable scrolling when content overflows */
        }

        .item-list {
        list-style: none;
        padding: 0;
        margin: 0;
        }

        .item-list li {
        padding: 5px 0;
        border-bottom: 1px solid #eee;
        }
        hr.dashed {
            border: none;
            border-top: 3px dashed #000; /* You can change the color here */
            height: 0;
            margin: 40px 0; /* Adjust margin as needed */
            /* margin-top: 40px; */
        }
        .custom-padding{
            padding-right: 2rem;
        }
        .c-m-row{
            width: 100%;
            justify-content: center;
        }
        .c-t-row{
            justify-content: center;

        }
        @media screen and (max-width: 500px) {
            #bg-info{
                background-image: url("{{asset('assets/images/haaland-images/haaland-header-400.svg')}}") !important;
                height: 527px !important;
            }
            .custom-padding{
                padding-right: 0.8rem;
            }
            #info-logo{
                width: 150px !important;
            }
            .c-icon{
                width: 20px !important;
            }
            .cc-icon{
                width: 15px !important;
            }
            .c-heading{
                font-size: 0.8rem !important;
            }
            .cc-heading{
                font-size: 1.2rem !important;
            }
            .c-span{
                font-size: 0.9rem !important;
            }
        }
    </style>



    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">
                <div class="card shadow-lg" id="bg-info" style="background-repeat: no-repeat; border:3px solid #fff8b0 !important;border-radius: 15px; height: 320px;background-image: url('{{asset('assets/images/haaland-images/haaland-header.svg')}}')">
                    <div class="row mx-1 my-3 align-items-center">
                        <div class="col-12 col-md-3 px-0 d-flex justify-content-center">
                            <img style="width: 70%;" src="{{asset('assets/panel/images/brand/jobist/text-logo.svg')}}" alt="">
                        </div>
                        <div class="col-3 col-md-1 my-2 px-0 d-flex justify-content-center">
                            <img class="c-icon" style="width: 30px;" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="">
                        </div>
                        <div class="col-9 col-md-2 my-2 px-0">
                            <h4 class="m-0 c-heading">{{Auth::user()->name}} {{Auth::user()->family}}</h4>
                        </div>
                        <div class="col-3 col-md-1 my-2 px-0 d-flex justify-content-center">
                            <img class="c-icon cc-icon" style="width: 20px;" src="{{asset('assets/panel/images/icon/SVG/mobile.svg')}}" alt="">
                        </div>
                        <div class="col-9 col-md-2 my-2 px-0">
                            <h4 class="m-0 c-heading">{{Auth::user()->mobile}}</h4>
                        </div>
                        <div class="col-3 col-md-1 my-2 px-0 d-flex justify-content-center">
                            <img class="c-icon" style="width: 40px;" src="{{asset('assets/panel/images/icon/SVG/n_code.svg')}}" alt="">
                        </div>
                        <div class="col-9 col-md-2 my-2 px-0">
                            @if (Auth::user()->n_code)
                                <h4 class="m-0 c-heading">{{Auth::user()->n_code}}</h4>
                            @else
                                <h4 class="m-0 c-heading">کد ملی ثبت نشده</h4>
                            @endif
                        </div>
                    </div>
                    <div class="row mx-1 my-3 justify-content-center align-items-center" style="margin-top: 0 !important">
                        <h2 class="text-center mb-2 cc-heading">کد رغبت شما:</h2>
                        <div class="col-auto">
                            <div class="d-flex gap-2">
                                <div class="bg-success rounded-3 p-3">
                                    <span class="text-white c-span" style="font-size: 1.1rem">{{$personality[2]['title']}}</span>
                                </div>
                                <div class="bg-danger rounded-3 p-3">
                                    <span class="text-white c-span" style="font-size: 1.1rem">{{$personality[1]['title']}}</span>
                                </div>
                                <div class="bg-primary rounded-3 p-3">
                                    <span class="text-white c-span" style="font-size: 1.1rem">{{$personality[0]['title']}}</span>
                                </div>
                            </div>
                        </div>
                        <h2 class="text-center mb-2 mt-2 cc-heading">بالاترین رغبت شما:</h2>
                        <div class="col-auto">
                            <div class="d-flex gap-2">
                                <div class="bg-success rounded-3 p-3">
                                    <span class="text-white c-span" style="font-size: 1.1rem">{{$personality[2]['fa_title']}}</span>
                                </div>
                                <div class="bg-danger rounded-3 p-3">
                                    <span class="text-white c-span" style="font-size: 1.1rem">{{$personality[1]['fa_title']}}</span>
                                </div>
                                <div class="bg-primary rounded-3 p-3">
                                    <span class="text-white c-span" style="font-size: 1.1rem">{{$personality[0]['fa_title']}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6">
                                <div id="chart1"></div>
                            </div>
                            <div class="col-12 col-md-6">
                                <h3 class="font-weight-bold">نتیجه تست رغبت سنج هالند</h3>
                                <p align="justify" class="c-font-size">
                                    کد تست هالند شما یعنی
                                    {{$personality[0]['fa_title']}} ({{$personality[0]['title']}}) ،
                                    {{$personality[1]['fa_title']}} ({{$personality[1]['title']}}) ،
                                    {{$personality[2]['fa_title']}} ({{$personality[2]['title']}}) ،
                                    تعیین می کند که چگونه با دنیای اطراف خود ارتباط برقرار می‌کنید. در ادامه با توجه به این کد و امتیاز در 6 کد اصلی هالند، تحلیلی دقیق‌تر انجام خواهیم داد تا شما را در یافتن مسیر مناسب هدایت کنیم.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6 custom-padding">
                                <h3 class="font-weight-bold">{{$personality[0]['fa_title']}}</h3>
                                <p align="justify" class="c-font-size">{{$personality[0]['description']}}</p>
                            </div>
                            <div class="col-12 col-md-6 d-flex justify-content-center">
                                <img class="c-img-margin" style="max-width: 50%" src="{{asset('assets/images/haaland-images/'.get_haaland_photo($personality[0]))}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6 d-flex justify-content-center">
                                <img class="c-img-margin" style="max-width: 50%" src="{{asset('assets/images/haaland-images/'.get_haaland_photo($personality[1]))}}" alt="">
                            </div>
                            <div class="col-12 col-md-6">
                                <h3 class="font-weight-bold">{{$personality[1]['fa_title']}}</h3>
                                <p align="justify" class="c-font-size">{{$personality[1]['description']}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6 custom-padding">
                                <h3 class="font-weight-bold">{{$personality[2]['fa_title']}}</h3>
                                <p align="justify" class="c-font-size">{{$personality[2]['description']}}</p>
                            </div>
                            <div class="col-12 col-md-6 d-flex justify-content-center">
                                <img class="c-img-margin" style="max-width: 50%" src="{{asset('assets/images/haaland-images/'.get_haaland_photo($personality[2]))}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-5 d-print-none">
                    <a id="print" class="btn btn-success text-white">چاپ نتیجه تست</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script>
    document.getElementById("print").onclick = function jsFunc() {
        window.print();
    }
    </script>

    <script>
        var options = {
          series: [{
          data: [{{percent(36,$haalandResult['R'])}},{{percent(36,$haalandResult['I'])}},{{percent(36,$haalandResult['A'])}},{{percent(36,$haalandResult['S'])}},{{percent(36,$haalandResult['E'])}},{{percent(36,$haalandResult['C'])}}],
        }],
        chart: {
            offsetX: -20,
            offsetY: 20,
            height: 350,
            type: 'bar',
            events: {
                click: function(chart, w, e) {
                // console.log(chart, w, e)
                }
            },
        },
        // colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true,
            // horizontal: true,
            dataLabels:{
                position: 'center',
            }
          }
        },
        dataLabels: {
          enabled: true
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [
            ['واقع گرا'],
            ['جستجوگر'],
            ['هنری'],
            ['اجتماعی'],
            ['سازنده'],
            ['قاعده مند'],
          ],
          labels: {
            style: {
            //   colors: colors,
              fontSize: '12px'
            }
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
                show: false,
            }
          }
        }],
        };

        var chart = new ApexCharts(document.querySelector("#chart1"), options);
        chart.render();


    </script>


@endsection

