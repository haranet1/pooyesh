@extends('panel.student.master')


@section('title','نتیجه تست NEO')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')
    <style>
        .scroll-box {
        max-height: 300px; /* Set the maximum height of the scrollable box */
        overflow: auto;   /* Enable scrolling when content overflows */
        }

        .item-list {
        list-style: none;
        padding: 0;
        margin: 0;
        }

        .item-list li {
        padding: 5px 0;
        border-bottom: 1px solid #eee;
        }
        hr.dashed {
            border: none;
            border-top: 3px dashed #000; /* You can change the color here */
            height: 0;
            margin: 40px 0; /* Adjust margin as needed */
            /* margin-top: 40px; */
        }
        .custom-padding{
                padding-right: 2rem;
            }
        @media screen and (width: 400px) {
            #bg-info{background-image: url("{{asset('assets/images/neo-images/neo-header1-400.svg')}}") !important} 
            .custom-padding{
                padding-right: 0.8rem;
            }
        }
    </style>

    {{-- <div class="row row-cards  mt-5 ">
        <div >
            <div class="main-body">
                <div class="row gutters-sm">
                    <div class="col-md-3 mb-3 pb ">
                    <div class="card pb-5">
                        <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150">
                            <div class="mt-3">
                            <h4>John Doe</h4>
                            <p class="text-secondary mb-1">Full Stack Developer</p>
                            <p class="text-muted font-size-sm">Bay Area, San Francisco, CA</p>
                            <button class="btn btn-primary">Follow</button>
                            <button class="btn btn-outline-primary">Message</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="card mt-1">
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe mr-2 icon-inline"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>Website</h6>
                            <span class="text-secondary">https://bootdey.com</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-github mr-2 icon-inline"><path d="M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22"></path></svg>Github</h6>
                            <span class="text-secondary">bootdey</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter mr-2 icon-inline text-info"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg>Twitter</h6>
                            <span class="text-secondary">@bootdey</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram mr-2 icon-inline text-danger"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg>Instagram</h6>
                            <span class="text-secondary">bootdey</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook mr-2 icon-inline text-primary"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg>Facebook</h6>
                            <span class="text-secondary">bootdey</span>
                        </li>
                        </ul>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="card mb-1">
                        <div class="card-body">
                        <div class="row">
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eum quidem rerum non perspiciatis qui est error quae officiis, possimus nisi, fugiat similique eligendi tenetur pariatur odio! Corporis natus esse quam. Officia, illum aliquam voluptatum illo laborum hic nemo omnis magnam debitis? Expedita cumque nemo repellendus consequatur veritatis ducimus, alias aperiam?</p>
                        </div>

                        
                        </div>
                    </div>
                    
        
                    <div class="row gutters-sm mt-4">
                        <div class="col-sm-6 mb-3">
                        <div class="card h-100">
                            <div class="card-body">
                            <h6 class="d-flex align-items-center mb-3"><i class="material-icons text-info mr-2">assignment</i>Project Status</h6>
                            <small>Web Design</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Website Markup</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 72%" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>One Page</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 89%" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Mobile Template</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Backend API</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 66%" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-6 mb-3">
                        <div class="card h-100">
                            <div class="card-body">
                            <h6 class="d-flex align-items-center mb-3"><i class="material-icons text-info mr-2">assignment</i>Project Status</h6>
                            <small>Web Design</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Website Markup</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 72%" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>One Page</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 89%" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Mobile Template</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Backend API</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 66%" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
        
            </div>
        </div>
    </div> --}}

    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">
                <div class="row mx-1 mb-4 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                        <img style="width: 30px;" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="">
                    </div>
                    <div class="col-6 col-md-3 my-2 px-0">
                        <h4 class="m-0">{{Auth::user()->name}} {{Auth::user()->family}}</h4>
                    </div>
                    <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                        <img style="width: 20px;" src="{{asset('assets/panel/images/icon/SVG/mobile.svg')}}" alt="">
                    </div>
                    <div class="col-6 col-md-3 my-2 px-0">
                        <h4 class="m-0">{{Auth::user()->mobile}}</h4>
                    </div>
                    <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                        <img style="width: 40px;" src="{{asset('assets/panel/images/icon/SVG/n_code.svg')}}" alt="">
                    </div>
                    <div class="col-6 col-md-3 my-2 px-0">
                        @if (Auth::user()->n_code)
                            <h4 class="m-0">{{Auth::user()->n_code}}</h4>
                        @else
                            <h4 class="m-0">کد ملی ثبت نشده</h4>
                        @endif
                    </div>
                </div>
                <img class="mb-4" src="{{asset('assets/images/ghq-images/'.set_ghq_header($ghqResult['sum_of_all']))}}" alt="">
                {{-- <div class="card shadow-lg" id="bg-info" style="border:3px solid #c9e9f6 !important;border-radius: 15px; height: 315px;background-image: url('{{asset('assets/images/ghq-images/ghq-header1.svg')}}')">
                    <div class="row mx-1 my-3 align-items-center">
                        <div class="col-12 col-md-3 px-0 d-flex justify-content-center">
                            <img style="width: 70%;" src="{{asset('assets/panel/images/brand/logo.svg')}}" alt="">
                        </div>
                        <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                            <img style="width: 30px;" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="">
                        </div>
                        <div class="col-6 col-md-2 my-2 px-0">
                            <h4 class="m-0">{{Auth::user()->name}} {{Auth::user()->family}}</h4>
                        </div>
                        <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                            <img style="width: 20px;" src="{{asset('assets/panel/images/icon/SVG/mobile.svg')}}" alt="">
                        </div>
                        <div class="col-6 col-md-2 my-2 px-0">
                            <h4 class="m-0">{{Auth::user()->mobile}}</h4>
                        </div>
                        <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                            <img style="width: 40px;" src="{{asset('assets/panel/images/icon/SVG/n_code.svg')}}" alt="">
                        </div>
                        <div class="col-6 col-md-2 my-2 px-0">
                            @if (Auth::user()->n_code)
                                <h4 class="m-0">{{Auth::user()->n_code}}</h4>
                            @else
                                <h4 class="m-0">کد ملی ثبت نشده</h4>
                            @endif
                        </div>
                    </div>
                    <div class="row mx-1 my-3 align-items-center">
                        <h1 class="text-center">تست شخصیت NEO</h1>
                    </div>
                </div> --}}
                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6">
                                <div id="chart1"></div>
                            </div>
                            <div class="col-12 col-md-6">
                                <h3 class="font-weight-bold">جمع کل نمرات مقیاس های سلامت عمومی</h3>
                                <p align="justify" class="c-font-size">نمره کل پرسشنامه GHQ عددی بین 0 تا 84 میتواند باشد. بر اساس نظر “گلدبرگ و هیلر” در صورتی که نمره فرد بین 0 تا 21 باشد او در طیف “کمترین حد و یا هیچ” از سنجش سلامت عمومی قرار دارد، بدین معنی که وی در شرایط سلامت عمومی قرار دارد. به همین ترتیب نمرات بین 22 تا 42 در طیف دارای علائم خفیف، 43 تا 63 علائم متوسط و نهایتا 64 تا 84 علائم شدید و خطرناک قرار میگیرند، بنابراین پیشنهاد میشود در صورتی که نمره شما بیشتر از 40 میباشد، به فکر بررسی و بهبود وضعیت سلامتی خود باشید.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6 custom-padding">
                                <h3 class="font-weight-bold">علائم جسمانی</h3>
                                <p align="justify" class="c-font-size">
                                    اختلال علائم جسمی زمانی تشخیص داده می شود که فرد تمرکز قابل توجهی بر علائم فیزیکی مانند درد، ضعف یا تنگی نفس داشته باشد تا حدی که منجر به پریشانی اساسی و/یا مشکلات عملکردی شود. فرد دارای افکار، احساسات و رفتارهای بیش از حد مربوط به علائم فیزیکی است.
                                </p>
                            </div>
                            <div class="col-12 col-md-6">
                                <div id="chart2"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6">
                                <div id="chart3"></div>
                            </div>
                            <div class="col-12 col-md-6">
                                <h3 class="font-weight-bold">علائم اضطرابی و اختلال خواب</h3>
                                <p align="justify" class="c-font-size">
                                    اضطراب اغلب با مشکلات خواب مرتبط است. نگرانی و ترس بیش از حد باعث می شود که به خواب رفتن و خواب ماندن در طول شب سخت تر شود. محرومیت از خواب می تواند اضطراب را بدتر کند و چرخه منفی شامل بی خوابی و اختلالات اضطرابی را تحریک کند.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6 custom-padding">
                                <h3 class="font-weight-bold">علائم کارکرد اجتماعی</h3>
                                <p align="justify" class="c-font-size">
                                    اختلال اضطراب اجتماعی یک ترس شدید و مداوم از زیر نظر گرفتن و قضاوت شدن توسط دیگران است. این ترس می تواند بر کار، مدرسه و سایر فعالیت های روزانه تأثیر بگذارد. حتی می تواند دوست یابی و حفظ آن را سخت کند.
                                </p>
                            </div>
                            <div class="col-12 col-md-6">
                                <div id="chart4"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6">
                                <div id="chart5"></div>
                            </div>
                            <div class="col-12 col-md-6">
                                <h3 class="font-weight-bold">علائم افسردگی</h3>
                                <p align="justify" class="c-font-size">
                                    افسردگی (اختلال افسردگی اساسی) یک بیماری شایع و جدی پزشکی است که بر احساس شما، طرز تفکر و نحوه عمل شما تأثیر منفی می گذارد. خوشبختانه قابل درمان نیز هست. افسردگی باعث احساس غم و اندوه و/یا از دست دادن علاقه به فعالیت هایی می شود که زمانی از آن لذت می بردید. این می تواند منجر به انواع مشکلات عاطفی و جسمی شود و می تواند توانایی شما را برای عملکرد در محل کار و خانه کاهش دهد.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-5 d-print-none">
                    <a id="print" class="btn btn-success text-white">چاپ نتیجه تست</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script>
    document.getElementById("print").onclick = function jsFunc() {
        window.print();
    }
    </script>

    <script>
        var options = {
          series: [{
          data: [{{percent(21,$ghqResult['physical'])}},{{percent(21,$ghqResult['anxiety'])}},{{percent(21,$ghqResult['social'])}},{{percent(21,$ghqResult['depression'])}}],
        }],
        chart: {
            offsetX: -20,
            offsetY: 20,
            height: 350,
            type: 'bar',
            events: {
                click: function(chart, w, e) {
                // console.log(chart, w, e)
                }
            },
        },
        // colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true,
            // horizontal: true,
            dataLabels:{
                position: 'center',
            }
          }
        },
        dataLabels: {
          enabled: true
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [
            ['علائم', 'جسمانی'],
            ['علائم', 'اضطرابی'],
            ['علائم', 'اجتماعی'],
            ['علائم', 'افسردگی'],
          ],
          labels: {
            style: {
            //   colors: colors,
              fontSize: '12px'
            }
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
                show: false,
            }
          }
        }],
        };

        var chart = new ApexCharts(document.querySelector("#chart1"), options);
        chart.render();

    
    </script>

    <script>
        
      
        var options = {
          series: [{{percent(21,$ghqResult['physical'])}},100 - {{percent(21,$ghqResult['physical'])}}],
          chart: {
          width: 380,
          type: 'pie',
          offsetX : -100,
        },
        labels: ['علائم جسمانی', 'سلامتی'],
        colors:['#753f94','#919191'],
        legend:{
            show:true,
            position: 'top',
            horizontalAlign: 'center', 
            fontSize: '18px',
            fontFamily: 'irans-serif',
            markers:{
                offsetX : 7,
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              offsetX : 0,
              width: 300
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart2"), options);
        chart.render();
      
    </script>

    <script>
        var options = {
          series: [{{percent(21,$ghqResult['anxiety'])}},100 - {{percent(21,$ghqResult['anxiety'])}}],
          chart: {
          width: 380,
          type: 'pie',
          offsetX : -100,
        },
        labels: ['علائم اضطراب', 'سلامتی'],
        colors:['#f2d527','#919191'],
        legend:{
            show:true,
            position: 'top',
            horizontalAlign: 'center', 
            fontSize: '18px',
            fontFamily: 'irans-serif',
            markers:{
                offsetX : 7,
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              offsetX : 0,
              width: 300
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart3"), options);
        chart.render();
    </script>

    <script>
        var options = {
          series: [{{percent(21,$ghqResult['social'])}},100 - {{percent(21,$ghqResult['social'])}}],
          chart: {
          width: 380,
          type: 'pie',
          offsetX : -100,
        },
        labels: ['علائم اجتماعی', 'سلامتی'],
        colors:['#4ccc36','#919191'],
        legend:{
            show:true,
            position: 'top',
            horizontalAlign: 'center', 
            fontSize: '18px',
            fontFamily: 'irans-serif',
            markers:{
                offsetX : 7,
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              offsetX : 0,
              width: 300
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart4"), options);
        chart.render();
    </script>

    <script>
        var options = {
          series: [{{percent(21,$ghqResult['depression'])}},100 - {{percent(21,$ghqResult['depression'])}}],
          chart: {
          width: 380,
          type: 'pie',
          offsetX : -100,
        },
        labels: ['علائم افسردگی', 'سلامتی'],
        colors:['#e52560','#919191'],
        legend:{
            show:true,
            position: 'top',
            horizontalAlign: 'center', 
            fontSize: '18px',
            fontFamily: 'irans-serif',
            markers:{
                offsetX : 7,
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              offsetX : 0,
              width: 300
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart5"), options);
        chart.render();
    </script>

@endsection

