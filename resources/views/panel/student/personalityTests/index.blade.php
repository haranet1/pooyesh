@extends('panel.student.master')

@section('meta')
    <meta name="description"  content=" با تست‌های شخصیتی در جابیست، ویژگی‌ها و نقاط قوت خود را شناسایی کنید. این تست‌ها به شما کمک می‌کنند تا در مسیر حرفه‌ای و شخصی خود پیشرفت کنید." />
    <meta property="og:title" content=" تست‌های شخصیتی در جابیست | ارزیابی و شناسایی ویژگی‌های شخصیتی"/>
    <meta property="og:url" content="https://jobist.ir/exams"/>
    <meta property="og:description" content=" با تست‌های شخصیتی در جابیست، ویژگی‌ها و نقاط قوت خود را شناسایی کنید. این تست‌ها به شما کمک می‌کنند تا در مسیر حرفه‌ای و شخصی خود پیشرفت کنید. " /> 
    <link rel="canonical" href="https://jobist.ir/exams"/>
@endsection


@section('title','ارزیابی و شناسایی ویژگی‌های شخصیتی')

@section('main')
    <div class="page-header">
        <h1 class="page-title">فهرست آزمون های شخصیت شناسی</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد</a></li>
                <li class="breadcrumb-item active" aria-current="page">فهرست آزمون ها</li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="card p-5">
                <div class="row">
                    {{-- mbti --}}
                    <div class="col-12 col-md-4" style="height: 400px">
                        <div class="card shadow-lg" style="border:1px gray !important;">
                            <img src="{{asset('assets/images/mbti.svg')}}" class="card-img-top" alt="MBTI">
                            <div class="card-body text-center">
                                <h3 class="text-warp" style="font-size: 17px">تست شخصیت شناسی MBTI</h3>
                                <a href="{{route('mbti.index')}}" class="btn btn-primary">مشاهده</a>
                            </div>
                        </div>
                    </div>
                    {{-- haaland --}}
                    <div class="col-12 col-md-4" style="height: 400px">
                        <div class="card shadow-lg" style=" border:1px gray !important;">
                            <img src="{{asset('assets/images/haaland.svg')}}" class="card-img-top" alt="Enneagram">
                            <div class="card-body text-center">
                                <h3 class="text-wrap" style="font-size: 17px;line-height:1.1rem;" >رغبت شناسی haaland (هالند)</h3>
                                <a href="{{route('haaland.index')}}" class="btn btn-primary">مشاهده</a>
                            </div>
                        </div>
                    </div>
                    {{-- enneagram --}}
                    <div class="col-12 col-md-4" style="height: 400px">
                        <div class="card shadow-lg" style=" border:1px gray !important;">
                            <img src="{{asset('assets/images/enneagram.svg')}}" class="card-img-top" alt="Enneagram">
                            <div class="card-body text-center">
                                <h3 class="text-wrap" style="font-size: 17px;line-height:1.1rem;" >تست شخصیت شناسی Enneagram (انیاگرام)</h3>
                                <a href="{{route('enneagram.index')}}" class="btn btn-primary">مشاهده</a>
                            </div>
                        </div>
                    </div>
                    {{-- neo --}}
                    <div class="col-12 col-md-4" style="height: 400px">
                        <div class="card shadow-lg" style=" border:1px gray !important;">
                            <img src="{{asset('assets/images/neo.svg')}}" class="card-img-top" alt="NEO">
                            <div class="card-body text-center">
                                <h3 class="text-wrap" style="font-size: 17px;line-height:1.1rem;" >تست شخصیت شناسی NEO</h3>
                                <a href="{{route('neo.index')}}" class="btn btn-primary">مشاهده</a>
                            </div>
                        </div>
                    </div>
                    {{-- ghq --}}
                    <div class="col-12 col-md-4" style="height: 400px">
                        <div class="card shadow-lg" style=" border:1px gray !important;">
                            <img src="{{asset('assets/images/ghq.svg')}}" class="card-img-top" alt="GHQ">
                            <div class="card-body text-center">
                                <h3 class="text-wrap" style="font-size: 17px;line-height:1.1rem;" >تست سلامت عمومی GHQ</h3>
                                <a href="{{route('ghq.index')}}" class="btn btn-primary">مشاهده</a>
                            </div>
                        </div>
                    </div>
                    {{--  --}}
                </div>
            </div>
        </div>
    </div>
@endsection
