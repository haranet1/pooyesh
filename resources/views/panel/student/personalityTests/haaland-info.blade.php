@extends('panel.student.master')


@section('title','تست هالند')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')

    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">
                <div class="row align-items-center">
                    <div class="col-md-5 col-12">
                        <div class="card shadow-lg">
                            <div class="row justify-content-center">
                                <img style="max-width: 70%;" class="m-5" src="{{asset('assets/images/haaland-test.svg')}}" alt="haaland-text">
                            </div>
                            <div class="row">
                                <img class="p-6" src="{{asset('assets/images/haaland-image.svg')}}" alt="haaland" class="img-fluid" style="">
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-7 col-12 p-5 ">
                        <h2 class="c-h-size" style="color: #d8a516">آزمون رغبت شناسی هالند</h2>
                        <p class="c-p-size" align="justify">
                            وقتی کارهایی را انجام می‌دهید که دوست دارید، از کار خود لذت می‌برید. کدهای تست شخصیت شناسی هالند مجموعه‌ای از انواع شخصیت است که توسط روانشناس جان ل. هالند در دهه ۱۹۷۰ ساخته‌شده است. دکتر هالند معتقد است که مردم بهترین عملکرد خود را در محیط‌های کاری‌ای نشان می‌دهند که با ترجیحات و علاقه‌مندی‌هایشان مطابقت دارد. هماهنگ بودن فرد و محیط کار می‌تواند بهترین شرایط را ایجاد کند. اکثر مردم ترکیبی از دو یا سه زمینه موردتوجه هالند هستند. این دو یا سه زمینه تبدیل به “کد هالند” شما می‌شوند.</p>
                        <div class="row p-5 justify-content-center">
                            <a href="{{route('haaland.take.test')}}" class="btn h4 p-2 text-white" style="max-width: 50%;background-color:#d8a516; !important">شروع آزمون</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
