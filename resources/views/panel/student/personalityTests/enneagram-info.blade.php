@extends('panel.student.master')


@section('title','تست Enneagram')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')

    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">
                <div class="row align-items-center">
                    <div class="col-md-5 col-12">
                        <div class="card shadow-lg">
                            <div class="row justify-content-center">
                                <img style="max-width: 80%;" class="m-4" src="{{asset('assets/images/enneagram-text.svg')}}" alt="Enneagram-text">
                            </div>
                            <div class="row">
                                <img class="p-6" src="{{asset('assets/images/enneagram-image.svg')}}" alt="Enneagram" class="img-fluid" style="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-12 p-5">
                        <h2 class="text-primary c-h-size">آزمون شخصیت شناسی انیاگرام</h2>
                        <p class="c-p-size" align="justify">
                            تست انیاگرام یک معیار شخصیت شناسی افراد هست که با استفاده از این میتوانند شخصیت قالب خود را پیدا نموده و برای بهبود شخصیتی خود اقدام کنند. انیاگرام یکی از سیستم های شخصیت شناسی است که انسانها را براساس خصوصیات افراد به 9 گروه یا نه تیپ شخصیتی تقسیم بندی می کند. انیاگرام enneagram کلمه‌ای یونانی‌ است و از دو بخش ennea به معنای 9 و gram به معنای وجه تشکیل شده است. enneagram در زبان فارسی اناگرام ، انیاگرام ، ایناگرام ، اینیاگرام و حتی آناگرام نیز ترجمه شده است
                        </p>
                        <div class="row p-5 justify-content-center">
                            <a href="{{route('enneagram.takeTest')}}" class="btn btn-primary h4 p-2" style="max-width: 50%;">شروع آزمون</a>
                        </div>
                    </div>
                </div>
        


                {{-- <div class="row">
                    <div class="col-md-6 col-12">
                        <h2>آزمون شخصیت شناسی Enneagram</h2>
                        <p style="font-size: 1.1rem; opacity:0.8">تست انیاگرام یک معیار شخصیت شناسی افراد هست که با استفاده از این میتوانند شخصیت قالب خود را پیدا نموده و برای بهبود شخصیتی خود اقدام کنند.  انیاگرام یکی از سیستم های شخصیت شناسی است که انسانها را براساس خصوصیات افراد به 9 گروه یا نه تیپ شخصیتی تقسیم بندی می کند. انیاگرام enneagram کلمه‌ای یونانی‌ است و از دو بخش ennea به معنای 9 و  gra به معنای وجه تشکیل شده است. enneagram در زبان فارسی اناگرام ، انیاگرام ، ایناگرام ، اینیاگرام و حتی آناگرام نیز ترجمه شده است.</p>
                        <h2>تاریخچه تست Enneagram</h2>
                        <p align="justify" style="font-size: 1.1rem; opacity:0.8">اولین بار گورجیف، دانشمند روسی این مکتب را به صورت منظم و هماهنگ گردآوری کرده است و اشخاصی چون “اسکار ایچازو” و “کلودیو نارن‏جو” روش اناگرام را با علم روان‏شناسی تلفیق کرده و به ‏وسیله یافته ‏های جدید قرن بیستم، ساختار علمی ‏تری به این روش داده ‏اند.در این نوع از تیپ شناسی نه تیپ به یک دایره تشبیه شده، از آنجایی که دایره رأس و قاعده ندارد فاصله هر تیپ تا مرکز دایره برابر است، بدین معنا که هیچ تیپی و هیچ موقعیتی از نظر مکانی به تیپ های دیگر برتری ندارد. . طبق نظریه انیاگرام، افراد با یک تیپ شخصیتی غالب متولد می‌شوند که عوامل محیطی و تجربیات هم در شکل‌گیری آن تأثیرگذارند، یعنی در حالی که ویژگی‌های ذاتی فرد روی واکنش‌ها و تجربیاتش اثر می‌گذارد، محیط نیز در شکل‌گیری هویت و شخصیت فرد نقش مهمی دارد.هر یک از انواع شخصیت های ۹ گانه دارای خصوصیات منحصر به خود و نقاط ضعف و قوت مختص خود و شامل دوبال (عدد قبل و بعد هر تیپ) و دوکانال (خطوط داخل دایره که از هر تیپ به دو تیپ دیگر وصل شده است) می باشد. اناگرام را  می توان برای خودشناسی و شناخت دیگران؛ کمک به ارتقای توانایی های افراد در سازمان و یا آزمون های استخدامی بکار برد. در ادامه به بررسی هر یک از تیپ های شخصیتی می پردازیم.</p>
                    </div>
                    <div class="col-md-6 col-12 justify-content-center text-center">
                        
                        <a href="{{route('enneagram.takeTest')}}" class="btn btn-success w-50 my-5 h5 text-white" >شروع آزمون</a>
                    </div>
                </div> --}}
                
            </div>
            
        </div>

    </div>
@endsection
