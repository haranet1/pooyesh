@extends('panel.student.master')


@section('title','تست MBTI')


@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/haaland.css')}}">
@endsection
@section('main')
<style>
    @media (min-width:1200px){
        .f-16{
            font-size: 1.2rem;
        }
        .f-12{
            font-size: 0.95rem;
        }
    }
    @media (max-width:360px){
        .nowrap {
            font-size: 16px;
            white-space: nowrap;
        }
        .f-16{
            font-size: 16px;
            margin-bottom: 0.2rem;
        }
        .f-12{
            font-size: 12px;
            margin-top: 0.2rem;
        }
    }
</style>

        <div id="progress" class="row align-items-center pb-3 " style="z-index:100;box-sizing: border-box; position: fixed; width:80%;background-color: #cfe2fa; left:45px;">  
            <div class="col-2 col-md-1 mt-2 d-flex justify-content-end">
                <span class="mx-2" id="prog1">1</span>
                <span style="white-space: nowrap !important">از 18</span>
            </div>
            <div class="col-8 col-md-10">
                <div class="progress progress-striped mt-3" dir="rtl" style="height:25px;">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1"
                         aria-valuemin="1" aria-valuemax="18" style="">
                        <span class="prog" id="prog2">1</span>
                    </div>
                </div>
            </div>
            <div class="col-2 col-md-1 mt-2">
                <span>پایان آزمون</span>
            </div>
        </div>

    <div class="page-header mt-0">
        <div>
            
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div id="question_container" class="card py-5 px-md-5 px-sm-0">
            </div>
            <div id="answers_container" class="card py-5 px-md-5 px-sm-0">
            </div>
        </div>
        <section class="btn-row">
            <section>
                <button class="btn btn-primary me-3 prev-btn">سوال قبل</button>
                <button class="btn btn-primary next-btn">سوال بعد</button>
                <button onclick="finishTest()" class="btn btn-primary d-none btn-end">پایان آزمون</button>
            </section>
        </section>
    </div>
    @endsection
    
    @section('script')

    <script>
        const haaladUrl = "{{route('haaland.test.result')}}";
        const csrf = "{{csrf_token()}}";
    </script>


    <script>
        let answers = JSON.parse(@json($answers));
        
    </script>
    <script src="{{asset('assets/panel/js/haaland.js')}}"></script>

@endsection
