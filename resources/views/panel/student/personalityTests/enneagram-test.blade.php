@extends('panel.student.master')


@section('title','تست Enneagram')

@section('main')
<style>
    @media (min-width:1200px){
        .f-16{
            font-size: 1.2rem;
        }
        .f-12{
            font-size: 0.95rem;
        }
    }
    @media (max-width:400px){
        .nowrap {
            font-size: 16px;
            white-space: wrap;
            line-height: 1.5rem;
        }
        .f-16{
            font-size: 16px;
            margin-bottom: 0.2rem;
        }
        .f-12{
            font-size: 12px;
            margin-top: 0.2rem;
        }
    }
</style>
    <div id="progress" class="row align-items-center pb-3" style="z-index:100;box-sizing: border-box; position: fixed; width:80%;background-color: #cfe2fa;left:45px;">  
        <div class="col-2 col-md-1 mt-2 d-flex justify-content-end">
            <span class="mx-2" id="prog1">0</span>
            <span style="white-space: nowrap !important">از 60</span>
        </div>
        <div class="col-8 col-md-10">
            <div class="progress progress-striped mt-3" dir="rtl" style="height:25px;">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0"
                    aria-valuemin="0" aria-valuemax="60" style="">
                    <span class="prog" id="prog2">0</span>
                </div>
            </div>
        </div>
        <div class="col-2 col-md-1 mt-2">
            <span>پایان آزمون</span>
        </div>
    </div>

    <div class="page-header mt-0">
        <div>

        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="card py-5 px-md-5 px-sm-0 ">
                <div class="alert alert-info alert-dismissible fade-show">
                    <div class="d-flex align-items-center">
                      <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
                      <div class="flex-grow-1">
                        <p class="mb-0" style="font-size: 1.2rem;">
                          <strong>توجه داشته باشید که همه سوالات را پاسخ دهید</strong>
                        </p>
                      </div>
                    </div>
                  </div>
                <form action="{{route('enneagram.step2')}}" method="POST" class="mt-3">
                    @csrf
                    @for ($i = 1; $i<=60 ; $i++)
                        <div id="{{$i}}" class="row m-3 border"  style="border-radius:15px; margin-bottom:130px !important;">
                            <h4 class="nowrap">{{$i}} - {{$questions[$i]['title']}}</h4>
                            <div class="form-check">
                                <input id="{{$questions[$i]['category'].$i.'z'}}" class="form-check-input" type="radio" name="{{$questions[$i]['category'].$i}}"
                                       value="z" 
                                       @if (old($questions[$i]['category'].$i) && old($questions[$i]['category'].$i) == "z") checked @endif
                                       onclick="updateProgress({{$i}});">
                                <label for="{{$questions[$i]['category'].$i.'z'}}" class="form-check-label text-primary">
                                    به ندرت
                                </label>
                            </div>
                            <div class="form-check">
                                <input id="{{$questions[$i]['category'].$i.'1'}}"  class="form-check-input" type="radio" name="{{$questions[$i]['category'].$i}}"
                                       value="1"
                                       @if (old($questions[$i]['category'].$i) && old($questions[$i]['category'].$i) == "1") checked @endif
                                       onclick="updateProgress({{$i}});">
                                <label for="{{$questions[$i]['category'].$i.'1'}}" class="form-check-label text-primary">
                                    بعضی اوقات
                                </label>
                            </div>
                            <div class="form-check">
                                <input id="{{$questions[$i]['category'].$i.'2'}}" class="form-check-input" type="radio" name="{{$questions[$i]['category'].$i}}"
                                       value="2"
                                       @if (old($questions[$i]['category'].$i) && old($questions[$i]['category'].$i) == "2") checked @endif
                                       onclick="updateProgress({{$i}});">
                                <label for="{{$questions[$i]['category'].$i.'2'}}" class="form-check-label text-primary">
                                    اغلب اوقات
                                </label>
                            </div>
                            @error($questions[$i]['category'].$i)
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    @endfor
                    <div class="row justify-content-center mt-5">
                        <button class="btn btn-success w-25" type="submit">مرحله بعد</button>
                    </div>
                </form>
            </div>

        </div>

    </div>
@endsection

@section('script')
    <script>
                
        var radios = $('input[type="radio"][class="form-check-input"]:checked').length;
        updateProgressNumbers();
        updateProgress(radios);
        adjustProgressSize();

        $('input[type="radio"]').on('click', function(){
            updateProgressNumbers();
            myScroll($(this));
        });

        function updateProgressNumbers() {
            var radios = $('input[type="radio"][class="form-check-input"]:checked').length;
            var prog1 = $('#prog1');
            var prog2 = $('#prog2');
            
            var currentProg1 = parseInt(prog1.text());
            var currentProg2 = parseInt(prog2.text());
            
            prog1.text(radios);
            prog2.text(radios);
        }

        function myScroll(element){
            var targetDiv = element.parent().parent();
            var id = targetDiv.attr('id');
            id = parseInt(id);
            
            $('html, body').animate({
                scrollTop: $('#' + id).offset().top
            }, 500);
            
            targetDiv.removeClass('border-primary border-4');
            id++;
            $('#' + id).addClass('border-primary border-5');
        }

        function updateProgress(step) {
            var progressBar =  $('.progress-bar');
            var width = (step / 60) * 100; // Calculate the percentage width

            progressBar.css('width', width + "%");
        }
        function adjustProgressSize() {
            var customWidth = $('.page-header').width();
            $('#progress').width(customWidth);
        }
    </script>
    <script>
        window.onresize = adjustProgressSize();
    </script>
@endsection
