@extends('panel.student.master')


@section('title','تست NEO')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')

    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">
                <div class="row align-items-center">
                    <div class="col-md-5 col-12">
                        <div class="card shadow-lg">
                            <div class="row justify-content-center">
                                <img style="max-width: 50%;" class="m-5" src="{{asset('assets/images/neo-test.svg')}}" alt="Enneagram-text">
                            </div>
                            <div class="row">
                                <img class="p-6" src="{{asset('assets/images/neo-image.svg')}}" alt="Enneagram" class="img-fluid" style="">
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-7 col-12 p-5 ">
                        <h2 class="c-h-size" style="color: #eb8117">آزمون شخصیت شناسی NEO</h2>
                        <p class="c-p-size" align="justify">
                            تست نئو آزمون مشهور و جامع برای سنجش 5 عامل بزرگ شخصیت می باشد. هریک از این 5 صفت بزرگ شخصیتی شامل 6 جنبه دیگر هستند که باعث می شود این تست 30 ویژگی جزئی را نیز ارزیابی نماید. تست شخصیت شناسی نئو دارای فرم های کوتاه و بلند است.
                        </p>
                        <div class="row p-5 justify-content-center">
                            <a href="{{route('neo.take.test')}}" class="btn h4 p-2 text-white" style="max-width: 50%;background-color:#eb8117; !important">شروع آزمون</a>
                        </div>
                    </div>
                </div>


                {{-- <div class="row">
                    <div class="col-md-6 col-12">
                        <h2>آزمون شخصیت شناسی MBTI</h2>
                        <p style="font-size: 1.1rem; opacity:0.8">تست MBTI، یک پرسش‌نامه روان‌سنجی فردی است که برای شناسایی نوع شخصیت، نقاط قوت و اولویت‌های افراد طراحی شده است. این تست توسط ایزابل مایرز (Isabel Myers) و مادرش کاترین بریگز (Katherine Briggs) و بر اساس کارشان بر روی تئوری کارل یونگ (Carl Jung) درمورد انواع شخصیت تهیه شده است.  امروزه، تست MBTI به‌عنوان یکی از از پرکاربردترین ابزارهای روان‌شناسی در دنیا شناخته می‌شود.</p>
                        <h2>اهمیت تست MBTI</h2>
                        <p style="font-size: 1.1rem; opacity:0.8">به‌طور مشخص، تست شخصیت شناسی به افراد کمک می‌کند تا شناخت بهتری از خود و دیگران پیدا کند. این شناخت یکی از باارزش‌ترین خصیصه‌هایی است که افراد در هر سازمان یا مجموعه‌ای می‌توانند در اختیار داشته باشند. اهمیت شناخت صحیح از شخصیت، هم برای سازمان و هم برای افراد، از چنان اهمیتی برای موفقیت فردی و سازمانی برخورد است که به‌هیچ ‌عنوان نمی‌توان آن را دست‌کم گرفت.برای یک استخدام موفق شما می‌توانید پس از انجام تست نتیجه آن را در  رزومه ساز کاربوم به رزومه خود پیوست کنید. با این کار احتمال بررسی رزومه و استخدام خود را بالا ببرید.</p>
                    </div>
                    <div class="col-md-6 col-12 justify-content-center text-center">
                        <img src="{{asset('assets/images/mbti.svg')}}" alt="MBTI" class="img-fluid" style="max-width: 800px">
                        <a href="{{route('mbti.takeTest')}}" class="btn btn-success w-50 my-5 h3 text-white" >شروع آزمون</a>
                    </div>
                </div> --}}
                
            </div>
            
        </div>

    </div>
@endsection
