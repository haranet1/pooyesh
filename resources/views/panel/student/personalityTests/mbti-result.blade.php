@extends('panel.student.master')
@section('title','نتیجه تست MBTI')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection
@section('full_width')
    @if(!in_array('career', $vip) || !in_array('major', $vip))
    <div class="p-2 px-4 alert-dismissible fade-show second-sticky">
        <div class="d-flex align-items-center flex-md-row flex-column justify-content-center">
            <div class="d-flex align-items-center mb-2 mb-md-0">
                <img class="me-3 icon-animate bounce" src="{{asset('assets/images/light-bulb.png')}}" style="max-width: 40px" alt="icon">
                <p class="fs-18 text-black mb-0 fw-normal text-center">
                    
                    <span class="text-dark-blue fw-bold">مشاغل و رشته های</span> مرتبط با تیپ شخصیتی شما

                </p>
            </div>
            <button data-bs-toggle="modal" data-bs-target="#gateway-modal" class="btn-modal fw-bold ms-md-5">مشاهده</button>

        </div>
    </div>
    @endif
    <div class="modal fade" id="gateway-modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header text-center">
                    <h6 class="modal-title fw-bold fs-6">خرید نتیجه تکمیلی تست شخصیت MBTI</h6>
                    <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('test.mbti.config') }}" method="POST">
                    @csrf
                    <div class="modal-body fieldset">
                        <div class="p-3 bg-title-modal mb-3">
                            <h5 class="fw-bold">نتایج تکمیلی:</h5>
                            <p>برای مشاهده رشته های تحصیلی و مشاغل پیشنهادی متناسب با تیپ شخصیتی شما باید هزینه مشاوره را پرداخت نمایید.</p>
                        </div>

                        <div class="mb-4 modal-box p-3">
                            <div class="d-flex align-items-center ms-3 mb-3">
                                <img class="me-2 desktop-logo" src="{{asset('assets/images/checked-blue.png')}}" style="max-width: 19px" alt="">
                                <h4 class="mb-0">انتخاب نوع خرید</h4>
                            </div>
                            <div class="row">
                                @if(!in_array('career', $vip))
                                <div class="col-md-6">
                                    <div class="radio-item">
                                        <label for="career">
                                            <input type="radio" id="career" name="type" value="career">
                                            <span>مشاغل متناسب با تیپ شخصیتی</span>
                                        </label>
                                        <div class="">قیمت : {{ $price_career }} تومان</div>
                                    </div>
                                </div>

                                @endif
                                @if(!in_array('major', $vip))
                                <div class="col-md-6">
                                    <div class="radio-item">
                                        <label for="major">
                                            <input type="radio" id="major" name="type" value="major">
                                            <span>رشته های تحصیلی متناسب با تیپ شخصیتی</span>
                                        </label>
                                        <div>قیمت : {{ $price_major }} تومان</div>
                                    </div>
                                </div>
                                   
                                @endif
                                @if(!in_array('major', $vip) && !in_array('career', $vip))
                                <div class="col-md-6">
                                    <div class="radio-item">
                                        <label for="full">
                                            <input type="radio" id="full" name="type" value="full">
                                            <span>مشاغل و رشته های تحصیلی</span>
                                        </label>
                                        <div>قیمت : {{ $price_major + $price_career }} تومان</div>
                                    </div>
                                </div>
                                 
                                @endif
                            </div>

                        </div>
                        <hr>

                        <div class="modal-box p-3">
                            <div class="d-flex align-items-center ms-3 mb-3">
                                <img class="me-2" src="{{asset('assets/images/checked-blue.png')}}" style="max-width: 19px" alt="">
                                <h4 class="mb-0">انتخاب درگاه پرداخت</h4>
                            </div>
                            <div class="radio-item item-gateway">
                                <label for="iaun">
                                    <input type="radio" id="iaun" name="gateway" value="iaun" checked>
                                    <span>درگاه دانشگاه آزاد نجف آباد</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn-modal" type="submit">تکمیل پرداخت</button>
                        <button class="btn ripple btn-danger rounded-pill" data-bs-dismiss="modal" type="button">بستن</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('main')
    <style>
        .scroll-box {
        max-height: 300px; /* Set the maximum height of the scrollable box */
        overflow: auto;   /* Enable scrolling when content overflows */
        }

        .item-list {
        list-style: none;
        padding: 0;
        margin: 0;
        }

        .item-list li {
        padding: 5px 0;
        border-bottom: 1px solid #eee;
        }
        hr.dashed {
            border: none;
            border-top: 3px dashed #000; /* You can change the color here */
            height: 0;
            margin: 40px 0; /* Adjust margin as needed */
            /* margin-top: 40px; */
        }
    </style>

    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">

                <div class="row mx-1 mb-6 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-3 px-0 d-flex justify-content-center">
                        <img style="width: 70%;" src="{{asset('assets/panel/images/brand/jobist/text-logo.svg')}}" alt="">
                    </div>
                    <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                        <img style="width: 30px;" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="">
                    </div>
                    <div class="col-6 col-md-2 my-2 px-0">
                        <h4 class="m-0">{{Auth::user()->name}} {{Auth::user()->family}}</h4>
                    </div>
                    <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                        <img style="width: 20px;" src="{{asset('assets/panel/images/icon/SVG/mobile.svg')}}" alt="">
                    </div>
                    <div class="col-6 col-md-2 my-2 px-0">
                        <h4 class="m-0">{{Auth::user()->mobile}}</h4>
                    </div>
                    <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                        <img style="width: 40px;" src="{{asset('assets/panel/images/icon/SVG/n_code.svg')}}" alt="">
                    </div>
                    <div class="col-6 col-md-2 my-2 px-0">
                        @if (Auth::user()->n_code)
                            <h4 class="m-0">{{Auth::user()->n_code}}</h4>
                        @else
                            <h4 class="m-0">کد ملی ثبت نشده</h4>
                        @endif
                    </div>
                </div>

                @if(Session::has('error'))
                    <div class="alert alert-danger  text-center">
                        {{Session::pull('error')}}
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success  text-center">
                        {{Session::pull('success')}}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(in_array('career', $vip) || in_array('major', $vip))
                <div class="d-flex justify-content-center mb-3 flex-lg-row flex-column flex-wrap gap-3">
                    @if(in_array('career', $vip))
                        <div class="text-center mb-1 mb-lg-0">
                            <a class="btn btn-info px-4 text-break text-white fs-6" id="scrollButton">مشاهده مشاغل متناسب با تیپ {{$mbtiResult->personality_type}}</a>
                        </div>
                    @endif
                    @if(in_array('major', $vip))
                        <div class="text-center">
                            <a class="btn btn-success px-4 text-break text-white fs-6" id="majorButton">مشاهده رشته های تحصیلی با تیپ {{$mbtiResult->personality_type}}</a>
                        </div>
                    @endif
                </div>

                   
                    
                @endif


                <img src="{{asset('assets/images/mbti-images/'.$personality_type->photo_path)}}" alt="image">

                <div class="row mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6">
                        <canvas id="chart1"></canvas>
                    </div>

                    <div class="col-12 col-md-6">
                        <h2 class="mt-3 c-h-size">شخصیت {{$mbtiResult->personality_type}} ({{$personality_type->fa_title}})</h3>
                        <p align="justify" class="c-font-size">{{$personality_type->analyse}}</p>
                    </div>
                </div>

                <div class="row mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6">
                        <h2 class="c-h-size">شخصیت {{$mbtiResult->personality_type}} در محل کار:</h2>
                        <p align="justify" class="c-font-size">{{$personality_type->work_analyse}}</p>
                    </div>

                    <div class="col-12 col-md-6">
                        <canvas id="chart2"></canvas>
                    </div>
                </div>

                <div class="row  mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                        <img class="c-img-margin"  style="max-width: 80%" src="{{asset('assets/images/mbti-work-image.svg')}}" alt="">
                    </div>

                    <div class="col-12 col-md-6">
                        <h2 class="c-h-size">شخصیت {{$mbtiResult->personality_type}} در روابط اجتماعی:</h2>
                        <p align="justify" class="c-font-size">{{$personality_type->relations_analyse}}</p>
                    </div>
                </div>

                <div class="row mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6">
                        <h2 class="c-h-size">توصیه هایی برای شما:</h2>
                        <div class="">
                            <ul class="item-list">
                                @foreach ($personality_type->other as $item)
                                    <li class="c-font-size">{{$item}}</li>
                                @endforeach
                            </ul>
                        </div>
                        <ul>

                        </ul>
                    </div>
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                        <img class="c-img-margin" style="max-width: 60%" src="{{asset('assets/images/mbti-other-image.svg')}}" alt="">
                    </div>
                </div>

                @if(!is_null($vip))
                    <div>
                        @if(in_array('career', $vip))
                            <div class="row  mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;" id="targetSection">
                                <div class="col-12 col-md-6 d-flex justify-content-center">
                                    <img class="c-img-margin"  style="max-width: 80%" src="{{asset('assets/images/mbti-career-image.jpg')}}" alt="">
                                </div>

                                <div class="col-12 col-md-6">
                                    <h2 class="c-h-size"> مشاغل متناسب با تیپ شخصیتی {{$mbtiResult->personality_type}}</h2>
                                    <p align="justify" class="c-font-size">{{$personality_type->career}}</p>
                                </div>
                            </div>
                            <div class="border shadow-sm mx-1 mt-5 " style="border-radius:15px;">
                                <h2 class="c-h-size mt-6 text-center">لیست مشاغل پیشنهادی:</h2>
                                <div class="row">
                                    @foreach ($jobs as $job)
                                        <div class="col-md-4">
                                            <div class="job-title">
                                                {{$job->title}} 
                                            </div>
                                        </div>
                                    @endforeach
        
                                </div>
                            </div>
    
                        @endif
                        @if(in_array('major', $vip))
                        <div class="border shadow-sm mx-1 mt-5" style="border-radius:15px;" id="majorSection">
                            <h2 class="c-h-size text-center mt-5">لیست رشته های تحصیلی پیشنهادی:</h2>
                            <div class="row row-cols-auto mb-4">
                                @foreach ($fields as $field)
                                    <div class="col-md-4">
                                        <div class="major-title">
                                            {{$field->title}} 
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            @if (count($fields) > 0)
                                <div class="row">
                                    <div class="card">
                                        <div class="card-body bg-info mx-3 text-white" style="border-radius: 1rem !important">
                                            <p class="card-title">
                                                لطفا جهت دریافت خدمات هدایت تحصیلی و انجام امور ثبت نام، به دفتر پذیرش آموزش دانشگاه واقع در ساختمان اندیشه مراجعه نمایید.
                                            </p>
                                            <p class="card-title">
                                                ضمناً می‌توانید با تماس به شماره <a href="tel:031-4200" class="btn btn-success"><span class="border-bottom">031-4200</span></a>
                                                مجموعه اطلاعات مفیدی در زمینه ثبت نام در دانش شهر ایران (دانشگاه آزاد اسلامی نجف آباد) کسب کنید.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        @endif
                    </div>
                @endif

                @if (Hash::check(Auth::user()->mobile,Auth::user()->password))
                    <hr class="dashed">
                    <p>قسمت مخصوص کاربر (از این جا بریده شود)</p>
                    <div class="row">
                        <div class="alert alert-info alert-dismissible fade-show">
                            <div class="d-flex align-items-center">
                            <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
                            <div class="flex-grow-1">
                                <h4>{{Auth::user()->name}} {{Auth::user()->family}} گرامی حساب کاربری شما در سامانه {{ env('APP_NAME') }} ایجاد شد.</h4>
                                <p>
                                    <strong>از این پس با وارد کردن شماره تلفن همراه خود به عنوان نام کاربری و رمز عبور می‌توانید وارد حساب کاربری خود شوید.</strong>
                                    <br>برای دریافت و مشاهده موقعیت های شغلی و کارآموزی متناسب با ویژگی ها، توانایی ها و مهارت های شخصی؛ از پنل کاربری نسبت به تکمیل رزومه و تست های شخصیتی دیگر اقدام نمایید.
                                </p>
                            </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row my-5 d-print-none">
                    <a id="print" class="btn btn-success text-white">چاپ نتیجه تست</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

    <script>
    document.getElementById("print").onclick = function jsFunc() {
        window.print();
    }
    </script>
    <script>
        var chart1 = document.getElementById('chart1');
        var mychart = new Chart(chart1,{
            type: 'bar',
            data: {
                labels:['برون گرا','درون گرا','شهودی','حسی','منطقی','احساسی','قضاوت گر','ادراکی'],
                datasets:[{
                    label: '',
                    data: [{{percent(15,$mbtiResult->E)}},{{percent(15,$mbtiResult->I)}},{{percent(15,$mbtiResult->N)}},{{percent(15,$mbtiResult->S)}},{{percent(15,$mbtiResult->T)}},{{percent(15,$mbtiResult->F)}},{{percent(15,$mbtiResult->J)}},{{percent(15,$mbtiResult->P)}}],
                    backgroundColor: [
                        'rgba(227, 52, 47, 0.5)',
                        'rgba(246, 153, 63, 0.5)',
                        'rgba(255, 237, 74, 0.5)',
                        'rgba(56, 193, 114, 0.5)',
                        'rgba(77, 192, 181, 0.5)',
                        'rgba(52, 144, 220, 0.5)',
                        'rgba(149, 97, 2267, 0.5)',
                        'rgba(246, 109, 155, 0.5)'
                    ],
                    borderColor:['rgb(227, 52, 47)',
                                'rgb(246, 153, 63)',
                                'rgb(255, 237, 74)',
                                'rgb(56, 193, 114)',
                                'rgb(77, 192, 181)',
                                'rgb(52, 144, 220)',
                                'rgb(149, 97, 2267)',
                                'rgb(246, 109, 155)'],
                    borderWidth: 1,

                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                aspectRatio: 1,
                legend: false,
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: false ,
                            drawBorder: false
                        },
                        ticks: {
                            max: 100,
                            min: 0,
                            stepSize: 10,
                            display: false
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false ,// Hide horizontal grid lines
                            drawBorder: false
                        }
                    }]
                }
            }
        })
    </script>

    <script>
        var chart2 = document.getElementById('chart2').getContext('2d');
        var myChart = new Chart(chart2, {
            type: 'doughnut',
            data: {
                labels: [
                    'برون گرا',
                    'درون گرا',
                    'شهودی',
                    'حسی',
                    'منطقی',
                    'احساسی',
                    'قضاوت گر',
                    'ادراکی'
                ],
                datasets: [{
                    data: [{{percent(15,$mbtiResult->E)}},{{percent(15,$mbtiResult->I)}},{{percent(15,$mbtiResult->N)}},{{percent(15,$mbtiResult->S)}},{{percent(15,$mbtiResult->T)}},{{percent(15,$mbtiResult->F)}},{{percent(15,$mbtiResult->J)}},{{percent(15,$mbtiResult->P)}}],
                    backgroundColor: [
                        'rgba(227, 52, 47, 0.8)',
                        'rgba(246, 153, 63, 0.8)',
                        'rgba(255, 237, 74, 0.8)',
                        'rgba(56, 193, 114, 0.8)',
                        'rgba(77, 192, 181, 0.8)',
                        'rgba(52, 144, 220, 0.8)',
                        'rgba(149, 97, 2267, 0.8)',
                        'rgba(246, 109, 155, 0.8)'
                    ],
                }]
            },
            options:{
                responsive: true,
                maintainAspectRatio: false,
                aspectRatio: 1
            }
        });
    </script>
    <script>

        $(document).ready( function(){
            let targetElement = document.getElementById('targetSection');
            if (targetElement != null) {
                    let scrollButton = document.getElementById('scrollButton');
                    scrollButton.addEventListener('click', function(e) {
                    targetElement.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });

                });
            }

            let targetElement2 = document.getElementById('majorSection');
            if (targetElement2 != null) {
                    let majorButton = document.getElementById('majorButton');
                    majorButton.addEventListener('click', function(e) {
                        targetElement2.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
                });
            }
        
        });



    </script>

    {{-- <script>
        var chart1 = document.getElementById('chart1');
        var chartImage = chart1.toDataURL('image/png');
        $.ajax({
            headers:{
                'X-CSRF-TOKEN':" {{csrf_token()}}"
            },
            method: "POST",
            url: "{{route('mbti.generatePDF.test')}}",
            data: {
                file: chartImage
            },
            dataType: "dataType",
            success: function (response) {
                console.log(response);
            },
            error: function(error){
                console.log(error);
            }
        });
    </script> --}}

@endsection

