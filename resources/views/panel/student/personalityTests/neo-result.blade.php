@extends('panel.student.master')


@section('title','نتیجه تست NEO')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')
    <style>
        .scroll-box {
        max-height: 300px; /* Set the maximum height of the scrollable box */
        overflow: auto;   /* Enable scrolling when content overflows */
        }

        .item-list {
        list-style: none;
        padding: 0;
        margin: 0;
        }

        .item-list li {
        padding: 5px 0;
        border-bottom: 1px solid #eee;
        }
        hr.dashed {
            border: none;
            border-top: 3px dashed #000; /* You can change the color here */
            height: 0;
            margin: 40px 0; /* Adjust margin as needed */
            /* margin-top: 40px; */
        }
        @media screen and (width: 400px) {
            #bg-info{background-image: url("{{asset('assets/images/neo-images/neo-header1-400.svg')}}") !important}
        }
    </style>

    {{-- <div class="row row-cards  mt-5 ">
        <div >
            <div class="main-body">
                <div class="row gutters-sm">
                    <div class="col-md-3 mb-3 pb ">
                    <div class="card pb-5">
                        <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150">
                            <div class="mt-3">
                            <h4>John Doe</h4>
                            <p class="text-secondary mb-1">Full Stack Developer</p>
                            <p class="text-muted font-size-sm">Bay Area, San Francisco, CA</p>
                            <button class="btn btn-primary">Follow</button>
                            <button class="btn btn-outline-primary">Message</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="card mt-1">
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe mr-2 icon-inline"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>Website</h6>
                            <span class="text-secondary">https://bootdey.com</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-github mr-2 icon-inline"><path d="M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22"></path></svg>Github</h6>
                            <span class="text-secondary">bootdey</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter mr-2 icon-inline text-info"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg>Twitter</h6>
                            <span class="text-secondary">@bootdey</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram mr-2 icon-inline text-danger"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg>Instagram</h6>
                            <span class="text-secondary">bootdey</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook mr-2 icon-inline text-primary"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg>Facebook</h6>
                            <span class="text-secondary">bootdey</span>
                        </li>
                        </ul>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="card mb-1">
                        <div class="card-body">
                        <div class="row">
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eum quidem rerum non perspiciatis qui est error quae officiis, possimus nisi, fugiat similique eligendi tenetur pariatur odio! Corporis natus esse quam. Officia, illum aliquam voluptatum illo laborum hic nemo omnis magnam debitis? Expedita cumque nemo repellendus consequatur veritatis ducimus, alias aperiam?</p>
                        </div>

                        
                        </div>
                    </div>
                    
        
                    <div class="row gutters-sm mt-4">
                        <div class="col-sm-6 mb-3">
                        <div class="card h-100">
                            <div class="card-body">
                            <h6 class="d-flex align-items-center mb-3"><i class="material-icons text-info mr-2">assignment</i>Project Status</h6>
                            <small>Web Design</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Website Markup</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 72%" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>One Page</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 89%" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Mobile Template</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Backend API</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 66%" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-6 mb-3">
                        <div class="card h-100">
                            <div class="card-body">
                            <h6 class="d-flex align-items-center mb-3"><i class="material-icons text-info mr-2">assignment</i>Project Status</h6>
                            <small>Web Design</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Website Markup</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 72%" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>One Page</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 89%" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Mobile Template</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <small>Backend API</small>
                            <div class="progress mb-3" style="height: 5px">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 66%" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div> --}}

    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">
                <div class="card shadow-lg" id="bg-info" style="border:3px solid #c9e9f6 !important;border-radius: 15px; height: 315px;background-image: url('{{asset('assets/images/neo-images/neo-header1.svg')}}')">
                    <div class="row mx-1 my-3 align-items-center">
                        <div class="col-12 col-md-3 px-0 d-flex justify-content-center">
                            <img style="width: 70%;" src="{{asset('assets/panel/images/brand/jobist/text-logo.svg')}}" alt="">
                        </div>
                        <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                            <img style="width: 30px;" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="">
                        </div>
                        <div class="col-6 col-md-2 my-2 px-0">
                            <h4 class="m-0">{{Auth::user()->name}} {{Auth::user()->family}}</h4>
                        </div>
                        <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                            <img style="width: 20px;" src="{{asset('assets/panel/images/icon/SVG/mobile.svg')}}" alt="">
                        </div>
                        <div class="col-6 col-md-2 my-2 px-0">
                            <h4 class="m-0">{{Auth::user()->mobile}}</h4>
                        </div>
                        <div class="col-6 col-md-1 my-2 px-0 d-flex justify-content-center">
                            <img style="width: 40px;" src="{{asset('assets/panel/images/icon/SVG/n_code.svg')}}" alt="">
                        </div>
                        <div class="col-6 col-md-2 my-2 px-0">
                            @if (Auth::user()->n_code)
                                <h4 class="m-0">{{Auth::user()->n_code}}</h4>
                            @else
                                <h4 class="m-0">کد ملی ثبت نشده</h4>
                            @endif
                        </div>
                    </div>
                    <div class="row mx-1 my-3 align-items-center">
                        <h1 class="text-center">تست شخصیت NEO</h1>
                    </div>
                </div>
                <div class="row">
                    {{-- right column --}}
                    <div class="col-12 col-md-6">
                        <div class="row p-2">
                            <div class="border shadow-sm" style="border-radius: 15px">
                                <div id="chart1"></div>
                            </div>
                        </div>
                        <div class="row p-2">
                            <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                                <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                  <img src="{{asset('assets/images/neo-images/E.svg')}}"/>
                                </div>
                                <div class="card-body">
                                  <h3 class="font-weight-bold">{{neo_result_fa('E')}}</h3>
                                  <p align="justify" class="c-font-size">{{$results['E']}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row p-2">
                            <div class="card border shadow-sm mb-0" style="border-radius: 15px; min-height: 492px">
                                <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                  <img src="{{asset('assets/images/neo-images/O.svg')}}"/>
                                </div>
                                <div class="card-body">
                                  <h3 class="font-weight-bold">{{neo_result_fa('O')}}</h3>
                                  <p align="justify" class="c-font-size">{{$results['O']}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- left column --}}
                    <div class="col-12 col-md-6">
                        <div class="row p-2">
                            <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                                <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                  <img src="{{asset('assets/images/neo-images/testi1.svg')}}"/>
                                </div>
                                <div class="card-body">
                                  <h3 class="font-weight-bold">{{neo_result_fa('N')}}</h3>
                                  <p align="justify" class="c-font-size">{{$results['N']}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row p-2 justify-content-center">
                            <div class="border shadow-sm" style="border-radius: 15px">
                                <div id="chart2"></div>
                            </div>
                        </div>
                        <div class="row p-2">
                            <div class="card border shadow-sm mb-0" style="border-radius: 15px; min-height: 526px">
                                <div class="bg-image hover-overlay ripple mt-3" data-mdb-ripple-color="light">
                                  <img src="{{asset('assets/images/neo-images/C.svg')}}" style="height: 105%"/>
                                </div>
                                <div class="card-body mt-3">
                                  <h3 class="font-weight-bold">{{neo_result_fa('C')}}</h3>
                                  <p align="justify" class="c-font-size">{{$results['C']}}</p>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                {{-- bottom column --}}
                <div class="row p-2">
                    <div class="card border shadow-sm mb-0" style="border-radius: 15px;">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6">
                                <img src="{{asset('assets/images/neo-images/A.svg')}}" style="height: 100%"/>
                            </div>
                            <div class="col-12 col-md-6">
                                <h3 class="font-weight-bold">{{neo_result_fa('A')}}</h3>
                                <p align="justify" class="c-font-size">{{$results['A']}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-5 d-print-none">
                    <a id="print" class="btn btn-success text-white">چاپ نتیجه تست</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script>
    document.getElementById("print").onclick = function jsFunc() {
        window.print();
    }
    </script>
    <script>
        
            
        var options = {
          series: [{
          data: [{{percent(48,$neoResult['N'])}},{{percent(48,$neoResult['E'])}},{{percent(48,$neoResult['O'])}},{{percent(48,$neoResult['A'])}},{{percent(48,$neoResult['C'])}}],
        }],
        chart: {
            offsetX: -20,
            offsetY: 20,
            height: 350,
            type: 'bar',
            events: {
                click: function(chart, w, e) {
                // console.log(chart, w, e)
                }
            },
        },
        // colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true,
            // horizontal: true,
            dataLabels:{
                position: 'center',
            }
          }
        },
        dataLabels: {
          enabled: true
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [
            ['بی ثباتی', 'هیجان'],
            ['برون گرایی', 'درون گرایی'],
            ['انعطاف', 'پذیری'],
            ['توافق با', 'دیگران'],
            ['وظیفه', 'شناسی'],
          ],
          labels: {
            style: {
            //   colors: colors,
              fontSize: '12px'
            }
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
                show: false,
            }
          }
        }],
        };

        var chart = new ApexCharts(document.querySelector("#chart1"), options);
        chart.render();

    
    </script>
    <script>
        
        var options = {
          series: [{{percent(48,$neoResult['N'])}},{{percent(48,$neoResult['E'])}},{{percent(48,$neoResult['O'])}},{{percent(48,$neoResult['A'])}},{{percent(48,$neoResult['C'])}}],
          chart: {
            offsetX: 0,
            offsetY: 0,
          height: 390,
          type: 'radialBar',
        },
        plotOptions: {
          radialBar: {
            offsetX: 0,
            offsetY: 11,
            startAngle: 0,
            endAngle: 270,
            hollow: {
              margin: 5,
              size: '30%',
              background: 'transparent',
              image: undefined,
            },
            dataLabels: {
              name: {
                show: true,
              },
              value: {
                show: true,
              }
            }
          }
        },
        labels: [
            ['بی ثباتی', 'هیجان'],
            ['برون‌گرای درون‌گرایی'],
            ['انعطاف', 'پذیری'],
            ['توافق با', 'دیگران'],
            ['وظیفه', 'شناسی'],
        ],
        legend: {
          show: true,
          floating: true,
          fontSize: '14px',
          position: 'left',
          offsetX: 50,
          offsetY: -10,
          labels: {
            useSeriesColors: true,
            
          },
          markers: {
            size: 10,
            offsetX: 5,
          },
          formatter: function(seriesName, opts) {
            return " " + " " + seriesName + ": " + opts.w.globals.series[opts.seriesIndex]
          },
          itemMargin: {
            vertical: 3
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
                show: false,
            }
          }
        }]
        };
        // options.labels.style.direction = 'ltr';

        var chart = new ApexCharts(document.querySelector("#chart2"), options);
        chart.render();
    
    </script>
@endsection

