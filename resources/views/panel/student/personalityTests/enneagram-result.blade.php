@extends('panel.student.master')


@section('title','نتیجه تست Enneagram')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')

    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">
                <img src="{{asset('assets/images/enneagram-images/'.$personality->photo_path)}}" alt="">

 
                <div class="row mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6">
                        <canvas id="chart1"></canvas>
                    </div>

                    <div class="col-12 col-md-6">
                        <h2 class="mt-3 c-h-size">شخصیت  {{enneagram_result_fa($enneagram->main_character)}}</h2>
                        {{-- <p align="justify" style="font-size: 1.1rem; opacity:0.8; line-height: 2rem;" class="">{{enneagram_result_fa($enneagram->main_character)}}</p> --}}
                    </div>
                </div>

                <div class="row mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6">
                        <h2 class="text-primary c-h-size">درباره شخصیت {{enneagram_result_fa($enneagram->main_character)}}:</h2>
                        <p align="justify" class="c-font-size">{{$personality->description}}</p>
                    </div>

                    <div class="col-12 col-md-6">
                        <img class="c-img-margin" style="max-width: 100%" src="{{asset('assets/images/enneagram-about-image.svg')}}" alt="">
                        {{-- <canvas id="chart2"></canvas> --}}
                    </div>
                </div>

                <div class="row  mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                        <img class="c-img-margin" style="max-width: 50%" src="{{asset('assets/images/enneagram-fear-image.svg')}}" alt="">
                    </div>

                    <div class="col-12 col-md-6">
                        <h2 class="text-primary c-h-size"> ترس شخصیت {{enneagram_result_fa($enneagram->main_character)}} :</h2>
                        <p align="justify" class="c-pf-size">{{$personality->most_fear}}</p>
                    </div>
                </div>

                <div class="row  mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6">
                        <h2 class="text-primary c-h-size"> خواسته شخصیت {{enneagram_result_fa($enneagram->main_character)}} :</h2>
                        <p align="justify" class="c-pf-size">{{$personality->most_desire}}</p>
                    </div>

                    <div class="col-12 col-md-6 d-flex justify-content-center">
                        <img class="c-img-margin" style="max-width: 50%" src="{{asset('assets/images/enneagram-desire-image.svg')}}" alt="">
                    </div>
                </div>

                <div class="row  mx-1 mt-5 border shadow-sm align-items-center" style="border-radius:15px;">
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                        <img class="c-img-margin" style="max-width: 80%" src="{{asset('assets/images/enneagram-motivations-image.svg')}}" alt="">
                    </div>

                    <div class="col-12 col-md-6">
                        <h2 class="text-primary c-h-size">انگیزه های {{enneagram_result_fa($enneagram->main_character)}} :</h2>
                        <p align="justify" class="c-font-size">{{$personality->motivations}}</p>
                    </div>
                </div>
                <div class="row my-5 d-print-none">
                    <a id="print" class="btn btn-success text-white">چاپ نتیجه تست</a>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        document.getElementById("print").onclick = function jsFunc() {
            window.print();
        }
    </script>
    <script>
        var chart1 = document.getElementById('chart1');
        var mychart = new Chart(chart1,{
            type: 'pie',
            data: {
                labels:[
                    '{{enneagram_result_fa($enneagram->main_character)}}',
                    '{{enneagram_result_fa($enneagram->second_character)}}',
                    '{{enneagram_result_fa($enneagram->weak_character)}}'
                ],
                datasets:[{
                    data: [
                        '{{$enneagram->main_character_score}}',
                        '{{$enneagram->second_character_score}}',
                        '{{$enneagram->weak_character_score}}'
                    ],
                    backgroundColor: [
                        'rgb(214,36,224)',
                        'rgb(40,204,224)',
                        'rgb(117,233,137)',
                    ],

                }]
            },

        })
    </script>
@endsection
