@extends('panel.student.master')


@section('title','تست GHQ')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/mbti-info.css')}}"/>
@endsection

@section('main')

    <div class="row row-cards mt-5">
        <div class="col-lg-12 col-xl-12">
            <div class="card c-padding">
                <div class="row align-items-center">
                    <div class="col-md-5 col-12">
                        <div class="card shadow-lg">
                            <div class="row justify-content-center">
                                <img style="max-width: 65%;" class="m-5" src="{{asset('assets/images/ghq-test.svg')}}" alt="GHQ-text">
                            </div>
                            <div class="row">
                                <img class="p-6" src="{{asset('assets/images/ghq-image.svg')}}" alt="GHQ" class="img-fluid" style="">
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-7 col-12 p-5 ">
                        <h2 class="c-h-size" style="color: #af46af">آزمون سلامت عمومی GHQ</h2>
                        <p class="c-p-size" align="justify">
                            تست سلامت عمومی (GHQ) بـه بررسـی وضـعیت روانی و جسمانی فرد در یک ماهه اخیر می پردازد و شامل نشانه هـایی مانند افکار و احساسات نابهنجار و جنبه هایی از رفتار قابل مشاهده است که بر موقعیت اینجـا و اکنـون تأکیـد دارد. تست سلامت عمومی دو نگرانی اصلی را شناسایی می کند: 1) ناتوانی در انجام عملکردهای عادی و 2) ظهور پدیده های جدید و ناراحت کننده. این آزمون به شما نشان می دهد که وضع عمومی سلامت شما در چند هفته گذشته، چگونه بوده است. تست سلامت عمومی GHQ چهار اصلی را می سنجد: علائم جسمانی، علائم افسردگی، علائم اضطرابی و اختلال خواب و علائم کارکرد اجتماعی.
                        </p>
                        <div class="row p-5 justify-content-center">
                            <a href="{{route('ghq.take.test')}}" class="btn h4 p-2 text-white" style="max-width: 50%;background-color:#af46af; !important">شروع آزمون</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
