@extends('panel.student.master')


@section('title','آزمون ها')

@section('main')
    <div class="page-header">
        <h1 class="page-title">نتیجه تست های شخصیت شناسی شما</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">برنامه‌ها</a></li>
                <li class="breadcrumb-item active" aria-current="page">فهرست آزمون ها</li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="card p-5">
                <div class="row">
                    @if (Auth::user()->mbti_test || Auth::user()->enneagram_test || Auth::user()->neo_test)
                        @if (Auth::user()->mbti_test)
                            <div class="col-12 col-md-4" style="height: 300px">
                                <div class="card shadow-lg" style="border:1px gray !important;">
                                    <img src="{{asset('assets/images/mbti-images/'.Auth::user()->personality_type_mbti->photo_path)}}" class="card-img-top" alt="MBTI">
                                    <div class="card-body text-center">
                                        <h3 class="text-warp" style="font-size: 17px">طبق تست MBTI تیپ شخصیتی شما {{Auth::user()->personality_type_mbti->fa_title}} است</h3>
                                        <a href="{{route('mbti.show.result')}}" class="btn btn-primary">جزئیات بیشتر</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (Auth::user()->haaland_test)
                            <div class="col-12 col-md-4" style="height: 300px">
                                <div class="card shadow-lg" style=" border:1px gray !important;">
                                    <div class="" style="max-height: 105px;max-width: 369px;">
                                        <img src="{{asset('assets/images/haaland-images/'.strtolower(Auth::user()->personality_type_haaland[0]->title).'.svg')}}" class="card-img-top w-100" alt="haaland" style="height: 100%;object-fit: cover;object-position: top;">
                                    </div>
                                    <div class="card-body text-center">
                                        <h3 class="text-wrap" style="font-size: 17px;line-height:1.1rem;" >طبق تست هالند تیپ شخصیتی شما {{(Auth::user()->personality_type_haaland[0]->fa_title)}} است</h3>
                                        <a href="{{route('haaland.show.result')}}" class="btn btn-primary">جزئیات بیشتر</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (Auth::user()->enneagram_test)
                            <div class="col-12 col-md-4" style="height: 300px">
                                <div class="card shadow-lg" style=" border:1px gray !important;">
                                    <img src="{{asset('assets/images/enneagram-images/'.Auth::user()->personality_type_enneagram->photo_path)}}" class="card-img-top" alt="Enneagram">
                                    <div class="card-body text-center">
                                        <h3 class="text-wrap" style="font-size: 17px;line-height:1.1rem;" >طبق تست انیاگرام تیپ شخصیتی شما {{enneagram_result_fa(Auth::user()->personality_type_enneagram->type)}} است</h3>
                                        <a href="{{route('enneagram.show.result')}}" class="btn btn-primary">جزئیات بیشتر</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (Auth::user()->neo_test)
                            <div class="col-12 col-md-4" style="height: 300px">
                                <div class="card shadow-lg" style=" border:1px gray !important;">
                                    <img src="{{asset('assets/images/neo-images/neo-header-result.svg')}}" class="card-img-top" alt="neo">
                                    <div class="card-body text-center">
                                        <h3 class="text-wrap" style="font-size: 17px;line-height:1.1rem;" >جزئیات تست شخصیت نئو خود را مشاهده کنید</h3>
                                        <a href="{{route('neo.show.result')}}" class="btn btn-primary">جزئیات بیشتر</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (Auth::user()->ghq_test)
                            <div class="col-12 col-md-4" style="height: 300px">
                                <div class="card shadow-lg" style=" border:1px gray !important;">
                                    <img src="{{asset('assets/images/ghq-images/'.set_ghq_header(Auth::user()->ghq_test['sum_of_all']))}}" class="card-img-top" alt="neo">
                                    <div class="card-body text-center">
                                        <h3 class="text-wrap" style="font-size: 17px;line-height:1.1rem;" >جزئیات تست سلامت ghq خود را مشاهده کنید</h3>
                                        <a href="{{route('ghq.show.result')}}" class="btn btn-primary">جزئیات بیشتر</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @else
                        <div class="col-12" style="height: 400px">
                            <div class="card shadow-lg text-center" style=" border:1px rgb(156, 30, 30) !important;">
                                <div class="alert alert-danger">
                                    <p style="font-size: 1.2rem;">شما هنوز هیچ تستی را انجام نداده اید</p>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
