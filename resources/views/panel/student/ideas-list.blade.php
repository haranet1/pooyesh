@extends('panel.student.master')
@section('title','لیست ایده ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو 
            @else
                کارجو
            @endif
        </h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> لیست ایده ها</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">

                <div class="card-header border-bottom-0">

                    <div class=" ">
                        <a class="btn btn-success" href="{{route('std.idea.create')}}">افزودن ایده/استارتاپ جدید</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($ideas) > 0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>عنوان </th>
                                <th>بودجه</th>
                                <th>توضیحات</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ideas as $idea)
                            <tr>
                                <td class="text-nowrap align-middle">{{$idea->title}}</td>
                                <td class="text-nowrap align-middle"><span>{{$idea->budget}}</span></td>
                                <td class="text-nowrap align-middle">{{Str::limit($idea->description,'20','...')}}</td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="{{route('ideas.single',$idea->id)}}" class="btn btn-sm btn-primary badge" type="button">مشاهده </a>
                                        <a href="{{route('std.idea.edit',$idea->id)}}" class="btn btn-sm btn-warning badge" type="button">ویرایش </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif

                    </div>
                </div>
            </div>
            <div class="mb-5">
               {{$ideas->links('pagination.panel')}}
            </div>
        </div>
    </div>
@endsection


