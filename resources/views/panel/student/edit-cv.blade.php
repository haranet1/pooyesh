@extends('panel.student.master')
@section('title','ویرایش رزومه')
@section('main')
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ویرایش رزومه</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">

            <div class="card">
                    <div class="card-header text-center justify-content-center">
                        <h4 class="card-title text-primary">ویرایش رزومه</h4>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" action="{{route('students.cv.update',$cv->id)}}" method="POST">
                            @csrf
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">نام و نام خانوادگی</label>
                                <div class="col-md-9">
                                    <input name="name" value="{{$cv->name}}" type="text" class="form-control">
                                    @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <label class="col-md-3 form-label" for="example-email">جنسیت</label>
                                <div class="col-md-9">
                                    <select class="form-control form-select" name="sex" id="">
                                        <option value="male">مرد</option>
                                        <option value="female">زن</option>
                                    </select>
                                    @error('sex') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <label class="col-md-3 form-label" for="example-email">وضعیت تاهل</label>
                                <div class="col-md-9">
                                    <select class="form-control form-select" name="marital" id="">
                                        <option value="single">مجرد</option>
                                        <option value="married">متاهل</option>
                                    </select>
                                    @error('marital') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <label class="col-md-3 form-label" for="example-email">وضعیت نظام وظیفه</label>
                                <div class="col-md-9">
                                    <select class="form-control form-select" name="military" id="">
                                        <option value="done">انجام شده</option>
                                        <option value="doing">در حال خدمت</option>
                                        <option value="exempt">معاف</option>
                                    </select>
                                    @error('military') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">شهر محل سکونت</label>
                                <div class="col-md-9">
                                    <input name="city" value="{{$cv->city}}" type="text" class="form-control">
                                    @error('city') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">تاریخ تولد</label>
                                <div class="col-md-9">
                                    <input name="birth" disabled value="{{verta($cv->birth)->formatDate()}}" id="birth" type="text" class="form-control">
                                    @error('birth') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">حقوق درخواستی</label>
                                <div class="col-md-9">
                                    <input name="salary" value="{{$cv->minimum_salary}}" type="text" class="form-control">
                                    @error('salary') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">گروه شغلی مورد علاقه</label>
                                <div class="col-md-9">
                                    <select class="form-control form-select" name="fave-job" id="">
                                        @foreach($job_groups as $group)
                                            <option value="{{$group->id}}">{{$group->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('fave-job') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>

                            <div class=" row mb-0 justify-content-center ">
                                <div class="col justify-content-center text-center ">
                                    <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $('#birth').persianDatepicker({
            altField: '#birth',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
    </script>
@endsection
