@extends('panel.student.master')
@section('title','لیست پروژه‌ها')
@section('main')
    <div class="page-header">
        <h1 class="page-title">فهرست پروژه ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد</a></li>
                <li class="breadcrumb-item active" aria-current="page">فهرست پروژه ها</li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">

            </div>

            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="">
                        <a class="btn btn-success" href="{{route('std.projects.create')}}">افزودن پروژه جدید</a>
                        <a class="btn btn-primary" href="{{route('std.project-cat.create')}}">افزودن دسته بندی جدید</a>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($projects) > 0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th class="text-center">عنوان</th>
                                <th  class="text-center"> درخواست دهنده</th>
                                <th  class="text-center">بودجه </th>
                                <th  class="text-center">توضیحات </th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr>
                                    <td class="text-nowrap align-middle text-center">{{$project->title}}</td>
                                    <td class="text-nowrap align-middle text-center">{{$project->owner->name." ".$project->owner->family}}</td>
                                    <td class="text-nowrap align-middle text-center"><span>{{$project->budget}}</span></td>
                                    <td class="text-nowrap align-middle text-center">{{Str::limit($project->description,'20','...')}}</td>
                                    <td class="text-center align-middle text-center">
                                        <div class="btn-group align-top">
                                            <a href="{{route('projects.single',[$project->id,$project->title])}}" class="btn btn-sm btn-primary badge" type="button">مشاهده </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$projects->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
