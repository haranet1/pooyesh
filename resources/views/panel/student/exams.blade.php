@extends('panel.student.master')


@section('title','آزمون ها')

@section('main')
    <div class="page-header">
        <h1 class="page-title">فهرست آزمون ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد</a></li>
                <li class="breadcrumb-item active" aria-current="page">فهرست آزمون ها</li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group mb-5">
                <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <h2 class="card-title">1 - 30 از 546 آزمون </h2>
                    <div class="page-options ms-auto">
                        <select class="form-control select2 w-100">
                            <option value="asc">آخرین</option>
                            <option value="desc">قدیمی ترین</option>
                        </select>
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>عنوان </th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-nowrap align-middle">عنوان آزمون</td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="#" class="btn btn-sm btn-primary badge" type="button">مشاهده </a>
                                    </div>
                                </td>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="mb-5">
                <ul class="pagination float-end">
                    <li class="page-item page-prev disabled">
                        <a class="page-link" href="javascript:void(0)" tabindex="-1">قبلی</a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">4</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">5</a></li>
                    <li class="page-item page-next">
                        <a class="page-link" href="javascript:void(0)">بعدی</a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
@endsection
