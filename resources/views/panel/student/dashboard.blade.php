@extends('panel.student.master')

@section('title')
    داشبورد 
    @if(Auth::user()->hasRole('student'))
        دانشجو 
    @else
        کارجو 
    @endif

@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/dashbord-swicher.css')}}">
@endsection
@section('main')
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>

        

        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد اصلی</a></li>
            </ol>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    {{Session::pull('error')}}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">تعداد شرکت ها</h6>
                                    <h2 class="mb-0 number-font">{{$companies}} </h2>
                                    <h6>شرکت</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img style="width: 70px" class="img-fluid" src="{{asset('assets/images/icons8-company-100.png')}}"
                                             alt="company">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 style="white-space: nowrap" class="">تعداد موقعیت های شغلی</h6>
                                    <h2 class="mb-0 number-font">{{$job_position}} </h2>
                                    <h6>موقعیت شغلی</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img style="width: 70px" class="img-fluid" src="{{asset('assets/images/icons8-idea.svg')}}" alt="idea">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 style="white-space: nowrap" class="">تعداد موقعیت های کارآموزی</h6>
                                    <h2 class="mb-0 number-font">{{$intern}}</h2>
                                    <h6>موقعیت کارآموزی</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img class="img-fluid" style="width: 70px" src="{{asset('assets/images/icons8-project-100.png')}}" alt="project">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xl-3">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="mt-2">
                                    <h6 class="">تعداد کارجویان</h6>
                                    <h2 class="mb-0 number-font">{{$students}} </h2>
                                    <h6>کارجو</h6>
                                </div>
                                <div class="ms-auto">
                                    <div class="chart-wrapper mt-1">
                                        <img style="width: 70px" class="img-fluid" src="{{asset('assets/images/icons8-std-96.png')}}"
                                             alt="student">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($events->count() > 0)
        <div class="row justify-content-start">
            <div class="col-lg-12 col-md-12 my-2  col-sm-12 col-xl-12" >
                {{-- <div id="pxp-blog-featured-posts-carousel"
                    class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-inner justify-content-center py-2" style="border:2px solid #52c679;border-radius: 10px">
                        @foreach($events as $event)
                            <div class="carousel-item justify-content-center " data-bs-interval="10000">
                                <a class="d-flex justify-content-center text-center" href="{{route('company.sadra')}}">
                                    <img style="max-height:220px;max-width: 727px;"
                                        src="{{ asset($event->banner->path) }}"
                                        class="d-block w-100 img-fluid " alt="banner">
                                </a>
                            </div>
                            @php($flag = false)
                        @endforeach
                    </div>
                </div> --}}
                @foreach ($events as $event)
                    @if (authUserCanRegisterInEvent($event))
                        @if ($event->infos->count() > 0)
                            <div class="card">
                                <div class="card-body">
                                    <div class=""  style="overflow: hidden; max-height: 400px" >
                                        <a href="{{ route('lets.register.event' , $event->id) }}">
                                            <img src="{{ asset($event->banner->path) }}" class="w-100" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card text-white bg-info overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        <i class="fa fa-comment"></i>
                        فرم ها - آیین نامه ها
                    </div>
                    <div class="card-toolbar">
                        <a class="btn btn-light "
                           data-bs-target="#modal-files"
                           data-bs-toggle="modal"
                           href="javascript:void(0)">
                            <i class="fa fa-eye"></i> مشاهده</a>
                    </div>
                </div>
                <div class="card-body">
                    <p class="mb-5"> فرم ها و آیین نامه های مورد نیاز خود را از این قسمت مشاهده و دریافت نمایید.</p>

                    @if(count($files)> 0)
                        @foreach($files as $file)
                            <a href="{{asset($file->path)}}">
                                <h4 class="text-light border">
                                    <i class="fa fa-caret-left"></i>
                                    {{$file->name}}
                                </h4>
                            </a>
                        @endforeach
                    @endif
                </div>
                <div class="modal fade " id="modal-files">
                    <div
                        class="modal-dialog" role="document">
                        <div class="modal-content modal-content-demo">
                            <div class="modal-header">
                                <h6 class="modal-title text-primary">لیست آیین نامه ها و فرم ها</h6>
                            </div>
                            <div class="modal-body">
                                @if(count($files)> 0)
                                    <ol class="list-group">
                                        @foreach($files as $file)
                                            <li class="list-group-item my1">
                                                <a href="{{asset($file->path)}}"><h6 class="m-0">{{$file->name}}</h6>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ol>
                                @endif
                            </div>
                            <div
                                class="modal-footer">
                                <button
                                    class="btn btn-light"
                                    data-bs-dismiss="modal">
                                    بستن
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card text-white bg-danger overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        <i class="fe fe-headphones"></i>
                        درخواست های پشتیبانی
                    </div>
                    <div class="card-toolbar">
                        <a class="btn btn-light" href="{{route('students.tickets')}}"><i class="fe fe-headphones"></i>
                            درخواست های ارسالی</a>
                    </div>
                </div>
                <div class="card-body">
                    <p class="mb-5"> درخواست های پشتیبانی خود را از این قسمت مشاهده نمایید.
                    </p>

                    @if(count($tickets)> 0)
                        @foreach($tickets as $ticket)
                            <div class="d-flex border-bottom pb-2 mt-2">
                                <div class="me-3 notifyimg   ">
                                    <i class="fe fe-mail"></i>
                                </div>
                                <div class="mt-1 wd-80p">
                                    <h5 class=" text-light mb-1">
                                        موضوع:
                                        {{$ticket->subject}}
                                    </h5>
                                    <span
                                        class="notification-subtext text-light">{{verta($ticket->created_at)->formatDifference()}}</span>
                                    @if ($ticket->receiver_id)
                                        <span>گیرنده:</span>
                                        <span
                                            class="notification-subtext text-light"> {{getUserNameByID($ticket->receiver_id)}}  </span>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card   overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        اطلاعیه ها
                    </div>
                    <div class="card-toolbar">
                    </div>
                </div>
                <div class="card-body d-flex flex-column p-2" style="height: 400px">
                    <div class="card-scroll scroll-auto p-5 pr-6 overflow-auto">
                        @if(count($announcements) > 0)
                            <ol class="list-group">
                                @foreach($announcements as $announce)

                                    <li class="list-group-item my-1">
                                        <i class="fa fa-caret-left"></i>
                                        <a data-bs-target="#modal-announce-{{$announce->id}}"
                                           data-bs-toggle="modal"
                                           href="javascript:void(0)">
                                            {{$announce->title}}
                                        </a>

                                    </li>
                                    <div class="modal fade" id="modal-announce-{{$announce->id}}">
                                        <div
                                            class="modal-dialog" role="document">
                                            <div class="modal-content modal-content-demo">
                                                <div class="modal-header">
                                                    <h6 class="modal-title text-primary">متن
                                                        اطلاعیه: {{$announce->title}}</h6>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        {{$announce->content}}
                                                    </p>
                                                    @if($announce->file)
                                                        <a download href="{{asset($announce->file->path)}}">دانلود
                                                            فایل</a>
                                                    @endif
                                                </div>
                                                <div
                                                    class="modal-footer">
                                                    <button
                                                        class="btn btn-light"
                                                        data-bs-dismiss="modal">
                                                        بستن
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </ol>
                        @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="card   overflow-hidden">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        اخبار
                    </div>
                    <div class="card-toolbar">
                    </div>
                </div>
                <div class="card-body d-flex flex-column p-2" style="height: 400px">
                    <div class="card-scroll scroll-auto p-5 pr-6 overflow-auto">
                        @if(count($news) > 0)
                            <ol class="list-group">
                                @foreach($news as $news_item)
                                    <li class="list-group-item my-1">
                                        <i class="fa fa-caret-left"></i>
                                        {{$news_item->title}}
                                    </li>
                                @endforeach
                            </ol>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(\Illuminate\Support\Facades\Auth::user()->hasRole('student'))
        <h3>طرح پویش</h3>
        <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">

            <div class="col-md-3 col-xl-3">
                <a href="{{route('std.companies-list')}}">
                    <div class="card text-white bg-primary">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3">
                                شرکت های متقاضی
                            </h4>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-xl-3">
                <a href="{{route('std.coo-requests-list')}}">
                    <div class="card text-white bg-primary">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3">درخواست های ثبت شده</h4>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-xl-3">
                <a href="{{route('std.pooyesh-requests-list')}}">
                    <div class="card text-white bg-primary">
                        <div class="card-body text-center">
                            <h4 class="card-title border-bottom py-3">درخواست های من</h4>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-6 col-md-6"><h3>استخدام</h3></div>
        <div class="col-6 col-md-6 d-flex align-items-center justify-content-end">
            @if (Auth::user()->IsApplicant())
                <h4 id="open-to-work" class="my-0 mx-2 p-2 text-white rounded-3 <?= Auth::user()->groupable->open_to_work ? 'bg-success': 'bg-dark'?>">آماده به کار</h4>
                <a id="swicher1" type="button" href="javascript:void(0)" onclick="set_open_to_work(this.id)" class="btn btn-toggle <?= Auth::user()->groupable->open_to_work ? 'active': ''?> ms-2" data-toggle="button" style="margin-left:4rem;" aria-pressed="true" autocomplete="off">
                    <div class="handle"></div>
                </a>
            @endif
            
        </div>
        
        
    </div>
    
    <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">

        <div class="col-md-4 col-xl-4">
            <a href="{{route('std.hire-requests-list.sent')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">درخواست های ثبت شده</h4>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-4 col-xl-4">
            <a href="{{route('std.hire-requests-list.received')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">درخواست های من</h4>
                    </div>
                </div>
            </a>
        </div>

        {{-- <div class="col-md-4 col-xl-4">
            <a href="{{route('std.projects.create')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">تعریف پروژه</h4>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-4 col-xl-4">
            <a href="{{route('std.projects.list')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">نمایش پروژه ها</h4>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-4 col-xl-4">
            <a href="{{route('std.idea.create')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">ارائه ایده و ایجاد استارتاپ</h4>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-4 col-xl-4">
            <a href="{{route('std.idea.index')}}">
                <div class="card text-white bg-success">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">لیست ایده ها</h4>
                    </div>
                </div>
            </a>
        </div>
    </div> --}}

    <h3>خدمات ویژه</h3>
    <div class="row py-2 mb-4" style="border-top: 1px #b0b0b0 solid">

        <div class="col-md-3 col-xl-3">
            <a href="{{route('std.exams')}}">
                <div class="card text-white bg-warning">
                    <div class="card-body text-center">
                        <h4 class="card-title border-bottom py-3">
                            شخصیت شناسی شغلی
                        </h4>
                    </div>
                </div>
            </a>
        </div>

    </div>

@endsection


@section('script')
    <script>

        function set_open_to_work(id){

            const element = document.getElementById(id);
            var lable = document.getElementById('open-to-work');
            var status = 1;
            if (element.classList.contains('active')) {
                status = 0;
                element.classList.remove('active');
                lable.classList.remove('bg-success');
                lable.classList.add('bg-dark');
            } else {
                element.classList.add('active');
                lable.classList.remove('bg-dark');
                lable.classList.add('bg-success');
            }
            console.log(status);

            $.ajax({
                headers:{
                    'X-CSRF-Token':"{{ csrf_token() }}"
                },
                type: "post",
                url: "{{route('students.ajax')}}",
                data: {
                    do: 'set-open-work-status',
                    status: status
                },
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    if (response == 0) {
                        element.classList.remove('active');
                        lable.classList.remove('bg-success');
                        lable.classList.add('bg-dark');
                    } else {
                        element.classList.add('active');
                        lable.classList.remove('bg-dark');
                        lable.classList.add('bg-success');
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });

        }
    </script>
    <script src="{{ asset('assets/panel/js/verify-email.js') }}"></script>
@endsection



  
  
  
