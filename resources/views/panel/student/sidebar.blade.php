@php
    $cvs = [
        Route::is('academic.info.cv'),
        Route::is('skills.cv'),
        Route::is('knowledgs.cv'),
        Route::is('abilities.cv'),
        Route::is('technology.skills.cv'),
        Route::is('work.styles.cv'),
        Route::is('lang.cv'),
        Route::is('work.experiences.cv'),
        Route::is('socials.cv'),
        Route::is('certificates.cv')
    ];
    $pooyeshRoutes = [
        Route::is('std.companies-list'),
        Route::is('std.sent.intern.req.job'),
        Route::is('std.received.intern.req.job'),
    ];
    $hireRoutes = [
        Route::is('std.hire-requests-list.sent'),
        Route::is('std.hire-requests-list.received')
    ];
    $specialRoutes = [
        Route::is('std.exams','mbti.index','mbti.takeTest','mbti.takeTest','enneagram.index','enneagram.takeTest','enneagram.step2','enneagram.test.result'),
        Route::is('exams.result'),
    ];
    $eventRoutes = [
        Route::is('list.event'),
        Route::is('registred.event'),
    ];
    $supRoutes = [
        Route::is('compose.sup.ticket'),
        Route::is('inbox.ticket'),
        Route::is('std.company_ticket.index')
    ];
@endphp
<ul class="side-menu">
    <li class="sub-category">
        <h3>اصلی</h3>
    </li>
    <li class="slide">
        <a class="side-menu__item has-link {{Route::is('std.panel')?'active':''}}" data-bs-toggle="slide" href="{{route('std.panel')}}">
            {{-- <i class="side-menu__icon fe fe-home"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/dashboard.svg')}}" alt="آیکون داشبورد">
            <span class="side-menu__label">داشبورد</span>
        </a>
    </li>
    <li class="slide ">
        <a class="side-menu__item has-link" data-bs-toggle="slide" href="{{route('home')}}">
            {{-- <i class="side-menu__icon fe fe-home"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/home.svg')}}" alt="صفحه اصلی">
            <span class="side-menu__label">صفحه اصلی سایت</span>
        </a>
    </li>
    @auth
        <li class="slide ">
            <a class="side-menu__item has-link" data-bs-toggle="slide" href="{{route('candidate.single', Auth::id())}}">
                {{-- <i class="side-menu__icon fe fe-home"></i> --}}
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/profile.svg')}}" alt="صفحه اختصاصی">
                <span class="side-menu__label">صفحه اختصاصی</span>
            </a>
        </li>
    @endauth
    
{{-- @if(Auth::check() && Auth::user()->hasRole('student'))
    <li class="slide">
        <a class="side-menu__item has-link {{Route::is('std.signature.create')?'active':''}}" data-bs-toggle="slide" href="{{route('std.signature.create')}}">
            <i class="side-menu__icon fe fe-slack"></i>
            <span class="side-menu__label">آپلود امضا</span>
        </a>
    </li>
@endif --}}
    <li class="sub-category">
        <h3> امکانات</h3>
    </li>


    @if (Auth::check())
        <li class="slide cv-menu
        @foreach ($cvs as $cv)
            @if (Route::currentRouteName() == $cv)
                is-expanded
            @endif
        @endforeach
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                {{-- <i class="side-menu__icon fe fe-cpu"></i> --}}
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/cv.svg')}}" alt="رزومه">
                <span class="side-menu__label">تکمیل رزومه</span><i class="angle fe fe-chevron-right"></i></a>
            <ul class="slide-menu" style="display: none;">
                <li class="side-menu-label1"><a href="javascript:void(0)">تکمیل رزومه</a></li>

                <li><a href="{{ route('academic.info.cv') }}" class="slide-item {{ Route::is('academic.info.cv')? 'active': '' }}">اطلاعات آکادمیک</a></li>
                <li><a href="{{ route('skills.cv') }}" class="slide-item {{ Route::is('skills.cv')? 'active': '' }}">مهارت ها</a></li>
                <li><a href="{{ route('knowledgs.cv') }}" class="slide-item {{ Route::is('knowledgs.cv')? 'active': '' }}">دانش</a></li>
                <li><a href="{{ route('abilities.cv') }}" class="slide-item {{ Route::is('abilities.cv')? 'active': '' }}">توانایی ها</a></li>
                <li><a href="{{ route('technology.skills.cv') }}" class="slide-item {{ Route::is('technology.skills.cv')? 'active': '' }}">مهارت ها فنی</a></li>
                <li><a href="{{ route('work.styles.cv') }}" class="slide-item {{ Route::is('work.styles.cv')? 'active': '' }}">ویژگی های کاری</a></li>
                <li><a href="{{ route('lang.cv') }}" class="slide-item {{ Route::is('lang.cv')? 'active': '' }}">زبان های خارجی</a></li>
                <li><a href="{{ route('work.experiences.cv') }}" class="slide-item {{ Route::is('work.experiences.cv')? 'active': '' }}">سابقه کاری</a></li>
                <li><a href="{{ route('socials.cv') }}" class="slide-item {{ Route::is('socials.cv')? 'active': '' }}">شبکه های اجتماعی</a></li>
                <li><a href="{{ route('certificates.cv') }}" class="slide-item {{ Route::is('certificates.cv')? 'active': '' }}">دوره ها و گواهینامه ها</a></li>
                
            </ul>
        </li>
    @endif


    @if(Auth::check() && Auth::user()->hasRole('student'))
        <li class="slide pooyesh-menu
        @foreach ($pooyeshRoutes as $pooyeshRoute)
            @if (Route::currentRouteName() == $pooyeshRoute)
                is-expanded
            @endif
        @endforeach
        ">
            <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/intern.svg')}}" alt="استخدام و سرمایه گذاری">
                <span class="side-menu__label">طرح پویش <span style="font-size: 10px">(اشتغال همراه با تحصیل)</span></span>
                <i class="angle fe fe-chevron-right"></i>
            </a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="javascript:void(0)">پویش</a></li>
                <li><a href="{{route('std.companies-list')}}" class="slide-item {{Route::is('std.companies-list')?'active':''}}"> شرکت های متقاضی</a></li>
                <li><a href="{{route('std.sent.intern.req.job')}}" class="slide-item {{Route::is('std.sent.intern.req.job')?'active':''}}"> درخواست های ثبت شده</a></li>
                <li><a href="{{route('std.received.intern.req.job')}}" class="slide-item {{Route::is('std.received.intern.req.job')?'active':''}}"> درخواست های همکاری</a></li>

            </ul>
        </li>
    @endif
    <li class="slide hire-menu 
    @foreach ($hireRoutes as $hireRoute)
        @if (Route::currentRouteName() == $hireRoute)
            is-expanded
        @endif
    @endforeach
    ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/dile2.svg')}}" alt="آیکون شغل">
            <span class="side-menu__label">استخدام</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">استخدام</a></li>
            <li><a href="{{route('std.sent.hire.req.job')}}" class="slide-item {{Route::is('std.sent.hire.req.job')?'active':''}}"> درخواست های ثبت شده </a></li>
            <li><a href="{{route('std.received.hire.req.job')}}" class="slide-item {{Route::is('std.received.hire.req.job')?'active':''}}"> درخواست های همکاری </a></li>
            {{-- <li><a href="{{route('std.projects.create')}}" class="slide-item {{Route::is('std.projects.create')?'active':''}}">تعریف پروژه</a></li>
            <li><a href="{{route('std.projects.list')}}" class="slide-item {{Route::is('std.projects.list')?'active':''}}">نمایش پروژه ها</a></li>
            <li><a href="{{route('std.idea.create')}}" class="slide-item {{Route::is('std.idea.create')?'active':''}}"> ارائه ایده و ایجاد استارتاپ  </a></li> --}}
            {{-- <li><a href="{{route('std.idea.index')}}" class="slide-item {{Route::is('std.idea.index')?'active':''}}"> لیست ایده ها  </a></li> --}}
            <li><a href="{{route('jobs.list')}}" class="slide-item">جستجوی موقعیت شغلی</a></li>
        </ul>
    </li>

    <li class="slide exhibition
    @foreach ($eventRoutes as $eventRoute)
        @if (Route::currentRouteName() == $eventRoute)
            is-expanded
        @endif
    @endforeach
    ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/Exhibition.svg')}}" alt="آیکون همایش">

            <span class="side-menu__label">رویداد ها</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">رویداد ها</a></li>

            <li><a href="{{route('list.event')}}" class="slide-item {{Route::is('list.event')?'active':''}}">شرکت در رویداد </a></li>
            <li><a href="{{route('registred.event')}}" class="slide-item {{Route::is('registred.event')?'active':''}}">رویداد های ثبت نام شده</a></li>
        </ul>
    </li>

    <li class="slide special-menu 
    @foreach ($specialRoutes as $specialRoute)
        @if (Route::currentRouteName() == $specialRoute)
            is-expanded
        @endif
    @endforeach
    ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/test.svg')}}" alt="استخدام">
            <span class="side-menu__label">شخصیت شناسی</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">شخصیت شناسی</a></li>
            <li><a href="{{route('std.exams')}}" class="slide-item {{Route::is('std.exams','mbti.index','mbti.takeTest','mbti.takeTest','enneagram.index','enneagram.takeTest','enneagram.step2','enneagram.test.result')?'active':''}}">آزمون ها</a></li>
            <li><a href="{{route('exams.result')}}" class="slide-item {{Route::is('exams.result')?'active':''}}">نتیجه تست ها</a></li>
        </ul>
    </li>

   {{-- <li class="slide edu-menu">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            <i class="side-menu__icon fe fe-slack"></i>
            <span class="side-menu__label"> آموزش</span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)">آموزش </a></li>
            <li><a href="{{route('std.tutorial.list')}}" class="slide-item">مشاهده کلیه دوره های مهارتی</a></li>
            <li><a href="#" class="slide-item">درخواست دوره</a></li>
        </ul>
    </li>--}}

    <li class="slide supp-menu
    @foreach ($supRoutes as $supRoute)
        @if (Route::currentRouteName() == $supRoute)
            is-expanded
        @endif
    @endforeach
    ">
        <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
            {{-- <i class="side-menu__icon fe fe-slack"></i> --}}
            <img class="me-3" style="width: 18px" src="{{asset('assets/panel/images/icon/SVG/support.svg')}}" alt="آیکون پشتیبانی">
            <span class="side-menu__label">  پشتیبانی </span>
            <i class="angle fe fe-chevron-right"></i>
        </a>
        <ul class="slide-menu">
            <li class="side-menu-label1"><a href="javascript:void(0)"> پشتیبانی </a></li>
            <li><a href="{{route('compose.sup.ticket')}}" class="slide-item {{Route::is('compose.sup.ticket')?'active':''}}">ارسال تیکت</a></li>
            <li><a href="{{route('inbox.ticket')}}" class="slide-item {{Route::is('inbox.ticket')?'active':''}}">لیست تیک ها</a></li>
            <li><a href="{{route('std.company_ticket.index')}}" class="slide-item {{Route::is('std.company_ticket.index')?'active':''}}"> تیکت های ارسالی به شرکت ها </a></li>
        </ul>
    </li>
</ul>
