@extends('panel.student.master')

@section('title','لیست پیام ها')

@section('main')
    <div class="page-header">
        <h1 class="page-title">فهرست پیام ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد</a></li>
                <li class="breadcrumb-item active" aria-current="page">فهرست پیام ها</li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            @if(Session::has('error'))
                <div class="alert alert-danger text-center">
                    {{Session::pull('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success text-center">
                    {{Session::pull('success')}}
                </div>
            @endif
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="page-options ms-auto">
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($tickets)>0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>موضوع </th>
                                <th>وضعیت</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tickets as $ticket)
                            <tr>
                                <td class="text-nowrap align-middle">{{$ticket->subject}}</td>
                                <td class="text-nowrap align-middle">
                                    @if($ticket->seen)
                                    <span class="badge bg-success">خوانده شده </span>
                                    @else
                                        <span class="badge bg-warning">خوانده نشده </span>
                                    @endif
                                </td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a class="btn btn-sm btn-primary badge" data-bs-target="#modal-ticket-{{$ticket->id}}" data-bs-toggle="modal" href="javascript:void(0)">مشاهده پیام</a>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade" id="modal-ticket-{{$ticket->id}}">
                                <div
                                    class="modal-dialog" role="document">
                                    <div class="modal-content modal-content-demo">
                                        <div class="modal-header">
                                            <div class="row">
                                                <h6 class="modal-title text-primary">متن پیام: </h6>
                                                <p class="modal-title">
                                                    {{ $ticket->content}}
                                                </p>
                                            </div>
                                            
                                        </div>
                                        <div class="modal-body" style="max-height: 450px !important; overflow-y: scroll !important">
                                            
                                            @if($ticket->replies)
                                               <h6>پاسخ دریافتی:</h6>
                                                @foreach($ticket->replies as $reply)
                                                    <div class="row justify-content-start mb-2">
                                                        <div class="col-10 border bg-info text-white" style="border-radius: 20px">
                                                            <div class="row justify-content-start">
                                                                <p class="text-dark">{{$ticket->sender->name." ".$ticket->sender->family}}</p>
                                                            </div>
                                                            <div class="row mt-2">
                                                                <div class="col-10">
                                                                    <p>{{$reply->content}} </p>
                                                                </div>
                                                                
                                                                <span class="text-end">{{verta($reply->created_at)->formatDate()}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="modal-footer justify-contant-start">
                                            <h6>ارسال پاسخ:</h6>
                                            <form class="py-1" action="{{route('students.ticket.reply')}}" method="POST">
                                                @csrf
                                                <input type="hidden" name="parent_id" value="{{$ticket->id}}">
                                                <input type="text" class="form-control" name="content">
                                                <button class="btn mt-2 btn-success" type="submit">ارسال</button>
                                            </form>
                                            <button
                                                class="btn btn-light"
                                                data-bs-dismiss="modal">
                                                بستن
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$tickets->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
