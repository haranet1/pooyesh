<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0,'>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="author" content="haranet.ir">
    <base href="{{route('std.panel')}}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    @yield('meta')


    <link  rel="shortcut icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}"/>
    <link rel="apple-touch-icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}" type="image/x-icon"/>


    <title>{{ env('APP_NAME') }}| @yield('title')</title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link  href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.rtl.min.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/panel/css/dark-style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/panel/css/transparent-style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/panel/css/skin-modes.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/icons.css')}}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{asset('assets/panel/colors/color1.css')}}" />

    <link rel="stylesheet" id="fonts" href="{{asset('assets/panel/fonts/styles-fa-num/iran-yekan.css')}}">
    <link href="{{asset('assets/panel/css/rtl.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('assets/panel/plugins/datepicker/dist/css/persian-datepicker.css')}}"/>

    @yield('css')
</head>
<body class="app sidebar-mini rtl light-mode">


<div id="global-loader">
    <img src="{{asset('assets/panel/images/loader.svg')}}" class="loader-img" alt="Loader">
</div>


<div class="page">
    <div class="page-main">

        <div class="app-header header sticky">
            <div class="container-fluid main-container">
                <div class="d-flex">
                    <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-bs-toggle="sidebar" href="javascript:void(0)"></a>

                    <a class="logo-horizontal " href="#">
                        <img src="{{asset('assets/panel/images/brand/jobist/white-text-logo.svg')}}" style="width: 120px" class="header-brand-img desktop-logo" alt="logo">
                        <img src="{{asset('assets/panel/images/brand/jobist/text-logo.svg')}}" style="width: 120px" class="header-brand-img light-logo1" alt="logo">
                    </a>

                    {{--<div class="main-header-center ms-3 d-none d-lg-block">
                        <input type="text" class="form-control" id="typehead" placeholder="جستجو برای نتایج ..." autocomplete="off">
                        <button class="btn px-0 pt-2"><i class="fe fe-search" aria-hidden="true"></i></button>
                    </div>--}}
                    <div class="d-flex order-lg-2 ms-auto header-right-icons">
                        <div class="dropdown d-none">
                            <a href="javascript:void(0)" class="nav-link icon" data-bs-toggle="dropdown">
                                <i class="fe fe-search"></i>
                            </a>
                            <div class="dropdown-menu header-search dropdown-menu-start">
                                <div class="input-group w-100 p-2">
                                    <input type="text" class="form-control" placeholder="جستجو ...">
                                    <div class="input-group-text btn btn-primary">
                                        <i class="fe fe-search" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="navbar-toggler navresponsive-toggler d-lg-none ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon fe fe-more-vertical"></span>
                        </button>
                        <div class="navbar navbar-collapse responsive-navbar p-0">
                            <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                                <div class="d-flex order-lg-2">
                                    {{--<div class="dropdown d-lg-none d-flex">
                                        <a href="javascript:void(0)" class="nav-link icon" data-bs-toggle="dropdown">
                                            <i class="fe fe-search"></i>
                                        </a>
                                        <div class="dropdown-menu header-search dropdown-menu-start">
                                            <div class="input-group w-100 p-2">
                                                <input type="text" class="form-control" placeholder="جستجو ...">
                                                <div class="input-group-text btn btn-primary">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--}}


                                    <div class="d-flex country">
                                        <a class="nav-link icon theme-layout nav-link-bg layout-setting">
                                            <span class="dark-layout"><i class="fe fe-moon"></i></span>
                                            <span class="light-layout"><i class="fe fe-sun"></i></span>
                                        </a>
                                    </div>



                                    <div class="dropdown d-flex">
                                        <a class="nav-link icon full-screen-link nav-link-bg">
                                            <i class="fe fe-minimize fullscreen-button"></i>
                                        </a>
                                    </div>

                                    @auth
                                        <div class="dropdown  d-flex notifications">
                                            <a class="nav-link icon" data-bs-toggle="dropdown">
                                                <i class="fe fe-bell"></i>
                                                @if(\Illuminate\Support\Facades\Auth::user()->unreadNotifications()->count() > 0 )
                                                    @php($hasNotify = true)
                                                <span class=" pulse"></span>
                                                    @endif
                                            </a>
                                            <div class="dropdown-menu  dropdown-menu-end dropdown-menu-arrow">
                                                <div class="drop-heading border-bottom">
                                                    <div class="d-flex">
                                                        <h6 class="mt-1 mb-0 fs-16 fw-semibold text-dark">اعلانات
                                                        </h6>
                                                    </div>
                                                </div>
                                                <div class="notifications-menu" style="overflow-y: scroll">
                                                    @if(isset($hasNotify))
                                                    @foreach(\Illuminate\Support\Facades\Auth::user()->unreadNotifications as $notification)
                                                        @isset($notification->data['link'])
                                                            <a href="{{ $notification->data['link'] }}" class="p-0">
                                                                <div class="dropdown-item d-flex" >
                                                                    <div class="me-3 notifyimg  bg-primary brround box-shadow-primary">
                                                                        <i class="fe fe-mail"></i>
                                                                    </div>
                                                                    <div class="mt-1 wd-80p">

                                                                        <h5 class="notification-label mb-1">
                                                                            {{$notification->data['message']}}
                                                                        </h5>

                                                                        <span class="notification-subtext">{{verta($notification->created_at)->formatDifference()}}</span>
                                                                        <a href="javascript:void(0)" onclick="MarkAsRead(this,'{{$notification->id}}')">
                                                                            <span class="btn btn-outline-success notification-subtext p-1">متوجه شدم</span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        @else
                                                            <div class="dropdown-item d-flex" >
                                                                <div class="me-3 notifyimg  bg-primary brround box-shadow-primary">
                                                                    <i class="fe fe-mail"></i>
                                                                </div>
                                                                <div class="mt-1 wd-80p">

                                                                    @isset($notification->data['message'])
                                                                    <h5 class="notification-label mb-1">
                                                                        {{$notification->data['message']}}
                                                                    </h5>
                                                                    @endisset

                                                                    <span class="notification-subtext">{{verta($notification->created_at)->formatDifference()}}</span>
                                                                    <a href="javascript:void(0)" onclick="MarkAsRead(this,'{{$notification->id}}')">
                                                                        <span class="btn btn-outline-success notification-subtext p-1">متوجه شدم</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endisset
                                                    @endforeach
                                                    @else
                                                        <h5 class="notification-label text-danger m-4">
                                                        اعلان جدیدی وجود ندارد
                                                        </h5>
                                                    @endif

                                                </div>
                                                <div class="dropdown-divider m-0"></div>
                                            {{-- <a href="notify-list.html" class="dropdown-item text-center p-3 text-muted">مشاهده همه
                                                    اعلان</a>--}}
                                            </div>
                                        </div>

                                        <div class="dropdown  d-flex message">
                                            <a class="nav-link icon text-center" data-bs-toggle="dropdown">
                                                <i class="fe fe-message-square"></i>
                                                @if(\Illuminate\Support\Facades\Auth::user()->getUnreadTickets()->count() > 0)
                                                    <span class="pulse-danger"></span>
                                                @endif
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                                <div class="drop-heading border-bottom">
                                                    <div class="d-flex">
                                                        <h6 class="mt-1 mb-0 fs-16 fw-semibold text-dark">شما {{\Illuminate\Support\Facades\Auth::user()->getUnreadTickets()->count()}} پیام خوانده نشده دارید</h6>
                                                    </div>
                                                </div>
                                                <div class="message-menu message-menu-scroll" style="overflow-y: scroll">
                                                    @if(\Illuminate\Support\Facades\Auth::user()->getUnreadTickets()->count() > 0)
                                                        @foreach(\Illuminate\Support\Facades\Auth::user()->getUnreadTickets() as $ticket)
                                                            <a class="dropdown-item d-flex" href="{{route('index.ticket' , $ticket->id)}}">
                                                                <div class="wd-90p">
                                                                    <div class="d-flex">
                                                                        <h5 class="mb-1">{{$ticket->sender->name." ".$ticket->sender->family}}</h5>
                                                                        <small class="text-muted ms-auto text-end">
                                                                            {{verta($ticket->created_at)->formatDifference()}}  
                                                                        </small>
                                                                    </div>
                                                                    <span>{{$ticket->subject}}</span>
                                                                </div>
                                                            </a>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                <div class="dropdown-divider m-0"></div>
                                                <a href="{{route('inbox.ticket')}}" class="dropdown-item text-center p-3 text-muted">نمایش همه پیام ها</a>
                                            </div>
                                        </div>
                                    @endauth
                                    {{-- <div class="demo-icon nav-link icon">
                                        <i class="fe fe-settings fa-spin  text_primary"></i>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                        @auth
                            <div class="dropdown d-flex profile-1">
                                <a href="javascript:void(0)" data-bs-toggle="dropdown" class="nav-link leading-none d-flex">
                                    <img src="{{asset('assets/panel/images/users/std-user.svg')}}" alt="profile-user" class="avatar profile-user brround cover-image">
                                </a>
                                <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow transform-dropdown">
                                    <div class="drop-heading">
                                        <div class="text-center">
                                            <h5 class="text-dark mb-0 fs-14 fw-semibold">{{\Illuminate\Support\Facades\Auth::user()->name." ".\Illuminate\Support\Facades\Auth::user()->family}}</h5>
                                            <small class="text-muted"> </small>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <a class="dropdown-item" href="{{route('std.profile.edit')}}">
                                        <i class="dropdown-icon fe fe-lock"></i> ویرایش پروفایل
                                    </a>
                                    <a onclick="document.getElementById('logout').submit()" class="dropdown-item" href="javascript:void(0)">
                                        <i class="dropdown-icon fe fe-lock"></i> خروج
                                    </a>
                                    <form id="logout" action="{{route('logout')}}" method="POST">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        @endauth

                    </div>
                </div>
            </div>
        </div>


        <div class="sticky">
            <div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
            <div class="app-sidebar">
                <div class="side-header">
                    <a class="header-brand1" href="{{route('home')}}" aria-label="home page link">
                        <img src="{{asset('assets/panel/images/brand/jobist/white-text-logo.svg')}}" style="width: 140px" class="header-brand-img desktop-logo" alt="logo">
                        <img src="{{asset('assets/panel/images/brand/jobist/white-notext-logo.svg')}}" style="width: 140px" class="header-brand-img toggle-logo" alt="logo">
                        <img src="{{asset('assets/panel/images/brand/jobist/logo.svg')}}" style="width: 140px" class="header-brand-img light-logo" alt="logo">
                        <img src="{{asset('assets/panel/images/brand/jobist/text-logo.svg')}}" style="width: 140px" class="header-brand-img light-logo1" alt="logo">

                </div>
                <div class="main-sidemenu">
                    <div class="slide-left disabled" id="slide-left"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24"><path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z" /></svg></div>
                    @include('panel.student.sidebar')
                    <div class="slide-right" id="slide-right"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24"><path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z" /></svg></div>
                </div>
            </div>

        </div>
        
        <div class="main-content app-content mt-0">
            @yield('full_width')
            <div class="side-app">

                <div class="main-container container-fluid">
                    @auth
                        @include('alert.email-alert')
                        @if (Auth::user()->groupable_type == App\Models\StudentInfo::class || Auth::user()->groupable_type == App\Models\ApplicantInfo::class)
                            @include('alert.cv-alert')
                        @endif
                    @endauth

                    @yield('main')
                </div>
            </div>
        </div>
    </div>




    <footer class="footer">
        <div class="container">
            <div class="row align-items-center flex-row-reverse">
                <div class="col-md-12 col-sm-12 text-center">
                    حرانت ©
                </div>
            </div>
        </div>
    </footer>

</div>

<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

{{--<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>--}}
<script src="{{asset('assets/panel/js/jquery.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{asset('assets/panel/js/jquery.sparkline.min.js')}}"></script>

<script src="{{asset('assets/panel/js/sticky.js')}}"></script>

<script src="{{asset('assets/panel/js/circle-progress.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/peitychart/jquery.peity.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/peitychart/peitychart.init.js')}}"></script>

<script src="{{asset('assets/panel/plugins/sidebar/sidebar.js')}}"></script>

<script src="{{asset('assets/panel/plugins/p-scroll/perfect-scrollbar.js')}}"></script>
<script src="{{asset('assets/panel/plugins/p-scroll/pscroll.js')}}"></script>
<script src="{{asset('assets/panel/plugins/p-scroll/pscroll-1.js')}}"></script>

<script src="{{asset('assets/panel/plugins/chart/Chart.bundle.js')}}"></script>
<script src="{{asset('assets/panel/plugins/chart/rounded-barchart.js')}}"></script>
<script src="{{asset('assets/panel/plugins/chart/utils.js')}}"></script>

<script src="{{asset('assets/panel/plugins/select2/select2.full.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
<script src="{{asset('assets/panel/plugins/datatable/dataTables.responsive.min.js')}}"></script>

<script src="{{asset('assets/panel/js/apexcharts.js')}}"></script>
<script src="{{asset('assets/panel/plugins/apexchart/irregular-data-series.js')}}"></script>

<script src="{{asset('assets/panel/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{asset('assets/panel/plugins/flot/jquery.flot.fillbetween.js')}}"></script>
<script src="{{asset('assets/panel/plugins/flot/chart.flot.sampledata.js')}}"></script>
<script src="{{asset('assets/panel/plugins/flot/dashboard.sampledata.js')}}"></script>

<script src="{{asset('assets/panel/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

<script src="{{asset('assets/panel/plugins/sidemenu/sidemenu.js')}}"></script>

{{-- <script src="{{asset('assets/panel/plugins/bootstrap5-typehead/autocomplete.js')}}"></script> --}}
{{-- <script src="{{asset('assets/panel/js/typehead.js')}}"></script> --}}

{{-- <script src="{{asset('assets/panel/js/index1.js')}}"></script> --}}

<script src="{{asset('assets/panel/js/themeColors.js')}}"></script>

<script src="{{asset('assets/panel/js/custom.js')}}"></script>
<script src="{{asset('assets/panel/js/custom1.js')}}"></script>

<script src="{{asset('assets/panel/plugins/datepicker/dist/js/persian-datepicker.js')}}"></script>
<script src="{{asset('assets/panel/plugins/datepicker/assets/persian-date.min.js')}}"></script>
<script src="{{asset('assets/swal2/swal2.all.min.js')}}"></script>
{{-- ck editor --}}
<script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('assets/ckeditor/adapters/jquery.js')}}"></script>
<script src="{{asset('js/chart.js')}}"></script>

<script>
    const myCsrf = '{{csrf_token()}}';
    var config = {
        routes: {
            academicInfoStore: "{{route('std.academicinfo.store')}}",
            ajax:"{{route('students.ajax')}}",
            uploadCertificate: "{{route('std.certificate.upload')}}"
        },
        csrf:'{{csrf_token()}}'
    }
</script>

{{-- student js --}}
<script src="{{asset('assets/panel/js/student.js')}}"></script>
<script>
    var currentRoute = "{{ Route::currentRouteName() }}";
    var pooyeshMenuRoutes = ['std.companies-list','std.coo-requests-list','std.pooyesh-requests-list'];
    var hireMenuRoutes = ['std.hire-requests-list.sent','std.hire-requests-list.received','std.projects.create','std.projects.list','std.idea.create','std.idea.index'];
    var specialMenuRoutes = ['std.exams','mbti.index','mbti.takeTest','mbti.takeTest','enneagram.index','enneagram.takeTest','enneagram.step2','enneagram.test.result'];
    var eduMenuRoutes = ['edu-menu'];
    var suppMenuRoutes = ['std.ticket.new','students.tickets','std.company_ticket.index'];

    if (pooyeshMenuRoutes.includes(currentRoute)) {
        $('.slide.pooyesh-menu').addClass('is-expanded');
    }
    if (hireMenuRoutes.includes(currentRoute)) {
        $('.slide.hire-menu').addClass('is-expanded');
    }
    if (specialMenuRoutes.includes(currentRoute)) {
        $('.slide.special-menu').addClass('is-expanded');
    }
    if (eduMenuRoutes.includes(currentRoute)) {
        $('.slide.edu-menu').addClass('is-expanded');
    }
    if (suppMenuRoutes.includes(currentRoute)) {
        $('.slide.supp-menu').addClass('is-expanded');
    }
</script>

<script>
function MarkAsRead(clicked_element,notification){

    let item = clicked_element.parentNode.parentNode
    $.ajax({
        headers:{
            'X-CSRF-TOKEN':'{{csrf_token()}}'
        },
        url: '{{ route("notifications.ajax") }}',
        type: 'POST',
        data: {
            do:'mark-as-read',
            notification:notification
        },
        success: function(response) {
            item.remove()
        },
        error: function(xhr, status, error) {
            // Handle any errors that occur during the request
        }
    });
}
</script>

@yield('script')
</body>
</html>
