@extends('panel.student.master')
@section('title','ویراش ایده/استارتاپ')
@section('main')
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ویراش ایده/استارتاپ</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">ویرایش ایده / استارتاپ</h3>
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal" action="{{route('std.idea.update',$idea)}}" method="POST">
                                @csrf
                                <div class=" row mb-4">
                                    <label class="col-md-3 form-label">عنوان</label>
                                    <div class="col-md-9">
                                        <input name="title" value="{{$idea->title}}" type="text" class="form-control" >
                                        @error('title') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <label class="col-md-3 form-label" for="example-email">دسته بندی</label>
                                    <div class="col-md-9">
                                        <input name="category" value="{{$idea->category}}" type="text" class="form-control" >
                                        @error('category') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                                <div class=" row mb-4">
                                    <label class="col-md-3 form-label">مهارت های مورد نیاز</label>
                                    <div class="col-md-9">
                                        <input name="required_skills" value="{{$idea->required_skills}}" type="text" class="form-control" >
                                        @error('required_skills') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                                <div class=" row mb-4">
                                    <label class="col-md-3 form-label">بودجه</label>
                                    <div class="col-md-9">
                                        <input name="budget" value="{{$idea->budget}}" type="text" class="form-control" >
                                        @error('budget') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                                <div class=" row mb-4">
                                    <label class="col-md-3 form-label">توضیحات</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control"  name="description" id="" cols="30" rows="10">{{$idea->description}}</textarea>
                                        @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>

                                <div class=" row mb-0 justify-content-center ">
                                    <div class="col justify-content-center text-center ">
                                        <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
