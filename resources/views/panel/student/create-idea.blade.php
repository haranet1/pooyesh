@extends('panel.student.master')
@section('title','ساخت ایده/استارتاپ')
@section('main')
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> تعریف ایده/استارتاپ</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">ساخت ایده / استارتاپ</h3>
                </div>
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="tab-menu-heading tab-menu-heading-boxed">
                            <div class="tabs-menu-boxed">
                                <ul class="nav panel-tabs">
                                    <li><a href="#tab25" class="active" data-bs-toggle="tab">ایده</a></li>
                                    <li><a href="#tab26" data-bs-toggle="tab">استارتاپ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body tabs-menu-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab25">
                                    <div class="card">
                                        <div class="card-body">
                                            <form class="form-horizontal" action="{{route('std.idea.store')}}" method="POST">
                                                @csrf
                                                <input name="type" type="hidden" value="idea" class="form-control" >

                                                <div class=" row mb-4">
                                                    <label class="col-md-3 form-label">عنوان</label>
                                                    <div class="col-md-9">
                                                        <input name="title" type="text" class="form-control" >
                                                        @error('title') <span class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class="row mb-4">
                                                    <label class="col-md-3 form-label" for="example-email">دسته بندی</label>
                                                    <div class="col-md-9">
                                                        <input name="category" type="text" class="form-control" >
                                                        @error('category') <span class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class=" row mb-4">
                                                    <label class="col-md-3 form-label">مهارت های مورد نیاز</label>
                                                    <div class="col-md-9">
                                                        <input name="required_skills" type="text" class="form-control" >
                                                        @error('required_skills') <span class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class=" row mb-4">
                                                    <label class="col-md-3 form-label">نیروی کار مورد نیاز</label>
                                                    <div class="col-md-9">
                                                        <input name="required_employees" type="text" class="form-control" >
                                                        @error('required_employees') <span class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class=" row mb-4">
                                                    <label class="col-md-3 form-label">بودجه</label>
                                                    <div class="col-md-9">
                                                        <input name="budget" type="text" class="form-control" >
                                                        @error('budget') <span class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class=" row mb-4">
                                                    <label class="col-md-3 form-label">توضیحات</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="description" id="" cols="30" rows="10"></textarea>
                                                        @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>

                                                <div class=" row mb-0 justify-content-center ">
                                                    <div class="col justify-content-center text-center ">
                                                        <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab26">
                                    <form class="form-horizontal" action="{{route('std.idea.store')}}" method="POST">
                                        @csrf
                                        <input name="type" type="hidden" value="startup" class="form-control" >

                                        <div class=" row mb-4">
                                            <label class="col-md-3 form-label">عنوان</label>
                                            <div class="col-md-9">
                                                <input name="title" type="text" class="form-control" >
                                                @error('title') <span class="text-danger">{{$message}}</span> @enderror

                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <label class="col-md-3 form-label" for="example-email">دسته بندی</label>
                                            <div class="col-md-9">
                                                <input name="category" type="text" class="form-control" >
                                                @error('category') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                        <div class=" row mb-4">
                                            <label class="col-md-3 form-label">مهارت های مورد نیاز</label>
                                            <div class="col-md-9">
                                                <input name="required_skills" type="text" class="form-control" >
                                                @error('required_skills') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                        <div class=" row mb-4">
                                            <label class="col-md-3 form-label">نیروی کار مورد نیاز</label>
                                            <div class="col-md-9">
                                                <input name="required_employees" type="text" class="form-control" >
                                                @error('required_employees') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                        <div class=" row mb-4">
                                            <label class="col-md-3 form-label">بودجه</label>
                                            <div class="col-md-9">
                                                <input name="budget" type="text" class="form-control" >
                                                @error('budget') <span class="text-danger">{{$message}}</span> @enderror

                                            </div>
                                        </div>
                                        <div class=" row mb-4">
                                            <label class="col-md-3 form-label">توضیحات</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="description" id="" cols="30" rows="10"></textarea>
                                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>

                                        <div class=" row mb-0 justify-content-center ">
                                            <div class="col justify-content-center text-center ">
                                                <button type="submit" class="btn btn-success btn-lg px-5">ثبت</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
