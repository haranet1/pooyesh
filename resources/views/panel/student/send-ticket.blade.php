@extends('panel.student.master')
@section('title','ارسال تیکت')
@section('main')
    <div class="page-header">
        <h1 class="page-title"> داشبورد 
            @if(Auth::user()->hasRole('student'))
                دانشجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @else
                کارجو {{ Auth::user()->name }} {{ Auth::user()->family }}
            @endif
        </h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)"> ارسال تیکت </a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center">
        <div class="col-xl-6 col-lg-12">

            <div class="card">

                    <div class="card-header text-center justify-content-center">
                        <h4 class="card-title text-primary"> ارسال پیام به کارشناس پویش</h4>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" action="{{route('students.ticket.store')}}" method="POST">
                            @csrf
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">موضوع</label>
                                <div class="col-md-9">
                                    <input name="subject" type="text" class="form-control">
                                    @error('subject') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class=" row mb-4">
                                <label class="col-md-3 form-label">متن</label>
                                <div class="col-md-9">
                                    <textarea class="form-control " name="content" id="" cols="40" rows="10"></textarea>
                                    @error('content') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                            <div class=" row mb-0 justify-content-center ">
                                <div class="col justify-content-center text-center ">
                                    <button type="submit" class="btn btn-success btn-lg px-5">ارسال</button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $('#birth').persianDatepicker({
            altField: '#birth',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
    </script>
@endsection
