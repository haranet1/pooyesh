@extends('panel.student.master')

@section('title','لیست پیام ها')

@section('main')
    <div class="page-header">
        <h1 class="page-title">فهرست پیام ها</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد</a></li>
                <li class="breadcrumb-item active" aria-current="page">فهرست پیام ها</li>
            </ol>
        </div>
    </div>

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            <div class="input-group w-25 mb-5">
               {{-- <input type="text" class="form-control" placeholder="جستجو">
                <div class="input-group-text btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>--}}
            </div>
            <div class="card">
                <div class="card-header border-bottom-0">
                    <div class="page-options ms-auto">
                    </div>
                </div>
                <div class="e-table px-5 pb-5">
                    <div class="table-responsive table-lg">
                        @if(count($tickets)>0)
                        <table class="table border-top table-bordered mb-0">
                            <thead>
                            <tr>
                                <th>موضوع</th>
                                <th>گیرنده</th>
                                <th>وضعیت</th>
                                <th class="text-center">عملکردها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tickets as $ticket)
                            <tr>
                                <td class="text-nowrap align-middle">{{$ticket->subject}}</td>
                                <td class="text-nowrap align-middle">{{getUserNameByID($ticket->receiver_id)}}</td>
                                <td class="text-nowrap align-middle">
                                    @if($ticket->read_at)
                                    <span class="badge bg-success">خوانده شده </span>
                                    @else
                                        <span class="badge bg-warning">خوانده نشده </span>
                                    @endif
                                </td>
                                <td class="text-center align-middle">
                                    <div class="btn-group align-top">
                                        <a href="javascript:void(0)" data-bs-target="#modal-{{$ticket->id}}" data-bs-toggle="modal" class="btn btn-sm btn-primary badge" type="button">مشاهده پیام</a>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade" id="modal-{{$ticket->id}}">
                                <div
                                    class="modal-dialog" role="document">
                                    <div class="modal-content modal-content-demo">
                                        <div class="modal-header">
                                            <h6 class="modal-title text-primary">متن پیام</h6>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                {{ $ticket->content}}
                                            </p>
                                            <hr class="bg-primary my-4">
                                            @if($ticket->replies)
                                               <h6>پاسخ دریافتی:</h6>
                                                @foreach($ticket->replies as $reply)
                                                    <div>
                                                        <p>
                                                            {{$reply->content}} 
                                                        </p>
                                                        <p>{{verta($reply->created_at)->formatDate()}}</p>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="modal-footer">
                                            <button
                                                class="btn btn-light"
                                                data-bs-dismiss="modal">
                                                بستن
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-warning text-center">
                                {{__('public.no_info')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$tickets->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
