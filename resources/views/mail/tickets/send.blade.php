@component('mail::message')
# Introduction

تیکت جدید ارسال کردید

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
