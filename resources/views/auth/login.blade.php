<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"  content=" با ورود به حساب کاربری جابیست، به فرصت‌های شغلی جدید دسترسی پیدا کنید و رزومه‌ی خود را مدیریت کنید. همچنین از امکانات دیگر سایت بهره‌مند شوید." />
    <meta name="author" content="haranet.ir">
    <meta name="theme-color" content="#585859">
    <meta property="og:title" content=" ورود به {{ env('APP_NAME') }} | به حساب کاربری خود وارد شوید"/>
    <meta property="og:url" content="https://jobist.ir/login"/>
    <meta property="og:description" content=" با ورود به حساب کاربری جابیست، به فرصت‌های شغلی جدید دسترسی پیدا کنید و رزومه‌ی خود را مدیریت کنید. همچنین از امکانات دیگر سایت بهره‌مند شوید. " />
    <meta property="og:site_name" content="جابیست" />

    <link rel="canonical" href="https://jobist.ir/login"/>
    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}"/>
    <link rel="apple-touch-icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}" type="image/x-icon" />

    <title> ورود به {{ env('APP_NAME') }} | به حساب کاربری خود وارد شوید </title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/dark-style.css'}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/transparent-style.css'}}" rel="stylesheet">
    <link href="{{'assets/panel/css/skin-modes.css'}}" rel="stylesheet" />

    <link href="{{'assets/panel/css/icons.css'}}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{'assets/panel/colors/color1.css'}}" />
{{--    <link href="{{'assets/panel/switcher/css/switcher.css'}}" rel="stylesheet" />--}}
{{--    <link href="{{'assets/panel/switcher/demo.css'}}" rel="stylesheet" />--}}
    <link rel="stylesheet" id="fonts" href="{{'assets/panel/fonts/styles-fa-num/iran-yekan.css'}}">
    <link href="{{'assets/panel/css/rtl.css'}}" rel="stylesheet" />
</head>
<body class="app sidebar-mini rtl login-img">

<div class="">

    <div id="global-loader">
        <img src="{{'assets/panel/images/loader.svg'}}" class="loader-img" alt="Loader">
    </div>

    @php
        if(route('home').'/' != url()->previous()){
            session()->put('previous_url', url()->previous());
        }
    @endphp

    <div class="page">
        <div class="">

            <div class="col col-login mx-auto mt-7">
                <div class="text-center">
                    <img src="{{'assets/panel/images/brand/jobist/white-text-logo.svg'}}" style="max-height: 70px; width:160px" class="header-brand-img" alt="">
                </div>
            </div>
            <div class="container-login100">
                <div class="wrap-login100 p-6">
                    <form action="{{route('login')}}" method="POST" class="login100-form validate-form">
                        @csrf
                        <span class="login100-form-title pb-5">
                        ورود
                        </span>
                        <div class="panel panel-primary">
                            <div class="tab-menu-heading">
                                @if(Session::has('success'))
                                   <div class="alert alert-success">
                                       <span class="text-success">{{Session::pull('success')}}</span>
                                   </div>
                                @endif
                            </div>
                            <div class="panel-body tabs-menu-body p-0 pt-5">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab5">
                                        <div class="wrap-input100 validate-input input-group">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="mdi mdi-phone text-muted" aria-hidden="true"></i>
                                            </a>
                                            <input inputmode="numeric" name="mobile" class="input100 border-start-0 form-control ms-0" type="text" placeholder="شماره تلفن">
                                        </div>
                                        @error('mobile')<span class="text-danger">{{$message}}</span>@enderror
                                        <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i>
                                            </a>
                                            <input name="password" class="input100 border-start-0 form-control ms-0" type="password" placeholder="کلمه عبور">
                                        </div>
                                        @error('password')<span class="text-danger">{{$message}}</span> @enderror
                                        <div class="text-end pt-4">
                                            <p class="mb-0"><a href="{{route('password.request')}}" class="text-primary ms-1">فراموشی رمز؟</a></p>
                                        </div>
                                        <div class="container-login100-form-btn">
                                            <button type="submit" class="login100-form-btn btn-primary">ورود</button>
                                        </div>
                                        <div class="text-center pt-3">
                                            <p class="text-dark mb-0">عضو نیستید؟<a href="{{route('register')}}" class="text-primary ms-1">عضو شوید</a></p>
                                        </div>
                                        {{-- <label class="login-social-icon"><span>ورود با شبکه های اجتماعی</span></label>
                                        <div class="d-flex justify-content-center">
                                            <a href="javascript:void(0)">
                                                <div class="social-login me-4 text-center">
                                                    <i class="fa fa-google"></i>
                                                </div>
                                            </a>
                                            <a href="javascript:void(0)">
                                                <div class="social-login me-4 text-center">
                                                    <i class="fa fa-facebook"></i>
                                                </div>
                                            </a>
                                            <a href="javascript:void(0)">
                                                <div class="social-login text-center">
                                                    <i class="fa fa-twitter"></i>
                                                </div>
                                            </a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>


<script src="{{'assets/panel/js/jquery.min.js'}}"></script>

<script src="{{'assets/panel/plugins/bootstrap/js/popper.min.js'}}"></script>
<script src="{{'assets/panel/plugins/bootstrap/js/bootstrap.min.js'}}"></script>

<script src="{{'assets/panel/js/show-password.min.js'}}"></script>

<script src="{{'assets/panel/js/generate-otp.js'}}"></script>

<script src="{{'assets/panel/plugins/p-scroll/perfect-scrollbar.js'}}"></script>

<script src="{{'assets/panel/js/themeColors.js'}}"></script>

<script src="{{'assets/panel/js/custom.js'}}"></script>
<script src="{{'assets/panel/js/custom1.js'}}"></script>

{{--<script src="{{'assets/panel/switcher/js/switcher.js'}}"></script>--}}
</body>
</html>
