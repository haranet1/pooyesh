<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description"  content=" با ثبت نام در جابیست، حساب کاربری خود را ایجاد کنید و به فرصت‌های شغلی جذاب دسترسی پیدا کنید. اینجا فرصتی برای شروع یک مسیر حرفه‌ای جدید است." />
    <meta name="author" content="haranet.ir">
    <meta name="theme-color" content="#585859">
    <meta property="og:url" content="https://jobist.ir/register"/>
    <meta property="og:title" content=" ثبت نام در جابیست | ایجاد حساب کاربری جدید" />
    <meta property="og:description" content="با ثبت نام در جابیست، حساب کاربری خود را ایجاد کنید و به فرصت‌های شغلی جذاب دسترسی پیدا کنید. اینجا فرصتی برای شروع یک مسیر حرفه‌ای جدید است." /> 
    <meta property="og:site_name" content="جابیست" />

    <link rel="canonical" href="https://jobist.ir/register"/>
    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}"/>
    <link rel="apple-touch-icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}" type="image/x-icon" />

    <title> ثبت نام در {{ env('APP_NAME') }} | ایجاد حساب کاربری جدید</title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"/>

    <link href="{{'assets/panel/css/style.css'}}" rel="stylesheet"/>
    <link href="{{'assets/panel/css/dark-style.css'}}" rel="stylesheet"/>
    <link href="{{'assets/panel/css/transparent-style.css'}}" rel="stylesheet">
    <link href="{{'assets/panel/css/skin-modes.css'}}" rel="stylesheet"/>

    <link href="{{'assets/panel/css/icons.css'}}" rel="stylesheet"/>

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{'assets/panel/colors/color1.css'}}"/>
    <!-- <link href="{{'assets/panel/switcher/css/switcher.css'}}" rel="stylesheet"/>
    <link href="{{'assets/panel/switcher/demo.css'}}" rel="stylesheet"/> -->
    <link rel="stylesheet" id="fonts" href="{{'assets/panel/fonts/styles-fa-num/iran-yekan.css'}}">
    <link href="{{'assets/panel/css/rtl.css'}}" rel="stylesheet"/>
    <style>
        .product-variants {
            margin: 0px 0 10px;
            color: #6f6f6f;
            font-size: 13px;
            line-height: 1.692;
        }
        ul.js-product-variants {
            margin: 20px 7px;
            display: inline-block;
            vertical-align: middle;
        }
        ul.js-product-variants li {
            margin: 0 8px 0 0;
            display: inline-block;
        }

        .product-variant>span {
            font-size: inherit;
            color: inherit;
            padding-left: 15px;
            margin-top: 3px;
            float: right;
        }

        .product-variants {
            margin-right: -8px;
            list-style: none;
            padding: 0;
            display: inline-block;
            margin-bottom: 0;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }



        .ui-variant input[type=radio] {
            visibility: hidden;
            position: absolute;
        }

        .ui-variant--check {
            cursor: pointer;
            border: 1px solid transparent;
            border-radius: 10px;
            color: #6f6f6f;
            padding: 3px 10px;
            font-size: 13px;
            font-size: .929rem;
            line-height: 1.692;
            display: block;
            -webkit-box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
            box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
        }

        .ui-variant--color .ui-variant--check {
            padding-right: 37px;
        }

        input[type=radio]:checked+.ui-variant--check {
            border-color: #2c90f6;
            background-color: #2c91f8;
            color: white;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .ui-variant--color .ui-variant-shape {
            width: 18px;
            height: 18px;
            position: absolute;
            right: 8px;
            top: 8px;
            border-radius: 50%;
            content: "";
            cursor: pointer;
        }

    </style>
</head>
<body class="app sidebar-mini rtl login-img">


<div class="">

    <div id="global-loader">
        <img src="{{asset('assets/panel/images/loader.svg')}}" class="loader-img" alt="Loader">
    </div>


    <div class="page">
        <div class="">

            <div class="col col-login mx-auto mt-7">
                <div class="text-center">
                    <img src="{{asset('assets/panel/images/brand/jobist/white-text-logo.svg')}}" style="max-height: 70px; width:160px" class="header-brand-img m-0"
                         alt="logo">
                </div>
            </div>

            <div class="container-login100">

                <div class="wrap-login100 p-6">
                    <form action="{{route('register')}}" method="POST" class="login100-form validate-form">
                       @csrf
                        <span class="login100-form-title">ثبت نام</span>

                        <div class="wrap-input100 validate-input input-group">
                            <span class="input-group-text bg-white text-muted">
                                <i class="mdi mdi-account" aria-hidden="true"></i>
                            </span>
                            <input name="name" value="{{old('name')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                   placeholder="نام">

                        </div>
                        @error('name') <small class="text-danger ">{{$message}}</small> @enderror


                        <div class="wrap-input100 validate-input input-group">
                            <span class="input-group-text bg-white text-muted">
                                <i class="mdi mdi-account" aria-hidden="true"></i>
                            </span>
                            <input name="family" value="{{old('family')}}" class="input100 border-start-0 ms-0 form-control" type="text"
                                   placeholder="نام خانوادگی">
                        </div>
                        @error('family') <small class="text-danger ">{{$message}}</small> @enderror



                        <div class="wrap-input100 validate-input input-group">
                            <span class="input-group-text bg-white text-muted">
                                <i class="mdi mdi-account-multiple" aria-hidden="true"></i>
                            </span>
                            <select name="sex" class="form-control form-select select2" >
                                <option value="" label="جنسیت">جنسیت</option>
                                <option value="male">مرد</option>
                                <option value="female">زن</option>
                            </select>
                        </div>
                        @error('sex') <small class="text-danger ">{{$message}}</small> @enderror

                        <div class="wrap-input100 validate-input input-group">
                            <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                <i class="mdi mdi-email" aria-hidden="true"></i>
                            </span>
                            <input name="email" value="{{old('email')}}" class="input100 border-start-0 ms-0 form-control" type="email"
                                   placeholder=" ایمیل">
                        </div>
                        @error('email') <small class="text-danger ">{{$message}}</small> @enderror

                        <div class="wrap-input100 validate-input input-group">
                            <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                <i class="mdi mdi-phone" aria-hidden="true"></i>
                            </span>
                            <input name="mobile" value="{{old('mobile')}}" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                   placeholder=" موبایل">
                        </div>
                        @error('mobile') <small class="text-danger ">{{$message}}</small> @enderror
                        
                        <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                            <span href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                <i class="zmdi zmdi-eye" aria-hidden="true"></i>
                            </span>
                            <input name="password" class="input100 border-start-0 ms-0 form-control" type="password"
                                   placeholder="کلمه عبور">
                        </div>
                        @error('password') <small class="text-danger ">{{$message}}</small> @enderror
                        <div class="wrap-input100 validate-input input-group">
                            <div class="product-variants">
                                <span>نوع کاربری: </span>
                                <ul class="js-product-variants">
                                    <li class="ui-variant">
                                        <label class="ui-variant ui-variant--color">
                                            <span class="ui-variant-shape">
                                              <i class="fa fa-building"></i>
                                            </span>
                                            <input type="radio" value="company" name="role"
                                                   class="variant-selector">
                                            <span class="ui-variant--check">شرکت</span>
                                        </label>
                                    </li>
                                    <li class="ui-variant">
                                        <label class="ui-variant ui-variant--color">
                                            <span class="ui-variant-shape">
                                              <i class="fa fa-user"></i>
                                            </span>
                                            <input type="radio" value="applicant" name="role"
                                                   class="variant-selector">
                                            <span class="ui-variant--check">کارجو</span>
                                        </label>
                                    </li>
                                    <li class="ui-variant">
                                        <label class="ui-variant ui-variant--color">
                                            <span class="ui-variant-shape">
                                              <i class="fa fa-user"></i>
                                            </span>
                                            <input type="radio" value="student" name="role"
                                                   class="variant-selector">
                                            <span class="ui-variant--check">دانشجو</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @error('role') <small class="text-danger ">{{$message}}</small> @enderror


                            {{--                        <label class="custom-control custom-checkbox mt-4">--}}
                            {{--                            <input type="checkbox" class="custom-control-input">--}}
                            {{--                            <span class="custom-control-label">موافقت با <a href="#">شرایط و خط مشی</a></span>--}}
                            {{--                        </label>--}}
                        <div class="container-login100-form-btn">
                            <button type="submit" class="login100-form-btn btn-primary">ثبت نام</button>
                        </div>
                        <div class="text-center pt-3">
                            <p class="text-dark mb-0">
                                از قبل حساب دارید؟<a href="{{route('login')}}" class="text-primary ms-1">ورود به سیستم</a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>

<script src="{{'assets/panel/js/jquery.min.js'}}"></script>

<script src="{{'assets/panel/plugins/bootstrap/js/popper.min.js'}}"></script>
<script src="{{'assets/panel/plugins/bootstrap/js/bootstrap.min.js'}}"></script>

<script src="{{'assets/panel/js/show-password.min.js'}}"></script>


<script src="{{'assets/panel/plugins/p-scroll/perfect-scrollbar.js'}}"></script>

<script src="{{'assets/panel/js/themeColors.js'}}"></script>

<script src="{{'assets/panel/js/custom.js'}}"></script>
<script src="{{'assets/panel/js/custom1.js'}}"></script>

{{--<script src="{{'assets/panel/switcher/js/switcher.js'}}"></script>--}}
</body>
</html>
