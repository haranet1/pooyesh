<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"  content=" با ورود به حساب کاربری جابیست، به فرصت‌های شغلی جدید دسترسی پیدا کنید و رزومه‌ی خود را مدیریت کنید. همچنین از امکانات دیگر سایت بهره‌مند شوید." />
    <meta name="author" content="haranet.ir">
    <meta name="theme-color" content="#585859">
    <meta property="og:title" content=" ورود به {{ env('APP_NAME') }} | به حساب کاربری خود وارد شوید"/>
    <meta property="og:url" content="https://jobist.ir/login"/>
    <meta property="og:description" content=" با ورود به حساب کاربری جابیست، به فرصت‌های شغلی جدید دسترسی پیدا کنید و رزومه‌ی خود را مدیریت کنید. همچنین از امکانات دیگر سایت بهره‌مند شوید. " />
    <meta property="og:site_name" content="جابیست" />

    <link rel="canonical" href="https://jobist.ir/login"/>
    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}"/>
    <link rel="apple-touch-icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}" type="image/x-icon" />

    <title> ورود به {{ env('APP_NAME') }} | به حساب کاربری خود وارد شوید </title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/dark-style.css'}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/transparent-style.css'}}" rel="stylesheet">
    <link href="{{'assets/panel/css/skin-modes.css'}}" rel="stylesheet" />

    <link href="{{'assets/panel/css/icons.css'}}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{'assets/panel/colors/color1.css'}}" />
{{--    <link href="{{'assets/panel/switcher/css/switcher.css'}}" rel="stylesheet" />--}}
{{--    <link href="{{'assets/panel/switcher/demo.css'}}" rel="stylesheet" />--}}
    <link rel="stylesheet" id="fonts" href="{{'assets/panel/fonts/styles-fa-num/iran-yekan.css'}}">
    <link href="{{'assets/panel/css/rtl.css'}}" rel="stylesheet" />
</head>
<body class="app sidebar-mini rtl login-img">

<style>
        .product-variants {
            margin: 0px 0 10px;
            color: #6f6f6f;
            font-size: 13px;
            line-height: 1.692;
        }
        ul.js-product-variants {
            margin: 20px 7px;
            display: inline-block;
            vertical-align: middle;
        }
        ul.js-product-variants li {
            margin: 0 8px 0 0;
            display: inline-block;
        }

        .product-variant>span {
            font-size: inherit;
            color: inherit;
            padding-left: 15px;
            margin-top: 3px;
            float: right;
        }

        .product-variants {
            margin-right: -8px;
            list-style: none;
            padding: 0;
            display: inline-block;
            margin-bottom: 0;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }



        .ui-variant input[type=radio] {
            visibility: hidden;
            position: absolute;
        }

        .ui-variant--check {
            cursor: pointer;
            border: 1px solid transparent;
            border-radius: 10px;
            color: #6f6f6f;
            padding: 3px 10px;
            font-size: 13px;
            font-size: .929rem;
            line-height: 1.692;
            display: block;
            -webkit-box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
            box-shadow: 0 2px 6px 0 rgba(51, 73, 94, 0.1);
        }

        .ui-variant--color .ui-variant--check {
            padding-right: 37px;
        }

        input[type=radio]:checked+.ui-variant--check {
            border-color: #2c90f6;
            background-color: #2c91f8;
            color: white;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .product-variants li {
            margin: 0 8px 8px 0;
            display: inline-block;
        }

        .ui-variant {
            display: inline-block;
            position: relative;
        }

        .ui-variant--color .ui-variant-shape {
            width: 18px;
            height: 18px;
            position: absolute;
            right: 8px;
            top: 8px;
            border-radius: 50%;
            content: "";
            cursor: pointer;
        }

    </style>

<div class="">

    <div id="global-loader">
        <img src="{{'assets/panel/images/loader.svg'}}" class="loader-img" alt="Loader">
    </div>

    @php
        if(route('home').'/' != url()->previous()){
            session()->put('previous_url', url()->previous());
        }
    @endphp

    <div class="page">
        <div class="">

            <div class="col col-login mx-auto mt-7">
                <div class="text-center">
                    <img src="{{'assets/panel/images/brand/jobist/white-text-logo.svg'}}" style="max-height: 70px; width:160px" class="header-brand-img" alt="">
                </div>
            </div>
            <div class="container-login100">
                <div class="wrap-login100 p-6">
                    <form action="{{route('store.role')}}" method="POST" class="login100-form validate-form">
                        @csrf
                        <span class="login100-form-title pb-5">
                        نوع کاربری خود را انتخاب کنید
                        </span>
                        <div class="panel panel-primary">
                            <div class="tab-menu-heading">
                                @if(Session::has('success'))
                                   <div class="alert alert-success">
                                       <span class="text-success">{{Session::pull('success')}}</span>
                                   </div>
                                @endif
                            </div>
                            <div class="panel-body tabs-menu-body p-0 pt-5">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab5">
                                        <div class="wrap-input100 validate-input input-group">
                                            <div class="product-variants">
                                                <span>نوع کاربری: </span>
                                                <ul class="js-product-variants">
                                                    <li class="ui-variant">
                                                        <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                            <i class="fa fa-building"></i>
                                                            </span>
                                                            <input type="radio" value="company" name="role"
                                                                class="variant-selector">
                                                            <span class="ui-variant--check">شرکت</span>
                                                        </label>
                                                    </li>
                                                    <li class="ui-variant">
                                                        <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                            <i class="fa fa-user"></i>
                                                            </span>
                                                            <input type="radio" value="applicant" name="role"
                                                                class="variant-selector">
                                                            <span class="ui-variant--check">کارجو</span>
                                                        </label>
                                                    </li>
                                                    <li class="ui-variant">
                                                        <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape">
                                                            <i class="fa fa-user"></i>
                                                            </span>
                                                            <input type="radio" value="student" name="role"
                                                                class="variant-selector">
                                                            <span class="ui-variant--check">دانشجو</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        @error('role') <small class="text-danger ">{{$message}}</small> @enderror
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="container-login100-form-btn">
                                <button type="submit" class="login100-form-btn btn-primary">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>


<script src="{{'assets/panel/js/jquery.min.js'}}"></script>

<script src="{{'assets/panel/plugins/bootstrap/js/popper.min.js'}}"></script>
<script src="{{'assets/panel/plugins/bootstrap/js/bootstrap.min.js'}}"></script>

<script src="{{'assets/panel/js/show-password.min.js'}}"></script>

<script src="{{'assets/panel/js/generate-otp.js'}}"></script>

<script src="{{'assets/panel/plugins/p-scroll/perfect-scrollbar.js'}}"></script>

<script src="{{'assets/panel/js/themeColors.js'}}"></script>

<script src="{{'assets/panel/js/custom.js'}}"></script>
<script src="{{'assets/panel/js/custom1.js'}}"></script>

{{--<script src="{{'assets/panel/switcher/js/switcher.js'}}"></script>--}}
</body>
</html>
