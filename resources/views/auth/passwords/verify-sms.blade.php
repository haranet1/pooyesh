<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <meta name="description" content="جابیست">
    <meta name="author" content="haranet.ir">

    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}"/>
    <link rel="apple-touch-icon" href="{{ asset('assets/frontend/img/home/logo-fav.svg') }}" type="image/x-icon" />

    <title>جابیست | تایید کد ارسالی</title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/panel/css/dark-style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/panel/css/transparent-style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/panel/css/skin-modes.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/icons.css')}}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{asset('assets/panel/colors/color1.css')}}" />
    {{--    <link href="{{'assets/panel/switcher/css/switcher.css'}}" rel="stylesheet" />--}}
    {{--    <link href="{{'assets/panel/switcher/demo.css'}}" rel="stylesheet" />--}}
    <link rel="stylesheet" id="fonts" href="{{asset('assets/panel/fonts/styles-fa-num/iran-yekan.css')}}">
    <link href="{{asset('assets/panel/css/rtl.css')}}" rel="stylesheet" />
</head>
<body class="app sidebar-mini rtl login-img">

<div class="">

    <div id="global-loader">
        <img src="{{asset('assets/panel/images/loader.svg')}}" class="loader-img" alt="Loader">
    </div>


    <div class="page">
        <div class="">

            <div class="col col-login mx-auto mt-7">
                <div class="text-center">
                    <img src="{{'../assets/panel/images/brand/jobist/white-text-logo.svg'}}" class="header-brand-img" style="max-height: 70px; width:160px" alt="">
                </div>
            </div>
            <div class="container-login100">
                <div class="wrap-login100 p-6">
                    <form action="{{route('password.reset.sms.verify')}}" method="POST" class="login100-form validate-form">
                        @csrf
                        <span class="login100-form-title pb-5">
 تایید کد ارسالی
</span>
                        <div class="panel panel-primary">
                            <div class="tab-menu-heading">
                            </div>
                            <div class="panel-body tabs-menu-body p-0 pt-5">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab5">
                                        <p>لطفا کد ارسال شده به  شماره تلفن خود را وارد کنید.</p>
                                        <div class="wrap-input100 validate-input input-group">
                                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                <i class="zmdi zmdi-phone text-muted" aria-hidden="true"></i>
                                            </a>
                                            <input type="hidden" name="id" value="{{$id}}">
                                            <input name="code" inputmode="numeric" class="input100 border-start-0 form-control ms-0" type="text" placeholder="کد">
                                        </div>
                                        @error('code')<span class="text-danger">{{$message}}</span>@enderror

                                        <div class="container-login100-form-btn">
                                            <button type="submit" class="login100-form-btn btn-primary">تایید</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>


<script src="{{asset('assets/panel/js/jquery.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{asset('assets/panel/js/show-password.min.js')}}"></script>

<script src="{{asset('assets/panel/js/generate-otp.js')}}"></script>

<script src="{{asset('assets/panel/plugins/p-scroll/perfect-scrollbar.js')}}"></script>

<script src="{{asset('assets/panel/js/themeColors.js')}}"></script>

<script src="{{asset('assets/panel/js/custom.js')}}"></script>
<script src="{{asset('assets/panel/js/custom1.js')}}"></script>

{{--<script src="{{'assets/panel/switcher/js/switcher.js'}}"></script>--}}
</body>
</html>
