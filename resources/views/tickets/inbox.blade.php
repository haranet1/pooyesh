    @extends($config['panel'])
@section('title' , $title)

@section('main')
    
    <div class="page-header">
        <h1 class="page-title">{{$title}}</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">صفحات</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
            </ol>
        </div>
    </div>


    <div class="row">
        @if(Session::has('success'))
            <div class="alert alert-success mt-2 text-center">
                <h5>{{Session::pull('success')}}</h5>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger mt-2 text-center">
                <h5>{{Session::pull('error')}}</h5>
            </div>
        @endif
        <div class="col-xl-3">
            <div id="scroll-stickybar" class="position-lg-relative w-100">
                <div class="card">
                    <div class="list-group list-group-transparent mb-0 mail-inbox pb-3">
                        @if (Auth::user()->groupable_type != App\Models\EmployeeInfo::class)
                            <div class="mt-4 mx-4 mb-4 text-center">
                                <a href="{{ route('compose.sup.ticket') }}" class="btn btn-primary btn-lg d-grid">نوشتن</a>
                            </div>
                            <a href="{{ route('inbox.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('inbox.ticket')?'active':''}}">
                                <span class="icons"><i class="ri-mail-line"></i></span> صندوق ورودی <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['MyInbox'] }}</span>
                            </a>
                            <a href="{{ route('sent.sup.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('sent.sup.ticket')?'active':''}}">
                                <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های ارسال شده <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['sentSup'] }}</span>
                            </a>
                        @else
                            <div class="mt-4"></div>
                            @can('support')
                                <a href="{{ route('inbox.ticket') }}" class=" list-group-item d-flex align-items-center mx-4 {{Route::is('inbox.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-line"></i></span> صندوق ورودی پشتیبانی <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['SupTickets'] }}</span>
                                </a>
                            @endcan
                            @canany(['unconfirmedUser' , 'confirmedUser'])
                                <a href="{{ route('user.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('user.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد کاربر <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['reg1Sent'] }}</span>
                                </a>
                            @endcanany
                            @can('protection')
                                <a href="{{ route('co-user.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('co-user.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد شرکت <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['reg2Sent'] }}</span>
                                </a>
                            @endcan
                            @can('pooyeshRequest')
                                <a href="{{ route('i-pos.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('i-pos.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد موقعیت کارآموزی <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['iPosSent'] }}</span>
                                </a>
                                <a href="{{ route('i-req.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('i-req.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد درخواست پویش <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['iReqSent'] }}</span>
                                </a>
                            @endcan
                            @can('hireRequest')
                                <a href="{{ route('h-pos.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('h-pos.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد موقعیت شغلی <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['hPosSent'] }}</span>
                                </a>
                                <a href="{{ route('h-req.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('h-req.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد درخواست استخدام <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['hReqSent'] }}</span>
                                </a>
                            @endcan
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9">
            <div class="card">
                <div class="card-body p-6">
                    <div class="inbox-body">
                        <div class="table-responsive">
                            @if ($tickets->count() > 0)
                                <table class="table table-hover table-striped-columns text-nowrap mb-0">
                                    <thead class="table-info text-white text-start ">  
                                        <tr>
                                            <th></th>
                                            @if (Route::is('inbox.ticket') && (Auth::user()->groupable_type == App\Models\EmployeeInfo::class || Auth::user()->groupable_type == App\Models\CompanyInfo::class))
                                                <th class="">فرستنده</th>
                                            @else
                                                <th class="">گیرنده</th>
                                            @endif
                                            <th>موضوع</th>
                                            <th>آخرین متن</th>
                                            @if (Route::is('co-user.rejected.sent.ticket') && Auth::user()->groupable_type == App\Models\EmployeeInfo::class)
                                                <th>وضعیت</th>
                                            @endif
                                            <th class="text-end">تاریخ ارسال</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($tickets as $ticket)
                                            <tr>
                                                @if ($ticket->status == 'seen')
                                                    <td class="inbox-small-cells"><i class="fa fa-eye inbox-started text-gray"></i></td>
                                                @else
                                                    <td class="inbox-small-cells"><i class="fa fa-eye inbox-started text-warning"></i></td>
                                                @endif

                                                @if (Route::is('inbox.ticket'))
                                                    @if ($ticket->sender->groupable_type == App\Models\EmployeeInfo::class)
                                                        <td class="view-message dont-show fw-semibold">
                                                            <p> پشتیبانی سایت </p>
                                                        </td>
                                                    @elseif ($ticket->sender->groupable_type == App\Models\CompanyInfo::class)
                                                        <td class="view-message dont-show fw-semibold">
                                                            <a href="{{ route('company.single', $ticket->sender_id) }}" target="_blank" class="text-description text-dark">
                                                                <span class="badge bg-primary">شرکت</span>
                                                                {{ $ticket->sender->groupable->name }}
                                                            </a>
                                                        </td>
                                                    @else
                                                        <td class="view-message dont-show fw-semibold">
                                                            <a href="{{ route('candidate.single', $ticket->sender_id) }}" target="_blank" class="text-description text-dark">
                                                                <span class="badge bg-primary">دانشجو</span>
                                                                {{ $ticket->sender->name }} {{ $ticket->sender->family }}
                                                            </a>
                                                        </td>
                                                    @endif
                                                @endif

                                                @if (!Route::is('inbox.ticket'))
                                                    @if (is_null($ticket->receiver))
                                                        <td class="view-message dont-show fw-semibold">
                                                            <p class="text-description text-dark">
                                                                <span class="badge bg-primary">پشتیبانی</span>
                                                            </p>
                                                        </td>
                                                    @elseif ($ticket->receiver->groupable_type == App\Models\CompanyInfo::class)
                                                        <td class="view-message dont-show fw-semibold">
                                                            <a href="{{ route('company.single', $ticket->receiver_id) }}" target="_blank" class="text-description text-dark">
                                                                <span class="badge bg-primary">شرکت</span>
                                                                {{ $ticket->receiver->groupable->name }}
                                                            </a>
                                                        </td>
                                                    @else
                                                        <td class="view-message dont-show fw-semibold">
                                                            <a href="{{ route('candidate.single' , $ticket->receiver_id) }}" target="_blank" class="text-description text-dark">
                                                                <span class="badge bg-success">دانشجو</span>
                                                                {{ $ticket->receiver->name }} {{ $ticket->receiver->family }}
                                                            </a>
                                                        </td>
                                                    @endif
                                                @endif

                                                <td class="view-message dont-show fw-semibold clickable-row" data-href='{{ route('index.ticket' , $ticket->id) }}'>
                                                    {{$ticket->subject}}
                                                </td>

                                                <td class="view-message clickable-row" data-href='{{ route('index.ticket' , $ticket->id) }}'>
                                                    @if ($ticket->replies)
                                                        @foreach ($ticket->replies as $reply)
                                                            @if ($loop->last)
                                                                {{Str::limit($reply->content  , '40' , '...')}}
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </td>

                                                @if (Route::is('co-user.rejected.sent.ticket') && Auth::user()->groupable_type == App\Models\EmployeeInfo::class)
                                                    
                                                    <td class="view-message dont-show fw-semibold clickable-row">
                                                        @if ($ticket->category == 'Reg1')
                                                            <span class="text-warning">رد شده توسط کارشناس</span>
                                                        @else
                                                            <span class="text-danger">رد شده توسط حراست</span>
                                                        @endif
                                                    </td>

                                                @endif

                                                <td class="view-message text-end fw-semibold clickable-row" data-href='{{ route('index.ticket' , $ticket->id) }}'>
                                                    {{ verta($ticket->created_at)->format('%d %B، %Y') }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-warning text-center">
                                    @lang('public.no_info')
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-5 d-flex justify-content-center">
                {{$tickets->links('pagination.panel')}}
            </div>
        </div>
    </div>
@endsection
