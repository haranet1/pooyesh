@extends($config['panel'])
@section('title' , $config['title'])

@section('main')
    <div class="page-header">
        <h1 class="page-title">{{ $config['title'] }}</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">صفحات</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $config['title'] }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3">
            <div id="scroll-stickybar" class="position-lg-relative w-100">
                <div class="card">
                    <div class="list-group list-group-transparent mb-0 mail-inbox pb-3">
                        <div class="mt-4 mx-4 mb-4 text-center">
                            <a href="{{ route('inbox.ticket') }}" class="btn btn-primary btn-lg d-grid">لیست تیکت ها</a>
                        </div>
                        <a href="email-inbox.html" class="list-group-item d-flex align-items-center active mx-4">
                            <span class="icons"><i class="ri-mail-line"></i></span> صندوق ورودی <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['MyInbox'] }}</span>
                        </a>
                        <a href="{{ route('sent.sup.ticket') }}" class="list-group-item d-flex align-items-center mx-4">
                            <span class="icons"><i class="ri-mail-send-line"></i></span> نامه ارسال شده <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['sentSup'] }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9">
            <div class="card">
                <form action="{{ route('send.sup.ticket') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header">
                        <h5>ارسال پیام جدید</h5>
                    </div>
                    <div class="card-body p-6">
                        <div class="form-group">
                            <div class="row align-items-center">
                                <label class="col-xl-2 form-label">موضوع</label>
                                <div class="col-xl-10">
                                    <input type="text" name="subject" class="form-control">
                                    @error('subject')<small class="text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ">
                                <label class="col-xl-2 form-label">متن پیام</label>
                                <div class="col-xl-10">
                                    <textarea rows="10" name="content" class="form-control"></textarea>
                                    @error('content')<small class="text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row align-items-center">
                                <label class="col-xl-2 form-label">فایل ضمیمه</label>
                                <div class="col-xl-10">
                                    <input type="file" name="attachment" class="form-control">
                                    @error('attachment')<small class="text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-sm-flex">
                        <div class="mt-2 mb-2">
                            <a href="javascript:void(0)" class="btn btn-icon btn-white btn-svg" data-bs-toggle="tooltip" title="" data-bs-original-title="پیوست"><span class="ri-attachment-2"></span></a>
                            <a href="javascript:void(0)" class="btn btn-icon btn-white btn-svg" data-bs-toggle="tooltip" title="" data-bs-original-title="لینک"><span class="ri-link"></span></a>
                            <a href="javascript:void(0)" class="btn btn-icon btn-white btn-svg" data-bs-toggle="tooltip" title="" data-bs-original-title="تصاویر"><span class="ri-image-line"></span></a>
                            <a href="javascript:void(0)" class="btn btn-icon btn-white btn-svg" data-bs-toggle="tooltip" title="" data-bs-original-title="حذف"><span class="ri-delete-bin-line"></span></a>
                        </div>
                        <div class="btn-list ms-auto my-auto">
                            <a href="{{ route('inbox.ticket') }}" class="btn btn-danger btn-space mb-0">لغو</a>
                            <button type="submit" class="btn btn-primary btn-space mb-0">ارسال پیام</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection