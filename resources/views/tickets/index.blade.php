@extends($config['panel'])
@section('title' , $title)
@section('main')

    <div class="page-header">
        <h1 class="page-title">{{$title}}</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">صفحات</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3">
            <div id="scroll-stickybar" class="position-lg-relative w-100">
                <div class="card">
                    <div class="list-group list-group-transparent mb-0 mail-inbox pb-3">
                        @if (Auth::user()->groupable_type != App\Models\EmployeeInfo::class)
                            <div class="mt-4 mx-4 mb-4 text-center">
                                <a href="{{ route('compose.sup.ticket') }}" class="btn btn-primary btn-lg d-grid">نوشتن</a>
                            </div>
                            <a href="{{ route('inbox.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('inbox.ticket')?'active':''}}">
                                <span class="icons"><i class="ri-mail-line"></i></span> صندوق ورودی <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['MyInbox'] }}</span>
                            </a>
                            <a href="{{ route('sent.sup.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('sent.sup.ticket')?'active':''}}">
                                <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های ارسال شده <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['sentSup'] }}</span>
                            </a>
                        @else
                            <div class="mt-4"></div>
                            @can('support')
                                <a href="{{ route('inbox.ticket') }}" class=" list-group-item d-flex align-items-center mx-4 {{Route::is('inbox.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-line"></i></span> صندوق ورودی پشتیبانی <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['SupTickets'] }}</span>
                                </a>
                            @endcan
                            @canany(['unconfirmedUser' , 'confirmedUser'])
                                <a href="{{ route('user.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('user.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد کاربر <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['reg1Sent'] }}</span>
                                </a>
                            @endcanany
                            @can('protection')
                                <a href="{{ route('co-user.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('co-user.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد شرکت <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['reg2Sent'] }}</span>
                                </a>
                            @endcan
                            @can('pooyeshRequest')
                                <a href="{{ route('i-pos.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('i-pos.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد موقعیت کارآموزی <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['iPosSent'] }}</span>
                                </a>
                                <a href="{{ route('i-req.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('i-req.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد درخواست پویش <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['iReqSent'] }}</span>
                                </a>
                            @endcan
                            @can('hireRequest')
                                <a href="{{ route('h-pos.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('h-pos.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد موقعیت شغلی <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['hPosSent'] }}</span>
                                </a>
                                <a href="{{ route('h-req.rejected.sent.ticket') }}" class="list-group-item d-flex align-items-center mx-4 {{Route::is('h-req.rejected.sent.ticket')?'active':''}}">
                                    <span class="icons"><i class="ri-mail-send-line"></i></span> تیکت های رد درخواست استخدام <span class="ms-auto badge bg-secondary bradius">{{ $config['sendCount']['hReqSent'] }}</span>
                                </a>
                            @endcan
                            
                        @endif
                    </div>
                    {{-- <div class="card-body border-top p-0 py-3">
                        <div class="list-group list-group-transparent mb-0 mail-inbox mx-4">
                            <a href="javascript:void(0)" class="list-group-item list-group-item-action d-flex align-items-center py-2">
                                <span class="w-3 h-3 brround bg-primary me-2"></span> دوستان
                            </a>
                            <a href="javascript:void(0)" class="list-group-item list-group-item-action d-flex align-items-center py-2">
                                <span class="w-3 h-3 brround bg-secondary me-2"></span> خانواده
                            </a>
                            <a href="javascript:void(0)" class="list-group-item list-group-item-action d-flex align-items-center py-2">
                                <span class="w-3 h-3 brround bg-success me-2"></span> اجتماعی
                            </a>
                            <a href="javascript:void(0)" class="list-group-item list-group-item-action d-flex align-items-center py-2">
                                <span class="w-3 h-3 brround bg-info me-2"></span> دفتر
                            </a>
                            <a href="javascript:void(0)" class="list-group-item list-group-item-action d-flex align-items-center py-2">
                                <span class="w-3 h-3 brround bg-warning me-2"></span> کار
                            </a>
                            <a href="javascript:void(0)" class="list-group-item list-group-item-action d-flex align-items-center py-2">
                                <span class="w-3 h-3 brround bg-danger me-2"></span> تنظیمات
                            </a>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>

        <div class="col-xl-9">
            {{-- info card header --}}
            <div class="card">
                <div class="card-header bg-secondary text-white">
                    موضوع : {{ $ticket->subject }}
                </div>
                <div class="card-body">
                    @if ($ticket->category == 'Sup')
                        <div class="">اطلاعات فرستنده :</div>
                        @if (Auth::user()->groupable_type == \App\Models\EmployeeInfo::class)
                            @if ($ticket->sender->groupable_type == \App\Models\CompanyInfo::class)
                                <div class="d-flex align-items-center justify-content-between">
                                    
                                    <a href="{{ route('company.single' , $ticket->sender_id) }}" class="d-flex align-items-center text-description text-dark">
                                        @if ($ticket->sender->groupable->logo)
                                            <img src="{{ asset($ticket->sender->groupable->logo->path) }}" alt="" class="me-2 rounded-circle avatar avatar-lg">
                                        @else
                                            <img src="{{ asset('assets/panel/images/users/company-user.svg') }}" alt="" class="me-2 rounded-circle avatar avatar-lg">
                                        @endif
                                        <span class="badge bg-primary">شرکت</span>
                                        <div class="ms-2">{{ $ticket->sender->groupable->name }}</div>
                                    </a>
                                    
                                    <div class="d-flex align-items-center">
                                        <span class="badge bg-orange">نام و نام خانوادگی</span>
                                        <div class="ms-2">{{ $ticket->sender->name }} {{ $ticket->sender->family }}</div>
                                    </div>

                                    <div class="d-flex align-items-center">
                                        <span class="badge bg-green">تلفن</span>
                                        <div class="ms-2">{{ $ticket->sender->mobile }}</div>
                                    </div>

                                </div>
                            @else
                                <div class="d-flex align-items-center justify-content-between">

                                    <a href="{{ route('candidate.single' , $ticket->sender_id) }}" class="d-flex align-items-center text-description text-dark">
                                        <img src="{{ asset('assets/panel/images/users/std-user.svg') }}" alt="" class="me-2 rounded-circle avatar avatar-lg">
                                        <span class="badge bg-primary">دانشجو</span>
                                        <div class="ms-2">{{ $ticket->sender->name }} {{ $ticket->sender->family }}</div>
                                    </a>

                                    <div class="d-flex align-items-center">
                                        <span class="badge bg-orange">
                                            @if ($ticket->sender->grade )
                                                مقطع
                                            @endif
                                            @if ($ticket->sender->grade && $ticket->sender->major)
                                                و
                                            @endif
                                            @if ($ticket->sender->major)
                                                رشته
                                            @endif
                                        </span>
                                        <div class="ms-2">
                                            @if ($ticket->sender->grade)
                                                {{ $ticket->sender->grade }} 
                                            @endif
                                            @if ($ticket->sender->grade && $ticket->sender->major)
                                                |
                                            @endif
                                            @if ($ticket->sender->major)
                                                {{ $ticket->sender->major }}
                                            @endif
                                        </div>
                                    </div>

                                    <div class="d-flex align-items-center">
                                        <span class="badge bg-green">تلفن</span>
                                        <div class="ms-2">{{ $ticket->sender->mobile }}</div>
                                    </div>

                                </div>
                            @endif
                        @else
                            ارسال تیکت به پشتیبانی
                        @endif
                    @else
                                
                    @endif
                    
                </div>
            </div>

            @if ($ticket->replies->count() > 0)
                @foreach ($ticket->replies as $reply)
                    <div class="card">
                        <div class="card-body p-6">
                            <div class="email-media">
                                <div class="mt-0 d-sm-flex">
                                    <img class="me-2 rounded-circle avatar avatar-lg" src="
                                    @if ($reply->sender->groupable_type ==  \App\Models\EmployeeInfo::class)
                                        {{ asset('assets/panel/images/users/admin-user.svg') }}
                                    @elseif($reply->sender->groupable_type == \App\Models\CompanyInfo::class && $reply->sender_id == Auth::id())
                                        @if (Auth::user()->groupable->logo)
                                            {{ asset(Auth::user()->groupable->logo->path) }}
                                        @else
                                            {{ asset('assets/panel/images/users/company-user.svg') }}
                                        @endif
                                    @else
                                        {{ asset('assets/panel/images/users/std-user.svg') }}  
                                    @endif
                                    " alt="avatar">
                                    <div class="media-body pt-0">
                                        <div class="float-end d-none d-md-flex fs-15">
                                            <small class="me-3 mt-3 text-muted">{{ verta($reply->created_at)->format('ساعت %H:%m | %d %B، %Y') }}</small>
                                        </div>
                                        <div class="media-title text-dark font-weight-semibold mt-1">
                                            @if ($reply->sender->groupable_type == \App\Models\CompanyInfo::class)
                                                <a href="{{ route('company.single' , $reply->sender_id) }}" class="text-decoration text-dark" target="blank">
                                                    {{ $reply->sender->groupable->name }}
                                                </a>
                                            @elseif($reply->sender->groupable_type == \App\Models\EmployeeInfo::class)
                                                کارشناس پویش
                                            @else
                                                <a href="{{ route('candidate.single' , $reply->sender_id) }}" class="text-decoration text-dark" target="blank">
                                                    {{ $reply->sender->name }} {{ $reply->sender->family }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="eamil-body mt-5">
                                    <p>{{ $reply->content }}</p>    
                                    @if ($reply->attachment)
                                        <hr>
                                        <div class="email-attch">
                                            <p class="font-weight-semibold"> پیوست
                                            </p>
                                        </div>
                                        <div class="row attachments-doc">
                                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 mb-2 mb-sm-0">
                                                <div class="border overflow-hidden p-0 br-7">
                                                    <a href="{{ asset($reply->attachment->path) }}" target="blank">
                                                        <img src="{{ asset($reply->attachment->path) }}" class="card-img-top" alt="img">
                                                    </a>
                                                    <div class="p-3 text-center">
                                                        <a href="{{ asset($reply->attachment->path) }}" class="fw-semibold fs-15 text-dark" target="blank">
                                                            {{ $reply->attachment->name }}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('send.reply.ticket') }}" method="POST" >
                        @csrf
                        <div class="d-flex align-items-center">
                            <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                            <textarea type="text" name="content" class="form-control px-2" rows="1" placeholder="پاسخ"></textarea>
                            <label for="attachment" class="mx-1 email-icon text-secondary bg-secondary-transparent my-0" style="cursor: pointer">
                                <input type="file" class="d-none" id="attachment" name="attachment">
                                <i class=" fa fa-paperclip"></i>
                            </label>
                            <button class="mx-1 email-icon text-secondary bg-secondary-transparent"><i class=" fa fa-reply"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection