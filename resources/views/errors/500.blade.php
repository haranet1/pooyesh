@extends('frontend.layout.master')

@section('title' , 'ارور ۵۰۰')
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/errors.css') }}">
@endsection


@section('main')

    <div class="container mt-5">
        <div class="row pt-5 flex-md-row flex-column-reverse align-items-center">
            <div class="col-md-7 col-12">
                <div class="d-flex flex-column justify-content-center align-items-center bg-circle">
                    <div class="d-flex mb-3">
                        <h2 class="blue fs-large fw-bold">ارور</h2>
                        <h2 class="text-shadow-yellow blue fs-large fw-bold">۵۰۰</h2>
                    </div>
                    <h2 class="blue fw-bold text-center mb-4">متاسفانه در این صفحه خطای ناگهانی رخ داده است!</h2>
                    <h5 class="blue fw-bold text-center mb-4">تیم فنی ما به زودی این مشکل را حل می‌کند ولی درصورتی که عجله دارید با پشتیبانی تماس بگیرید.</h5>
                    <a class="align-self-center erorr-btn mb-3" href="{{ route('home') }}">
                        <span class="blue fs-5 fw-bold">بازگشت به خانه</span>
                        <img src="{{ asset('assets/frontend/img/errors/arrow-rotate-left-solid.png') }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-5 col-12">
                <img src="{{ asset('assets/frontend/img/errors/Group 1599.png') }}" class="img-fluid mb-5 mb-md-0">
            </div>
        </div>
    </div>

@endsection


