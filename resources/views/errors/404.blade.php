@extends('frontend.layout.master')

@section('title' , 'ارور ۴۰۴')
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/errors.css') }}">
@endsection


@section('main')

    <div class="container mt-5">
        <div class="row pt-5 flex-md-row flex-column-reverse align-items-center">
            <div class="col-md-6 col-12">
                <div class="d-flex flex-column justify-content-center align-items-center bg-circle">
                    <div class="d-flex mb-3">
                        <h2 class="blue fs-large fw-bold">ارور</h2>
                        <h2 class="text-shadow-yellow blue fs-large fw-bold">۴۰۴</h2>
                    </div>
                    <h3 class="blue fs-1 fw-bold text-center mb-4">دسترسی شما به این صفحه مجاز نیست!</h3>
                    <a class="align-self-center erorr-btn mb-3" href="{{route('home')}}">
                        <span class="blue fs-5 fw-bold">بازگشت به خانه</span>
                        <img src="{{ asset('assets/frontend/img/errors/arrow-rotate-left-solid.png') }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <img src="{{ asset('assets/frontend/img/errors/Group 432.png') }}" class="img-fluid mb-5 mb-md-0">
            </div>
        </div>
    </div>

@endsection


