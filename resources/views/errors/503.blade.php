@extends('frontend.layout.master')

@section('title' , 'ارور ۴۰۵')
@section('page' , 'جابیست')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/errors.css') }}">
@endsection


@section('main')

    <div class="container mt-5">
        <div class="row pt-5 flex-md-row flex-column-reverse align-items-center">
            <div class="col-md-6 col-12">
                <div class="d-flex flex-column justify-content-center align-items-center bg-circle">
                    <div class="d-flex mb-3">
{{--                        <h4 class="blue fs-medium fw-bold">۵۰۳</h4>--}}
                        <h4 class="text-shadow-yellow blue fs-medium fw-bold text-center">در حال به‌روز رسانی هستیم!</h4>
                    </div>
                    <h4 class="blue fs-1 text-center mb-4">به زودی برمیگردیم</h4>
                    <a class="align-self-center erorr-btn mb-3" href="{{ route('home') }}">
                        <span class="blue fs-5 fw-bold">بازگشت به خانه</span>
                        <img src="{{ asset('assets/frontend/img/errors/arrow-rotate-left-solid.png') }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <img src="{{ asset('assets/frontend/img/errors/503.png') }}" class="img-fluid mb-5 mb-md-0">
            </div>
        </div>
    </div>

@endsection


