
@foreach ($comments as $comment)
    @php
        if(isset($depth))
            $depth = $depth + 1
    @endphp
    <div class="mt-3 @if($comment->parent_id && $depth < 6) {{ 'm-reply' }} @else {{ 'm-reply-inherit' }}  @endif;">
        <div class="p-md-3 p-2 bg-white rounded-4 position-relative {{ $comment->confirmedReplies->count() > 0 ? 'mb-5' : '' }}">
            <div class="d-flex align-items-center p-1 flex-column-reverse flex-md-row">

                <div class="col-left-reply p-2 fs-6 fw-medium">
                    {{ $comment->body }} 
                </div>

                <div class="col-right user-comment mx-sm-2 px-0">
                    @if($comment->user->sex == 'female')
                        <img src="{{ asset('assets/frontend/img/candidate/famale.png') }}" alt="" class="img-fluid">
                    @else
                        <img src="{{ asset('assets/frontend/img/candidate/male.svg') }}" alt="" class="img-fluid">
                    @endif
                    <div class="mt-1 text-center">
                        <span class="fs-14">{{ $comment->user->name. ' '. $comment->user->family }}</span>
                    </div>
                </div>

            </div>
            <div class="text-start">
                <div class="btn btn-send rounded-3 bg-blue pointer fs-6 px-2 mt-1" data-bs-toggle="collapse" data-bs-target="#comment-{{ $comment->id }}" aria-expanded="true">
                    <span class="text-white">پاسخ</span>
                    <span class="text-white me-1">
                        <i class="fa-solid fa-reply"></i>
                    </span>
                </div>

            </div>
            @if ($comment->confirmedReplies->count() > 0)
                <div class="text-start open-comments">
                    <div class="position-relative">
                        <img src="{{ asset('assets/frontend/img/blog/svg-path.svg') }}" alt="" class="img-fluid">
                        <span class="pointer blue p-2 fs-4" onclick="angleDownRotate(this)" data-bs-toggle="collapse" data-bs-target="#reply-{{ $comment->id }}" aria-expanded="true">
                            <i class="fa-solid fa-angle-down"></i>
                        </span>
                    </div>
                </div>
            @endif
        </div>
        
        <div class="text-start">
            <div class="collapse p-3" id="comment-{{ $comment->id }}">
                <form method="POST" action="{{ route('store.comment.article') }}">
                    @csrf
                    <input type="hidden" name="commentable_id" value="{{ $article->id }}">
                    <input type="hidden" name="parent_id" value="{{ $comment->id }}">
                    <textarea class="form-control comment-input" name="body" rows="5" required></textarea>
                    <button type="submit" class="btn btn-send rounded-3 bg-blue text-white fs-6 px-5 mt-3">ارسال</button>
                </form>
            </div>
        </div>
        

        @if ($comment->confirmedReplies->count() > 0)
            <div class="mt-3 collapse" id="reply-{{ $comment->id }}">
                @include('partials.comment', ['comments' => $comment->confirmedReplies, 'depth' => $depth])
            </div>
        @endif

    </div>
@endforeach