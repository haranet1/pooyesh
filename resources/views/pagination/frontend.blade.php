
@if ($paginator->hasPages())
    <div class="d-flex justify-content-center align-items-center mt-5 gap-3 flex-nowrap">
        @if (! is_null($paginator->previousPageUrl()))
            <div class="bg-white rounded-circle  page-item-shadow page-item">
                <a href="{{ $paginator->previousPageUrl() }}" aria-label="previous page">
                    <img src="{{ asset('assets/frontend/img/companies/arrow-right-solid.svg') }}" class="page-arrow" alt="previous page" height="15" width="20">
                </a>
            </div>
        @endif
        <div class="bg-white rounded-pill d-flex page-item-shadow px-md-4 px-2 gap-md-3 gap-1 py-1 flex-wrap">
            @if ($paginator->currentPage() == 1)
                @foreach ($elements as $element)
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page <= 3 || $page == $paginator->lastPage())
                                @if ($page == $paginator->currentPage())
                                    <div class="rounded-circle fanumber page-num fs-4 fw-semibold blue active">
                                        {{ $page }}
                                    </div>
                                @else
                                    <a href="{{ $url }}" class="rounded-circle fanumber page-num fs-4 fw-semibold blue" aria-label="page number">
                                        {{ $page }}
                                    </a>
                                @endif
                            @elseif ($page == 4)
                                <div class="rounded-circle fanumber page-num fs-4 fw-semibold blue ">
                                    ...
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @elseif ($paginator->currentPage() == $paginator->lastPage() - 1)
                @foreach ($elements as $element)
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == 1)
                                @php
                                    $firstPage = $page;
                                    $firstUrl = $url;
                                @endphp
                            @endif
                            @if ($page == $paginator->lastPage())
                                @php
                                    $lastPage = $page;
                                @endphp
                            @endif
                            @if ($page >= $paginator->lastPage() - 2 && $page <= $paginator->lastPage())
                                @if ($page == $paginator->currentPage())
                                    <div class="rounded-circle fanumber page-num fs-4 fw-semibold blue active">
                                        {{ $page }}
                                    </div>
                                @else
                                    <a href="{{ $url }}" class="rounded-circle fanumber page-num fs-4 fw-semibold blue" aria-label="page number">
                                        {{ $page }}
                                    </a>
                                @endif
                            @elseif ($page == $paginator->lastPage() - 3)
                                <a href="{{ $firstUrl }}" class="rounded-circle fanumber page-num fs-4 fw-semibold blue" aria-label="page number">
                                    {{ $firstPage }}
                                </a>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @else
                @foreach ($elements as $element)
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                                 @if ($page == 1)
                                    @php
                                        $firstPage = $page;
                                        $firstUrl = $url;
                                    @endphp
                                @endif
                                
                            @if ($page >= $paginator->currentPage() - 1 && $page <= $paginator->currentPage() + 1)
                                @if ($page == $paginator->currentPage())
                                    <div class="rounded-circle fanumber page-num fs-4 fw-semibold blue active">
                                        {{ $page }}
                                    </div>
                                @else
                                    <a href="{{ $url }}" class="rounded-circle fanumber page-num fs-4 fw-semibold blue" aria-label="page number">
                                        {{ $page }}
                                    </a>
                                @endif
                            @elseif ($page == $paginator->currentPage() - 2)
                                <a href="{{ $firstUrl }}" class="rounded-circle fanumber page-num fs-4 fw-semibold blue" aria-label="page number">
                                    {{ $firstPage }}
                                </a>
                            @elseif($page == $paginator->currentPage() + 2)
                                <a href="{{ $paginator->url($paginator->lastPage()) }}" class="rounded-circle fanumber page-num fs-4 fw-semibold blue" aria-label="page number">
                                    {{ $paginator->lastPage() }}
                                </a>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif
        </div>    
        @if (! is_null($paginator->nextPageUrl()))
            <div class="bg-white rounded-circle  page-item-shadow page-item">
                <a href="{{ $paginator->nextPageUrl() }}" aria-label="next page">
                    <img src="{{ asset('assets/frontend/img/companies/arrow-left-solid.svg') }}" class="page-arrow" alt="next page" height="15" width="20">
                </a>
            </div>
        @endif
    </div>
@endif



{{-- 
@if ($paginator->hasPages())
    <div class="mx-auto mt-4 page-wrapper">
        <ul class="pagination-custom justify-content-center p-2">
            
            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="page-item rounded-2">
                        <span class="page-link">{{ $element }}</span>
                    </li>
                @endif
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item rounded-2 active">
                                <span class="page-link">{{ $page }}</span>
                            </li>
                        @else
                        <a class="page-link" href="{{ $url }}">
                            <li class="page-item rounded-2">
                                {{ $page }}
                            </li>
                        </a>
                        @endif
                    @endforeach
                @endif
            @endforeach
            <li class="page-item rounded-2 active">
                <a class="page-link">اولین</a>
            </li>
            
            <li class="page-item rounded-2"><a class="page-link" href="#">۲</a></li>
            <li class="page-item rounded-2"><a class="page-link" href="#">۳</a></li>
            <li class="page-item rounded-2 active">
                <a class="page-link" href="#">آخرین</a>
            </li>
        </ul>
    </div>
@endif --}}