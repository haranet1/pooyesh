@if ($paginator->hasPages())
    <ul class="pagination float-end">
        @if ($paginator->onFirstPage())
            <li class="page-item page-prev disabled">
                <a class="page-link" href="javascript:void(0)" tabindex="-1">قبلی</a>
            </li>
        @else
            <li class="page-item page-prev ">
                <a class="page-link" rel="prev" href="{{ $paginator->previousPageUrl() }}" tabindex="-1">قبلی</a>
            </li>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="disabled page-item"><span class="page-link">{{ $element }}</span></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><a class="page-link" href="javascript:void(0)">{{ $page }}</a></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())
            <li class="page-item page-next">
                <a class="page-link" rel="next" href="{{ $paginator->nextPageUrl() }}">بعدی</a>
            </li>
        @else
            <li class="page-item disabled page-next">
                <a class="page-link" href="javascript:void(0)">بعدی</a>
            </li>
        @endif
    </ul>
@endif
