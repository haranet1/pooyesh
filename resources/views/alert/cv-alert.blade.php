@php
    $user = Auth::user();
    $missingItems = [];

    if ($user->skills->isEmpty()) {
        $missingItems['skills'] = '<a class="text-dark" href='.route('skills.cv').'>بخش مهارت های رزومه خود را تکمیل نکرده اید</a>';
        
    }
    if ($user->knowledges->isEmpty()) {
        $missingItems['knowledges'] = '<a class="text-dark" href='.route('knowledgs.cv').'>بخش دانش رزومه خود را تکمیل نکرده اید</a>';
    }
    if ($user->abilities->isEmpty()) {
        $missingItems['abilities'] = '<a class="text-dark" href='.route('abilities.cv').'>بخش توانایی های رزومه خود را تکمیل نکرده اید</a>';
    }
    if ($user->technologySkills->isEmpty()) {
        $missingItems['technologySkills'] = '<a class="text-dark" href='.route('technology.skills.cv').'>بخش مهارت های فنی رزومه خود را تکمیل نکرده اید</a>';
    }
    if ($user->workStyles->isEmpty()) {
        $missingItems['workStyles'] = '<a class="text-dark" href='.route('work.styles.cv').'>بخش ویژگی های کاری رزومه خود را تکمیل نکرده اید</a>';
    }
    if ($user->languages->isEmpty()) {
        $missingItems['languages'] = '<a class="text-dark" href='.route('lang.cv').'>بخش زبان های خارجی رزومه خود را تکمیل نکرده اید</a>';
    }
    if ($user->work_experiences->isEmpty()) {
        $missingItems['work_experiences'] = '<a class="text-dark" href='.route('work.experiences.cv').'>بخش سابقه کاری رزومه خود را تکمیل نکرده اید</a>';
    }
    if ($user->social_networks->isEmpty()) {
        $missingItems['social_networks'] = '<a class="text-dark" href='.route('socials.cv').'>بخش شبکه های اجتماعی رزومه خود را تکمیل نکرده اید</a>';
    }
    if ($user->academicInfos->isEmpty()) {
        $missingItems['academicInfos'] = '<a class="text-dark" href='.route('academic.info.cv').'>بخش اطلاعات آکادمیکی رزومه خود را تکمیل نکرده اید</a>';
    }
    if ($user->certificates->isEmpty()) {
        $missingItems['certificates'] = '<a class="text-dark" href='.route('certificates.cv').'>بخش گواهینامه رزومه خود را تکمیل نکرده اید</a>';
    }

@endphp
@if (!empty($missingItems))
    <div class="row px-3 mt-5">
        <div class="card px-0 mb-0">
            <div class="card-header bg-warning">
                <img src="{{ asset('assets/panel/images/icon/SVG/warning.svg') }}" class="me-2" width="32px" height="32px" alt="">
                <div class="" style="color: #654c03">
                    <h4 class="card-title">رزومه خود را تکمیل کنید !</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach ($missingItems as $key => $value)
                        <div class="col-12 col-md-4">
                            <label class="selectgroup-item alert-warning rounded-3">
                                <span class="selectgroup-button"> 
                                    <i class="fa fa-hand-pointer-o text-warning"></i>
                                    @php
                                        echo $value;
                                    @endphp 
                                </span>
                            </label>
                        </div>
                         
                    @endforeach
                </div>
            </div>
        </div>
    
    </div>
@endif

