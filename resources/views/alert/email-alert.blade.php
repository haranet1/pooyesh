<div class="row px-3 my-5 {{session('mustVerifyEmail') && ! session('emailVerificationSended')?'':'d-none'}}" id="warningRow">
    <div class="card px-0">
        <div class="card-header bg-warning">
            <img src="{{ asset('assets/panel/images/icon/SVG/warning.svg') }}" class="me-2" width="32px" height="32px" alt="">
            <div class="" style="color: #654c03">
                <strong>ایمیل شما تایید نشده است !</strong> <br>
                <p class="text-muted mb-0 mt-1">
                    برای دریافت اطلاع رسانی ها و رویداد ها ایمیل خود را تایید نمایید.
                    تا زمان تایید ایمیل امکانات سایت برای شما قابل استفاده نخواهد بود.
                </p>
            </div>                
        </div>
        <div class="card-body ">
            <div class="form-horizontal px-5">
                <div class="row mb-4 justify-content-center">
                    <label id="emailLbl" for="inputEmail3" class="col-md-2 form-label">ایمیل</label>
                    <div class="col-md-7">
                        <input type="hidden" id="verifyEmailUrl" value="{{ route('auth.email.verify') }}">
                        @if (Auth::user()->email)
                            <input type="email" class="form-control mb-2" id="emailInput" placeholder="info@example.com" name="email" value="{{ Auth::user()->email }}">
                            <span dir="rtl" class="text-warning" id="emailErr">در صورتی که ایمیل {{Auth::user()->email}} صحیح نیست، آن را تغییر دهید.</span>
                        @else
                            <input type="email" class="form-control" id="emailInput" placeholder="info@example.com" name="email">
                            <span class="text-warning d-none" id="emailErr">در صورتی که ایمیل {{Auth::user()->email}} صحیح نیست، آن را تغییر دهید.</span>
                        @endif
                    </div>
                </div>
                <div class="mb-0 mt-4 row justify-content-center">
                    <div class="col-md-5">
                        <button id="emailBtn" class="btn btn-success" onclick="codeRequest()">ارسال ایمیل تاییدیه</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row px-3 my-5 {{! session('mustVerifyEmail') && session('emailVerificationSended')?'':'d-none'}}" id="successRow">
    <div class="card px-0">
        <div class="card-header bg-success">
            <input type="hidden" id="verifyEmailUrl2" value="{{ route('auth.email.verify') }}">
            <img src="{{ asset('assets/panel/images/icon/SVG/success.svg') }}" class="me-2" width="32px" height="32px" alt="">
            <div class="text-white">
                <span>ایمیل تاییده به {{ Auth::user()->email }} ارسال شده است. بر روی لینک داخل ایمیل کلیک کنید تا ایمیل شما تایید شود.</span>
            </div>                
        </div>
        <div class="card-body ">
            <div class="form-horizontal px-5">
                <div class="row mb-4 justify-content-center align-items-center">
                    <div class="col-8">
                        <span>ایمیل خود را چک کنید و در صورتی که ایمیل در inbox شما نبود، حتما قسمت spam ایمیل خود را نیز چک کنید.</span>

                        <span>در صورتی که ایمیل را دریافت نکرده اید بعد از <span id="timer"></span> دوباره درخواست دهید.</span>
                    </div>
                    
                    <div class="col-md-3">
                        <button class="btn btn-success" id="sendAgainBtn" disabled="" onclick="sendAgainEmail()">ارسال مجدد ایمیل</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row px-3 my-5 {{ session('emailHasVerified')?'':'d-none'}}" id="successRow2">
    <div class="card px-0">
        <div class="card-header bg-success">
            <img src="{{ asset('assets/panel/images/icon/SVG/success.svg') }}" class="me-2" width="32px" height="32px" alt="">
            <div class="text-white">
                <span>ایمیل شما با موفقیت تایید شد.</span>
            </div>                
        </div>
        @php
            session()->forget('emailHasVerified' , true);
        @endphp
    </div>
</div>
