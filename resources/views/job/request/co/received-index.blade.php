@extends('panel.company.master')

@section('title' , $config['title'])

@section('main')
    <div class="page-header">
        <h1 class="page-title">{{ $config['title'] }}</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{ $config['title'] }}</a></li>
            </ol>
        </div>
    </div>

    @if(Session::has('success'))
        <div class="alert alert-success mt-2 text-center">
            <h5>{{Session::pull('success')}}</h5>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger mt-2 text-center">
            <h5>{{Session::pull('error')}}</h5>
        </div>
    @endif

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
            <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
            <div class="flex-grow-1">
                <p class="f-16" >
                    <strong>{{ $config['alertDescription'] }}</strong>
                </p>
            </div>
        </div>
    </div>

    @if(!is_null($notAllowed)) 
    <div class="alert alert-warning alert-dismissible fade-show">
        <div class="d-flex align-items-center justify-content-between flex-md-row flex-column row-gap-2">
            <div class="d-flex align-items-center w-75">
                <img class="me-4" src="{{asset('assets/images/danger-icon.svg')}}" style="max-width: 33px" alt="info-icon">
                <p class="f-16">
                    <strong>تعداد مجاز تایید درخواست کارآموزی توسط شما به پایان رسیده است</strong>
                </p>
            </div>
            <a href="{{route('index.package.company')}}" class="btn btn-info mx-2" type="button">
            مشاهده و تایید درخواست های بیشتر
            </a>
        </div>
    </div>
    @endif

    <div class="row row-cards justify-content-center px-0 px-md-3">
        <div class="col-xl-12 col-lg-12 px-0">
            @if ($positions->count() > 0)
                <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                    @foreach ($positions as $position)
                        <div class="col-12 col-md-4  mb-3  ">
                            <div class="card radius-10 border-0 border-3 border-info shadow-lg">
                                <div class="card-body pb-2">
                                    <div class="d-flex align-items-center position-relative">
                                        <div class="flex-shrink-0">
                                            @php
                                                $img = rand(1,5);
                                            @endphp
                                            <img class="img-md " width="70px" height="70px" src="{{asset('assets/panel/images/job-icon/job-icon-'.$img.'.svg')}}" alt="Profile Picture" loading="lazy">
                                        </div>
                                        <div class="flex-grow-1 ms-3">
                                            <span class="badge bg-primary"> {{ $config['jobTypeTitle'] }} </span>
                                            @if($position->job)
                                                <a target="_blank" href="{{route('job', $position->id)}}" class="text-description text-dark">
                                                    <p class="h4 mt-2">{{$position->title}}</p>
                                                </a>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    @php
                                        if(Route::is('co.received.intern.req.job')){
                                            $sentReqCount = 0;
                                            foreach($position->requests as $req){
                                                if($req->sender_id != Auth::id() && is_null($req->status) && $req->sender->groupable_type != \App\Models\ApplicantInfo::class){
                                                    $sentReqCount++;

                                                }

                                            }
                                        } else {
                                            $sentReqCount = 0;
                                            foreach($position->requests as $req){
                                                if($req->sender_id != Auth::id() && is_null($req->status)){
                                                    $sentReqCount++;

                                                }

                                            }
                                        }
                                    @endphp

                                        @if ($sentReqCount > 0)
                                            @if(!is_null($notAllowed))
                                                <div class="btn btn-dark w-100 disabled">
                                                    مشاهده {{ $sentReqCount }} درخواست
                                                </div>
                                            @else
                                                <a href="{{ route($config['reqByIdLink'] , $position->id) }}" 
                                                    target="_blank" class="btn btn-info w-100">
                                                    مشاهده {{ $sentReqCount }} درخواست
                                                </a>
                                            @endif
                                        @else
                                            <div class="btn btn-dark w-100 disabled">
                                                درخواستی وجود ندارد
                                            </div>
                                       @endif

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="alert alert-warning w-100 text-center">
                    @lang('public.no_info')
                </div>
            @endif
        </div>
    </div>
@endsection
