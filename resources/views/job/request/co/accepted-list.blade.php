@extends('panel.company.master')

@section('title' , $config['title'])

@section('main')
    <div class="page-header">
        <h1 class="page-title">{{ $config['title'] }}</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{ $config['title'] }}</a></li>
            </ol>
        </div>
    </div>

    @if(Session::has('success'))
        <div class="alert alert-success mt-2 text-center">
            <h5>{{Session::pull('success')}}</h5>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger mt-2 text-center">
            <h5>{{Session::pull('error')}}</h5>
        </div>
    @endif

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
            <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
            <div class="flex-grow-1">
                <p class="f-16" >
                    <strong>{{ $config['alertDescription'] }}</strong>
                </p>
            </div>
        </div>
    </div>

    <div class="row row-cards justify-content-center px-0 px-md-3">
        <div class="col-xl-12 col-lg-12 px-0">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="list-group list-group-transparent mb-0 mail-inbox pb-3 panel-tabs">
                                    <a href="#tab1" data-bs-toggle="tab" class="list-group-item d-flex align-items-center border rounded-3 justify-self-center mb-2 active">
                                        درخواست های در حال انجام
                                    </a>
                                    <a href="#tab2" data-bs-toggle="tab" class="list-group-item d-flex align-items-center border rounded-3 justify-self-center mb-2">
                                        درخواست های لغو شده
                                    </a>
                                    <a href="#tab3" data-bs-toggle="tab" class="list-group-item d-flex align-items-center border rounded-3 justify-self-center mb-2">
                                        درخواست های رد شده
                                    </a>
                                    <a href="#tab4" data-bs-toggle="tab" class="list-group-item d-flex align-items-center border rounded-3 justify-self-center mb-2">
                                        درخواست های به اتمام رسیده
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-9">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <div class="row">
                                    @if ($activeRequests->count() > 0)
                                        @foreach ($activeRequests as $ar)
                                            @php
                                                $img = rand(1,5);
                                            @endphp
                                            <div class="col-12 mb-3 px-md-5">
                                                <div class="card radius-10 border-0 border-3 border-info shadow-lg {{ $ar->status == 2 ? 'disable' : ''}}">
                                                    <div class="card-body">

                                                        <div class="row align-items-center">
                                                            <div class="col-12 col-md-10">
                                                                <div class="row mb-4">
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-5">
                                                                        <div class="d-flex align-items-center gap-2">
                                                                            <span class="badge bg-primary mb-1">{{ $config['jobTypeTitle'] }}</span>
                                                                            <a href="{{ route('job' , $ar->job->id) }}" class="d-flex text-description text-dark" target="_blank">
                                                                                <p class="h4 mb-0">{{ $ar->job->title }}</p> 
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-7">
                                                                        @if ($ar->sender_id == Auth::id())
                                                                            <div class="d-flex align-items-center gap-2">
                                                                                <span>درخواست شما به دانشجو </span>  
                                                                                <a class="text-description d-flex" href="{{ route('candidate.single' , $ar->receiver_id) }}" target="_blank">
                                                                                    <h5 class="m-0">{{$ar->receiver->name." ".$ar->receiver->family}}</h5>
                                                                                </a>
                                                                            </div>
                                                                        @else
                                                                            <div class="d-flex align-items-center gap-2">
                                                                                <span> درخواست دانشجو</span>
                                                                                <a class="text-description d-flex" href="{{ route('candidate.single' , $ar->sender_id) }}" target="_blank">
                                                                                    <h5 class="m-0">{{$ar->sender->name." ".$ar->sender->family}}</h5>
                                                                                </a>
                                                                                <span> به شما</span>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-2">
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-4">
                                                                        <div class="d-flex">
                                                                            <div class="text-gray">
                                                                                تاریخ ارسال درخواست :
                                                                            </div>
                                                                            <strong>{{ verta($ar->created_at)->formatJalaliDate() }}</strong>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-8">
                                                                        <div class="d-flex">
                                                                            @if ($ar->process && getPooyeshProcessStatus($ar->process->id) != '')
                                                                                <span class="text-gray">
                                                                                    آخرین وضعیت :
                                                                                </span>
                                                                                <div class="">
                                                                                    <strong class="">{{ getPooyeshProcessStatus($ar->process->id) }} , {{ getNextPooyeshProcess($ar->process->getMostRecentlyUpdatedColumn()) }}</strong>
                                                                                </div>
                                                                                
                                                                                @if ($ar->status == 2)
                                                                                    <span class="badge bg-danger text-center">لغو شده</span>
                                                                                @endif
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="row mb-2">
                                                                    @if ($ar->salary)
                                                                        <div class="col-12 mb-2 mb-md-0">
                                                                            <div class="d-flex aligh-items-center gap-1 mt-2">
                                                                                <div class="text-gray">
                                                                                    حقوق پیشنهادی شما : 
                                                                                </div>
                                                                                <strong class="">
                                                                                    {{ $ar->salary }} تومان
                                                                                </strong>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="d-flex algin-items-center">
                                                                    <div class="mx-2 mt-3">
                                                                        <a href="" class="btn btn-sm btn-danger {{ $ar->status == 2 ? 'disabled' : '' }}"
                                                                        data-bs-toggle="modal" 
                                                                        data-bs-target="#cancle-modal-{{$ar->id}}">لغو</a>

                                                                        <a href="javascript:void(0)"class="btn btn-sm btn-primary {{ $ar->status == 2 ? 'disabled' : '' }}"
                                                                        type="button" data-bs-target="#modal-ar-{{$ar->id}}"
                                                                        data-bs-toggle="modal">
                                                                            نمایش روند درخواست
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-12 col-md-2 d-none d-md-block">
                                                                <div class="d-flex aligh-items-center justify-self-end">
                                                                    <div class="m-2">
                                                                        <img width="90px" height="90px" class="img-md" src="{{asset('assets/panel/images/icon/requests/req'.$img.'.svg')}}" alt="Profile Picture" loading="lazy">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-center gap-3">
                                            <div class="modal fade " id="modal-ar-{{$ar->id}}">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content modal-content-demo">
                                                            <div class="modal-header">
                                                                <h6 class="modal-title">
                                                                    روند پویش
                                                                    کارجو {{$ar->sender->name." ".$ar->sender->family}}</h6>
                                                                <button
                                                                    aria-label="Close"
                                                                    class="btn-close"
                                                                    data-bs-dismiss="modal">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div
                                                                    class="card-body">
                                                                    <div
                                                                        class="vtimeline">
                                                                        @php
                                                                            $timeline = getPooyeshProcess($ar->id);
                                                                            $flag = 0;
                                                                        @endphp
                                                                        @foreach($timeline as $key => $value)
                                                                            @if($flag % 2 == 0)
                                                                                <div
                                                                                    class="timeline-wrapper timeline-wrapper-primary">
                                                                                    <div class="avatar avatar-md timeline-badge">
                                                                                        <span class="timeline-icon">
                                                                                            <svg style="width:25px;height:25px" viewBox="0 0 24 24">
                                                                                                <path fill="currentColor" d="M4,2A2,2 0 0,0 2,4V11C2,11.55 2.22,12.05 2.59,12.42L11.59,21.42C11.95,21.78 12.45,22 13,22C13.55,22 14.05,21.78 14.41,21.41L21.41,14.41C21.78,14.05 22,13.55 22,13C22,12.45 21.77,11.94 21.41,11.58L12.41,2.58C12.05,2.22 11.55,2 11,2H4V2M11,4L20,13L13,20L4,11V4H11V4H11M6.5,5A1.5,1.5 0 0,0 5,6.5A1.5,1.5 0 0,0 6.5,8A1.5,1.5 0 0,0 8,6.5A1.5,1.5 0 0,0 6.5,5M10.95,10.5C9.82,10.5 8.9,11.42 8.9,12.55C8.9,13.12 9.13,13.62 9.5,14L13,17.5L16.5,14C16.87,13.63 17.1,13.11 17.1,12.55A2.05,2.05 0 0,0 15.05,10.5C14.5,10.5 13.97,10.73 13.6,11.1L13,11.7L12.4,11.11C12.03,10.73 11.5,10.5 10.95,10.5Z"/>
                                                                                            </svg>
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="timeline-panel">
                                                                                        <div class="timeline-heading">
                                                                                            <h6 class="timeline-title">{{$key}}</h6>
                                                                                        </div>
                                                                                        <div class="timeline-body">
                                                                                            <p></p>
                                                                                        </div>
                                                                                        <div class="timeline-footer d-flex align-items-center flex-wrap">
                                                                                            <span class="ms-auto">
                                                                                                <i class="fe fe-calendar text-muted mx-1"></i>
                                                                                                {{verta($value)->formatDate()}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @else
                                                                                <div class="timeline-wrapper timeline-inverted timeline-wrapper-secondary">
                                                                                    <div class="avatar avatar-md timeline-badge">
                                                                                        <span class="timeline-icon">
                                                                                            <svg style="width:26px;height:26px" viewBox="0 0 24 24">
                                                                                                <path fill="currentColor" d="M12 4C14.2 4 16 5.8 16 8C16 10.1 13.9 13.5 12 15.9C10.1 13.4 8 10.1 8 8C8 5.8 9.8 4 12 4M12 2C8.7 2 6 4.7 6 8C6 12.5 12 19 12 19S18 12.4 18 8C18 4.7 15.3 2 12 2M12 6C10.9 6 10 6.9 10 8S10.9 10 12 10 14 9.1 14 8 13.1 6 12 6M20 19C20 21.2 16.4 23 12 23S4 21.2 4 19C4 17.7 5.2 16.6 7.1 15.8L7.7 16.7C6.7 17.2 6 17.8 6 18.5C6 19.9 8.7 21 12 21S18 19.9 18 18.5C18 17.8 17.3 17.2 16.2 16.7L16.8 15.8C18.8 16.6 20 17.7 20 19Z"/>
                                                                                            </svg>
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="timeline-panel">
                                                                                        <div class="timeline-heading">
                                                                                            <h6 class="timeline-title">{{$key}}</h6>
                                                                                        </div>
                                                                                        <div class="timeline-body">

                                                                                        </div>
                                                                                        <div class="timeline-footer d-flex align-items-center flex-wrap">
                                                                                            <span class="ms-auto">
                                                                                                <i class="fe fe-calendar text-muted mx-1"></i>
                                                                                                {{verta($value)->formatDate()}}
                                                                                                </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                            @php
                                                                                $flag++;
                                                                            @endphp
                                                                        @endforeach

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="modal-footer">
                                                                <button
                                                                    class="btn btn-light"
                                                                    data-bs-dismiss="modal">
                                                                    بستن
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="cancle-modal-{{$ar->id}}">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content modal-content-demo">
                                                        <div class="modal-header">
                                                            <h6 class="modal-title">ارسال تیکت</h6>
                                                            <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{ route('co.cancel.req.job') }}" method="POST" enctype="multipart/form-data">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <input type="hidden" name="request_id" value="{{$ar->id}}">
                                                                <div class="mb-3">
                                                                    <label for="recipient-name" class="col-form-label">موضوع :</label>
                                                                    <input name="subject" value="لغو {{ $config['jobTypeTitle'] }}" type="text" class="form-control" id="recipient-name">
                                                                </div>
                                                                @error('subject') <small id="subjectErr" class="text-danger">{{$message}}</small> @enderror
                                                                <div class="mb-3">
                                                                    <label for="message-text" class="col-form-label">متن :</label>
                                                                    <textarea name="content" class="form-control" id="message-text"></textarea>
                                                                </div>
                                                                @error('content') <small id="contentErr" class="text-danger">{{$message}}</small> @enderror
                                                                <div class="mb-3">
                                                                    <label for="attachment-file" class="col-form-label">فایل ضمیمه :</label>
                                                                    <input name="attachment" type="file" class="form-control" id="attachment-file">
                                                                </div>
                                                                @error('attachment') <small id="attachmentErr" class="text-danger">{{$message}}</small> @enderror
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn ripple btn-primary" type="submit">ارسال و لغو درخواست</button>
                                                                <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach 
                                    @else
                                        <div class="alert alert-warning w-100 text-center">
                                            @lang('public.no_info')
                                        </div>
                                    @endif
                                </div>

                            </div>
                            <div class="tab-pane" id="tab2">
                                <div class="row">
                                    @if ($canceldRequests->count() > 0)
                                        @foreach ($canceldRequests as $cr)
                                            @php
                                                $img = rand(1,5);
                                            @endphp
                                            <div class="col-12 mb-3 px-md-5">
                                                <div class="card radius-10 border-0 border-3 border-info shadow-lg {{ $cr->status == 2 ? 'disable' : ''}}">
                                                    <div class="card-body py-2">

                                                        <div class="row align-items-center">
                                                            <div class="col-12 col-md-10">
                                                                <div class="row mb-4">
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-4">
                                                                        <div class="d-flex align-items-center gap-2">
                                                                            <span class="badge bg-primary mb-1">{{ $config['jobTypeTitle'] }}</span>
                                                                            <a href="{{ route('job' , $cr->job->id) }}" class="d-flex text-description text-dark" target="_blank">
                                                                                <p class="h4 mb-0">{{ $cr->job->title }}</p> 
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-8">
                                                                        @if ($cr->sender_id == Auth::id())
                                                                            <div class="d-flex align-items-center gap-2">
                                                                                <span>درخواست شما به دانشجو</span>
                                                                                <a class="text-description" href="{{ route('candidate.single' , $cr->receiver_id) }}" target="_blank">
                                                                                    <h5 class="m-0">{{$cr->receiver->name." ".$cr->receiver->family}}</h5>
                                                                                </a>
                                                                            </div>
                                                                        @else
                                                                            <div class="d-flex align-items-center gap-2">
                                                                                <span>درخواست دانشجو</span>
                                                                                <a class="text-description d-flex" href="{{ route('candidate.single' , $cr->sender_id) }}" target="_blank">
                                                                                    <h5 class="m-0">{{$cr->sender->name." ".$cr->sender->family}}</h5>
                                                                                </a>
                                                                                <span> به شما</span>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-2">
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-4">
                                                                        <div class="d-flex">
                                                                            <div class="text-gray">
                                                                                تاریخ ارسال درخواست :
                                                                            </div>
                                                                            <strong>{{ verta($cr->created_at)->formatJalaliDate() }}</strong>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-8">
                                                                        <div class="d-flex">
                                                                            @if ($cr->process && getPooyeshProcessStatus($cr->process->id) != '')
                                                                                <span class="text-gray">
                                                                                    آخرین وضعیت :
                                                                                </span>
                                                                                <div class="">
                                                                                    <strong class="">{{ getPooyeshProcessStatus($cr->process->id) }} , {{ getNextPooyeshProcess($cr->process->getMostRecentlyUpdatedColumn()) }}</strong>
                                                                                </div>
                                                                                
                                                                                @if ($cr->status == 2)
                                                                                    <span class="badge bg-danger text-center py-3 py-md-1">لغو شده</span>
                                                                                @endif
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="row mb-2">
                                                                    @if ($cr->salary)
                                                                        <div class="col-12 mb-2 mb-md-0">
                                                                            <div class="d-flex aligh-items-center gap-1 mt-2">
                                                                                <div class="text-gray">
                                                                                    حقوق پیشنهادی شما : 
                                                                                </div>
                                                                                <strong class="">
                                                                                    {{ $ar->salary }} تومان
                                                                                </strong>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="col-12 col-md-2 d-none d-md-block">
                                                                <div class="d-flex aligh-items-center justify-self-end">
                                                                    <div class="m-2">
                                                                        <img width="90px" height="90px" class="img-md" src="{{asset('assets/panel/images/icon/requests/req'.$img.'.svg')}}" alt="Profile Picture" loading="lazy">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach 
                                    @else
                                        <div class="alert alert-warning w-100 text-center">
                                            @lang('public.no_info')
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="tab-pane" id="tab3">
                                <div class="row">
                                    @if ($rejecedRequests->count() > 0)
                                        @foreach ($rejecedRequests as $rr)
                                            @php
                                                $img = rand(1,5);
                                            @endphp
                                            <div class="col-12 mb-3 px-md-5">
                                                <div class="card radius-10 border-0 border-3 border-info shadow-lg {{ $rr->status == 2 ? 'disable' : ''}}">
                                                    <div class="card-body">

                                                        <div class="row align-items-center">
                                                            <div class="col-12 col-md-10">
                                                                <div class="row mb-4">
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-5">
                                                                        <div class="d-flex align-items-center gap-2">
                                                                            <span class="badge bg-primary mb-1">{{ $config['jobTypeTitle'] }}</span>
                                                                            <a href="{{ route('job' , $rr->job->id) }}" class="d-flex text-description text-dark" target="_blank">
                                                                                <p class="h4 mb-0">{{ $rr->job->title }}</p> 
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-7">
                                                                        @if ($rr->sender_id == Auth::id())
                                                                            <div class="d-flex align-items-center gap-2">
                                                                                <span>درخواست شما به دانشجو</span>
                                                                                <a class="text-description" href="{{ route('candidate.single' , $rr->receiver_id) }}" target="_blank">
                                                                                    <h5 class="m-0">{{$rr->receiver->name." ".$rr->receiver->family}}</h5>
                                                                                </a>
                                                                            </div>
                                                                        @else
                                                                            <div class="d-flex align-items-center gap-2">
                                                                                <span>درخواست دانشجو</span>
                                                                                <a class="text-description d-flex" href="{{ route('candidate.single' , $rr->sender_id) }}" target="_blank">
                                                                                    <h5 class="m-0">{{$rr->sender->name." ".$rr->sender->family}}</h5>
                                                                                </a>
                                                                                <span> به شما</span>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-2">
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-4">
                                                                        <div class="d-flex">
                                                                            <div class="text-gray">
                                                                                تاریخ ارسال درخواست :
                                                                            </div>
                                                                            <strong>{{ verta($rr->created_at)->formatJalaliDate() }}</strong>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12 mb-2 mb-md-0 col-md-8">
                                                                        <div class="d-flex">
                                                                            @if ($rr->process && getPooyeshProcessStatus($rr->process->id) != '')
                                                                                <span class="text-gray">
                                                                                    آخرین وضعیت :
                                                                                </span>
                                                                                <div class="">
                                                                                    <strong class="">{{ getPooyeshProcessStatus($rr->process->id) }} , {{ getNextPooyeshProcess($rr->process->getMostRecentlyUpdatedColumn()) }}</strong>
                                                                                </div>
                                                                                
                                                                                @if ($rr->status == 2)
                                                                                    <span class="badge bg-danger text-center py-3 py-md-1">لغو شده</span>
                                                                                @endif
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                @if ($rr->salary)
                                                                    <div class="row mb-2">
                                                                        <div class="col-12 mb-2 mb-md-0">
                                                                            <div class="d-flex aligh-items-center gap-1 mt-2">
                                                                                <div class="text-gray">
                                                                                    حقوق پیشنهادی شما : 
                                                                                </div>
                                                                                <strong class="">
                                                                                    {{ $rr->salary }} تومان
                                                                                </strong>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>

                                                            <div class="col-12 col-md-2 d-none d-md-block">
                                                                <div class="d-flex aligh-items-center justify-self-end">
                                                                    <div class="m-2">
                                                                        <img width="90px" height="90px" class="img-md" src="{{asset('assets/panel/images/icon/requests/req'.$img.'.svg')}}" alt="Profile Picture" loading="lazy">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach 
                                    @else
                                        <div class="alert alert-warning w-100 text-center">
                                            @lang('public.no_info')
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="tab-pane" id="tab4">
                                
                            </div>
                        </div>
                    </div>

                </div>
            
        </div>
    </div>
@endsection
@section('script')
    <script>
        const packageUrl = "{{ route('index.package.company') }}";
    </script>
    <script src="{{ asset('assets/pooyesh/js/co-request.js') }}"></script>
@endsection
