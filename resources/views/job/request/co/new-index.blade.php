@extends('panel.company.master')

@section('title' , $config['title'])

@section('main')

    <style>
        .dark-mode .accordion-button:not(.collapsed) {
                background: unset;
        }
    </style>
    
    <div class="page-header">
        <h1 class="page-title">{{ $config['title'] }}</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{ $config['title'] }}</a></li>
            </ol>
        </div>
    </div>

    @if(Session::has('success'))
        <div class="alert alert-success mt-2 text-center">
            <h5>{{Session::pull('success')}}</h5>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger mt-2 text-center">
            <h5>{{Session::pull('error')}}</h5>
        </div>
    @endif

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
            <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
            <div class="flex-grow-1">
                <p class="f-16" >
                    <strong>{{ $config['alertDescription'] }}</strong>
                </p>
            </div>
        </div>
    </div>

    <div class="row row-cards justify-content-center px-0 px-md-3">
        <div class="col-xl-12 col-lg-12 px-0">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="list-group list-group-transparent mb-0 mail-inbox pb-3 panel-tabs">
                                    <a href="#tab1" data-bs-toggle="tab" class="list-group-item d-flex align-items-center border rounded-3 justify-self-center mb-2 active">
                                        درخواست های دریافتی
                                    </a>
                                    <a href="#tab2" data-bs-toggle="tab" class="list-group-item d-flex align-items-center border rounded-3 justify-self-center mb-2">
                                        درخواست های ارسالی
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-9">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <div class="row">
                                    <div class="col-12 mb-3 px-md-5">
                                        <div class="card radius-10 border-0 border-3 border-info shadow-lg">
                                            <div class="card-body">
                                                @if ($receiverPositions->count() > 0)
                                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                        @foreach ($receiverPositions as $position)
                                                            <div class="panel panel-default mb-4">
                                                                <div class="panel-heading " role="tab" id="headingOne-{{ $position->id }}">
                                                                    
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="row collapsed accordion-button" data-bs-toggle="collapse" data-bs-parent="#accordion" href="#collapse-{{ $position->id }}" aria-expanded="false" aria-controls="collapse1">
                                                                                <div class="col-2 d-flex justify-content-center">
                                                                                    <a role="button"  class="">
                                                                                        {{-- @php
                                                                                            $img = rand(1,5);
                                                                                        @endphp --}}
                                                                                        <img class="img-sm " width="100px" height="100px" src="{{asset($position->job->category->photo->path)}}" alt="Profile Picture" loading="lazy">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col-9 d-flex align-items-center">
                                                                                    <a role="button" data-bs-toggle="collapse" data-bs-parent="#accordion" href="#collapse-{{ $position->id }}" aria-expanded="false" aria-controls="collapse1" class="collapsed">
                                                                                        <div class="">
                                                                                            <span class="badge bg-primary"> {{ $config['jobTypeTitle'] }} </span>
                                                                                            @if($position->job)
                                                                                                <a target="_blank" href="{{route('job', $position->id)}}" class="text-description text-dark">
                                                                                                    <p class="h4 my-0 mt-3">{{$position->title}}</p>
                                                                                                </a>
                                                                                            @endif
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div id="collapse-{{ $position->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne-{{ $position->id }}" style="">
                                                                    <div class="panel-body px-5">
                                                                        <div class="row">
                                                                            @foreach ($position->requests as $request)
                                                                                <div class="card">
                                                                                    <div class="card-body">
                                                                                        <div class="row">
                                                                                            <div class="col-12 col-md-9 pe-0">
                                                                                                <div class="row ">
                                                                                                    <div class="col-12 mb-2 mb-md-0 col-md-8">
                                                                                                        <div class="d-flex align-items-center gap-2">
                                                                                                            <span>درخواست دانشجو</span>
                                                                                                            <a class="text-description d-flex" href="{{ route('candidate.single' , $request->sender_id) }}" target="_blank">
                                                                                                                <h5 class="m-0">{{$request->sender->name." ".$request->sender->family}}</h5>
                                                                                                            </a>
                                                                                                            <span> به شما</span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row mt-2">
                                                                                                    <div class="col-12 mb-2 mb-md-0">
                                                                                                        <div class="d-flex">
                                                                                                            <div class="text-gray">
                                                                                                                تاریخ ارسال درخواست :
                                                                                                            </div>
                                                                                                            <strong>{{ verta($request->created_at)->formatJalaliDate() }}</strong>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>
                                                                                                @if ($request->salary)
                                                                                                    <div class="row mt-2">
                                                                                                            <div class="col-12 mb-2 mb-md-0">
                                                                                                                <div class="d-flex aligh-items-center gap-1">
                                                                                                                    <div class="text-gray">
                                                                                                                        حقوق پیشنهادی شما : 
                                                                                                                    </div>
                                                                                                                    <strong class="">
                                                                                                                        {{ $request->salary }} تومان
                                                                                                                    </strong>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                @endif
                                                                                            </div>
                                                                                            <div class="col-12 col-md-3 pe-0">
                                                                                                <div class="row mt-2">
                                                                                                    <div class="col-6">
                                                                                                        <a href="javascript:void(0)" onclick="acceptInternRequest({{$request->id}})" class="btn btn-success accept w-100" type="button">
                                                                                                            تایید
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="col-6">
                                                                                                        <button type="button" class="btn btn-danger w-100" data-bs-toggle="modal" data-bs-target="#edit-modal-{{$request->id}}">
                                                                                                            رد
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal fade" id="edit-modal-{{$request->id}}">
                                                                                    <div class="modal-dialog" role="document">
                                                                                        <div class="modal-content modal-content-demo">
                                                                                            <div class="modal-header">
                                                                                                <h6 class="modal-title">ارسال تیکت</h6>
                                                                                                <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                                                    <span aria-hidden="true">×</span>
                                                                                                </button>
                                                                                            </div>
                                                                                            <form action="{{ route('co.reject.req.job') }}" method="POST" enctype="multipart/form-data">
                                                                                                @csrf
                                                                                                <div class="modal-body">
                                                                                                    <input type="hidden" name="request_id" value="{{$request->id}}">
                                                                                                    <input type="hidden" name="receiver_id" value="{{$request->sender_id}}">
                                                                                                    <div class="mb-3">
                                                                                                        <label for="recipient-name" class="col-form-label">موضوع :</label>
                                                                                                        <input name="subject" value="رد درخواست کارآموزی" type="text" class="form-control" id="recipient-name">
                                                                                                    </div>
                                                                                                    @error('subject') <small id="subjectErr" class="text-danger">{{$message}}</small> @enderror
                                                                                                    <div class="mb-3">
                                                                                                        <label for="message-text" class="col-form-label">متن :</label>
                                                                                                        <textarea name="content" class="form-control" id="message-text"></textarea>
                                                                                                    </div>
                                                                                                    @error('content') <small id="contentErr" class="text-danger">{{$message}}</small> @enderror
                                                                                                    <div class="mb-3">
                                                                                                        <label for="attachment-file" class="col-form-label">فایل ضمیمه :</label>
                                                                                                        <input name="attachment" type="file" class="form-control" id="attachment-file">
                                                                                                    </div>
                                                                                                    @error('attachment') <small id="attachmentErr" class="text-danger">{{$message}}</small> @enderror
                                                                                                </div>
                                                                                                <div class="modal-footer">
                                                                                                    <button class="btn ripple btn-primary" type="submit">ارسال و رد درخواست</button>
                                                                                                    <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach

                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @else
                                                    <div class="alert alert-warning w-100 text-center">
                                                        @lang('public.no_info')
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2">
                                <div class="row">
                                    <div class="col-12 mb-3 px-md-5">
                                        <div class="card radius-10 border-0 border-3 border-info shadow-lg">
                                            <div class="card-body">
                                                @if ($senderPositions->count() > 0)
                                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                        @foreach ($senderPositions as $position)
                                                            <div class="panel panel-default mb-4">
                                                                <div class="panel-heading " role="tab" id="headingOne-{{ $position->id }}">
                                                                    
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="row collapsed accordion-button" data-bs-toggle="collapse" data-bs-parent="#accordion" href="#collapse-{{ $position->id }}" aria-expanded="false" aria-controls="collapse1">
                                                                                <div class="col-2 d-flex justify-content-center">
                                                                                    <a role="button"  class="">
                                                                                        {{-- @php
                                                                                            $img = rand(1,5);
                                                                                        @endphp --}}
                                                                                        <img class="img-sm " width="100px" height="100px" src="{{asset($position->job->category->photo->path)}}" alt="Profile Picture" loading="lazy">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col-9 d-flex align-items-center">
                                                                                    <a role="button" data-bs-toggle="collapse" data-bs-parent="#accordion" href="#collapse-{{ $position->id }}" aria-expanded="false" aria-controls="collapse1" class="collapsed">
                                                                                        <div class="">
                                                                                            <span class="badge bg-primary"> {{ $config['jobTypeTitle'] }} </span>
                                                                                            @if($position->job)
                                                                                                <a target="_blank" href="{{route('job', $position->id)}}" class="text-description text-dark">
                                                                                                    <p class="h4 my-0 mt-3">{{$position->title}}</p>
                                                                                                </a>
                                                                                            @endif
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div id="collapse-{{ $position->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne-{{ $position->id }}" style="">
                                                                    <div class="panel-body px-5">
                                                                        <div class="row">
                                                                            @foreach ($position->requests as $request)
                                                                                <div class="card">
                                                                                    <div class="card-body">
                                                                                        <div class="row">
                                                                                            <div class="col-12 col-md-9 pe-0">
                                                                                                <div class="row ">
                                                                                                    <div class="col-12 mb-2 mb-md-0 col-md-8">
                                                                                                        <div class="d-flex align-items-center gap-2">
                                                                                                            <span>درخواست شما به دانشجو</span>
                                                                                                            <a class="text-description d-flex" href="{{ route('candidate.single' , $request->receiver_id) }}" target="_blank">
                                                                                                                <h5 class="m-0">{{$request->receiver->name." ".$request->receiver->family}}</h5>
                                                                                                            </a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row mt-2">
                                                                                                    <div class="col-12 mb-2 mb-md-0">
                                                                                                        <div class="d-flex">
                                                                                                            <div class="text-gray">
                                                                                                                تاریخ ارسال درخواست :
                                                                                                            </div>
                                                                                                            <strong>{{ verta($request->created_at)->formatJalaliDate() }}</strong>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>
                                                                                                @if ($request->salary)
                                                                                                    <div class="row mt-2">
                                                                                                            <div class="col-12 mb-2 mb-md-0">
                                                                                                                <div class="d-flex aligh-items-center gap-1">
                                                                                                                    <div class="text-gray">
                                                                                                                        حقوق پیشنهادی شما : 
                                                                                                                    </div>
                                                                                                                    <strong class="">
                                                                                                                        {{ $request->salary }} تومان
                                                                                                                    </strong>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                    </div>
                                                                                                @endif
                                                                                            </div>
                                                                                            
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                            @endforeach

                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @else
                                                    <div class="alert alert-warning w-100 text-center">
                                                        @lang('public.no_info')
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="alert alert-warning w-100 text-center">
                    @lang('public.no_info')
                </div> --}}
        </div>
    </div>
    <input type="hidden" id="companyAcceptRequestUrl" value="{{ route('co.accept.req.job') }}">
@endsection
@section('script')
    <script>
        const packageUrl = "{{ route('index.package.company') }}";
    </script>
    <script src="{{ asset('assets/pooyesh/js/co-request.js') }}"></script>
@endsection