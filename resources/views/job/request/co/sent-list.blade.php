@extends('panel.company.master')

@section('title' , $config['title'])

@section('main')
    <div class="page-header">
        <h1 class="page-title">{{ $config['title'] }}</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{ $config['title'] }}</a></li>
            </ol>
        </div>
    </div>

    @if(Session::has('success'))
        <div class="alert alert-success mt-2 text-center">
            <h5>{{Session::pull('success')}}</h5>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger mt-2 text-center">
            <h5>{{Session::pull('error')}}</h5>
        </div>
    @endif

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
            <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
            <div class="flex-grow-1">
                <p class="f-16" >
                    <strong>{{ $config['alertDescription'] }}</strong>
                </p>
            </div>
        </div>
    </div>

    <div class="row row-cards justify-content-center px-0 px-md-3">
        <div class="col-xl-12 col-lg-12 px-0">
            @if ($requests->count() > 0)
                <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                    @foreach ($requests as $request)
                        <div class="col-12 col-md-4  mb-3 ">
                            <div class="card radius-10 border-0 border-3 border-info shadow-lg">
                                <div class="card-body pb-2">
                                    <!-- Profile picture and short information -->
                                    <div class="d-flex align-items-center position-relative pb-3">
                                        <div class="flex-shrink-0">
                                            <img width="70px" height="70px" class="img-md rounded-circle" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="Profile Picture" loading="lazy">
                                        </div>
                                        <div class="flex-grow-1 ms-3">
                                            <span class="badge bg-primary"> {{ $config['jobTypeTitle'] }} </span>
                                            
                                            <a class="mt-2 text-description text-dark" href="{{ route('candidate.single' , $request->receiver_id) }}" target="_blank">
                                                <h6 class="mt-2">{{$request->receiver->name." ".$request->receiver->family}}</h6>
                                            </a>
                                            @if ($request->receiver->groupable)
                                                <div class="mt-2">
                                                    <h6>{{$request->receiver->groupable->grade." ".$request->receiver->groupable->major}}</h6>
                                                </div>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row justify-content-center">
                                        <div class="col-10 col-md-6 mt-1 px-1">
                                            <a href="{{ route('candidate.single',$request->receiver_id)}}" target="_blank" class="btn btn-info w-100">
                                                مشاهده
                                            </a>
                                        </div>
                                        {{-- <div class="col-10 col-md-6 mt-1 px-1">
                                            <a href="javascript:void(0)" onclick="acceptInternRequest({{$request->id}})" class="btn btn-success accept w-100" type="button">
                                                تایید
                                            </a>
                                        </div> --}}
                                        <div class="col-10 col-md-6 mt-1 px-1">
                                            <a href="{{route('company.get-std-letter',$request->sender_id)}}"class="btn btn-secondary w-100"type="button">
                                                درخواست معرفی نامه
                                            </a>
                                        </div>
                                        <div class="col-10 col-md-6 mt-1 px-1">
                                            <a href="javascript:void(0)" onclick="cancelInternRequest({{$request->id}})" class="btn btn-danger decline w-100" type="button">
                                                لغو
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    @endforeach
                </div>
            @else
                <div class="alert alert-warning w-100 text-center">
                    @lang('public.no_info')
                </div>
            @endif
        </div>
    </div>
    <input type="hidden" id="companyAcceptRequestUrl" value="{{ route('co.accept.req.job') }}">
    <input type="hidden" id="companyRejectRequestUrl" value="{{ route('co.reject.req.job') }}">
    <input type="hidden" id="companyCancleRequestUrl" value="{{ route('co.cancel.req.job') }}">
@endsection
@section('script')
    <script>
        const packageUrl = "{{ route('index.package.company') }}";
    </script>
    <script src="{{ asset('assets/pooyesh/js/co-request.js') }}"></script>
@endsection