@extends('admin.layouts.master')
@section('title' , $title)
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="d-flex card-header justify-content-between">
                    <h3 class="card-title">{{$title}}</h3>
                    <div class="d-flex align-items-center">
                        <h4 class="card-title fs-7"> تعداد کل : {{ $users->total() }} </h4>
                        <a href="{{ route('emp.intern.req.job') }}" class="btn btn-primary mx-2">صفحه اصلی</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            
                            <form action="{{ route('search.user.option.request.intern') }}" method="get" class="input-group">
                                @csrf
                                <input type="hidden" name="title" value="{{ $title }}">
                                <input type="hidden" name="type" value="std">

                                <input type="text" name="std" class="form-control"
                                    @isset($oldData['std'])
                                        value="{{ $oldData['std'] }}"
                                    @endisset
                                placeholder="نام و نام خانوادگی دانشجو :">

                                <input type="text" name="co" class="form-control" 
                                    @isset($oldData['co'])
                                        value="{{ $oldData['co'] }}"
                                    @endisset
                                placeholder="نام شرکت :">

                                <input type="text" name="job" class="form-control" 
                                    @isset($oldData['job'])
                                        value="{{ $oldData['job'] }}"
                                    @endisset
                                placeholder="موقعیت شغلی :">

                               
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-info form-control div-border-user-company-style">جستجو</button>
                                </div>
                            </form>

                        </div>
                        <div class="col-2 me-2">
                            <a href="{{ route('std-requests.emp.list.request.intern') }}" class="btn btn-warning w-100">رفع فیلتر</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route('export.std-requests.emp.list.request.intern') }}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($users)>0)
                            <table class="table table-info border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-info text-white fs-12">ردیف</th>
                                        <th class="text-center bg-info text-white">دانشجو</th>
                                        <th class="text-center bg-info text-white">شرکت</th>
                                        <th class="text-center bg-info text-white">موقعیت شغلی</th>
                                        <th class="text-center bg-info text-white">تاریخ ارسال</th>
                                    </tr>
                                </thead>
                                <tbody id="co_table">
                                    @php
                                        $counter = (($users->currentPage() -1) * $users->perPage()) + 1;
                                    @endphp
                                    @foreach ($users as $student)
                                        <tr>
                                            <td class="text-nowrap text-center align-middle">{{$counter++}}</td>
                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route('candidate.single' , $student->id) }}" class="text-decoration text-dark">
                                                    <div class="col-5 col-md-9 p-md-0">{{$student->name}} {{$student->family}}</div>
                                                </a>
                                            </td>
                                            <td class="text-nowrap align-middle">
                                                <table class="table mb-0 table-bordered">
                                                    <tbody>
                                                        @foreach ($student->sendRequest as $send)
                                                            @if ($send->type == 'intern')
                                                                <tr>
                                                                    <td class="text-nowrap align-middle">
                                                                        @if ($send->receiver->groupable)
                                                                            <a href="{{ route('company.single' , $send->receiver_id) }}" class="text-decoration text-dark">
                                                                                <div class="col-5 col-md-9 p-md-0">
                                                                                    {{$send->receiver->groupable->name}}
                                                                                </div>
                                                                            </a>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <table class="table mb-0 table-bordered">
                                                    <tbody>
                                                        @foreach ($student->sendRequest as $send)
                                                            @if ($send->type == 'intern' && $send->job)
                                                                <tr>
                                                                    <td class="text-nowrap align-middle">
                                                                        <a href="{{ route('job' , $send->job->id) }}" class="text-decoration text-dark">
                                                                            {{$send->job->title}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <table class="table mb-0 table-bordered">
                                                    <tbody>
                                                        @foreach ($student->sendRequest as $send)
                                                            @if ($send->type == 'intern')
                                                                <tr>
                                                                    <td class="text-center align-middle">
                                                                        {{verta($send->created_at)->formatDate()}}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                @lang('public.no_info')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$users->links('pagination.panel')}}
            </div>
        </div>
    </div>
@endsection
