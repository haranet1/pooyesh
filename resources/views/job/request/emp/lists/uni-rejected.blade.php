@extends('admin.layouts.master')
@section('title' , $title)
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="d-flex card-header justify-content-between">
                    <h3 class="card-title">{{$title}}</h3>
                    <div class="d-flex align-items-center">
                        <h4 class="card-title fs-7">تعداد کل : {{$requests->total()}}</h4>
                        <a href="{{ route('emp.intern.req.job') }}" class="btn btn-primary mx-2">صفحه اصلی</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            
                            <form action="{{ route('uni.search.option.request.intern') }}" method="get" class="input-group">
                                @csrf
                                <input type="hidden" name="title" value="{{ $title }}">
                                <input type="hidden" name="type" value="uniRe">
                                <input type="hidden" name="student" value="sender">
                                <input type="hidden" name="company" value="receiver">

                                <input type="text" name="std" class="form-control"
                                    @isset($oldData['std'])
                                        value="{{ $oldData['std'] }}"
                                    @endisset
                                placeholder="نام و نام خانوادگی فرستنده :">

                                <input type="text" name="co" class="form-control" 
                                    @isset($oldData['co'])
                                        value="{{ $oldData['co'] }}"
                                    @endisset
                                placeholder="نام و نام خانوادگی گیرنده :">

                                <input type="text" name="job" class="form-control" 
                                    @isset($oldData['job'])
                                        value="{{ $oldData['job'] }}"
                                    @endisset
                                placeholder="موقعیت شغلی :">

                               
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-info form-control div-border-user-company-style">جستجو</button>
                                </div>
                            </form>

                        </div>
                        <div class="col-2 me-2">
                            <a href="{{ route('uni-rejected.emp.list.request.intern') }}" class="btn btn-warning w-100">رفع فیلتر</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route('export.uni-rejected.emp.list.request.intern') }}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($requests)>0)
                            <table class="table table-info border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-info text-white fs-12">ردیف</th>
                                        <th class="text-center bg-info text-white">فرستنده</th>
                                        <th class="text-center bg-info text-white">گیرنده</th>
                                        <th class="text-center bg-info text-white">موقعیت شغلی</th>
                                        <th class="text-center bg-info text-white">تاریخ ارسال</th>
                                        <th class="text-center bg-info text-white">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="co_table">
                                    @php
                                        $counter = (($requests->currentPage() -1) * $requests->perPage()) + 1;
                                    @endphp
                                    @foreach ($requests as $request)
                                        <tr>
                                            <td class="text-nowrap text-center align-middle">{{$counter++}}</td>
                                            <td class="text-nowrap align-middle">
                                                @if ($request->sender->groupable_type == \App\Models\StudentInfo::class)
                                                    <a href="{{ route( 'candidate.single' , $request->sender->id) }}" class="text-decoration text-dark">
                                                        <div class="col-5 col-md-9 p-md-0">
                                                            <span class="badge bg-success">دانشجو</span>
                                                            {{$request->sender->name}} {{$request->sender->family}}
                                                        </div>
                                                    </a>
                                                @else
                                                    <a href="{{ route( 'company.single' , $request->sender->id) }}" class="text-decoration text-dark">
                                                        @if ($request->sender->groupable)
                                                            <div class="col-5 col-md-9 p-md-0">
                                                                <span class="badge bg-warning">شرکت</span>
                                                                {{$request->sender->groupable->name}}
                                                            </div>
                                                        @endif
                                                    </a>
                                                @endif
                                            </td>
                                            <td class="text-nowrap align-middle">
                                                @if ($request->receiver->groupable_type == \App\Models\StudentInfo::class)
                                                    <a href="{{ route( 'candidate.single' , $request->receiver->id) }}" class="text-decoration text-dark">
                                                        <div class="col-5 col-md-9 p-md-0">
                                                            <span class="badge bg-success">دانشجو</span>
                                                            {{$request->receiver->name}} {{$request->receiver->family}}
                                                        </div>
                                                    </a>
                                                @else
                                                    <a href="{{ route( 'company.single' , $request->receiver->id) }}" class="text-decoration text-dark">
                                                        @if ($request->receiver->groupable)
                                                            <div class="col-5 col-md-9 p-md-0">
                                                                <span class="badge bg-warning">شرکت</span>
                                                                {{$request->receiver->groupable->name}}
                                                            </div>
                                                        @endif
                                                    </a>
                                                @endif
                                            </td>
                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route( 'job' , $request->job_position_id) }}" class="text-decoration text-dark">
                                                    {{job_type_persian_badge($request->job->type)}}
                                                    @if ($request->job)
                                                        {{$request->job->title}}
                                                    @endif
                                                </a>
                                            </td>
                                            <td class="text-nowrap align-middle">
                                                {{verta($request->created_at)->formatDate()}}
                                            </td>
                                            <td class="text-nowrap align-middle text-center">
                                                @if (is_null($request->process->accepted_by_uni_at))
                                                    @if ($request->status == 1)
                                                        <a class="btn btn-sm btn-success"
                                                        onclick="acceptPooyeshRequest({{$request->id}})"
                                                        href="javascript:void(0)">تایید</a>
                                                        
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                @lang('public.no_info')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="confirmByUniUrl" value="{{ route('emp.accept.req.job') }}">
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/panel/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/pooyesh/js/pooyesh.js') }}"></script>
@endsection
