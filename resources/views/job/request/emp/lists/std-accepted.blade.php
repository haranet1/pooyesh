@extends('admin.layouts.master')
@section('title' , $title)
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif
            <div class="card">
                <div class="d-flex card-header justify-content-between">
                    <h3 class="card-title">{{$title}}</h3>
                    <div class="d-flex align-items-center">
                        <h4 class="card-title fs-7">تعداد کل : {{$requests->total()}}</h4>
                        <a href="{{ route('emp.intern.req.job') }}" class="btn btn-primary mx-2">صفحه اصلی</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            
                            <form action="{{ route('search.option.request.intern') }}" method="get" class="input-group">
                                @csrf
                                <input type="hidden" name="title" value="{{ $title }}">
                                <input type="hidden" name="type" value="stdAc">
                                <input type="hidden" name="student" value="sender">
                                <input type="hidden" name="company" value="receiver">

                                <input type="text" name="std" class="form-control"
                                    @isset($oldData['std'])
                                        value="{{ $oldData['std'] }}"
                                    @endisset
                                placeholder="نام و نام خانوادگی دانشجو :">

                                <input type="text" name="co" class="form-control" 
                                    @isset($oldData['co'])
                                        value="{{ $oldData['co'] }}"
                                    @endisset
                                placeholder="نام شرکت :">

                                <input type="text" name="job" class="form-control" 
                                    @isset($oldData['job'])
                                        value="{{ $oldData['job'] }}"
                                    @endisset
                                placeholder="موقعیت شغلی :">

                               
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-info form-control div-border-user-company-style">جستجو</button>
                                </div>
                            </form>

                        </div>
                        <div class="col-2 me-2">
                            <a href="{{ route('std-accepted.emp.list.request.intern') }}" class="btn btn-warning w-100">رفع فیلتر</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route('export.std-accepted.emp.list.request.intern') }}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="table-responsive table-lg">
                        @if (count($requests)>0)
                            <table class="table table-info border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-info text-white fs-12">ردیف</th>
                                        <th class="text-center bg-info text-white">دانشجو</th>
                                        <th class="text-center bg-info text-white">شرکت</th>
                                        <th class="text-center bg-info text-white">موقعیت شغلی</th>
                                        <th class="text-center bg-info text-white">تاریخ ارسال</th>
                                        <th class="text-center bg-info text-white">امکانات</th>
                                    </tr>
                                </thead>
                                <tbody id="co_table">
                                    @php
                                        $counter = (($requests->currentPage() -1) * $requests->perPage()) + 1;
                                    @endphp
                                    @foreach ($requests as $request)
                                        <tr>
                                            <td class="text-nowrap text-center align-middle">{{$counter++}}</td>
                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route( 'candidate.single' , $request->sender->id) }}" class="text-decoration text-dark">
                                                    <div class="col-5 col-md-9 p-md-0">
                                                        <span class="badge bg-success">دانشجو</span>
                                                        {{$request->sender->name}} {{$request->sender->family}}
                                                    </div>
                                                </a>
                                            </td>
                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route( 'company.single' , $request->receiver->id) }}" class="text-decoration text-dark">
                                                    @if ($request->receiver->groupable)
                                                        <div class="col-5 col-md-9 p-md-0">
                                                            <span class="badge bg-warning">شرکت</span>
                                                            {{$request->receiver->groupable->name}}
                                                        </div>
                                                    @endif
                                                </a>
                                            </td>
                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route( 'job' , $request->job_position_id) }}" class="text-decoration text-dark">
                                                    {{job_type_persian_badge($request->job->type)}}
                                                    @if ($request->job)
                                                        {{$request->job->title}}
                                                    @endif
                                                </a>
                                            </td>
                                            <td class="text-nowrap align-middle">
                                                {{verta($request->created_at)->formatDate()}}
                                            </td>
                                            <td class="text-nowrap align-middle text-center">
                                                @if (is_null($request->process->accepted_by_uni_at) && is_null($request->process->rejected_by_uni_at))
                                                    @if ($request->status == 1)
                                                        <a class="btn btn-sm btn-success"
                                                        onclick="acceptPooyeshRequest({{$request->id}})"
                                                        href="javascript:void(0)">تایید</a>
                                                        <button type="button" class="btn btn-sm btn-danger badge" data-bs-toggle="modal" data-bs-target="#edit-modal-{{$request->id}}">رد</button>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit-modal-{{$request->id}}">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content modal-content-demo">
                                                    <div class="modal-header">
                                                        <h6 class="modal-title">ارسال تیکت</h6>
                                                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{ route('emp.reject.req.job') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <input type="hidden" id="userType" name="userType" value="'std'">
                                                            <input type="hidden" id="request_id" name="request_id" value="{{$request->id}}">
                                                            <div class="mb-3">
                                                                <label for="recipient-name" class="col-form-label">موضوع :</label>
                                                                <input name="subject" value="رد درخواست کارآموزی" type="text" class="form-control" id="recipient-name">
                                                            </div>
                                                            @error('subject') <small id="subjectErr" class="text-danger">{{$message}}</small> @enderror
                                                            <div class="mb-3">
                                                                <label for="message-text" class="col-form-label">متن :</label>
                                                                <textarea name="content" class="form-control" id="message-text"></textarea>
                                                            </div>
                                                            @error('content') <small id="contentErr" class="text-danger">{{$message}}</small> @enderror
                                                            <div class="mb-3">
                                                                <label for="attachment-file" class="col-form-label">فایل ضمیمه :</label>
                                                                <input name="attachment" type="file" class="form-control" id="attachment-file">
                                                            </div>
                                                            @error('attachment') <small id="attachmentErr" class="text-danger">{{$message}}</small> @enderror
                                                            <div class="form-check my-3">
                                                                <input class="form-check-input" value="{{ $request->sender_id }}" type="radio" name="receiver_id" id="student" checked>
                                                                <label class="form-check-label" for="student">
                                                                    ارسال برای دانشجو
                                                                </label>
                                                            </div>
                                                            <div class="form-check ">
                                                                <input class="form-check-input" value="{{ $request->receiver_id }}" type="radio" name="receiver_id" id="company">
                                                                <label class="form-check-label" for="company">
                                                                  ارسال برای شرکت
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn ripple btn-primary" type="submit">ارسال و رد درخواست</button>
                                                            <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                @lang('public.no_info')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$requests->links('pagination.panel')}}
            </div>
        </div>
        <input type="hidden" id="confirmByUniUrl" value="{{ route('emp.accept.req.job') }}">
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/panel/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/pooyesh/js/pooyesh.js') }}"></script>
@endsection
