@extends('admin.layouts.master')
@section('title' , $title)
@section('main')

    <div class="page-header">
        <h1 class="page-title"> {{ $title }} </h1>
        @if(Session::has('success'))
            <div class="row">
                <div class="alert alert-success">
                    {{Session::pull('success')}}
                </div>
            </div>
        @endif
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">داشبورد اصلی</a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="row">
                
                {{-- student section --}}
                <div class="col-12 col-md-6 col-xl-4">

                    <div class="card overflow-hidden border border-success p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('std-requests.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> دانشجویان درخواست داده </h5>
                                    <h2 class="mb-0 number-font">{{ $data['stdHasRequestToCo'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/std.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card overflow-hidden border border-success p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('std-accepted.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> درخواست های تایید شده دانشجو </h5>
                                    <h2 class="mb-0 number-font">{{ $data['acceptedStdReq'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/accept.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card overflow-hidden border border-success p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('std-canceled.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> درخواست های لغو شده دانشجو </h5>
                                    <h2 class="mb-0 number-font">{{ $data['canceledStdReq'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/cancle.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card overflow-hidden border border-success p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('std-rejected.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> درخواست های رد شده دانشجو </h5>
                                    <h2 class="mb-0 number-font">{{ $data['rejectedStdReq'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/reject.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                
                {{-- company section --}}
                <div class="col-12 col-md-6 col-xl-4">

                    <div class="card overflow-hidden border border-info p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('co-requests.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> شرکت های درخواست داده </h5>
                                    <h2 class="mb-0 number-font">{{ $data['coHasRequestToStd'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/co.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card overflow-hidden border border-info p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('co-accepted.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> درخواست های تایید شده شرکت </h5>
                                    <h2 class="mb-0 number-font">{{ $data['acceptedCoReq'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/accept.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card overflow-hidden border border-info p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('co-canceled.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> درخواست های لغو شده شرکت </h5>
                                    <h2 class="mb-0 number-font">{{ $data['canceledCoReq'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/cancle.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card overflow-hidden border border-info p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('co-rejected.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> درخواست های رد شده شرکت </h5>
                                    <h2 class="mb-0 number-font">{{ $data['rejectedCoReq'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/reject.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                
                {{-- university section --}}
                <div class="col-12 col-md-6 col-xl-4">

                    <div class="card overflow-hidden border border-primary p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('uni-accepted.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> تایید درخواست توسط دانشگاه </h5>
                                    <h2 class="mb-0 number-font">{{ $data['acceptedByUni'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/accept.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card overflow-hidden border border-primary p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('uni-rejected.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> رد درخواست توسط دانشگاه </h5>
                                    <h2 class="mb-0 number-font">{{ $data['rejectedByUni'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/reject.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card overflow-hidden border border-primary p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('uni-printed.emp.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> قرار داد های چاپ شده </h5>
                                    <h2 class="mb-0 number-font">{{ $data['contractPrinted'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/print.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card overflow-hidden border border-primary p-0 mb-3">
                        <div class="card-body">
                            <a href="{{ route('done.list.request.intern') }}" class="d-flex text-desciption text-dark">
                                <div class="mt-2">
                                    <h5 class=""> اتمام پویش </h5>
                                    <h2 class="mb-0 number-font">{{ $data['done'] }}</h2>
                                </div>
                                <div class="ms-auto">
                                    <div class="mt-1">
                                        <img width="55px" src="{{ asset('assets/panel/images/icon/pooyesh-dashboard/done.svg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
