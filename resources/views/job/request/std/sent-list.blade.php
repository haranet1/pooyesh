@extends('panel.student.master')

@section('title' , $config['title'])

@section('main')
    <div class="page-header">
        <h1 class="page-title">{{ $config['title'] }}</h1>
        <div>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{ $config['title'] }}</a></li>
            </ol>
        </div>
    </div>

    <div class="row row-cards justify-content-center px-0 px-md-3">
        <div class="col-xl-12 col-lg-12 px-0">
            @if ($requests->count() > 0)
                <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                    @foreach ($requests as $request)
                        <div class="col-12 col-md-4  mb-3  ">
                            <div class="card radius-10 border-0 border-3 border-info shadow-lg">
                                <div class="card-body pb-2">
                                    <!-- Profile picture and short information -->
                                    <div class="d-flex align-items-center position-relative pb-3">
                                        <div class="flex-shrink-0">
                                            <img width="70px" height="70px" class="img-md rounded-circle" src="{{asset('assets/panel/images/icon/SVG/user.svg')}}" alt="Profile Picture" loading="lazy">
                                        </div>
                                        <div class="flex-grow-1 ms-3">
                                            <div class="">
                                                <a class="text-description text-dark" href="{{ route('company.single' , $request->receiver_id) }}">
                                                    <span class=""> شرکت {{ $request->receiver->groupable->name }}</span>
                                                </a>
                                            </div>
                                            <div class="">
                                                <span class="badge bg-primary"> {{ $config['type'] }} </span>
                                                <a class="text-description text-dark" href="{{ route('job' , $request->job->id) }}" target="_blank">
                                                    <span class="fs-18">{{$request->job->title}}</span>
                                                </a>
                                            </div>
                                            <div class="mt-2">
                                                <span>تاریخ ارسال : {{ verta($request->created_at)->formatDate() }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center ms-md-4">
                                        <div class="col-10 col-md-6 mt-1 px-1">
                                            @if (is_null($request->status))
                                                <span class="badge bg-info"> در انتظار بررسی توسط شرکت </span>
                                            @else
                                                @if($request->status == 1)
                                                    <span class="badge bg-success">قبول شده توسط شرکت</span>
                                                @elseif($request->status == 0)
                                                    <span class="badge bg-danger"> رد شده توسط شرکت </span>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-10 col-md-6 mt-1 px-1">
                                            @if($request->process)
                                                @if ($request->status == 2)
                                                    @if (!is_null($request->process->canceled_by_std_at))
                                                        <span class="badge bg-danger"> لغو شده توسط شما</span>
                                                    @elseif (!is_null($request->process->canceled_by_co_at))
                                                        <span class="badge bg-danger"> لغو شده توسط شرکت</span>
                                                    @else
                                                        <span class="badge bg-danger">لغو شده</span>
                                                    @endif
                                                @else
                                                    @if(!is_null($request->process->accepted_by_uni_at) && is_null($request->process->rejected_by_uni_at) )
                                                        <span class="badge bg-success"> تایید توسط دانشگاه</span>
                                                    @endif

                                                    @if(!is_null($request->process->rejected_by_uni_at) )
                                                        <span class="badge bg-danger"> رد توسط دانشگاه</span>
                                                    @endif
                                                    @if(is_null($request->process->accepted_by_uni_at) && is_null($request->process->rejected_by_uni_at) )
                                                        <span class="badge bg-warning"> در انتظار بررسی توسط دانشگاه</span>
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row justify-content-center">
                                        <div class="col-10 col-md-6 mt-1 px-1">
                                            <a href="javascript:void(0)" class="btn w-100 
                                                @if ($request->status != 1)
                                                    disabled btn-dark
                                                @else
                                                    btn-secondary
                                                @endif
                                                "type="button"
                                                data-bs-target="#modal-{{$request->id}}"
                                                data-bs-toggle="modal">
                                                روند درخواست
                                            </a>
                                        </div>
                                        <div class="col-10 col-md-6 mt-1 px-1">
                                            <a href="" class="btn btn-danger w-100 {{ $request->status == 2 ? 'disabled' : '' }}"
                                                data-bs-toggle="modal" 
                                                data-bs-target="#cancle-modal-{{$request->id}}">لغو درخواست</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade " id="modal-{{$request->id}}">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div
                                            class="modal-content modal-content-demo">
                                            <div
                                                class="modal-header">
                                                <h6 class="modal-title">
                                                    روند پویش
                                                    کارجو {{$request->sender->name." ".$request->sender->family}}</h6>
                                                <button
                                                    aria-label="Close"
                                                    class="btn-close"
                                                    data-bs-dismiss="modal">
                                                    <span
                                                        aria-hidden="true ">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div
                                                    class="card-body">
                                                    <div
                                                        class="vtimeline">
                                                        @php($timeline = getPooyeshProcess($request->id))
                                                        @php($flag = 0)
                                                        @foreach($timeline as $key => $value)
                                                            
                                                            @if($flag % 2 == 0)
                                                                <div
                                                                    class="timeline-wrapper timeline-wrapper-primary">
                                                                    <div
                                                                        class="avatar avatar-md timeline-badge">
                                                                <span class="timeline-icon"><svg style="width:25px;height:25px" viewBox="0 0 24 24">
                                                                <path fill="currentColor"
                                                                    d="M4,2A2,2 0 0,0 2,4V11C2,11.55 2.22,12.05 2.59,12.42L11.59,21.42C11.95,21.78 12.45,22 13,22C13.55,22 14.05,21.78 14.41,21.41L21.41,14.41C21.78,14.05 22,13.55 22,13C22,12.45 21.77,11.94 21.41,11.58L12.41,2.58C12.05,2.22 11.55,2 11,2H4V2M11,4L20,13L13,20L4,11V4H11V4H11M6.5,5A1.5,1.5 0 0,0 5,6.5A1.5,1.5 0 0,0 6.5,8A1.5,1.5 0 0,0 8,6.5A1.5,1.5 0 0,0 6.5,5M10.95,10.5C9.82,10.5 8.9,11.42 8.9,12.55C8.9,13.12 9.13,13.62 9.5,14L13,17.5L16.5,14C16.87,13.63 17.1,13.11 17.1,12.55A2.05,2.05 0 0,0 15.05,10.5C14.5,10.5 13.97,10.73 13.6,11.1L13,11.7L12.4,11.11C12.03,10.73 11.5,10.5 10.95,10.5Z"/>
                                                                </svg></span>
                                                                    </div>
                                                                    <div
                                                                        class="timeline-panel">
                                                                        <div
                                                                            class="timeline-heading">
                                                                            <h6 class="timeline-title">{{$key}}</h6>
                                                                        </div>
                                                                        <div
                                                                            class="timeline-body">
                                                                            <p></p>
                                                                        </div>
                                                                        <div
                                                                            class="timeline-footer d-flex align-items-center flex-wrap">
                                                                            <span class="ms-auto">
                                                                                <i class="fe fe-calendar text-muted mx-1"></i>
                                                                                {{verta($value)->formatDate()}}
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @else
                                                                <div class="timeline-wrapper timeline-inverted timeline-wrapper-secondary">
                                                                    <div class="avatar avatar-md timeline-badge">
                                                                        <span class="timeline-icon"><svg style="width:26px;height:26px" viewBox="0 0 24 24">
                                                                        <path fill="currentColor"
                                                                            d="M12 4C14.2 4 16 5.8 16 8C16 10.1 13.9 13.5 12 15.9C10.1 13.4 8 10.1 8 8C8 5.8 9.8 4 12 4M12 2C8.7 2 6 4.7 6 8C6 12.5 12 19 12 19S18 12.4 18 8C18 4.7 15.3 2 12 2M12 6C10.9 6 10 6.9 10 8S10.9 10 12 10 14 9.1 14 8 13.1 6 12 6M20 19C20 21.2 16.4 23 12 23S4 21.2 4 19C4 17.7 5.2 16.6 7.1 15.8L7.7 16.7C6.7 17.2 6 17.8 6 18.5C6 19.9 8.7 21 12 21S18 19.9 18 18.5C18 17.8 17.3 17.2 16.2 16.7L16.8 15.8C18.8 16.6 20 17.7 20 19Z"/>
                                                                        </svg></span>
                                                                    </div>
                                                                    <div class="timeline-panel">
                                                                        <div class="timeline-heading">
                                                                            <h6 class="timeline-title">{{$key}}</h6>
                                                                        </div>
                                                                        <div class="timeline-body">

                                                                        </div>
                                                                        <div class="timeline-footer d-flex align-items-center flex-wrap">
                                                                            <span class="ms-auto">
                                                                                <i class="fe fe-calendar text-muted mx-1"></i> 
                                                                                {{verta($value)->formatDate()}}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @php($flag++)
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="modal-footer">
                                                <button
                                                    class="btn btn-light"
                                                    data-bs-dismiss="modal">
                                                    بستن
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="cancle-modal-{{$request->id}}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content modal-content-demo">
                                            <div class="modal-header">
                                                <h6 class="modal-title">ارسال تیکت</h6>
                                                <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <form action="{{ route('std.cancle.req.job') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-body">
                                                    <input type="hidden" name="request_id" value="{{$request->id}}">
                                                    <input type="hidden" name="receiver_id" value="{{$request->receiver_id}}">
                                                    <div class="mb-3">
                                                        <label for="recipient-name" class="col-form-label">موضوع :</label>
                                                        <input name="subject" value="لغو درخواست کارآموزی" type="text" class="form-control" id="recipient-name">
                                                    </div>
                                                    @error('subject') <small id="subjectErr" class="text-danger">{{$message}}</small> @enderror
                                                    <div class="mb-3">
                                                        <label for="message-text" class="col-form-label">متن :</label>
                                                        <textarea name="content" class="form-control" id="message-text"></textarea>
                                                    </div>
                                                    @error('content') <small id="contentErr" class="text-danger">{{$message}}</small> @enderror
                                                    <div class="mb-3">
                                                        <label for="attachment-file" class="col-form-label">فایل ضمیمه :</label>
                                                        <input name="attachment" type="file" class="form-control" id="attachment-file">
                                                    </div>
                                                    @error('attachment') <small id="attachmentErr" class="text-danger">{{$message}}</small> @enderror
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn ripple btn-primary" type="submit">ارسال و رد درخواست</button>
                                                    <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    @endforeach
                </div>
            @else
                <div class="alert alert-warning w-100 text-center">
                    @lang('public.no_info')
                </div>
            @endif
        </div>
    </div>
@endsection