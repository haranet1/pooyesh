@extends('admin.layouts.master')
@section('title' , $config['title'])
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="card">
                <div class="d-flex card-header justify-content-between">
                    <h3 class="card-title">{{ $config['title'] }}</h3>
                    <a href="{{ route($config['mainPage']) }}" class="btn btn-primary">صفحه اصلی</a>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            
                            <form action="{{ route('search.pos.job') }}" method="get" class="input-group">
                                @csrf
                                <input type="hidden" name="title" value="{{ $config['title'] }}">
                                
                                <input type="hidden" name="type" value="{{ $config['searchType'] }}">
                                

                                <input type="text" name="job_name" class="form-control"
                                    @isset($oldData['job_name'])
                                        value="{{ $oldData['job_name'] }}"
                                    @endisset
                                placeholder="عنوان :">

                                <input type="text" name="co_name" class="form-control" 
                                    @isset($oldData['co_name'])
                                        value="{{ $oldData['co_name'] }}"
                                    @endisset
                                placeholder="نام شرکت :">

                               
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-info form-control div-border-user-company-style">جستجو</button>
                                </div>
                            </form>

                        </div>
                        <div class="col-2 me-2">
                            <a href="{{ route($config['thisRoute']) }}" class="btn btn-warning w-100">رفع فیلتر</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route($config['export']) }}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="row w-100 mb-2">
                        <h4 class="card-title fs-7"> تعداد کل : {{ $job_positions->total() }} </h4>
                    </div>
                    <div class="table-responsive table-lg">
                        @if ($job_positions->count() > 0)
                            <table class="table table-primary border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-primary text-white fs-12">ردیف</th>
                                        <th class="text-center bg-primary text-white">عنوان</th>
                                        <th class="text-center bg-primary text-white">شرکت</th>
                                        <th class="text-center bg-primary text-white">امکانات</th>
                                        <th class="text-center bg-primary text-white">محل</th>
                                        <th class="text-center bg-primary text-white">حقوق</th>
                                        <th class="text-center bg-primary text-white">تاریخ ایجاد</th>
                                    </tr>
                                </thead>
                                <tbody id="">
                                    @php
                                        $counter = (($job_positions->currentPage() -1) * $job_positions->perPage()) + 1;
                                    @endphp
                                    @foreach ($job_positions as $job)
                                        <tr>
                                            <td class="text-nowrap text-center align-middle">{{ $counter++ }}</td>

                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route('job' , $job->id) }}" class="text-decoration text-dark">
                                                    <div class="col-5 col-md-9 p-md-0">
                                                        {{ $job->title }}
                                                    </div>
                                                </a>
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route('company.single' , $job->company->id) }}" class="text-decoration text-dark">
                                                    {{ $job->company->groupable->name }}
                                                </a>
                                            </td>

                                            <td class="text-nowrap align-middle text-center">

                                                @if ($config['intern'])
                                                    <a onclick="acceptInternPosition({{ $job->id }})" 
                                                        class="btn btn-sm btn-success" href="javascript:void(0)"> تایید </a>
                                                @else
                                                    <a onclick="acceptHirePosition({{ $job->id }})" 
                                                        class="btn btn-sm btn-success" href="javascript:void(0)"> تایید </a>
                                                @endif
                                                
                                                <button type="button" class="btn btn-sm btn-danger badge"
                                                    data-bs-toggle="modal" data-bs-target="#reject-modal-{{$job->id}}"> رد </button>
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                {{ Str::limit(unmatterToPersion($job->address), 60, '...') }}
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                {{ getPersionSalaryForOnetJob($job->salary) }}
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                {{ verta($job->created_at)->formatDate() }}
                                            </td>

                                        </tr>
                                        <div class="modal fade" id="reject-modal-{{$job->id}}">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content modal-content-demo">
                                                    <div class="modal-header">
                                                        <h6 class="modal-title">ارسال تیکت</h6>
                                                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{ route('reject.pos.job') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <input type="hidden" id="job_id" name="job_id" value="{{$job->id}}">
                                                            <input type="hidden" id="receiver_id" name="receiver_id" value="{{$job->company_id}}">

                                                            <div class="mb-3">
                                                                <label for="recipient-name" class="col-form-label">موضوع :</label>
                                                                <input name="subject" value="{{ $config['rejectSubject'] }}" type="text" class="form-control" id="recipient-name">
                                                            </div>
                                                            @error('subject') <small id="subjectErr" class="text-danger">{{$message}}</small> @enderror

                                                            <div class="mb-3">
                                                                <label for="message-text" class="col-form-label">متن :</label>
                                                                <textarea name="content" class="form-control" id="message-text"></textarea>
                                                            </div>
                                                            @error('content') <small id="contentErr" class="text-danger">{{$message}}</small> @enderror

                                                            <div class="mb-3">
                                                                <label for="attachment-file" class="col-form-label">فایل ضمیمه :</label>
                                                                <input name="attachment" type="file" class="form-control" id="attachment-file">
                                                            </div>
                                                            @error('attachment') <small id="attachmentErr" class="text-danger">{{$message}}</small> @enderror
                                                           
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn ripple btn-primary" type="submit">ارسال و رد موقعیت</button>
                                                            <button class="btn ripple btn-danger" data-bs-dismiss="modal" type="button">بستن</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                @lang('public.no_info')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$job_positions->links('pagination.panel')}}
            </div>
        </div>
    </div>
    <input type="hidden" id="acceptJobPositionUrl" value="{{ route('accept.pos.job') }}">
@endsection
@section('script')
    <script src="{{ asset('assets/intern/js/emp-position-options.js') }}"></script>
@endsection