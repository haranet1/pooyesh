@extends('admin.layouts.master')
@section('title' , $config['title'])
@section('main')
    <div class="row row-cards mt-4">
        <div class="col-12">
            @if(Session::has('success'))
                <div class="alert alert-success mt-2 text-center">
                    <h5>{{Session::pull('success')}}</h5>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-2 text-center">
                    <h5>{{Session::pull('error')}}</h5>
                </div>
            @endif

            <div class="card">
                <div class="d-flex card-header justify-content-between">
                    <h3 class="card-title">{{ $config['title'] }}</h3>
                    <a href="{{ route($config['mainPage']) }}" class="btn btn-primary">صفحه اصلی</a>
                </div>
                <div class="card-body">
                    <div class="row w-100 mb-5">
                        <div class="col-9">
                            
                            <form action="{{ route('search.pos.job') }}" method="get" class="input-group">
                                @csrf
                                <input type="hidden" name="title" value="{{ $config['title'] }}">
                                <input type="hidden" name="type" value="{{ $config['searchType'] }}">

                                <input type="text" name="job_name" class="form-control"
                                    @isset($oldData['job_name'])
                                        value="{{ $oldData['job_name'] }}"
                                    @endisset
                                placeholder="عنوان :">

                                <input type="text" name="co_name" class="form-control" 
                                    @isset($oldData['co_name'])
                                        value="{{ $oldData['co_name'] }}"
                                    @endisset
                                placeholder="نام شرکت :">

                               
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-info form-control div-border-user-company-style">جستجو</button>
                                </div>
                            </form>

                        </div>
                        <div class="col-2 me-2">
                            <a href="{{ route($config['thisRoute']) }}" class="btn btn-warning w-100">رفع فیلتر</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route($config['export']) }}" class="btn btn-secondary">خروجی اکسل</a>
                        </div>
                    </div>
                    <div class="row w-100 mb-2">
                        <h4 class="card-title fs-7"> تعداد کل : {{ $job_positions->total() }} </h4>
                    </div>
                    <div class="table-responsive table-lg">
                        @if ($job_positions->count() > 0)
                            <table class="table table-success border-top table-bordered mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-success text-white fs-12">ردیف</th>
                                        <th class="text-center bg-success text-white">عنوان</th>
                                        <th class="text-center bg-success text-white">شرکت</th>
                                        <th class="text-center bg-success text-white">محل</th>
                                        <th class="text-center bg-success text-white">حقوق</th>
                                        <th class="text-center bg-success text-white">تاریخ ایجاد</th>
                                    </tr>
                                </thead>
                                <tbody id="">
                                    @php
                                        $counter = (($job_positions->currentPage() -1) * $job_positions->perPage()) + 1;
                                    @endphp
                                    @foreach ($job_positions as $job)
                                        <tr>
                                            <td class="text-nowrap text-center align-middle">{{ $counter++ }}</td>

                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route('job' , $job->id) }}" class="text-decoration text-dark">
                                                    <div class="col-5 col-md-9 p-md-0">
                                                        {{ $job->title }}
                                                    </div>
                                                </a>
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                <a href="{{ route('company.single' , $job->company->id) }}" class="text-decoration text-dark">
                                                    {{ $job->company->groupable->name }}
                                                </a>
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                {{ Str::limit($job->location, 70, '...') }}
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                {{ getPersionSalaryForOnetJob($job->salary) }}
                                            </td>

                                            <td class="text-nowrap align-middle">
                                                {{ verta($job->created_at)->formatDate() }}
                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-warning text-center">
                                @lang('public.no_info')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mb-5">
                {{$job_positions->links('pagination.panel')}}
            </div>
        </div>
    </div>
@endsection