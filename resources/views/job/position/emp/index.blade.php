@extends('admin.layouts.master')

@section('title' , $config['title'])
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/panel/plugins/chartjs/css/coreui-chartjs.css') }}">
@endsection
@section('main')
    <div class="mt-4">
        
        <div class="row">
            <div class="col-md-4 col-xl-4">
                <a href="{{ $links['un'] }}">
                    <div class="card text-white bg-primary">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <h4 class="card-title mb-0">موقعیت های {{ $config['type'] }} در انتظار بررسی</h4>
                                <div>
                                    <h1 class="card-text" style="font-size: 4rem">{{ $config['un'] }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-xl-4">
                <a href="{{ $links['ac'] }}">
                    <div class="card text-white bg-success">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <h4 class="card-title mb-0">موقعیت های {{ $config['type'] }} تایید شده</h4>
                                <div>
                                    <h1 class="card-text" style="font-size: 4rem">{{ $config['ac'] }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-xl-4">
                <a href="{{ $links['re'] }}">
                    <div class="card text-white bg-warning">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <h4 class="card-title mb-0">موقعیت های {{ $config['type'] }} رد شده</h4>
                                <div>
                                    <h1 class="card-text" style="font-size: 4rem">{{ $config['re'] }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection