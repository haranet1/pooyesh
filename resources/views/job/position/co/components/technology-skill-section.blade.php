<script>
    let technologySkill = false;
    let technologySkillNum = 0;
</script>
@if($technologySkills && $technologySkills->count() > 0)
    <div id="technologySkillBox" class="mt-9 rounded-4 py-5 px-md-6 px-3 box-body duties-box position-relative">
        <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3">
            <h3 class="fw-bold text-white-c fs-5 mb-0 task-title">مهارت های فنی</h3>
        </div>

        <script>
            technologySkill = true;
            technologySkillNum = {{ $technologySkills->count() }}
        </script>

        @if ($technologySkills->count() > 0)
            <div class="row">
                @foreach ($technologySkills as $technologySkill)
                    @if ($loop->index < 5)

                        <div class="col-md-6 col-12">
                            <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                <div class="flex-column d=flex">
                                    <div class="d-flex align-self-start align-self-md-center">
                                        <input name="technologySkill_{{ $technologySkill->technologySkillName->id }}" class="form-check-input" type="checkbox" 
                                            id="technologySkill-{{ $loop->index + 1 }}" value="{{ $technologySkill->id }}"/>
                                        <label for="technologySkill-{{ $loop->index + 1 }}" 
                                            class="text-dark-c mb-0 fs-16 fw-normal">{{ $technologySkill->technologySkillName->title }}</label>
                                    </div>
                                    <div class="mb-0 fs-14 fw-normal text-info ms-5">مثال : {{ $technologySkill->example->example }}</div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        @endif


        <div class="bg-body-2 mt-4 rounded-4 p-3 width-more mx-auto">
            <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapsetechnologySkills" aria-expanded="false">موارد بیشتر</span>
        </div>

            <div class="collapse" id="collapsetechnologySkills">
                <div class="row">
                    @foreach ($technologySkills as $technologySkill)
                        @if ($loop->index >= 5)
                            <div class="col-md-6 col-12">
                                <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                    <div class="flex-column d=flex">
                                        <div class="d-flex align-self-start align-self-md-center">
                                            <input name="technologySkill_{{ $technologySkill->technologySkillName->id }}" class="form-check-input" type="checkbox" 
                                                id="technologySkill-{{ $loop->index + 1 }}" value="{{ $technologySkill->id }}"/>
                                            <label for="technologySkill-{{ $loop->index + 1 }}" 
                                                class="text-dark-c mb-0 fs-16 fw-normal">{{ $technologySkill->technologySkillName->title }}</label>
                                        </div>
                                        <div class="mb-0 fs-14 fw-normal text-info ms-5">مثال : {{ $technologySkill->example->example }}</div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        
    
    </div>
    <p id="technologySkillErr" class="red form-feedback fs-18 mt-2"></p>
@endif