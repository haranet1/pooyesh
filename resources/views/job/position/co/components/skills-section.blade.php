<script>
    let skill = false;
    let skillNum = 0;
</script>
@if($skills && $skills->count() > 0)
    <div id="skillBox" class="mt-9 rounded-4 py-5 px-md-6 px-3 box-body duties-box position-relative">
        <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3">
            <h3 class="fw-bold text-white-c fs-5 mb-0 task-title">مهارت ها</h3>
        </div>
        <script>
            skill = true;
            skillNum = {{ $skills->count() }};
        </script>

        @if ($skills->count() > 0)
            @foreach ($skills as $skill)
                @if ($loop->index < 5)
                    <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                        <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                            <div class="d-flex align-self-start align-self-md-center">

                                <input onclick="setDataInForm(this , 'skill')" class="form-check-input" type="checkbox"
                                    id="skillInput-{{ $loop->index + 1 }}" value="{{ $skill->skillname->id }}" />

                                <label for="skillInput-{{ $loop->index + 1 }}" 
                                    class="text-dark-c mb-0 fs-16 fw-normal">{{ $skill->skillname->title }}</label>

                            </div>
                            <div class="d-flex width-range gap-3 align-items-center me-md-5">
                                
                                <input oninput="changeDataRange(this , 'skill')" id="skillRange-{{ $loop->index + 1 }}" type="range" 
                                    skillLevel=""  class="form-range" min="1" max="5" step="1" value="{{ round($skill->level->data_value) }}">

                                <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                    {{ getOnetLevel($skill->level->data_value) }}
                                </div>

                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        @endif

        {{-- <div class="bg-body-2 mt-4 rounded-4 p-3 d-flex flex-column input-add-active">

            <input placeholder="اضافه کردن مورد دلخواه ..." class="input-add">
            <div class="d-flex align-items-md-center gap-3 flex-md-row flex-column align-items-start mt-md-0 mt-3">
                <span class="fw-medium feedback-range">میزان اهمیت :</span>
                <div class="width-range">
                    <input type="range" name="slider" class="form-range" min="0" max="4" step="1" value="None">
                </div>
            </div>

            <button class="blue bg-yellow-c btn-edit2 align-self-end">
                + اضافه کردن
            </button>

        </div> --}}

    

        <div class="bg-body-2 mt-4 rounded-4 p-3">
            <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseskill" aria-expanded="false">موارد بیشتر</span>
            
            <div class="collapse" id="collapseskill">

                @foreach ($skills as $skill)
                    @if ($loop->index >= 5)
                        <div class="bg-body-2 mt-4 rounded-4 skill-active">
                            <div class="box-body p-3 rounded-4">
                                <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                    <div class="d-flex align-self-start align-self-md-center">

                                        <input onclick="setDataInForm(this , 'skill')" class="form-check-input" type="checkbox"
                                            id="skillInput-{{ $loop->index + 1 }}" value="{{ $skill->skillname->id }}" />

                                        <label for="skillInput-{{ $loop->index + 1 }}" 
                                            class="text-dark-c mb-0 fs-16 fw-normal">{{ $skill->skillname->title }}</label>

                                    </div>
                                    <div class="d-flex width-range gap-3 align-items-center me-md-5">

                                        <input oninput="changeDataRange(this , 'skill')" id="skillRange-{{ $loop->index + 1 }}" type="range" 
                                            skillLevel=""  class="form-range" min="1" max="5" step="1" value="{{ round($skill->level->data_value) }}">

                                        <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                            {{ getOnetLevel($skill->level->data_value) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

            </div>
        
        </div>

    </div>
    <p id="skillErr" class="red form-feedback fs-18 mt-2"></p>
@endif