<script>
    let task = false;
    let taskNum = 0;
</script>
@if($tasks && $tasks->count() > 0)
    <div id="taskBox" class="mt-9 rounded-4 py-5 px-md-6 px-3 box-body skills-box position-relative mb-9">
        <div class="skills-box2 position-relative">

            <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3 skill-title position-relative">
                <h3 class="fw-bold text-white-c fs-5 mb-0">وظایف</h3>
            </div>

            <script>
                task = true;
                taskNum = {{ $tasks->count() }}
            </script>

            @if ($tasks->count() > 0)
                @foreach ($tasks as $task)
                    @if ($loop->index < 5)
                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                            <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                <div class="d-flex align-self-start align-self-md-center">
                                    <input name="task_{{ $task->id }}" class="form-check-input" type="checkbox" 
                                        id="task-{{ $loop->index + 1 }}" value="{{ $task->id }}"/>
                                    <label for="task-{{ $loop->index + 1 }}" 
                                        class="text-dark-c mb-0 fs-16 fw-normal">{{ $task->title }}</label>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif


            <div class="bg-body-2 mt-4 rounded-4 p-3">
                <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapsetask" aria-expanded="false">موارد بیشتر</span>
                
                <div class="collapse" id="collapsetask">

                    @foreach ($tasks as $task)
                        @if ($loop->index >= 5)
                            <div class="bg-body-2 mt-4 rounded-4 skill-active">
                                <div class="box-body p-3 rounded-4">
                                    <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                        <div class="d-flex align-self-start align-self-md-center">
                                            <input name="task_{{ $task->id }}" class="form-check-input" type="checkbox" 
                                                id="task-{{ $loop->index + 1 }}" value="{{ $task->id }}"/>
                                            <label for="task-{{ $loop->index + 1 }}" 
                                                class="text-dark-c mb-0 fs-16 fw-normal">{{ $task->title }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    

                </div>
            
            </div>
        </div>



    </div>
    <p id="taskErr" class="red form-feedback fs-18 mt-2"></p>
@endif