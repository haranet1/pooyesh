<script>
    let ability = false;
    let abilityNum = 0;
</script>
@if($abilities && $abilities->count() > 0)
    <div id="abilityBox" class="mt-9 rounded-4 py-5 px-md-6 px-3 box-body duties-box position-relative">
        <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3">
            <h3 class="fw-bold text-white-c fs-5 mb-0 task-title">توانایی ها</h3>
        </div>
    <script>
        ability = true;
        abilityNum = {{ $abilities->count() }}
    </script>
        @if ($abilities->count() > 0)
            @foreach ($abilities as $ability)
                @if ($loop->index < 5)
                    <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                        <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                            <div class="d-flex align-self-start align-self-md-center">

                                <input onclick="setDataInForm(this , 'ability')" class="form-check-input" type="checkbox"
                                    id="abilityInput-{{ $loop->index + 1 }}" value="{{ $ability->abilityName->id }}" />

                                <label for="abilityInput-{{ $loop->index + 1 }}" 
                                    class="text-dark-c mb-0 fs-16 fw-normal">{{ $ability->abilityName->title }}</label>

                            </div>
                            <div class="d-flex width-range gap-3 align-items-center me-md-5">
                                <input oninput="changeDataRange(this , 'ability')" id="abilityRange-{{ $loop->index + 1 }}" type="range" 
                                    class="form-range" min="1" max="5" step="1" value="{{ round($ability->level->data_value) }}">
                                <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                        {{ getOnetLevel($ability->level->data_value) }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        @endif

        <div class="bg-body-2 mt-4 rounded-4 p-3">
            <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseability" aria-expanded="false">موارد بیشتر</span>
            
            <div class="collapse" id="collapseability">

                @foreach ($abilities as $ability)
                    @if ($loop->index >= 5)
                        <div class="bg-body-2 mt-4 rounded-4 skill-active">
                            <div class="box-body p-3 rounded-4">
                                <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                    <div class="d-flex align-self-start align-self-md-center">

                                        <input onclick="setDataInForm(this , 'ability')" class="form-check-input" type="checkbox"
                                            id="abilityInput-{{ $loop->index + 1 }}" value="{{ $ability->abilityName->id }}" />

                                        <label for="abilityInput-{{ $loop->index + 1 }}" 
                                            class="text-dark-c mb-0 fs-16 fw-normal">{{ $ability->abilityName->title }}</label>

                                    </div>
                                    <div class="d-flex width-range gap-3 align-items-center me-md-5">

                                        <input oninput="changeDataRange(this , 'ability')" id="abilityRange-{{ $loop->index + 1 }}" type="range" 
                                            class="form-range" min="1" max="5" step="1" value="{{ round($ability->level->data_value) }}">

                                        <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                            {{ getOnetLevel($ability->level->data_value) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

            </div>
        
        </div>

    </div>
    <p id="abilityErr" class="red form-feedback fs-18 mt-2"></p>
@endif