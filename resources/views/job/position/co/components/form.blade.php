<div class="mt-9 rounded-bottom-3 py-5 px-md-4 box-body">
    <div class="d-flex align-items-center heading-top position-relative justify-content-center">
        <div class="line-top2"></div>
        <h3 class="fw-bold px-3 width-fit-content">ثبت اطلاعات</h3>
        <div class="line-top2"></div>
    </div>

    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
        <h4 class="form-title">مشخصات کاری</h4>
        <div class="row">

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title"></label>
                <div class="form-group text-center">
                    <script>
                        let canCreateIntern = "{{ $canCreateIntern }}";
                    </script>
                    <label class="custom-switch form-switch mb-0">
                        <label class="lable-title mx-2">استخدامی</label>
                        <input onchange="swichHireOrIntern()" 
                            @if ($canCreateIntern)
                                checked
                            @endif 
                        type="checkbox" id="hire_or_intern" name="hire_or_intern" class="custom-switch-input">
                        <span class="custom-switch-indicator custom-switch-indicator-lg"></span>
                        <label class="lable-title mx-2">کارآموزی</label>
                    </label>
                </div>
                <p id="typeErr" class="red form-feedback text-end mt-2"></p>
                {{-- @error('type')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror --}}
            </div>

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title necessary">نوع همکاری:</label>
                <select name="type" id="type" class="form-select">
                    <option 
                    @if (old('type') == 'full-time')
                        selected
                    @endif
                    value="full-time" selected>{{ job_type_persian('full-time') }}</option>

                    <option 
                    @if (old('type') == 'part-time')
                        selected
                    @endif
                    value="part-time"> {{ job_type_persian('part-time') }} </option>

                    <option 
                    @if (old('type') == 'remote')
                        selected
                    @endif
                    value="remote"> {{ job_type_persian('remote') }} </option>
                    @if ($canCreateIntern)
                    <option 
                        @if (old('type') == 'intern')
                            selected
                        @endif
                        value="intern"> {{ job_type_persian('intern') }} </option>
                    @endif


                </select>
                <p id="typeErr" class="red form-feedback text-end mt-2"></p>
                {{-- @error('type')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror --}}
            </div>

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title">سابقه کار:</label>
                <select name="experience" class="form-select">
                    <option 
                    @if (old('experience') == 'unmatter')
                        selected
                    @endif
                    value="2">فرقی ندارد</option>

                    <option 
                    @if (old('experience') == '2')
                        selected
                    @endif
                    value="2">تا ۲ سال</option>

                    <option 
                    @if (old('experience') == '5')
                        selected
                    @endif
                    value="5">۳ تا ۵ سال</option>

                    <option 
                    @if (old('experience') == '10')
                        selected
                    @endif
                    value="10">۶ تا ۱۰ سال</option>

                    <option 
                    @if (old('experience') == '11')
                        selected
                    @endif
                    value="11">بالای ۱۰ سال</option>

                </select>
                @error('experience')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
            </div>

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title">سطح تجربه:</label>
                <select name="level" class="form-select">
                    <option 
                    @if (old('level') == 'unmatter')
                        selected
                    @endif
                    value="unmatter">فرقی ندارد</option>

                    <option 
                    @if (old('level') == 'senior')
                        selected
                    @endif
                    value="senior">حرفه ای</option>

                    <option 
                    @if (old('level') == 'mid-level')
                        selected
                    @endif
                    value="mid-level">متوسط</option>

                    <option 
                    @if (old('level') == 'junior')
                        selected
                    @endif
                    value="junior">تازه کار</option>

                </select>
                @error('level')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
            </div>

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title">ساعت کاری:</label>
                <input name="working_hours"
                    @if (old('working_hours'))
                        value="{{ old('working_hours') }}"
                    @endif
                placeholder="مثال : 8 الی 16"
                type="text" class="form-control">
                @error('working_hours')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
            </div>

            <div class="col-md-6 col-12 px-md-5 px-4 mb-2">
                <label class="lable-title necessary">حقوق:</label>
                <select name="salary" id="salary" class="form-select">
                    <option value="agreement">{{ getPersionSalaryForOnetJob('agreement') }}</option>
                    <option value="work_office">{{ getPersionSalaryForOnetJob('work_office') }}</option>
                    <option value="4">{{ getPersionSalaryForOnetJob('4') }}</option>
                    <option value="8">{{ getPersionSalaryForOnetJob('8') }}</option>
                    <option value="16">{{ getPersionSalaryForOnetJob('16') }}</option>
                    <option value="25">{{ getPersionSalaryForOnetJob('25') }}</option>
                    <option value="30">{{ getPersionSalaryForOnetJob('30') }}</option>
                </select>
                <p class="text-warning form-feedback fs-16 mt-2">برای گرفتن بازخورد بهتر حقوق و دستمزد مورد نظر خود را انتخاب کنید.</p>
            </div>
        </div>
        
    </div>


    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
        <h4 class="form-title">مشخصات فردی</h4>
        <div class="row">
            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title">رنج سنی:</label>
                <select name="age" class="form-select">
                    <option 
                        @if (old('age') == '0')
                            selected
                        @endif
                    value="0">فرقی ندارد</option>

                    <option 
                        @if (old('age') == '20')
                            selected
                        @endif
                    value="20">... تا 20 سال</option>

                    <option 
                        @if (old('age') == '30')
                            selected
                        @endif
                    value="30">21 تا 30 سال</option>

                    <option 
                        @if (old('age') == '40')
                            selected
                        @endif
                    value="40">31 تا 40 سال</option>

                </select>
                @error('age')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
            </div>

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title">جنسیت:</label>
                <select name="sex" class="form-select">
                    <option 
                        @if (old('sex') == 'unmatter')
                            selected
                        @endif
                    value="unmatter">فرقی ندارد</option>

                    <option 
                        @if (old('sex') == 'male')
                            selected
                        @endif
                    value="male">مرد</option>

                    <option 
                        @if (old('sex') == 'female')
                            selected
                        @endif
                    value="female">زن</option>

                </select>
                @error('sex')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
            </div>

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title">وضعیت تاهل:</label>
                <select name="marige_type" class="form-select">
                    <option 
                        @if (old('marige_type') == 'unmatter')
                            selected
                        @endif
                    value="unmatter">فرقی ندارد</option>

                    <option 
                        @if (old('marige_type') == 'single')
                            selected
                        @endif
                    value="single">مجرد</option>

                    <option 
                        @if (old('marige_type') == 'married')
                            selected
                        @endif
                    value="married">متاهل</option>
                </select>
                @error('marige_type')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
            </div>

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title">وضعیت نظام وظیفه:</label>
                <select name="military_service_status" class="form-select">
                    <option 
                        @if (old('military_service_status') == 'unmatter')
                            selected
                        @endif
                    value="unmatter">فرقی ندارد</option>

                    <option 
                        @if (old('military_service_status') == 'پایان خدمت یا معافیت دائم')
                            selected
                        @endif
                    value="پایان خدمت یا معافیت دائم">پایان خدمت یا معافیت دائم</option>

                    <option 
                        @if (old('military_service_status') == 'پایان خدمت')
                            selected
                        @endif
                    value="پایان خدمت">پایان خدمت</option>

                    <option 
                        @if (old('military_service_status') == 'معافیت تحصیلی')
                            selected
                        @endif
                    value="معافیت تحصیلی">معافیت تحصیلی</option>

                    <option 
                        @if (old('military_service_status') == 'مشمول')
                            selected
                        @endif
                    value="مشمول">مشمول</option>

                </select>
                @error('military_service_status')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
            </div>
        </div>
        
    </div>


    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-5">
        <h4 class="form-title">تحصیلات</h4>
        <div class="row">

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title">مقطع تحصیلی:</label>
                <select name="grade" class="form-select">
                    <option 
                        @if (old('grade') == 'unmatter')
                            selected
                        @endif
                    value="unmatter">فرقی ندارد</option>

                    <option 
                        @if (old('grade') == 'دکترا')
                            selected
                        @endif
                    value="دکترا">دکترا</option>

                    <option 
                        @if (old('grade') == 'کارشناسی ارشد')
                            selected
                        @endif
                    value="کارشناسی ارشد">کارشناسی ارشد</option>

                    <option 
                        @if (old('grade') == 'کارشناسی')
                            selected
                        @endif
                    value="کارشناسی">کارشناسی</option>

                    <option 
                        @if (old('grade') == 'کاردانی')
                            selected
                        @endif
                    value="کاردانی">کاردانی</option>

                    <option 
                        @if (old('grade') == 'دیپلم')
                            selected
                        @endif
                    value="دیپلم">دیپلم</option>

                </select>
                @error('grade')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
            </div>

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title">رشته تحصیلی:</label>
                <select name="major" id="major" class="form-select">
                    <option value="">فرقی ندارد</option>
                    @foreach ($majors as $major)
                        <option 
                            @if (old('major') == $major->id)
                                selected
                            @endif
                        value="{{ $major->id }}">{{ $major->title }}</option>
                    @endforeach
                </select>
                
                @error('major')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
                
            </div>

        </div>
    </div>

    <div id="workPlaceInfoBox" class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
        <h4 class="form-title">مشخصات محل کار</h4>
        <div class="row">
            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title necessary">استان:</label>
                <select name="province" id="province" class="form-select" onchange="getCities()">
                    <option value="">انتخاب ...</option>
                    @foreach ($provinces as $province)
                        <option 
                            @if (old('province') == $province->id)
                                selected
                            @endif
                        value="{{ $province->id }}">{{ $province->name }}</option>
                    @endforeach

                </select>
                <p id="provinceErr" class="red form-feedback text-end mt-2"></p>
                {{-- @error('province')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror --}}
            </div>

            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                <label class="lable-title necessary">شهر:</label>
                <select class="form-select" id="city" name="city">
                    <option value="unmatter">ابتدا استان را انتخاب کنید</option>
                </select>
                <p id="cityErr" class="red form-feedback text-end mt-2"></p>
                {{-- @error('city')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror --}}
            </div>

            <div class="col-12 px-md-6 px-4 mb-2">
                <label class="lable-title necessary">آدرس کامل:</label>
                <input name="address" type="text" class="form-control" placeholder="آدرس...">
                <p id="addressErr" class="red form-feedback text-end mt-2"></p>
                {{-- @error('address')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror --}}
            </div>
        </div>
        
    </div>

    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
        <h4 class="form-title">تسهیلات</h4>
        <div class="row">
            <div class="col-12 px-md-6 px-4 mb-2">
                <div class="row">

                    <div class="col-md-6 col-12">
                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                            <div class="flex-column">
                                <div class="d-flex align-self-start align-self-md-center">
                                    <input class="form-check-input" type="checkbox" id="facilities-bime-tamin" name="facilities_بیمه تامین اجتماعی" />
                                    <label for="facilities-bime-tamin" class="text-dark-c mb-0 fs-16 fw-normal">بیمه تامین اجتماعی</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                            <div class="flex-column">
                                <div class="d-flex align-self-start align-self-md-center">
                                    <input class="form-check-input" type="checkbox" id="facilities-bime-takmili" name="facilities_بیمه تکمیلی" />
                                    <label for="facilities-bime-takmili" class="text-dark-c mb-0 fs-16 fw-normal">بیمه تکمیلی</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                            <div class="flex-column">
                                <div class="d-flex align-self-start align-self-md-center">
                                    <input class="form-check-input" type="checkbox" id="facilities-service" name="facilities_سرویس ایاب و ذهاب" />
                                    <label for="facilities-service" class="text-dark-c mb-0 fs-16 fw-normal">سرویس ایاب و ذهاب</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                            <div class="flex-column">
                                <div class="d-flex align-self-start align-self-md-center">
                                    <input class="form-check-input" type="checkbox" id="facilities-ezafekari" name="facilities_اضافه کاری" />
                                    <label for="facilities-ezafekari" class="text-dark-c mb-0 fs-16 fw-normal">اضافه کاری</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                            <div class="flex-column">
                                <div class="d-flex align-self-start align-self-md-center">
                                    <input class="form-check-input" type="checkbox" id="facilities-padash" name="facilities_پاداش" />
                                    <label for="facilities-padash" class="text-dark-c mb-0 fs-16 fw-normal">پاداش</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                            <div class="flex-column">
                                <div class="d-flex align-self-start align-self-md-center">
                                    <input class="form-check-input" type="checkbox" id="facilities-safartafrihi" name="facilities_سفر های تفریحی" />
                                    <label for="facilities-safartafrihi" class="text-dark-c mb-0 fs-16 fw-normal">سفر های تفریحی</label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                
                @error('facilities')
                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                @enderror
            </div>

        </div>
        
    </div>

    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
        <h4 class="form-title">توضیحات</h4>
        <div class="row">
            <textarea class="form-control" name="about" id="editor" cols="30" rows="5"></textarea>

        </div>
        
    </div>

    <div class="d-flex justify-content-center mt-5">
        <button type="button" 
            onclick="submitForm()" 
            class="text-white-c bg-blue-c btn-add">
            ثبت
        </button>
    </div>
</div>