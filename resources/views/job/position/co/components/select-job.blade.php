<div class="row mt-5 align-items-center row-gap-3 justify-content-center">
    <div class="col-md-8 col-12 position-relative">
            <form action="{{ route('show.pos.job') }}" id="selectJobForm" method="POST" class="d-flex p-2 rounded-pill-c box-shadow-c bg-white-c px-4 flex-md-row flex-column">
                @csrf
                
                <div class="w-100 down-icon-input pt-md-0 pt-2">
                    <input id="job_title" oninput="autocompleteAjax()" type="text" placeholder="عنوان شغل مورد نظر را وارد کنید" class="select-custom w-75">
                    <input name="job_id" type="hidden" value="" id="job_id">
                </div>

            </form>
            <div id="dropdownElement" class="dropdown-menu pb-3 mx-4 mw-dropdown overflow-y-scroll position-absolute start-0 end-0" style="height: 60vh;"></div>
            
    </div>
    {{-- <div class="col-12 col-md-2">
        <button class="text-white fs-18 bg-blue-c box-shadow-c rounded-pill text-center btn-job-p w-100 mt-md-0 mt-3">انتخاب</button>
    </div> --}}
</div>
