<div class="mt-9 rounded-bottom-3 py-5 px-md-6 px-3 box-body">
    <div class="d-flex align-items-center heading-top position-relative">
        <div class="line-top"></div>
        <h3 class="fw-bold px-3">عنوان</h3>
        <div class="line-top"></div>
    </div>

    <label for="" class="form-lable ">
        عنوان اختصاصی خود را وارد کنید
        
    </label>
    <input type="text" name="title" value="" id="title" class="form-control rounded-pill w-50">
    <p class="text-danger mt-2 mx-2 fs-18" id="titleErr"></p>

    {{-- <button type="button" class="blue bg-yellow-c rounded-pill btn-edit">
        + ویرایش کردن عنوان
    </button> --}}

</div>