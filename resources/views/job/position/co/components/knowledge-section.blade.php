<script>
    let knowledge = false;
    let knowledgeNum = 0;
</script>
@if($knowledges && $knowledges->count() > 0)
    <div id="knowledgeBox" class="mt-9 rounded-4 py-5 px-md-6 px-3 box-body skills-box position-relative ">
        <div class="skills-box2 position-relative">

            <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3 skill-title position-relative">
                <h3 class="fw-bold text-white-c fs-5 mb-0">دانش</h3>
            </div>

            <script>
                knowledge = true;
                knowledgeNum = {{ $knowledges->count() }}
            </script>

            @if ($knowledges->count() > 0)
                @foreach ($knowledges as $knowledge)
                    @if ($loop->index < 5)
                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                            <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                <div class="d-flex align-self-start align-self-md-center">

                                    <input onclick="setDataInForm(this , 'knowledge')" class="form-check-input" type="checkbox"
                                        id="knowledgeInput-{{ $loop->index + 1 }}" value="{{ $knowledge->knowledgeName->id }}" />

                                    <label for="knowledgeInput-{{ $loop->index + 1 }}" 
                                        class="text-dark-c mb-0 fs-16 fw-normal">{{ $knowledge->knowledgeName->title }}</label>

                                </div>
                                <div class="d-flex width-range gap-3 align-items-center me-md-5">

                                    <input oninput="changeDataRange(this , 'knowledge')" id="knowledgeRange-{{ $loop->index + 1 }}" type="range" 
                                        class="form-range" min="1" max="5" step="1" value="{{ round($knowledge->level->data_value) }}">

                                    <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                        {{ getOnetLevel($knowledge->level->data_value) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif


            <div class="bg-body-2 mt-4 rounded-4 p-3">
                <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseknowledge" aria-expanded="false">موارد بیشتر</span>
                
                <div class="collapse" id="collapseknowledge">

                    @foreach ($knowledges as $knowledge)
                        @if ($loop->index >= 5)
                            <div class="bg-body-2 mt-4 rounded-4 skill-active">
                                <div class="box-body p-3 rounded-4">
                                    <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                        <div class="d-flex align-self-start align-self-md-center">

                                            <input onclick="setDataInForm(this , 'knowledge')" class="form-check-input" type="checkbox"
                                                id="knowledgeInput-{{ $loop->index + 1 }}" value="{{ $knowledge->knowledgeName->id }}" />

                                            <label for="knowledgeInput-{{ $loop->index + 1 }}" 
                                                class="text-dark-c mb-0 fs-16 fw-normal">{{ $knowledge->knowledgeName->title }}</label>

                                        </div>
                                        <div class="d-flex width-range gap-3 align-items-center me-md-5">
                                            
                                            <input oninput="changeDataRange(this , 'knowledge')" id="knowledgeRange-{{ $loop->index + 1 }}" type="range" 
                                                class="form-range" min="1" max="5" step="1" value="{{ round($knowledge->level->data_value) }}">

                                            <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                                {{ getOnetLevel($knowledge->level->data_value) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            
            </div>
        </div>



    </div>
    <p id="knowledgeErr" class="red form-feedback fs-18 mt-2"></p>
@endif