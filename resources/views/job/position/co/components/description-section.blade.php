<div class="mt-9 rounded-bottom-3 py-5 px-md-6 px-3 box-body">
    <div class="d-flex align-items-center heading-top position-relative">
        <div class="line-top"></div>
        <h3 class="fw-bold px-3">توضیحات</h3>
        <div class="line-top"></div>
    </div>

    <p class="fs-18 lh-lg text-justify"> 
        {{ $job->description }}
    </p>

    {{-- <button type="button" class="blue bg-yellow-c rounded-pill btn-edit">
        + ویرایش کردن توضیحات
    </button> --}}

</div>