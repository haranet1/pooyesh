{{-- @extends('admin.layouts.master') --}}
@extends('panel.company.master')
@section('title', $config['title'])
@section('css')
    <link rel="stylesheet" href="{{asset('assets/panel/css/sent-resume-styles.css')}}">
@endsection
@section('main')

    <div class="alert alert-info alert-dismissible fade-show">
        <div class="d-flex align-items-center">
        <img class="me-4" src="{{asset('assets/images/info-icon.svg')}}" style="max-width: 33px" alt="info-icon">
        <div class="flex-grow-1">
            <p class="f-16" >
                <strong>{{ $config['alert'] }}</strong>
            </p>
        </div>
        </div>
    </div>

    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @if (session('limit'))
        <div class="alert alert-danger d-flex align-items-center">
           <div class="me-3"> {{ session('limit') }} </div> 
           <a href="{{route('index.package.company')}}" class="btn btn-info mx-2" type="button">
                 بسته های خدماتی
            </a>
        </div>
    
    @endif
    
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <div class="row row-cards">
        <div class="col-lg-12 col-xl-12">
            
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h1 class="card-title">{{ $config['title'] }}</h1>
                    <a class="btn btn-info mx-2" href="{{route('create.pos.job')}}">{{ $config['create'] }}</a>
                </div>
                <div class="card-body">
                    @if ($jobPositions->count() > 0)
                        <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                            @foreach ($jobPositions as $job)
                                <div class="col-12 col-md-4  mb-3">
                                    <div class="card border-0 border-3 border-info shadow-lg bg-white">
                                        <div class="card-body">
                                            <!-- Profile picture and short information -->
                                            <div class="d-flex align-items-center position-relative">
                                                <div class="flex-grow-1">
                                                    {{-- @php
                                                        $img = rand(1,5);
                                                    @endphp --}}
                                                    <img class="img-sm" width="100px" height="100px" src="{{asset($job->job->category->photo->path)}}" alt="Profile Picture" loading="lazy">
                                                </div>
                                                <div class="flex-grow-1 ms-3">

                                                    <a class="h4" target="_blank" 
                                                        href="{{ route('job', $job->id) }}">
                                                        <p class="mt-2">{{$job->title}}</p>
                                                    </a>
                                                    
                                                    <div class="">
                                                        <h6> گروه شغلی : {{ $job->job->category->title }}</h6>
                                                    </div>

                                                    <div class="form-group mb-2 mt-4">
                                                        <label class="custom-switch form-switch mb-0">
                                                            <input {{ $job->active == 1 ? 'checked':'' }} 
                                                                type="checkbox" 
                                                                id="active-{{$job->id}}" 
                                                                class="custom-switch-input" 
                                                                onchange="activeJobPosition({{ $job->id }})">
                                                            <span class="custom-switch-indicator"></span>
                                                            <span id="active-span-{{$job->id}}" class="custom-switch-description">
                                                                {{ $job->active == 1 ? 'فعال' : 'غیرفعال' }}
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <!-- END : Profile picture and short information -->
                        
                                            <!-- Options buttons -->
                                            <div class="mt-3 pt-2 text-center border-top">
                                                <div class="d-flex justify-content-center gap-3">
                                                    @if ($job->status == 1)
                                                        <a href="{{route('job',$job->id)}}" target="_blank" class="btn btn-info">
                                                            <i class="fa fa-eye" aria-hidden="true"></i> جزئیات
                                                        </a>
                                                    @else
                                                        <div class="btn btn-primary" type="button">
                                                            <i class="fa fa-hourglass-start" aria-hidden="true"></i> در انتظار تایید 
                                                        </div>
                                                    @endif
                                                    <input type="hidden" class="resume_id" value="{{$job->id}}">
                                                    <a href="{{ route('edit.pos.job' , $job->id) }}" class="btn btn-success" type="button">
                                                        <i class="fa fa-edit" aria-hidden="true"></i> ویرایش
                                                    </a>
                                                    <a href="javascript:void(0)" onclick="deleteJobPosition({{$job->id}})" class="btn btn-danger" type="button">
                                                        <i class="fa fa-close" aria-hidden="true"></i> حذف
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- END : Options buttons -->
                        
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="alert alert-warning text-center">
                            شما هنوز هیچ موقعیت شغلی ایجاد نکرده اید
                        </div>
                    @endif
                </div>
            </div>
            <div class="mb-5">
                {{$jobPositions->links('pagination.panel')}}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        const deleteJobPositionUrl = "{{ route('delete.pos.job') }}"
        const activeJobPositionUrl = "{{ route('active.pos.job') }}"
    </script>
    <script src="{{ asset('assets/panel/js/job/job-position.js') }}"></script>

    <script src="{{asset('assets/panel/js/company.js')}}"></script>
@endsection
