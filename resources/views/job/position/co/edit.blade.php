@extends('panel.company.master')

@section('title','ثبت موقعیت کارآموزی')

@section('css')
    <link href="{{ asset('assets/panel/css/style-css/create-job-position.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/panel/css/style-css/edit-job-position.css') }}" rel="stylesheet" />
@endsection

@section('main')
<style>
    .autocomplete {
    /*the container must be positioned relative:*/
    position: relative;
    display: inline-block;
    }

    .autocomplete-items {
    position: absolute;
    border: 1px solid #d4d4d4;
    border-bottom: none;
    border-top: none;
    z-index: 99;
    /*position the autocomplete items to be the same width as the container:*/
    top: 100%;
    left: 0;
    right: 0;
    }
    .autocomplete-items div {
    padding: 10px;
    cursor: pointer;
    background-color: #fff;
    border-bottom: 1px solid #d4d4d4;
    }
    .autocomplete-items div:hover {
    /*when hovering an item:*/
    background-color: #e9e9e9;
    }
    .autocomplete-active {
    /*when navigating through the items using the arrow keys:*/
    background-color: DodgerBlue !important;
    color: #ffffff;
    }
</style>
    

    {{-- <div class="row mt-5 align-items-center row-gap-3">
        <div class="col-md-12 col-12">
                <form action="{{ route('show.pos.job') }}" id="selectJobForm" method="POST" class="d-flex p-2 rounded-pill-c box-shadow-c bg-white-c px-4 flex-md-row flex-column">
                    @csrf
                    <div class="d-flex border-left-c w-50 pb-md-0 pb-2">
                        <div class="align-self-center">
                            <img src="{{ asset('assets/frontend/img/search-jobs/search.svg') }}" class="img-fluid w-90">
                        </div>
                        <div id="category_text" class="select-custom w-100 dropdown" data-bs-toggle="dropdown" aria-expanded="false">گروه شغلی خود را جستجو کنید</div>
                        <input type="hidden" value="" id="category_input">
                        <div class="dropdown-menu pb-3 mw-dropdown overflow-y-scroll" style="height: 60vh;">
                            <ul class="">
                                @foreach ($categories as $category)
                                    <li role="button" 
                                    onclick="setValueToInput('{{ $category->title }}' , '{{ $category->id }}')"
                                    class="dropdown-item" value="{{ $category->id }}">{{ $category->title }}</li>
                                @endforeach
                                
                            </ul>
                        </div>
    
                    </div>
                    <div class="w-50 down-icon-input pt-md-0 pt-2">
                        <input id="job_title" oninput="autocompleteAjax()" type="text" placeholder="شغل خود را انتخاب کنید" class="select-custom dropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        <input name="job_id" type="hidden" value="" id="job_id">
                        <div id="dropdownElement" class="dropdown-menu pb-3 mw-dropdown overflow-y-scroll" style="height: 60vh;"></div>
                    </div>
                </form>
                
                <div class="w-50 down-icon-input pt-md-0 pt-2">
                    <input id="job_title" oninput="autocompleteAjax()" placeholder="شغل خود را انتخاب کنید" class="select-custom  dropdown" data-bs-toggle="dropdown" aria-expanded="false">
                    <div id="dropdownElement" class="dropdown-menu pb-3 mw-dropdown"></div>
                    <div id="dropdownElement" class="dropdown-menu pb-3 mw-dropdown">
                        <ul>
                            <li><a class="dropdown-item" href="#">شغل ۱</a></li>
                            <li><a class="dropdown-item" href="#">شغل ۲</a></li>
                            <li><a class="dropdown-item" href="#">شغل ۳</a></li>
                        </ul>
                        <p class="ms-4 red fs-17">این شغل وجود ندارد ... !</p>
                        <button class="bg-blue-c text-white fs-6 px-2 py-1 btn-job-p rounded-3 float-end me-3 px-4">+ اضافه کردن</button>
                    </div>
                    
                </div>
                

        </div>
        <div class="col-12 col-md-2">
            <button class="text-white fs-18 bg-blue-c box-shadow-c rounded-pill text-center btn-job-p w-100 mt-md-0 mt-3">انتخاب</button>
        </div>
    </div> --}}




    @isset($jp)
        <div class="row mt-5">
            <div class="col-md-6 col-12 yellow-circle position-relative">
                <h3 class="fs-5 bg-blue-header rounded-top-3 p-3 text-white-c text-center fs-md-16 mb-md-0">گروه شغلی : {{ $jp->job->category->title }}</h3>
            </div>
            <div class="col-md-6 col-12">
                <h3 class="fs-5 bg-blue-header rounded-top-3 p-3 text-white-c text-center fs-md-16 mb-md-0"> شغل : {{ $jp->title }}</h3>
            </div>
        </div>

            
        <form action="{{ route('update.pos.job') }}" method="post" id="savedForm">
            @csrf
            <input type="hidden" name="job_id" value="{{ $job->id }}">
            <input type="hidden" name="jp_id" value="{{ $jp->id }}">
            <input type="hidden" name="title" value="{{ $jp->title }}">
            <input type="hidden" name="description" value="{{ $jp->description }}">
            <div class="px-md-7 px-2 card pb-8">
                <div class="mt-9 rounded-bottom-3 py-5 px-md-6 px-3 box-body">
                    <div class="d-flex align-items-center heading-top position-relative">
                        <div class="line-top"></div>
                        <h3 class="fw-bold px-3">عنوان</h3>
                        <div class="line-top"></div>
                    </div>

                    <label for="" class="form-lable ps-4">
                        ویرایش عنوان شغل
                        {{-- <span class="text-warning"> (توجه داشته باشید عنوان را به طور کامل نمیتوانید تغییر دهید) </span> --}}
                    </label>
                    <input type="text" name="title" value="{{ $jp->title }}" class="form-control rounded-pill w-50">
    
                    {{-- <button type="button" class="blue bg-yellow-c rounded-pill btn-edit">
                        + ویرایش کردن توضیحات
                    </button> --}}
    
                </div>

                @if ($job->description)
                    <div class="mt-9 rounded-bottom-3 py-5 px-md-6 px-3 box-body">
                        <div class="d-flex align-items-center heading-top position-relative">
                            <div class="line-top"></div>
                            <h3 class="fw-bold px-3">توضیحات</h3>
                            <div class="line-top"></div>
                        </div>
        
                        <p class="fs-18 lh-lg text-justify"> 
                            {{ $job->description }}
                        </p>
        
                        {{-- <button type="button" class="blue bg-yellow-c rounded-pill btn-edit">
                            + ویرایش کردن توضیحات
                        </button> --}}
        
                    </div>
                @endif
    
                <script>
                    let skill = false;
                    let skillNum = 0;
                </script>
                @if($skills && $skills->count() > 0)
                    <div id="skillBox" class="mt-9 rounded-4 py-5 px-md-6 px-3 box-body duties-box position-relative">
                        <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3">
                            <h3 class="fw-bold text-white-c fs-5 mb-0 task-title">مهارت ها</h3>
                        </div>

                        <script>
                            skill = true;
                            skillNum = {{ $skills->count() }}
                        </script>
    
                        @if ($jp->skills->count() > 0)
                            @foreach ($jp->skills as $skill)
                                    <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                        <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                            <div class="d-flex align-self-start align-self-md-center">
                                                
                                                <input onclick="setDataInForm(this , 'skill')" class="form-check-input" type="checkbox"
                                                    data-type-name="skill_{{ $skill->skill->id }}"
                                                    data-type-range="{{ $skill->importance }}"
                                                    id="skillInput-{{ $loop->index + 1 }}" value="{{ $skill->skill->id }}" />

                                                <label for="skillInput-{{ $loop->index + 1 }}" 
                                                    class="text-dark-c mb-0 fs-16 fw-normal">{{ $skill->skill->title }}</label>

                                            </div>
                                            <div class="d-flex width-range gap-3 align-items-center me-md-5">
                                                
                                                <input oninput="changeDataRange(this , 'skill')" id="skillRange-{{ $loop->index + 1 }}" type="range" 
                                                    skillLevel=""  class="form-range" min="1" max="5" step="1" value="{{ round($skill->importance) }}">

                                                <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                                    {{ getOnetLevel($skill->importance) }}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        @endif
    
                        {{-- <div class="bg-body-2 mt-4 rounded-4 p-3 d-flex flex-column input-add-active">
    
                            <input placeholder="اضافه کردن مورد دلخواه ..." class="input-add">
                            <div class="d-flex align-items-md-center gap-3 flex-md-row flex-column align-items-start mt-md-0 mt-3">
                                <span class="fw-medium feedback-range">میزان اهمیت :</span>
                                <div class="width-range">
                                    <input type="range" name="slider" class="form-range" min="0" max="4" step="1" value="None">
                                </div>
                            </div>
    
                            <button class="blue bg-yellow-c btn-edit2 align-self-end">
                                + اضافه کردن
                            </button>
    
                        </div> --}}
    
                    
    
                        <div class="bg-body-2 mt-4 rounded-4 p-3">
                            <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseskill" aria-expanded="false">موارد بیشتر</span>
                            
                            <div class="collapse" id="collapseskill">
    
                                @foreach ($skills as $skill)
                                    @php
                                        $jpSkills = array();
                                        foreach ($jp->skills as $skl) {
                                            array_push($jpSkills , $skl->skill->id);
                                        }
                                    @endphp
                                    @if (! in_array($skill->skillname->id , $jpSkills))
                                        <div class="bg-body-2 mt-4 rounded-4 skill-active">
                                            <div class="box-body p-3 rounded-4">
                                                <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                                    <div class="d-flex align-self-start align-self-md-center">

                                                        <input onclick="setDataInForm(this , 'skill')" class="form-check-input" type="checkbox"
                                                            id="skillInput-{{ $loop->index + 1 }}" value="{{ $skill->skillname->id }}" />

                                                        <label for="skillInput-{{ $loop->index + 1 }}" 
                                                            class="text-dark-c mb-0 fs-16 fw-normal">{{ $skill->skillname->title }}</label>

                                                    </div>
                                                    <div class="d-flex width-range gap-3 align-items-center me-md-5">

                                                        <input oninput="changeDataRange(this , 'skill')" id="skillRange-{{ $loop->index + 1 }}" type="range" 
                                                            skillLevel=""  class="form-range" min="1" max="5" step="1" value="{{ round($skill->level->data_value) }}">

                                                        <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                                            {{ getOnetLevel($skill->level->data_value) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
    
                            </div>
                        
                        </div>
    
                    </div>
                    <p id="skillErr" class="red form-feedback fs-18 mt-2"></p>
                @endif
    
                <script>
                    let knowledge = false;
                    let knowledgeNum = 0;
                </script>
                @if($knowledges && $knowledges->count() > 0)
                    <div id="knowledgeBox" class="mt-9 rounded-4 py-5 px-md-6 px-3 box-body skills-box position-relative ">
                        <div class=" position-relative">
    
                            <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3 skill-title position-relative">
                                <h3 class="fw-bold text-white-c fs-5 mb-0">دانش</h3>
                            </div>

                            <script>
                                knowledge = true;
                                knowledgeNum = {{ $knowledges->count() }}
                            </script>
    
                            @if ($jp->knowledges->count() > 0)
                                @foreach ($jp->knowledges as $knowledge)
                                    <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                        <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                            <div class="d-flex align-self-start align-self-md-center">

                                                <input onclick="setDataInForm(this , 'knowledge')" class="form-check-input" type="checkbox"
                                                    data-type-name="knowledge_{{ $knowledge->knowledge->id }}"
                                                    data-type-range="{{ $knowledge->importance }}"
                                                    id="knowledgeInput-{{ $loop->index + 1 }}" value="{{ $knowledge->knowledge->id }}" />

                                                <label for="knowledgeInput-{{ $loop->index + 1 }}" 
                                                    class="text-dark-c mb-0 fs-16 fw-normal">{{ $knowledge->knowledge->title }}</label>

                                            </div>
                                            <div class="d-flex width-range gap-3 align-items-center me-md-5">

                                                <input oninput="changeDataRange(this , 'knowledge')" id="knowledgeRange-{{ $loop->index + 1 }}" type="range" 
                                                    class="form-range" min="1" max="5" step="1" value="{{ round($knowledge->importance) }}">

                                                <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                                    {{ getOnetLevel($knowledge->importance) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
    
    
                            <div class="bg-body-2 mt-4 rounded-4 p-3 skills-box2">
                                <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseknowledge" aria-expanded="false">موارد بیشتر</span>
                                
                                <div class="collapse" id="collapseknowledge">
    
                                    @foreach ($knowledges as $knowledge)
                                        @php
                                            $jpKnowledge = array();
                                            foreach ($jp->knowledges as $kno) {
                                                array_push($jpKnowledge, $kno->knowledge->id);
                                            }
                                        @endphp
                                        @if (! in_array($knowledge->knowledgeName->id , $jpKnowledge))
                                            <div class="bg-body-2 mt-4 rounded-4 skill-active">
                                                <div class="box-body p-3 rounded-4">
                                                    <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                                        <div class="d-flex align-self-start align-self-md-center">
    
                                                            <input onclick="setDataInForm(this , 'knowledge')" class="form-check-input" type="checkbox"
                                                                id="knowledgeInput-{{ $loop->index + 1 }}" value="{{ $knowledge->knowledgeName->id }}" />
        
                                                            <label for="knowledgeInput-{{ $loop->index + 1 }}" 
                                                                class="text-dark-c mb-0 fs-16 fw-normal">{{ $knowledge->knowledgeName->title }}</label>

                                                        </div>
                                                        <div class="d-flex width-range gap-3 align-items-center me-md-5">
                                                            
                                                            <input oninput="changeDataRange(this , 'knowledge')" id="knowledgeRange-{{ $loop->index + 1 }}" type="range" 
                                                                class="form-range" min="1" max="5" step="1" value="{{ round($knowledge->level->data_value) }}">

                                                            <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                                                {{ getOnetLevel($knowledge->level->data_value) }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            
                            </div>
                        </div>
    
    
    
                    </div>
                    <p id="knowledgeErr" class="red form-feedback fs-18 mt-2"></p>
                @endif
                
                <script>
                    let ability = false;
                    let abilityNum = 0;
                </script>
                @if($abilities && $abilities->count() > 0)
                    <div id="abilityBox" class="mt-10 rounded-4 py-5 px-md-6 px-3 box-body duties-box position-relative">
                        <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3">
                            <h3 class="fw-bold text-white-c fs-5 mb-0 task-title">توانایی ها</h3>
                        </div>

                        <script>
                            ability = true;
                            abilityNum = {{ $abilities->count() }};
                        </script>
                        @if ($jp->abilities->count() > 0)
                            @foreach ($jp->abilities as $ability)
                                <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                    <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                        <div class="d-flex align-self-start align-self-md-center">

                                            <input onclick="setDataInForm(this , 'ability')" class="form-check-input" type="checkbox"
                                                data-type-name="ability_{{ $ability->ability->id }}"
                                                data-type-range="{{ $ability->importance }}"
                                                id="abilityInput-{{ $loop->index + 1 }}" value="{{ $ability->ability->id }}" />

                                            <label for="abilityInput-{{ $loop->index + 1 }}" 
                                                class="text-dark-c mb-0 fs-16 fw-normal">{{ $ability->ability->title }}</label>

                                        </div>
                                        <div class="d-flex width-range gap-3 align-items-center me-md-5">
                                            <input oninput="changeDataRange(this , 'ability')" id="abilityRange-{{ $loop->index + 1 }}" type="range" 
                                                class="form-range" min="1" max="5" step="1" value="{{ round($ability->importance) }}">
                                            <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                                    {{ getOnetLevel($ability->importance) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
    
                        <div class="bg-body-2 mt-4 rounded-4 p-3">
                            <sapn class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseability" aria-expanded="false">موارد بیشتر</sapn>
                            
                            <div class="collapse" id="collapseability">
    
                                @foreach ($abilities as $ability)
                                    @php
                                        $jpability = array();
                                        foreach ($jp->abilities as $abl) {
                                            array_push($jpability , $abl->ability->id);
                                        }
                                    @endphp
                                    @if (! in_array($ability->abilityName->id , $jpability))
                                        <div class="bg-body-2 mt-4 rounded-4 skill-active">
                                            <div class="box-body p-3 rounded-4">
                                                <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                                    <div class="d-flex align-self-start align-self-md-center">

                                                        <input onclick="setDataInForm(this , 'ability')" class="form-check-input" type="checkbox"
                                                            id="abilityInput-{{ $loop->index + 1 }}" value="{{ $ability->abilityName->id }}" />

                                                        <label for="abilityInput-{{ $loop->index + 1 }}" 
                                                            class="text-dark-c mb-0 fs-16 fw-normal">{{ $ability->abilityName->title }}</label>

                                                    </div>
                                                    <div class="d-flex width-range gap-3 align-items-center me-md-5">

                                                        <input oninput="changeDataRange(this , 'ability')" id="abilityRange-{{ $loop->index + 1 }}" type="range" 
                                                            class="form-range" min="1" max="5" step="1" value="{{ round($ability->level->data_value) }}">

                                                        <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                                            {{ getOnetLevel($ability->level->data_value) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
    
                            </div>
                        
                        </div>
    
                    </div>
                    <p id="abilityErr" class="red form-feedback fs-18 mt-2"></p>
                @endif

                <script>
                    let workStyle = false;
                    let workStyleNum = 0;
                </script>
                @if($workStyles && $workStyles->count() > 0)
                    <div id="workStyleBox" class="mt-9 rounded-4 py-5 px-md-6 px-3 box-body skills-box position-relative ">
                        <div class=" position-relative">
    
                            <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3 skill-title position-relative">
                                <h3 class="fw-bold text-white-c fs-5 mb-0">ویژگی های کاری</h3>
                            </div>
                            
                            <script>
                                workStyle = true;
                                workStyleNum = {{ $workStyles->count() }}
                            </script>
                            @if ($jp->workStyles->count() > 0)
                                @foreach ($jp->workStyles as $workStyle)
                                    <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                        <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                            <div class="d-flex align-self-start align-self-md-center">

                                                <input onclick="setDataInForm(this , 'workStyle')" class="form-check-input" type="checkbox"
                                                    data-type-name="workStyle_{{ $workStyle->workStyle->id }}"
                                                    data-type-range="{{ $workStyle->importance }}"
                                                    id="workStyleInput-{{ $loop->index + 1 }}" value="{{ $workStyle->workStyle->id }}" />
                                                
                                                <label for="workStyleInput-{{ $loop->index + 1 }}" 
                                                    class="text-dark-c mb-0 fs-16 fw-normal">{{ $workStyle->workStyle->title }}</label>

                                            </div>
                                            <div class="d-flex width-range gap-3 align-items-center me-md-5">
                                                
                                                <input oninput="changeDataRange(this , 'workStyle')" id="workStyleRange-{{ $loop->index + 1 }}" type="range" 
                                                    class="form-range" min="1" max="5" step="1" value="{{ round($workStyle->importance ?? 0) }}" />

                                                <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                                    {{ getOnetLevel($workStyle->importance ?? 0) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
    
    
                            <div class="bg-body-2 mt-4 rounded-4 p-3 skills-box2">
                                <sapn class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapseworkStyle" aria-expanded="false">موارد بیشتر</sapn>
                                
                                <div class="collapse" id="collapseworkStyle">
    
                                    @foreach ($workStyles as $workStyle)
                                        @php
                                            $jpworkStyle = array();
                                            foreach ($jp->workStyles as $skl) {
                                                array_push($jpworkStyle , $skl->workStyle->id);
                                            }
                                        @endphp
                                        @if (! in_array($workStyle->workStyleName->id , $jpworkStyle))
                                            <div class="bg-body-2 mt-4 rounded-4 skill-active">
                                                <div class="box-body p-3 rounded-4">
                                                    <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                                        <div class="d-flex align-self-start align-self-md-center">

                                                            <input onclick="setDataInForm(this , 'workStyle')" class="form-check-input" type="checkbox"
                                                                id="workStyleInput-{{ $loop->index + 1 }}" value="{{ $workStyle->workStyleName->id }}" />
                                                            
                                                            <label for="workStyleInput-{{ $loop->index + 1 }}" 
                                                                class="text-dark-c mb-0 fs-16 fw-normal">{{ $workStyle->workStyleName->title }}</label>

                                                        </div>
                                                        <div class="d-flex width-range gap-3 align-items-center me-md-5">
                                                            
                                                            <input oninput="changeDataRange(this , 'workStyle')" id="workStyleRange-{{ $loop->index + 1 }}" type="range" 
	                                                            class="form-range" min="1" max="5" step="1" value="{{ round($workStyle->importance->data_value ?? 0) }}">

                                                            <div for="" class="text-dark-c mb-0 fs-16 fw-normal fit-content">
                                                                {{ getOnetLevel($workStyle->importance->data_value ?? 0) }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                    
    
                                </div>
                            
                            </div>
                        </div>
    
    
    
                    </div>
                    <p id="workStyleErr" class="red form-feedback fs-18 mt-2"></p>
                @endif

                <script>
                    let technologySkill = false;
                    let technologySkillNum = 0;
                </script>
                @if($technologySkills && $technologySkills->count() > 0)
                    <div id="technologySkillBox" class="mt-10 rounded-4 py-5 px-md-6 px-3 box-body duties-box position-relative">
                        <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3">
                            <h3 class="fw-bold text-white-c fs-5 mb-0 task-title">مهارت های فنی</h3>
                        </div>

                        <script>
                            technologySkill = true;
                            technologySkillNum = {{ $technologySkills->count() }}
                        </script>
    
                        @if ($jp->technologySkills->count() > 0)
                            <div class="row">
                                @foreach ($jp->technologySkills as $technologySkill)
                                    <div class="col-md-6 col-12">
                                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                            <div class="flex-column d=flex">
                                                <div class="d-flex align-self-start align-self-md-center">
                                                    <input name="technologySkill_{{ $technologySkill->technologySkill->id }}" class="form-check-input" type="checkbox" 
                                                        data-type-name2=""
                                                        id="technologySkill-{{ $loop->index + 1 }}" value="{{ $technologySkill->id }}"/>
                                                    <label for="technologySkill-{{ $loop->index + 1 }}" 
                                                        class="text-dark-c mb-0 fs-16 fw-normal">{{ $technologySkill->technologySkill->title }}</label>
                                                </div>
                                                <div class="mb-0 fs-14 fw-normal text-info ms-5">مثال : {{ $technologySkill->example->example ?? '---' }}</div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
    
    
                        <div class="bg-body-2 mt-4 rounded-4 p-3 width-more mx-auto">
                            <span class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapsetechnologySkills" aria-expanded="false">موارد بیشتر</span>
                        </div>
    
                            <div class="collapse" id="collapsetechnologySkills">
                                <div class="row">
                                    @foreach ($technologySkills as $technologySkill)
                                        @php
                                            $jptech = array();
                                            foreach ($jp->technologySkills as $tech) {
                                                array_push($jptech , $tech->technologySkill->id);
                                            }
                                        @endphp
                                        @if (! in_array($technologySkill->technologySkillName->id , $jptech))
                                            <div class="col-md-6 col-12">
                                                <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                                    <div class="flex-column d=flex">
                                                        <div class="d-flex align-self-start align-self-md-center">
                                                            <input name="technologySkill_{{ $technologySkill->technologySkillName->id }}" class="form-check-input" type="checkbox" 
                                                                id="technologySkill-{{ $loop->index + 1 }}" value="{{ $technologySkill->example }}"/>
                                                            <label for="technologySkill-{{ $loop->index + 1 }}" 
                                                                class="text-dark-c mb-0 fs-16 fw-normal">{{ $technologySkill->technologySkillName->title }}</label>
                                                        </div>
                                                        <div class="mb-0 fs-14 fw-normal text-info ms-5">مثال : {{ $technologySkill->example->example }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        
                   
                    </div>
                    <p id="technologySkillErr" class="red form-feedback fs-18 mt-2"></p>
                @endif

                <script>
                    let task = false;
                    let taskNum = 0;
                </script>

                @if($tasks && $tasks->count() > 0)
                    <div id="taskBox" class="mt-9 rounded-4 py-5 px-md-6 px-3 box-body skills-box position-relative mb-9">
                        <div class=" position-relative">
    
                            <div class="heading-box-2 rounded-bottom-3 heading-top-2 px-7 py-3 skill-title position-relative">
                                <h3 class="fw-bold text-white-c fs-5 mb-0">وظایف</h3>
                            </div>

                            <script>
                                task = true;
                                taskNum = {{ $tasks->count() }}
                            </script>
    
                            @if ($jp->tasks->count() > 0)
                                @foreach ($jp->tasks as $task)
                                    <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                        <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                            <div class="d-flex align-self-start align-self-md-center">
                                                <input name="task_{{ $task->id }}" class="form-check-input" type="checkbox"
                                                    data-type-name2=""
                                                    id="task-{{ $loop->index + 1 }}" value="{{ $task->task->id }}"/>
                                                <label for="task-{{ $loop->index + 1 }}" 
                                                    class="text-dark-c mb-0 fs-16 fw-normal">{{ $task->task->title }}</label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
    
    
                            <div class="bg-body-2 mt-4 rounded-4 p-3 skills-box2">
                                <sapn class="blue3 fs-17 pointer more-icon position-relative" data-bs-toggle="collapse" data-bs-target="#collapsetask" aria-expanded="false">موارد بیشتر</sapn>
                                
                                <div class="collapse" id="collapsetask">
    
                                    @foreach ($tasks as $task)
                                        @php
                                            $jptask = array();
                                            foreach ($jp->tasks as $tk) {
                                                array_push($jptask, $tk->task->id);
                                            }
                                        @endphp
                                        @if (! in_array($task->id , $jptask))
                                            <div class="bg-body-2 mt-4 rounded-4 skill-active">
                                                <div class="box-body p-3 rounded-4">
                                                    <div class="d-flex align-items-center gap-md-5 gap-3 flex-md-row flex-column">
                                                        <div class="d-flex align-self-start align-self-md-center">
                                                            <input name="task_{{ $task->id }}" class="form-check-input" type="checkbox" 
                                                                id="task-{{ $loop->index + 1 }}" value="{{ $task->id }}"/>
                                                            <label for="task-{{ $loop->index + 1 }}" 
                                                                class="text-dark-c mb-0 fs-16 fw-normal">{{ $task->title }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                    
    
                                </div>
                            
                            </div>
                        </div>
    
    
    
                    </div>
                    <p id="taskErr" class="red form-feedback fs-18 mt-2"></p>
                @endif
    
                {{-- form  --}}
    
                <div class="mt-9 rounded-bottom-3 py-5 px-md-4 box-body">
                    <div class="d-flex align-items-center heading-top position-relative justify-content-center">
                        <div class="line-top2"></div>
                        <h3 class="fw-bold px-3 width-fit-content">ثبت اطلاعات</h3>
                        <div class="line-top2"></div>
                    </div>
    
                    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
                        <h4 class="form-title">مشخصات کاری</h4>
                        <div class="row">

                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title necessary">نوع همکاری:</label>
                                <select name="type" class="form-select">
                                    @if (!$isIntern)
                                        <option 
                                        @if ($jp->type == 'full-time' || old('type') == 'full-time')
                                            selected
                                        @endif
                                        value="full-time" selected>{{ job_type_persian('full-time') }}</option>

                                        <option 
                                        @if ($jp->type == 'part-time' || old('type') == 'part-time')
                                            selected
                                        @endif
                                        value="part-time"> {{ job_type_persian('part-time') }} </option>

                                        <option 
                                        @if ($jp->type == 'remote' || old('type') == 'remote')
                                            selected
                                        @endif
                                        value="remote"> {{ job_type_persian('remote') }} </option>
                                    @endif

                                    @if ($isIntern)
                                        <option 
                                        @if ($jp->type == 'intern' || old('type') == 'intern')
                                            selected
                                        @endif
                                        value="intern"> {{ job_type_persian('intern') }} </option>
                                    @endif

                                </select>
                                <p id="typeErr" class="red form-feedback text-end mt-2"></p>
                                {{-- @error('type')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror --}}
                            </div>
    
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title">سابقه کار:</label>
                                <select name="experience" class="form-select">
                                    <option 
                                    @if ($jp->experience == 'unmatter' || old('experience') == 'unmatter')
                                        selected
                                    @endif
                                    value="2">فرقی ندارد</option>

                                    <option 
                                    @if ($jp->experience == '2' || old('experience') == '2')
                                        selected
                                    @endif
                                    value="2">تا ۲ سال</option>

                                    <option 
                                    @if ($jp->experience == '5' || old('experience') == '5')
                                        selected
                                    @endif
                                    value="5">۳ تا ۵ سال</option>

                                    <option 
                                    @if ($jp->experience == '10' || old('experience') == '10')
                                        selected
                                    @endif
                                    value="10">۶ تا ۱۰ سال</option>

                                    <option 
                                    @if ($jp->experience == '11' || old('experience') == '11')
                                        selected
                                    @endif
                                    value="11">بالای ۱۰ سال</option>

                                </select>
                                @error('experience')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                            </div>
    
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title">سطح تجربه:</label>
                                <select name="level" class="form-select">
                                    <option 
                                    @if ($jp->level == 'unmatter' || old('level') == 'unmatter')
                                        selected
                                    @endif
                                    value="unmatter">فرقی ندارد</option>

                                    <option 
                                    @if ($jp->level == 'senior' || old('level') == 'senior')
                                        selected
                                    @endif
                                    value="senior">حرفه ای</option>

                                    <option 
                                    @if ($jp->level == 'mid-level' || old('level') == 'mid-level')
                                        selected
                                    @endif
                                    value="mid-level">متوسط</option>

                                    <option 
                                    @if ($jp->level == 'junior' || old('level') == 'junior')
                                        selected
                                    @endif
                                    value="junior">تازه کار</option>

                                </select>
                                @error('level')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                            </div>
    
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title">ساعت کاری:</label>
                                <input name="working_hours"
                                    @if($jp->working_hours)
                                        value="{{ $jp->working_hours }}"
                                    @elseif (old('working_hours'))
                                        value="{{ old('working_hours') }}"
                                    @endif
                                placeholder="مثال : 8 الی 16"
                                type="text" class="form-control">
                                @error('working_hours')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="col-md-6 col-12 px-md-5 px-4 mb-2">
                                <label class="lable-title necessary">حقوق:</label>
                                <select name="salary" id="salary" class="form-select">
                                    <option 
                                    @if ($jp->salary == 'agreement' || old('salary') == 'agreement')
                                        selected
                                    @endif
                                    value="agreement">{{ getPersionSalaryForOnetJob('agreement') }}</option>
                                    <option 
                                    @if ($jp->salary == 'work_office' || old('salary') == 'work_office')
                                        selected
                                    @endif
                                    value="work_office">{{ getPersionSalaryForOnetJob('work_office') }}</option>
                                    <option 
                                    @if ($jp->salary == '4' || old('salary') == '4')
                                        selected
                                    @endif
                                    value="4">{{ getPersionSalaryForOnetJob('4') }}</option>
                                    <option 
                                    @if ($jp->salary == '8' || old('salary') == '8')
                                        selected
                                    @endif
                                    value="8">{{ getPersionSalaryForOnetJob('8') }}</option>
                                    <option 
                                    @if ($jp->salary == '16' || old('salary') == '16')
                                        selected
                                    @endif
                                    value="16">{{ getPersionSalaryForOnetJob('16') }}</option>
                                    <option 
                                    @if ($jp->salary == '25' || old('salary') == '25')
                                        selected
                                    @endif
                                    value="25">{{ getPersionSalaryForOnetJob('25') }}</option>
                                    <option 
                                    @if ($jp->salary == '30' || old('salary') == '30')
                                        selected
                                    @endif
                                    value="30">{{ getPersionSalaryForOnetJob('30') }}</option>
                                </select>
                                <p class="text-info form-feedback fs-16 mt-2">برای گرفتن بازخورد بهتر حقوق و دستمزد مورد نظر خود را انتخاب کنید.</p>
                            </div>
                        </div>
                        
                    </div>
    
    
                    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
                        <h4 class="form-title">مشخصات فردی</h4>
                        <div class="row">
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title">رنج سنی:</label>
                                <select name="age" class="form-select">
                                    <option 
                                        @if ($jp->age == '0' || old('age') == '0')
                                            selected
                                        @endif
                                    value="0">فرقی ندارد</option>

                                    <option 
                                        @if ($jp->age == '20' || old('age') == '20')
                                            selected
                                        @endif
                                    value="20">... تا 20 سال</option>

                                    <option 
                                        @if ($jp->age == '30' || old('age') == '30')
                                            selected
                                        @endif
                                    value="30">21 تا 30 سال</option>

                                    <option 
                                        @if ($jp->age == '40' || old('age') == '40')
                                            selected
                                        @endif
                                    value="40">31 تا 40 سال</option>

                                </select>
                                @error('age')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                            </div>
    
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title">جنسیت:</label>
                                <select name="sex" class="form-select">
                                    <option 
                                        @if ($jp->sex == 'unmatter' || old('sex') == 'unmatter')
                                            selected
                                        @endif
                                    value="unmatter">فرقی ندارد</option>

                                    <option 
                                        @if ($jp->sex == 'male' || old('sex') == 'male')
                                            selected
                                        @endif
                                    value="male">مرد</option>

                                    <option 
                                        @if ($jp->sex == 'female' || old('sex') == 'female')
                                            selected
                                        @endif
                                    value="female">زن</option>

                                </select>
                                @error('sex')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                            </div>
    
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title">وضعیت تاهل:</label>
                                <select name="marige_type" class="form-select">
                                    <option 
                                        @if ($jp->marige_type == 'unmatter' || old('marige_type') == 'unmatter')
                                            selected
                                        @endif
                                    value="unmatter">فرقی ندارد</option>

                                    <option 
                                        @if ($jp->marige_type == 'single' || old('marige_type') == 'single')
                                            selected
                                        @endif
                                    value="single">مجرد</option>

                                    <option 
                                        @if ($jp->marige_type == 'married' || old('marige_type') == 'married')
                                            selected
                                        @endif
                                    value="married">متاهل</option>
                                </select>
                                @error('marige_type')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                            </div>
    
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title">وضعیت نظام وظیفه:</label>
                                <select name="military_service_status" class="form-select">
                                    <option 
                                        @if ($jp->military_service_status == 'unmatter' || old('military_service_status') == 'unmatter')
                                            selected
                                        @endif
                                    value="unmatter">فرقی ندارد</option>

                                    <option 
                                        @if ($jp->military_service_status == 'پایان خدمت یا معافیت دائم' || old('military_service_status') == 'پایان خدمت یا معافیت دائم')
                                            selected
                                        @endif
                                    value="پایان خدمت یا معافیت دائم">پایان خدمت یا معافیت دائم</option>

                                    <option 
                                        @if ($jp->military_service_status == 'پایان خدمت' || old('military_service_status') == 'پایان خدمت')
                                            selected
                                        @endif
                                    value="پایان خدمت">پایان خدمت</option>

                                    <option 
                                        @if ($jp->military_service_status == 'معافیت تحصیلی' || old('military_service_status') == 'معافیت تحصیلی')
                                            selected
                                        @endif
                                    value="معافیت تحصیلی">معافیت تحصیلی</option>

                                    <option 
                                        @if ($jp->military_service_status == 'مشمول' || old('military_service_status') == 'مشمول')
                                            selected
                                        @endif
                                    value="مشمول">مشمول</option>

                                </select>
                                @error('military_service_status')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        
                    </div>
    
    
                    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-5">
                        <h4 class="form-title">تحصیلات</h4>
                        <div class="row">

                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title">مقطع تحصیلی:</label>
                                <select name="grade" class="form-select">
                                    <option 
                                        @if ($jp->grade == 'unmatter' || old('grade') == 'unmatter')
                                            selected
                                        @endif
                                    value="unmatter">فرقی ندارد</option>

                                    <option 
                                        @if ($jp->grade == 'دکترا' || old('grade') == 'دکترا')
                                            selected
                                        @endif
                                    value="دکترا">دکترا</option>

                                    <option 
                                        @if ($jp->grade == 'کارشناسی ارشد' || old('grade') == 'کارشناسی ارشد')
                                            selected
                                        @endif
                                    value="کارشناسی ارشد">کارشناسی ارشد</option>

                                    <option 
                                        @if ($jp->grade == 'کارشناسی' || old('grade') == 'کارشناسی')
                                            selected
                                        @endif
                                    value="کارشناسی">کارشناسی</option>

                                    <option 
                                        @if ($jp->grade == 'کاردانی' || old('grade') == 'کاردانی')
                                            selected
                                        @endif
                                    value="کاردانی">کاردانی</option>

                                    <option 
                                        @if ($jp->grade == 'دیپلم' || old('grade') == 'دیپلم')
                                            selected
                                        @endif
                                    value="دیپلم">دیپلم</option>

                                </select>
                                @error('grade')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                            </div>
    
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title">رشته تحصیلی:</label>
                                <select name="major" id="major" class="form-select">
                                    <option 
                                        @if ($jp->major_id == null || old('major') == 'unmatter')
                                            selected
                                        @endif
                                    value="unmatter">فرقی ندارد</option>
                                    @foreach ($majors as $major)
                                        <option 
                                            @if ($jp->major_id == $major->id || old('major') == $major->id)
                                                selected
                                            @endif
                                        value="{{ $major->id }}">{{ $major->title }}</option>
                                    @endforeach
                                </select>
                                
                                @error('major')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                                
                            </div>
    
                        </div>
                    </div>
    
                    <div id="workPlaceInfoBox" class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
                        <h4 class="form-title">مشخصات محل کار</h4>
                        <div class="row">
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title necessary">استان:</label>
                                <select name="province" id="province" class="form-select" onchange="getCities()">
                                    <option value="">انتخاب ...</option>
                                    @foreach ($provinces as $province)
                                        <option 
                                            @if ($jp->province_id == $province->id || old('province') == $province->id)
                                                selected
                                            @endif
                                        value="{{ $province->id }}">{{ $province->name }}</option>
                                    @endforeach

                                </select>
                                <p id="provinceErr" class="red form-feedback text-end mt-2"></p>
                                {{-- @error('province')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror --}}
                            </div>
    
                            <div class="col-md-6 col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title necessary">شهر:</label>
                                <select class="form-select" id="city" name="city">
                                    <option value="unmatter">ابتدا استان را انتخاب کنید</option>
                                    @if ($jp->city)
                                        <option selected value="{{ $jp->city_id }}">{{ $jp->city->name }}</option>
                                    @endif
                                </select>
                                <p id="cityErr" class="red form-feedback text-end mt-2"></p>
                                {{-- @error('city')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror --}}
                            </div>
    
                            <div class="col-12 px-md-6 px-4 mb-2">
                                <label class="lable-title necessary">آدرس کامل:</label>
                                <input 
                                @if ($jp->address || old('address'))
                                    value="{{ $jp->address ?? old('address') }}"
                                @endif
                                name="address" type="text" class="form-control" placeholder="آدرس...">
                                <p id="addressErr" class="red form-feedback text-end mt-2"></p>
                                {{-- @error('address')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror --}}
                            </div>
                        </div>
                        
                    </div>
    
                    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
                        <h4 class="form-title">تسهیلات</h4>
                        <div class="row">
                            <div class="col-12 px-md-6 px-4 mb-2">
                                <div class="row">

                                    <div class="col-md-6 col-12">
                                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                            <div class="flex-column">
                                                <div class="d-flex align-self-start align-self-md-center">
                                                    <input class="form-check-input" type="checkbox" id="facilities-bime-tamin" name="facilities_بیمه تامین اجتماعی" />
                                                    <label for="facilities-bime-tamin" class="text-dark-c mb-0 fs-16 fw-normal">بیمه تامین اجتماعی</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                            <div class="flex-column">
                                                <div class="d-flex align-self-start align-self-md-center">
                                                    <input class="form-check-input" type="checkbox" id="facilities-bime-takmili" name="facilities_بیمه تکمیلی" />
                                                    <label for="facilities-bime-takmili" class="text-dark-c mb-0 fs-16 fw-normal">بیمه تکمیلی</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                            <div class="flex-column">
                                                <div class="d-flex align-self-start align-self-md-center">
                                                    <input class="form-check-input" type="checkbox" id="facilities-service" name="facilities_سرویس ایاب و ذهاب" />
                                                    <label for="facilities-service" class="text-dark-c mb-0 fs-16 fw-normal">سرویس ایاب و ذهاب</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                            <div class="flex-column">
                                                <div class="d-flex align-self-start align-self-md-center">
                                                    <input class="form-check-input" type="checkbox" id="facilities-ezafekari" name="facilities_اضافه کاری" />
                                                    <label for="facilities-ezafekari" class="text-dark-c mb-0 fs-16 fw-normal">اضافه کاری</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                            <div class="flex-column">
                                                <div class="d-flex align-self-start align-self-md-center">
                                                    <input class="form-check-input" type="checkbox" id="facilities-padash" name="facilities_پاداش" />
                                                    <label for="facilities-padash" class="text-dark-c mb-0 fs-16 fw-normal">پاداش</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="bg-body-2 mt-4 rounded-4 p-3 skill-active">
                                            <div class="flex-column">
                                                <div class="d-flex align-self-start align-self-md-center">
                                                    <input class="form-check-input" type="checkbox" id="facilities-safartafrihi" name="facilities_سفر های تفریحی" />
                                                    <label for="facilities-safartafrihi" class="text-dark-c mb-0 fs-16 fw-normal">سفر های تفریحی</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                
                                @error('facilities')
                                    <p class="red form-feedback text-end mt-2">{{ $message }}</p>
                                @enderror
                            </div>
    
                        </div>
                        
                    </div>
    
                    <div class="border-gray-dashed mt-7 px-md-6 px-4 py-4">
                        <h4 class="form-title">توضیحات</h4>
                        <div class="row">
                            <textarea class="form-control" name="about" id="editor" cols="30" rows="5"></textarea>
    
                        </div>
                        
                    </div>
    
                    <div class="d-flex justify-content-center mt-5">
                        <button type="button" 
                            onclick="submitForm()" 
                            class="text-white-c bg-blue-c btn-add">
                            ثبت
                        </button>
                    </div>
                </div>
            </div>
        </form>
    @endisset

<input type="hidden" id="getCitiesUrl" value="{{ route('get.cities.ajax.front') }}">
<input type="hidden" id="Csrf32" value="{{ csrf_token() }}">
@endsection



@section('script')
    <script>
        let jobsByCatIdUrl = "{{ route('get.jobs.by.catid') }}";
        let jobNameUrl = "{{ route('get.jobs.by.catid.and.name') }}";
        let selectJobUrl = "{{ route('show.pos.job') }}";
    </script>
    <script src="{{ asset('assets/panel/js/company.js') }}"></script>
    <script src="{{ asset('assets/panel/js/job/create-job-position.js') }}"></script>
    <script src="{{ asset('assets/panel/js/job/create-intern-position.js') }}"></script>
    <script src="{{ asset('assets/panel/js/job/edit-onet.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/ckeditor2/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/ckeditor2/translations/fa.js') }}"></script>
    <script src="{{ asset('assets/panel/js/job/onet-validation.js') }}"></script>
@endsection