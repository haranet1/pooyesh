@extends('panel.company.master')

@section('title',$title)

@section('css')
    <link href="{{ asset('assets/panel/css/style-css/create-job-position.css') }}" rel="stylesheet" />
@endsection

@section('main')


    @include('job.position.co.components.select-job')

    @isset($job)
        <div class="row mt-5">
            <div class="col-md-6 col-12 yellow-circle position-relative">
                <h3 class="fs-5 bg-blue-header rounded-top-3 p-3 text-white-c text-center fs-md-16 mb-md-0">گروه شغلی : {{ $job->category->title }}</h3>
            </div>
            <div class="col-md-6 col-12">
                <h3 class="fs-5 bg-blue-header rounded-top-3 p-3 text-white-c text-center fs-md-16 mb-md-0"> شغل : {{ $job->title }}</h3>
            </div>
        </div>

            
        <form action="{{ route('store.pos.job') }}" method="get" id="savedForm">
            <input type="hidden" name="job_id" value="{{ $job->id }}">
            {{-- <input type="hidden" name="title" value="{{ $job->title }}"> --}}
            <input type="hidden" name="description" value="{{ $job->description }}">

            <div class="px-md-7 px-2 card pb-8">
                
                @include('job.position.co.components.title-section')

                @include('job.position.co.components.description-section')
    
                @include('job.position.co.components.skills-section')
    
                @include('job.position.co.components.knowledge-section')

                @include('job.position.co.components.ability-section')

                @include('job.position.co.components.work-style-section')

                @include('job.position.co.components.technology-skill-section')

                @include('job.position.co.components.tasks-section')
                
                {{-- form  --}}
    
                @include('job.position.co.components.form')
            </div>
        </form>
    @endisset

<input type="hidden" id="getCitiesUrl" value="{{ route('get.cities.ajax.front') }}">
<input type="hidden" id="Csrf32" value="{{ csrf_token() }}">
@endsection



@section('script')
    <script>
        let jobsByCatIdUrl = "{{ route('get.jobs.by.catid') }}";
        let jobNameUrl = "{{ route('get.jobs.by.catid.and.name') }}";
        let selectJobUrl = "{{ route('show.pos.job') }}";
        let companyPackageUrl = "{{ route('index.package.company') }}"
    </script>
    <script src="{{asset('assets/panel/js/company.js')}}"></script>
    <script src="{{ asset('assets/panel/js/job/create-job-position.js') }}"></script>
    <script src="{{ asset('assets/panel/js/job/create-intern-position.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/ckeditor2/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/panel/plugins/ckeditor2/translations/fa.js') }}"></script>
    <script src="{{ asset('assets/panel/js/job/onet-validation.js') }}"></script>
    <script src="{{ asset('assets/panel/js/job/hire-or-intern.js') }}"></script>
@endsection

