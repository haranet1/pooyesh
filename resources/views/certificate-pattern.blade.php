<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <link  href="{{public_path('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
    <title>form</title>
    <style>

        body, html {
            height: 100%;
            margin: 0;
            padding: 0;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .container {
            padding: 20px;
            text-align: center;
        }

        body{
            background-image: url('{{public_path('bg.jpg')}}');
            margin: 0;
            padding: 0;
            background-repeat: no-repeat;
            background-size:contain;
            background-position: center center;
        }


    </style>
</head>

<body style=" font-family: fa; text-align: center;" >
<div class="container" style="margin-top: 50px;margin-left: 140px; padding: 20px;">
    <div class="row text-center" style="margin-top: 50px;">
        <div class="col-8 text-center">
            <h1 style="font-size: 32px">بسمه تعالی</h1>
        </div>
    </div>

    <div class="row text-center" style="margin-top: 30px">
        <div class="col-8">
            <h2 style="font-size: 24px">گواهی پایان دوره پویش</h2>
            <h5 style="font-size: 18px">دانشگاه آزاد اسلامی واحد نجف آباد</h5>
        </div>
    </div>

    <div class="row text-center" style="margin-top: 80px">
        <div class="col-8" style="font-size: 18px">
            <p> بدینوسیله گواهی میگردد آقای/خانم</p>
            <p>علی معروفی</p>
            <p>فرزند</p>
            <p>علی</p>
            <p>به شماره ملی</p>
            <p>108002546</p>
            <p>دوره کارآموزی خود را طرح پویش دانشگاه آزاد اسلامی واحد نجف آباد با موفقیت به پایان رسانده است</p>
        </div>
    </div>

    <div class="row" style="margin-top: 50px">
        <div class="col-4 text-center" style="text-align: right;">
            مهر و امضا
        </div>
    </div>
</div>

</body>
</html>
