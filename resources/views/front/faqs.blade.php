@extends('front.master')
@section('title','سوالات متداول')
@section('main')
    <section class="mt-100 pxp-no-hero">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">سوالات متداول</h2>
            <p class="pxp-text-light text-center">ما به کارفرمایان و کاندیدها کمک می کنیم تا مناسب را پیدا کنند</p>

            <div class="row mt-4 mt-lg-5 justify-content-center">
                <div class="col-xxl-7">
                    <div class="accordion pxp-faqs-accordion" id="pxpFAQsAccordion">
                        <!-- 1 -->
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="pxpFAQsHeader1">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#pxpCollapseFAQs1" aria-expanded="false"
                                        aria-controls="pxpCollapseFAQs1">
                                    نسخه آزمایشی رایگان چگونه کار می کند؟
                                </button>
                            </h2>
                            <div id="pxpCollapseFAQs1" class="accordion-collapse collapse"
                                 aria-labelledby="pxpFAQsHeader1" data-bs-parent="#pxpFAQsAccordion">
                                <div class="accordion-body">
                                    یک طرح را انتخاب کنید و یک آزمایش رایگان 4 روزه در هر طرح اشتراکی دریافت کنید. در
                                    طول دوره آزمایشی خود، مشاغل را به صورت رایگان ارسال کنید. وقتی دوره آزمایشی رایگان
                                    شما به پایان برسد، از روش پرداختی که در حین ثبت نام ارائه کرده اید هزینه کسر می
                                    کنیم. می‌توانید در هر زمان با ورود به حساب خود، اشتراک خود را موقتاً متوقف، لغو یا
                                    تغییر دهید.
                                </div>
                            </div>
                        </div>
                        <!-- 2 -->
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="pxpFAQsHeader2">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#pxpCollapseFAQs2" aria-expanded="false"
                                        aria-controls="pxpCollapseFAQs2">
                                    آگهی شغلی من در کجا آگهی می شود؟
                                </button>
                            </h2>
                            <div id="pxpCollapseFAQs2" class="accordion-collapse collapse"
                                 aria-labelledby="pxpFAQsHeader2" data-bs-parent="#pxpFAQsAccordion">
                                <div class="accordion-body">پست شغلی شما در جابستر، صدها سایت خبری محلی، برنامه تلفن
                                    همراه ما و سایت های شبکه جابستر دیده می شود.
                                </div>
                            </div>
                        </div>
                        <!-- 3 -->
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="pxpFAQsHeader3">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#pxpCollapseFAQs3" aria-expanded="false"
                                        aria-controls="pxpCollapseFAQs3">
                                    آیا محدودیتی برای تعداد کاندیدهایی که می توانند برای پست من، درخواست دهند وجود
                                    دارد؟
                                </button>
                            </h2>
                            <div id="pxpCollapseFAQs3" class="accordion-collapse collapse"
                                 aria-labelledby="pxpFAQsHeader3" data-bs-parent="#pxpFAQsAccordion">
                                <div class="accordion-body">
                                    نه! آسمان برای مدت زمان ارسال شما محدودیت دارد.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="pxpFAQsHeader4">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#pxpCollapseFAQs4" aria-expanded="false"
                                        aria-controls="pxpCollapseFAQs4">
                                    به زودی دریافت رزومه را شروع خواهم کرد؟
                                </button>
                            </h2>
                            <div id="pxpCollapseFAQs4" class="accordion-collapse collapse"
                                 aria-labelledby="pxpFAQsHeader4" data-bs-parent="#pxpFAQsAccordion">
                                <div class="accordion-body">
                                    آگهی شغلی شما در عرض یک ساعت پس از ارسال به صورت زنده منتشر می شود و به محض
                                    دریافت رزومه از متقاضیان شروع به دریافت رزومه خواهید کرد.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="pxpFAQsHeader5">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#pxpCollapseFAQs5" aria-expanded="false"
                                        aria-controls="pxpCollapseFAQs5">
                                    چه مدت طول می کشد تا شغلم را پست کنم؟
                                </button>
                            </h2>
                            <div id="pxpCollapseFAQs5" class="accordion-collapse collapse"
                                 aria-labelledby="pxpFAQsHeader5" data-bs-parent="#pxpFAQsAccordion">
                                <div class="accordion-body">
                                    تکمیل فرم ساده استخدام ما فقط چند دقیقه طول می کشد. باید عنوان شغل، موقعیت
                                    مکانی، توضیحات و آدرس ایمیلی که می‌خواهید برنامه‌ها در آن ارسال شود را وارد
                                    کنید.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="pxpFAQsHeader6">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#pxpCollapseFAQs6" aria-expanded="false"
                                        aria-controls="pxpCollapseFAQs6">
                                    من مطمئن نیستم که چه چیزی را در آگهی شغلی خود قرار دهم. آیا می توانی کمک کنی؟
                                </button>
                            </h2>
                            <div id="pxpCollapseFAQs6" class="accordion-collapse collapse"
                                 aria-labelledby="pxpFAQsHeader6" data-bs-parent="#pxpFAQsAccordion">
                                <div class="accordion-body">
                                    حتما! راهنمای ما برای نوشتن شرح شغل عالی را بررسی کنید. و برای راهنمایی بیشتر،
                                    به برخی از نمونه شرح وظایف ما نگاهی بیندازید.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="pxpFAQsHeader7">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#pxpCollapseFAQs7" aria-expanded="false"
                                        aria-controls="pxpCollapseFAQs7">
                                    سیاست انصراف شما چیست؟
                                </button>
                            </h2>
                            <div id="pxpCollapseFAQs7" class="accordion-collapse collapse"
                                 aria-labelledby="pxpFAQsHeader7" data-bs-parent="#pxpFAQsAccordion">
                                <div class="accordion-body">
                                    شما می توانید هر زمان که بخواهید از طریق پیوند موجود در تنظیمات برگزیده حساب
                                    مشتری خود، طرح ارزش ماهانه خود را لغو کنید. اگر روزهای باقی مانده در چرخه 30
                                    روزه فعلی شما باقی مانده باشد، همچنان می توانید از طرح خود استفاده کنید.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
