<!doctype html>
<html lang="en" class="pxp-root">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">


    <link rel="shortcut icon" href="{{asset('assets/front/images/favicon3.svg')}}" type="image/x-icon">
    <link href="{{asset('assets/front/css/bootstrap-rtl.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/styleresponsive.css')}}">
    @yield('css')

    <title>فن ‌یار | @yield('title') </title>
</head>

<body>
<div class="pxp-preloader"><span>درحال بارگزاری...</span></div>


@include('front.header')

<div class="pb-3">
    @yield('main')
</div>



@include('front.footer')

<div class="modal fade pxp-user-modal" id="pxp-signin-modal" aria-hidden="true" aria-labelledby="signinModal"
     tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="pxp-user-modal-fig text-center">
                    <img src="{{asset('assets/front/images/signin-fig.png')}}" alt="Sign in">
                </div>
                <h5 class="modal-title text-center mt-4" id="signinModal">خوش آمدی!</h5>
                <form class="mt-4">
                    <div class="form-floating mb-3">
                        <input type="email" class="form-control" id="pxp-signin-email" placeholder="آدرس ایمیل">
                        <label for="pxp-signin-email">آدرس ایمیل</label>
                        <span class="fa fa-envelope-o"></span>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="password" class="form-control" id="pxp-signin-password" placeholder="رمز عبور">
                        <label for="pxp-signin-password">رمز عبور</label>
                        <span class="fa fa-lock"></span>
                    </div>
                    <a href="#" class="btn rounded-pill pxp-modal-cta">ادامه</a>
                    <div class="mt-4 text-center pxp-modal-small">
                        <a href="#" class="pxp-modal-link">فراموشی رمز عبور</a>
                    </div>
                    <div class="mt-4 text-center pxp-modal-small">
                        تازه وارد جابستر شده اید؟<a role="button" class="" data-bs-target="#pxp-signup-modal"
                                                    data-bs-toggle="modal" data-bs-dismiss="modal">حساب کاربری ایجاد کنید</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade pxp-user-modal" id="pxp-signup-modal" aria-hidden="true" aria-labelledby="signupModal"
     tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="pxp-user-modal-fig text-center">
                    <img src="{{asset('assets/front/images/signup-fig.png')}}" alt="Sign up">
                </div>
                <h5 class="modal-title text-center mt-4" id="signupModal">حساب کاربری ایجاد کنید</h5>
                <form class="mt-4">
                    <div class="form-floating mb-3">
                        <input type="email" class="form-control" id="pxp-signup-email" placeholder="آدرس ایمیل">
                        <label for="pxp-signup-email">آدرس ایمیل</label>
                        <span class="fa fa-envelope-o"></span>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="password" class="form-control" id="pxp-signup-password"
                               placeholder="ایجاد رمز عبور">
                        <label for="pxp-signup-password">ایجاد رمز عبور</label>
                        <span class="fa fa-lock"></span>
                    </div>
                    <a href="#" class="btn rounded-pill pxp-modal-cta">ادامه</a>
                    <div class="mt-4 text-center pxp-modal-small">
                        از قبل حساب کاربری دارید؟<a role="button" class="" data-bs-target="#pxp-signin-modal"
                                                    data-bs-toggle="modal">ورود</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('assets/front/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('assets/front/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/front/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/front/js/nav.js')}}"></script>
<script src="{{asset('assets/front/js/main.js')}}"></script>
<script src="{{asset('assets/swal2/swal2.all.min.js')}}"></script>
<script>
    function separate(Number)
    {
        Number+= '';
        Number= Number.replace(',', '');
        x = Number.split('.');
        y = x[0];
        z= x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(y))
            y= y.replace(rgx, '$1' + ',' + '$2');
        return y+ z;
    }
</script>
@yield('script')
</body>

</html>
