@extends('front.master')
@section('title','لیست ایده ها')
@section('css')
    <style>
        .range-slider {
            position: relative;
            width: 200px;
            height: 35px;
            text-align: center;
        }

        .range-slider input {
            pointer-events: none;
            position: absolute;
            overflow: hidden;
            left: 0;
            top: 15px;
            width: 200px;
            outline: none;
            height: 18px;
            margin: 0;
            padding: 0;
        }

        .range-slider input::-webkit-slider-thumb {
            pointer-events: all;
            position: relative;
            z-index: 1;
            outline: 0;
        }

        .range-slider input::-moz-range-thumb {
            pointer-events: all;
            position: relative;
            z-index: 10;
            -moz-appearance: none;
            width: 9px;
        }

        .range-slider input::-moz-range-track {
            position: relative;
            z-index: -1;
            background-color: rgba(0, 0, 0, 1);
            border: 0;
        }

        .range-slider input:last-of-type::-moz-range-track {
            -moz-appearance: none;
            background: none transparent;
            border: 0;
        }

        .range-slider input[type=range]::-moz-focus-outer {
            border: 0;
        }

        .rangeValue {
            width: 30px;
        }

        .output {
            position: absolute;
            /*border:1px solid #999;*/
            width: 40px;
            height: 30px;
            text-align: center;
            color: #999;
            border-radius: 4px;
            display: inline-block;
            font: bold 15px/30px Helvetica, Arial;
            bottom: 75%;
            left: 50%;
            transform: translate(-50%, 0);
        }

        .output.outputTwo {
            left: 100%;
        }

        .container {
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        input[type=range] {
            -webkit-appearance: none;
            background: none;
        }

        input[type=range]::-webkit-slider-runnable-track {
            height: 5px;
            border: none;
            border-radius: 3px;
            background: transparent;
        }

        input[type=range]::-ms-track {
            height: 5px;
            background: transparent;
            border: none;
            border-radius: 3px;
        }

        input[type=range]::-moz-range-track {
            height: 5px;
            background: transparent;
            border: none;
            border-radius: 3px;
        }

        input[type=range]::-webkit-slider-thumb {
            -webkit-appearance: none;
            border: none;
            height: 16px;
            width: 16px;
            border-radius: 50%;
            background: #555;
            margin-top: -5px;
            position: relative;
            z-index: 10000;
        }

        input[type=range]::-ms-thumb {
            -webkit-appearance: none;
            border: none;
            height: 16px;
            width: 16px;
            border-radius: 50%;
            background: #555;
            margin-top: -5px;
            position: relative;
            z-index: 10000;
        }

        input[type=range]::-moz-range-thumb {
            -webkit-appearance: none;
            border: none;
            height: 16px;
            width: 16px;
            border-radius: 50%;
            background: #0a53be;
            margin-top: -5px;
            position: relative;
            z-index: 10000;
        }

        input[type=range]:focus {
            outline: none;
        }

        .full-range,
        .incl-range {
            width: 100%;
            height: 5px;
            left: 0;
            top: 21px;
            position: absolute;
            background: #DDD;
        }

        .incl-range {
            background: #0dcaf0;
        }
    </style>
        @endsection
@section('main')
    <section class="pxp-page-header-simple" style="background-color: var(--pxpMainColorLight);">
        <div class="pxp-container">
            <h1 class="text-center">جستجوی ایده ها</h1>
            <div class="pxp-hero-subtitle pxp-text-light text-center">ایده مورد نظر خود را از میان
                <strong>{{count($ideas)}}</strong> ایده جستجو کنید
            </div>
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <div class="row">
                <div class="col-lg-5 col-xl-4 col-xxl-3">
                    <div class="pxp-jobs-list-side-filter">
                        <div class="pxp-list-side-filter-header d-flex d-lg-none">
                            <div class="pxp-list-side-filter-header-label">فیلتر </div>
                            <a role="button"><span class="fa fa-sliders"></span></a>
                        </div>
                        <div class="mt-4 mt-lg-0 d-lg-block pxp-list-side-filter-panel">
                            <h3>جستجو با کلمات کلیدی</h3>
                            <form action="{{route('ideas.list.search')}}" method="get">
                                @csrf
                            <div class="mt-2 mt-lg-3">
                                <div class="input-group">
                                    <span class="input-group-text"><span class="fa fa-search"></span></span>
                                    <input required name="key" type="text" class="form-control" placeholder="کلمه کلیدی یا عنوان ایده">
                                </div>
                                <div class="justify-content-center text-center my-2">
                                    <button class="btn btn-primary rounded-pill" type="submit">جستجو</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-xl-8 col-xxl-9">
                    <div class="pxp-jobs-list-top mt-4 mt-lg-0">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <h2><span class="pxp-text-light">نمایش</span> {{count($ideas)}} <span
                                        class="pxp-text-light">ایده</span></h2>
                            </div>
                            {{--<div class="col-auto">
                                <select class="form-select">
                                    <option value="0" selected> مرتبط ترین</option>
                                    <option value="1">جدیدترین</option>
                                    <option value="2">قدیمی ترین</option>
                                </select>
                            </div>--}}
                        </div>
                    </div>

                    <div>
                        @if(count($ideas)>0)
                            @foreach($ideas as $idea)
                        <div class="pxp-jobs-card-3 pxp-has-border">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-xxl-auto">
                                    <a href="#" class="pxp-jobs-card-3-company-logo"
                                       style="background-image: url(assets/front/images/company-logo-1.png);"></a>
                                </div>
                                <div class="col-sm-9 col-md-10 col-lg-9 col-xl-10 col-xxl-4">
                                    <a href="{{route('ideas.single',$idea->id)}}" class="pxp-jobs-card-3-title mt-3 mt-sm-0"> {{$idea->title}}</a>
                                    <div class="pxp-jobs-card-3-details">
                                        <div class="pxp-jobs-card-3-type"> {{Str::limit($idea->description,'30','...')}}</div>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-xl-6 col-xxl-4 mt-3 mt-xxl-0">
                                    <a href="#" class="pxp-jobs-card-3-category">
                                        <div class="pxp-jobs-card-3-category-label">{{$idea->category}}</div>
                                    </a>
                                    <div class="pxp-jobs-card-3-date-company">
                                        <span class="pxp-jobs-card-3-date pxp-text-light">{{verta($idea->created_at)->formatDifference()}} توسط</span>
                                        @if($idea->owner_type == 'company')
                                        <a href="{{route('company.single',$idea->owner_id)}}" class="pxp-jobs-card-3-company">{{$idea->owner_name}}</a>
                                        @elseif($idea->owner_type == 'student')
                                            <a href="{{route('candidate.single',$idea->owner_id)}}" class="pxp-jobs-card-3-company">{{$idea->owner_name}}</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xl-2 col-xxl-auto mt-3 mt-xxl-0 pxp-text-right">
                                    <a href="{{route('ideas.single',$idea->id)}}" class="btn rounded-pill pxp-card-btn">جزئیات</a>
                                </div>
                            </div>
                        </div>
                            @endforeach
                        @else
                            <div class="alert alert-warning text-center">{{__('public.no_info')}}</div>
                        @endif
                    </div>

                    <div class="row mt-4 mt-lg-5 justify-content-between align-items-center">
                        <div class="col-auto">
                            <nav class="mt-3 mt-sm-0" aria-label="Jobs list pagination">
                                {{$ideas->links('pagination.front')}}
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

