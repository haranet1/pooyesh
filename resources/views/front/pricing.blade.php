@extends('front.master')
@section('title','طرح های قیمت')
@section('main')
    <section class="mt-100 pxp-no-hero">
        <div class="pxp-container">
            <h2 class="pxp-section-h2">طرح های قیمت</h2>
            <p class="pxp-text-light">برنامه ای را انتخاب کنید که برای شما مناسب تر است</p>
            <div class="mt-4 mt-md-5">
                <div class="btn-group pxp-price-plans-switcher" role="group" aria-label="Price plans switcher">
                    <input type="radio" class="btn-check" name="pxp-price-plans-switcher" id="pxp-price-plan-monthly"
                           data-period="month" checked>
                    <label class="btn btn-outline-primary" for="pxp-price-plan-monthly">ماهانه</label>

                    <input type="radio" class="btn-check" name="pxp-price-plans-switcher" id="pxp-price-plan-nnnual"
                           data-period="year">
                    <label class="btn btn-outline-primary" for="pxp-price-plan-nnnual">سالانه</label>
                </div>
            </div>

            <div class="row mt-3 mt-md-4 pxp-animate-in pxp-animate-in-top">
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-plans-card-1-container">
                    <div class="pxp-plans-card-1">
                        <div class="pxp-plans-card-1-top">
                            <div class="pxp-plans-card-1-title">پایه</div>
                            <div class="pxp-plans-card-1-price">
                                <div class="pxp-plans-price-monthly">
                                    199 دلار<span class="pxp-period">/ماه</span>
                                </div>
                                <div class="pxp-plans-price-annual">
                                    189 دلار<span class="pxp-period">/سال</span>
                                </div>
                            </div>
                            <div class="pxp-plans-card-1-list">
                                <ul class="list-unstyled">
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt=""> 1 آگهی شغلی</li>
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt=""> 0 کار ویژه</li>
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt=""> شغل به مدت 20 روز نمایش داده می شود</li>
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt="">حق بیمه
                                        پشتیبانی 24/7 </li>
                                </ul>
                            </div>
                        </div>
                        <div class="pxp-plans-card-1-bottom">
                            <div class="pxp-plans-card-1-cta">
                                <a href="#" class="btn rounded-pill pxp-card-btn">برنامه را انتخاب کنید</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-plans-card-1-container">
                    <div class="pxp-plans-card-1">
                        <div class="pxp-plans-card-1-top">
                            <div class="pxp-plans-card-1-title">استاندارد</div>
                            <div class="pxp-plans-card-1-price">
                                <div class="pxp-plans-price-monthly">
                                    299 دلار<span class="pxp-period">/ماه</span>
                                </div>
                                <div class="pxp-plans-price-annual">
                                    289 دلار<span class="pxp-period">/سال</span>
                                </div>
                            </div>
                            <div class="pxp-plans-card-1-list">
                                <ul class="list-unstyled">
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt=""> 1 آگهی شغلی</li>
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt=""> 0 کار ویژه</li>
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt=""> شغل به مدت 20 روز نمایش داده می شود</li>
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt="">حق بیمه
                                        پشتیبانی 24/7 </li>
                                </ul>
                            </div>
                        </div>
                        <div class="pxp-plans-card-1-bottom">
                            <div class="pxp-plans-card-1-cta">
                                <a href="#" class="btn rounded-pill pxp-card-btn">برنامه را انتخاب کنید</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-plans-card-1-container">
                    <div class="pxp-plans-card-1 pxp-is-featured">
                        <div class="pxp-plans-card-1-top">
                            <div class="pxp-plans-card-1-featured-label">توصیه می شود</div>
                            <div class="pxp-plans-card-1-title">حرفه ای</div>
                            <div class="pxp-plans-card-1-price">
                                <div class="pxp-plans-price-monthly">
                                    499 دلار<span class="pxp-period">/ماه</span>
                                </div>
                                <div class="pxp-plans-price-annual">
                                    489 دلار<span class="pxp-period">/سال</span>
                                </div>
                            </div>
                            <div class="pxp-plans-card-1-list">
                                <ul class="list-unstyled">
                                    <li><img src="{{asset('assets/front/images/check-light.svg')}}" alt=""> 1 آگهی شغلی</li>
                                    <li><img src="{{asset('assets/front/images/check-light.svg')}}" alt=""> 0 کار ویژه</li>
                                    <li><img src="{{asset('assets/front/images/check-light.svg')}}" alt=""> شغل به مدت 20 روز نمایش داده می شود
                                    </li>
                                    <li><img src="{{asset('assets/front/images/check-light.svg')}}" alt="">حق بیمه
                                        پشتیبانی 24/7 </li>
                                </ul>
                            </div>
                        </div>
                        <div class="pxp-plans-card-1-bottom">
                            <div class="pxp-plans-card-1-cta">
                                <a href="#" class="btn rounded-pill pxp-card-btn">برنامه را انتخاب کنید</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-plans-card-1-container">
                    <div class="pxp-plans-card-1">
                        <div class="pxp-plans-card-1-top">
                            <div class="pxp-plans-card-1-title">تمدید شده</div>
                            <div class="pxp-plans-card-1-price">
                                <div class="pxp-plans-price-monthly">
                                    799 دلار<span class="pxp-period">/ماه</span>
                                </div>
                                <div class="pxp-plans-price-annual">
                                    799 دلار<span class="pxp-period">/سال</span>
                                </div>
                            </div>
                            <div class="pxp-plans-card-1-list">
                                <ul class="list-unstyled">
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt=""> 1 آگهی شغلی</li>
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt=""> 0 کار ویژه</li>
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt=""> شغل به مدت 20 روز نمایش داده می شود</li>
                                    <li><img src="{{asset('assets/front/images/check.svg')}}" alt="">حق بیمه
                                        پشتیبانی 24/7 </li>
                                </ul>
                            </div>
                        </div>
                        <div class="pxp-plans-card-1-bottom">
                            <div class="pxp-plans-card-1-cta">
                                <a href="#" class="btn rounded-pill pxp-card-btn">برنامه را انتخاب کنید</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
