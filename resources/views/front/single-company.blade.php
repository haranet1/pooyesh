@extends('front.master')
@section('title','مشخصات شرکت '.$company->groupable->name.'')
@section('main')
    <section>
        <div class="pxp-container">
            <div class="pxp-single-company-container pxp-has-columns">
                <div class="row">
                    <div class="col-lg-7 col-xl-8 col-xxl-9">
                        <div class="pxp-single-company-hero pxp-cover pxp-boxed"style="background-image: url({{asset($company->header)}});">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-single-company-hero-caption">
                                <div class="pxp-container">
                                    <div class="pxp-single-company-hero-content">
                                        <div class="pxp-single-company-hero-logo pxp-cover pxp-boxed"style="background-image: url({{asset($company->logo)}});background-size: cover;"></div>
                                        <div class="pxp-single-company-hero-title">
                                            <h1>{{$company->groupable->name}}</h1>
                                            <div class="pxp-single-company-hero-location"><span
                                                    class="fa fa-globe"></span>ایران، {{$company->groupable->city}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @auth
                            @if (!$company->IsConfirmed())
                                <div class="row">
                                    <div class="alert alert-danger mt-3" style="border-radius: 25px;">
                                        این شرکت هنوز تایید نشده است.
                                    </div>
                                </div>
                            @elseif (!$company->job_positions->count() > 0 || $jobIsConfirmed == false)
                                @if (Auth::user()->IsConfirmed())
                                    <div class="pxp-single-company-side-panel row mt-3 mt-lg-4 justify-content-center mx-0">
                                        <div class="col-4">
                                            <input type="hidden" class="form-control" id="receiver_id" value="{{$company->id}}">
                                            <input type="hidden" class="form-control" id="sender_id" value="{{Auth::user()->id}}">
                                            <a href="javascript:void(0)" id="resume" class="btn rounded-pill pxp-section-cta d-block">ارسال درخواست اشتغال</a>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @else
                            <div class="pxp-single-company-side-panel row mt-3 mt-lg-4 justify-content-center mx-0">
                                <div class="col-4">
                                    <a href="{{route('login')}}" class="btn rounded-pill pxp-section-cta d-block">برای ارسال درخواست وارد شوید</a>
                                </div>
                            </div>
                        @endauth

                        <section class="mt-4 mt-lg-5">
                            <div class="pxp-single-company-content">
                                
                                
                                <h2>درباره {{$company->groupable->name}}</h2>
                                <p>
                                    @php
                                        echo $company->groupable->about    
                                    @endphp
                                </p>
                                
                                @if (Auth::check())
                                    @if($company->hire_positions)
                                        <section class="" style="margin-top: 2rem;">
                                            <div class="pxp-container">
                                                <h2 class="pxp-subsection-h2"> موقعیت های استخدامی</h2>
                                                <p class="pxp-text-light">لیست موقعیت های استخدامی ارسال شده توسط {{$company->groupable->name}}</p>
                                                <div class="row mt-3 mt-md-4 pxp-animate-in pxp-animate-in-top pxp-in">
                                                    @foreach($company->hire_positions as $hire_job)
                                                        <div class="col-xl-4 pxp-jobs-card-2-container">
                                                            <div class="pxp-jobs-card-2 pxp-has-border">
                                                                <div class="pxp-jobs-card-2-top">
                                                                    <a href="{{route('job',$hire_job->id)}}" class="pxp-jobs-card-2-company-logo"
                                                                    style="width: 60px !important; height: 60px !important;background-image: url({{asset('assets/front/images/company-logo-2.png')}})"></a>
                                                                    <div class="pxp-jobs-card-2-info">
                                                                        <a href="{{route('job',$hire_job->id)}}"
                                                                        class="pxp-jobs-card-2-title">{{$hire_job->title}} </a>
                                                                        <div class="row">
                                                                            <a href="{{route('job',$hire_job->id)}}"
                                                                            class="pxp-jobs-card-2-location mt-2">
                                                                                <span class="fa fa-globe"></span>{{job_province_and_city_name($hire_job->province,$hire_job->city)}}
                                                                            </a>
                                                                            <div class="pxp-jobs-card-1-type mt-2">{{job_type_persian($hire_job->type)}}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="pxp-jobs-card-2-bottom">
                                                                    <a href="{{route('jobs.list.by-category',$hire_job->category->name)}}"
                                                                    class="pxp-jobs-card-2-category">
                                                                        <div class="pxp-jobs-card-2-category-label">{{$hire_job->category->name}}</div>
                                                                    </a>
                                                                    <div class="pxp-jobs-card-2-bottom-right">
                                                                        <span class="pxp-jobs-card-2-date pxp-text-light">{{verta($hire_job->created_at)->formatDifference()}} توسط</span>
                                                                        <a
                                                                            href="{{route('company.single',$company->id)}}"
                                                                            class="pxp-jobs-card-2-company">{{$company->groupable->name}}</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>

                                            </div>
                                        </section>
                                    @endif

                                    @if($company->intern_positions)
                                        <section class="" style="margin-top: 2rem;">
                                            <div class="pxp-container">
                                                <h2 class="pxp-subsection-h2"> موقعیت های کارآموزی</h2>
                                                <p class="pxp-text-light">لیست موقعیت های کارآموزی ارسال شده توسط {{$company->groupable->name}}</p>

                                                <div class="row mt-3 mt-md-4 pxp-animate-in pxp-animate-in-top pxp-in">
                                                    @foreach($company->intern_positions as $intern_job)
                                                        <div class="col-xl-4 pxp-jobs-card-2-container">
                                                            <div class="pxp-jobs-card-2 pxp-has-border">
                                                                <div class="pxp-jobs-card-2-top">
                                                                    <a href="{{route('job',$intern_job->id)}}" class="pxp-jobs-card-2-company-logo"
                                                                    style="width: 60px !important; height: 60px !important; background-image: url({{asset('assets/front/images/company-logo-4.png')}});"></a>
                                                                    <div class="pxp-jobs-card-2-info">
                                                                        <a href="{{route('job',$intern_job->id)}}"
                                                                        class="pxp-jobs-card-2-title">{{$intern_job->title}} </a>
                                                                        <div class="row">
                                                                            <a href="{{route('job',$intern_job->id)}}"
                                                                            class="pxp-jobs-card-2-location mt-2">
                                                                                <span class="fa fa-globe"></span>{{job_province_and_city_name($intern_job->province,$intern_job->city)}}
                                                                            </a>
                                                                            <div class="pxp-jobs-card-1-type mt-2">{{job_type_persian($intern_job->type)}}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="pxp-jobs-card-2-bottom">
                                                                    <a href="{{route('jobs.list.by-category',$intern_job->category->name)}}"
                                                                    class="pxp-jobs-card-2-category">
                                                                        <div class="pxp-jobs-card-2-category-label">{{$intern_job->category->name}}</div>
                                                                    </a>
                                                                    <div class="pxp-jobs-card-2-bottom-right">
                                                                        <span class="pxp-jobs-card-2-date pxp-text-light">{{verta($intern_job->created_at)->formatDifference()}} توسط</span>
                                                                        <a
                                                                            href="{{route('company.single',$company->id)}}"
                                                                            class="pxp-jobs-card-2-company">{{$company->groupable->name}}</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>

                                            </div>
                                        </section>
                                    @endif
                                @endif
                                
                                {{-- <div class="pxp-single-company-gallery mt-3 mt-lg-4">
                                     <div class="row">
                                         <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-3">
                                             <a href="assets/front/images/company-hero-1.jpg" class="pxp-single-company-gallery-item">
                                                 <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                      style="background-image: url(assets/front/images/company-hero-1.jpg);"></div>
                                                 <div class="pxp-single-company-gallery-item-caption">کپشن تصویر 1</div>
                                             </a>
                                             <a href="assets/front/images/company-hero-3.jpg" class="pxp-single-company-gallery-item">
                                                 <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                      style="background-image: url(assets/front/images/company-hero-3.jpg);"></div>
                                                 <div class="pxp-single-company-gallery-item-caption">کپشن تصویر 2</div>
                                             </a>
                                         </div>
                                         <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-3">
                                             <a href="assets/front/images/company-hero-2.jpg"
                                                class="pxp-single-company-gallery-item pxp-is-tall">
                                                 <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                      style="background-image: url(assets/front/images/company-hero-2.jpg);"></div>
                                                 <div class="pxp-single-company-gallery-item-caption">کپشن تصویر 3</div>
                                             </a>
                                         </div>
                                         <div class="col-xl-12 col-xxl-6">
                                             <a href="assets/front/images/company-hero-4.jpg" class="pxp-single-company-gallery-item">
                                                 <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                      style="background-image: url(assets/front/images/company-hero-4.jpg);"></div>
                                                 <div class="pxp-single-company-gallery-item-caption">کپشن تصویر 4</div>
                                             </a>
                                             <div class="row">
                                                 <div class="col-6">
                                                     <a href="assets/front/images/company-hero-5.jpg"
                                                        class="pxp-single-company-gallery-item">
                                                         <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                              style="background-image: url(assets/front/images/company-hero-5.jpg);">
                                                         </div>
                                                         <div class="pxp-single-company-gallery-item-caption">کپشن تصویر
                                                             5</div>
                                                     </a>
                                                 </div>
                                                 <div class="col-6">
                                                     <a href="assets/front/images/company-hero-3.jpg"
                                                        class="pxp-single-company-gallery-item">
                                                         <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                              style="background-image: url(assets/front/images/company-hero-3.jpg);">
                                                         </div>
                                                         <div class="pxp-single-company-gallery-item-caption">کپشن تصویر
                                                             6</div>
                                                     </a>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>--}}


                                {{--<div class="mt-4 mt-lg-5">
                                    <div class="row justify-content-between">
                                        <div class="col-md-6">
                                            <iframe src=""
                                                    title="YouTube video player"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen></iframe>
                                        </div>
                                        <div class="col-md-5 mt-3 mt-md-0">
                                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده
                                                از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و
                                                سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای
                                                متنوع با </p>
                                        </div>
                                    </div>
                                </div>--}}
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-5 col-xl-4 col-xxl-3">
                        <div class="pxp-single-company-side-panel mt-5 mt-lg-0">
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">حوزه کاری</div>
                                <div
                                    class="pxp-single-company-side-info-data">{{$company->groupable->activity_field}}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">اندازه شرکت</div>
                                <div class="pxp-single-company-side-info-data">{{$company->groupable->size}}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">تاسیس شده در</div>
                                <div class="pxp-single-company-side-info-data">{{$company->groupable->year}}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">تلفن</div>
                                <div class="pxp-single-company-side-info-data"
                                     dir="ltr">{{$company->groupable->phone}}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">ایمیل</div>
                                <div class="pxp-single-company-side-info-data">{{$company->groupable->email}}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">مکان</div>
                                <div class="pxp-single-company-side-info-data">
                                    ایران، {{$company->groupable->city}}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">کد qr</div>
                                <div class="pxp-single-company-side-info-data">
                                    {!! QrCode::size(100)->generate(route('company.single', $company->id)) !!}
                                </div>
                            </div>
                            {{--<div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">وب سایت</div>
                                <div class="pxp-single-company-side-info-data"><a href="#">www.craftgenics.com</a></div>
                            </div>--}}
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-data">
                                    <ul class="list-unstyled pxp-single-company-side-info-social">
                                        <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                        <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                        <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                                        <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        {{--<div class="pxp-single-company-side-panel mt-3 mt-lg-4">
                            <h3>ارسال درخواست همکاری</h3>

                            <div class="mb-3">
                                <label for="type" class="form-label">نوع همکاری</label>
                                <select name="type" class="form-control form-select"
                                        style="background-position: left 20px center!important;" id="type">
                                    <option value="intern">کارآموزی</option>
                                    <option value="hire">استخدام</option>
                                </select>
                            </div>
                            @auth
                                @if(is_null($user_request))
                                    <a href="javascript:void(0)" id="work-request"
                                       class="btn rounded-pill pxp-section-cta d-block">ارسال درخواست</a>
                                @else
                                    <a href="#" disabled="disabled"
                                       class="btn rounded-pill pxp-section-cta d-block disabled">شما قبلا برای این شرکت درخواست فرستاده اید</a>

                                @endif
                            @else
                                <a href="#" disabled="disabled"
                                   class="btn rounded-pill pxp-section-cta d-block disabled">ابتدا باید وارد شوید</a>
                            @endif

                        </div>--}}
                        <div class="pxp-single-company-side-panel mt-3 mt-lg-4">
                            <h3>تماس با شرکت</h3>
                                {{--<form class="mt-4">--}}
                                <input type="hidden" value="{{$company->id}}" id="owner">
                                <div class="mb-3">
                                    <label for="contact-company-name" class="form-label">موضوع</label>
                                    <input required type="text" class="form-control" id="contact-company-subject"
                                           placeholder="موضوع  را اینجا بنویسید">
                                </div>
                                <div class="mb-3">
                                    <label for="contact-company-email" class="form-label">ایمیل</label>
                                    <input required type="text" class="form-control" id="contact-company-email"
                                           placeholder="ایمیل خود را اینجا بنویسید">
                                </div>
                                <div class="mb-3">
                                    <label for="contact-company-message" class="form-label">پیام</label>
                                    <textarea required class="form-control" id="contact-company-message"
                                              placeholder="پیام خود را اینجا بنویسید..."></textarea>
                                </div>
                                @guest
                                    <a href="{{route('login')}}" class="btn rounded-pill pxp-section-cta d-block">برای ارسال پیام باید وارد شوید</a>
                                @else
                                    <a href="javascript:void(0)" id="send-ticket" class="btn rounded-pill pxp-section-cta d-block">ارسال پیام</a>
                                @endauth
                                {{--</form>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
@endsection
@section('script')
    <script>
        $('#send-ticket').on('click',function (){
            let subjectElement = document.getElementById('contact-company-subject');
            let emailElement = document.getElementById('contact-company-email');
            let messageElement = document.getElementById('contact-company-message');
            let subject = $('#contact-company-subject').val()
            let email=$('#contact-company-email').val()
            let message =$('#contact-company-message').val()
            let owner =$('#owner').val()
            console.log(subjectElement.value);
            if (subject != "" && email != "" && message != ""){
                $.ajax({
                    headers:{
                        'X-CSRF-TOKEN':'{{csrf_token()}}'
                    },
                    url:"{{route('front.ajax.ticket.store-company-ticket')}}",
                    type:'post',
                    data:{
                        subject:subject,
                        owner:owner,
                        email:email,
                        message:message,
                    },
                    success:function (response){
                        console.log(subjectElement);
                        subjectElement.value = ""
                        emailElement.value = ""
                        messageElement.value = ""
                        $('#send-ticket').addClass('disabled')
                        swal.fire('پیام شما ارسال شد.')
                    },
                    error:function (response){
                        console.log(response)
                    }
                })
            }
        })
    </script>

    <script>
        let resumeElement = document.getElementById('resume');
        resumeElement.addEventListener('click', function(){
            let receiverId = document.getElementById('receiver_id').value;
            let senderId = document.getElementById('sender_id').value;

            $.ajax({
                    headers:{
                        'X-CSRF-TOKEN':'{{csrf_token()}}'
                    },
                    url:"{{route('front.ajax.send.resume')}}",
                    type:'post',
                    data:{
                        receiver : receiverId,
                        sender : senderId
                    },
                    success:function (response){
                        console.log(response);
                        if(response['status'] == true){
                            $('#resume').addClass('disabled')
                            swal.fire('درخواست اشتغال شما ارسال شد.')
                        }else{
                            $('#resume').addClass('disabled')
                            swal.fire('درخواست اشتغال شما قبلا به این شرکت ارسال شده است.')
                        }
                        
                    },
                    error:function (response){
                        console.log(response)
                    }
                })
        });
    </script>
@endsection
