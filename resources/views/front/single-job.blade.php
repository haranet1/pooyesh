@extends('front.master')
@section('title','مشخصات موقعیت شغلی')
@section('main')
    <section>
        <div class="pxp-container">
            <div class="pxp-single-job-content pxp-has-columns">
                @if ($hasPermission)
                    @auth
                        <div class="row">
                            <div class="col-lg-7 col-xl-8 col-xxl-9">
                                <div class="row justify-content-between align-items-center mt-4 mt-lg-5">
                                    <div class="col-12 mt-4 mt-lg-5">
                                        <div class="border p-3" style="border-radius: 1rem !important;">
                                            <div class="col-xl-8 col-xxl-8">
                                                <h1>{{$job->title}}</h1>
                                                <div class="pxp-single-job-company-location">
                                                    توسط <a href="{{route('company.single',$job->company->id)}}"
                                                            class="pxp-single-job-company">{{$job->company->groupable->name}}</a> در
                                                    @if ($job->province && $job->city)
                                                        <span class="pxp-single-job-location">{{$job->province->name}} - {{$job->city->name}} - {{$job->location}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-xxl-5">
                                                <div class="pxp-single-job-options mt-4 col-xl-0">
                                                    <button class="btn pxp-single-job-save-btn"><span
                                                            class="fa fa-heart-o"></span></button>
                                                    <div class="dropdown ms-2">
                                                        <button class="btn pxp-single-job-share-btn dropdown-toggle" type="button"
                                                                id="socialShareBtn" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <span class="fa fa-share-alt">
                                                            </span></button>
                                                        <ul class="dropdown-menu dropdown-menu-end pxp-single-job-share-dropdown"
                                                            aria-labelledby="socialShareBtn">
                                                            <li><a class="dropdown-item" href="#"><span class="fa fa-facebook"></span>
                                                                    فیس بوک</a></li>
                                                            <li><a class="dropdown-item" href="#"><span class="fa fa-twitter"></span>
                                                                    توییتر</a></li>
                                                            <li><a class="dropdown-item" href="#"><span class="fa fa-pinterest"></span>
                                                                    پینترست</a></li>
                                                            <li><a class="dropdown-item" href="#"><span class="fa fa-linkedin"></span>
                                                                    لینکدین</a></li>
                                                        </ul>
                                                    </div>
                                                    @auth
                                                        @if ($hasCooperationRequest)
                                                            <a href="javascript:void(0)" id="work-request"
                                                            class="btn ms-2 btn-dark rounded-pill disabled">درخواست ثبت شده است</a>
                                                        @else
                                                            @if (!Auth::user()->IsConfirmed())
                                                                <a href="javascript:void(0)" id="work-request"
                                                                class="btn ms-2 btn-warning rounded-pill disabled">حساب کاربری شما تایید نشده است</a>
                                                            @else
                                                                <a href="javascript:void(0)" id="work-request"
                                                                class="btn ms-2 btn-primary rounded-pill">ارسال درخواست</a>
                                                            @endif
                                                        @endif
                                                    @else
                                                        <a href="{{route('login')}}"
                                                        class="btn ms-2 pxp-single-job-apply-btn rounded-pill">برای ارسال درخواست
                                                            وارد شوید</a>    
                                                    @endauth
                                                    {{-- @auth
                                                        @if (Auth::user()->IsConfirmed())
                                                            @if($job->type === 'intern' && Auth::user()->hasRole('student'))
                                                                @if (Auth::user()->groupable)
                                                                    <a href="javascript:void(0)" id="work-request"
                                                                    class="btn ms-2 btn-primary rounded-pill">ارسال درخواست</a>
                                                                @else
                                                                    <a href="{{route('set.std.info')}}"
                                                                    class="btn ms-2 btn-primary rounded-pill">ارسال درخواست</a>
                                                                @endif
                                                            
                                                            @elseif($job->type != 'intern')
                                                                @if(!Auth::user()->groupable && Auth::user()->hasRole('student'))
                                                                    <a href="{{route('set.std.info')}}"
                                                                    class="btn ms-2 btn-primary rounded-pill">ارسال درخواست</a>
                                                                @elseif(!Auth::user()->groupable && Auth::user()->hasRole('applicant'))
                                                                    <a href="{{route('set.applicant.info')}}"
                                                                    class="btn ms-2 btn-primary rounded-pill">ارسال درخواست</a>
                                                                @else
                                                                    <a href="javascript:void(0)" id="work-request"
                                                                    class="btn ms-2 btn-primary rounded-pill">ارسال درخواست</a>
                                                                @endif
                                                            @endif
                                                        @else
                                                            <a href="javascript:void(0)" id="work-request"
                                                            class="btn ms-2 btn-warning rounded-pill disabled">در انتظار تایید مدیر</a>
                                                        @endif
                                                    @else
                                                        <a href="{{route('login')}}"
                                                        class="btn ms-2 pxp-single-job-apply-btn rounded-pill">برای ارسال درخواست
                                                            وارد شوید</a>
                                                    @endauth --}}
                                                </div>
                                            </div>
                                            <div class="row mt-4 justify-content-between align-items-center">
                                                {{-- category part --}}
                                                <div class="col-6">
                                                    <a href="{{route('jobs.list.by-category',$job->category->name)}}"
                                                    class="pxp-single-job-category">
                                                        <div class="pxp-single-job-category-icon"><span class="fa fa-calendar-o"></span>
                                                        </div>
                                                        <div class="pxp-single-job-category-label">{{$job->category->name}}</div>
                                                    </a>
                                                </div>
                                                {{-- created at part --}}
                                                <div class="col-auto">
                                                    <div
                                                        class="pxp-single-job-date pxp-text-light">{{verta($job->created_at)->formatDifference()}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- skills and lang and desc--}}
                                <div class="pxp-single-job-content-details mt-4 mt-lg-5">
                                    @if (count($job->skills) > 0)
                                        <div class="row">
                                            <div class="col-12 mt-4 mt-lg-5">
                                                <div class="border p-3" style="border-radius: 1rem !important;">
                                                    <h2 class="mb-3">مهارت ها</h2>
                                                    <div class="pxp-single-candidate-skills">
                                                        <ul class="list-unstyled">
                                                            @foreach ($job->skills as $skill)
                                                                <li><span>{{ $skill->name }}</span>
                                                                    {{ level_badge($skill->pivot->skill_level)}}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if (count($job->languages) > 0)
                                        <div class="row">
                                            <div class="col-12 mt-4 mt-lg-5">
                                                <div class="border p-3" style="border-radius: 1rem !important;">
                                                    <h2 class="mb-3">زبان های خارجی</h2>
                                                    <div class="pxp-single-candidate-skills">
                                                        <ul class="list-unstyled">
                                                            @foreach ($job->languages as $language)
                                                                <li><span>{{ $language->name }}</span>
                                                                    {{ level_badge($language->pivot->level) }}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-12 mt4 mt-lg-5">
                                            <div class="border p-3" style="border-radius: 1rem !important;">
                                                <div class="mt-4">
                                                    <h2 class="mb-3">توضیحات </h2>
                                                    <p><?php echo $job->description ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    {{-- <div class="mt-4">
                                        <h4>الزامات</h4>
                                        <ul>
                                            <li>بیش از 4 سال تجربه مدیریت سیستم با پلتفرم سرور مایکروسافت (1395/1399،
                                                Microsoft IIS، Active Directory)</li>
                                            <li>بیش از 3 سال تجربه مدیریت عملی سیستم با AWS (EC2، Elastic Load
                                                Balancing، Multi AZ، و غیره)</li>
                                            <li>بیش از 4 سال تجربه کار با SQL Server, MySQL</li>
                                            <li>دانش کار تکنیک ها و پروتکل های رمزگذاری، احراز هویت چند عاملی، حفاظت از
                                                داده ها، تست نفوذ، تهدیدات امنیتی</li>
                                            <li>مدرک لیسانس یا بیش از 4 سال تجربه عملی فناوری اطلاعات</li>
                                        </ul>
                                    </div>--}}

                                </div>
                                {{--  --}}
                                
                            </div>
                            <div class="col-lg-5 col-xl-4 col-xxl-3">
                                <div class="pxp-single-job-side-panel mt-5 mt-lg-0">
                                    {{-- experince --}}
                                    <div>
                                        <div class="pxp-single-job-side-info-label pxp-text-light">سابقه مورد نیاز</div>
                                        <div class="pxp-single-job-side-info-data">{{$job->experience}} سال</div>
                                    </div>
                                    {{-- level --}}
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">سطح کاری</div>
                                        <div class="pxp-single-job-side-info-data">{{job_level_persian($job->level)}}</div>
                                    </div>
                                    {{-- type --}}
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">نوع همکاری</div>
                                        <div class="pxp-single-job-side-info-data">{{job_type_persian($job->type)}}</div>
                                    </div>
                                    {{-- salary --}}
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">حقوق</div>
                                        @if($job->salary)
                                            <div class="pxp-single-job-side-info-data">{{number_format($job->salary)}} /
                                                ماهانه
                                            </div>
                                        @endif
                                    </div>
                                    {{-- sex --}}
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">جنسیت</div>
                                        <div class="pxp-single-job-side-info-data">{{$job->sex == 0 ? "فرقی ندارد" : $job->sex}}</div>
                                    </div>
                                    {{-- age --}}
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">رنج سنی</div>
                                        <div class="pxp-single-job-side-info-data">
                                        {{$job->age == 0 ? "فرقی ندارد" : ''}}
                                        {{$job->age == 20 ? "تا 20 سال" : ''}}
                                        {{$job->age == 30 ? "21 تا 30 سال" : ''}}
                                        {{$job->age == 40 ? "31 تا 40 سال" : ''}}
                                        </div>
                                    </div>
                                    {{-- academic level --}}
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">مقطع تحصیلی</div>
                                        <div class="pxp-single-job-side-info-data">
                                        {{$job->academic_level == 0 ? "فرقی ندارد" : $job->academic_level}}
                                        </div>
                                    </div>
                                    {{-- marige --}}
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">وضعیت تاهل</div>
                                        <div class="pxp-single-job-side-info-data">
                                        {{$job->marige_type == 0 ? "فرقی ندارد" : $job->marige_type}}
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3 mt-lg-4 pxp-single-job-side-panel">
                                    <div class="pxp-single-job-side-company">
                                        <div class="pxp-single-job-side-company-logo pxp-cover"
                                            style="background-image: url(assets/front/images/company-logo-2.png);"></div>
                                        <div class="pxp-single-job-side-company-profile">
                                            <div
                                                class="pxp-single-job-side-company-name">{{$job->company->groupable->name}}</div>
                                            <a href="{{route('company.single',$job->company->id)}}">مشاهده پروفایل</a>
                                        </div>
                                    </div>
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">حوزه کاری</div>
                                        <div
                                            class="pxp-single-job-side-info-data">{{$job->company->groupable->activity_field}}</div>
                                    </div>
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">تعداد کارمندان</div>
                                        <div class="pxp-single-job-side-info-data">{{$job->company->groupable->size}}</div>
                                    </div>
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">تاسیس شده در</div>
                                        <div class="pxp-single-job-side-info-data">{{$job->company->groupable->year}}</div>
                                    </div>
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">تلفن</div>
                                        <div class="pxp-single-job-side-info-data"
                                            dir="ltr">{{$job->company->groupable->phone}}</div>
                                    </div>
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">ایمیل</div>
                                        <div class="pxp-single-job-side-info-data">{{$job->company->groupable->email}}</div>
                                    </div>
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">مکان</div>
                                        <div class="pxp-single-job-side-info-data">{{$job->company->groupable->city}}</div>
                                    </div>
                                    <div class="mt-4">
                                        <div class="pxp-single-job-side-info-label pxp-text-light">وب سایت</div>
                                        <div class="pxp-single-job-side-info-data"><a
                                                href="#">{{$job->company->groupable->website}}</a></div>
                                    </div>
                                    {{-- <div class="mt-4">
                                        <div class="pxp-single-job-side-info-data">
                                            <ul class="list-unstyled pxp-single-job-side-info-social">
                                                <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                                <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                                                <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row align-items-center mt-4 mt-lg-5 d-flex justify-content-end">
                            <div class="col-5 mt-4 mt-lg-5">
                                <img style="width: 100%; border: 2px solid lightslategray; border-radius: 1rem;" src="{{asset('assets/front/images/SVG/office2.svg')}}" alt="office">
                            </div>
                            <div class="col-7 mt-4 mt-lg-5 ">
                                <div class="border p-3" style="border-radius: 1rem !important;">
                                    <div class="col-xl-8 col-xxl-8">
                                        <h1>{{$job->title}}</h1>
                                        <div class="pxp-single-job-company-location">
                                            توسط <a href="{{route('company.single',$job->company->id)}}"
                                                    class="pxp-single-job-company">{{$job->company->groupable->name}}</a> در
                                            @if ($job->province && $job->city)
                                                <span class="pxp-single-job-location">{{$job->province->name}} - {{$job->city->name}} - {{$job->location}}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xxl-6">
                                        <div class="pxp-single-job-options mt-4 col-xl-0">
                                            <button class="btn pxp-single-job-save-btn"><span
                                                    class="fa fa-heart-o"></span></button>
                                            <div class="dropdown ms-2">
                                                <button class="btn pxp-single-job-share-btn dropdown-toggle" type="button"
                                                        id="socialShareBtn" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <span class="fa fa-share-alt">
                                                    </span></button>
                                                <ul class="dropdown-menu dropdown-menu-end pxp-single-job-share-dropdown"
                                                    aria-labelledby="socialShareBtn">
                                                    <li><a class="dropdown-item" href="#"><span class="fa fa-facebook"></span>
                                                            فیس بوک</a></li>
                                                    <li><a class="dropdown-item" href="#"><span class="fa fa-twitter"></span>
                                                            توییتر</a></li>
                                                    <li><a class="dropdown-item" href="#"><span class="fa fa-pinterest"></span>
                                                            پینترست</a></li>
                                                    <li><a class="dropdown-item" href="#"><span class="fa fa-linkedin"></span>
                                                            لینکدین</a></li>
                                                </ul>
                                            </div>
                                            @auth
                                                @if($job->type === 'intern' && \Illuminate\Support\Facades\Auth::user()->hasRole('student'))
                                                    <a href="javascript:void(0)" id="work-request"
                                                    class="btn ms-2 btn-primary rounded-pill">ارسال درخواست</a>
                                                @elseif($job->type != 'intern')
                                                    <a href="javascript:void(0)" id="work-request"
                                                    class="btn ms-2 btn-primary rounded-pill">ارسال درخواست</a>
                                                @endif

                                            @else
                                                <a href="{{route('login')}}"
                                                class="btn ms-2 pxp-single-job-apply-btn rounded-pill">برای ارسال درخواست
                                                    وارد شوید</a>
                                            @endauth
                                        </div>
                                    </div>
                                    <div class="row mt-4 justify-content-between align-items-center">
                                        {{-- category part --}}
                                        <div class="col-6">
                                            <a href="{{route('jobs.list')}}"
                                            class="pxp-single-job-category">
                                                <div class="pxp-single-job-category-icon"><span class="fa fa-calendar-o"></span>
                                                </div>
                                                <div class="pxp-single-job-category-label">{{$job->category->name}}</div>
                                            </a>
                                        </div>
                                        {{-- created at part --}}
                                        <div class="col-auto">
                                            <div
                                                class="pxp-single-job-date pxp-text-light">{{verta($job->created_at)->formatDifference()}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-center">
                            <div class="col-6 mt-4 mt-lg-5">
                                <div class="row border p-3 d-flex justify-content-center bg-primary " style="border-radius: 1rem !important;">
                                    <h4 class="mb-3 text-white text-center">برای مشاهده اطلاعات بیشتر باید وارد شوید</h4>
                                    
                                    <div class="row d-flex justify-content-center mt-lg-3">
                                        <div class="col-4">
                                            <a class="btn btn-light  w-100" href="{{route('login')}}">ورود</a>
                                        </div>
                                        <div class="col-4">
                                            <a class="btn btn-light w-100" href="{{route('register')}}">ثبت نام</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endauth
                @else
                    @if (!Auth::check())
                        <div class="row">
                            <div class="alert alert-danger mt-3" style="border-radius: 25px;">
                                برای مشاهده اطلاعات موقعیت شغلی باید با حساب کاربری خود وارد شوید. 
                            </div>
                        </div>
                    @endif
                    @if (!$job->IsConfirmed())
                        <div class="row">
                            <div class="alert alert-danger mt-3" style="border-radius: 25px;">
                                این موقعیت شغلی هنوز تایید نشده است.
                            </div>
                        </div>
                    @endif
                @endif
                

            </div>
        </div>
    </section>

    @if(count($similar_jobs) > 0)
        <section class="mt-100">
            <div class="pxp-container">
                <h2 class="pxp-subsection-h2">مشاغل مرتبط</h2>
                <p class="pxp-text-light">مشاغل مشابه دیگری که ممکن است برای شما جالب باشد</p>

                <div class="row mt-3 mt-md-4 pxp-animate-in pxp-animate-in-top pxp-in">
                    @foreach($similar_jobs as $similar)
                        <div class="col-xl-6 pxp-jobs-card-2-container">
                            <div class="pxp-jobs-card-2 pxp-has-border">
                                <div class="pxp-jobs-card-2-top">
                                    <a href="{{route('company.single',$similar->company->id)}}"
                                       class="pxp-jobs-card-2-company-logo"
                                       style="background-image: url(assets/front/images/company-logo-2.png);"></a>
                                    <div class="pxp-jobs-card-2-info">
                                        <a href="{{route('job',$similar->id)}}"
                                           class="pxp-jobs-card-2-title">{{$similar->title}}</a>
                                        <div class="pxp-jobs-card-2-details">
                                            <span class="fa fa-globe mx-1"></span> ایران، {{$similar->location}}
                                            <a href="{{route('jobs.list.by-type',$similar->type)}}"
                                               class="pxp-jobs-card-2-location">
                                                <div class="pxp-jobs-card-2-type">{{job_type_persian($job->type)}}</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="pxp-jobs-card-2-bottom">
                                    <a href="{{route('jobs.list.by-category',$similar->category->name)}}"
                                       class="pxp-jobs-card-2-category">
                                        <div class="pxp-jobs-card-2-category-label">{{$similar->category->name}} </div>
                                    </a>
                                    <div class="pxp-jobs-card-2-bottom-right">
                                        <span class="pxp-jobs-card-2-date pxp-text-light">{{verta($similar->created_at)->formatDifference()}} توسط</span>
                                        <a
                                            href="{{route('company.single',$similar->company->id)}}"
                                            class="pxp-jobs-card-2-company">{{$similar->company->groupable->name}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@endsection

@section('script')
    <script>
        $('#work-request').on('click', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            })
            $.ajax({
                type: 'POST',
                url: "{{route('front.ajax.cooperation-request')}}",
                data: {
                    job_id: {{$job->id}},
                },
                success: function (data) {
                    Swal.fire({
                        icon: 'success',
                        title: 'درخواست شما با موفقیت ارسال شد',
                        showConfirmButton: false,
                        timer: 3500
                    })
                    $('#work-request').addClass('disabled')
                },
                error: function (data) {
                    console.log(data.error)
                }
            })
        })
    </script>
@endsection
