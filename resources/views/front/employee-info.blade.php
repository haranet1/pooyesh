<!doctype html>
<html lang="fa-IR" dir="rtl">

<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="پویش">
    <meta name="author" content="haranet.ir">

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/panel/images/brand/favicon.ico')}}" />

    <title>پویش | تکمیل اطلاعات</title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/panel/css/dark-style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/panel/css/transparent-style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/panel/css/skin-modes.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/icons.css')}}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{asset('assets/panel/colors/color1.css')}}" />
    <link href="{{asset('assets/panel/switcher/css/switcher.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/panel/switcher/demo.css')}}" rel="stylesheet" />
    <link rel="stylesheet" id="fonts" href="{{asset('assets/panel/fonts/styles-fa-num/iran-yekan.css')}}">
    <link href="{{asset('assets/panel/css/rtl.css')}}" rel="stylesheet" />
</head>
<body class="app sidebar-mini rtl login-img">

<div class="">

    <div id="global-loader">
        <img src="{{asset('assets/panel/images/loader.svg')}}" class="loader-img" alt="Loader">
    </div>


    <div class="page">
        <div class="">
            <div class="col col-login mx-auto mt-7">
                <div class="text-center">
                    <img src="{{'assets/panel/images/brand/logo-white.png'}}" class="header-brand-img" alt="">
                </div>
            </div>
            <div class="container-login100">

                <div class="wrap-login100 p-6">

                    <span class="login100-form-title pb-5">
                        تکمیل اطلاعات کارمند
                    </span>
                        <div class="panel panel-primary">
                            <form action="{{route('employees.storeInfo')}}" method="POST" class="login100-form validate-form">
                                @csrf
                                @error('province') <small class="text-danger ">{{$message}}</small> @enderror
                                <div class="wrap-input100 validate-input input-group">
                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                    </a>
                                    <select id="province" onchange="getCities()" name="province" class="form-control form-select select2" >
                                        <option value="0" label="انتخاب استان">انتخاب استان</option>
                                        @foreach($provinces as $province)
                                            <option value="{{$province->id}}" label="{{$province->name}}">{{$province->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @error('city') <small class="text-danger ">{{$message}}</small> @enderror
                                <div class="wrap-input100 validate-input input-group">
                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                    </a>
                                    <select id="city" name="city" class="form-control form-select select2" >
                                        <option label="انتخاب شهر">انتخاب شهر</option>

                                    </select>
                                </div>

                                @error('university_id') <small class="text-danger ">{{$message}}</small> @enderror
                                <div class="wrap-input100 validate-input input-group">
                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                    </a>
                                    <select id="university" name="university_id" class="form-control form-select select2" >
                                        <option value="0" label="انتخاب واحد دانشگاهی">انتخاب واحد دانشگاهی</option>
                                        @foreach($universities as $uni)
                                            <option value="{{$uni->id}}" label="{{$uni->name}}">{{$uni->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @error('uni_post') <small class="text-danger ">{{$message}}</small> @enderror
                                <div class="wrap-input100 validate-input input-group">
                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                    </a>
                                    <input name="uni_post" class="input100 border-start-0 ms-0 form-control" type="text"
                                           placeholder="سمت در دانشگاه">
                                </div>

                                @error('n_code') <small class="text-danger ">{{$message}}</small> @enderror
                                <div class="wrap-input100 validate-input input-group">
                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                    </a>
                                    <input name="n_code" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                           placeholder="کد ملی">
                                </div>

                                @error('p_code') <small class="text-danger ">{{$message}}</small> @enderror
                                <div class="wrap-input100 validate-input input-group">
                                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                        <i class="mdi mdi-account" aria-hidden="true"></i>
                                    </a>
                                    <input name="p_code" inputmode="numeric" class="input100 border-start-0 ms-0 form-control" type="text"
                                           placeholder="کد پرسنلی">
                                </div>

                                <div class="container-login100-form-btn">
                                    <button type="submit" class="login100-form-btn btn-primary">ثبت اطلاعات</button>
                                </div>
                            </form>
                        </div>
                </div>
            </div>

        </div>
    </div>

</div>


<script src="{{asset('assets/panel/js/jquery.min.js')}}"></script>

<script src="{{asset('assets/panel/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/panel/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

{{--<script src="{{'assets/panel/js/show-password.min.js'}}"></script>--}}

{{--<script src="{{'assets/panel/js/generate-otp.js'}}"></script>--}}
{{--<script src="{{'assets/panel/plugins/p-scroll/perfect-scrollbar.js'}}"></script>--}}

<script src="{{asset('assets/panel/js/themeColors.js')}}"></script>

<script src="{{asset('assets/panel/js/custom.js')}}"></script>
<script src="{{asset('assets/panel/js/custom1.js')}}"></script>

{{--<script src="{{'assets/panel/switcher/js/switcher.js'}}"></script>--}}
<script>
    function getCities(){
        if ($('#province').val() != 0 ){
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('users.registerAjax')}}',
                data: {
                    do: 'get-cities',
                    province:$('#province').val() ,
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response)
                    var city = document.getElementById('city')
                    city.innerHTML =' <option label="انتخاب شهر">انتخاب شهر</option>'
                    response.forEach(function (res){
                        city.innerHTML += '<option value="'+res['id']+'" label="'+res['name']+'">'+res['name']+'</option>'
                    })
                },
                error: function (response) {
                    console.log(response)
                    console.log('error')
                }
            });
        }
    }
</script>
</body>
</html>
