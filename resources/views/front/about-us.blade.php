@extends('front.master')
@section('title','درباره ما')
@section('main')
    <section class="pxp-page-image-hero pxp-cover" style="background-image: url(assets/front/images/company-hero-3.jpg); margin-top: 80px">
        <div class="pxp-hero-opacity"></div>
        <div class="pxp-page-image-hero-caption">
            <div class="pxp-container">
                <div class="row justify-content-center">
                    <div class="col-9 col-md-8 col-xl-7 col-xxl-6">
                        <h1 class="text-center">ما به کارفرمایان و کارجویان کمک می‌کنیم تا راحت تر به اهداف شغلی خود برسند</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-xxl-6">
                    <h2 class="pxp-section-h2 text-center">درباره فن یار</h2>
                    <p class="pxp-text-light text-center">فرصتی جدید برای تحصیل مهارت محور و اشتغال همزمان با تحصیل</p>

                    <div class="mt-4 mt-md-5 text-center">

                        <p>
                            هدف از اجرای این طرح فراهم نمودن شرايط اشتغال و ارتقای کار آمادگی دانشجويان از طريق توأم‌کردن آموزش تخصصی و مهارت حرفه‌ای و ایجاد ارتباط و پیوند نظام‌مند میان دانشگاه، صنعت و جامعه است .در این طرح دانشکده مهارت و کارآفرینی با مراکز صنعتی، اداری و تولیدی تفاهم‌نامه امضا خواهد کرد و دانشجویان متقاضی پس از کسب حداقل مهارت ها و توانایی هایی لازم برای کارآموزی جهت معرفی به ارگان یا نهاد مورد نظر گزینش می شوند و می توانند تا 8 واحد (هر واحد حداقل 120 ساعت) تحت عنوان کارآموزی پویشی در صنعت بگذرانند که بسته به قرارداد دانشکده یا دانشگاه با ارگان و نهاد مورد نظر می تواند همراه با پرداخت کمک هزینه تحصیلی از سمت ارگان یا نهاد مربوطه به دانشجو باشد که به حساب شهریه دانشجو واریز می شود. به عبارتی در این طرح فرصتی بینظیر برای اشتغال و تحصيل همزمان دانشجو فراهم می شود و در نتیجه دانشجو در محل کار با تعامل اجتماعی آشنا شده و اعتماد به نفس کافی را برای ورود به بازار کار و حتی کارآفرینی پیدا می‌کند. ضمن اینکه گواهی دوره های گذرانده شده را نیز دریافت می کند که به عنوان سابقه کار برای شخص مطرح می باشد.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

   {{-- <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">ما می خواهیم درباره شما بیشتر بدانیم</h2>
            <p class="pxp-text-light text-center">نمایه خود را انتخاب کنید</p>

            <div class="row justify-content-evenly mt-4 mt-md-5 pxp-animate-in pxp-animate-in-top">
                <div class="col-lg-4 col-xl-3 pxp-services-1-item-container">
                    <div class="pxp-services-1-item text-center pxp-animate-icon">
                        <div class="pxp-services-1-item-icon">
                            <img src="{{asset('assets/front/images/service-1-icon.png')}}" alt="Candidate">
                        </div>
                        <div class="pxp-services-1-item-title">من یک کاندید هستم</div>
                        <div class="pxp-services-1-item-text pxp-text-light"> شما هیچ شغلی را نمی خواهید، بلکه می خواهید
                            مناسب آن را اینجا پیدا کنید.</div>
                        <div class="pxp-services-1-item-cta">
                            <a href="{{route('about')}}">بیشتر بخوانید<span class="fa fa-angle-left"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 pxp-services-1-item-container">
                    <div class="pxp-services-1-item text-center pxp-animate-icon">
                        <div class="pxp-services-1-item-icon">
                            <img src="{{asset('assets/front/images/service-2-icon.png')}}" alt="Employer">
                        </div>
                        <div class="pxp-services-1-item-title">من کارفرما هستم</div>
                        <div class="pxp-services-1-item-text pxp-text-light">شما هیچ کاندید را نمی خواهید. شما مناسب را
                            می خواهید</div>
                        <div class="pxp-services-1-item-cta">
                            <a href="{{route('about')}}">بیشتر بخوانید<span class="fa fa-angle-left"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 pxp-services-1-item-container">
                    <div class="pxp-services-1-item text-center pxp-animate-icon">
                        <div class="pxp-services-1-item-icon">
                            <img src="{{asset('assets/front/images/service-3-icon.png')}}" alt="Press">
                        </div>
                        <div class="pxp-services-1-item-title">من عضو مطبوعات هستم</div>
                        <div class="pxp-services-1-item-text pxp-text-light"> ببینید در جابستر چه اتفاقی می افتد.</div>
                        <div class="pxp-services-1-item-cta">
                            <a href="{{route('about')}}">بیشتر بخوانید<span class="fa fa-angle-left"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}

    <section class="mt-100">
        <div class="pxp-container">
            <div class="row justify-content-between align-items-center mt-4 mt-md-5">
                <div class="col-lg-6 col-xxl-5">
                    <div class="pxp-info-fig pxp-animate-in pxp-animate-in-right">
                        <div class="pxp-info-fig-image pxp-cover"
                             style="background-image: url(assets/front/images/info-section-image.jpg);"></div>
                        <div class="pxp-info-stats">
                            <div class="pxp-info-stats-item pxp-animate-in pxp-animate-bounce">
                                <div class="pxp-info-stats-item-number">{{count($jobs_count)}}<span>موقعیت شغلی</span></div>
                                <div class="pxp-info-stats-item-description">در سامانه پویش</div>
                            </div>
                            <div class="pxp-info-stats-item pxp-animate-in pxp-animate-bounce">
                                <div class="pxp-info-stats-item-number">{{count($company_count)}}<span>کارفرما</span></div>
                                <div class="pxp-info-stats-item-description">در سامانه پویش</div>
                            </div>
                            <div class="pxp-info-stats-item pxp-animate-in pxp-animate-bounce">
                                <div class="pxp-info-stats-item-number">{{count($students_count)}}<span>کارجو</span></div>
                                <div class="pxp-info-stats-item-description">در سامانه پویش</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-xxl-6">
                    <div class="pxp-info-caption pxp-animate-in pxp-animate-in-top mt-4 mt-sm-5 mt-lg-0">
                        <h2 class="pxp-section-h2"><br> موردی را که مناسب شماست پیدا کنید.</h2>
                        <p class="pxp-text-light"> همه موقعیت های باز را در سامانه پویش جستجو کنید.</p>
                        <div class="pxp-info-caption-list">
                            <div class="pxp-info-caption-list-item">
                                <img src="{{asset('assets/front/images/check.svg')}}" alt="-"><span>مهارت</span>
                            </div>
                            <div class="pxp-info-caption-list-item">
                                <img src="{{asset('assets/front/images/check.svg')}}" alt="-"><span>کارآموزی</span>
                            </div>
                            <div class="pxp-info-caption-list-item">
                                <img src="{{asset('assets/front/images/check.svg')}}" alt="-"><span>اشتغال</span>
                            </div>
                        </div>
                        <div class="pxp-info-caption-cta">
                            <a href="{{route('jobs.list')}}" class="btn rounded-pill pxp-section-cta">اکنون شروع کنید<span
                                    class="fa fa-angle-left"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

  {{--  <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">چرا مشتریان ما را دوست دارند</h2>
            <p class="pxp-text-light text-center">آنچه مشتریان ما در مورد ما می گویند</p>

            <div class="pxp-testimonials-1">
                <div class="pxp-testimonials-1-circles d-none d-md-block">
                    <div class="pxp-testimonials-1-circles-item pxp-item-1 pxp-cover pxp-animate-in pxp-animate-bounce"
                         style="background-image: url(assets/front/images/customer-1.png);"></div>
                    <div class="pxp-testimonials-1-circles-item pxp-item-2 pxp-animate-in pxp-animate-bounce"></div>
                    <div class="pxp-testimonials-1-circles-item pxp-item-3 pxp-animate-in pxp-animate-bounce"></div>
                    <div class="pxp-testimonials-1-circles-item pxp-item-4 pxp-cover pxp-animate-in pxp-animate-bounce"
                         style="background-image: url(assets/front/images/customer-2.png);"></div>
                    <div class="pxp-testimonials-1-circles-item pxp-item-5 pxp-cover pxp-animate-in pxp-animate-bounce"
                         style="background-image: url(assets/front/images/customer-3.png);"></div>
                    <div class="pxp-testimonials-1-circles-item pxp-item-6 pxp-animate-in pxp-animate-bounce"></div>
                    <div class="pxp-testimonials-1-circles-item pxp-item-7 pxp-cover pxp-animate-in pxp-animate-bounce"
                         style="background-image: url(assets/front/images/customer-4.png);"></div>
                    <div class="pxp-testimonials-1-circles-item pxp-item-8 pxp-animate-in pxp-animate-bounce"></div>
                    <div class="pxp-testimonials-1-circles-item pxp-item-9 pxp-cover pxp-animate-in pxp-animate-bounce"
                         style="background-image: url(assets/front/images/customer-5.png);"></div>
                    <div class="pxp-testimonials-1-circles-item pxp-item-10 pxp-cover pxp-animate-in pxp-animate-bounce"
                         style="background-image: url(assets/front/images/customer-6.png);"></div>
                </div>

                <div class="pxp-testimonials-1-carousel-container">
                    <div class="row justify-content-center align-items-center">
                        <div class="col-10 col-md-6 col-lg-6 col-xl-5 col-xxl-4">
                            <div class="pxp-testimonials-1-carousel pxp-animate-in pxp-animate-in-top">
                                <div id="pxpTestimonials1Carousel" class="carousel slide" data-bs-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item text-center active">
                                            <div class="pxp-testimonials-1-carousel-item-text"> جابستر یک محل کار همیشه
                                                در حال تغییر است که با گرد هم آوردن کارفرمایان و نامزدها در تلاش برای
                                                نوآوری است. ما وقف بهبود زندگی مشتریان خود و همچنین کارمندان خود هستیم.
                                            </div>
                                            <div class="pxp-testimonials-1-carousel-item-name">سوزان ویل</div>
                                            <div class="pxp-testimonials-1-carousel-item-company">روشن فکر استودیو
                                            </div>
                                        </div>
                                        <div class="carousel-item text-center">
                                            <div class="pxp-testimonials-1-carousel-item-text"> هر روز، من از همکارانم
                                                الهام می‌گیرم تا نوآوری را به انجام برسانم. جابستر محیطی از اعتماد و
                                                پشتیبانی را ایجاد می کند که در آن می توانم موفقیت مشتری را به ارمغان
                                                بیاورم.</div>
                                            <div class="pxp-testimonials-1-carousel-item-name"> کنت اسپایرز</div>
                                            <div class="pxp-testimonials-1-carousel-item-company">وب لایف</div>
                                        </div>
                                        <div class="carousel-item text-center">
                                            <div class="pxp-testimonials-1-carousel-item-text"> جابستر یک محل کار همیشه
                                                در حال تغییر است که با گرد هم آوردن کارفرمایان و نامزدها در تلاش برای
                                                نوآوری است. ما وقف بهبود زندگی مشتریان خود و همچنین کارمندان خود هستیم.
                                            </div>
                                            <div class="pxp-testimonials-1-carousel-item-name">ربکا ایسون</div>
                                            <div class="pxp-testimonials-1-carousel-item-company">وب پویا</div>
                                        </div>
                                    </div>
                                    <button class="carousel-control-prev" type="button"
                                            data-bs-target="#pxpTestimonials1Carousel" data-bs-slide="prev">
                                        <span class="fa fa-angle-right" aria-hidden="true"></span>
                                        <span class="visually-hidden">قبلی</span>
                                    </button>
                                    <button class="carousel-control-next" type="button"
                                            data-bs-target="#pxpTestimonials1Carousel" data-bs-slide="next">
                                        <span class="fa fa-angle-left" aria-hidden="true"></span>
                                        <span class="visually-hidden">بعدی</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}

    {{-- <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">با خبر شوید</h2>
            <p class="pxp-text-light text-center">برای دریافت فید هفتگی ما در خبرنامه ما مشترک شوید.</p>

            <div class="row mt-4 mt-md-5 justify-content-center">
                <div class="col-md-9 col-lg-7 col-xl-6 col-xxl-5">
                    <div class="pxp-subscribe-1-container pxp-animate-in pxp-animate-in-top">
                        <div class="pxp-subscribe-1-image">
                            <img src="{{asset('assets/front/images/subscribe.png')}}" alt="Stay Up to Date">
                        </div>
                        <div class="pxp-subscribe-1-form">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="ایمیل خود را وارد کنید...">
                                    <button class="btn btn-primary" type="button">اشتراک<span
                                            class="fa fa-angle-left"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
@endsection
