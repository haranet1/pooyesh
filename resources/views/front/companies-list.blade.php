@extends('front.master')
@section('title','لیست شرکت ها')
@section('main')
    <section class="pxp-page-header-simple">
        <div class="pxp-container">
            <h1>جستجوی شرکت ها</h1>
            <div class="pxp-hero-subtitle pxp-text-ligh">برای بهترین شرکت های جهان کار کنید</div>
            <div class="pxp-hero-form pxp-hero-form-round pxp-large mt-3 mt-lg-4 pxp-has-border">
                <form class="row gx-3 align-items-center" method="GET" action="{{route('companies.list.search')}}">
                    @csrf
                    <div class="col-12 col-lg">
                        <div class="input-group mb-3 mb-lg-0">
                            <span class="input-group-text"><span class="fa fa-search"></span></span>
                            <input type="text" name="name" class="form-control" placeholder="نام شرکت">
                        </div>
                    </div>
                    <div class="col-12 col-lg-auto">
                        <button>یافتن شرکت ها</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <section class="mt-4 mt-lg-5 pt-100 pb-100" style="background-color: var(--pxpSecondaryColorLight);">
        <div class="pxp-container">
            <div class="pxp-jobs-list-top">
                <div class="row justify-content-between align-items-center">
                    <div class="col-auto">
                        <h2><span class="pxp-text-light">نمایش</span> {{count($companies)}} <span
                                class="pxp-text-light">شرکت </span>
                        </h2>
                    </div>
                </div>
            </div>
            @if(count($companies) > 0)
                <div class="row">
                    @foreach($companies as $company)
                        <div class="col-md-6 col-xl-4 col-xxl-3 pxp-companies-card-1-container">
                            <div class="pxp-companies-card-1 pxp-has-shadow">
                                <div class="pxp-companies-card-1-top">
                                    <a href="{{route('company.single',$company->id)}}"
                                       class="pxp-companies-card-1-company-logo"
                                       style="background-image: url(assets/front/images/company-logo-1.png);"></a>
                                    <a href="{{route('company.single',$company->id)}}"
                                       class="pxp-companies-card-1-company-name">{{$company->groupable->name}}</a>
                                    <div class="pxp-companies-card-1-company-description pxp-text-light">
                                        @php
                                            echo Str::limit(strip_tags($company->groupable->about), 50, '...')
                                        @endphp
                                    </div>
                                </div>
                                <div class="pxp-companies-card-1-bottom">
                                    <a href="{{route('jobs.list')}}" class="pxp-companies-card-1-company-jobs">
                                        {{count($company->job_positions)}}
                                        شغل
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="pxp-jobs-card-3 pxp-has-border">
                    <div class="row align-items-center text-center justify-content-between">
                        <h4>{{__('public.no_info')}}</h4>
                    </div>
                </div>
            @endif

            <div class="row mt-4 mt-lg-5 justify-content-between align-items-center">
                <div class="col-auto">
                    <nav class="mt-3 mt-sm-0" aria-label="Companies pagination">
                        {{$companies->links('pagination.front')}}
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection
