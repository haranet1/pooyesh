@extends('front.master')
@section('title','تماس با ما')
@section('main')
    <section class="mt-100 pxp-no-hero">
        <div class="pxp-container">
            <h3 class="pxp-section-h5 text-center">مراحل پایش سلامت (انجام پرسشنامه سلامت عمومی)</h3>

            <div class="row mt-4 mt-md-5 justify-content-center pxp-animate-in pxp-animate-in-top">
                <div class="">
                    <div class="pxp-single-candidate-timeline text-center">
                        <div class="pxp-single-candidate-timeline-item">
                            <div class="pxp-single-candidate-timeline-dot"></div>
                            <div class="pxp-single-candidate-timeline-info ms-3">
                                <div class="pxp-single-candidate-timeline-time"><span class="me-3">گام 1</span></div>
                                <div class="pxp-single-candidate-timeline-position mt-2">
                                </div>
                                <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                    از طریق دکمه زیر وارد پرسشنامه شوید.
                                </div>
                            </div>
                        </div>
                        <div class="pxp-single-candidate-timeline-item">
                            <div class="pxp-single-candidate-timeline-dot"></div>
                            <div class="pxp-single-candidate-timeline-info ms-3">
                                <div class="pxp-single-candidate-timeline-time"><span class="me-3">گام 2</span></div>
                                <div class="pxp-single-candidate-timeline-position mt-2">
                                </div>
                                <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                    بر روی صفحه کلیک کنید و استان اصفهان و واحد مستقل نجف آباد را در گزینه های خواسته شده انتخاب نمایید.
                                </div>
                            </div>
                        </div>
                        <div class="pxp-single-candidate-timeline-item">
                            <div class="pxp-single-candidate-timeline-dot"></div>
                            <div class="pxp-single-candidate-timeline-info ms-3">
                                <div class="pxp-single-candidate-timeline-time"><span class="me-3">گام 3</span></div>
                                <div class="pxp-single-candidate-timeline-position mt-2">
                                </div>
                                <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                    زبان تایپ گوشی یا کامپیوتر خود را لاتین نموده و شماره ملی خود را وارد نمایید.
                                </div>
                            </div>
                        </div>
                        <div class="pxp-single-candidate-timeline-item">
                            <div class="pxp-single-candidate-timeline-dot"></div>
                            <div class="pxp-single-candidate-timeline-info ms-3">
                                <div class="pxp-single-candidate-timeline-time"><span class="me-3">گام 4</span></div>
                                <div class="pxp-single-candidate-timeline-position mt-2">
                                </div>
                                <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                    شماره موبایل خود را بدون صفر وارد کنید.
                                </div>
                            </div>
                        </div>
                        <div class="pxp-single-candidate-timeline-item">
                            <div class="pxp-single-candidate-timeline-dot"></div>
                            <div class="pxp-single-candidate-timeline-info ms-3">
                                <div class="pxp-single-candidate-timeline-time"><span class="me-3">گام 5</span></div>
                                <div class="pxp-single-candidate-timeline-position mt-2">
                                </div>
                                <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                    پرسشنامه 28 سوالی سلامت عمومی را پاسخ دهید.
                                </div>
                            </div>
                        </div>
                        <div class="pxp-single-candidate-timeline-item">
                            <div class="pxp-single-candidate-timeline-dot"></div>
                            <div class="pxp-single-candidate-timeline-info ms-3">
                                <div class="pxp-single-candidate-timeline-time"><span class="me-3">گام 6</span></div>
                                <div class="pxp-single-candidate-timeline-position mt-2">
                                </div>
                                <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                    روی گزینه ارسال پاسخ های خودشناسی کلیک نمایید.
                                </div>
                            </div>
                        </div>
                        <div class="pxp-single-candidate-timeline-item">
                            <div class="pxp-single-candidate-timeline-dot"></div>
                            <div class="pxp-single-candidate-timeline-info ms-3">
                                <div class="pxp-single-candidate-timeline-time"><span class="me-3">گام 7</span></div>
                                <div class="pxp-single-candidate-timeline-position mt-2">
                                </div>
                                <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                    درصورتی که سامانه کد تایید درخواست می¬نماید، کد تایید را وارد نموده و مجددا دکمه ارسال پاسخ¬ها را کلیک نمایید.
                                </div>
                            </div>
                        </div>
                        <div class="pxp-single-candidate-timeline-item">
                            <div class="pxp-single-candidate-timeline-dot"></div>
                            <div class="pxp-single-candidate-timeline-info ms-3">
                                <div class="pxp-single-candidate-timeline-time"><span class="me-3">گام 8</span></div>
                                <div class="pxp-single-candidate-timeline-position mt-2">
                                </div>
                                <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                    کد پیگیری دریافتی از سامانه را به همکاران مرکز مشاوره ارائه فرمایید.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center my-4">
                        <div class="text-center">
                            <a class="btn btn-primary" href="https://stu.iau.ir/sima/fa/form/59/%D8%AA%D8%B3%D8%AA">ورود به پرسشنامه</a>
                        </div>
                    </div>
                </div>

                {{-- <ul>
                    <li>
                        1-آدرس زیر را در مرورگر خود وارد نمایید https://stu.iau.ir/sima/fa
                    </li>
                    <li>
                        2-بر روی صفحه کلیک کنید و استان اصفهان و واحد مستقل نجف آباد را در گزینه های خواسته شده انتخاب نمایید.
                    </li>
                    <li>
                        3-زبان تایپ گوشی یا کامپیوتر خود را لاتین نموده و شماره ملی خود را وارد نمایید.
                    </li>
                    <li>
                        4-شماره موبایل خود را بدون صفر وارد کنید.
                    </li>
                    <li>
                        5-پرسشنامه 28 سوالی سلامت عمومی را پاسخ دهید.
                    </li>
                    <li>
                        6-روی گزینه ارسال پاسخ های خودشناسی کلیک نمایید.
                    </li>
                    <li>
                        7-درصورتی که سامانه کد تایید درخواست می¬نماید، کد تایید را وارد نموده و مجددا دکمه ارسال پاسخ¬ها را کلیک نمایید.
                    </li>
                    <li>
                        8-کد پیگیری دریافتی از سامانه را به همکاران مرکز مشاوره ارائه فرمایید.
                    </li>
                </ul> --}}
            </div>

           {{-- <div class="row mt-100 justify-content-center pxp-animate-in pxp-animate-in-top">
                <div class="col-lg-6 col-xxl-4">
                    <div class="pxp-contact-us-form pxp-has-animation pxp-animate">
                        <h2 class="pxp-section-h2 text-center"> تماس با ما </h2>
                        <form class="mt-4">
                            <div class="mb-3">
                                <label for="contact-us-name" class="form-label">نام</label>
                                <input type="text" class="form-control" id="contact-us-name"
                                       placeholder="نام خود را اینجا بنویسید">
                            </div>
                            <div class="mb-3">
                                <label for="contact-us-email" class="form-label">ایمیل</label>
                                <input type="text" class="form-control" id="contact-us-email"
                                       placeholder="ایمیل خود را اینجا بنویسید">
                            </div>
                            <div class="mb-3">
                                <label for="contact-us-message" class="form-label">پیام</label>
                                <textarea class="form-control" id="contact-us-message"
                                          placeholder="پیام خود را اینجا بنویسید..."></textarea>
                            </div>
                            <a href="#" class="btn rounded-pill pxp-section-cta d-block">ارسال پیام</a>
                        </form>
                    </div>
                </div>
            </div>--}}
        </div>
    </section>
@endsection
