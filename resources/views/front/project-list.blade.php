@extends('front.master')
@section('title','لیست پروژه ها')
@section('css')
    <style>
        .range-slider {
            position: relative;
            width: 200px;
            height: 35px;
            text-align: center;
        }

        .range-slider input {
            pointer-events: none;
            position: absolute;
            overflow: hidden;
            left: 0;
            top: 15px;
            width: 200px;
            outline: none;
            height: 18px;
            margin: 0;
            padding: 0;
        }

        .range-slider input::-webkit-slider-thumb {
            pointer-events: all;
            position: relative;
            z-index: 1;
            outline: 0;
        }

        .range-slider input::-moz-range-thumb {
            pointer-events: all;
            position: relative;
            z-index: 10;
            -moz-appearance: none;
            width: 9px;
        }

        .range-slider input::-moz-range-track {
            position: relative;
            z-index: -1;
            background-color: rgba(0, 0, 0, 1);
            border: 0;
        }

        .range-slider input:last-of-type::-moz-range-track {
            -moz-appearance: none;
            background: none transparent;
            border: 0;
        }

        .range-slider input[type=range]::-moz-focus-outer {
            border: 0;
        }

        .rangeValue {
            width: 30px;
        }

        .output {
            position: absolute;
            /*border:1px solid #999;*/
            width: 40px;
            height: 30px;
            text-align: center;
            color: #999;
            border-radius: 4px;
            display: inline-block;
            font: bold 15px/30px Helvetica, Arial;
            bottom: 75%;
            left: 50%;
            transform: translate(-50%, 0);
        }

        .output.outputTwo {
            left: 100%;
        }

        .container {
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        input[type=range] {
            -webkit-appearance: none;
            background: none;
        }

        input[type=range]::-webkit-slider-runnable-track {
            height: 5px;
            border: none;
            border-radius: 3px;
            background: transparent;
        }

        input[type=range]::-ms-track {
            height: 5px;
            background: transparent;
            border: none;
            border-radius: 3px;
        }

        input[type=range]::-moz-range-track {
            height: 5px;
            background: transparent;
            border: none;
            border-radius: 3px;
        }

        input[type=range]::-webkit-slider-thumb {
            -webkit-appearance: none;
            border: none;
            height: 16px;
            width: 16px;
            border-radius: 50%;
            background: #555;
            margin-top: -5px;
            position: relative;
            z-index: 10000;
        }

        input[type=range]::-ms-thumb {
            -webkit-appearance: none;
            border: none;
            height: 16px;
            width: 16px;
            border-radius: 50%;
            background: #555;
            margin-top: -5px;
            position: relative;
            z-index: 10000;
        }

        input[type=range]::-moz-range-thumb {
            -webkit-appearance: none;
            border: none;
            height: 16px;
            width: 16px;
            border-radius: 50%;
            background: #0a53be;
            margin-top: -5px;
            position: relative;
            z-index: 10000;
        }

        input[type=range]:focus {
            outline: none;
        }

        .full-range,
        .incl-range {
            width: 100%;
            height: 5px;
            left: 0;
            top: 21px;
            position: absolute;
            background: #DDD;
        }

        .incl-range {
            background: #0dcaf0;
        }
    </style>
@endsection
@section('main')
    <section class="pxp-page-header-simple" style="background-color: var(--pxpMainColorLight);">
        <div class="pxp-container">
            <h1 class="text-center">جستجوی پروژه ها</h1>
            <div class="pxp-hero-subtitle pxp-text-light text-center">پروژه مورد نظر خود را از میان
                <strong>{{count($projects)}}</strong> پروژه جستجو کنید
            </div>
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <div class="row">
                <div class="col-lg-5 col-xl-4 col-xxl-3">
                    <div class="pxp-jobs-list-side-filter">
                        <div class="pxp-list-side-filter-header d-flex d-lg-none">
                            <div class="pxp-list-side-filter-header-label">فیلتر</div>
                            <a role="button"><span class="fa fa-sliders"></span></a>
                        </div>
                        <div class="mt-4 mt-lg-0 d-lg-block pxp-list-side-filter-panel">
                            <form action="{{route('projects.search')}}" method="get">
                                @csrf
                            <h3>جستجوی پروژه ها</h3>
                            <div class="mt-2 mt-lg-3 justify-content-center text-center">
                                <div class="input-group mb-3">
                                    <span class="input-group-text"><span class="fa fa-search"></span></span>
                                    <input name="key" type="text" required class="form-control" placeholder="کلمه کلیدی یا عنوان پروژه ">
                                </div>
                                <button type="submit" class="btn btn-primary rounded-pill pxp-nav-btn px-4" >جستجو</button>
                            </div>

                            {{--<h3 class="mt-3 mt-lg-4">محدوده بودجه مود نیاز </h3>

                            <div class="list-group mt-5 py-3 text-center mt-lg-3 " dir="ltr">
                                <div class="d-flex align-items-center justify-content-center text-center">
                                    <div class="range-slider " dir="ltr">
                                        <span class="output outputOne"></span>
                                        <span class="output outputTwo"></span>
                                        <span class="full-range"></span>
                                        <span class="incl-range"></span>
                                        <input name="rangeOne" value="100000" min="100000" max="50000000" step="1" type="range">
                                        <input name="rangeTwo" value="50000000" min="100000" max="50000000" step="1" type="range">
                                    </div>
                                </div>
                            </div>--}}
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-xl-8 col-xxl-9">
                    <div class="pxp-jobs-list-top mt-4 mt-lg-0">
                        <div class="row justify-content-between align-items-center">
                            {{--<div class="col-auto">
                                <h2><span class="pxp-text-light">نمایش</span> 8,536 <span
                                        class="pxp-text-light">پروژه </span></h2>
                            </div>
                            <div class="col-auto">
                                <select class="form-select">
                                    <option value="0" selected> مرتبط ترین</option>
                                    <option value="1">جدیدترین</option>
                                    <option value="2">قدیمی ترین</option>
                                </select>
                            </div>--}}
                        </div>
                    </div>

                    <div>
                        @if(count($projects) > 0)
                            @foreach($projects as $project)
                                <div class="pxp-jobs-card-3 pxp-has-border">
                                    <div class="row align-items-center justify-content-between">
                                        <div class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-xxl-auto">
                                            <a href="{{route('company.single',$project->owner->id)}}" class="pxp-jobs-card-3-company-logo"
                                               style="background-image: url(assets/front/images/company-logo-1.png);"></a>
                                        </div>
                                        <div class="col-sm-9 col-md-10 col-lg-9 col-xl-10 col-xxl-4">
                                            <a href="{{route('projects.single',[$project->id,$project->title])}}"
                                               class="pxp-jobs-card-3-title mt-3 mt-sm-0"> {{$project->title}}</a>
                                            <div class="pxp-jobs-card-3-details">
                                                <div class="pxp-jobs-card-3-type">{{Str::limit($project->description,'35','...')}}</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 col-xl-6 col-xxl-4 mt-3 mt-xxl-0">
                                            <a href="{{route('projects.search-by-cat',$project->category->id)}}" class="pxp-jobs-card-3-category">
                                                <div class="pxp-jobs-card-3-category-label">{{$project->category->title}}</div>
                                            </a>
                                            <div class="pxp-jobs-card-3-date-company">
                                                <span class="pxp-jobs-card-3-date pxp-text-light"> {{verta($project->created_at)->formatDifference()}} توسط</span>
                                                <a
                                                    href="{{route('company.single',$project->owner->id)}}" class="pxp-jobs-card-3-company">{{$project->owner->groupable->name." ".$project->owner->groupable->family}}</a>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xl-2 col-xxl-auto mt-3 mt-xxl-0 pxp-text-right">
                                            <a href="{{route('projects.single',[$project->id,$project->title])}}" class="btn rounded-pill pxp-card-btn">جزئیات</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="pxp-jobs-card-3 pxp-has-border">
                                <div class="row align-items-center text-center justify-content-between">
                                  <h4>{{__('public.no_info')}}</h4>
                                </div>
                            </div>
                        @endif

                    </div>

                    <div class="row mt-4 mt-lg-5 justify-content-between align-items-center">
                        <div class="col-auto">
                            <nav class="mt-3 mt-sm-0" aria-label="Jobs list pagination">
                                {{$projects->links('pagination.front')}}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        var rangeOne = document.querySelector('input[name="rangeOne"]'),
            rangeTwo = document.querySelector('input[name="rangeTwo"]'),
            outputOne = document.querySelector('.outputOne'),
            outputTwo = document.querySelector('.outputTwo'),
            inclRange = document.querySelector('.incl-range'),
            updateView = function () {
                if (this.getAttribute('name') === 'rangeOne') {
                    outputOne.innerHTML = separate(this.value);
                    outputOne.style.left = this.value / this.getAttribute('max') * 100 + '%';
                } else {
                    outputTwo.style.left = this.value / this.getAttribute('max') * 100 + '%';
                    outputTwo.innerHTML = separate(this.value)
                }
                if (parseInt(rangeOne.value) > parseInt(rangeTwo.value)) {
                    inclRange.style.width = (rangeOne.value - rangeTwo.value) / this.getAttribute('max') * 100 + '%';
                    inclRange.style.left = rangeTwo.value / this.getAttribute('max') * 100 + '%';
                } else {
                    inclRange.style.width = (rangeTwo.value - rangeOne.value) / this.getAttribute('max') * 100 + '%';
                    inclRange.style.left = rangeOne.value / this.getAttribute('max') * 100 + '%';
                }
            };

        document.addEventListener('DOMContentLoaded', function () {
            updateView.call(rangeOne);
            updateView.call(rangeTwo);
            $('input[type="range"]').on('mouseup', function () {
                this.blur();
            }).on('mousedown input', function () {
                updateView.call(this);
            });
        });
    </script>
@endsection
