<!doctype html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="پویش">
    <meta name="author" content="haranet.ir">

    <link rel="shortcut icon" type="image/x-icon" href="{{'assets/panel/images/brand/favicon.ico'}}" />

    <title>پویش | تکمیل اطلاعات</title>

    <link id="style" href="{{asset('assets/panel/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/panel/css/style.css')}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/dark-style.css'}}" rel="stylesheet" />
    <link href="{{'assets/panel/css/transparent-style.css'}}" rel="stylesheet">
    <link href="{{'assets/panel/css/skin-modes.css'}}" rel="stylesheet" />

    <link href="{{'assets/panel/css/icons.css'}}" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{'assets/panel/colors/color1.css'}}" />
    <link href="{{'assets/panel/switcher/css/switcher.css'}}" rel="stylesheet" />
    <link href="{{'assets/panel/switcher/demo.css'}}" rel="stylesheet" />
    <link rel="stylesheet" id="fonts" href="{{'assets/panel/fonts/styles-fa-num/iran-yekan.css'}}">
    <link href="{{'assets/panel/css/rtl.css'}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{'assets/datepicker/dist/css/persian-datepicker.css'}}"/>

</head>
<body class="app sidebar-mini rtl login-img">

<div class="">

    <div id="global-loader">
        <img src="{{'assets/panel/images/loader.svg'}}" class="loader-img" alt="Loader">
    </div>

    <div class="page">
        <div class="">
            <div class="col col-login mx-auto mt-7">
                <div class="text-center">
                    <img src="{{'assets/panel/images/brand/logo-white.png'}}" class="header-brand-img" alt="">
                </div>
            </div>
            <div class="container mt-3" style="margin-bottom: 100px">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-8">
                        <div class="wrap-login100 p-6">
                            <span class="login100-form-title pb-3">تکمیل اطلاعات کاربری</span>
                            <div class="row justify-content-center">
                                <span class="text-danger text-center pb-3" style="font-size: 1rem">(تمام فیلد ها الزامی است)</span>
                            </div>
                            <div class="panel panel-primary ">
                                <form action="{{route('applicants.storeInfo')}}" method="POST" class="login100-form validate-form">
                                    @csrf
                                    <div class="row align-items-start">
                                        <div class="col-12 col-md-6">

                                            {{-- province --}}
                                            <div class="wrap-input100 mb-0  validate-input input-group">
                                                        <span class="input-group-text bg-white text-muted">
                                                            <i class="mdi mdi-map-marker-radius" aria-hidden="true"></i>
                                                        </span>
                                                <select id="province" style="height: 44px" onchange="getCities()" name="province" class="form-control form-select select2" >
                                                    <option label="انتخاب استان">انتخاب استان</option>
                                                    @foreach($provinces as $province)
                                                        <option value="{{$province->id}}" label="{{$province->name}}">{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @error('province') <small class="text-danger ">{{$message}}</small> @enderror

                                            

                                            {{-- marital_status --}}

                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-city" aria-hidden="true"></i>
                                                </span>
                                                <select style="height: 44px" id="marital_status" name="marital_status" class="form-control form-select select2" >
                                                    <option value="" label="وضعیت تاهل">وضعیت تاهل</option>
                                                    @foreach(\App\Enums\MaritalStatus::toValues() as $item)
                                                        <option value="{{$item}}">{{$item}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            @error('marital_status') <small class="text-danger ">{{$message}}</small> @enderror

                                            {{-- birth --}}
                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account" aria-hidden="true"></i>
                                                </span>
                                                <input autocomplete="off" id="birth" name="birth" class="input100 border-start-0 ms-0 form-control" type="text"
                                                    placeholder="تاریخ تولد">
                                            </div>
                                            @error('birth') <small class="text-danger ">{{$message}}</small> @enderror

                                            {{-- experience --}}
                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-account" aria-hidden="true"></i>
                                                </span>
                                                <input autocomplete="off" id="experience" name="experience" class="input100 border-start-0 ms-0 form-control" type="number"
                                                    placeholder="سابقه کاری">
                                            </div>
                                            @error('experience') <small class="text-danger ">{{$message}}</small> @enderror

                                        </div>


                                        <div class="col-12 col-md-6">

                                            {{-- city --}}
                                            <div class="wrap-input100 mb-0 mt-2 mt-md-0 validate-input input-group">
                                                    <span class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-city" aria-hidden="true"></i>
                                                    </span>
                                                <select style="height: 44px" id="city" name="city" class="form-control form-select select2" >
                                                    <option label="انتخاب شهر">انتخاب شهر</option>

                                                </select>
                                            </div>
                                            @error('city') <small class="text-danger ">{{$message}}</small> @enderror

                                            {{-- sex --}}
                                            @if (is_null(Auth::user()->sex))
                                                <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                    <span class="input-group-text bg-white text-muted">
                                                        <i class="mdi mdi-city" aria-hidden="true"></i>
                                                    </span>
                                                    <select style="height: 44px" id="sex" name="sex" class="form-control form-select select2" >
                                                        <option label="جنسیت">جنسیت</option>
                                                        <option label="مرد">مرد</option>
                                                        <option label="زن">زن</option>
                                                    </select>
                                                </div>
                                            @endif

                                            {{-- military_status --}}
                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-city" aria-hidden="true"></i>
                                                </span>
                                                <select style="height: 44px" id="military_status" name="military_status" class="form-control form-select select2" >
                                                    <option value="" label="وضعیت نظام وظیفه">وضعیت نظام وظیفه</option>
                                                    @foreach(\App\Enums\MilitaryStatus::toValues() as $item)
                                                        <option value="{{$item}}">{{$item}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @error('military_status') <small class="text-danger ">{{$message}}</small> @enderror

                                            {{-- requested_salary --}}
                                            <div class="wrap-input100 mb-0 mt-2 validate-input input-group">
                                                <span class="input-group-text bg-white text-muted">
                                                    <i class="mdi mdi-city" aria-hidden="true"></i>
                                                </span>
                                                <select style="height: 44px" id="requested_salary" name="requested_salary" class="form-control form-select select2" >
                                                    <option label="حقوق درخواستی" value="">حقوق درخواستی</option>
                                                    <option value="3 - 2">3 - 2 میلیون تومان</option>
                                                    <option value="4 - 3">4 - 3 میلیون تومان</option>
                                                    <option value="5 - 4">5 - 4 میلیون تومان</option>
                                                    <option value="6 - 5">6 - 5 میلیون تومان</option>
                                                    <option value="8 - 6">8 - 6 میلیون تومان</option>
                                                    <option value="10 - 8">10 - 8 میلیون تومان</option>
                                                    <option value="12 - 10">12 - 10 میلیون تومان</option>
                                                    <option value="15 - 12">15 - 12 میلیون تومان</option>
                                                    <option value="20 - 15">20 - 15 میلیون تومان</option>
                                                    <option value="25 - 20">25 - 20 میلیون تومان</option>
                                                    <option value="35 - 25">35 - 25 ميليون تومان</option>
                                                    <option value="45 - 35">45 - 35 ميليون تومان</option>
                                                    <option value="60 - 45">60 - 45 میلیون تومان</option>
                                                    <option value="60">60 میلیون تومان به بالا</option>
                                                </select>
                                            </div>
                                            @error('requested_salary') <small class="text-danger ">{{$message}}</small> @enderror

                                            
                                        </div>
                                    </div>
                                    

                                    <div class="wrap-input100 mt-2 mb-0 validate-input input-group">

                                        <textarea  class="form-control" placeholder="آدرس محل سکونت " name="address" id="address" cols="30" rows="8">{{old('address')}}</textarea>
                                    </div>
                                    @error('address') <small class="text-danger ">{{$message}}</small> @enderror


                                    <div class="container-login100-form-btn">
                                        <button type="submit" class="login100-form-btn btn-primary">ثبت اطلاعات</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>


<script src="{{'assets/panel/js/jquery.min.js'}}"></script>

<script src="{{'assets/panel/plugins/bootstrap/js/popper.min.js'}}"></script>
<script src="{{'assets/panel/plugins/bootstrap/js/bootstrap.min.js'}}"></script>


<script src="{{'assets/panel/js/themeColors.js'}}"></script>

<script src="{{'assets/panel/js/custom.js'}}"></script>
<script src="{{'assets/panel/js/custom1.js'}}"></script>

<script src="{{'assets/datepicker/dist/js/persian-datepicker.js'}}"></script>
<script src="{{'assets/datepicker/assets/persian-date.min.js'}}"></script>
<script>
    function getCities(){
        if ($('#province').val() != 0 ){
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('users.registerAjax')}}',
                data: {
                    do: 'get-cities',
                    province:$('#province').val() ,
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response)
                    var city = document.getElementById('city')
                    city.innerHTML =' <option label="انتخاب شهر (ضروری)">انتخاب شهر</option>'
                    response.forEach(function (res){
                        city.innerHTML += '<option value="'+res['id']+'" label="'+res['name']+'">'+res['name']+'</option>'
                    })
                },
                error: function (response) {
                    console.log(response)
                    console.log('error')
                }
            });
        }
    }

    $('#birth').persianDatepicker({
        altField: '#birth',
        altFormat: "YYYY/MM/DD",
        observer: true,
        format: 'YYYY/MM/DD',
        initialValue: false,
        initialValueType: 'persian',
        autoClose: true,
        responsive: true,

    });
</script>
</body>
</html>
