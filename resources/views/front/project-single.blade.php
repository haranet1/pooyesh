@extends('front.master')
@section('title','جزئیات پروژه')
@section('css')
    <style>
        .btn-like{
            cursor: pointer;
            outline: 0;
            color: #AAA;

        }

        .btn-like:focus {
            outline: none;
        }

        .green{
            color: green;
        }

        .red{
            color: red;
        }
        .progress {
            overflow: hidden;
            height: 20px;
            background-color: #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
            box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
            margin-bottom: 20px;
        }
        .progress-bar {
            width: 0;
            height: 100%;
            color: #fff;
            text-align: center;
            background-color: #0fa751;



        }
        .progress-striped .progress-bar {
            background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-size: 40px 40px;
        }
        .progress.active .progress-bar {
            -webkit-animation: progress-bar-stripes 2s linear infinite;
            animation: progress-bar-stripes 2s linear infinite;
            -moz-animation: progress-bar-stripes 2s linear infinite;
        }
    </style>

    @endsection
@section('main')
    <section>
        <div class="pxp-container">
            <div class="pxp-single-company-container pxp-has-columns">
                <div class="row">
                    <div class="col-lg-7 col-xl-8 col-xxl-9">
                        <div class="pxp-single-company-hero pxp-cover pxp-boxed"
                             style="background-image: url(assets/front/images/company-hero-3.jpg);">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-single-company-hero-caption">
                                <div class="pxp-container">
                                    <div class="pxp-single-company-hero-content">
                                        <div class="pxp-single-company-hero-logo"
                                             style="background-image: url(assets/front/images/company-logo-2.png);"></div>
                                        <div class="pxp-single-company-hero-title">
                                            <h1>{{$project->title}} </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <section class="mt-4 mt-lg-5">
                            <div class="pxp-single-company-content">
                                <h2>جزئیات پروژه </h2>
                                <p>{{$project->description}}</p>

                                @if($project->photos)
                                <div class="pxp-single-company-gallery mt-3 mt-lg-4">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-3">
                                            <a href="images/company-hero-1.jpg" class="pxp-single-company-gallery-item">
                                                <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                     style="background-image: url(assets/front/images/company-hero-1.jpg);"></div>
                                                <div class="pxp-single-company-gallery-item-caption">کپشن تصویر 1</div>
                                            </a>
                                            <a href="images/company-hero-3.jpg" class="pxp-single-company-gallery-item">
                                                <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                     style="background-image: url(assets/front/images/company-hero-3.jpg);"></div>
                                                <div class="pxp-single-company-gallery-item-caption">کپشن تصویر 2</div>
                                            </a>
                                        </div>
                                        <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-3">
                                            <a href="images/company-hero-2.jpg"
                                               class="pxp-single-company-gallery-item pxp-is-tall">
                                                <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                     style="background-image: url(assets/front/images/company-hero-2.jpg);"></div>
                                                <div class="pxp-single-company-gallery-item-caption">کپشن تصویر 3</div>
                                            </a>
                                        </div>
                                        <div class="col-xl-12 col-xxl-6">
                                            <a href="images/company-hero-4.jpg" class="pxp-single-company-gallery-item">
                                                <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                     style="background-image: url(assets/front/images/company-hero-4.jpg);"></div>
                                                <div class="pxp-single-company-gallery-item-caption">کپشن تصویر 4</div>
                                            </a>
                                            <div class="row">
                                                <div class="col-6">
                                                    <a href="images/company-hero-5.jpg"
                                                       class="pxp-single-company-gallery-item">
                                                        <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                             style="background-image: url(assets/front/images/company-hero-5.jpg);">
                                                        </div>
                                                        <div class="pxp-single-company-gallery-item-caption">کپشن تصویر
                                                            5</div>
                                                    </a>
                                                </div>
                                                <div class="col-6">
                                                    <a href="images/company-hero-3.jpg"
                                                       class="pxp-single-company-gallery-item">
                                                        <div class="pxp-single-company-gallery-item-fig pxp-cover"
                                                             style="background-image: url(assets/front/images/company-hero-3.jpg);">
                                                        </div>
                                                        <div class="pxp-single-company-gallery-item-caption">کپشن تصویر
                                                            6</div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endif

                                {{--<div class="mt-4 mt-lg-5">
                                    <div class="row justify-content-between">
                                        <div class="col-md-6">
                                            <video width="500" height="300" controls="">
                                                <source src="{{asset('assets/front/videos/water.mp4')}}" type="video/mp4">
                                            </video>
                                        </div>
                                        <div class="col-md-5 mt-3 mt-md-0">
                                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده
                                                از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و
                                                سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای
                                                متنوع با </p>
                                        </div>
                                    </div>
                                </div>--}}

                            </div>
                        </section>
                    </div>
                    <div class="col-lg-5 col-xl-4 col-xxl-3">
                        <div class="pxp-single-company-side-panel mt-5 mt-lg-0">
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">دسته بندی</div>
                                <div class="pxp-single-company-side-info-data">{{$project->category->title}}</div>
                            </div>
                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light"> مهارت های مورد نیاز</div>
                                <div class="pxp-single-company-side-info-data">{{$project->required_skills}}</div>
                            </div>

                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">بودجه</div>
                                <div class="pxp-single-company-side-info-data" dir="ltr"> {{number_format($project->budget)}} </div>
                            </div>

                            <div class="mt-4">
                                <div class="pxp-single-company-side-info-label pxp-text-light">پسند ها</div>
                                <div class="pxp-single-company-side-info-data"><span id="likes">57</span> پسندیده شده</div>
                                <button class="btn btn-like" id="red"><i class="fa fa-thumbs-down fa-lg" aria-hidden="true"></i></button>
                                <button class="btn btn-like" id="green"><i class="fa fa-thumbs-up fa-lg" aria-hidden="true"></i></button>
                            </div>
                        </div>

                        <div class="pxp-single-company-side-panel mt-3 mt-lg-4">
                            <h3>تماس با کارفرما</h3>
                            <form class="mt-4">
                                <input type="hidden" id="owner" value="{{$project->owner_id}}">
                                <div class="mb-3">
                                    <label for="contact-company-name" class="form-label">موضوع</label>
                                    <input required type="text" class="form-control" id="contact-company-subject"
                                           placeholder="موضوع  را اینجا بنویسید">
                                </div>
                                <div class="mb-3">
                                    <label for="contact-company-email" class="form-label">ایمیل</label>
                                    <input required type="text" class="form-control" id="contact-company-email"
                                           placeholder="ایمیل خود را اینجا بنویسید">
                                </div>
                                <div class="mb-3">
                                    <label for="contact-company-message" class="form-label">پیام</label>
                                    <textarea required class="form-control" id="contact-company-message"
                                              placeholder="پیام خود را اینجا بنویسید..."></textarea>
                                </div>
                                @guest
                                <a href="{{route('login')}}" class="btn rounded-pill pxp-section-cta d-block">برای ارسال پیام باید وارد شوید</a>
                                @else
                                    <a href="javascript:void(0)" id="send-ticket" class="btn rounded-pill pxp-section-cta d-block">ارسال پیام</a>
                                @endauth
                            </form>
                        </div>

                        <div class="pxp-single-company-side-panel mt-3 mt-lg-4">
                            <h3>ارسال نظر</h3>
                            <form class="mt-4">
                                <div class="mb-3">
                                    <label for="contact-company-message" class="form-label">نظر</label>
                                    <textarea class="form-control" id="contact-company-message"
                                              placeholder="نظر خود را اینجا بنویسید..."></textarea>
                                </div>
                                <a href="#" class="btn rounded-pill pxp-section-cta d-block">ارسال نظر </a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <div class="row d-flex ">
                <div class="col-md-12 col-lg-10 col-xl-8">
                    <div class="">
                        <div class="card-body p-4">
                            <h4 class="text-center mb-4 pb-2">نظرات</h4>
                            <div class="row">
                                <div class="col">
                                    <div class="d-flex flex-start mb-3">
                                        <img class="rounded-circle shadow-1-strong me-3"
                                             src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(32).webp" alt="avatar" width="65"
                                             height="65" />
                                        <div class="flex-grow-1 flex-shrink-1">
                                            <div>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <p class="mb-1">
                                                        رضا رضایی <span class="small">- 2 ساعت پیش</span>
                                                    </p>
                                                </div>
                                                <p class="small mb-0">
                                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="d-flex flex-start mt-5">
                                        <img class="rounded-circle shadow-1-strong me-3"
                                             src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(32).webp" alt="avatar" width="65"
                                             height="65" />
                                        <div class="flex-grow-1 flex-shrink-1">
                                            <div>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <p class="mb-1">
                                                        رضا رضایی <span class="small">- 2 ساعت پیش</span>
                                                    </p>
                                                </div>
                                                <p class="small mb-0">
                                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        var btn1 = document.querySelector('#green');
        var btn2 = document.querySelector('#red');
        var likes = document.querySelector('#likes')

        btn1.addEventListener('click', function() {

            if (btn2.classList.contains('red')) {
                btn2.classList.remove('red');
            }
            this.classList.toggle('green');
            likes.innerHTML = parseInt(likes.innerHTML) + 1

        });

        btn2.addEventListener('click', function() {

            if (btn1.classList.contains('green')) {
                btn1.classList.remove('green');
            }
            this.classList.toggle('red');
            likes.innerHTML = parseInt(likes.innerHTML) - 1

        });


    </script>
    <script>
        $('#send-ticket').on('click',function (){
            let subject = $('#contact-company-subject').val()
            let email=$('#contact-company-email').val()
            let message =$('#contact-company-message').val()
            let owner = $('#owner').val()
            if (subject != "" && email != "" && message != ""){
                $.ajax({
                    headers:{
                        'X-CSRF-TOKEN':'{{csrf_token()}}'
                    },
                    url:'{{route('frontAjax')}}',
                    type:'post',
                    data:{
                        do:'store-company-ticket',
                        subject:subject,
                        owner:owner,
                        email:email,
                        message:message,
                    },
                    success:function (response){
                        $('#send-ticket').addClass('disabled')
                        swal.fire('پیام شما ارسال شد.')
                    },
                    error:function (response){
                        console.log(response)
                    }
                })
            }

        })
    </script>
@endsection
