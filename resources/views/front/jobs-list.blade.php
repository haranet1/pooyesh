@extends('front.master')
@section('title','موقعیت های شغلی')
@section('css')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css"> --}}
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"/> --}}
<link rel="stylesheet" href="{{asset('assets/front/css/filter_multi_select.css')}}"/>
@endsection
@section('main')
<style>
    .autocomplete {
    /*the container must be positioned relative:*/
    position: relative;
    display: inline-block;
    }

    .autocomplete-items {
    position: absolute;
    border: 1px solid #d4d4d4;
    border-bottom: none;
    border-top: none;
    z-index: 10;
    /*position the autocomplete items to be the same width as the container:*/
    top: 100%;
    left: 0;
    right: 0;
    max-height: 300px;
    overflow-y: scroll;
    }
    #titleIdautocomplete-list{
        border-radius: 0px 0px 20px 20px !important;
    }
    .autocomplete-items div {
    padding: 10px;
    cursor: pointer;
    background-color: #fff;
    border-bottom: 1px solid #d4d4d4;
    }
    .autocomplete-items div:hover {
    /*when hovering an item:*/
    background-color: #e9e9e9;
    }
    .autocomplete-active {
    /*when navigating through the items using the arrow keys:*/
    background-color: DodgerBlue !important;
    color: #ffffff;
    }
</style>
    {{-- search section --}}
    <section class="pxp-page-header-simple" style="background-color: var(--pxpMainColorLight);">
        <div class="pxp-container">
            <h1>جستجوی شغل ها</h1>
            <div class="pxp-hero-subtitle pxp-text-light">فرصت شغلی خود را از میان <strong>{{count($jobs)}}</strong>
                شغل پیدا کنید
            </div>
            <div class="pxp-hero-form pxp-hero-form-round pxp-large mt-3 mt-lg-4">
                <form  class="row gx-3 align-items-center" action="{{route('jobs.list.search')}}" method="GET">
                    @csrf
                        <div class="col-12 col-lg ">
                            <div class="input-group mb-3 mb-lg-0">
                                <span class="input-group-text"><span class="fa fa-folder-o"></span></span>
                                <select onchange="UpdateVars()" id="jobCat" required name="category" class="form-select">
                                    <option value="0" selected>گروه شغلی</option>
                                    @foreach($job_cats as $category)
                                        @if (isset($_GET["category"]) && $_GET["category"] == $category->id)
                                            <option selected value="{{$category->id}}">{{$category->name}}</option>
                                        @else
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-lg pxp-has-left-border">
                            <div class="input-group mb-3 mb-lg-0">
                                <span class="input-group-text"><span class="fa fa-search"></span></span>
                                <input required id="titleId" name="title" type="text" class="form-control" 
                                @if (isset($_GET['title']))
                                    value="{{$_GET['title']}}"
                                @endif
                                placeholder="عنوان شغل">
                            </div>
                        </div>
                        
                        <div class="col-12 col-lg pxp-has-left-border">
                            <div class="input-group mb-3 mb-lg-0">
                                <span class="input-group-text"><span class="fa fa-globe"></span></span>
                                <select onchange="UpdateVars()" id="provinceId" name="province" class="form-select">
                                    <option value="0" selected>انتخاب استان</option>
                                    @foreach($provinces as $province)
                                        @if (isset($_GET['province']) && $_GET['province'] == $province->id)
                                            <option selected value="{{$province->id}}">{{$province->name}}</option>
                                        @else
                                            <option value="{{$province->id}}">{{$province->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                </form>
            </div>
            <div class="pxp-hero-form-filter mt-3 mt-lg-4 pxp-has-border">
                <div class="row justify-content-start">
                    {{-- نوع همکاری --}}
                    <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0 ">
                            <select onchange="UpdateVars()" id="jobTypeId" class="form-select bg-white">
                                <option selected value="0">نوع همکاری</option>
                                <option value="full-time">تمام وقت</option>
                                <option value="part-time">پاره وقت</option>
                                <option value="remote">دورکاری</option>
                            </select>
                        </div>
                    </div>
                    {{-- تجربه --}}
                    {{-- <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select class="form-select bg-white">
                                <option selected> سطح تجربه</option>
                                <option value="1">بدون تجربه</option>
                                <option value="2">سطح ورودی</option>
                                <option value="3">سطح میانی</option>
                                <option value="4">سطح ارشد</option>
                                <option value="5">مدیر / اجرایی</option>
                            </select>
                        </div>
                    </div> --}}
                    {{-- جنسیت --}}
                    <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select onchange="UpdateVars()" id="sexId" class="form-select bg-white">
                                <option selected value="0">جنسیت</option>
                                <option value="مرد">مرد</option>
                                <option value="زن">زن</option>
                            </select>
                        </div>
                    </div>
                    {{-- تحصیلات --}}
                    <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select onchange="UpdateVars()" id="academicLevelId" class="form-select bg-white">
                                <option selected value="0">تحصیلات</option>
                                <option value="دکترا">دکترا</option>
                                <option value="کارشناسی ارشد">کارشناسی ارشد</option>
                                <option value="کارشناسی">کارشناسی</option>
                                <option value="کاردانی">کاردانی</option>
                                <option value="دیپلم">دیپلم</option>
                            </select>
                        </div>
                    </div>
                    {{-- تاهل --}}
                    <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select onchange="UpdateVars()" id="mariageTypeId" class="form-select bg-white">
                                <option selected value="0">وضعیت تاهل</option>
                                <option value="مجرد">مجرد</option>
                                <option value="متاهل">متاهل</option>
                            </select>
                        </div>
                    </div>
                    {{-- سابقه ی کاری --}}
                    <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select onchange="UpdateVars()" id="workExperienceId" class="form-select bg-white">
                                <option selected value="0">سابقه کاری</option>
                                <option value="2">تا 2 سال</option>
                                <option value="5">3 تا 5 سال</option>
                                <option value="10">6 تا 10 سال</option>
                                <option value="11">بالای 10 سال</option>
                            </select>
                        </div>
                    </div>
                    {{-- شهر --}}
                    <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select onchange="UpdateVars()" id="cityId" class="form-select bg-white">
                                <option selected value="0">شهر</option>
                            </select>
                        </div>
                    </div>
                    {{-- مهارت ها --}}
                    {{-- <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select onchange="UpdateVars()" id="skillsId" class="form-select bg-white ">
                                <option selected value="0">مهارت ها</option>
                                <option>Ketchup</option>
                                <option>Relish</option>
                            </select>
                        </div>
                    </div> --}}
                    {{-- تخصص ها --}}
                    {{-- <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select onchange="UpdateVars()" id="expertiseId" class="form-select bg-white ">
                                <option selected value="0">تخصص ها</option>
                                <option>Ketchup</option>
                                <option>Relish</option>
                            </select>
                        </div>
                    </div> --}}
                    {{-- تناسب براساس تست شخصیت --}}
                    {{-- <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select onchange="UpdateVars()" id="personalityTestId" class="form-select bg-white ">
                                <option selected value="0">تناسب براساس تست شخصیت</option>
                                <option>Ketchup</option>
                                <option>Relish</option>
                            </select>
                        </div>
                    </div> --}}
                    {{-- سن --}}
                    <div class="col-12 col-sm-auto">
                        <div class="mb-3 mb-lg-0">
                            <select onchange="UpdateVars()" id="ageId" class="form-select bg-white ">
                                <option selected value="0">رنج سنی</option>
                                <option value="20">... تا 20 سال</option>
                                <option value="30">21 تا 30 سال</option>
                                <option value="40">31 تا 40 سال</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    {{-- show section --}}
    <section class="mt-100">
        <div class="pxp-container">
            <div class="row">
                {{-- <div class="col-lg-5 col-xl-4 col-xxl-3">
                    <div class="pxp-jobs-list-side-filter">
                        <div class="pxp-list-side-filter-header d-flex d-lg-none">
                            <div class="pxp-list-side-filter-header-label">فیلتر مشاغل</div>
                            <a role="button"><span class="fa fa-sliders"></span></a>
                        </div>
                        <div class="mt-4 mt-lg-0 d-lg-block pxp-list-side-filter-panel">
                            <form action="{{route('jobs.list.by-experience-type')}}" method="post">
                                @csrf
                            <h3>نوع اشتغال</h3>
                            <div class="list-group mt-2 mt-lg-3">
                                <label
                                    class="list-group-item d-flex justify-content-between align-items-center pxp-checked">
                                    <span class="d-flex">
                                        <input name="type[]" class="form-check-input me-2" type="checkbox" value="full-time" checked>
                                        تمام وقت
                                    </span>
                                </label>
                                <label
                                    class="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                    <span class="d-flex">
                                        <input name="type[]" class="form-check-input me-2" type="checkbox" value="part-time">
                                        پاره وقت
                                    </span>
                                   <span class="badge rounded-pill">34</span>
                                </label>
                                <label
                                    class="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                    <span class="d-flex">
                                        <input name="type[]" class="form-check-input me-2" type="checkbox" value="remote">
                                        دور کاری
                                    </span>
                                   <span class="badge rounded-pill">24</span>
                                </label>
                                <label
                                    class="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                    <span class="d-flex">
                                        <input name="type[]" class="form-check-input me-2" type="checkbox" value="intern">
                                        کارآموزی
                                    </span>
                                   <span class="badge rounded-pill">27</span>
                                </label>
                            </div>

                            <h3 class="mt-3 mt-lg-4"> سطح تجربه</h3>
                            <div class="list-group mt-2 mt-lg-3">
                                <label class="list-group-item d-flex justify-content-between align-items-center">
                                    <span class="d-flex">
                                        <input name="experience[]" class="form-check-input me-2" type="checkbox" value="junior">
                                        تازه کار
                                    </span>
                                   <span class="badge rounded-pill">98</span>
                                </label>
                                <label
                                    class="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3 pxp-checked">
                                    <span class="d-flex">
                                        <input name="experience[]" class="form-check-input me-2" type="checkbox" value="mid-level" checked>
                                        سطح متوسط
                                    </span>
                                   <span class="badge rounded-pill">44</span>
                                </label>
                                <label
                                    class="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3 pxp-checked">
                                    <span class="d-flex">
                                        <input name="experience[]" class="form-check-input me-2" type="checkbox" value="senior" checked>
                                        سطح ارشد
                                    </span>
                                   <span class="badge rounded-pill">35</span>--}}
                                {{-- </label>

                            </div>

                            <div class="list-group mt-2 mt-lg-3">
                                <button type="submit" class="btn btn-primary rounded-pill">جستجو</button>
                            </div>
                            </form>

                        </div>
                    </div>
                </div> --}}
                <div class="col-lg-7 col-xl-8 col-xxl-12">
                    {{-- <div class="pxp-jobs-list-top mt-4 mt-lg-0">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <h2><span class="pxp-text-light">نمایش</span> {{count($jobs)}} <span
                                        class="pxp-text-light">شغل</span></h2>
                            </div>
                            <div class="col-auto">
                               {{-- <select class="form-select">
                                    <option value="0" selected> مرتبط ترین</option>
                                    <option value="1">جدیدترین</option>
                                    <option value="2">قدیمی ترین</option>
                                </select>--}}
                            {{-- </div>
                        </div>
                    </div> --}}

                    <div id="rowId" class="row">
                        
                        @if(count($jobs)> 0)
                            @foreach($jobs as $job)
                            <div class="col-xxl-4 pxp-jobs-card-2-container">
                                <div class="pxp-jobs-card-2 pxp-has-border">
                                    <div class="pxp-jobs-card-2-top">
                                        @if (!is_null($job->company->groupable->logo))
                                            <a href="{{route('company.single',$job->company_id)}}" class="pxp-jobs-card-2-company-logo"
                                            style="background-image: url({{$job->company->groupable->logo->path}});"></a>
                                        @else
                                            <a href="{{route('company.single',$job->company_id)}}" class="pxp-jobs-card-2-company-logo"
                                            style="background-image: url({{asset('assets/images/default-logo.svg')}});"></a>
                                        @endif
                                        <div class="pxp-jobs-card-2-info">
                                            <a href="{{route('job',$job->id)}}" class="pxp-jobs-card-2-title">{{$job->job->name}}</a>
                                            <div class="pxp-jobs-card-2-details">
                                                @if ($job->province && $job->city)
                                                    <span class="pxp-jobs-card-2-location">
                                                        <span class="fa fa-globe"></span>{{$job->province->name}}، {{$job->city->name}} 
                                                    </span>
                                                @endif
                                                <div class="pxp-jobs-card-2-type">{{job_type_persian($job->type)}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pxp-jobs-card-2-bottom">
                                        <span  class="pxp-jobs-card-2-category">
                                            <div class="pxp-jobs-card-2-category-label">{{$job->job->category->name}}</div>
                                        </span>
                                        <div class="pxp-jobs-card-2-bottom-right">
                                            <span class="pxp-jobs-card-2-date pxp-text-light">{{verta($job->created_at)->formatDifference()}} توسط</span> <a
                                                href="{{route('company.single',$job->company->id)}}" class="pxp-jobs-card-2-company">{{$job->company->groupable->name}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-xxl-6 pxp-jobs-card-2-container">
                                <div class="pxp-jobs-card-2-top">
                                    <a href="${coroute}" class="pxp-jobs-card-2-company-logo"
                                        style="background-image: url(`+ logoPath +`);"></a>
                                    <div class="pxp-jobs-card-2-info">
                                        <a href="{{route('job.single',$job->id)}}" class="pxp-jobs-card-2-title">{{$job->title}}</a>
                                        <div class="pxp-jobs-card-2-details">
                                            <a href="jobs-list-1.html" class="pxp-jobs-card-2-location">
                                                <span class="fa fa-globe"></span>`+ item['province']['name'] +` ، `+ item['city']['name'] +`
                                            </a>
                                            <div class="pxp-jobs-card-2-type">{{job_type_persian($job->type)}}</div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="pxp-jobs-card-2-bottom">
                                    <a href="{{route('jobs.list.by-category',$job->category->name)}}" class="pxp-jobs-card-2-category">
                                        <div class="pxp-jobs-card-2-category-label">{{$job->category->name}}</div>
                                    </a>
                                    <div class="pxp-jobs-card-2-bottom-right">
                                        <span class="pxp-jobs-card-2-date pxp-text-light">{{verta($job->created_at)->formatDifference()}} توسط </span><a
                                            href="{{route('company.single',$job->company->id)}}" class="pxp-jobs-card-2-company">`+ item['company']['groupable']['name'] +`</a>
                                    </div>
                                </div>
                            </div> --}}
                                
                                
                                {{-- <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div class="pxp-jobs-card-1 pxp-has-border">
                                        <div class="pxp-jobs-card-1-top">
                                            <a href="{{route('jobs.list.by-category',$job->category->name)}}"
                                               class="pxp-jobs-card-1-category">
                                                <div class="pxp-jobs-card-1-category-icon"><span
                                                        class="fa fa-bullhorn"></span>
                                                </div>
                                                <div
                                                    class="pxp-jobs-card-1-category-label">{{$job->category->name}}</div>
                                            </a>
                                            <a href="{{route('job.single',$job->id)}}"
                                               class="pxp-jobs-card-1-title">{{$job->title}}</a>
                                            <div class="pxp-jobs-card-1-details">
                                                <a href="{{route('job.single',$job->id)}}"
                                                   class="pxp-jobs-card-1-location">
                                                    <span class="fa fa-globe"></span>{{$job->location}}
                                                </a>
                                                <div class="pxp-jobs-card-1-type">
                                                    <a href="{{route('jobs.list.by-type',$job->type)}}">{{job_type_persian($job->type)}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pxp-jobs-card-1-bottom">
                                            <div class="pxp-jobs-card-1-bottom-left">
                                                <div class="pxp-jobs-card-1-date pxp-text-light">{{verta($job->created_at)->formatDifference()}} توسط</div>
                                                <a href="{{route('company.single',$job->company->id)}}" class="pxp-jobs-card-1-company">{{$job->company->groupable->name}}</a>
                                            </div>
                                            <a href="{{route('company.single',$job->company->id)}}" class="pxp-jobs-card-1-company-logo"
                                               style="background-image: url(images/company-logo-1.png);"></a>
                                        </div>
                                    </div>
                                </div> --}}
                            @endforeach
                        @else
                            <div class="pxp-jobs-card-3 pxp-has-border">
                                <div class="row align-items-center text-center justify-content-between">
                                    <h4>{{__('public.no_info')}}</h4>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="row mt-4 mt-lg-5 justify-content-between align-items-center">
                        <div class="col-auto">
                            <nav class="mt-3 mt-sm-0" aria-label="Jobs list pagination">
                               {{$jobs->links('pagination.front')}}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        const url = "{{route('jobs.ajax')}}"; 
        const csrf = "{{csrf_token()}}";
        const companyRoute = "{{route('company.single',':id')}}";
        const jobRoute = "{{route('job.single',':id')}}"
    </script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js"></script> --}}
    <script src="{{asset('assets/front/js/filter-multi-select-bundle.min.js')}}"></script>
    {{-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> --}}
    <script src="{{asset('assets/front/js/jobs-search.js')}}"></script>

@endsection
