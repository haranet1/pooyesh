<footer class="pxp-main-footer d-print-none">
    <div class="pxp-main-footer-top pt-4 pb-2" style="background-color: var(--pxpMainColorLight);">
        <div class="pxp-container">
            <div class="row">
                <div class="col-lg-6 col-xl-5 col-xxl-4 mb-4">
                    <div class="pxp-footer-logo">
                        <a href="{{route('home')}}" class="pxp-animate"><span
                                style="color: var(--pxpMainColor)"> فن  </span>یار</a>
                    </div>
                    <div class="pxp-footer-section mt-3 mt-md-4">
                        <h3>با ما تماس بگیرید</h3>
                        <div class="pxp-footer-phone" dir="ltr">031 4200 3305 </div>
                    </div>
                    <div class="mt-3 mt-md-4 pxp-footer-section">
                        <div class="pxp-footer-text">
                            اصفهان، نجف آباد، بلوار دانشگاه، <br>
                            دانشگاه آزاد اسلامی واحد نجف آباد، پارک علم و فناوری، اتاق 205 <br>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-7 col-xxl-8">
                    <div class="row">
                        <div class="col-md-6 col-xl-4 col-xxl-3 mb-4">
                            <div class="pxp-footer-section">
                                <h3>برای کارجویان</h3>
                                <ul class="pxp-footer-list">
                                    <li><a href="{{route('jobs.list')}}">جستجوی شغل</a></li>
                                    <li><a href="{{route('companies.list')}}">لیست کارفرمایان</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4 col-xxl-3 mb-4">
                            <div class="pxp-footer-section">
                                <h3>برای کارفرمایان</h3>
                                <ul class="pxp-footer-list">
                                    <li><a href="{{route('candidates.list')}}">کارجویان</a></li>
                                    {{-- <li><a href="#">پروژه ها</a></li> --}}
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4 col-xxl-3 mb-4">
                            <div class="pxp-footer-section">
                                <h3>درباره ما</h3>
                                <ul class="pxp-footer-list">
                                    <li><a href="{{route('about')}}">درباره ما</a></li>
{{--                                    <li><a href="{{route('pricing')}}">قیمت</a></li>--}}
                                    <li><a href="{{route('blog')}}">بلاگ</a></li>
                                    <li><a href="{{route('contact')}}">تماس با ما</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4 col-xxl-3 mb-4">
                            <div class="pxp-footer-section">
                                <h3>منابع مفید</h3>
                                <ul class="pxp-footer-list">
                                    <li><a href="{{route('faqs')}}">سوالات متداول</a></li>
{{--                                    <li><a href="{{route('login')}}">ورود</a></li>--}}
{{--                                    <li><a href="{{route('register')}}">ثبت نام</a></li>--}}

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="py-3" style="background-color: var(--pxpSecondaryColor);">
        <div class="pxp-container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-auto">
                    <div class="pxp-footer-copyright pxp-text-light">© تمامی حقوق مادی و معنوی برای موسسه کاریابی اینترنتی فن‌یار محفوظ است.</div>
                </div>
                <div class="col-lg-auto">
                    <div class="pxp-footer-social mt-3 mt-lg-0">
                        <ul class="list-unstyled">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="pxp-footer-copyright pxp-text-light text-center w-100">طراحی و اجرای سامانه: <a href="https://haranet.ir" target="_blank">شرکت آرتین نوآفرین حرا(حرانت)</a></div>

            </div>
        </div>
    </div>
</footer>
