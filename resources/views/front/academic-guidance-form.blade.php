@extends('front.master')
@section('title','هدایت تحصیلی')
@section('main')
    <section class="mt-100 pxp-no-hero">
        <div class="pxp-container justify-content-center">
            <h2>هدایت تحصیلی ویژه داوطلبان کنکور</h2>
            <p class="pxp-text-light">لطفا اطلاعات زیر را با دقت تکمیل و در انتها ثبت کنید.</p>

            <form method="POST" action="{{route('academic-guidance.store')}}">
                @csrf
                <div class="row mt-4 mt-lg-5">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="national_code" class="form-label">کد ملی</label>
                            <input type="text" name="national_code"  class="form-control" placeholder="کد ملی">
                            @error('national_code')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="name" class="form-label">نام</label>
                            <input type="text" name="name"  class="form-control" placeholder="نام">
                            @error('name')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="family" class="form-label">نام خانوادگی</label>
                            <input type="text" name="family" class="form-control" placeholder="نام خانوادگی">
                            @error('family')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="mobile" class="form-label">تلفن همراه</label>
                            <input type="text" name="mobile" class="form-control" placeholder="تلفن همراه">
                            @error('mobile')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="parent_mobile" class="form-label">تلفن همراه پدر یا مادر</label>
                            <input type="text" name="parent_mobile" class="form-control" placeholder="تلفن همراه پدر یا مادر">
                            @error('parent_mobile')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="grade" class="form-label">پایه تحصیلی</label>
                            <select class="form-control form-select" name="grade" >
                                <option value="پایه 9">پایه 9</option>
                                <option value="پایه 10">پایه 10</option>
                                <option value="پایه 11">پایه 11</option>
                                <option value="پایه 12">پایه 12</option>
                            </select>
                            @error('grade')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="edu_branch" class="form-label">شاخه تحصیلی</label>
                            <select class="form-control form-select" name="edu_branch" >
                                <option value="نظری">نظری</option>
                                <option value="فنی و حرفه ای">فنی و حرفه ای</option>
                                <option value="کار دانش">کار دانش</option>
                            </select>
                            @error('edu_branch')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="gender" class="form-label">جنسیت</label>
                            <select class="form-control form-select" name="gender" >
                                <option value="مرد">مرد</option>
                                <option value="زن">زن</option>
                            </select>
                            @error('gender')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="school_id" class="form-label">نام مدرسه</label>
                            <input type="text" name="school" class="form-control" placeholder="نام مدرسه">
                            @error('school')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="suggestion" class="form-label">پیشنهادات</label>
                            <textarea class="form-control" name="suggestion"  cols="30" rows="10"></textarea>
                            @error('suggestion')<span class="text-danger">{{$message}}</span>@enderror
                        </div>
                    </div>
                </div>

                <div class="mt-4 mt-lg-5">
                    <button type="submit" class="btn rounded-pill pxp-section-cta">ثبت اطلاعات</button>
                </div>
            </form>
        </div>
    </section>
@endsection
