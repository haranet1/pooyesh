@extends('front.master')
@section('title','مقاله')
@section('main')
    <section>
        <div class="pxp-container">
            <div class="pxp-blog-hero">
                <div class="row justify-content-between align-items-end">
                    <div class="col-lg-8 col-xxl-6">
                        <h1>چگونه شروع به جستجوی شغل جدید کنیم</h1>
                        <div class="pxp-hero-subtitle pxp-text-light">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                            صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و
                            سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود
                            .</div>
                    </div>
                    <div class="col-lg-4 col-xxl-6">
                        <div class="text-start text-lg-end mt-4 mt-lg-0">
                            <div class="pxp-single-blog-top-category">
                                ارسال شده در <a href="#">آینده کاری</a>
                            </div>
                            <div class="pxp-single-blog-top-author">
                                توسط <a href="#">علی رجبلو</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <img class="pxp-single-blog-featured-img" src="{{asset('assets/front/images/single-blog-post.jpg')}}"
                 alt="How to Start Looking for a New Job">
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <div class="row justify-content-center">
                <div class="col-xl-7">
                    <div class="pxp-single-blog-content">
                        <h2>بگذارید شغل عالی شما را پیدا کند</h2>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که </p>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی
                            تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی
                            در شصت و سه </p>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                            چاپگرها و .</p>
                        <h2 class="mt-4 mt-lg-5">رزومه خود را درست انجام دهید</h2>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی
                            تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی
                            در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با </p>
                        <blockquote class="blockquote-style-one mb-5 mt-5">
                            <p>"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک
                                است، چاپگرها و متون بلکه .</p>
                            <cite>صادق محرمی</cite>
                        </blockquote>
                        <img src="{{asset('assets/front/images/single-blog-post-1.jpg')}}" alt="Get Your Resume Done Right" class="mt-4 mt-lg-5">
                        <h2 class="mt-4 mt-lg-5">رزومه خود را درست انجام دهید</h2>
                        <ul>
                            <li>اما با بیش از 5 میلیون شغل در Jobster</li>
                            <li>نکات و ترفندهای ما را در زیر دنبال کنید تا به شما کمک کند بهتر پیدا کنید</li>
                            <li>رزومه شما کلید شروع یک جستجوی شغلی قوی است</li>
                            <li>اجازه دهید کارشناسان Jobster رزومه شما را ایجاد کنند</li>
                        </ul>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که </p>
                    </div>

                    <div class="mt-100">
                        <div class="pxp-single-blog-share">
                            <span class="me-4">این مقاله را به اشتراک بگذارید</span>
                            <ul class="list-unstyled">
                                <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="mt-100">
                        <div class="pxp-blog-comments-block">
                            <div class="pxp-blog-post-comments">
                                <h4>2 نظر</h4>
                                <div class="mt-3 mt-lg-4">
                                    <ol class="pxp-comments-list">
                                        <li>
                                            <div class="pxp-comments-list-item">
                                                <img src="{{asset('assets/front/images/avatar-1.jpg')}}" alt="سمانه مجبی">
                                                <div class="pxp-comments-list-item-body">
                                                    <h5>سمانه مجبی</h5>
                                                    <div class="pxp-comments-list-item-date">تیر 6, 1400 در 9:10 ق.ظ
                                                    </div>
                                                    <div class="pxp-comments-list-item-content mt-2">لورم ایپسوم متن
                                                        ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از
                                                        طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و
                                                        سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
                                                        کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای
                                                        زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و
                                                        متخصصان .</div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mt-3 mt-lg-4">
                                            <div class="pxp-comments-list-item">
                                                <img src="{{asset('assets/front/images/avatar-2.jpg')}}" alt="علی رجبلو">
                                                <div class="pxp-comments-list-item-body">
                                                    <h5>علی رجبلو</h5>
                                                    <div class="pxp-comments-list-item-date">تیر 6, 1400 در 9:10 ق.ظ
                                                    </div>
                                                    <div class="pxp-comments-list-item-content mt-2">لورم ایپسوم متن
                                                        ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از
                                                        طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و
                                                        سطرآنچنان که لازم است، و .</div>
                                                </div>
                                            </div>
                                        </li>
                                    </ol>
                                </div>
                                <div class="mt-3 mt-lg-4">
                                    <h4>پاسخ بگذارید</h4>
                                    <form class="pxp-comments-form">
                                        <div class="pxp-comments-logged-as">
                                            وارد شده با حساب سمانه مجبی. <a href="#">خروج؟</a>
                                        </div>
                                        <div class="mt-3 mt-lg- mb-3">
                                            <label for="pxp-comments-comment" class="form-label">نظر</label>
                                            <textarea class="form-control" id="pxp-comments-comment"
                                                      placeholder="نظر خود را اینجا تایپ کنید..."></textarea>
                                        </div>
                                        <button class="btn rounded-pill pxp-section-cta">ارسال نظر</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-subsection-h2">مقالات مرتبط</h2>
            <p class="pxp-text-light"> آخرین توصیه های شغلی را مرور کنید</p>

            <div class="row mt-3 mt-md-4">
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container">
                    <div class="pxp-posts-card-1 pxp-has-border">
                        <div class="pxp-posts-card-1-top">
                            <div class="pxp-posts-card-1-top-bg">
                                <div class="pxp-posts-card-1-image pxp-cover"
                                     style="background-image: url(assets/front/images/post-card-1.jpg);"></div>
                                <div class="pxp-posts-card-1-info">
                                    <div class="pxp-posts-card-1-date">31 فروردین 1400</div>
                                    <a href="{{route('blog')}}" class="pxp-posts-card-1-category"> ارزیابی ها</a>
                                </div>
                            </div>
                            <div class="pxp-posts-card-1-content">
                                <a href="{{route('article')}}" class="pxp-posts-card-1-title">10 خود ارزیابی رایگان
                                    حرفه ای</a>
                                <div class="pxp-posts-card-1-summary pxp-text-light"> فهمیدن اینکه وقتی بزرگ شدی
                                    می‌خواهی چه کاری بشوی سخت است، اما یک آزمون شغلی می‌تواند پیدا کردن آن را آسان‌تر
                                    کند...</div>
                            </div>
                        </div>
                        <div class="pxp-posts-card-1-bottom">
                            <div class="pxp-posts-card-1-cta">
                                <a href="{{route('article')}}">بیشتر بخوانید<span class="fa fa-angle-left"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container">
                    <div class="pxp-posts-card-1 pxp-has-border">
                        <div class="pxp-posts-card-1-top">
                            <div class="pxp-posts-card-1-top-bg">
                                <div class="pxp-posts-card-1-image pxp-cover"
                                     style="background-image: url(assets/front/images/post-card-2.jpg);"></div>
                                <div class="pxp-posts-card-1-info">
                                    <div class="pxp-posts-card-1-date">۵ اردیبهشت 1400</div>
                                    <a href="{{route('blog')}}" class="pxp-posts-card-1-category">شغل ها</a>
                                </div>
                            </div>
                            <div class="pxp-posts-card-1-content">
                                <a href="{{route('article')}}" class="pxp-posts-card-1-title">چگونه شروع به جستجوی شغل
                                    کنیم</a>
                                <div class="pxp-posts-card-1-summary pxp-text-light"> رزومه شما عالی است. این بهینه شده
                                    برای کلمات کلیدی، مشخص شده در صنعت، پر از دستاوردها، با پشتوانه داده ها...</div>
                            </div>
                        </div>
                        <div class="pxp-posts-card-1-bottom">
                            <div class="pxp-posts-card-1-cta">
                                <a href="{{route('article')}}">بیشتر بخوانید<span class="fa fa-angle-left"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container">
                    <div class="pxp-posts-card-1 pxp-has-border">
                        <div class="pxp-posts-card-1-top">
                            <div class="pxp-posts-card-1-top-bg">
                                <div class="pxp-posts-card-1-image pxp-cover"
                                     style="background-image: url(assets/front/images/post-card-3.jpg);"></div>
                                <div class="pxp-posts-card-1-info">
                                    <div class="pxp-posts-card-1-date"> 10 اردیبهشت 1400</div>
                                    <a href="{{route('blog')}}" class="pxp-posts-card-1-category">رزومه</a>
                                </div>
                            </div>
                            <div class="pxp-posts-card-1-content">
                                <a href="{{route('article')}}" class="pxp-posts-card-1-title">نمونه های رزومه</a>
                                <div class="pxp-posts-card-1-summary pxp-text-light"> برای نوشتن رزومه به کمک نیاز
                                    دارید؟ به دنبال نمونه های رزومه برای صنایع خاص هستید؟ تنوع پیدا خواهید کرد...</div>
                            </div>
                        </div>
                        <div class="pxp-posts-card-1-bottom">
                            <div class="pxp-posts-card-1-cta">
                                <a href="{{route('article')}}">بیشتر بخوانید<span class="fa fa-angle-left"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container">
                    <div class="pxp-posts-card-1 pxp-has-border">
                        <div class="pxp-posts-card-1-top">
                            <div class="pxp-posts-card-1-top-bg">
                                <div class="pxp-posts-card-1-image pxp-cover"
                                     style="background-image: url(assets/front/images/post-card-4.jpg);"></div>
                                <div class="pxp-posts-card-1-info">
                                    <div class="pxp-posts-card-1-date"> 15 اردیبهشت 1400</div>
                                    <a href="{{route('blog')}}" class="pxp-posts-card-1-category">مصاحبه</a>
                                </div>
                            </div>
                            <div class="pxp-posts-card-1-content">
                                <a href="{{route('article')}}" class="pxp-posts-card-1-title"> 100 سوال برتر مصاحبه -
                                    آماده باشید</a>
                                <div class="pxp-posts-card-1-summary pxp-text-light"> در حالی که به تعداد مصاحبه
                                    کنندگان، سؤالات مختلف ممکن برای مصاحبه وجود دارد، همیشه کمک می کند...</div>
                            </div>
                        </div>
                        <div class="pxp-posts-card-1-bottom">
                            <div class="pxp-posts-card-1-cta">
                                <a href="{{route('article')}}">بیشتر بخوانید<span class="fa fa-angle-left"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
