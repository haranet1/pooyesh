@extends('front.master')
@section('title','بلاگ')
@section('main')
    <section>
        <div class="pxp-container">
            <div class="pxp-blog-hero">
                <h1> مشاوره شغلی برتر</h1>
                <div class="pxp-hero-subtitle pxp-text-light"> آخرین توصیه های شغلی را مرور کنید</div>
            </div>

            <div id="pxp-blog-featured-posts-carousel"
                 class="carousel slide carousel-fade pxp-blog-featured-posts-carousel" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="pxp-featured-posts-item pxp-cover"
                             style="background-image: url(assets/front/images/company-hero-5.jpg);">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-featured-posts-item-caption">
                                <div class="pxp-featured-posts-item-caption-content">
                                    <div class="row align-content-center justify-content-center">
                                        <div class="col-9 col-md-8 col-xl-7 col-xxl-6">
                                            <div class="pxp-featured-posts-item-date">31 فروردین 1400</div>
                                            <div class="pxp-featured-posts-item-title"> 10 خود ارزیابی رایگان حرفه ای
                                            </div>
                                            <div class="pxp-featured-posts-item-summary pxp-text-light mt-2"> فهمیدن
                                                اینکه وقتی بزرگ شوید چه کاره می خواهید شوید، سخت است، اما یک آزمون شغلی
                                                می تواند پیدا کردن آن را آسان تر کند...</div>
                                            <div class="mt-4 mt-md-5 text-center">
                                                <a href="{{route('article')}}"
                                                   class="btn rounded-pill pxp-section-cta"> مقاله را بخوانید<span
                                                        class="fa fa-angle-left"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="pxp-featured-posts-item pxp-cover"
                             style="background-image: url(assets/front/images/company-hero-3.jpg);">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-featured-posts-item-caption">
                                <div class="pxp-featured-posts-item-caption-content">
                                    <div class="row align-content-center justify-content-center">
                                        <div class="col-9 col-md-8 col-xl-7 col-xxl-6">
                                            <div class="pxp-featured-posts-item-date">۵ اردیبهشت 1400</div>
                                            <div class="pxp-featured-posts-item-title"> چگونه شروع به جستجوی کار کنیم
                                            </div>
                                            <div class="pxp-featured-posts-item-summary pxp-text-light mt-2"> رزومه شما
                                                عالی است. این بهینه شده برای کلمات کلیدی، مشخص شده در صنعت، پر از
                                                دستاوردها، با پشتوانه داده ها...</div>
                                            <div class="mt-4 mt-md-5 text-center">
                                                <a href="{{route('article')}}"
                                                   class="btn rounded-pill pxp-section-cta"> مقاله را بخوانید<span
                                                        class="fa fa-angle-left"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="pxp-featured-posts-item pxp-cover"
                             style="background-image: url(assets/front/images/company-hero-1.jpg);">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-featured-posts-item-caption">
                                <div class="pxp-featured-posts-item-caption-content">
                                    <div class="row align-content-center justify-content-center">
                                        <div class="col-9 col-md-8 col-xl-7 col-xxl-6">
                                            <div class="pxp-featured-posts-item-date"> 10 اردیبهشت 1400</div>
                                            <div class="pxp-featured-posts-item-title"> نمونه های رزومه</div>
                                            <div class="pxp-featured-posts-item-summary pxp-text-light mt-2"> برای نوشتن
                                                رزومه به کمک نیاز دارید؟ به دنبال نمونه های رزومه برای صنایع خاص هستید؟
                                                انواع مختلفی پیدا خواهید کرد...</div>
                                            <div class="mt-4 mt-md-5 text-center">
                                                <a href="{{route('article')}}"
                                                   class="btn rounded-pill pxp-section-cta"> مقاله را بخوانید<span
                                                        class="fa fa-angle-left"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="pxp-featured-posts-item pxp-cover"
                             style="background-image: url(assets/front/images/company-hero-2.jpg);">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-featured-posts-item-caption">
                                <div class="pxp-featured-posts-item-caption-content">
                                    <div class="row align-content-center justify-content-center">
                                        <div class="col-9 col-md-8 col-xl-7 col-xxl-6">
                                            <div class="pxp-featured-posts-item-date"> 15 اردیبهشت 1400</div>
                                            <div class="pxp-featured-posts-item-title"> 100 سوال برتر مصاحبه - آماده
                                                باشید</div>
                                            <div class="pxp-featured-posts-item-summary pxp-text-light mt-2">در حالی که
                                                به تعداد مصاحبه کنندگان، سؤالات مختلف ممکن برای مصاحبه وجود دارد، همیشه
                                                کمک می کند...</div>
                                            <div class="mt-4 mt-md-5 text-center">
                                                <a href="{{route('article')}}"
                                                   class="btn rounded-pill pxp-section-cta"> مقاله را بخوانید<span
                                                        class="fa fa-angle-left"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#pxp-blog-featured-posts-carousel"
                        data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">قبلی</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#pxp-blog-featured-posts-carousel"
                        data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">بعدی</span>
                </button>
            </div>

            <div class="mt-4 mt-lg-5">
                <div class="row">
                    <div class="col-lg-7 col-xl-8 col-xxl-9">
                        <div class="row">
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                <div class="pxp-posts-card-1 pxp-has-border">
                                    <div class="pxp-posts-card-1-top">
                                        <div class="pxp-posts-card-1-top-bg">
                                            <div class="pxp-posts-card-1-image pxp-cover"
                                                 style="background-image: url(assets/front/images/post-card-1.jpg);"></div>
                                            <div class="pxp-posts-card-1-info">
                                                <div class="pxp-posts-card-1-date">31 فروردین 1400</div>
                                                <a href="blog-list-1.html" class="pxp-posts-card-1-category"> ارزیابی
                                                    ها</a>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-content">
                                            <a href="{{route('article')}}" class="pxp-posts-card-1-title"> 10 خود
                                                ارزیابی رایگان حرفه ای</a>
                                            <div class="pxp-posts-card-1-summary pxp-text-light"> فهمیدن
                                                اینکه وقتی بزرگ شوید چه کاره می خواهید شوید، سخت است، اما یک آزمون شغلی
                                                می تواند پیدا کردن آن را آسان تر کند...</div>
                                        </div>
                                    </div>
                                    <div class="pxp-posts-card-1-bottom">
                                        <div class="pxp-posts-card-1-cta">
                                            <a href="{{route('article')}}">بیشتر بخوانید<span
                                                    class="fa fa-angle-left"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                <div class="pxp-posts-card-1 pxp-has-border">
                                    <div class="pxp-posts-card-1-top">
                                        <div class="pxp-posts-card-1-top-bg">
                                            <div class="pxp-posts-card-1-image pxp-cover"
                                                 style="background-image: url(assets/front/images/post-card-2.jpg);"></div>
                                            <div class="pxp-posts-card-1-info">
                                                <div class="pxp-posts-card-1-date">۵ اردیبهشت 1400</div>
                                                <a href="blog-list-1.html" class="pxp-posts-card-1-category">شغل ها</a>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-content">
                                            <a href="{{route('article')}}" class="pxp-posts-card-1-title">چگونه شروع به
                                                جستجوی شغل کنیم</a>
                                            <div class="pxp-posts-card-1-summary pxp-text-light"> رزومه شما
                                                عالی است. این بهینه شده برای کلمات کلیدی، مشخص شده در صنعت، پر از
                                                دستاوردها، با پشتوانه داده ها...</div>
                                        </div>
                                    </div>
                                    <div class="pxp-posts-card-1-bottom">
                                        <div class="pxp-posts-card-1-cta">
                                            <a href="{{route('article')}}">بیشتر بخوانید<span
                                                    class="fa fa-angle-left"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                <div class="pxp-posts-card-1 pxp-has-border">
                                    <div class="pxp-posts-card-1-top">
                                        <div class="pxp-posts-card-1-top-bg">
                                            <div class="pxp-posts-card-1-image pxp-cover"
                                                 style="background-image: url(assets/front/images/post-card-3.jpg);"></div>
                                            <div class="pxp-posts-card-1-info">
                                                <div class="pxp-posts-card-1-date"> 10 اردیبهشت 1400</div>
                                                <a href="blog-list-1.html" class="pxp-posts-card-1-category">رزومه</a>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-content">
                                            <a href="{{route('article')}}" class="pxp-posts-card-1-title">نمونه های
                                                رزومه</a>
                                            <div class="pxp-posts-card-1-summary pxp-text-light"> برای نوشتن رزومه به
                                                کمک نیاز دارید؟ به دنبال نمونه های رزومه برای صنایع خاص هستید؟ انواع
                                                مختلفی پیدا خواهید کرد...</div>
                                        </div>
                                    </div>
                                    <div class="pxp-posts-card-1-bottom">
                                        <div class="pxp-posts-card-1-cta">
                                            <a href="{{route('article')}}">بیشتر بخوانید<span
                                                    class="fa fa-angle-left"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                <div class="pxp-posts-card-1 pxp-has-border">
                                    <div class="pxp-posts-card-1-top">
                                        <div class="pxp-posts-card-1-top-bg">
                                            <div class="pxp-posts-card-1-image pxp-cover"
                                                 style="background-image: url(assets/front/images/post-card-4.jpg);"></div>
                                            <div class="pxp-posts-card-1-info">
                                                <div class="pxp-posts-card-1-date"> 15 اردیبهشت 1400</div>
                                                <a href="blog-list-1.html" class="pxp-posts-card-1-category">مصاحبه</a>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-content">
                                            <a href="{{route('article')}}" class="pxp-posts-card-1-title"> 100 سوال
                                                برتر مصاحبه - آماده باشید</a>
                                            <div class="pxp-posts-card-1-summary pxp-text-light">در حالی که
                                                به تعداد مصاحبه کنندگان، سؤالات مختلف ممکن برای مصاحبه وجود دارد، همیشه
                                                کمک می کند...</div>
                                        </div>
                                    </div>
                                    <div class="pxp-posts-card-1-bottom">
                                        <div class="pxp-posts-card-1-cta">
                                            <a href="{{route('article')}}">بیشتر بخوانید<span
                                                    class="fa fa-angle-left"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                <div class="pxp-posts-card-1 pxp-has-border">
                                    <div class="pxp-posts-card-1-top">
                                        <div class="pxp-posts-card-1-top-bg">
                                            <div class="pxp-posts-card-1-image pxp-cover"
                                                 style="background-image: url(assets/front/images/post-card-1.jpg);"></div>
                                            <div class="pxp-posts-card-1-info">
                                                <div class="pxp-posts-card-1-date">31 فروردین 1400</div>
                                                <a href="blog-list-1.html" class="pxp-posts-card-1-category"> ارزیابی
                                                    ها</a>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-content">
                                            <a href="{{route('article')}}" class="pxp-posts-card-1-title"> 10 خود
                                                ارزیابی رایگان حرفه ای</a>
                                            <div class="pxp-posts-card-1-summary pxp-text-light"> فهمیدن
                                                اینکه وقتی بزرگ شوید چه کاره می خواهید شوید، سخت است، اما یک آزمون شغلی
                                                می تواند پیدا کردن آن را آسان تر کند...</div>
                                        </div>
                                    </div>
                                    <div class="pxp-posts-card-1-bottom">
                                        <div class="pxp-posts-card-1-cta">
                                            <a href="{{route('article')}}">بیشتر بخوانید<span
                                                    class="fa fa-angle-left"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                <div class="pxp-posts-card-1 pxp-has-border">
                                    <div class="pxp-posts-card-1-top">
                                        <div class="pxp-posts-card-1-top-bg">
                                            <div class="pxp-posts-card-1-image pxp-cover"
                                                 style="background-image: url(assets/front/images/post-card-2.jpg);"></div>
                                            <div class="pxp-posts-card-1-info">
                                                <div class="pxp-posts-card-1-date">۵ اردیبهشت 1400</div>
                                                <a href="blog-list-1.html" class="pxp-posts-card-1-category">شغل ها</a>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-content">
                                            <a href="{{route('article')}}" class="pxp-posts-card-1-title">چگونه شروع به
                                                جستجوی شغل کنیم</a>
                                            <div class="pxp-posts-card-1-summary pxp-text-light"> رزومه شما
                                                عالی است. این بهینه شده برای کلمات کلیدی، مشخص شده در صنعت، پر از
                                                دستاوردها، با پشتوانه داده ها...</div>
                                        </div>
                                    </div>
                                    <div class="pxp-posts-card-1-bottom">
                                        <div class="pxp-posts-card-1-cta">
                                            <a href="{{route('article')}}">بیشتر بخوانید<span
                                                    class="fa fa-angle-left"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                <div class="pxp-posts-card-1 pxp-has-border">
                                    <div class="pxp-posts-card-1-top">
                                        <div class="pxp-posts-card-1-top-bg">
                                            <div class="pxp-posts-card-1-image pxp-cover"
                                                 style="background-image: url(assets/front/images/post-card-3.jpg);"></div>
                                            <div class="pxp-posts-card-1-info">
                                                <div class="pxp-posts-card-1-date"> 10 اردیبهشت 1400</div>
                                                <a href="blog-list-1.html" class="pxp-posts-card-1-category">رزومه</a>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-content">
                                            <a href="{{route('article')}}" class="pxp-posts-card-1-title">نمونه های
                                                رزومه</a>
                                            <div class="pxp-posts-card-1-summary pxp-text-light"> برای نوشتن رزومه به
                                                کمک نیاز دارید؟ به دنبال نمونه های رزومه برای صنایع خاص هستید؟ انواع
                                                مختلفی پیدا خواهید کرد...</div>
                                        </div>
                                    </div>
                                    <div class="pxp-posts-card-1-bottom">
                                        <div class="pxp-posts-card-1-cta">
                                            <a href="{{route('article')}}">بیشتر بخوانید<span
                                                    class="fa fa-angle-left"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                <div class="pxp-posts-card-1 pxp-has-border">
                                    <div class="pxp-posts-card-1-top">
                                        <div class="pxp-posts-card-1-top-bg">
                                            <div class="pxp-posts-card-1-image pxp-cover"
                                                 style="background-image: url(assets/front/images/post-card-4.jpg);"></div>
                                            <div class="pxp-posts-card-1-info">
                                                <div class="pxp-posts-card-1-date"> 15 اردیبهشت 1400</div>
                                                <a href="blog-list-1.html" class="pxp-posts-card-1-category">مصاحبه</a>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-content">
                                            <a href="{{route('article')}}" class="pxp-posts-card-1-title"> 100 سوال
                                                برتر مصاحبه - آماده باشید</a>
                                            <div class="pxp-posts-card-1-summary pxp-text-light">در حالی که
                                                به تعداد مصاحبه کنندگان، سؤالات مختلف ممکن برای مصاحبه وجود دارد، همیشه
                                                کمک می کند...</div>
                                        </div>
                                    </div>
                                    <div class="pxp-posts-card-1-bottom">
                                        <div class="pxp-posts-card-1-cta">
                                            <a href="{{route('article')}}">بیشتر بخوانید<span
                                                    class="fa fa-angle-left"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                <div class="pxp-posts-card-1 pxp-has-border">
                                    <div class="pxp-posts-card-1-top">
                                        <div class="pxp-posts-card-1-top-bg">
                                            <div class="pxp-posts-card-1-image pxp-cover"
                                                 style="background-image: url(assets/front/images/post-card-1.jpg);"></div>
                                            <div class="pxp-posts-card-1-info">
                                                <div class="pxp-posts-card-1-date">31 فروردین 1400</div>
                                                <a href="blog-list-1.html" class="pxp-posts-card-1-category"> ارزیابی
                                                    ها</a>
                                            </div>
                                        </div>
                                        <div class="pxp-posts-card-1-content">
                                            <a href="{{route('article')}}" class="pxp-posts-card-1-title"> 10 خود
                                                ارزیابی رایگان حرفه ای</a>
                                            <div class="pxp-posts-card-1-summary pxp-text-light"> فهمیدن
                                                اینکه وقتی بزرگ شوید چه کاره می خواهید شوید، سخت است، اما یک آزمون شغلی
                                                می تواند پیدا کردن آن را آسان تر کند...</div>
                                        </div>
                                    </div>
                                    <div class="pxp-posts-card-1-bottom">
                                        <div class="pxp-posts-card-1-cta">
                                            <a href="{{route('article')}}">بیشتر بخوانید<span
                                                    class="fa fa-angle-left"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4 mt-lg-5 justify-content-between align-items-center">
                            <div class="col-auto">
                                <nav class="mt-3 mt-sm-0" aria-label="Blog articles pagination">
                                    <ul class="pagination pxp-pagination">
                                        <li class="page-item active" aria-current="page">
                                            <span class="page-link">1</span>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-auto">
                                <a href="#" class="btn rounded-pill pxp-section-cta mt-3 mt-sm-0">بیشتر به من نشان
                                    بده<span class="fa fa-angle-left"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-xl-4 col-xxl-3">
                        <div class="pxp-blogs-list-side-panel">
                            <h3>جستجوی مقالات</h3>
                            <div class="mt-2 mt-lg-3">
                                <div class="input-group">
                                    <span class="input-group-text"><span class="fa fa-search"></span></span>
                                    <input type="text" class="form-control" placeholder="جستجو بر اساس کلمه کلیدی">
                                </div>
                            </div>

                            <h3 class="mt-3 mt-lg-4">دسته بندی ها</h3>
                            <ul class="list-unstyled pxp-blogs-side-list mt-2 mt-lg-3">
                                <li><a href="blog-list-1.html"> ارزیابی ها</a></li>
                                <li><a href="blog-list-1.html">شغل ها</a></li>
                                <li><a href="blog-list-1.html">رزومه</a></li>
                                <li><a href="blog-list-1.html">آینده کاری</a></li>
                                <li><a href="blog-list-1.html">مصاحبه</a></li>
                            </ul>

                            <h3 class="mt-3 mt-lg-4">مقالات اخیر</h3>
                            <ul class="list-unstyled pxp-blogs-side-articles-list mt-2 mt-lg-3">
                                <li class="mb-3">
                                    <a href="{{route('article')}}" class="pxp-blogs-side-articles-list-item">
                                        <div class="pxp-blogs-side-articles-list-item-fig pxp-cover"
                                             style="background-image: url(assets/front/images/post-card-1.jpg);"></div>
                                        <div class="pxp-blogs-side-articles-list-item-content ms-3">
                                            <div class="pxp-blogs-side-articles-list-item-title"> 10 خود ارزیابی رایگان
                                                حرفه ای</div>
                                            <div class="pxp-blogs-side-articles-list-item-date pxp-text-light"> 3
                                                سپتامبر 2021</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="mb-3">
                                    <a href="{{route('article')}}" class="pxp-blogs-side-articles-list-item">
                                        <div class="pxp-blogs-side-articles-list-item-fig pxp-cover"
                                             style="background-image: url(assets/front/images/post-card-2.jpg);"></div>
                                        <div class="pxp-blogs-side-articles-list-item-content ms-3">
                                            <div class="pxp-blogs-side-articles-list-item-title">چگونه شروع به جستجوی
                                                شغل کنیم</div>
                                            <div class="pxp-blogs-side-articles-list-item-date pxp-text-light">۵ سپتامبر
                                                ۲۰۲۱</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="mb-3">
                                    <a href="{{route('article')}}" class="pxp-blogs-side-articles-list-item">
                                        <div class="pxp-blogs-side-articles-list-item-fig pxp-cover"
                                             style="background-image: url(assets/front/images/post-card-3.jpg);"></div>
                                        <div class="pxp-blogs-side-articles-list-item-content ms-3">
                                            <div class="pxp-blogs-side-articles-list-item-title"> نمونه های رزومه</div>
                                            <div class="pxp-blogs-side-articles-list-item-date pxp-text-light"> 10
                                                سپتامبر 2021</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="mb-3">
                                    <a href="{{route('article')}}" class="pxp-blogs-side-articles-list-item">
                                        <div class="pxp-blogs-side-articles-list-item-fig pxp-cover"
                                             style="background-image: url(assets/front/images/post-card-4.jpg);"></div>
                                        <div class="pxp-blogs-side-articles-list-item-content ms-3">
                                            <div class="pxp-blogs-side-articles-list-item-title"> 100 سوال برتر مصاحبه -
                                                آماده باشید</div>
                                            <div class="pxp-blogs-side-articles-list-item-date pxp-text-light"> 15
                                                سپتامبر 2021</div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('article')}}" class="pxp-blogs-side-articles-list-item">
                                        <div class="pxp-blogs-side-articles-list-item-fig pxp-cover"
                                             style="background-image: url(assets/front/images/post-card-1.jpg);"></div>
                                        <div class="pxp-blogs-side-articles-list-item-content ms-3">
                                            <div class="pxp-blogs-side-articles-list-item-title"> 10 خود ارزیابی رایگان
                                                حرفه ای</div>
                                            <div class="pxp-blogs-side-articles-list-item-date pxp-text-light">۲۰
                                                سپتامبر ۲۰۲۱</div>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                            <h3 class="mt-3 mt-lg-4">برچسب ها</h3>
                            <div class="mt-2 mt-lg-3 pxp-blogs-side-tags">
                                <a href="blog-list-1.html">دور کاری</a>
                                <a href="blog-list-1.html">پاره وقت</a>
                                <a href="blog-list-1.html">مدیریت</a>
                                <a href="blog-list-1.html">امور مالی</a>
                                <a href="blog-list-1.html">خرده فروشی</a>
                                <a href="blog-list-1.html">فناوری اطلاعات</a>
                                <a href="blog-list-1.html">مهندسی</a>
                                <a href="blog-list-1.html">فروش</a>
                                <a href="blog-list-1.html">تولید</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
