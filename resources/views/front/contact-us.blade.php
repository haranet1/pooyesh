@extends('front.master')
@section('title','تماس با ما')
@section('main')
    <section class="mt-100 pxp-no-hero">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">با ما در ارتباط باشید</h2>

            <div class="row mt-4 mt-md-5 justify-content-center pxp-animate-in pxp-animate-in-top">
                <div class="col-lg-4 col-xxl-3 pxp-contact-card-1-container">
                    <a href="#" class="pxp-contact-card-1">
                        <div class="pxp-contact-card-1-icon-container">
                            <div class="pxp-contact-card-1-icon">
                                <span class="fa fa-globe"></span>
                            </div>
                        </div>
                        <div class="pxp-contact-card-1-title">پارک علم و فناوری دانشگاه آزاد اسلامی نجف آباد</div>
                    </a>
                </div>
                <div class="col-lg-4 col-xxl-3 pxp-contact-card-1-container">
                    <a href="#" class="pxp-contact-card-1">
                        <div class="pxp-contact-card-1-icon-container">
                            <div class="pxp-contact-card-1-icon">
                                <span class="fa fa-phone"></span>
                            </div>
                        </div>
                        <div class="pxp-contact-card-1-title" dir="rtl">031-4200 داخلی 3305</div>
                    </a>
                </div>
                <div class="col-lg-4 col-xxl-3 pxp-contact-card-1-container">
                    <a href="#" class="pxp-contact-card-1">
                        <div class="pxp-contact-card-1-icon-container">
                            <div class="pxp-contact-card-1-icon">
                                <span class="fa fa-envelope-o"></span>
                            </div>
                        </div>
                        <div class="pxp-contact-card-1-title">info@fanyarco.ir</div>
                    </a>
                </div>
            </div>

           {{-- <div class="row mt-100 justify-content-center pxp-animate-in pxp-animate-in-top">
                <div class="col-lg-6 col-xxl-4">
                    <div class="pxp-contact-us-form pxp-has-animation pxp-animate">
                        <h2 class="pxp-section-h2 text-center"> تماس با ما </h2>
                        <form class="mt-4">
                            <div class="mb-3">
                                <label for="contact-us-name" class="form-label">نام</label>
                                <input type="text" class="form-control" id="contact-us-name"
                                       placeholder="نام خود را اینجا بنویسید">
                            </div>
                            <div class="mb-3">
                                <label for="contact-us-email" class="form-label">ایمیل</label>
                                <input type="text" class="form-control" id="contact-us-email"
                                       placeholder="ایمیل خود را اینجا بنویسید">
                            </div>
                            <div class="mb-3">
                                <label for="contact-us-message" class="form-label">پیام</label>
                                <textarea class="form-control" id="contact-us-message"
                                          placeholder="پیام خود را اینجا بنویسید..."></textarea>
                            </div>
                            <a href="#" class="btn rounded-pill pxp-section-cta d-block">ارسال پیام</a>
                        </form>
                    </div>
                </div>
            </div>--}}
        </div>
    </section>
@endsection
