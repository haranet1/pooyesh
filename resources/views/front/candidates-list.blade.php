@extends('front.master')
@section('title','لیست کارجویان')
@section('main')
    <section class="pxp-page-header-simple">
        <div class="pxp-container">
            <h1>جستجوی کارجویان</h1>
            <div class="pxp-hero-subtitle pxp-text-ligh">با مستعدترین کارجویان کار کنید</div>
            <div class="pxp-hero-form pxp-hero-form-round pxp-large mt-3 mt-lg-4 pxp-has-border">
                <form class="row gx-3 align-items-center">
                    <div class="col-12 col-lg">
                        <div class="input-group mb-3 mb-lg-0">
                            <span class="input-group-text"><span class="fa fa-search"></span></span>
                            <input type="text" class="form-control" placeholder="نام یا کلمه کلیدی کارجو">
                        </div>
                    </div>
                    <div class="col-12 col-lg pxp-has-left-border">
                        <div class="input-group mb-3 mb-lg-0">
                            <span class="input-group-text"><span class="fa fa-folder-o"></span></span>
                            <select class="form-select">
                                <option selected>کلیه صنایع</option>
                                <option>توسعه دهنده کسب و کار</option>
                                <option> ساخت و ساز</option>
                                <option>خدمات مشتری</option>
                                <option>امور مالی</option>
                                <option> مراقبت‌های بهداشتی</option>
                                <option>منابع انسانی</option>
                                <option>بازاریابی و ارتباطات</option>
                                <option>مدیریت پروژه</option>
                                <option>مهندس نرم افزار</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-lg-auto">
                        <button>جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <section class="mt-4 mt-lg-5 pt-100 pb-100" style="background-color: var(--pxpSecondaryColorLight);">
        <div class="pxp-container">
            <div class="pxp-candidates-list-top">
                <div class="row justify-content-between align-items-center">
                    <div class="col-auto">
                        <h2><span class="pxp-text-light">نمایش</span> {{count($candidates)}} <span class="pxp-text-light">کارجو</span>
                        </h2>
                    </div>
                    <div class="col-auto">
                       {{-- <select class="form-select">
                            <option value="0" selected> مرتبط ترین</option>
                            <option value="1">نام صعودی</option>
                            <option value="2">نام نزولی</option>
                        </select>--}}
                    </div>
                </div>
            </div>

            <div class="row">
                @if(count($candidates)>0)
                    @foreach($candidates as $candidate)
                <div class="col-md-6 col-xl-4 col-xxl-3 pxp-candiadates-card-1-container">
                    <div class="pxp-candiadates-card-1 pxp-has-shadow text-center">
                        <div class="pxp-candiadates-card-1-top">
                            @if($candidate->photo)
                                <div class="pxp-candiadates-card-1-avatar pxp-cover"
                                     style="background-image: url({{$candidate->photo->path}});"></div>
                            @else
                            <div class="pxp-candiadates-card-1-avatar pxp-cover"
                                 style="background-image: url(assets/front/images/user-avatar.svg);"></div>
                            @endif
                            <div class="pxp-candiadates-card-1-name"> {{$candidate->name." ".$candidate->family}}</div>
                            <div class="pxp-candiadates-card-1-title">{{$candidate->groupable->major}}</div>
                            <div class="pxp-candiadates-card-1-location"><span class="fa fa-globe"></span> {{$candidate->groupable->city->name}}
                            </div>
                        </div>
                        <div class="pxp-candiadates-card-1-bottom">
                            <div class="pxp-candiadates-card-1-cta">
                                <a href="{{route('candidate.single',$candidate->id)}}">مشاهده پروفایل<span
                                        class="fa fa-angle-left"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach
                @else
                    <div class="alert alert-warning">اطلاعاتی جهت نمایش وجود ندارد</div>
                @endif
            </div>

            <div class="row mt-4 mt-lg-5 justify-content-between align-items-center">
                <div class="col-auto">
                    <nav class="mt-3 mt-sm-0" aria-label="Candidates pagination">
                        {{$candidates->links('pagination.front')}}
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection
