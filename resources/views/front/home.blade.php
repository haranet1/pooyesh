@extends('front.master')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/front/css/accordion-style.css')}}">
@endsection

@section('title','صفحه اصلی')

@section('main')
<style>
    .autocomplete {
    /*the container must be positioned relative:*/
    position: relative;
    display: inline-block;
    }

    .autocomplete-items {
    position: absolute;
    border: 1px solid #d4d4d4;
    border-bottom: none;
    border-top: none;
    z-index: 10;
    /*position the autocomplete items to be the same width as the container:*/
    top: 100%;
    left: 0;
    right: 0;
    max-height: 300px;
    overflow-y: scroll;
    border-radius: 0px 0px 15px 15px;
    }
    .autocomplete-items div {
    padding: 10px;
    cursor: pointer;
    background-color: #fff;
    border-bottom: 1px solid #d4d4d4;
    }
    .autocomplete-items div:hover {
    /*when hovering an item:*/
    background-color: #e9e9e9;
    }
    .autocomplete-active {
    /*when navigating through the items using the arrow keys:*/
    background-color: DodgerBlue !important;
    color: #ffffff;
    }
    .custom-color:nth-child(odd){
        background-color: #ddf0f6;
        border: 1px solid #1e81ce;
    }
    .custom-color:nth-child(even){
        border: 1px solid #818181;
    }
</style>
    <section class="pxp-hero vh-100" style="background-color: var(--pxpMainColorLight);">
        <div class="pxp-hero-caption">
            <div class="pxp-container">
                @if(Session::has('success'))
                    <div class="alert alert-success text-center">
                        <h3>{{Session::pull('success')}}</h3>
                    </div>
                @endif

                <div class="row pxp-pl-80 align-items-center justify-content-between">

                    {{-------------Banner------------------}}
                    @if(count($events) > 0)
                        @php($flag = true)
                        <div class="row mb-3 justify-content-start">
                            <div class="col-md-7 py-3" style="border: 3px dashed #4a55a2;border-radius: 10px">
                                <div id="pxp-blog-featured-posts-carousel"
                                     class="carousel slide carousel-fade" data-bs-ride="carousel">
                                    <div class="carousel-inner">
                                        @foreach($events as $event)
                                            <div class="carousel-item {{$flag?'active':''}}" data-bs-interval="10000">
                                                <a href="{{route('company.sadra')}}">
                                                    <img style="max-height:260px;max-width: 727px;"
                                                         src="{{asset($event->banner->path)}}"
                                                         class="d-block w-100 img-fluid rounded" alt="banner">
                                                </a>
                                            </div>
                                            @php($flag = false)
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    {{-------------End-Banner------------------}}


                    <div class="col-12 col-xl-6 col-xxl-5">

                        <h1>مهارت، <br><span style="color: var(--pxpMainColor);">کارآموزی، </span> اشتغال</h1>
                        <div class="pxp-hero-subtitle mt-3 mt-lg-4">فرصت شغلی خود را از میان
                            <strong>{{count($jobs_count)}}</strong> شغل پیدا کنید
                        </div>

                        <div class="pxp-hero-form pxp-hero-form-round mt-3 mt-lg-4">
                            <form class="row gx-3 align-items-center"
                                  action="{{route('jobs.list')}}" method="get">

                                <div class="col-12 col-sm">
                                    <div class="mb-3 mb-sm-0 autocomplete w-100">
                                        <input id="titleId" name="title" type="text" class="form-control"
                                               placeholder="کلمه کلیدی یا عنوان شغل">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-auto">
                                    <button type="submit"><span class="fa fa-search"></span></button>
                                </div>
                            </form>
                        </div>


                    </div>

                    <div class="d-none d-xl-block col-xl-5 position-relative">
                        <div class="pxp-hero-cards-container pxp-animate-cards pxp-mouse-move" data-speed="160">
                            <div class="pxp-hero-card pxp-cover pxp-cover-top"
                                 style="background-image: url(assets/front/images/hero-bg-1.jpg);"></div>
                            <div class="pxp-hero-card-dark"></div>
                            <div class="pxp-hero-card-light"></div>
                        </div>

                        <div class="pxp-hero-card-info-container pxp-mouse-move" data-speed="60">
                            <div class="pxp-hero-card-info pxp-animate-bounce">
                                <div class="pxp-hero-card-info-item">
                                    <div class="pxp-hero-card-info-item-number">{{count($company_count)}}
                                        <span> کارفرما</span>
                                    </div>
                                    <div class="pxp-hero-card-info-item-description">ثبت شده در سامانه</div>
                                </div>
                                <div class="pxp-hero-card-info-item">
                                    <div class="pxp-hero-card-info-item-number">{{count($students_count)}}
                                        <span> کارجو</span>
                                    </div>
                                    <div class="pxp-hero-card-info-item-description">ثبت شده در سامانه</div>
                                </div>
                                {{-- <div class="pxp-hero-card-info-item">
                                    <div class="pxp-hero-card-info-item-number">{{count($hire_requests)}}
                                        <span> پیشنهاد استخدام</span>
                                    </div>
                                    <div class="pxp-hero-card-info-item-description">ثبت شده در سامانه</div>
                                </div> --}}
                                {{-- <div class="pxp-hero-card-info-item">
                                    <div class="pxp-hero-card-info-item-number">{{count($intern_requests)}}
                                        <span> درخواست طرح پویش</span>
                                    </div>
                                    <div class="pxp-hero-card-info-item-description">ثبت شده در سامانه</div>
                                </div> --}}
                                <div class="pxp-hero-card-info-item">
                                    <div class="pxp-hero-card-info-item-number">{{count($jobs_count)}}
                                        <span> موقعیت شغلی </span>
                                    </div>
                                    <div class="pxp-hero-card-info-item-description">ثبت شده در سامانه</div>
                                </div>
                                <div class="pxp-hero-card-info-item">
                                    <div class="pxp-hero-card-info-item-number">{{$mbti_test_user}}
                                        <span>تست MBTI</span>
                                    </div>
                                    <div class="pxp-hero-card-info-item-description">ثبت شده در سامانه</div>
                                </div>
                                <div class="pxp-hero-card-info-item">
                                    <div class="pxp-hero-card-info-item-number">{{$enneagram_test_user}}
                                        <span>تست انیاگرام</span>
                                    </div>
                                    <div class="pxp-hero-card-info-item-description">ثبت شده در سامانه</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="pxp-hero-logos-carousel-container">
            <div class="pxp-container">
                <div class="row pxp-pl-80">
                    <div class="col-12 col-xl-6">
                        <div class="pxp-hero-logos-carousel owl-carousel">
                            <img src="{{asset('assets/front/images/jan-snak-logo.svg')}}" alt="jan-snak-logo">
                            <img src="{{asset('assets/front/images/hamgam.svg')}}" alt="hamgam-logo">
                            <img src="{{asset('assets/front/images/arshak-logo.svg')}}" alt="arshak-logo">
                            {{-- <img src="{{asset('assets/front/images/hero-logo-3.svg')}}" alt="Logo 3">
                            <img src="{{asset('assets/front/images/hero-logo-4.svg')}}" alt="Logo 4">
                            <img src="{{asset('assets/front/images/hero-logo-5.svg')}}" alt="Logo 5">
                            <img src="{{asset('assets/front/images/hero-logo-6.svg')}}" alt="Logo 6">
                            <img src="{{asset('assets/front/images/hero-logo-7.svg')}}" alt="Logo 7">
                            <img src="{{asset('assets/front/images/hero-logo-1.svg')}}" alt="Logo 8">
                            <img src="{{asset('assets/front/images/hero-logo-2.svg')}}" alt="Logo 9">
                            <img src="{{asset('assets/front/images/hero-logo-3.svg')}}" alt="Logo 10"> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="pxp-hero-right-bg-card pxp-has-animation"></div>
    </section>


    {{-- Jobs--}}
    @if(count($jobs) >0)

        <section>

            <div class="container pt-3">
                <h2 class="pxp-section-h2 text-center">آخرین پیشنهاد های شغلی</h2>
                <p class="pxp-text-light text-center">فرصت شغلی خود را از بین چندین شغل جستجو کنید</p>
                <div class="container">
                    @foreach($jobs as $job)
                        <div class="accordion custom-color">
                            <div class="accordion-title">
                                <div class="row align-items-center w-100">
                                    <div class="col-12 col-md-4">
                                        <img alt="img-custom" src="https://fanyarco.ir/assets/images/default-logo.svg"style="width: 45px; border-radius: 10px">
                                        <span class="text-span span-title">{{job_type_persian($job->type)}}</span>
                                        <span class="text-title" style="font-size: 0.9rem !important">{{$job->title}}</span>
                                    </div>
                                    <div class="col-12 col-md-3 text-center text-lg-right">
                                        <a href="" class="text-a">
                                            <span >{{job_province_and_city_name($job->province , $job->city)}}</span>
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-5 d-flex justify-content-center p-0">
                                        <div class="row w-100 align-items-center">
                                            <div class="col-12">
                                                <span class="text-span span-title p-0">{{verta($job->created_at)->formatDifference()}}</span>
                                                <span class="text-span span-title p-0 ">در گروه</span>
                                                <a href="{{route('jobs.list')}}" class="text-a3">{{$job->category->name}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-content">
                                @auth
                                    <div class="row w-100 my-3 align-items-center justify-content-center">
                                        <hr style="height: 2px; padding-right: 10px; margin-right: 10px; color: #1e81ce">
                                        <div class="row w-100 ">
                                            <div class="col-6 col-lg-4 d-lg-flex justify-content-lg-center custom-text-400">
                                                <span class="mx-lg-2">توسط شرکت</span>
                                                <a href="{{route('company.single',$job->company->id)}}" class="">{{$job->company->groupable->name}}</a>
                                            </div>
                                            <div class="col-6 col-lg-4 d-lg-flex justify-content-lg-center custom-text-400">
                                                <span class="mx-lg-2">سابقه مورد نیاز :</span>
                                                <span>{{$job->experience}} سال</span>
                                            </div>
                                            <div class="col-12 col-lg-4 d-lg-flex justify-content-lg-center text-center">
                                                <a href="{{route('job.single',$job->id)}}" class="btn btn-primary" style="border-radius: 30px">اطلاعات بیشتر</a>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="row justify-content-center w-100">
                                        <hr style="height: 2px; margin-right: 25px; color: #1e81ce">
                                        <div class="col-12 col-md-6 d-flex justify-content-center mb-3">
                                            <h4 class="custom-h4">برای مشاهده اطلاعات بیشتر ثبت نام یا وارد شوید</h4>
                                        </div>
                                        <div class="col-12 col-md-6 d-flex justify-content-around justify-content-lg-center">
                                            <a href="{{route('login')}}" class="btn btn-primary p-1 px-3 mb-3 mx-2" style="border-radius: 30px">ورود</a>
                                            <a href="{{route('register')}}" class="btn btn-primary p-1 px-3 mb-3 mx-2" style="border-radius: 30px">ثبت نام</a>
                                        </div>
                                    </div>
                                @endauth
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="mt-4 mt-md-5 text-center pxp-animate-in pxp-animate-in-top">
                    <a href="{{route('jobs.list')}}" class="btn rounded-pill pxp-section-cta">همه موقعیت های شغلی<span
                            class="fa fa-angle-left"></span></a>
                </div>
            </div>

            {{-- <div class="pxp-container pt-3">
                <h2 class="pxp-section-h2 text-center">آخرین پیشنهاد های شغلی</h2>
                <p class="pxp-text-light text-center">فرصت شغلی خود را از بین چندین شغل جستجو کنید</p>
                <div class="row mt-4 pxp-animate-in pxp-animate-in-top mt-3" id="jobs-container">
                    @foreach($jobs as $job)
                        <div class="pxp-jobs-card-3 pxp-has-border p-0 px-2 mt-2 custom-color">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-12 col-lg-5 d-flex align-items-center" style="height: 50px">
                                    <span class="d-block">
                                        <img class="d-inline" src="{{asset('assets/images/default-logo.svg')}}" alt="company logo" style="width: 40px; border-radius: 20px">
                                        <div class="d-inline">{{job_type_persian($job->type)}}</div>
                                        <span class="d-inline pxp-jobs-card-3-title mt-3 mt-sm-0">{{$job->title}}</span>
                                        <a href="{{route('jobs.list')}}" class="d-inline pxp-jobs-card-3-location">
                                            در
                                            @if ($job->province)
                                                <span >{{$job->province->name}}</span>
                                            @endif
                                            @if ($job->city)
                                                <span>، {{$job->city->name}}</span>
                                            @endif
                                        </a>
                                    </span>
                                </div>
                                <div class="col-12 col-lg-7 d-flex align-items-center justify-content-between">

                                    @auth
                                        <span class="d-inline pxp-jobs-card-3-date pxp-text-light">{{verta($job->created_at)->formatDifference()}} توسط</span>
                                        <a href="{{route('company.single',$job->company->id)}}" class="d-inline pxp-jobs-card-3-company">{{$job->company->groupable->name}}</a>
                                        <div class="d-none d-md-inline">
                                            در گروه
                                            <a href="{{route('jobs.list')}}" class="d-inline pxp-jobs-card-3-category">
                                                <div class="d-inline pxp-jobs-card-3-category-label">{{$job->category->name}}</div>
                                            </a>
                                        </div>
                                    @else
                                        <span class="d-inline pxp-jobs-card-3-date pxp-text-light">{{verta($job->created_at)->formatDifference()}}</span>
                                        در گروه
                                        <a href="{{route('jobs.list')}}" class="d-inline pxp-jobs-card-3-category">
                                            <div class="d-inline pxp-jobs-card-3-category-label">{{$job->category->name}}</div>
                                        </a>
                                    @endauth
                                    <div class="d-inline pb-2 pb-lg-0 mt-xxl-0 pxp-text-right">
                                        <a href="{{route('job.single',$job->id)}}" class="btn rounded-pill pxp-card-btn">جزئیات</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="pxp-jobs-card-3 pxp-has-border p-0 px-2 mt-3 custom-color">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-xxl-auto d-flex align-items-center ms-2" style="height: 80px">
                                    <span class="d-block">
                                        <img src="{{asset('assets/images/default-logo.svg')}}" alt="company logo" style="width: 60px; border-radius: 20px">
                                    </span>
                                </div>
                                <div class="col-sm-9 col-md-10 col-lg-9 col-xl-10 col-xxl-4">
                                    <span class="pxp-jobs-card-3-title mt-3 mt-sm-0">{{$job->title}}</span>
                                    <div class="row pxp-jobs-card-3-details">
                                        <a href="{{route('jobs.list')}}" class="col-6 pxp-jobs-card-3-location">
                                            @if ($job->province)
                                                <span class="fa fa-globe"></span>
                                                <span >{{$job->province->name}}</span>
                                            @endif
                                            @if ($job->city)
                                                <span>، {{$job->city->name}}</span>
                                            @endif
                                        </a>
                                        <div class="col-6 pxp-jobs-card-3-type">{{job_type_persian($job->type)}}</div>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-xl-6 col-xxl-4 mt-3 mt-xxl-0">
                                    <a href="{{route('jobs.list')}}" class="pxp-jobs-card-3-category">
                                        <div class="pxp-jobs-card-3-category-label">{{$job->category->name}}</div>
                                    </a>
                                    <div class="pxp-jobs-card-3-date-company">
                                        @auth
                                            @php($auth = 1)
                                            <span class="pxp-jobs-card-3-date pxp-text-light">{{verta($job->created_at)->formatDifference()}} توسط </span>
                                            <a href="{{route('company.single',$job->company->id)}}" class="pxp-jobs-card-3-company">{{$job->company->groupable->name}}</a>
                                        @else
                                            @php($auth = 0)
                                            <span class="pxp-jobs-card-3-date pxp-text-light">{{verta($job->created_at)->formatDifference()}}</span>
                                        @endauth
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xl-2 col-xxl-auto mt-3 mt-xxl-0 pxp-text-right">
                                    <a href="{{route('job.single',$job->id)}}" class="btn rounded-pill pxp-card-btn">جزئیات</a>
                                </div>
                            </div>
                        </div> --}}
                    {{--@endforeach
                </div>
                <div class="mt-4 mt-md-5 text-center pxp-animate-in pxp-animate-in-top">
                    <a href="{{route('jobs.list')}}" class="btn rounded-pill pxp-section-cta">همه موقعیت های شغلی<span
                            class="fa fa-angle-left"></span></a>
                </div>
            </div> --}}
        </section>
    @endif


    {{--Companies--}}
    @if(count($companies) > 0)
        <section class="mt-5 pt-3 pb-5" style="background-color: var(--pxpSecondaryColorLight);">
            <div class="pxp-container">
                <h2 class="pxp-section-h2 text-center">شرکت هایی که به ما اعتماد کرده‌اند</h2>
                <p class="pxp-text-light text-center">افتخار فن‌یار، اعتماد شرکت هاییست که دغدغه نیروی انسانی خود را با آن حل کرده اند.</p>

                    <div class="row mt-3 pxp-animate-in pxp-animate-in-top">
                        @foreach($companies as $company)
                            <div class="col-md-6 col-xl-4 col-xxl-3 pxp-companies-card-1-container">
                                <div class="pxp-companies-card-1 pxp-has-border">
                                    <div class="pxp-companies-card-1-top">
                                        <a href="{{route('company.single',$company->id)}}"
                                        class="pxp-companies-card-1-company-logo border border-2 border-dark"
                                        style="background-image: 
                                        @if($company->groupable->logo)
                                            url({{asset($company->groupable->logo->path)}});
                                        @else
                                            url(assets/front/images/company-logo-1.png);
                                        @endif
                                        "></a>
                                        <a href="{{route('company.single',$company->id)}}"
                                        class="pxp-companies-card-1-company-name">{{$company->groupable->name}}</a>
                                        <div
                                            class="pxp-companies-card-1-company-description pxp-text-light">
                                            {{Str::limit(strip_tags($company->groupable->about),'80','...')}}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                <div class="mt-4 mt-md-5 text-center pxp-animate-in pxp-animate-in-top">
                    <a href="{{route('companies.list')}}" class="btn rounded-pill pxp-section-cta">همه شرکت ها<span
                            class="fa fa-angle-left"></span></a>
                </div>
            </div>
        </section>
    @endif

    <section class="mt-4">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">امکانات ویژه برای شرکت ها</h2>
            <p class="pxp-text-light text-center">فن‌یار برای افزایش راندمان شرکت شما در تکاپو است.</p>
                <div class="row mt-4 mt-md-5 pxp-animate-in pxp-animate-in-top" style="margin-top: 1.5rem !important">

                    <div class="col-md-6 col-xll-6 ">
                        <div class="row border shadow-lg m-3" style="border-radius: 15px">
                            <div class="col-7">
                                <div class="pxp-jobs-card-1-title mx-2 mb-2" style="color: #f27d14">طرح پویش (کارآموزی)</div>
                                <p align="justify" class="text-dark px-3">اشتغال، همزمان با تحصیل</p>
                            </div>
                            <div class="col-4">
                                <img class="p-3" style="" src="{{asset('assets/front/images/SVG/bicon1.svg')}}" alt="icon">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xll-6">
                        <div class="row border shadow-lg m-3" style="border-radius: 15px;">
                            <div class="col-7">
                                <div class="pxp-jobs-card-1-title mx-2 mb-2" style="color: #53b729">استخدام تخصصی</div>
                                <p align="justify" class="text-dark px-3">شناسایی نیروی متخصص و با مهارت بر اساس نیاز شما</p>
                            </div>
                            <div class="col-4">
                                <img class="p-3" style="" src="{{asset('assets/front/images/SVG/bicon2.svg')}}" alt="icon">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xll-6 ">
                        <div class="row border shadow-lg m-3" style="border-radius: 15px">
                            <div class="col-7">
                                <div class="pxp-jobs-card-1-title mx-2 mb-2" style="color: #1e81ce">آموزش مهارت محور</div>
                                <p align="justify" class="text-dark px-3">آموزش تخصصی مهارت های مورد نیاز به متقاضیان</p>
                            </div>
                            <div class="col-4">
                                <img class="p-3" style="" src="{{asset('assets/front/images/SVG/bicon3.svg')}}" alt="icon">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xll-6">
                        <div class="row border shadow-lg m-3" style="border-radius: 15px">
                            <div class="col-7">
                                <div class="pxp-jobs-card-1-title mx-2 mb-2" style="color: #e0ce22">رویداد ها و نمایشگاه ها</div>
                                <p align="justify" class="text-dark px-3">برگزاری رویداد های تخصصی جهت شناسایی بهترین ها برای شما</p>
                            </div>
                            <div class="col-4">
                                <img class="p-3" style="" src="{{asset('assets/front/images/SVG/bicon4.svg')}}" alt="icon">
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>

    {{-- categories--}}
    @if(count($job_cats) > 0)
        <section class="mt-3 py-3"  style="background-color: var(--pxpSecondaryColorLight); background-image: linear-gradient(180deg, #fff3df, white);">
            <div class="pxp-container">
                <h2 class="pxp-section-h2 text-center">دسته بندی های شغلی</h2>
                <p class="pxp-text-light text-center">فرصت شغلی خود را با دسته بندی های ما جستجو کنید</p>

                    <div class="row mt-3 pxp-animate-in pxp-animate-in-top">
                        @foreach($job_cats as $category)
                            <div class="col-12 col-md-4 col-lg-3 col-xxl-2 pxp-categories-card-1-container">
                                <a href="{{route('jobs.list')}}" class="pxp-categories-card-1">
                                    <div class="pxp-categories-card-1-icon-container">
                                        <div class="pxp-categories-card-1-icon">
                                            <span class="fa fa-pie-chart"></span>
                                        </div>
                                    </div>
                                    <div class="pxp-categories-card-1-title">{{$category->name}}</div>
                                    <div class="pxp-categories-card-1-subtitle">{{count($category->job_positions)}} موقعیت
                                        باز
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>


                <div class="mt-4 mt-md-5 text-center pxp-animate-in pxp-animate-in-top">
                    <a href="{{route('jobs.list')}}" class="btn rounded-pill pxp-section-cta">همه دسته بندی ها<span
                            class="fa fa-angle-left"></span></a>
                </div>
            </div>
        </section>
    @endif

    {{-- news --}}
    {{-- <section class="mt-100">
        <div class="pxp-container">
            <h2 class="pxp-section-h2 text-center">با خبر شوید</h2>
            <p class="pxp-text-light text-center">برای دریافت فید هفتگی ما در خبرنامه ما مشترک شوید.</p>

            <div class="row mt-4 mt-md-5 justify-content-center">
                <div class="col-md-9 col-lg-7 col-xl-6 col-xxl-5">
                    <div class="pxp-subscribe-1-container pxp-animate-in pxp-animate-in-top">
                        <div class="pxp-subscribe-1-image">
                            <img src="{{asset('assets/front/images/subscribe.png')}}" alt="Stay Up to Date">
                        </div>
                        <div class="pxp-subscribe-1-form">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="ایمیل خود را وارد کنید...">
                                    <button class="btn btn-primary" type="button">اشتراک<span
                                            class="fa fa-angle-left"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
@endsection


@section('script')
    <script src="{{asset('assets/front/js/accordion.js')}}"></script>
    {{-- search u --}}
    <script>
        let jobname = document.getElementById('titleId');
        jobname.addEventListener('input', function(){
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                url:"{{route('jobs.ajax')}}",
                data: {
                    do:'get-jobs',
                        job_title: jobname.value
                },
                dataType: 'json',
                    success: function (response) {
                        console.log(response);
                        autocomplete(jobname,response);

                },
                error: function (response) {
                    console.log(response)
                }
            });
        })


        function autocomplete(inp, arr) {
            var existingElement = document.getElementById("titleIdautocomplete-list")
            if (existingElement)
                existingElement.remove()

            /*the autocomplete function takes two arguments,
            the text field element and an array of possible autocompleted values:*/
            var currentFocus;

            var a, b = null
            /*, val = this.value;
                            /!*close any already open lists of autocompleted values*!/
                            closeAllLists();
                            if (!val) {
                                return false;
                            }*/
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", inp.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items border border-primary  bg-light py-2 px-1");
            /*append the DIV element as a child of the autocomplete container:*/
            inp.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (var i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i]['name'] + "</strong>";
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    cat = document.getElementById('jobs')
                    cat.value = this.getElementsByTagName("input")[0].id
                    document.getElementById("jobs").value = this.getElementsByTagName("input")[0].id
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }

            /*execute a function when someone writes in the text field:*/
            /*inp.addEventListener("input", function (e) {

            });*/

            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });

            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }

            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }

            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }

            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }

    </script>

    {{-- <script>
        getLatestJobPositions()
        function getLatestJobPositions(){
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                url:"{{route('front.ajax.get.jobPositions')}}",
                data: {
                    count : 10,
                },
                dataType: 'json',
                    success: function (response) {
                        console.log(response);
                        makeRows(response);

                },
                error: function (response) {
                    console.log(response)
                }
            });
        }

        function makeRows(data){
            var assetPath = "{{asset('assets/images/default-logo.svg')}}";
            var jobsListRoute = "{{route('jobs.list')}}";

            var auth = {{$auth}};
            var jobsContainer = document.getElementById("jobs-container");
            jobsContainer.innerHTML = '';
            var jobs = data;
            jobs.forEach(job => {
                var detailRoute = "{{route('job.single',':id')}}"
                detailRoute = detailRoute.replace(':id', job['id']);
                var companyRoute = "{{route('company.single',':id')}}";
                companyRoute.replace(':id', job['company_id']);
                var jobCard = document.createElement("div");
                jobCard.classList.add("pxp-jobs-card-3", "pxp-has-border", "p-0", "px-2", "mt-3", "custom-color");

                jobCard.innerHTML = `
                    <div class="row align-items-center justify-content-between">
                        <div class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-xxl-auto d-flex align-items-center ms-2" style="height: 80px">
                            <span class="d-block">
                                <img src="${assetPath}" alt="company logo" style="width: 60px; border-radius: 20px">
                            </span>
                        </div>
                        <div class="col-sm-9 col-md-10 col-lg-9 col-xl-10 col-xxl-4">
                            <span class="pxp-jobs-card-3-title mt-3 mt-sm-0">${job['title']}</span>
                            <div class="row pxp-jobs-card-3-details">
                                <a href="${jobsListRoute}" class="col-6 pxp-jobs-card-3-location">

                                </a>
                                <div class="col-6 pxp-jobs-card-3-type">${job['type']}</div>
                            </div>
                        </div>
                        <div class="col-sm-8 col-xl-6 col-xxl-4 mt-3 mt-xxl-0">
                            <a href="${jobsListRoute}" class="pxp-jobs-card-3-category">
                                <div class="pxp-jobs-card-3-category-label">${job['job']['category']['name']}</div>
                            </a>
                            <div class="pxp-jobs-card-3-date-company">
                                ${auth
                                    ? `<span class="pxp-jobs-card-3-date pxp-text-light">${job['created_at_fa']} توسط </span>
                                    <a href="${companyRoute}" class="pxp-jobs-card-3-company">${job['company']['groupable']['name']}</a>`
                                    : `<span class="pxp-jobs-card-3-date pxp-text-light">${job['created_at_fa']}</span>`
                                }
                            </div>
                        </div>
                        <div class="col-sm-4 col-xl-2 col-xxl-auto mt-3 mt-xxl-0 pxp-text-right">
                            <a href="${detailRoute}" class="btn rounded-pill pxp-card-btn">جزئیات</a>
                        </div>
                    </div>
                `;
                jobsContainer.appendChild(jobCard);
            });
        }
    </script> --}}
@endsection



{{-- this is code of old job list  --}}

{{-- <section class="mt-100 pt-100 pb-100">
            <div class="pxp-container">
                <h2 class="pxp-section-h2 text-center">آخرین پیشنهاد های شغلی</h2>
                <p class="pxp-text-light text-center">فرصت شغلی خود را از بین چندین شغل جستجو کنید</p>
                <div class="row mt-4 mt-md-5 pxp-animate-in pxp-animate-in-top">
                    @foreach($jobs as $job)
                        <div class="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                            <div class="pxp-jobs-card-1 pxp-has-shadow">
                                <div class="pxp-jobs-card-1-top">
                                    <a href="{{route('jobs.list')}}"
                                    class="pxp-jobs-card-1-category">
                                        <div class="pxp-jobs-card-1-category-icon"><span class="fa fa-bullhorn"></span>
                                        </div>
                                        <div class="pxp-jobs-card-1-category-label">{{$job->category->name}}</div>
                                    </a>
                                    <a href="{{route('job.single',$job->id)}}"
                                    class="pxp-jobs-card-1-title">{{$job->title}}</a>
                                    <div class="pxp-jobs-card-1-details">
                                        <a href="{{route('job.single',$job->id)}}" class="pxp-jobs-card-1-location">
                                            <span class="fa fa-globe"></span>{{$job->position}}
                                        </a>
                                        <div class="pxp-jobs-card-1-type">{{job_type_persian($job->type)}}</div>
                                    </div>
                                </div>
                                <div class="pxp-jobs-card-1-bottom">
                                    <div class="pxp-jobs-card-1-bottom-left">
                                        <div
                                            class="pxp-jobs-card-1-date pxp-text-light">{{verta($job->created_at)->formatDifference()}}
                                            توسط
                                        </div>
                                        <a href="{{route('company.single',$job->company->id)}}"
                                        class="pxp-jobs-card-1-company">{{$job->company->groupable->name}}</a>
                                    </div>
                                    <a href="{{route('company.single',$job->company->id)}}"
                                    class="pxp-jobs-card-1-company-logo"
                                    style="background-image: url(assets/front/images/company-logo-1.png);"></a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section> --}}
