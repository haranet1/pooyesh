@extends('front.master')

@section('title', 'پروفایل کارجو')
@section('main')
    <section>
        <div class="pxp-container">
            <div class="pxp-single-candidate-container pxp-has-columns">
                <div class="row">
                    <div class="col-lg-7 col-xl-8 col-xxl-9">
                        <div class="pxp-single-candidate-hero pxp-cover pxp-boxed"style="background-image: url({{ asset('assets/front/images/office.svg') }}); background-size: cover;">
                            <div class="pxp-hero-opacity"></div>
                            <div class="pxp-single-candidate-hero-caption">
                                <div class="pxp-container">
                                    <div class="pxp-single-candidate-hero-content">
                                        <div class="pxp-single-candidate-hero-avatar"
                                            style="background-image: url(assets/front/images/avatar-3.jpg);">
                                        </div>
                                        <div class="shadow border border-3 border-info pxp-single-candidate-hero-name bg-light p-3 px-4"
                                            style="border-radius: 3rem !important;">
                                            <h1 class="text-dark">{{ $user->name . ' ' . $user->family }}</h1>
                                            <div class="pxp-single-candidate-hero-title text-dark">
                                                @if ($user->groupable && $user->groupable->major)
                                                    {{ $user->groupable->major }}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (!$isConfirmed)
                            <div class="row">
                                <div class="alert alert-danger mt-3" style="border-radius: 25px;">
                                    این کاربر تایید نشده است. پس از بررسی اطلاعات کاربری بصورت خودکار تایید می‌شود و اطلاعات آن قابل مشاهده می‌گردد.
                                </div>
                            </div>
                        @endif
                        @if ((!$isConfirmed && Auth::check() && (Auth::id() == $user->id || Auth::user()->hasRole('employee'))) || $isConfirmed)
                            <section class="mt-4 mt-lg-5">
                                <div class="pxp-single-candidate-content">
                                    {{-- درباره کاربر --}}
                                    @if($user->groupable && $user->groupable->about)
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="border p-3" style="border-radius: 1rem !important;">
                                                    <h2>درمورد {{ $user->name . ' ' . $user->family }}</h2>
                                                    <p>
                                                        @php echo $user->groupable->about @endphp
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- دوره ها و گواهینامه ها --}}
                                    @if($user->certificates && count($user->certificates) > 0)
                                        <div class="row">
                                            <div class="col-12 mt-4 mt-lg-5">
                                                <div class="border p-3" style="border-radius: 1rem !important;">
                                                    <h2>دوره ها و گواهینامه ها</h2>
                                                    <div class="pxp-single-candidate-skills">
                                                        <ul class="list-unstyled">
                                                            @foreach ($user->certificates as $certificate)
                                                                <li class="p-4">
                                                                    <a target="_blank"
                                                                        href="{{ asset($certificate->photo->path) }}">
                                                                        <img style="max-height: 100px; min-height: 100px; max-width: 200px; min-width: 200px; border-radius: 1rem !important;"
                                                                            src="{{ asset($certificate->photo->path) }}"
                                                                            class="img-fluid mb-4">
                                                                    </a>
                                                                    <br>
                                                                    <span class="">{{ $certificate->title }}</span>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- تخصص ها --}}
                                    @if ($user->expertises && count($user->expertises) > 0)
                                        <div class="row">
                                            <div class="cpl-12 mt-4 mt-lg-5">
                                                <div class="border p-3" style="border-radius: 1rem !important;">
                                                    <h2>تخصص ها</h2>
                                                    <div class="pxp-single-candidate-skills">
                                                        <ul class="list-unstyled">
                                                            @foreach ($user->expertises as $expertise)
                                                                <li><span>{{ $expertise->name }}</span>
                                                                    {{ level_badge($expertise->pivot->skill_level) }}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- مهارت ها --}}
                                    @if ($user->skills && count($user->skills) > 0)
                                        <div class="row">
                                            <div class="col-12 mt-4 mt-lg-5">
                                                <div class="border p-3" style="border-radius: 1rem !important;">
                                                    <h2>مهارت ها</h2>
                                                    <div class="pxp-single-candidate-skills">
                                                        <ul class="list-unstyled">
                                                            @foreach ($user->skills as $skill)
                                                                <li><span>{{ $skill->name }}</span>
                                                                    {{ level_badge($skill->pivot->skill_level) }}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- زبان های خارجی --}}
                                    @if ($user->languages && count($user->languages) > 0)
                                        <div class="row">
                                            <div class="col-12 mt-4 mt-lg-5">
                                                <div class="border p-3" style="border-radius: 1rem !important;">
                                                    <h2>زبان های خارجی</h2>
                                                    <div class="pxp-single-candidate-skills">
                                                        <ul class="list-unstyled">
                                                            @foreach ($user->languages as $language)
                                                                <li><span>{{ $language->name }}</span>
                                                                    {{ level_badge($language->pivot->level) }}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- سطر اطلاعات آکادمیک و سوابق کاری --}}
                                    <div class="row">
                                        {{-- اطلاعات آکادمیک --}}
                                        @if ($user->academicInfos && count($user->academicInfos) > 0)
                                            <div class="col-12 col-md-6 mt-4 mt-lg-5">
                                                <div class="border p-3" style="border-radius: 1rem !important;">
                                                    <h2>اطلاعات آکادمیک</h2>
                                                    <div class="pxp-single-candidate-timeline">
                                                        @foreach ($user->academicInfos as $info)
                                                            <div class="pxp-single-candidate-timeline-item">
                                                                <div class="pxp-single-candidate-timeline-dot"></div>
                                                                <div class="pxp-single-candidate-timeline-info ms-3">
                                                                    <div class="pxp-single-candidate-timeline-time">
                                                                        <span class="me-3">
                                                                            {{ $info->end_date }}
                                                                        </span>
                                                                    </div>
                                                                    <div class="pxp-single-candidate-timeline-position mt-2">
                                                                        {{ $info->major }}
                                                                    </div>
                                                                    <div class="pxp-single-candidate-timeline-company pxp-text-light">
                                                                    <span>{{ $info->grade }}</span> : <span>{{ $info->university }}</span>
                                                                    </div>
                                                                    <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                                                        معدل : {{ $info->avg }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- سوابق کاری --}}
                                        @if ($user->work_experiences && count($user->work_experiences) > 0)
                                            <div class="col-12 col-md-6 mt-4 mt-lg-5">
                                                <div class="border p-3" style="border-radius: 1rem !important;">
                                                    <h2>سوابق کاری</h2>
                                                    <div class="pxp-single-candidate-timeline">
                                                        @foreach ($user->work_experiences as $workExperience)
                                                            <div class="pxp-single-candidate-timeline-item">
                                                                <div class="pxp-single-candidate-timeline-dot"></div>
                                                                <div class="pxp-single-candidate-timeline-info ms-3">
                                                                    <div class="pxp-single-candidate-timeline-time">
                                                                        <span class="me-3" style="white-space: nowrap">
                                                                            {{ Verta($workExperience->start_date)->year }} -
                                                                            {{ Verta($workExperience->end_date)->year }}
                                                                        </span>
                                                                    </div>
                                                                    <div class="pxp-single-candidate-timeline-position mt-2">
                                                                        {{ $workExperience->title }}
                                                                    </div>
                                                                    <div
                                                                        class="pxp-single-candidate-timeline-company pxp-text-light">
                                                                        {{ $workExperience->company }}
                                                                    </div>
                                                                    <div class="pxp-single-candidate-timeline-about mt-2 pb-4">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </section>
                        @endif
                    </div>
                    <div class="col-lg-5 col-xl-4 col-xxl-3">
                        @if ((!$isConfirmed && Auth::check() && (Auth::id() == $user->id || Auth::user()->hasRole('employee'))) || $isConfirmed)
                            @if ($show_info || $show_resume)
                                @if ($user->hasRole('student'))
                                    <div class="pxp-single-candidate-side-panel mt-5 mt-lg-0">
                                        <div>
                                            <div class="pxp-single-candidate-side-info-label pxp-text-light">ایمیل</div>
                                            <div class="pxp-single-candidate-side-info-data">{{$user->groupable->email}}</div>
                                        </div>
                                        @if ($user->groupable->city)
                                            <div class="mt-4">
                                                <div class="pxp-single-candidate-side-info-label pxp-text-light">شهر</div>
                                                <div class="pxp-single-candidate-side-info-data">{{$user->groupable->city->name}}
                                                </div>
                                            </div>
                                        @endif
                                        <div class="mt-4">
                                            <div class="pxp-single-candidate-side-info-label pxp-text-light">تلفن</div>
                                            <div dir="ltr" class="pxp-single-candidate-side-info-data">{{ $user->mobile }}
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="pxp-single-candidate-side-panel mt-5 mt-lg-0">
                                        <div class="mt-4">
                                            <div class="pxp-single-candidate-side-info-label pxp-text-light">استان</div>
                                            <div class="pxp-single-candidate-side-info-data">{{ $user->groupable->province->name }}
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <div class="pxp-single-candidate-side-info-label pxp-text-light">شهر</div>
                                            <div class="pxp-single-candidate-side-info-data">{{ $user->groupable->city->name }}
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <div class="pxp-single-candidate-side-info-label pxp-text-light">تاریخ تولد</div>
                                            <div dir="ltr" class="pxp-single-candidate-side-info-data">{{ verta($user->groupable->birth)->formatDate() }}
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <div class="pxp-single-candidate-side-info-label pxp-text-light">وضعیت تاهل</div>
                                            <div dir="ltr" class="pxp-single-candidate-side-info-data">{{ $user->groupable->marital_status }}
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <div class="pxp-single-candidate-side-info-label pxp-text-light">وضعیت نظام وظیفه</div>
                                            <div dir="ltr" class="pxp-single-candidate-side-info-data">{{ $user->groupable->military_status }}
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <div class="pxp-single-candidate-side-info-label pxp-text-light">حقوق درخواستی</div>
                                            <div dir="rtl" class="pxp-single-candidate-side-info-data">
                                                
                                                {{ $user->groupable->requested_salary }}
                                                <span>میلیون تومان</span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                            <div class="pxp-single-candidate-side-panel mt-4 mt-lg-5">
                                @auth
                                    @if (Auth::user()->groupable_type == \App\Models\CompanyInfo::class)
                                        <h3>ارسال درخواست همکاری</h3>
                                        <div class="mb-3">
                                            @if ($user->hasRole('student'))
                                                <label for="type" class="form-label">موقعیت کارآموزی - شغلی</label>
                                                <select name="type" class="form-control form-select"
                                                    style="background-position: left 20px center!important;" id="job">
                                                    <option value="">انتخاب کنید</option>
                                                    @foreach ($job_positions as $job)
                                                        <option value="{{$job->id}}">
                                                            {{job_type_persian($job->type)}} -
                                                            {{$job->title}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <label for="type" class="form-label">موقعیت شغلی</label>
                                                <select name="type" class="form-control form-select"
                                                    style="background-position: left 20px center!important;" id="job">
                                                    <option value="">انتخاب کنید</option>
                                                    @foreach ($job_positions as $job)
                                                        @if ($job->type == 'part-time' || $job->type == 'full-time' || $job->type == 'remote')
                                                            <option value="{{$job->id}}">
                                                                {{job_type_persian($job->type)}} -
                                                                {{$job->title}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                        @if (is_null($user_request))
                                            <a href="javascript:void(0)" id="work-request"
                                            class="btn rounded-pill pxp-section-cta d-block">ارسال درخواست</a>
                                        @else
                                            <a href="#" disabled="disabled"
                                            class="btn rounded-pill pxp-section-cta d-block disabled">شما قبلا برای این
                                            کارجو درخواست فرستاده اید</a>
                                        @endif
                                    @endif
                                @else
                                    <a href="{{route('login')}}" 
                                    class="btn rounded-pill pxp-section-cta d-block">ابتدا باید وارد شوید</a>
                                @endauth
                            </div>
                            

                            {{-- <div class="pxp-single-candidate-side-panel mt-4 mt-lg-5">
                                <h3>ارسال درخواست همکاری</h3>
                                <div class="mb-3">
                                    <label for="type" class="form-label">نوع همکاری</label>
                                    <select name="type" class="form-control form-select"
                                        style="background-position: left 20px center!important;" id="type">
                                        @if ($user->hasRole('student'))
                                            <option value="intern">کارآموزی</option>
                                        @endif
                                        <option value="hire">استخدام</option>
                                    </select>
                                </div>
                                @auth
                                    @if (\Illuminate\Support\Facades\Auth::user()->groupable_type == \App\Models\CompanyInfo::class)
                                        @if (is_null($user_request))
                                            <a href="javascript:void(0)" id="work-request"
                                                class="btn rounded-pill pxp-section-cta d-block">ارسال درخواست</a>
                                        @else
                                            <a href="#" disabled="disabled"
                                                class="btn rounded-pill pxp-section-cta d-block disabled">شما قبلا برای این
                                                کارجو درخواست فرستاده اید</a>

                                        @endif
                                    @else
                                        <a href="#" disabled="disabled"
                                            class="btn rounded-pill pxp-section-cta d-block disabled">فقط شرکت ها مجاز به
                                            ارسال درخواست همکاری با دانشجو هستند</a>

                                    @endif
                                @else
                                        <a href="#" disabled="disabled"
                                            class="btn rounded-pill pxp-section-cta d-block disabled">ابتدا باید وارد شوید</a>
                                @endauth
                                

                            </div> --}}
                            @if (count($user->social_networks) > 0)
                                <div class="pxp-single-candidate-side-panel mt-4 mt-lg-5">
                                    {{-- شبکه ها اجتماعی --}}
                                    <div class="mt-4 mt-lg-3">
                                        <h3 class="mb-4 mb-lg-4">شبکه های اجتماعی</h3>
                                        @foreach ($user->social_networks as $socialNetwork)
                                            <div class="pxp-single-candidate-skills">
                                                <div class="row bg-white my-2 p-2 border" style="border-radius: 1rem !important;">
                                                    <div class="col-3 pe-2 ps-3">
                                                        {{ social_network_icons($socialNetwork->name) }}
                                                    </div>
                                                    <div class="col-9 d-flex align-items-center ps-0">
                                                        <span
                                                            class="badge px-3 py-2 rounded-pill bg-info text-dark d-inline">{{ social_network_link($socialNetwork) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $('#work-request').on('click', function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            })
            let job = $('#job').val()
            let user = '{{ $user->id }}'
            $.ajax({
                type: 'POST',
                url: "{{ route('front.ajax.co-cooperation-request') }}",
                data: {
                    job: job,
                    receiver: user
                },
                success: function(data) {
                    Swal.fire({
                        icon: 'success',
                        title: 'درخواست شما با موفقیت ارسال شد',
                        showConfirmButton: false,
                        timer: 3500
                    })
                    $('#work-request').addClass('disabled')
                    $('#work-request').addAttribute('disabled')
                },
                error: function(data) {
                    console.log(data.error)
                }

            })
        })
    </script>
@endsection
