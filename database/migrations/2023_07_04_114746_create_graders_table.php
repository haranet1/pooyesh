<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

//This Model Represents Students who are at Highschool

class CreateGradersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('graders', function (Blueprint $table) {
            $table->id();
            $table->integer('national_code');
            $table->string('name');
            $table->string('family');
            $table->char('mobile');
            $table->char('parent_mobile');
            $table->string('grade');
            $table->string('edu_branch');
            $table->string('gender');
            $table->string('school');
            $table->text('suggestion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('graders');
    }
}
