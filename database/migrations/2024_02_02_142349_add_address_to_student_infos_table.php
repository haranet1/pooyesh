<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressToStudentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_infos', function (Blueprint $table) {
            $table->integer('duration')->nullable()->change();
            $table->text('address')->after('city_id')->nullable();
            $table->string('Plaque')->after('address')->nullable();
            $table->string('post_code')->after('Plaque')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_infos', function (Blueprint $table) {
            $table->integer('duration')->change();
            $table->dropColumn('address');
            $table->dropColumn('Plaque');
            $table->dropColumn('post_code');
        });
    }
}
