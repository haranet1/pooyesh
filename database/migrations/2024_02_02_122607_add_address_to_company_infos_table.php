<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressToCompanyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_infos', function (Blueprint $table) {
            $table->foreignId('province_id')->after('email')->nullable();
            $table->foreignId('city_id')->after('province_id')->nullable();
            $table->text('address')->after('city_id')->nullable();
            $table->string('Plaque')->after('address')->nullable();
            $table->string('post_code')->after('Plaque')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_infos', function (Blueprint $table) {
            $table->dropColumn('province_id');
            $table->dropColumn('city_id');
            $table->dropColumn('address');
            $table->dropColumn('Plaque');
            $table->dropColumn('post_code');
        });
    }
}
