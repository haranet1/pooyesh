<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePooyeshRequestProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pooyesh_request_processes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('req_id');
            $table->dateTime('accepted_by_co_at')->nullable();
            $table->dateTime('accepted_by_std_at')->nullable();
            $table->dateTime('canceled_by_co_at')->nullable();
            $table->dateTime('canceled_by_std_at')->nullable();
            $table->dateTime('rejected_by_co_at')->nullable();
            $table->dateTime('rejected_by_std_at')->nullable();
            $table->dateTime('accepted_by_uni_at')->nullable();
            $table->dateTime('rejected_by_uni_at')->nullable();
            $table->dateTime('done_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pooyesh_request_processes');
    }
}
