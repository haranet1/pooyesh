<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameOJpLikesTableAndModifyColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('o_jp_likes', function (Blueprint $table) {
            
            $table->rename('o_jp_favorites');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('o_jp_likes', function (Blueprint $table) {
            $table->rename('o_jp_likes');
        });
    }
}
