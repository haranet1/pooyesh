<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSadrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sadras', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignId('photo_id')->nullable();
            $table->foreignId('map_id')->nullable();
            $table->dateTime('start_at');
            $table->dateTime('end_at');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sadras');
    }
}
