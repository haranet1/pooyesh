<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOjobTechnologySkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ojob_technology_skills', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('job_id')->nullable();
            $table->foreign('job_id')->references('id')->on('ojobs');

            $table->unsignedBigInteger('technology_skill_id')->nullable();
            $table->foreign('technology_skill_id')->references('id')->on('otechnology_skills');
            
            $table->string('hot_technology')->nullable();
            $table->string('in_demand')->nullable();
            $table->foreignId('example_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ojob_technology_skills');
    }
}
