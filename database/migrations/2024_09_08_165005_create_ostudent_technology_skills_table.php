<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOstudentTechnologySkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ostudent_technology_skills', function (Blueprint $table) {
            $table->id();
            $table->foreignId('student_id');
            $table->foreignId('otechnology_skill_id');
            $table->integer('example_id');
            $table->string('title')->nullable();
            $table->integer('level');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ostudent_technology_skills');
    }
}
