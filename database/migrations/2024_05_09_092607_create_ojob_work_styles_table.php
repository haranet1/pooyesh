<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOjobWorkStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ojob_work_styles', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('job_id')->nullable();
            $table->foreign('job_id')->references('id')->on('ojobs');
            $table->unsignedBigInteger('work_style_id')->nullable();
            $table->foreign('work_style_id')->references('id')->on('owork_styles');
            $table->unsignedBigInteger('importance_id')->nullable();
            $table->foreign('importance_id')->references('id')->on('oimportances');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ojob_work_styles');
    }
}
