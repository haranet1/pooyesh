<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentDownFieldsToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string('tracking_code')->after('status')->nullable();
            $table->string('card_number')->after('receipt')->nullable();
            $table->string('payer_bank')->after('card_number')->nullable();
            $table->string('payment_date_time')->after('payer_bank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('tracking_code');
            $table->dropColumn('card_number');
            $table->dropColumn('payer_bank');
            $table->dropColumn('payment_date_time');
        });
    }
}
