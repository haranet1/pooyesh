<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldInPooyeshRequestProcess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pooyesh_request_processes', function (Blueprint $table) {
            
            $table->dateTime('course_unit_registration_at')->nullable()->after('contract_printed_at');
            $table->integer('score')->nullable()->after('course_unit_registration_at');
            $table->dateTime('score_registration_at')->nullable()->after('done_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pooyesh_request_processes', function (Blueprint $table) {
            //
        });
    }
}
