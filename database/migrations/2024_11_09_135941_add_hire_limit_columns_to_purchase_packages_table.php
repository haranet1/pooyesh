<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHireLimitColumnsToPurchasePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_packages', function (Blueprint $table) {
            $table->integer('additional_hire_positions')->after('type');
            $table->integer('additional_hire_requests')->after('type');
            $table->integer('additional_hire_confirms')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_packages', function (Blueprint $table) {
            $table->dropColumn('additional_hire_positions');
            $table->dropColumn('additional_hire_requests');
            $table->dropColumn('additional_hire_confirms');
        });
    }
}
