<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_tickets', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id');
            $table->foreignId('event_id');
            $table->foreignId('event_info_id');
            $table->foreignId('discount_id')->nullable();
            $table->string('discount_price')->nullable();
            $table->string('payable_price');
            $table->integer('count')->default(1);
            $table->string('receipt')->nullable();
            $table->boolean('status');
            $table->boolean('deleted')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_tickets');
    }
}
