<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_infos', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('year');
            $table->string('n_code'); //NATIONAL CODE
            $table->string('phone');
            $table->string('website')->nullable();
            $table->text('team')->nullable();
            $table->text('rewards')->nullable();
            $table->string('registration_number');
            $table->string('activity_field');
            $table->string('ceo')->nullable();
            $table->string('hr_manager')->nullable();
            $table->string('size');
            $table->text('about');
            $table->text('email')->nullable();
            $table->text('city');
            $table->boolean('status')->default(0);
            $table->foreignId('logo_id')->nullable();
            $table->foreignId('header_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_infos');
    }
}
