<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreateInternshipLimitToCompanyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_infos', function (Blueprint $table) {
            $table->integer('create_internship_limit')->after('about')->default(3);
            $table->integer('request_internship_limit')->after('about')->default(3);
            $table->integer('confirm_internship_limit')->after('about')->default(3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_infos', function (Blueprint $table) {
            $table->dropColumn('create_internship_limit');
            $table->dropColumn('request_internship_limit');
            $table->dropColumn('confirm_internship_limit');
        });
    }
}
