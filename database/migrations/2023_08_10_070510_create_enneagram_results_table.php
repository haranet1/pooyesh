<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnneagramResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enneagram_results', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('A')->default(0);
            $table->integer('B')->default(0);
            $table->integer('C')->default(0);
            $table->string('main_character')->nullable();
            $table->integer('main_character_score')->default(0);
            $table->string('second_character')->nullable();
            $table->integer('second_character_score')->default(0);
            $table->string('weak_character')->nullable();
            $table->integer('weak_character_score')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enneagram_results');
    }
}
