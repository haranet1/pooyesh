<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHaalandResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('haaland_results', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('R');
            $table->integer('I');
            $table->integer('A');
            $table->integer('S');
            $table->integer('E');
            $table->integer('C');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('haaland_results');
    }
}
