<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('price');
            $table->unsignedInteger('dimensions'); // size of the booth (GHORFEH)
            $table->unsignedInteger('tables_qty')->default(0);
            $table->unsignedInteger('chairs_qty')->default(0);
            $table->boolean('deleted')->default(0);
            $table->foreignId('sadra_id');
            $table->foreignId('map_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_plans');
    }
}
