<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStartEndRegisterColumnToSadrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sadras', function (Blueprint $table) {
            $table->addColumn('dateTime','start_register_at')->nullable();
            $table->addColumn('dateTime','end_register_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sadras', function (Blueprint $table) {
            $table->dropColumn('start_register_at');
            $table->dropColumn('end_register_at');
        });
    }
}
