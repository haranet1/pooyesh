<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuggestedTestJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggested_test_jobs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('testable_id');
            $table->string('testable_type');
            $table->boolean('status')->nullable();
            $table->string('option');
            $table->foreignId('receipt_id')->nullable();
            $table->bigInteger('discount_id')->nullable();
            $table->string('discount_price')->nullable();
            $table->string('payable_price')->nullable();
            $table->timestamp('updated_result');
            $table->boolean('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggested_test_jobs');
    }
}
