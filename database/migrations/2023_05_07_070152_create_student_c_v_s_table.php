<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentCVSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('student_c_v_s')){
        Schema::create('student_c_v_s', function (Blueprint $table) {
            $table->id();
            $table->foreignId('student_id');
            $table->string('name');
            $table->string('sex');
            $table->string('marital_status');
            $table->string('military_status')->nullable();
            $table->string('city');
            $table->dateTime('birth');
            $table->string('minimum_salary');
            $table->foreignId('fav_job_id');
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_c_v_s');
    }
}
