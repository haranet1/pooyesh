<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldToDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discounts', function (Blueprint $table) {
            $table->boolean('company_acceptance')->after('description')->default(0);
            $table->boolean('student_acceptance')->after('company_acceptance')->default(0);
            $table->boolean('applicant_acceptance')->after('student_acceptance')->default(0);
            $table->boolean('deleted')->after('applicant_acceptance')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discounts', function (Blueprint $table) {
            //
        });
    }
}
