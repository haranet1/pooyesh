<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGhqResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ghq_results', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('physical');
            $table->integer('anxiety');
            $table->integer('social');
            $table->integer('depression');
            $table->integer('sum_of_all');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ghq_results');
    }
}
