<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldToJobPositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_positions', function (Blueprint $table) {
            $table->foreignId('province_id');
            $table->foreignId('city_id')->nullable();
            $table->integer('age');
            $table->string('sex');
            $table->string('academic_level');
            $table->string('marige_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_positions', function (Blueprint $table) {
            $table->dropColumn('province_id');
            $table->dropColumn('city_id');
            $table->dropColumn('age');
            $table->dropColumn('sex');
            $table->dropColumn('academic_level');
            $table->dropColumn('marige_type');
        });
    }
}
