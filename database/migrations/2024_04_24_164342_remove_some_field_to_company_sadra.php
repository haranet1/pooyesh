<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSomeFieldToCompanySadra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_sadras', function (Blueprint $table) {
            
            $table->dropColumn('dont_want_engineer');
            $table->dropColumn('electrical_engineer');
            $table->dropColumn('mechanical_engineer');
            $table->dropColumn('material_engineer');
            $table->dropColumn('industriall_engineer');
            $table->dropColumn('civil_engineer');
            $table->dropColumn('computer_engineer');
            $table->dropColumn('medical_engineer');
            $table->dropColumn('other_cases_engineer');

            $table->dropColumn('dont_want_humanities');
            $table->dropColumn('management');
            $table->dropColumn('rights');
            $table->dropColumn('accounting');
            $table->dropColumn('literature');
            $table->dropColumn('english');
            $table->dropColumn('history_and_geography');
            $table->dropColumn('other_cases_humanities');

            $table->dropColumn('dont_want_medical');
            $table->dropColumn('medical');
            $table->dropColumn('nursing');
            $table->dropColumn('midwifery');
            $table->dropColumn('surgeryRoom');
            $table->dropColumn('hygiene');
            $table->dropColumn('psychology');
            $table->dropColumn('other_cases_medical');

            $table->dropColumn('dont_want_science');
            $table->dropColumn('math');
            $table->dropColumn('physics');
            $table->dropColumn('chemistry');
            $table->dropColumn('biology');
            $table->dropColumn('other_cases_science');

            $table->dropColumn('general');
            $table->dropColumn('expertise');
            $table->dropColumn('dont_want_marketing');

            $table->dropColumn('student');
            $table->dropColumn('graduate');
            $table->dropColumn('advice_from_professors');
            $table->dropColumn('other_cases_cooperation');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_sadras', function (Blueprint $table) {
            //
        });
    }
}
