<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnneagramPersonalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enneagram_personalities', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->text('description');
            $table->string('most_fear')->nullable();
            $table->string('most_desire')->nullable();
            $table->text('motivations')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enneagram_personalities');
    }
}
