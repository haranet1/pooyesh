<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOJpKnowledgeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('o_jp_knowledge', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('job_position_id');
            $table->foreign('job_position_id')->references('id')->on('ojob_positions');
            
            $table->unsignedBigInteger('knowledge_id')->nullable();
            $table->foreign('knowledge_id')->references('id')->on('oknowledges');

            $table->string('title')->nullable();
            $table->integer('importance')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('o_jp_knowledge');
    }
}
