<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSomeFieldInOjobPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ojob_positions', function (Blueprint $table) {
            
            $table->string('salary')->nullable()->after('working_hours');
            
            $table->dropColumn('major');

            $table->foreignId('major_id')->nullable()->after('grade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ojob_positions', function (Blueprint $table) {
            //
        });
    }
}
