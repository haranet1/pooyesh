<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJobsAndMajorColumnToMbtiPersonalities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mbti_personalities', function (Blueprint $table) {
            $table->text('career')->nullable()->after('other');
            $table->text('majors')->nullable()->after('other');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mbti_personalities', function (Blueprint $table) {
            $table->dropColumn('career');
            $table->dropColumn('majors');

        });
    }
}
