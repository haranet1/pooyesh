<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCooperationRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cooperation_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('job_position_id');
            $table->foreignId('receiver_id');
            $table->foreignId('sender_id');
            $table->string('type');
            $table->boolean('status')->nullable();
            $table->foreignId('certificate_id')->nullable();
            $table->foreignId('contract_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cooperation_requests');
    }
}
