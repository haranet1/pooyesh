<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanySadraMajorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_sadra_majors', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('company_sadra_id');
            $table->foreign('company_sadra_id')->references('id')->on('company_sadras');

            $table->string('college');
            $table->string('major');
            $table->string('number');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_sadra_majors');
    }
}
