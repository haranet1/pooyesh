<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_infos', function (Blueprint $table) {
            $table->id();
            $table->string('age');
            $table->string('major');
            $table->string('skills')->nullable();
            $table->text('about')->nullable();
            $table->foreignId('university_id');
            $table->foreignId('province_id');
            $table->foreignId('city_id');
            $table->string('email');
            $table->char('n_code'); //student national code
            $table->char('std_number');
            $table->integer('duration');
            $table->string('grade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_infos');
    }
}
