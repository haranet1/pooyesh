<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNeoResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('neo_results', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('N');
            $table->integer('E');
            $table->integer('O');
            $table->integer('A');
            $table->integer('C');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('neo_results');
    }
}
