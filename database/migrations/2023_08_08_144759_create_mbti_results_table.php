<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMbtiResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbti_results', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('personality_type');
            $table->integer('I');
            $table->integer('E');
            $table->integer('N');
            $table->integer('S');
            $table->integer('T');
            $table->integer('F');
            $table->integer('J');
            $table->integer('P');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbti_results');
    }
}
