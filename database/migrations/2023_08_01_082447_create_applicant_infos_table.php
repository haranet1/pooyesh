<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\MilitaryStatus;
use App\Enums\MaritalStatus;

class CreateApplicantInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_infos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('province_id');
            $table->foreignId('city_id');
            $table->date('birth');
            $table->enum('military_status',MilitaryStatus::toValues())->default(MilitaryStatus::OTHER());
            $table->enum('marital_status',MaritalStatus::toValues())->default(MaritalStatus::OTHER());
            $table->text('address');
            $table->string('requested_salary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_infos');
    }
}
