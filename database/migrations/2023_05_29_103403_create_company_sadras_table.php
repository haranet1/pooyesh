<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanySadrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_sadras', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id');
            $table->foreignId('sadra_id');
            $table->foreignId('booth_id');
            $table->boolean('status')->nullable();
            $table->foreignId('receipt_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_sadras');
    }
}
