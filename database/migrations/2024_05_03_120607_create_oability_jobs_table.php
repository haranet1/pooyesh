<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOabilityJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oability_jobs', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('job_id')->nullable();
            $table->foreign('job_id')->references('id')->on('ojobs');

            $table->unsignedBigInteger('ability_id')->nullable();
            $table->foreign('ability_id')->references('id')->on('oabilities');

            $table->unsignedBigInteger('level_id')->nullable();
            $table->foreign('level_id')->references('id')->on('olevels');

            $table->unsignedBigInteger('importance_id')->nullable();
            $table->foreign('importance_id')->references('id')->on('oimportances');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oability_jobs');
    }
}
