<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContractPrintedAtFieldToPooyeshRequestProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pooyesh_request_processes', function (Blueprint $table) {
            $table->dateTime('contract_printed_at')->after('rejected_by_uni_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pooyesh_request_processes', function (Blueprint $table) {
            $table->dropColumn('contract_printed_at');
        });
    }
}
