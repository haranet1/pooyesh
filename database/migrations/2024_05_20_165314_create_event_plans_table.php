<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_plans', function (Blueprint $table) {
            $table->id();

            $table->foreignId('event_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('price')->default(0);
            $table->integer('count');
            $table->foreignId('photo_id')->nullable();
            $table->boolean('deleted')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_plans');
    }
}
