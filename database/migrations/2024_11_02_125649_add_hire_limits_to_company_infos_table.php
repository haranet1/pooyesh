<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHireLimitsToCompanyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_infos', function (Blueprint $table) {
            $table->integer('create_hire_limit')->after('about')->default(3);
            $table->integer('request_hire_limit')->after('about')->default(3);
            $table->integer('confirm_hire_limit')->after('about')->default(3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_infos', function (Blueprint $table) {
            $table->dropColumn('create_hire_limit');
            $table->dropColumn('request_hire_limit');
            $table->dropColumn('confirm_hire_limit');
        });
    }
}
