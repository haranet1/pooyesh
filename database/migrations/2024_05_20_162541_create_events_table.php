<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();

            $table->string('title');
            $table->text('description');
            $table->foreignId('poster_id')->nullable();
            $table->foreignId('banner_id')->nullable();
            $table->foreignId('map_id')->nullable();

            $table->boolean('status')->default(0);
            $table->boolean('is_active')->default(1);
            $table->boolean('is_free')->default(0);
            $table->boolean('can_multy_buy')->default(0);
            $table->boolean('company_acceptance')->default(0);
            $table->boolean('student_acceptance')->default(0);
            $table->boolean('applicant_acceptance')->default(0);
            $table->boolean('deleted')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
