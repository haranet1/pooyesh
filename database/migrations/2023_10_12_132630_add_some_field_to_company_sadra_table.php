<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldToCompanySadraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_sadras', function (Blueprint $table) {
            $table->string('coordinator_name')->after('booth_id');
            $table->string('coordinator_mobile')->after('coordinator_name');
            
            $table->boolean('dont_want_engineer')->default(0);
            $table->integer('electrical_engineer')->default(0);
            $table->integer('mechanical_engineer')->default(0);
            $table->integer('material_engineer')->default(0);
            $table->integer('industriall_engineer')->default(0);
            $table->integer('civil_engineer')->default(0);
            $table->integer('computer_engineer')->default(0);
            $table->integer('medical_engineer')->default(0);
            $table->integer('other_cases_engineer')->default(0);

            $table->boolean('dont_want_humanities')->default(0);
            $table->integer('management')->default(0);
            $table->integer('rights')->default(0);
            $table->integer('accounting')->default(0);
            $table->integer('literature')->default(0);
            $table->integer('english')->default(0);
            $table->integer('history_and_geography')->default(0);
            $table->integer('other_cases_humanities')->default(0);

            $table->boolean('dont_want_medical')->default(0);
            $table->integer('medical')->default(0);
            $table->integer('nursing')->default(0);
            $table->integer('midwifery')->default(0);
            $table->integer('surgeryRoom')->default(0);
            $table->integer('hygiene')->default(0);
            $table->integer('psychology')->default(0);
            $table->integer('other_cases_medical')->default(0);
            
            $table->boolean('dont_want_science')->default(0);
            $table->integer('math')->default(0);
            $table->integer('physics')->default(0);
            $table->integer('chemistry')->default(0);
            $table->integer('biology')->default(0);
            $table->integer('other_cases_science')->default(0);

            $table->integer('general')->default(0);
            $table->integer('expertise')->default(0);
            $table->integer('dont_want_marketing')->default(0);

            $table->boolean('student')->default(false);
            $table->boolean('graduate')->default(false);
            $table->boolean('advice_from_professors')->default(false);
            $table->boolean('other_cases_cooperation')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_sadras', function (Blueprint $table) {
            //
        });
    }
}
