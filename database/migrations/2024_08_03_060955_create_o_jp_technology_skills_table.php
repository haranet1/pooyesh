<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOJpTechnologySkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('o_jp_technology_skills', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('job_position_id');
            $table->foreign('job_position_id')->references('id')->on('ojob_positions');
            
            $table->unsignedBigInteger('technology_skill_id')->nullable();
            $table->foreign('technology_skill_id')->references('id')->on('otechnology_skills');

            $table->unsignedBigInteger('job_technology_skill_id')->nullable();
            $table->foreign('job_technology_skill_id')->references('id')->on('ojob_technology_skills');

            $table->string('title')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('o_jp_technology_skills');
    }
}
