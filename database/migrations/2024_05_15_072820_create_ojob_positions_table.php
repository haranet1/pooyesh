<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOjobPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ojob_positions', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('users');

            $table->unsignedBigInteger('job_id');
            $table->foreign('job_id')->references('id')->on('ojobs');

            $table->text('title');
            $table->text('description');

            $table->string('type');
            $table->string('level')->nullable();
            $table->string('experience')->nullable();
            $table->string('working_hours')->nullable();

            $table->string('age')->nullable();
            $table->string('sex');
            $table->string('marige_type')->nullable();
            $table->string('military_service_status')->nullable();

            $table->string('grade')->nullable();
            $table->string('major')->nullable();

            $table->foreignId('province_id');
            $table->foreignId('city_id');
            $table->text('address');
            
            $table->text('about')->nullable();

            $table->boolean('status')->nullable();
            $table->boolean('active')->default(1);
            $table->boolean('deleted')->default(0);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ojob_positions');
    }
}
