<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMbtiPersonalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbti_personalities', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('fa_title');
            $table->string('photo_path')->nullable();
            $table->text('analyse')->nullable();
            $table->text('work_analyse')->nullable();
            $table->text('relations_analyse')->nullable();
            $table->text('leisure_time_analyse')->nullable();
            $table->text('other')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbti_personalities');
    }
}
