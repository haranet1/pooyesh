<?php

namespace Database\Seeders;

use App\Imports\OknowledgeImport;
use App\Models\Oimportance;
use App\Models\Ojob;
use App\Models\OjobKnowledge;
use App\Models\Oknowledge;
use App\Models\Olevel;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OjobKnowledgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path1 = public_path('assets/docs/1_knowledge.xlsx');
        $path2 = public_path('assets/docs/2_knowledge.xlsx');
        $path3 = public_path('assets/docs/3_knowledge.xlsx');

        $collection1 = Excel::toCollection(new OknowledgeImport , $path1);
        $collection2 = Excel::toCollection(new OknowledgeImport , $path2);
        $collection3 = Excel::toCollection(new OknowledgeImport , $path3);

        foreach($collection1 as $col1){
            $know1 = $col1;
        }
        foreach($collection2 as $col2){
            $know2 = $col2;
        }
        foreach($collection3 as $col3){
            $know3 = $col3;
        }

        $knowledges = $know1->merge($know2);
        $knowledges = $knowledges->merge($know3);

        $jobElement = null;
        $knowledgeElement = null;
        $importanceElemnt = null;
        $levelElement = null;
        $jobKnowledge = null;

        foreach($knowledges as $knowledge){

            
            $jobElement = Ojob::where('code' , $knowledge['code'])->first();
            $knowledgeElement = Oknowledge::where('code' , $knowledge['knowledge_code'])->first();


            if( $jobElement['code'] == $knowledge['code'] and $knowledgeElement['code'] == $knowledge['knowledge_code'] and $knowledge['scale_name'] == 'Importance'){

                $jobKnowledge = OjobKnowledge::create([
                    'job_id' => $jobElement['id'],
                    'knowledge_id' => $knowledgeElement['id'],
                ]);
            }

            if($knowledge['scale_name'] == 'Importance'){

                $importanceElemnt = Oimportance::create([
                    'data_value' => $knowledge['data_value'] ?? 0,
                    'lower' => $knowledge['lower_bound'] ?? 0,
                    'upper' => $knowledge['upper_bound'] ?? 0,
                ]);

                $jobKnowledge->update([
                    'importance_id' => $importanceElemnt['id']
                ]);

            }else{

                $levelElement = Olevel::create([
                    'data_value' => $knowledge['data_value'] ?? 0,
                    'lower' => $knowledge['lower_bound'] ?? 0,
                    'upper' => $knowledge['upper_bound'] ?? 0,
                ]);

                $jobKnowledge->update([
                    'level_id' => $levelElement['id']
                ]);

            }

            $jobElement = null;
            $knowledgeElement = null;
            $importanceElemnt = null;
            $levelElement = null;
        }

        $this->command->info('job knowledge seed to ojob_knowledges table');
    }
}
