<?php

namespace Database\Seeders;

use App\Models\CooperationRequest;
use App\Models\PooyeshRequestProcess;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CooperationRequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r = DB::table('cooperation_requests')
            ->select('job_position_id', 'sender_id', 'receiver_id', DB::raw('COUNT(*) as count'))
            ->groupBy('job_position_id', 'sender_id', 'receiver_id')
            ->having('count', '>', 1)
            ->get();

        $arr=[];
        foreach($r as $rr){

            $hi = CooperationRequest::where('job_position_id' , $rr->job_position_id)
            ->where('sender_id' , $rr->sender_id)
            ->where('receiver_id' , $rr->receiver_id)
            ->get();
            $c=1;
            foreach($hi as $h){
                if($c==1){
                    $c++;
                    continue;
                }else
                    array_push($arr,$h->id);
            }
        }
        CooperationRequest::whereIn('id', $arr)->delete();
        PooyeshRequestProcess::whereIn('req_id', $arr)->delete();

        $job_arr = array();
        $jobs = CooperationRequest::whereDoesntHave('job')->get();

        foreach($jobs as $job){
            array_push($job_arr , $job->id);
        }

        CooperationRequest::whereIn('id', $job_arr)->delete();
        PooyeshRequestProcess::whereIn('req_id', $job_arr)->delete();


        $req_arr = array();
        $reqs = CooperationRequest::whereColumn('sender_id' ,'receiver_id')->get();

        foreach($reqs as $req){
            array_push($req_arr , $req->id);
        }

        CooperationRequest::whereIn('id', $req_arr)->delete();
        PooyeshRequestProcess::whereIn('req_id', $req_arr)->delete();

        

        $idarr = array();
        $id = CooperationRequest::with('process')->whereRelation('sender' , 'status' , 0)->get();

        foreach($id as $ids){
            array_push($idarr , $ids->id);
        }
        $q = CooperationRequest::whereIn('id' , $idarr)->delete();
        $v = PooyeshRequestProcess::whereIn('req_id', $idarr)->delete();
        
        $this->command->info($q);
        
    }
}
