<?php

namespace Database\Seeders;

use App\Imports\OabilityImport;
use App\Models\Oability;
use App\Models\OabilityJob;
use App\Models\Oimportance;
use App\Models\Ojob;
use App\Models\Olevel;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OabilityJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path1 = public_path('assets/docs/1_ability.xlsx');
        $path2 = public_path('assets/docs/2_ability.xlsx');
        $path3 = public_path('assets/docs/3_ability.xlsx');
        $path4 = public_path('assets/docs/4_ability.xlsx');
        $path5 = public_path('assets/docs/5_ability.xlsx');

        $abilityCollection1 = Excel::toCollection(new OabilityImport , $path1);
        $abilityCollection2 = Excel::toCollection(new OabilityImport , $path2);
        $abilityCollection3 = Excel::toCollection(new OabilityImport , $path3);
        $abilityCollection4 = Excel::toCollection(new OabilityImport , $path4);
        $abilityCollection5 = Excel::toCollection(new OabilityImport , $path5);

        foreach($abilityCollection1 as $ability1){
            $abis1 = $ability1;
        }
        foreach($abilityCollection2 as $ability2){
            $abis2 = $ability2;
        }
        foreach($abilityCollection3 as $ability3){
            $abis3 = $ability3;
        }
        foreach($abilityCollection4 as $ability4){
            $abis4 = $ability4;
        }
        foreach($abilityCollection5 as $ability5){
            $abis5 = $ability5;
        }

        $abilities = $abis1->merge($abis2);
        $abilities = $abilities->merge($abis3);
        $abilities = $abilities->merge($abis4);
        $abilities = $abilities->merge($abis5);


        $job_codes = [];
        $ability_codes = [];


        foreach($abilities as $abili){
            if(! in_array($abili['code'], $job_codes)){
                array_push($job_codes , $abili['code']);
            }
            if(! in_array($abili['ability_code'], $ability_codes)){
                array_push($ability_codes , $abili['ability_code']);
            }
        }

        
        $jobsModels = Ojob::whereIn('code' , $job_codes)->get();
        $abilityModel = Oability::whereIn('code' , $ability_codes)->get();
        

        $jobElement = null;
        $abilityElement = null;
        $importanceElemnt = null;
        $levelElement = null;
        $abilityJob = null;

        foreach($abilities as $abi){
            $jobElement = $jobsModels->where('code' , $abi['code'])->first();
            $abilityElement = $abilityModel->where('code' , $abi['ability_code'])->first();
            
            if( $jobElement['code'] == $abi['code'] and $abilityElement['code'] == $abi['ability_code'] and $abi['scale_name'] == 'Importance'){

                $abilityJob = OabilityJob::create([
                    'job_id' => $jobElement['id'],
                    'ability_id' => $abilityElement['id'],
                ]);

            }

            if($abi['scale_name'] == 'Importance'){

                $importanceElemnt = Oimportance::create([
                    'data_value' => $abi['data_value'] ?? 0,
                    'lower' => $abi['lower_bound'] ?? 0,
                    'upper' => $abi['upper_bound'] ?? 0,
                ]);

                $abilityJob->update([
                    'importance_id' => $importanceElemnt['id']
                ]);

            }else{

                $levelElement = Olevel::create([
                    'data_value' => $abi['data_value'] ?? 0,
                    'lower' => $abi['lower_bound'] ?? 0,
                    'upper' => $abi['upper_bound'] ?? 0,
                ]);

                $abilityJob->update([
                    'level_id' => $levelElement['id']
                ]);

            }

            $jobElement = null;
            $abilityElement = null;
            $importanceElemnt = null;
            $levelElement = null;
        }

        $this->command->info('Ability jobs Seeding Is Done!');
    }
}
