<?php

namespace Database\Seeders;

use App\Imports\OcategoryImport;
use App\Models\Ocategory;
use App\Models\Ojob;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catInfos = [
            [
                'title' => 'معماری و مهندسی',
                'en_title' => 'Architecture and Engineering',
                'path' => public_path('assets/docs/job_category/Architecture_and_Engineering.xlsx')
            ],
            [
                'title'=>'هنر، طراحی، سرگرمی، ورزش و رسانه',
                'en_title'=>'Arts, Design, Entertainment, Sports, and Media	',
                'path' => public_path('assets/docs/job_category/Arts_Design_Entertainment_Sports_and_Media.xlsx')
            ],
            [
                'title'=>'نظافت و نگهداری ساختمان و زمین',
                'en_title'=>'Building and Grounds Cleaning and Maintenance',
                'path' => public_path('assets/docs/job_category/Building_and_Grounds_Cleaning_and_Maintenance.xlsx')
            ],
            [
                'title'=>'عملیات تجاری و مالی',
                'en_title'=>'Business and Financial Operations',
                'path' => public_path('assets/docs/job_category/Business_and_Financial_Operations.xlsx')
            ],
            [
                'title'=>'خدمات اجتماعی',
                'en_title'=>'Community and Social Service',
                'path' => public_path('assets/docs/job_category/Community_and_Social_Service.xlsx')
            ],
            [
                'title'=>'کامپیوتر و ریاضی',
                'en_title'=>'Computer and Mathematical',
                'path' => public_path('assets/docs/job_category/Computer_and_Mathematical.xlsx')
            ],
            [
                'title'=>'ساخت و ساز و استخراج',
                'en_title'=>'Construction and Extraction',
                'path' => public_path('assets/docs/job_category/Construction_and_Extraction.xlsx')
            ],
            [
                'title'=>'دستورالعمل آموزشی و کتابخانه',
                'en_title'=>'Educational Instruction and Library',
                'path' => public_path('assets/docs/job_category/Educational_Instruction_and_Library.xlsx')
            ],
            [
                'title'=>'کشاورزی، ماهیگیری و جنگلداری',
                'en_title'=>'Farming, Fishing, and Forestry',
                'path' => public_path('assets/docs/job_category/Farming_Fishing_and_Forestry.xlsx')
            ],
            [
                'title'=>'مربوط به تهیه و سرو غذا',
                'en_title'=>'Food Preparation and Serving Related',
                'path' => public_path('assets/docs/job_category/Food_Preparation_and_Serving_Related.xlsx')
            ],
            [
                'title'=>'پزشکان و فنی بهداشت و درمان',
                'en_title'=>'Healthcare Practitioners and Technical',
                'path' => public_path('assets/docs/job_category/Healthcare_Practitioners_and_Technical.xlsx')
            ],
            [
                'title'=>'پشتیبانی مراقبت های بهداشتی',
                'en_title'=>'Healthcare Support',
                'path' => public_path('assets/docs/job_category/Healthcare_Support.xlsx')
            ],
            [
                'title'=>'نصب، نگهداری و تعمیر',
                'en_title'=>'Installation, Maintenance, and Repair',
                'path' => public_path('assets/docs/job_category/Installation_Maintenance_and_Repair.xlsx')
            ],
            [
                'title'=>'حقوقی',
                'en_title'=>'Legal',
                'path' => public_path('assets/docs/job_category/Legal.xlsx')
            ],
            [
                'title'=>'علوم زیستی، فیزیکی و اجتماعی',
                'en_title'=>'Life, Physical, and Social Science',
                'path' => public_path('assets/docs/job_category/Life_Physical_and_Social_Science.xlsx')
            ],
            [
                'title'=>'مدیریت',
                'en_title'=>'Management',
                'path' => public_path('assets/docs/job_category/Management.xlsx')
            ],
            [
                'title'=>'ویژه نظامی',
                'en_title'=>'Military Specific',
                'path' => public_path('assets/docs/job_category/Military_Specific.xlsx')
            ],
            [
                'title'=>'خدمات اداری و اجرایی',
                'en_title'=>'Office and Administrative Support',
                'path' => public_path('assets/docs/job_category/Office_and_Administrative_Support.xlsx')
            ],
            [
                'title'=>'تولید',
                'en_title'=>'Production',
                'path' => public_path('assets/docs/job_category/Production.xlsx')
            ],
            [
                'title'=>'سرویس حفاظتی',
                'en_title'=>'Protective Service',
                'path' => public_path('assets/docs/job_category/Protective_Service.xlsx')
            ],
            [
                'title'=>'فروش و ارتباطات',
                'en_title'=>'Sales and Related',
                'path' => public_path('assets/docs/job_category/Sales_and_Related.xlsx')
            ],
            [
                'title'=>'حمل و نقل و جابجایی مواد',
                'en_title'=>'Transportation and Material Moving',
                'path' => public_path('assets/docs/job_category/Transportation_and_Material_Moving.xlsx')
            ],
            [
                'title'=>'مراقبت و خدمات شخصی',
                'en_title'=>'Personal Care and Service',
                'path' => public_path('assets/docs/job_category/Personal_Care_and_Service.xlsx')
            ]
        ];

        foreach($catInfos as $info){

            $categoryModel = Ocategory::updateOrCreate([
                'title' => $info['title'],
                'english_title' => $info['en_title'],
            ]);

            $categories2 = Excel::toCollection(new OcategoryImport , $info['path']);

            foreach($categories2 as $category){
                $categories = $category;
            }

            $codes = [];

            foreach($categories as $category){
                array_push($codes , $category['code']);
            };

            $jobs = Ojob::whereIn('code' , $codes)->get();

            foreach($categories as $cat){
                $job = $jobs->where('code' , $cat['code'])->first();

                $job->updateOrFail([
                    'category_id' => $categoryModel['id'],
                    'english_title' => $cat['occupation']
                ]);
            }
        }

        $this->command->info('job category seed to ocategories table');
    }
}
