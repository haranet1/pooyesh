<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'id' => 1,
                'key' => 'request_internship_limit',
                'value' => '3',
            ],
            [
                'id' => 2,
                'key' => 'create_internship_limit',
                'value' => '3',
            ],
            [
                'id' => 3,
                'key' => 'confirm_internship_limit',
                'value' => '3',
            ],
            [
                'id' => 4,
                'key' => 'career_matches_price',
                'value' => '200',
            ],
            [
                'id' => 5,
                'key' => 'major_matches_price',
                'value' => '200',
            ],
            [
                'id' => 6,
                'key' => 'create_hire_limit',
                'value' => '3',
            ],
            [
                'id' => 7,
                'key' => 'request_hire_limit',
                'value' => '3',
            ],
            [
                'id' => 8,
                'key' => 'confirm_hire_limit',
                'value' => '3',
            ],
        ];

        DB::table('settings')->insertOrIgnore($settings);
    }
}
