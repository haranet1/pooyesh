<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langs = [
            ['name' => 'انگلیسی' , 'en_name' => 'en'],
            ['name' => 'فرانسوی' , 'en_name' => 'fr'],
            ['name' => 'آلمانی' , 'en_name' => 'de'],
            ['name' => 'اسپانیایی' , 'en_name' => 'es'],
            ['name' => 'ترکی' , 'en_name' => 'tr'],
            ['name' => 'روسی' , 'en_name' => 'ru'],
            ['name' => 'ایتالیایی' , 'en_name' => 'it'],
            ['name' => 'چینی' , 'en_name' => 'zh'],
            ['name' => 'ژاپنی' , 'en_name' => 'ja'],
            ['name' => 'کره ای' , 'en_name' => 'ko'],
            ['name' => 'هندی' , 'en_name' => 'hi'],
            ['name' => 'ارمنی' , 'en_name' => 'hy'],
            ['name' => 'عربی' , 'en_name' => 'ar'],
            ['name' => 'گرجی' , 'en_name' => 'ka'],
            ['name' => 'کردی' , 'en_name' => 'ku'],
            ['name' => 'یونانی' , 'en_name' => 'el'],
        ];
        DB::table('langs')->insertOrIgnore($langs);
    }
}
