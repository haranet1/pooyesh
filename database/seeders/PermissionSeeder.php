<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['id' => '1' ,'name' => 'createUser' , 'guard_name' => 'web' , 'fa_name' => 'افزودن کاربر'],
            ['id' => '2' ,'name' => 'unconfirmedUser' , 'guard_name' => 'web' , 'fa_name' => 'کاربران تایید نشده'],
            ['id' => '3' ,'name' => 'confirmedUser' , 'guard_name' => 'web' , 'fa_name' => 'کاربران تایید شده'],
            ['id' => '4' ,'name' => 'importedUser' , 'guard_name' => 'web' , 'fa_name' => 'کاربران ایمپورت شده'],
            ['id' => '5' ,'name' => 'testedUser' , 'guard_name' => 'web' , 'fa_name' => 'کاربران تست داده'],
            ['id' => '6' ,'name' => 'exportAllUser' , 'guard_name' => 'web' , 'fa_name' => 'خروجی تمام کاربران'],
            ['id' => '7' ,'name' => 'unconfirmedJobs' , 'guard_name' => 'web' , 'fa_name' => 'موقعیت های شغلی تایید نشده'],
            ['id' => '8' ,'name' => 'pooyeshRequest' , 'guard_name' => 'web' , 'fa_name' => 'درخواست های پویش'],
            ['id' => '9' ,'name' => 'hireRequest' , 'guard_name' => 'web' , 'fa_name' => 'درخواست های استخدام'],
            ['id' => '10' ,'name' => 'manageRegulations' , 'guard_name' => 'web' , 'fa_name' => 'مدیریت آیین نامه ها'],
            ['id' => '11' ,'name' => 'manageNotifications' , 'guard_name' => 'web' , 'fa_name' => 'مدیریت اطلاعیه ها'],
            ['id' => '12' ,'name' => 'signature' , 'guard_name' => 'web' , 'fa_name' => 'افزودن امضاء'],
            ['id' => '13' ,'name' => 'manageEvents' , 'guard_name' => 'web' , 'fa_name' => 'مدیریت رویداد ها'],
            ['id' => '14' ,'name' => 'support' , 'guard_name' => 'web' , 'fa_name' => 'پشتیبانی'],
            ['id' => '15' ,'name' => 'readLog' , 'guard_name' => 'web' , 'fa_name' => 'مشاهده گزارشات'],
            ['id' => '16' ,'name' => 'managePermission' , 'guard_name' => 'web' , 'fa_name' => 'مدیریت دسترسی ها'],
            ['id' => '17' ,'name' => 'secretariat' , 'guard_name' => 'web' , 'fa_name' => 'دبیرخانه'],
            ['id' => '18' ,'name' => 'protection' , 'guard_name' => 'web' , 'fa_name' => 'حراست'],
            ['id' => '19' ,'name' => 'manageCompany' , 'guard_name' => 'web' , 'fa_name' => 'مدیریت شرکت ها'],
            ['id' => '20' ,'name' => 'manageStudent' , 'guard_name' => 'web' , 'fa_name' => 'مدیریت دانشجویان'],
            ['id' => '21' ,'name' => 'manageApplicant' , 'guard_name' => 'web' , 'fa_name' => 'مدیریت کارجویان'],
            ['id' => '22' ,'name' => 'manageArticles' , 'guard_name' => 'web' , 'fa_name' => 'مدیریت مجله'],
            ['id' => '23' ,'name' => 'amozeshyarExpert' , 'guard_name' => 'web' , 'fa_name' => 'کارشناس آموزشیار'],
            ['id' => '24' ,'name' => 'manageOnet' , 'guard_name' => 'web' , 'fa_name' => 'ویرایش استاندارد onet'],
            ['id' => '25' ,'name' => 'accounting' , 'guard_name' => 'web' , 'fa_name' => 'حسابداری'],


        ];

        DB::table('permissions')->insertOrIgnore($permissions);
    }
}
