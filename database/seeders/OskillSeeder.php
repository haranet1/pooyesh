<?php

namespace Database\Seeders;

use App\Imports\OskillImport;
use App\Models\Oskill;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OskillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path1 = public_path('assets/docs/1_skill.xlsx');

        $skillCollection1 = Excel::toCollection(new OskillImport , $path1);
 
        foreach($skillCollection1 as $collection){
            $skills1 = $collection;
        }

        $skill_codes = [];

        foreach($skills1 as $skill){

            if(! in_array($skill['skill_code'] , $skill_codes)){
                array_push($skill_codes , $skill['skill_code']);


                Oskill::updateOrCreate([
                    'code' => $skill['skill_code'],
                    'title' => $skill['title_fa'],
                    'english_title' => $skill['title']
                ]);
            }
        }

        $this->command->info('job skills seed to oskills table');
    }
}
