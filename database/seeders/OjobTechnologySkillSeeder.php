<?php

namespace Database\Seeders;

use App\Imports\OtechnologySkillImport;
use App\Models\Ojob;
use App\Models\OjobTechnologySkill;
use App\Models\OtechnologySkill;
use App\Models\OtechnologySkillExample;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OjobTechnologySkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('assets/docs/7-Technology Skills.xlsx');
        $tecnologySkill = Excel::toCollection(new OtechnologySkillImport , $path);

        foreach ($tecnologySkill as $techSkill_collection){
            $techSkills = $techSkill_collection;
        }

        $job_codes = [];
        $techskill_codes = [];

        foreach ($techSkills as $techSkill) {
            array_push($job_codes, $techSkill['code']);
            array_push($techskill_codes, $techSkill['techskill_code']);
        }

        $jobs = Ojob::whereIn('code', $job_codes)->get();
        $technologySkill = OtechnologySkill::whereIn('code', $techskill_codes)->get();

        $jobElement = null;
        $techElement = null;

        $examples = [];  // مقداردهی خارج از حلقه اصلی
        foreach ($techSkills as $techSkill){

            if (!in_array($techSkill['example'], $examples)){
                array_push($examples , $techSkill['example']);
                $exm = OtechnologySkillExample::create([
                    'example' => $techSkill['example_fa'],
                    'english_example' => $techSkill['example'],
                ]);
            }

            $jobElement = $jobs->where('code', $techSkill['code'])->first();
            $techElement = $technologySkill->where('code', $techSkill['techskill_code'])->first();

            if ($jobElement['code'] == $techSkill['code'] and $techElement['code'] == $techSkill['techskill_code']){
                OjobTechnologySkill::create([
                    'job_id' => $jobElement['id'],
                    'technology_skill_id' => $techElement['id'],
                    'hot_technology' => $techSkill['hot_technology'],
                    'in_demand' => $techSkill['in_demand'],
                    'example_id' => $exm->id,
                ]);
            }
        }


        $this->command->info('job technology skill seed to ojob_technology_skills table');
    }
}
