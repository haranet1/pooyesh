<?php

namespace Database\Seeders;

use App\Models\CompanyInfo;
use App\Models\OjobPosition;
use Illuminate\Database\Seeder;
use App\Models\CooperationRequest;
use App\Models\User;

class ResetCompanyLimitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = User::Company()->with('groupable')->get();

        foreach ($companies as $company) {

            $company = $company->groupable;

            $internPosCount = OjobPosition::where('company_id', $company->user->id)
            ->where('type', 'intern')->count();

            if($internPosCount > $company->create_internship_limit){
                $company->create_internship_limit = $internPosCount;
            }

            $confirmedInternRequest = CooperationRequest::Intern()
            ->where('receiver_id' , $company->user->id)
            ->Accepted()->count();

            if($confirmedInternRequest > $company->confirm_internship_limit){
                $company->confirm_internship_limit = $confirmedInternRequest;
            }

            $sent_requests_intern = CooperationRequest::where('sender_id', $company->user->id)
                ->where('type' , 'intern')->count();

            if($sent_requests_intern > $company->request_internship_limit){
                $company->request_internship_limit = $sent_requests_intern;
            }

            $hirePosCount = OjobPosition::where('company_id', $company->user->id)
            ->where('type','!=' ,'intern')->count();

            if($hirePosCount > $company->create_hire_limit){
                $company->create_hire_limit = $hirePosCount;
            }

            $confirmedHireRequest = CooperationRequest::Hire()
            ->where('receiver_id' , $company->user->id)
            ->Accepted()->count();

            if($confirmedHireRequest > $company->confirm_hire_limit){
                $company->confirm_hire_limit = $confirmedHireRequest;
            }

            $sent_requests_hire = CooperationRequest::where('sender_id', $company->user->id)
            ->where('type', '!=', 'intern')->count();
            
            if($sent_requests_hire > $company->request_hire_limit){
                $company->request_hire_limit = $sent_requests_hire;
            }

            $company->save();

            $packages = $company->companyPaidPackages;
            if(count($packages) > 0){
                foreach ($packages as $package) {
                    $package = $package->package;
                    $company->update([
                        'confirm_internship_limit' => $package->additional_internship_confirms + $company->confirm_internship_limit,
                        'request_internship_limit' => $package->additional_internship_requests + $company->request_internship_limit,
                        'create_internship_limit' => $package->additional_internship_positions + $company->create_internship_limit,
                        'confirm_hire_limit' => $package->additional_hire_confirms + $company->confirm_hire_limit,
                        'request_hire_limit' => $package->additional_hire_requests + $company->request_hire_limit,
                        'create_hire_limit' => $package->additional_hire_positions + $company->create_hire_limit,
                    ]);
                }
            }


        }
    }
}
