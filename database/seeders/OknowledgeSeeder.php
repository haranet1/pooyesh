<?php

namespace Database\Seeders;

use App\Imports\OknowledgeImport;
use App\Models\Oknowledge;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OknowledgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path1=public_path('assets/docs/1_knowledge.xlsx');

        $knowledge1 = Excel::toCollection(new OknowledgeImport , $path1);

        foreach ($knowledge1 as $know){
            $knowledges = $know;
        }

        $codes=[];

        foreach ($knowledges as $knowledge){
            if (! in_array( $knowledge['knowledge_code'] , $codes)){

                array_push($codes , $knowledge['knowledge_code']);

                Oknowledge::updateOrCreate([
                    'code'=>$knowledge['knowledge_code'],
                    'title'=>$knowledge['title_fa'],
                    'english_title'=>$knowledge['title'],
                ]);
            }
        }

        $this->command->info('knwoledge seed to oknowledges table');
    }
}
