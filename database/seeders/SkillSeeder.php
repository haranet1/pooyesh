<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skills = [
            ['name' => 'خلاق و باانگیزه','status' => '1'],
            ['name' => 'روحیه کار تیمی','status' => '1'],
            ['name' => 'تمایل به یادگیری','status' => '1'],
            ['name' => 'توانایی حل مسئله','status' => '1'],
            ['name' => 'تداوم و پافشاری','status' => '1'],
            ['name' => 'انعطاف پذیری','status' => '1'],
            ['name' => 'انضباط','status' => '1'],
        ];
        DB::table('skills')->insertOrIgnore($skills);
    }
}
