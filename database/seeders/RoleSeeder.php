<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => 'student','guard_name'=>'web'],
            ['name' => 'company','guard_name'=>'web'],
            ['name' => 'employee','guard_name'=>'web'],
            ['name' => 'applicant','guard_name'=>'web'],
        ];

        DB::table('roles')->insertOrIgnore($roles);
    }
}
