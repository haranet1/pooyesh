<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class MoveEmailFromInfosToUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereHas('groupable')
            ->whereRelation('groupable' , 'email' , '!=' , null)
            ->with('groupable')
            ->get();

        $emails = [];
        
        foreach($users as $user){
            if(in_array(strtolower($user->groupable->email) , $emails)){
                continue;
            }else{
                array_push($emails , strtolower($user->groupable->email));
                $user->update(['email' => strtolower($user->groupable->email)]);
                // $user->groupable->email = null;
                // $user->groupable->save();
            }
        }
        // dd($emails);    


        $this->command->info('The emails were deleted from the infos table and moved to the user table :)');
    }
}
