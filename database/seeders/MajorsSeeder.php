<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MajorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $majors = [
            [
                'category_id' => 1,
                'title' => 'مهندسی برق'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی کامپیوتر'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی مکانیک'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی عمران'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی معماری'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی کشتی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی هوافضا'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی انرژی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی شهرسازی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی فناوری اطلاعات'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی صنایع'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی شیمی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی پزشکی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی دریا'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی نساجی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی رباتیک'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی نفت'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی ایمنی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی معدن'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی پلیمر'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی مواد و متالوژی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی مدیریت پروژه'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی راه آهن'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی کشاورزی'
            ],
            [
                'category_id' => 1,
                'title' => 'علوم مهندسی'
            ],
            [
                'category_id' => 1,
                'title' => 'معماری داخلی'
            ],
            [
                'category_id' => 1,
                'title' => 'کاردان فنی مکانیک'
            ],
            [
                'category_id' => 1,
                'title' => 'کاردان فنی عمران'
            ],
            [
                'category_id' => 1,
                'title' => 'کاردانی مراقبت پرواز'
            ],
            [
                'category_id' => 1,
                'title' => 'کاردان فنی برق'
            ],
            [
                'category_id' => 1,
                'title' => 'کاردانی هواپیما'
            ],
            [
                'category_id' => 1,
                'title' => 'کاردان فنی معدن'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی مکاترونیک'
            ],
            [
                'category_id' => 1,
                'title' => 'علوم کامپیوتر'
            ],
            [
                'category_id' => 1,
                'title' => 'ریاضی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی شیمی'
            ],
            [
                'category_id' => 1,
                'title' => 'آمار'
            ],
            [
                'category_id' => 1,
                'title' => 'حسابداری'
            ],
            [
                'category_id' => 1,
                'title' => 'فیزیک'
            ],
            [
                'category_id' => 1,
                'title' => 'فیزیک مهندسی'
            ],
            [
                'category_id' => 1,
                'title' => 'ریاضیات و کاربرد ها'
            ],
            [
                'category_id' => 1,
                'title' => 'کارشناسی چند رسانه ای'
            ],
            [
                'category_id' => 1,
                'title' => 'هوانوردی – خلبانی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی بازرسی فنی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی ماشین های کشاورزی'
            ],
            [
                'category_id' => 1,
                'title' => 'هوانوردی – ناوبری هوایی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی مدیریت اجرایی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی مواد و طراحی صنایع غذایی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی مکانیک نیروگاه'
            ],
            [
                'category_id' => 1,
                'title' => 'علوم و فنون هوانوردی – خلبانی هلیکوپتری'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی فرماندهی و کنترل هوایی'
            ],
            [
                'category_id' => 1,
                'title' => 'مهندسی اپتیک و لیزر'
            ],


            [
                'category_id' => 2,
                'title' => 'پزشکی'
            ],
            [
                'category_id' => 2,
                'title' => 'دندانپزشکی'
            ],
            [
                'category_id' => 2,
                'title' => 'داروسازی'
            ],
            [
                'category_id' => 2,
                'title' => 'دکتری پیوسته بیوتکنولوژی'
            ],
            [
                'category_id' => 2,
                'title' => 'دامپزشکی'
            ],
            [
                'category_id' => 2,
                'title' => 'زیست شناسی'
            ],
            [
                'category_id' => 2,
                'title' => 'فیزیوتراپی'
            ],
            [
                'category_id' => 2,
                'title' => 'اعضای مصنوعی'
            ],
            [
                'category_id' => 2,
                'title' => 'بهداشت مواد غذایی'
            ],
            [
                'category_id' => 2,
                'title' => 'مدیریت و کمیسر دریایی'
            ],
            [
                'category_id' => 2,
                'title' => 'اتاق عمل'
            ],
            [
                'category_id' => 2,
                'title' => 'دبیری زیست شناسی'
            ],
            [
                'category_id' => 2,
                'title' => 'تکنولوژی مرتع و آبخیزداری'
            ],
            [
                'category_id' => 2,
                'title' => 'ارگونومی'
            ],
            [
                'category_id' => 2,
                'title' => 'هوشبری'
            ],
            [
                'category_id' => 2,
                'title' => 'پروتزهای دندانی'
            ],
            [
                'category_id' => 2,
                'title' => 'بینایی سنجی'
            ],
            [
                'category_id' => 2,
                'title' => 'شنوایی شناسی'
            ],
            [
                'category_id' => 2,
                'title' => 'علوم آزمایشگاهی'
            ],
            [
                'category_id' => 2,
                'title' => 'علوم آزمایشگاهی دامپزشکی'
            ],
            [
                'category_id' => 2,
                'title' => 'کتابداری پزشکی'
            ],
            [
                'category_id' => 2,
                'title' => 'مامایی'
            ],
            [
                'category_id' => 2,
                'title' => 'علوم تغذیه'
            ],
            [
                'category_id' => 2,
                'title' => 'علوم و صنایع غذایی'
            ],
            [
                'category_id' => 2,
                'title' => 'مهندسی بهداشت محیط'
            ],
            [
                'category_id' => 2,
                'title' => 'مهندسی فضای سبز'
            ],
            [
                'category_id' => 2,
                'title' => 'کارشناسی بهداشت عمومی'
            ],
            [
                'category_id' => 2,
                'title' => 'مهندسی منابع طبیعی'
            ],
            [
                'category_id' => 2,
                'title' => 'کاردانی دامپزشکی'
            ],
            [
                'category_id' => 2,
                'title' => 'پرستاری'
            ],
            [
                'category_id' => 2,
                'title' => 'مدارک پزشکی'
            ],
            [
                'category_id' => 2,
                'title' => 'زیست شناسی سلولی مولکولی'
            ],
            [
                'category_id' => 2,
                'title' => 'مهندسی بهداشت حرفه ای'
            ],
            [
                'category_id' => 2,
                'title' => 'مهندسی تولیدات گیاهی'
            ],
            [
                'category_id' => 2,
                'title' => 'کارشناسی فناوری اطلاعات سلامت'
            ],
            [
                'category_id' => 2,
                'title' => 'علوم مهندسی زیست محیطی'
            ],
            [
                'category_id' => 2,
                'title' => 'دبیری شیمی'
            ],
            [
                'category_id' => 2,
                'title' => 'شیمی'
            ],
            [
                'category_id' => 2,
                'title' => 'گفتار درمانی'
            ],
            [
                'category_id' => 2,
                'title' => 'تکنولوژی تولیدات دامی'
            ],
            [
                'category_id' => 2,
                'title' => 'کاردانی فوریت های پزشکی'
            ],
            [
                'category_id' => 2,
                'title' => 'مهندسی منابع طبیعی'
            ],
            [
                'category_id' => 2,
                'title' => 'مهندسی کشاورزی'
            ],
            [
                'category_id' => 2,
                'title' => 'اقیانوس شناسی'
            ],
            [
                'category_id' => 2,
                'title' => 'زمین شناسی'
            ],
            [
                'category_id' => 2,
                'title' => 'کاردرمانی'
            ],
            [
                'category_id' => 2,
                'title' => 'کارشناسی تکنولوژی پرتوشناسی'
            ],
            [
                'category_id' => 2,
                'title' => 'ایمنی شناسی'
            ],
            [
                'category_id' => 2,
                'title' => 'تکنولوژی پزشکی هسته ای'
            ],
            [
                'category_id' => 2,
                'title' => 'کاردانی شیمی'
            ],
            [
                'category_id' => 2,
                'title' => 'کاردانی ناپیوسته علمی – کاربردی پرورش زنبور عسل'
            ],
            [
                'category_id' => 2,
                'title' => 'کاردانی علمی کاربردی تولید و بهره برداری گیاهان دارویی و معطر'
            ],
            [
                'category_id' => 3,
                'title' => 'حقوق'
            ],
            [
                'category_id' => 3,
                'title' => 'کارشناسی ارشد الهیات'
            ],
            [
                'category_id' => 3,
                'title' => 'مطالعات ارتباطی'
            ],
            [
                'category_id' => 3,
                'title' => 'علوم اقتصادی'
            ],
            [
                'category_id' => 3,
                'title' => 'مدیریت'
            ],
            [
                'category_id' => 3,
                'title' => 'علوم سیاسی'
            ],
            [
                'category_id' => 3,
                'title' => 'علوم قضایی'
            ],
            [
                'category_id' => 3,
                'title' => 'تربیت بدنی'
            ],
            [
                'category_id' => 3,
                'title' => 'علوم اجتماعی'
            ],
            [
                'category_id' => 3,
                'title' => 'تفسیر قران مجید'
            ],
            [
                'category_id' => 3,
                'title' => 'روانشناسی'
            ],
            [
                'category_id' => 3,
                'title' => 'زبان و ادبیات فارسی'
            ],
            [
                'category_id' => 3,
                'title' => 'زبان و ادبیات عربی'
            ],
            [
                'category_id' => 3,
                'title' => 'روابط عمومی'
            ],
            [
                'category_id' => 3,
                'title' => 'مطالعات خانواده'
            ],
            [
                'category_id' => 3,
                'title' => 'کتابداری'
            ],
            [
                'category_id' => 3,
                'title' => 'روزنامه نگاری'
            ],
            [
                'category_id' => 3,
                'title' => 'مددکاری اجتماعی'
            ],
            [
                'category_id' => 3,
                'title' => 'راهنمایی و مشاوره'
            ],
            [
                'category_id' => 3,
                'title' => 'ادبیات داستانی'
            ],
            [
                'category_id' => 3,
                'title' => 'آب و هواشناسی'
            ],
            [
                'category_id' => 3,
                'title' => 'ژئومورفولوژی'
            ],
            [
                'category_id' => 3,
                'title' => 'فقه و حقوق'
            ],
            [
                'category_id' => 3,
                'title' => 'جغرافیا'
            ],
            [
                'category_id' => 3,
                'title' => 'مدیریت هتلداری'
            ],
            [
                'category_id' => 3,
                'title' => 'علوم تربیتی'
            ],
            [
                'category_id' => 3,
                'title' => 'مدیریت جهانگردی'
            ],
            [
                'category_id' => 3,
                'title' => 'دبیری جغرافیا'
            ],
            [
                'category_id' => 3,
                'title' => 'امور گمرکی'
            ],
            [
                'category_id' => 3,
                'title' => 'تاریخ'
            ],
            [
                'category_id' => 3,
                'title' => 'علوم حدیث'
            ],
            [
                'category_id' => 3,
                'title' => 'فلسفه'
            ],
            [
                'category_id' => 3,
                'title' => 'رشد و پرورش کودکان پیش دبستانی'
            ],
            [
                'category_id' => 3,
                'title' => 'دبیری زبان و ادبیات عربی'
            ],
            [
                'category_id' => 3,
                'title' => 'دبیری زبان و ادبیات فارسی'
            ],
            [
                'category_id' => 3,
                'title' => 'علوم قرانی و حدیث'
            ],
            [
                'category_id' => 3,
                'title' => 'علم اطلاعات و دانش شناسی'
            ],
            [
                'category_id' => 3,
                'title' => 'مدیریت'
            ],
            [
                'category_id' => 3,
                'title' => 'علوم ارتباطات اجتماعی'
            ],
            [
                'category_id' => 3,
                'title' => 'الهیات و معارف اسلامی'
            ],

            [
                'category_id' => 4,
                'title' => 'سینما'
            ],

            [
                'category_id' => 4,
                'title' => 'عکاسی'
            ],

            [
                'category_id' => 4,
                'title' => 'چاپ'
            ],

            [
                'category_id' => 4,
                'title' => 'کارشناسی فرش'
            ],

            [
                'category_id' => 4,
                'title' => 'گرافیک'
            ],

            [
                'category_id' => 4,
                'title' => 'صنایع دستی'
            ],

            [
                'category_id' => 4,
                'title' => 'موزه داری'
            ],
            [
                'category_id' => 4,
                'title' => 'باستان شناسی'
            ],
            [
                'category_id' => 4,
                'title' => 'مرمت آثار تاریخی'
            ],
            [
                'category_id' => 4,
                'title' => 'تلویزیون و هنرهای دیجیتالی'
            ],
            [
                'category_id' => 4,
                'title' => 'زبان انگلیسی'
            ],
            [
                'category_id' => 4,
                'title' => 'زبان فرانسه'
            ],
            [
                'category_id' => 4,
                'title' => 'زبان روسی'
            ],
            [
                'category_id' => 4,
                'title' => 'زبان چینی'
            ],
            [
                'category_id' => 4,
                'title' => 'زبان آلمانی'
            ],
            [
                'category_id' => 4,
                'title' => 'زبان ژاپنی'
            ],
            [
                'category_id' => 4,
                'title' => 'زبان ارمنی'
            ],
            [
                'category_id' => 4,
                'title' => 'زبان و ادبیات اردو'
            ],
            [
                'category_id' => 4,
                'title' => 'زبان ایتالیایی'
            ],
            [
                'category_id' => 3,
                'title' => 'آموزش ابتدایی'
            ],
            [
                'category_id' => 3,
                'title' => 'علوم ورزشی'
            ],
            [
                'category_id' => 4,
                'title' => 'هنرهای تجسمی'
            ],
            [
                'category_id' => 4,
                'title' => 'نقاشی'
            ],
            [
                'category_id' => 4,
                'title' => 'مجسمه سازی'
            ],



        ];

        DB::table('majors')->insertOrIgnore($majors);
    }
}
