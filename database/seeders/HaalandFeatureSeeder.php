<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HaalandFeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fearures = [
            ['personality_id' => '1','title' => 'خوددار'], //1
            ['personality_id' => '1','title' => 'مُصِر'], //2
            ['personality_id' => '1','title' => 'جدی در کار'], //3
            ['personality_id' => '1','title' => 'مادی گرا'], //4
            ['personality_id' => '1','title' => 'سرسخت'], //5
            ['personality_id' => '1','title' => 'متواضع'], //6
            ['personality_id' => '1','title' => 'انعطاف پذیر'], //7
            ['personality_id' => '1','title' => 'دارای توانایی بدنی و مکانیکی'], //8
            ['personality_id' => '1','title' => 'تمایل کار با با حیوانات، گیاهان، چوب، ابزار آلات و ماشین آلات'], //9
            ['personality_id' => '2','title' => 'کنجکاو'], //10
            ['personality_id' => '2','title' => 'تحلیل گر'], //11
            ['personality_id' => '2','title' => 'محتاط'], //12
            ['personality_id' => '2','title' => 'مستقل'], //13
            ['personality_id' => '2','title' => 'روشن فکر'], //14
            ['personality_id' => '2','title' => 'درون نگر'], //15
            ['personality_id' => '2','title' => 'پیچیده'], //16
            ['personality_id' => '2','title' => 'دقیق'], //17
            ['personality_id' => '2','title' => 'منطقی'], //18
            ['personality_id' => '2','title' => 'فروتن'], //19
            ['personality_id' => '2','title' => 'کناره گیر '], //20
            ['personality_id' => '2','title' => 'اهل تفکر و سازمان دهی و استدلال'], //21
            ['personality_id' => '2','title' => 'طرفدار اصالحات اساسی و بنیادین'], //22
            ['personality_id' => '3','title' => 'مبتکر'], //23
            ['personality_id' => '3','title' => 'نو آور'], //24
            ['personality_id' => '3','title' => 'پذیرنده'], //25
            ['personality_id' => '3','title' => 'مستقل'], //26
            ['personality_id' => '3','title' => 'پر هرج و مرج'], //27
            ['personality_id' => '3','title' => 'احساسی'], //28
            ['personality_id' => '3','title' => 'نامرتب'], //29
            ['personality_id' => '3','title' => 'آرمان گرا'], //30
            ['personality_id' => '3','title' => 'خیال پرداز'], //31
            ['personality_id' => '3','title' => 'درون گرا'], //32
            ['personality_id' => '3','title' => 'ابرازگر'], //33
            ['personality_id' => '3','title' => 'حساس'], //34
            ['personality_id' => '3','title' => 'مبهم'], //35
            ['personality_id' => '3','title' => 'غیر سنتی'], //36
            ['personality_id' => '3','title' => 'دارای منش و روحیه بازی'], //37
            ['personality_id' => '4','title' => 'مسئول'], //38
            ['personality_id' => '4','title' => 'صمیمی'], //39
            ['personality_id' => '4','title' => 'آرمان گرا'], //40
            ['personality_id' => '4','title' => 'سخاوتمند'], //41
            ['personality_id' => '4','title' => 'مهربان'], //42
            ['personality_id' => '4','title' => 'همدل'], //43
            ['personality_id' => '4','title' => 'خوش مشرب'], //44
            ['personality_id' => '4','title' => 'معاشرتی'], //45
            ['personality_id' => '4','title' => 'صبور'], //46
            ['personality_id' => '4','title' => 'دارای روحیه همکاری'], //47
            ['personality_id' => '4','title' => 'متقاعد کننده'], //48
            ['personality_id' => '4','title' => 'مشوق'], //49
            ['personality_id' => '4','title' => 'فهمیده'], //50
            ['personality_id' => '4','title' => 'اهل رفاقت و دوستی'], //51
            ['personality_id' => '5','title' => 'ماجراجو'], //52
            ['personality_id' => '5','title' => 'بلند پرواز'], //53
            ['personality_id' => '5','title' => 'مطمئن به خود'], //54
            ['personality_id' => '5','title' => 'برون نگر'], //55
            ['personality_id' => '5','title' => 'هیجان طلب'], //56
            ['personality_id' => '5','title' => 'اجتماعی'], //57
            ['personality_id' => '5','title' => 'خوش بین'], //58
            ['personality_id' => '5','title' => 'پر انرژی '], //59
            ['personality_id' => '5','title' => 'رهبر'], //60
            ['personality_id' => '5','title' => 'مایل به جمع آوری دارایی'], //61
            ['personality_id' => '5','title' => 'آگاه به منابع مختلف'], //62
            ['personality_id' => '5','title' => 'پر ذوق و شوق'], //63
            ['personality_id' => '5','title' => 'ریسک کننده'], //64
            ['personality_id' => '5','title' => 'مقتدر'], //65
            ['personality_id' => '5','title' => 'توانایی بالا در متقاعد سازی دیگران'], //66
            ['personality_id' => '5','title' => 'دوست دارند با مردم کار کنند و آنها را برای رسیدن به اهدافشان کمک کنند'], //67
            ['personality_id' => '6','title' => 'سازمان یافته'], //68
            ['personality_id' => '6','title' => 'مرتب'], //69
            ['personality_id' => '6','title' => 'واقع بین'], //70
            ['personality_id' => '6','title' => 'پیگیر'], //71
            ['personality_id' => '6','title' => 'انعطاف ناپذیر '], //72
            ['personality_id' => '6','title' => 'کارآمد'], //73
            ['personality_id' => '6','title' => 'وظیفه شناس'], //74
            ['personality_id' => '6','title' => 'مطیع'], //75
            ['personality_id' => '6','title' => 'فاقد تخیل'], //76
            ['personality_id' => '6','title' => 'مدافع'], //77
            ['personality_id' => '6','title' => 'علاقه به کار با داده ها'], //78
            ['personality_id' => '6','title' => 'عدم تمایل به فعالیت های جسمانی'], //79
        ];

        DB::table('haaland_features')->insertOrIgnore($fearures);
    }
}
