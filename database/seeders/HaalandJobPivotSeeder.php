<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HaalandJobPivotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = [
            ['personality_id' => '1', 'job_id' => '1'], //1 
            ['personality_id' => '1', 'job_id' => '2'], //2 
            ['personality_id' => '1', 'job_id' => '3'], //3 
            ['personality_id' => '1', 'job_id' => '4'], //4 
            ['personality_id' => '1', 'job_id' => '5'], //5 
            ['personality_id' => '2', 'job_id' => '6'], //6 
            ['personality_id' => '2', 'job_id' => '7'], //7 
            ['personality_id' => '2', 'job_id' => '8'], //8 
            ['personality_id' => '2', 'job_id' => '9'], //9 
            ['personality_id' => '2', 'job_id' => '10'], //10 
            ['personality_id' => '3', 'job_id' => '11'], //11
            ['personality_id' => '3', 'job_id' => '12'], //12
            ['personality_id' => '3', 'job_id' => '13'], //13 
            ['personality_id' => '3', 'job_id' => '14'], //14
            ['personality_id' => '3', 'job_id' => '15'], //15
            ['personality_id' => '3', 'job_id' => '16'], //16
            ['personality_id' => '3', 'job_id' => '17'], //17
            ['personality_id' => '3', 'job_id' => '18'], //18
            ['personality_id' => '3', 'job_id' => '19'], //19
            ['personality_id' => '3', 'job_id' => '20'], //20
            ['personality_id' => '4', 'job_id' => '21'], //21
            ['personality_id' => '4', 'job_id' => '22'], //22
            ['personality_id' => '4', 'job_id' => '23'], //23
            ['personality_id' => '4', 'job_id' => '24'], //24
            ['personality_id' => '4', 'job_id' => '25'], //25
            ['personality_id' => '4', 'job_id' => '26'], //26
            ['personality_id' => '4', 'job_id' => '27'], //27
            ['personality_id' => '4', 'job_id' => '28'], //28
            ['personality_id' => '4', 'job_id' => '29'], //29
            ['personality_id' => '5', 'job_id' => '30'], //30
            ['personality_id' => '5', 'job_id' => '31'], //31
            ['personality_id' => '5', 'job_id' => '32'], //32
            ['personality_id' => '5', 'job_id' => '33'], //33
            ['personality_id' => '6', 'job_id' => '34'], //34
            ['personality_id' => '6', 'job_id' => '35'], //35
            ['personality_id' => '6', 'job_id' => '36'], //36
            ['personality_id' => '6', 'job_id' => '37'], //37
        ];

        DB::table('haaland_job_pivots')->insertOrIgnore($ids);
    }
}
