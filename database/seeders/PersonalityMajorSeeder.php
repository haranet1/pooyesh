<?php

namespace Database\Seeders;

use App\Models\MbtiPersonality;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonalityMajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personality_majors = [
            // ENFJ 
            [
                'major_id' => 115,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 164,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 64,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 89,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 132,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 138,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 139,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 59,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 63,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 82,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 83,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 90,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 84,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 79,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 118,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 113,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 143,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 123,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],

            // ENFP 

            [
                'major_id' => 121,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 42,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 107,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 143,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 113,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 143,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 164,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 137,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 64,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 89,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 139,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 132,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            // ESFJ 
            [
                'major_id' => 90,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 82,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 74,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 67,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 63,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 115,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 118,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],

            // ESFP 
            [
                'major_id' => 118,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 64,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 89,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 132,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 138,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 139,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 113,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 122,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 164,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 143,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 59,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 63,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 82,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 83,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 90,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 84,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 79,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],

            // INFJ 
            [
                'major_id' => 115,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 115,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 82,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 74,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 67,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 63,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 113,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 122,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 143,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 118,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 142,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 64,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 89,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 132,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 138,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 139,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            // INFP 
            [
                'major_id' => 155,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 146,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 149,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 115,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 121,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 64,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 89,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 132,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 138,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 26,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 166,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 167,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 168,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            // INTJ 
            [
                'major_id' => 108,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 2,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 16,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 34,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 35,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 41,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 142,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 127,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 105,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 111,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            // INTP 
            [
                'major_id' => 108,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 5,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 8,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 2,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 10,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 34,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 136,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            // ISFJ 
            [
                'major_id' => 115,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 74,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 67,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 63,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 113,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 122,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 143,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 59,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 63,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 82,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 83,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 90,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 84,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 79,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],

            // ISFP 
            [
                'major_id' => 146,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 149,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 118,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 118,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 80,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 82,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 74,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 67,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 63,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 165,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 166,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 167,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 168,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],

            // ISTJ 
            [
                'major_id' => 118,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 109,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 38,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 105,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 127,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 25,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 54,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],

            // ISTP 
            [
                'major_id' => 34,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 3,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 108,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 58,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 84,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 88,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 35,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 41,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],

            // ESTJ 
            [
                'major_id' => 127,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 105,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 118,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 110,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 111,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 107,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 38,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 142,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            // ENTJ 
            [
                'major_id' => 108,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 118,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 107,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 142,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 105,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 127,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 111,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 110,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 127,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            // ENTP 
            [
                'major_id' => 109,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 108,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 34,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 121,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 164,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 64,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 34,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 89,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 132,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 138,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 139,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 105,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 111,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],

            // ESTP 
            [
                'major_id' => 58,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 84,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 38,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 108,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 121,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'major_id' => 142,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],

        ];

        DB::table('personality_majors')->insertOrIgnore($personality_majors);
    }
}
