<?php

namespace Database\Seeders;

use App\Models\MbtiPersonality;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonalityJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personality_jobs = [

            // ENFJ 
            [
                'ojob_id' => 273,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 275,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 279,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 280,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 281,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 282,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 283,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 284,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 285,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 286,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 287,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 289,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 290,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 424,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 425,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 428,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 432,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 433,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 434,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 436,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 437,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 438,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 439,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 440,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 441,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 442,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 443,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 455,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 489,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 486,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 493,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 503,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 504,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 335,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 354,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 358,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 359,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 361,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 380,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 379,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 386,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 399,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 371,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 369,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 367,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 382,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 397,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 390,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 137,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 179,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 2,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 4,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 625,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 626,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 627,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 636,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 7,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 27,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 80,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 238,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 82,
                'test_id' => 1,
                'test_type' => MbtiPersonality::class
            ],


            // ENFP 

            [
                'ojob_id' => 395,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 275,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 279,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 280,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 281,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 282,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 283,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 284,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 285,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 286,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 287,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 289,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 290,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 82,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 399,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 400,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 111,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 436,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 437,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 438,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 439,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 441,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 442,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 443,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 493,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 397,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 396,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 46,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 95,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 635,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 749,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 394,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 364,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 300,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 301,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 302,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 303,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 304,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 305,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 306,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 307,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 308,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 309,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 310,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 311,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 312,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 313,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 314,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 315,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 316,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 317,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 318,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 319,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 320,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 321,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 322,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 323,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 324,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 325,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 326,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 327,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 328,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 329,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 330,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 331,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 332,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 333,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 334,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 390,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 391,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 291,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 274,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 103,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 424,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 432,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 433,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 434,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 241,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 242,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 243,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 244,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 245,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 246,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 367,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 3,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 53,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 369,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 370,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 371,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 604,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 380,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 7,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 430,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 418,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 74,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 638,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 253,
                'test_id' => 2,
                'test_type' => MbtiPersonality::class
            ],

            // ESFJ
            [
                'ojob_id' => 93,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 358,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 359,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 612,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 337,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            
            [
                'ojob_id' => 288,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 74,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 103,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 449,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 111,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 436,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 437,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 438,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 439,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 441,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 442,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 443,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 493,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 352,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 353,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 275,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 279,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 280,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 281,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 282,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 283,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 284,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 285,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 286,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 287,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 289,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 290,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 691,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 641,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 34,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 430,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 3,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 667,
                'test_id' => 3,
                'test_type' => MbtiPersonality::class
            ],

            // ESFP 
            [
                'ojob_id' => 385,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 369,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 370,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 371,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 604,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 380,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 386,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 373,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 275,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 279,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 280,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 281,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 282,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 283,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 284,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 285,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 286,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 287,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 289,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 290,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 615,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 376,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 638,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            
            [
                'ojob_id' => 390,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 391,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 408,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 396,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 667,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 394,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 395,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 453,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 391,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 111,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 436,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 437,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 438,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 439,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 441,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 442,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 443,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 493,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 53,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 625,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 352,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 353,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 624,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 587,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 533,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 533,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 82,
                'test_id' => 4,
                'test_type' => MbtiPersonality::class
            ],

            // INFJ 

            [
                'ojob_id' => 103,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 424,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 432,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 433,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 434,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 399,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 400,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 455,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 241,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 242,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 243,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 244,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 245,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 246,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 275,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 279,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 280,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 281,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 282,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 283,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 284,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 285,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 286,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 287,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 289,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 290,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 612,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 279,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 352,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 353,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 390,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 391,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 408,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 364,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 300,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 301,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 302,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 303,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 304,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 305,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 306,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 307,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 308,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 309,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 310,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 311,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 312,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 313,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 314,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 315,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 316,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 317,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 318,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 319,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 320,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 321,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 322,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 323,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 324,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 325,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 326,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 327,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 328,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 329,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 330,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 331,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 332,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 333,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 334,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 358,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 359,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 370,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 371,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 604,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 380,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 457,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 379,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 74,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 503,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 614,
                'test_id' => 5,
                'test_type' => MbtiPersonality::class
            ],


            // INFP 

            [
                'ojob_id' => 288,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 275,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 279,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 280,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 281,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 282,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 283,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 284,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 285,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 286,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 287,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 289,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 290,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 426,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 373,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 103,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 352,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 353,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 390,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 391,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 424,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 432,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 433,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 434,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 380,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 369,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 370,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 371,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 604,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 395,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 364,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 300,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 301,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 302,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 303,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 304,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 305,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 306,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 307,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 308,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 309,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 310,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 311,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 312,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 313,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 314,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 315,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 316,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 317,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 318,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 319,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 320,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 321,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 322,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 323,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 324,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 325,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 326,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 327,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 328,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 329,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 330,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 331,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 332,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 333,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 334,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 4,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 409,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 410,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 403,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 411,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 358,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 359,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 397,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 375,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 399,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 400,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 241,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 242,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 243,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 244,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 245,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 246,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 401,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 253,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 408,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 74,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 410,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 443,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 430,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 85,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 240,
                'test_id' => 6,
                'test_type' => MbtiPersonality::class
            ],

            // INTJ 
            [
                'ojob_id' => 110,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 122,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 928,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 136,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 115,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 179,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 87,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 240,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 143,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 214,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 227,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 230,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 234,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 236,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 457,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 291,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 364,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 300,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 301,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 302,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 303,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 304,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 305,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 306,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 307,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 308,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 309,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 310,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 311,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 312,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 313,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 314,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 315,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 316,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 317,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 318,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 319,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 320,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 321,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 322,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 323,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 324,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 325,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 326,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 327,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 328,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 329,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 330,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 331,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 332,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 333,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 334,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 241,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 242,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 243,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 244,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 245,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 246,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 144,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 88,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 89,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1005,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1002,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 53,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 295,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 352,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 353,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 417,
                'test_id' => 7,
                'test_type' => MbtiPersonality::class
            ],

            // INTP 

            [
                'ojob_id' => 364,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 300,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 301,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 302,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 303,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 304,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 305,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 306,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 307,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 308,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 309,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 310,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 311,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 312,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 313,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 314,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 315,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 316,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 317,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 318,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 319,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 320,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 321,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 322,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 323,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 324,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 325,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 326,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 327,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 328,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 329,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 330,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 331,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 332,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 333,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 334,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 143,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],


            [
                'ojob_id' => 214,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 227,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 230,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 234,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 236,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 249,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 148,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 149,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 139,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 369,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 370,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 371,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 604,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 398,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 89,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 110,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 122,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 928,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 136,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 115,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 238,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 179,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 241,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 242,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 243,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 244,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 245,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 246,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 455,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 240,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 352,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 353,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 638,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 401,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 390,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 391,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 114,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 116,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 295,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 291,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 294,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 3,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 80,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 53,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 98,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 651,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 103,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 408,
                'test_id' => 8,
                'test_type' => MbtiPersonality::class
            ],

            // ISFJ 
            [
                'ojob_id' => 53,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 612,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 337,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 543,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 539,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 641,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 103,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 457,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 275,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 279,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 280,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 281,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 282,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 283,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 284,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 285,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 286,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 287,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 289,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 290,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 380,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 74,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 498,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 691,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 641,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 624,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 240,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 143,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 214,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 227,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 230,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 234,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 236,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 612,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 337,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 358,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 359,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 111,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 436,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 437,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 438,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 439,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 441,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 442,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 443,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 493,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 93,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 144,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 41,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 399,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 400,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1005,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 685,
                'test_id' => 9,
                'test_type' => MbtiPersonality::class
            ],

            // ISFP 
            [
                'ojob_id' => 369,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 370,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 371,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 604,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 715,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 352,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 353,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 556,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 612,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 453,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 390,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 103,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 275,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 279,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 280,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 281,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 282,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 283,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 284,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 285,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 286,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 287,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 288,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 289,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 290,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 379,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 426,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 435,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 337,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 220,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 221,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 358,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 359,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 397,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 390,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 391,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 111,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 436,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 437,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 438,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 439,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 441,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 442,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 443,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 493,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 173,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 399,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 400,
                'test_id' => 10,
                'test_type' => MbtiPersonality::class
            ],


            // ISTJ 
            [
                'ojob_id' => 93,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1005,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 543,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 539,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 53,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 455,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 417,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 107,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 538,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 547,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 143,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 214,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 227,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 230,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 234,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 236,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 301,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 291,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 295,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 3,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 122,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 928,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 136,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 115,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 358,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 359,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 741,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 728,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 173,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 110,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 192,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 193,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 195,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 196,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 200,
                'test_id' => 11,
                'test_type' => MbtiPersonality::class
            ],

            // ISTP 
            [
                'ojob_id' => 385,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 715,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 722,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 173,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1005,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1002,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 796,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 486,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 29,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 533,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 70,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 267,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 947,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 948,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 543,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 539,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 284,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 538,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 547,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 959,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 957,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 954,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 955,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 161,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 179,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 741,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 158,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 254,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 976,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 638,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 122,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 928,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 136,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 115,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 3,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 80,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 53,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 625,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 626,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 143,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 214,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 227,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 230,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 234,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 236,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 110,
                'test_id' => 12,
                'test_type' => MbtiPersonality::class
            ],

            // ESTJ 

            [
                'ojob_id' => 93,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1005,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1002,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 543,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 539,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 651,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 98,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 107,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 658,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 69,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 179,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 60,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 23,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 144,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            
            [
                'ojob_id' => 136,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 115,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 538,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 547,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 143,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],


            [
                'ojob_id' => 214,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 227,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 230,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 234,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 236,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 295,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 65,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 100,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 626,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 238,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 352,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 353,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 397,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 493,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 653,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 364,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 300,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 301,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 302,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 303,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 304,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 305,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 306,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 307,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 308,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 309,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 310,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 311,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 312,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 313,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 314,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 315,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 316,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 317,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 318,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 319,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 320,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 321,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 322,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 323,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 324,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 325,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 326,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 327,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 328,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 329,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 330,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 331,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 332,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 333,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 334,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 53,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 358,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 359,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 240,
                'test_id' => 13,
                'test_type' => MbtiPersonality::class
            ],

            // ENTJ  
            [
                'ojob_id' => 88,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 89,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 3,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 80,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 53,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 252,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 13,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 121,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 291,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 295,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 143,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],


            [
                'ojob_id' => 214,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 227,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 230,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 234,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 236,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 651,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 110,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 352,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 353,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 364,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 300,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 301,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 302,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 303,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 304,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 305,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 306,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 307,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 308,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 309,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 310,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 311,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 312,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 313,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 314,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 315,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 316,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 317,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 318,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 319,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 320,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 321,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 322,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 323,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 324,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 325,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 326,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 327,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 328,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 329,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 330,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 331,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 332,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            [
                'ojob_id' => 333,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 334,
                'test_id' => 14,
                'test_type' => MbtiPersonality::class
            ],

            // ENTP 
            [
                'ojob_id' => 179,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 122,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 928,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 110,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 395,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 291,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 638,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 252,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 4,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 455,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 241,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 242,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 243,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 244,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 245,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 246,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 274,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 103,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 273,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 276,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 277,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 278,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 113,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 143,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],


            [
                'ojob_id' => 214,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 227,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 230,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 234,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 236,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 399,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 400,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 408,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 625,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 626,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 637,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 380,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 390,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 391,
                'test_id' => 15,
                'test_type' => MbtiPersonality::class
            ],

            // ESTP 
            [
                'ojob_id' => 715,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 167,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 170,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 372,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 486,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 543,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 539,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 538,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 547,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 959,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 958,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 957,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 955,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 954,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 952,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 533,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1004,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1005,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1015,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 1002,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 29,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 115,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 638,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 3,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 80,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 53,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 128,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 63,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 625,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 626,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 629,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],
            [
                'ojob_id' => 630,
                'test_id' => 16,
                'test_type' => MbtiPersonality::class
            ],

        ];


        DB::table('personality_jobs')->insertOrIgnore($personality_jobs);

    }
}
