<?php

namespace Database\Seeders;

use App\Imports\OskillImport;
use App\Models\Oimportance;
use App\Models\Ojob;
use App\Models\OjobSkill;
use App\Models\Olevel;
use App\Models\Oskill;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OjobSkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path1 = public_path('assets/docs/1_skill.xlsx');
        $path2 = public_path('assets/docs/2_skill.xlsx');
        $path3 = public_path('assets/docs/3_skill.xlsx');

        $skillCollection1 = Excel::toCollection(new OskillImport , $path1);
        $skillCollection2 = Excel::toCollection(new OskillImport , $path2);
        $skillCollection3 = Excel::toCollection(new OskillImport , $path3);

        foreach($skillCollection1 as $collection){
            $skills1 = $collection;
        }
        foreach($skillCollection2 as $collection){
            $skills2 = $collection;
        }
        foreach($skillCollection3 as $collection){
            $skills3 = $collection;
        }

        $skills = $skills1->merge($skills2);
        $skills = $skills->merge($skills3);

        $job_codes = [];
        $skill_code = [];

        foreach($skills as $skil){
            array_push($job_codes , $skil['code']);
            array_push($skill_code , $skil['skill_code']);
        }

        $jobsModel = Ojob::whereIn('code' , $job_codes)->get();
        $skillsModel = Oskill::whereIn('code' , $skill_code)->get();

        $jobElement = null;
        $skillElement = null;
        $importanceElemnt = null;
        $levelElement = null;
        $jobSkill = null;

        foreach($skills as $skill){

            
            $jobElement = $jobsModel->where('code' , $skill['code'])->first();
            $skillElement = $skillsModel->where('code' , $skill['skill_code'])->first();


            if( $jobElement['code'] == $skill['code'] and $skillElement['code'] == $skill['skill_code'] and $skill['scale_name'] == 'Importance'){

                $jobSkill = OjobSkill::create([
                    'job_id' => $jobElement['id'],
                    'skill_id' => $skillElement['id'],
                ]);
            }

            if($skill['scale_name'] == 'Importance'){

                $importanceElemnt = Oimportance::create([
                    'data_value' => $skill['data_value'] ?? 0,
                    'lower' => $skill['lower_bound'] ?? 0,
                    'upper' => $skill['upper_bound'] ?? 0,
                ]);

                $jobSkill->update([
                    'importance_id' => $importanceElemnt['id']
                ]);

            }else{

                $levelElement = Olevel::create([
                    'data_value' => $skill['data_value'] ?? 0,
                    'lower' => $skill['lower_bound'] ?? 0,
                    'upper' => $skill['upper_bound'] ?? 0,
                ]);

                $jobSkill->update([
                    'level_id' => $levelElement['id']
                ]);

            }

            $jobElement = null;
            $skillElement = null;
            $importanceElemnt = null;
            $levelElement = null;
        }

        $this->command->info('job skills seed to ojobskills table');
    }
}
