<?php

namespace Database\Seeders;

use App\Models\HaalandMajorPivot;
use App\Models\Lang;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // $this->call(RoleSeeder::class);
        // $this->call(SkillSeeder::class);
        // $this->call(UniversitySeeder::class);
        // $this->call(Job_GroupSeeder::class);
        // $this->call(JobSeeder::class);
        // $this->call(LangSeeder::class);
        // $this->call(MbtiAnswerSeeder::class);
        // $this->call(MbtiPersonalitySeeder::class);
        // $this->call(EnneagramPersonalitySeeder::class);
        // $this->call(EnneagramQuestionSeeder::class);
        // $this->call(NeoQuestionSeeder::class);
        // $this->call(NeoPersonalitySeeder::class);
        // $this->call(HaalandJobSeeder::class);
        // $this->call(HaalandAnswerSeeder::class);
        // $this->call(HaalandFeatureSeeder::class);
        // $this->call(HaalandJobPivotSeeder::class);
        // $this->call(HaalandMajorSeeder::class);
        // $this->call(HaalandMajorPivotSeeder::class);
        // $this->call(HaalandPesonalitySeeder::class);
        // $this->call(GhqQuestionSeeder::class);
        // $this->call(PermissionSeeder::class);

        $this->call(OjobSeeder::class);
        $this->call(OcategorySeeder::class);

        $this->call(OabilitySeeder::class);
        $this->call(OabilityJobSeeder::class);
        
        $this->call(OknowledgeSeeder::class);
        $this->call(OjobKnowledgeSeeder::class);

        $this->call(OskillSeeder::class);
        $this->call(OjobSkillSeeder::class);

        $this->call(OworkStyleSeeder::class);
        $this->call(OjobWorkStyleSeeder::class);

        $this->call(OtechnologySkillSeeder::class);
        $this->call(OjobTechnologySkillSeeder::class);

        $this->call(OtaskSeeder::class);

    }
}
