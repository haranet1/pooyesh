<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HaalandPesonalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personalities = [
            ['title' => 'R','description' => 'تیپ شخصیت واقع گرایی در تست استعدادسنجی هالند به افرادی تعلق می‌گیرد که عملگرا هستند و در واقع استعدادشان کار با ابزار آلات و ماشین آلات است. این افراد کار های فیزیکی و دشوار را به خوبی انجام می‌دهند. بسیاری از افراد که در این گروه قرار دارند به کارکردن در محیط های باز تمایل دارند و خیلی با کاغذ بازی و یا کار کردن با دیگران عالقه‌ای ندارند! مشاغل واقع گرایانه، اغلب فعالیت هایی با کار های عملی و راه حل های دستی را شامل می‌شود .','fa_title' => 'واقع گرا'], //1
            ['title' => 'I','description' => 'ویژگی های شخصیتی مثل: انجام کارهای عملی، فکری، اکتشافی، اندیشمندانه و افرادی که تفکر تحلیلی و علمی دارند در تیپ شخصیتی جستجو گر معرفی می‌شود. مشاغل جستجو گر معموالا شامل کار با ایده ها بوده و این گونه از مشاغل به دنبال رسیدن به حقیقت و حل کردن ذهنی مسائل هستند.','fa_title' => 'جستجوگر'], //2
            ['title' => 'A','description' => 'این افراد در شناسایی و بیان خصوصیات و ویژگی های خود مهارت دارند. کمتر خودشان را کنترل می‌کنند و احساساتشان را به راحتی بیان می کنند. مشاغل هنری معموالا شامل کار با الگوهای مختلف، طرح ها و اشکال است. افرادی که درگیر این گونه کارها هستند اغلب احوالات خود را در کار به راحتی بیان می‌کنند.','fa_title' => 'هنری'], //3
            ['title' => 'S','description' => 'افرادی با ویژگی شخصیتی حمایت گری، درمان و مراقبت هستند که با انجام کارهایی مانند مادری کردن، باغبانی و امدادگری احساس رضایت می‌کنند. مهارت های کلامی بالایی دارند و دوست دارند با مردم کار کنند تا آنها را رشد دهند و مطلع و آگاه سازند.','fa_title' => 'اجتماعی'], //4
            ['title' => 'E','description' => 'این افراد قدرت رهبری، متقاعد کردن و قرار گرفتن در محیط های رقابتی و توانایی تصمیم گیری های مهم را دارند. معموالا این افراد ریسک پذیر بوده و در کارهای تجاری می‌توانند موفقیت های فراوانی کسب کنند. شروع کردن و به سرانجام رساندن کارها از جمله مواردی است که این گونه افراد به خوبی آن را انجام می‌دهند.','fa_title' => 'متهور، جسور و سازنده'], //5
            ['title' => 'C','description' => 'این افراد جزئی نگر هستند و می‌توانند کارهای سازمانی و دفتری را به خوبی انجام دهند. آن ها بیش تر از اینکه با ایده ها سر و کار داشته باشند، با داده ها، جزئیات و اطلاعات کار می‌کنند. این تیپ افراد معموالا خط مشی واضحی دارند که از آن پیروی می‌کنند.','fa_title' => 'قراردادی یا قاعده‌مند'], //6
        ];
        
        DB::table('haaland_pesonalities')->insertOrIgnore($personalities);
    }
}
