<?php

namespace Database\Seeders;

use App\Imports\OtaskImport;
use App\Models\Ojob;
use App\Models\Otask;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OtaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file_path = public_path('assets/docs/task_statements.xlsx');

        $tasks = Excel::toCollection(new OtaskImport , $file_path);

        foreach($tasks as $task){
            $tasks2 = $task;
        }

        $codes = [];

        foreach($tasks2 as $task){
            array_push($codes , $task['code']);
        }

        $jobs = Ojob::whereIn('code' , $codes)->get();

        $jobId = null;
        $jobCode = null;

        foreach($tasks2 as $task){
            if($task['code'] != $jobCode){
                $job = $jobs->where('code' , $task['code'])->first();
                $jobCode = $job['code'];
                $jobId = $job['id'];
            }
            Otask::create([
                'job_id' => $jobId,
                'task_code' => $task['task_id'],
                'title' => $task['task_fa'],
                'type' => $task['task_type'],
                'english_title' => $task['task']
            ]);
        }

        $this->command->info('jobs tasks seed to ojobs table');
    }
}
