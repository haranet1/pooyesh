<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HaalandAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $answers = [
            ['title' => 'نصب وسایل الکتریکی','category' => 'R','question_number' => '1','question_category' => 'activities'], //A1
            ['title' => 'تعمیر اتومبیل','category' => 'R','question_number' => '1','question_category' => 'activities'], //A2
            ['title' => 'ساخت اشیاء چوبی','category' => 'R','question_number' => '1','question_category' => 'activities'], //A3
            ['title' => 'راندن کامیون یا تراکتور','category' => 'R','question_number' => '1','question_category' => 'activities'], //A4
            ['title' => 'استفاده از ابزار ماشینی یا فلز کاری','category' => 'R','question_number' => '1','question_category' => 'activities'], //A5
            ['title' => 'تغییر ابتکاری موتور اتومبیل و موتورسیکلت','category' => 'R','question_number' => '1','question_category' => 'activities'], //A6
            ['title' => 'گذراندن دوره کارگاه','category' => 'R','question_number' => '1','question_category' => 'activities'], //A7
            ['title' => 'گذراندن دوره طراحی مکانیکی','category' => 'R','question_number' => '1','question_category' => 'activities'], //A8
            ['title' => 'گذراندن دوره دوردگری(کارهای چوبی)','category' => 'R','question_number' => '1','question_category' => 'activities'], //A9
            ['title' => 'گذراندن دوره خیاطی','category' => 'R','question_number' => '1','question_category' => 'activities'], //A10
            ['title' => 'گذراندن دوره اتوماتیک(خودرو)','category' => 'R','question_number' => '1','question_category' => 'activities'], //A11
            ['title' => 'خواندن کتاب یا مجالت علمی','category' => 'I','question_number' => '2','question_category' => 'activities'], //A12
            ['title' => 'کار درآزمایشگاه','category' => 'I','question_number' => '2','question_category' => 'activities'], //A13
            ['title' => 'کار بر روی یک پروژعلمی','category' => 'I','question_number' => '2','question_category' => 'activities'], //A14
            ['title' => 'ساختن مدل هایی از هواپیما(ماکت)','category' => 'I','question_number' => '2','question_category' => 'activities'], //A15
            ['title' => 'کار با وسایل و مواد شیمیایی','category' => 'I','question_number' => '2','question_category' => 'activities'], //A16
            ['title' => 'مطالعه آزاد درباره موضوعات خاص','category' => 'I','question_number' => '2','question_category' => 'activities'], //A17
            ['title' => 'حل معماهای ریاضی و شطرنج','category' => 'I','question_number' => '2','question_category' => 'activities'], //A18
            ['title' => 'گذراندن دوره فیزیک','category' => 'I','question_number' => '2','question_category' => 'activities'], //A19
            ['title' => 'گذراندن دوره شیمی','category' => 'I','question_number' => '2','question_category' => 'activities'], //A20
            ['title' => 'گذراندن دوره هندسه و مثلثات','category' => 'I','question_number' => '2','question_category' => 'activities'], //A21
            ['title' => 'گذراندن دوره زیست شناسی','category' => 'I','question_number' => '2','question_category' => 'activities'], //A22
            ['title' => 'طراحی ترسیم و نقاشی','category' => 'A','question_number' => '3','question_category' => 'activities'], //A23
            ['title' => 'بازیگری تئاتر و سینما','category' => 'A','question_number' => '3','question_category' => 'activities'], //A24
            ['title' => 'طراحی داخلی ساختمان یا دکراسیون','category' => 'A','question_number' => '3','question_category' => 'activities'], //A25
            ['title' => 'نوازندگی در گروه موسیقی و سرود','category' => 'A','question_number' => '3','question_category' => 'activities'], //A26
            ['title' => 'نوازندگی یکی از آلات موسیقی','category' => 'A','question_number' => '3','question_category' => 'activities'], //A27
            ['title' => 'رفتن به برنامه های اجرای موسیقی','category' => 'A','question_number' => '3','question_category' => 'activities'], //A28
            ['title' => 'خواندن داستان های معروف','category' => 'A','question_number' => '3','question_category' => 'activities'], //A29
            ['title' => 'نقاشی از روی مدل یا عکس','category' => 'A','question_number' => '3','question_category' => 'activities'], //A30
            ['title' => 'نقد نمایش نامه ها','category' => 'A','question_number' => '3','question_category' => 'activities'], //A31
            ['title' => 'خواندن شعر یا سرودن شعر','category' => 'A','question_number' => '3','question_category' => 'activities'], //A32
            ['title' => 'گذراندن دوره هنری','category' => 'A','question_number' => '3','question_category' => 'activities'], //A33
            ['title' => 'نامه نگاری با دوستان','category' => 'S','question_number' => '4','question_category' => 'activities'], //A34
            ['title' => 'شرکت در برنامه های مذهبی','category' => 'S','question_number' => '4','question_category' => 'activities'], //A35
            ['title' => 'عضویت در انجمن ها و مراکز فرهنگی واجتماعی','category' => 'S','question_number' => '4','question_category' => 'activities'], //A36
            ['title' => 'کمک به حل مشکلات شخصی دیگران','category' => 'S','question_number' => '4','question_category' => 'activities'], //A37
            ['title' => 'مراقبت از کودکان','category' => 'S','question_number' => '4','question_category' => 'activities'], //A38
            ['title' => 'شرکت در میهمانی ها و مراسم جشن و سرود','category' => 'S','question_number' => '4','question_category' => 'activities'], //A39
            ['title' => 'بیان مطالب شنیدنی و لطیفه در جمع دوستان','category' => 'S','question_number' => '4','question_category' => 'activities'], //A40
            ['title' => 'مطالب کتاب های روانشناسی','category' => 'S','question_number' => '4','question_category' => 'activities'], //A41
            ['title' => 'حضور در جلسات و سمینارها','category' => 'S','question_number' => '4','question_category' => 'activities'], //A42
            ['title' => 'شرکت در کلاس های ورزشی','category' => 'S','question_number' => '4','question_category' => 'activities'], //A43
            ['title' => 'پیدا کردن دوستان و افراد جدید','category' => 'S','question_number' => '4','question_category' => 'activities'], //A44
            ['title' => 'تاثیر گذاشتن روی دیگران','category' => 'E','question_number' => '5','question_category' => 'activities'], //A45
            ['title' => 'فروشندگی اجناس','category' => 'E','question_number' => '5','question_category' => 'activities'], //A46
            ['title' => 'بحث درباره مسائل سیاسی','category' => 'E','question_number' => '5','question_category' => 'activities'], //A47
            ['title' => 'کار کردن در مغازه یا فروشگاه شخصی','category' => 'E','question_number' => '5','question_category' => 'activities'], //A48
            ['title' => 'شرکت فعال در سمینارها','category' => 'E','question_number' => '5','question_category' => 'activities'], //A49
            ['title' => 'سخنرانی','category' => 'E','question_number' => '5','question_category' => 'activities'], //A50
            ['title' => 'خدمت به عنوان مدیر یک گروه','category' => 'E','question_number' => '5','question_category' => 'activities'], //A51
            ['title' => 'سرپرستی و نظارت برکار دیگران','category' => 'E','question_number' => '5','question_category' => 'activities'], //A52
            ['title' => 'ملاقات با شخصیت های مهم','category' => 'E','question_number' => '5','question_category' => 'activities'], //A53
            ['title' => 'رهبری یک گروه برای رسیدن به هدف','category' => 'E','question_number' => '5','question_category' => 'activities'], //A54
            ['title' => 'مشارکت در فعالیت های سیاسی','category' => 'E','question_number' => '5','question_category' => 'activities'], //A55
            ['title' => 'دقت در تمیز نگهداشتن کتاب ها و لوازم التحریر خود','category' => 'C','question_number' => '6','question_category' => 'activities'], //A56
            ['title' => 'ماشین نویسی اوراق یا نامه برای خود و دیگران','category' => 'C','question_number' => '6','question_category' => 'activities'], //A57
            ['title' => 'حسابداری ساده در کسب و کار یا امور دفتری','category' => 'C','question_number' => '6','question_category' => 'activities'], //A58
            ['title' => 'کار با انواع ماشین های اداری(حساب، کامپیوتر و غیره)','category' => 'C','question_number' => '6','question_category' => 'activities'], //A59
            ['title' => 'ثبت و نگهداری اسناد هزینه ها و درآمد','category' => 'C','question_number' => '6','question_category' => 'activities'], //A60
            ['title' => 'گذراندن دوره ماشین نویسی','category' => 'C','question_number' => '6','question_category' => 'activities'], //A61
            ['title' => 'گذراندن دوره بازرگانی','category' => 'C','question_number' => '6','question_category' => 'activities'], //A62
            ['title' => 'گذراندن دوره دفتر داری','category' => 'C','question_number' => '6','question_category' => 'activities'], //A63
            ['title' => 'گذراندن دوره حسابداری مالی','category' => 'C','question_number' => '6','question_category' => 'activities'], //A64
            ['title' => 'بایگانی نامه ها ،گزارش ها، سوابق و غیره','category' => 'C','question_number' => '6','question_category' => 'activities'], //A65
            ['title' => 'نوشتن نامه های اداری و رسمی','category' => 'C','question_number' => '6','question_category' => 'activities'], //A66
            ['title' => 'از ابزارهای برقی کارگاه نجاری مانند اره، ماشین تراش یا ماشین سمباده استفاده کرده‌ام','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A67
            ['title' => 'کار کردن با ولت متر را می‌دانم','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A68
            ['title' => 'می‌توانم کاربراتور را تنظیم کنم','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A69
            ['title' => 'با ابزار های برقی مانند دریل، آسیاب، خردکن و چرخ خیاطی کار کرده‌ام','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A70
            ['title' => 'کار جلا زدن و لکه زدایی مبلمان یا کار های چوبی را می‌توانم انجام دهم','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A71
            ['title' => 'می‌توانم نقشه ساختمان یا طرح ماشین را بخوانم','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A72
            ['title' => 'تعمیرات ساده وسایل برقی را می‌توانم انجام دهم','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A73
            ['title' => 'می‌توانم وسایل چوبی (صندلی، میز و غیره) را تعمیر کنم','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A74
            ['title' => 'رسم های فنی را می‌توانم بکشم','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A75
            ['title' => 'می‌توانم تعمیرات ساده تلویزیون را انجام دهم','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A76
            ['title' => 'تعمیرات ساده لوله کشی را می‌توانم انجام دهم','category' => 'R','question_number' => '7','question_category' => 'experiences'], //A77
            ['title' => 'طرز کار لامپ خلاء (لوله خالی از هوا)','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A78
            ['title' => 'می‌توانم سه غذا را که از لحاظ مواد پروتئینی غنی هستند را نام ببرم','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A79
            ['title' => 'مفهوم نیمه عمر عنصر رادیو اکتیو را می‌دانم','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A80
            ['title' => 'می‌توانم جدول مندلیف را توضیح دهم','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A81
            ['title' => 'می‌توانم از خط کش محاسبه برای ضرب یا تقسیم استفاده کنم','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A82
            ['title' => 'از میکروسکوپ می‌توانم استفاده کنم','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A83
            ['title' => 'سه نوع صورت فلکی ستارگان را می‌توانم شناسایی کنم','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A84
            ['title' => 'چگونگی عملکرد گلبول های سفید خون را می‌توانم توضیح دهم','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A85
            ['title' => 'فرمول های ساده شیمی را می‌توانم تفسیر کنم','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A86
            ['title' => 'می‌دانم که چرا ماهواره ها به زمین نمی‌افتند','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A87
            ['title' => 'در سمینار و یا مسابقه علمی شرکت کرده‌ام','category' => 'I','question_number' => '8','question_category' => 'experiences'], //A88
            ['title' => 'می‌توانم در نمایش تا سه نقش را بازی کنم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A89
            ['title' => 'می‌توانم شعر یا نوشته ای را دکلمه کنم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A90
            ['title' => 'می‌توانم در نمایش عروسکی بازی کنم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A91
            ['title' => 'می‌توانم افراد را طوری نقاشی کنم که قابل شناسایی باشند','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A92
            ['title' => 'می‌توانم نقاشی یا مجسمه سازی کنم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A93
            ['title' => 'می‌توانم سفال گری کنم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A94
            ['title' => 'طراحی لباس ،پوستر یا وسایل چوبی را می‌توانم انجام دهم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A95
            ['title' => 'می‌توانم به خوبی شعر بگویم یا داستان بنویسم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A96
            ['title' => 'می‌توانم یکی از آلات موسیقی را بنوازم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A97
            ['title' => 'در سرود های دو تا چهار نفره می‌توانم شر کت کنم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A98
            ['title' => 'می‌توانم یکی از آلات موسیقی را در یک جمع رسمی بنوازم','category' => 'A','question_number' => '9','question_category' => 'experiences'], //A99
            ['title' => 'می‌توانم به خوبی مطالب را برای دیگران توضیح دهم','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A100
            ['title' => 'در مورد خیریه یا جمع‌آوری کمک های مردمی شرکت کرده‌ام','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A101
            ['title' => 'با دیگران به خوبی کار و تشریک مساعی می‌کنم','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A102
            ['title' => 'در سرگرم کردن افراد بزرگتر از خود مهارت دارم','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A103
            ['title' => 'می‌توانم میزبان خوبی باشم','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A104
            ['title' => 'به راحتی می‌توانم به کودکان آموزش دهم','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A105
            ['title' => 'می‌توانم برای سرگرمی دیگران در یک مهمانی برنامه ریزی کنم','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A106
            ['title' => 'به خوبی می‌توانم به افراد مضطرب یا دارای مشکل کمک کنم','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A107
            ['title' => 'دواطلبانه در بیمارستان، کلینیک یا مراکز امداد رسانی (هلال احمر و ...) کار کرده‌ام','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A108
            ['title' => 'برای امور خیریه مربوط به مدرسه یا مسجد می‌توانم برنامه ریزی کنم','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A109
            ['title' => 'درباره شخصیت افراد به خوبی می‌توانم قضاوت کنم','category' => 'S','question_number' => '10','question_category' => 'experiences'], //A111
            ['title' => 'در مدرسه به عنوان نماینده انتخاب شده‌ام','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A112
            ['title' => 'می‌توانم کار دیگران را سر پرستی کنم','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A113
            ['title' => 'اشتیاق و انرژی فوق العاده‌ای در انجام کارهایم دارم','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A114
            ['title' => 'به خوبی می‌توانم مردم را به انجام کارهای مورد علاقه خودم وادار کنم','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A115
            ['title' => 'در فروشندگی مهارت دارم','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A116
            ['title' => 'به عنوان نماینده یک گروه در ارائه پیشنهادات، با شکایت به مقام بالاتر انجام وظیفه کرده‌ام','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A117
            ['title' => 'در کار فروشندگی یا رهبری گروه پاداش گرفته‌ام','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A118
            ['title' => 'باشگاه، گروه و یا دسته ای را سازمان داده‌ام','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A119
            ['title' => 'اقدام به ایجاد کسب و کار نموده‌ام','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A120
            ['title' => 'می‌دانم چگونه سر دسته و یا نماینده موفقی باشم','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A121
            ['title' => 'مباحثه گر خوبی هستم','category' => 'E','question_number' => '11','question_category' => 'experiences'], //A122
            ['title' => 'در هر دقیقه می‌توانم 40 کلمه تایپ کنم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A123
            ['title' => 'می‌توانم با دستگاه فتوکپی یا ماشین جمع بندی قیمت کالا کار کنم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A124
            ['title' => 'می‌توانم تند نویسی کنم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A125
            ['title' => 'می‌توانم نامه ها و سایر اوراق را بایگانی کنم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A126
            ['title' => 'در امور اداری کار کرده‌ام','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A127
            ['title' => 'می‌توانم زمان بندی مشخصی برای انجام کارهایم داشته باشم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A128
            ['title' => 'می‌توانم مقدار زیادی از کارهای دفتری را در مدت کوتاهی انجام دهم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A129
            ['title' => 'از ماشین حساب می‌توانم استفاده کنم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A130
            ['title' => 'می‌توانم از کامپیوتر استفاده کنم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A131
            ['title' => 'می‌توانم بدهی‌ها و طلب‌ها (ترازنامه) را در دفتر کل وارد کنم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A132
            ['title' => 'می‌توانم گزارش‌های دقیق پرداخت‌ها و فروش‌ها را نگهداری و تنظیم کنم','category' => 'C','question_number' => '12','question_category' => 'experiences'], //A133
            ['title' => 'مکانیک هواپیما','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A134
            ['title' => 'متخصص موجودات آبزی و وحشی','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A135
            ['title' => 'مکانیک اتومبیل','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A136
            ['title' => 'نجار','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A137
            ['title' => 'راننده خاکبری برقی (جرثقیل)','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A138
            ['title' => 'نقشه بردار','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A139
            ['title' => 'بازرس ساختمان','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A140
            ['title' => 'مهندس مخابرات','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A141
            ['title' => 'جوشکار','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A142
            ['title' => 'پرورش دهنده گل ها و گیاهان','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A143
            ['title' => 'راننده تاکسی سرویس (آژانس)','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A144
            ['title' => 'مهندس لوکوموتیو','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A145
            ['title' => 'خیاط','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A146
            ['title' => 'برق کار','category' => 'R','question_number' => '13','question_category' => 'jobs'], //A147
            ['title' => 'متخصص هواشناسی','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A148
            ['title' => 'زیست شناسی','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A149
            ['title' => 'ستاره شناس (منجم)','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A150
            ['title' => 'تکنسین آزمایشگاه پزشکی','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A151
            ['title' => 'مردم شناس','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A152
            ['title' => 'دام پزشک','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A153
            ['title' => 'شیمی دان','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A154
            ['title' => 'پژوهشگر (محقق)','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A155
            ['title' => 'نویسنده مقالات علمی','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A156
            ['title' => 'سر دبیر یک مجله علمی','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A157
            ['title' => 'زمین شناس','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A158
            ['title' => 'گیاه شناس','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A159
            ['title' => 'کارمند تحقیقات علمی ','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A160
            ['title' => 'فیزیکدان','category' => 'I','question_number' => '14','question_category' => 'jobs'], //A161
            ['title' => 'شاعر','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A161
            ['title' => 'رهبر اورکستر','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A162
            ['title' => 'موسیقی دان','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A163
            ['title' => 'نویسنده','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A164
            ['title' => 'هنرمند آگهی های تبلیغاتی','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A165
            ['title' => 'نویسنده ادبیات کودکان','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A166
            ['title' => 'بازیگر سینما و تاتر','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A167
            ['title' => 'خبرنگار','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A168
            ['title' => 'صورت گر (نقاش)','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A169
            ['title' => 'خواننده سرود','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A170
            ['title' => 'آهنگساز','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A171
            ['title' => 'مجسمه ساز','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A172
            ['title' => 'نمایش نامه نویس','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A173
            ['title' => 'فیلم ساز','category' => 'A','question_number' => '15','question_category' => 'jobs'], //A174
            ['title' => 'جامعه شناس','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A175
            ['title' => 'دبیر دبیرستان','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A178
            ['title' => 'متخصص اصالح و تربیت رفتار','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A179
            ['title' => 'گفتار درمانگر','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A180
            ['title' => 'مشاور ازدواج','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A181
            ['title' => 'مدیر مدرسه','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A182
            ['title' => 'سرپرست خوابگاه','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A183
            ['title' => 'روانشناس','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A184
            ['title' => 'دبیر علوم اجتماعی','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A185
            ['title' => 'مدیر امور خیریه','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A186
            ['title' => 'مربی ورزش','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A187
            ['title' => 'مشاور','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A188
            ['title' => 'دستیار امور روانپزشکی','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A189
            ['title' => 'راهنما و مشاور شغلی','category' => 'S','question_number' => '16','question_category' => 'jobs'], //A190
            ['title' => 'معامله گر، واسطه گر امور تجاری','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A191
            ['title' => 'مامور خرید','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A192
            ['title' => 'مدیر اجرایی آگهی و مسئول تبلیغات','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A193
            ['title' => 'نماینده فروش کارخانه تولیدی','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A194
            ['title' => 'تولید کننده برنامه های تلویزیونی','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A195
            ['title' => 'مدیر هتل','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A196
            ['title' => 'مدیر شرکت های بازرگانی','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A197
            ['title' => 'مدیر رستوران','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A198
            ['title' => 'مدیر مراسم و تشریفات','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A199
            ['title' => 'فروشنده','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A200
            ['title' => 'واسطه گر معاملات ملکی یا اتومبیل','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A201
            ['title' => 'مدیر تبلیغات یا مدیر روابط عمومی','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A202
            ['title' => 'موسس باشگاه ورزشی','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A203
            ['title' => 'مدیر فروش','category' => 'E','question_number' => '17','question_category' => 'jobs'], //A204
            ['title' => 'دفتر دار','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A205
            ['title' => 'معلم درس ماشین نویسی','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A206
            ['title' => 'مسئول برنامه های بودجه (ذی حساب)','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A207
            ['title' => 'حسابدار تحصیل کرده','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A208
            ['title' => 'ازرش مالی اداری','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A209
            ['title' => 'تند نویس دادگاه','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A210
            ['title' => 'تحویل دار بانک','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A211
            ['title' => 'کارشناس مالیات','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A212
            ['title' => 'ناظر صورت برداری از موجودی','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A213
            ['title' => 'اپراتور کامپیوتر','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A214
            ['title' => 'ارزیاب هزینه ها (کارپرداز)','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A215
            ['title' => 'کارشناس امور مالی','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A216
            ['title' => 'کارمند امور مالی','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A217
            ['title' => 'بازرس بانک','category' => 'C','question_number' => '18','question_category' => 'jobs'], //A218
        ];
        
        DB::table('haaland_answers')->insertOrIgnore($answers);
    }
}
