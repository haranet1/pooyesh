<?php

namespace Database\Seeders;

use App\Models\ResumeSent;
use Illuminate\Database\Seeder;

class ResumeRequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ResumeSent::whereColumn('receiver_id','sender_id')->delete();
        ResumeSent::where('id' , '68')->delete();
    }
}
