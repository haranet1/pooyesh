<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HaalandJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobs = [
            'title' => 'مهندس مکانیک', // 1
            'title' => 'مهندس ماشین آلات سنگین', // 2
            'title' => 'خلبان', // 3
            'title' => 'تکنسین', // 4
            'title' => 'تعمیرات و تاسیسات', // 5
            'title' => 'پزشک', // 6
            'title' => 'فیزیک دان', // 7
            'title' => 'جراح', // 8
            'title' => 'شیمیدان', // 9
            'title' => 'دندان پزشک', // 10
            'title' => 'معماری', // 11
            'title' => 'آهنگساز', // 12
            'title' => 'طراح تبلیغات', // 13
            'title' => 'فیلمنامه نویس', // 14
            'title' => 'خوشنویس', // 15
            'title' => 'شاعر', // 16
            'title' => 'بازیگر', // 17
            'title' => 'نویسنده', // 18
            'title' => 'نقاش', // 19
            'title' => 'روزنامه نگار', // 20
            'title' => 'معلم', // 21
            'title' => 'مدیر مدرسه', // 22
            'title' => 'مربی الهیات و علوم دینی', // 23
            'title' => 'مربی تربیتی', // 24
            'title' => 'جامعه شناس', // 25
            'title' => 'روانشناس', // 26
            'title' => 'باغبانی', // 27
            'title' => 'امدادگری', // 28
            'title' => 'کار در مراکز خیریه', // 29
            'title' => 'مدیر بازرگانی', // 30
            'title' => 'بازاریاب', // 31
            'title' => 'رهبری', // 32
            'title' => 'مدیر هتل و رستوران', // 33
            'title' => 'حسابدار', // 34
            'title' => 'کارشناس امور اداری', // 35
            'title' => 'کارشناس و مشاور مالیاتی', // 36
            'title' => 'کارشناس امور گمرکی', // 37
        ];

        DB::table('haaland_jobs')->insertOrIgnore($jobs);
    }
}
