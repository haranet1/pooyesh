<?php

namespace Database\Seeders;

use App\Imports\OtechnologySkillImport;
use App\Models\OtechnologySkill;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OtechnologySkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('assets/docs/7-Technology Skills.xlsx');

        $tecnologySkill = Excel::toCollection(new OtechnologySkillImport , $path);

        foreach ($tecnologySkill as $techSkill_collection){
            $techSkills = $techSkill_collection;
        }

        $techSkill_codes = [];

        foreach ($techSkills as $techSkill){

            if (! in_array($techSkill['techskill_code'], $techSkill_codes)){

                array_push($techSkill_codes, $techSkill['techskill_code']);

                OtechnologySkill::create([
                    'code' => $techSkill['techskill_code'],
                    'title' => $techSkill['title_fa'],
                    'english_title' => $techSkill['title']
                ]);
            }
        }

        $this->command->info('technology skill seed to otechnologyskills');
    }
}
