<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UniversitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'واحد نجف آباد'],
            ['name' => 'واحد خمینی شهر'],
            ['name' => 'واحد تیران'],
            ['name' => 'واحد خوراسگان'],
        ];
        DB::table('universities')->insertOrIgnore($items);
    }
}
