<?php

namespace Database\Seeders;

use App\Models\CooperationRequest;
use App\Models\Job_position;
use App\Models\job_skill;
use App\Models\Ojob;
use App\Models\OjobPosition;
use Illuminate\Database\Seeder;

class MergeJobPositionsTableAndOjobPositionsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobs = Job_position::all();

        foreach($jobs as $job){
            $oldSkills = job_skill::where('job_position_id' , $job->id)->get();

            $cooperationRequests = CooperationRequest::where('job_position_id', $job->id)->get();

            $ojob = Ojob::where('code' , $job->job_id)->first();

            $experience = null;
            if(! is_null($job->experience) && is_numeric($job->experience)){

                if($job->experience == 0){
                    $experience = 'unmatter';
                } else {
                    $experience = $job->experience;
                }

            }

            $salary = 'agreement';

            if(!empty($job->salary) && is_numeric($job->salary)){

                switch(true){
                    case $job->salary <= 400000 :
                        $salary = 4;
                        break;
                    case $job->salary >= 4000000 && $job->salary <= 8000000:
                        $salary = 8;
                        break;
                    case $job->salary >= 8000000 && $job->salary <= 16000000:
                        $salary = 16;
                        break;
                    case $job->salary >= 16000000 && $job->salary <= 25000000:
                        $salary = 25;
                        break;
                    case $job->salary >= 25000000 && $job->salary <= 30000000:
                        $salary = 30;
                        break;
                }

            }

            $age = 'unmatter';

            if($job->age != 0){
                switch(true){
                    case $job->age <= 20:
                        $age = 20;
                        break;
                    case $job->age >= 20 && $job->age <= 30:
                        $age = 30;
                        break;
                    case $job->age >= 30 && $job->age <= 40:
                        $age = 40;
                        break;
                }
            }

            $sex = 'unmatter';

            if(!empty($job->sex) && $job->sex != 0){
                $sex = change_sex_fa_to_en($job->sex);
            }

            $marige_type = 'unmatter';

            if(!empty($job->marige_type) && $job->marige_type != 0){
                $marige_type = change_marige_type_fa_to_en($job->marige_type);
            }

            $status = null;

            if($job->status == 1){
                $status = 1;
            } elseif($job->status == 2){
                $status = 0;
            }

            $ojob_position = OjobPosition::create([
                'company_id' => $job->company_id,
                'job_id' => $ojob->id,
                'title' => $job->title,
                'description' => $job->description,
                'type' => $job->type,
                'level' => $job->level,
                'experience' => $experience,
                'working_hours' => null,
                'salary' => $salary,
                'age' => $age,
                'sex' => $sex,
                'marige_type' => $marige_type,
                'military_service_status' => 'unmatter',
                'grade' => 'unmatter',
                'major_id' => null,
                'province_id' => $job->province_id,
                'city_id' => $job->city_id ?? 0,
                'address' => 'unmatter',
                'about' => null,
                'status' => $status,
                'active' => $job->active,
                'deleted' => $job->deleted,
            ]);

            foreach($oldSkills as $skill){
                $skill->update([
                    'job_position_id' => $ojob_position->id
                ]);
            }

            foreach($cooperationRequests as $request){
                $request->update([
                    'job_position_id' => $ojob_position->id
                ]);
            }

        }
    }
}
