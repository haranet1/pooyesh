<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MajorCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $major_categories = [
            [
                'title' => 'ریاضی و فیزیک'
            ],
            [
                'title' => 'علوم تجربی'
            ],
            [
                'title' => 'علوم انسانی'
            ],
            [
                'title' => 'هنر و زبان'
            ],
        ];

        DB::table('majors_categories')->insertOrIgnore($major_categories);
    }
}
