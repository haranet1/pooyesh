<?php

namespace Database\Seeders;

use App\Models\PooyeshContract;
use Illuminate\Database\Seeder;

class PooyeshContractTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pooyeshContradcts = PooyeshContract::get();

        foreach($pooyeshContradcts as $contract){
            $created_at = $contract->created_at;

            
            $todayContracts = PooyeshContract::whereBetween('created_at' , [$created_at->startOfDay() , $created_at->endOfDay()])->count();
            
            $todayContracts++;
            $file_name = verta($created_at)->year.'/'.'پ'.'/'.verta($created_at)->month.verta($created_at)->day.$todayContracts;
            
            $contract->update([
                'name' => $file_name,
            ]);
        }

        $this->command->info('contract name is change :)');
    }
}
