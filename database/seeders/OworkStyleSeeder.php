<?php

namespace Database\Seeders;

use App\Imports\OworkStyleImport;
use App\Models\OworkStyle;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OworkStyleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('assets/docs/6-Work Styles.xlsx');

        $work_excel = Excel::toCollection(new OworkStyleImport , $path);

        foreach( $work_excel as $works ) {

            $work_styles = $works;

        }

        $codes = [];

        foreach( $work_styles as $work_style ) {

            if(! in_array($work_style['work_code'] , $codes) ) {

                array_push( $codes, $work_style['work_code'] );

                OworkStyle::create([
                    'code' => $work_style['work_code'],
                    'title' => $work_style['title_fa'],
                    'english_title' => $work_style['title']
                ]);

            }

        }

        $this->command->info('work styles seed to owork_styles table');
    }
}
