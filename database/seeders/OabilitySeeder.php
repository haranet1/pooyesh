<?php

namespace Database\Seeders;

use App\Imports\OabilityImport;
use App\Models\Oability;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OabilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path1 = public_path('assets/docs/1_ability.xlsx');

        $abilities1 = Excel::toCollection(new OabilityImport , $path1);

        foreach($abilities1 as $ability){
            $abilitiesCollection1 = $ability;
        }

        $ability_codes = [];

        foreach($abilitiesCollection1 as $abi){

            if(! in_array($abi['ability_code'] , $ability_codes)){

                array_push($ability_codes , $abi['ability_code']);

                Oability::create([
                    'code' => $abi['ability_code'],
                    'title' => $abi['title_fa'],
                    'english_title' => $abi['title']
                ]);
            }
        }

        $this->command->info('ability seed to oabilities table');       
    }
}
