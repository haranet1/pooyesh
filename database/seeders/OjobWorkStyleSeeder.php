<?php

namespace Database\Seeders;

use App\Imports\OworkStyleImport;
use App\Models\Oimportance;
use App\Models\Ojob;
use App\Models\OjobWorkStyle;
use App\Models\OworkStyle;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OjobWorkStyleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('assets/docs/6-Work Styles.xlsx');

        $work_excel = Excel::toCollection(new OworkStyleImport , $path);

        foreach( $work_excel as $works ) {

            $work_styles = $works;

        }

        $jobCodes = [];
        $workCodes = [];

        foreach ($work_styles as $job_work) {
            array_push($jobCodes, $job_work['code']);
            array_push($workCodes, $job_work['work_code']);
        }

        $jobs = Ojob::whereIn('code', $jobCodes)->get();
        $workstyles = OworkStyle::whereIn('code', $workCodes)->get();
        $jobElement = null;
        $workElement = null;

        foreach ($work_styles as $job_work) {
            $jobElement = $jobs->where('code', $job_work['code'])->first();
            $workElement = $workstyles->where('code', $job_work['work_code'])->first();
            if ($jobElement['code'] == $job_work['code'] and $workElement['code'] == $job_work['work_code']) {

                $importance = Oimportance::create([
                    'data_value' => $job_work['data_value'] ?? 0,
                    'lower' => $job_work['lower_bound'] ?? 0,
                    'upper' => $job_work['upper_bound'] ?? 0
                ]);

                OjobWorkStyle::create([
                    'job_id' => $jobElement['id'],
                    'work_style_id' => $workElement['id'],
                    'importance_id' => $importance['id']
                ]);
            }

            $jobElement = null;
            $workElement = null;
        }

        $this->command->info('job work styles seed to ojob_work_styles');

    }
}
