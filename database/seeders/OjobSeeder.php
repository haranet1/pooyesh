<?php

namespace Database\Seeders;

use App\Imports\OjobImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class OjobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = asset('assets/docs/1-jobs.xlsx');

        Excel::import(new OjobImport , $path);

        $this->command->info('onet job seed in ojobs table');
    }
}
