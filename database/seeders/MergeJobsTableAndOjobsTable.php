<?php

namespace Database\Seeders;

use App\Models\Job;
use App\Models\Ojob;
use Illuminate\Database\Seeder;

class MergeJobsTableAndOjobsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobs = Job::all();

        foreach($jobs as $job){
            
            switch($job->category_id) {
                case 5:
                case 6:
                case 13:
                case 15:
                case 21:
                case 26:
                case 35:
                case 42:
                    $job->category_id = 1;
                    break;
                case 2:
                case 12:
                case 14:
                case 20:
                case 29:
                case 30:
                case 47:
                    $job->category_id = 2;
                    break;
                case 3:
                case 22:
                case 23:
                case 32:
                case 38:
                    $job->category_id = 4;
                    break;
                case 50:
                    $job->category_id = 5;
                    break;
                case 1:
                case 18:
                case 37:
                case 49:
                    $job->category_id = 6;
                    break;
                case 10:
                case 24:
                    $job->category_id = 8;
                    break;
                case 46:
                    $job->category_id = 9;
                    break;
                case 9:
                    $job->category_id = 10;
                    break;
                case 7:
                case 11:
                    $job->category_id = 11;
                    break;
                case 28:
                    $job->category_id = 13;
                    break;
                case 8:
                    $job->category_id = 14;
                    break;
                case 36:
                case 40:
                case 43:
                case 45:
                    $job->category_id = 15;
                    break;
                case 31:
                case 44:
                    $job->category_id = 16;
                    break;
                case 16:
                case 19:
                    $job->category_id = 18;
                    break;
                case 25:
                case 48:
                    $job->category_id = 19;
                    break;
                case 41:
                    $job->category_id = 20;
                    break;
                case 4:
                case 17:
                case 34:
                    $job->category_id = 21;
                    break;
                case 27:
                case 33:
                    $job->category_id = 22;
                    break;
                case 39:
                    $job->category_id = 23;
                    break;
            }
            
            
            Ojob::create([
                'category_id' => $job->category_id,
                'code' => $job->id,
                'title' => $job->name,
            ]);
        }

    }
}
