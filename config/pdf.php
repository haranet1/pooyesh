<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Laravel Pdf',
	'display_mode'          => 'fullpage',
	'tempDir'               => public_path('files/pooyesh-contracts/pdf'),
//	'pdf_a'                 => false,
//	'pdf_a_auto'            => false,
//	'icc_profile_path'      => '',
    'font_path' => public_path('/fonts'),
    'font_data' => [
        'Sans' => [
            'R'  => 'woff/IRANSansWeb.woff',
            // 'B'  => 'ttf/IRANSansWeb_Bold.ttf',
            'useOTL' => 0xFF,
            'useKashida' => 75,
        ],

    ],
    'dompdf' => [
        'options' => [
            'size' => 'A4',
            // 'orientation' => 'portrait',
            // other options...
        ],
    ],
];
