module.exports = {
    darkMode: 'class', // یا 'media' برای تطابق با تنظیمات سیستم
    content: [
        './vendor/filament/**/*.blade.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.js',
    ],
    theme: {
        extend: {},
    },
    plugins: [],
}
